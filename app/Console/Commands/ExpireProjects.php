<?php

namespace App\Console\Commands;

use App\Events\Common\Project\ProjectExpired;
use App\Models\Project\Project;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireProjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expires projects that have reached the x days limit from config';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Expire open projects that have not made it.
        $projects = Project::where('status', 1)->where('approved_at','<', Carbon::now()->subDays(config('app.settings.request_days_expire')))->get();

        foreach ($projects As $project) {
            $project->update(['status'=> 5]);

            event(new ProjectExpired($project));


        }
    }
}
