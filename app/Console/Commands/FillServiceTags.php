<?php

namespace App\Console\Commands;

use App\Helpers\SearchHelper;
use App\Models\Service\Service;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FillServiceTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'services:fill_tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates tags with title, occupation and category names (plural + singular)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $removewords = ['services', 'service'];

        $services = Service::with('categories', 'occupation')->get();
        foreach ($services AS $service) {

            $service->fillTags($service);
            echo '*';
        }
    }
}
