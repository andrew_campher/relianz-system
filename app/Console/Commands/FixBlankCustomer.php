<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixBlankCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:blank_customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix blank customer ids populated by sync';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Pull All sales from invoicelineitems - Include the Downpacked adjustments for bulk items.
         * Exclude items that were creditted.
         */

        $blanks = DB::connection('qbdb')
            ->select("SELECT il.ReferenceNumber, MAX(s.CustomerName) CustomerName, MAX(s.CustomerId) CustomerId FROM InvoiceLineItems il
 LEFT JOIN SalesOrderLinkedTransactions st ON st.TransactionReferenceNumber = il.ReferenceNumber
 LEFT JOIN SalesOrders s ON s.ID = st.SalesOrderId
 WHERE il.CustomerId = '' OR il.CustomerId = 0 GROUP BY il.ReferenceNumber");

        foreach ($blanks AS $blank) {
            if ($blank->CustomerId) {
                $updateSQL = "UPDATE InvoiceLineItems il LEFT JOIN Invoices i ON i.ID = il.InvoiceId 
SET il.CustomerId = '".$blank->CustomerId."' AND il.CustomerName = '".addslashes($blank->CustomerName)."' AND i.CustomerId = '".$blank->CustomerId."' AND i.CustomerName = '".addslashes($blank->CustomerName)."' 
WHERE il.ReferenceNumber = '".$blank->ReferenceNumber."' AND il.CustomerId = '' AND il.CustomerName = ''";
                DB::connection('qbdb')->update($updateSQL);

                $updateSQL = "UPDATE SalesOrderLinkedTransactions SET CustomerId = '".$blank->CustomerId."' AND CustomerName = '".addslashes($blank->CustomerName)."' WHERE TransactionReferenceNumber = '".$blank->ReferenceNumber."' AND CustomerId = '' AND CustomerName = ''";
                DB::connection('qbdb')->update($updateSQL);
            }
        }
    }
}
