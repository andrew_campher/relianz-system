<?php

namespace App\Console\Commands;

use App\Models\Location\Location;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GetGooglePlacesInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google:sync_suburbs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finds out which Towns/Cities the suburb falls in';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $locations = Location::whereNull('parent_id')->where('type','!=','province')->offset(0)->limit(2000)->get();

        $comsp = ['sublocality_level_2', 'sublocality_level_1', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'postal_code'];

        foreach($locations AS $location) {

            $loc = DB::table('locations')->where('id',$location->id)->first();
            if ($loc->parent_id) {
                echo "Location has PArent_id.\n";
                continue;
            }

            echo $location->title."\n";

            echo $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$location->lat.','.$location->lng.'&result_type=political|country|locality|sublocality&key='.config('app.google.api_maps_key');

            echo "\n";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = json_decode(curl_exec($ch));


            if (isset($result->results[0]->address_components) && $result->results[0]->address_components) {

                $start_recording = 0;
                $found = 0;

                $first_id = $location->id;
                $location_type = 'sublocality_level_2';

                $search_location = $location->title;

                ##LOOP THROUGH all to see if LAT / Lng is WRONG
                foreach ($result->results[0]->address_components AS $part){
                    if (strtolower($part->long_name) == strtolower($location->title)) {
                        $found = 1;
                    }
                }
                ###
                if (!$found) {
                    $subsub = DB::table('locations')->select('id')->where('title',$result->results[0]->address_components[0]->long_name)->first();

                    $start_recording = 1;

                    if(!$subsub) {
                        echo "Location NOT found in Database\n";

                        $id = DB::table('locations')
                            ->insertGetId(
                                [
                                    'title' => $result->results[0]->address_components[0]->long_name,
                                    'slug' => str_slug($result->results[0]->address_components[0]->long_name),
                                    'lat' => $result->results[0]->geometry->location->lat,
                                    'lng' => $result->results[0]->geometry->location->lng,
                                    'type' => $result->results[0]->address_components[0]->types[0],
                                ]);

                        $search_location = $result->results[0]->address_components[0]->long_name;

                        $first_id = $id;
                    }
                }



                foreach ($result->results[0]->address_components AS $key => $part){

                    #echo $part->long_name.' | '.implode(',',$part->types)." | ".implode(',',$comsp)."\n";

                    echo $part->long_name." == ".$location->title."\n";

                    #We have found this locations level - record from that point
                    if (!$start_recording && strtolower($part->long_name) == strtolower($search_location)) {
                        $start_recording = 1;
                        echo "Found this ITem\n";

                        continue;
                    }


                    if (array_intersect($part->types, $comsp)) {

                        #Go ahead and start recording from that point
                        if ($start_recording) {

                            echo "Start recording\n";

                            //UPDATE sub Suburb
                            if (in_array('sublocality_level_1', $part->types)) {

                                echo "Suburb Found\n";

                                $sublocality_level_1 = DB::table('locations')->select('id')->where('title',$part->long_name)->first();

                                if($sublocality_level_1) {
                                    DB::table('locations')
                                        ->where('id', $first_id)->whereNull('parent_id')
                                        ->update(['parent_id' => $sublocality_level_1->id, 'type' => $location_type]);

                                    $location_type = 'sublocality_level_1';

                                    $first_id = $sublocality_level_1->id;
                                } else {
                                    #INSERT suburb

                                }
                            }


                            //UPDATE Suburb
                            if (in_array('administrative_area_level_3', $part->types)) {

                                echo "City Found\n";

                                $administrative_area_level_3 = DB::table('locations')->select('id')->where('title',$part->long_name)->first();

                                if($administrative_area_level_3) {
                                    DB::table('locations')
                                        ->where('id', $first_id)->whereNull('parent_id')
                                        ->update(['parent_id' => $administrative_area_level_3->id, 'type' => $location_type]);

                                    $location_type = 'administrative_area_level_3';

                                    $first_id = $administrative_area_level_3->id;
                                } else {
                                    #INSERT CITY
                                }
                            }

                            //UPDATE CITY
                            if (in_array('administrative_area_level_1', $part->types)) {

                                echo "Province Found\n";

                                $administrative_area_level_1 = DB::table('locations')->select('id')->where('title',$part->long_name)->first();

                                if($administrative_area_level_1) {
                                    DB::table('locations')
                                        ->where('id', $first_id)->whereNull('parent_id')
                                        ->update(['parent_id' => $administrative_area_level_1->id, 'type' => $location_type]);

                                    $first_id = $administrative_area_level_1->id;
                                } else {
                                    #INSERT
                                }
                            }

                            if (in_array('postal_code', $part->types)) {

                                echo "Update Postal\n";

                                DB::table('locations')
                                    ->where('id', $location->id)->whereNull('postal_code')
                                    ->update(['postal_code' => $part->long_name]);
                            }


                        } else {
                            /*//LOWER
                            #Probably lower level places - We can check if we have it (smaller suburbs) and add it.
                            $loc = DB::table('locations')->where('title',$part->long_name)->get();
                            if ($loc->isEmpty()) {
                                $newlocs[] = ['slug' => 'taylor@example.com', 'title' => 0, 'parent_id' => $parent_id];
                            }*/
                        }
                    } else {
                        //echo "Not FOUND in: ".implode(';',$comsp)."\n";
                    }
                }
            } else {
                echo 'No results';
            }

        }
    }
}
