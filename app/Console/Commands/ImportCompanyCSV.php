<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportCompanyCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'professional:import_csv {file_path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports professionals from CSV | #php artisan professional:import_csv {file_path}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if (!$this->argument('file_path')) {
            echo 'No file_path given data/';
            exit;
        }

        //
        //
        $csvFile = public_path('data/'.$this->argument('file_path'));

        if (file_exists($csvFile)) {
            $delimiters = array(
                'semicolon' => ";",
                'tab'       => "\t",
                'comma'     => ",",
            );

            $csv = file_get_contents($csvFile);
            foreach ($delimiters as $key => $delim) {
                $res[$key] = substr_count($csv, $delim);
            }

            arsort($res);

            reset($res);
            $first_key = key($res);
        } else {
            echo 'File does not exist';
            exit;
        }




        $row = 1;
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            #Loop through each row
            while (($data = fgetcsv($handle, 0, $delimiters[$first_key], "\"")) !== FALSE) {

                $header_row = 0;

                #Clean Data
                foreach ($data AS $key => $item) {
                    $data[$key] = trim($item);
                    if ($data[$key] == 'Title') {
                        $header_row = 1;
                    }
                }

                if ($header_row == 1) {
                    continue;
                }


                $company = array();
                $branch = array();

                $NEW_INSERT = null;

                foreach ($data AS $key => $field) {
                    $field = trim($field);
                    switch ($key) {
                        #title - companyn ame
                        case 0:

                            $company['title'] = ucwords(strtolower($field));
                            $company['title'] = str_replace(' Cc', ' CC', $company['title']);
                            break;
                        case 1:
                            $company['tags'] = $field;
                            break;
                        case 2:
                            $company['phone'] = $field?'0'.$field:'';
                            $branch['telephone'] = $company['phone'];
                            break;
                        case 3:
                            $company['fax'] = $field?'0'.$field:'';
                            break;
                        case 4:
                            $company['alt_phone'] = $field?'0'.$field:'';
                            break;
                        case 5:
                            $company['mobile'] = $field?'0'.$field:'';
                            $branch['mobile'] = $company['mobile'];
                            break;
                        case 6:
                            $company['website'] = $field;
                            break;
                        case 7:
                            $company['email'] = $field;
                            $branch['email'] = $company['email'];
                            break;
                        case 8:
                            $company['lat'] = $field;
                            $branch['lat'] = $field;
                            break;
                        case 9:
                            $company['lng'] = $field;
                            $branch['lng'] = $field;
                            break;
                        case 10:
                            $company['street'] = $field;
                            break;
                        case 11:
                            $company['city'] = $field;
                            break;
                        case 12:
                            $company['postcode'] = $field;
                            break;
                        case 13:
                            $company['province'] = $field;
                            break;
                        case 14:
                            #Skip country
                            continue;
                            break;
                        case 15:
                            $company['area'] = $field;
                            $branch['area'] = $company['area'];
                            break;
                    }
                }


                ##ADD PROFESSIONAL
                if (!$professional_id = DB::table('professionals')->select('id')->where('title', $company['title'])->value('id')) {

                    $NEW_INSERT = 1;

                    $professional_id = DB::table('professionals')
                        ->insertGetId(
                            [
                                'title' => $company['title'],
                                'slug' => str_slug($company['title']),
                                'website_url' => $company['website'],
                                'distance_travelled' => 30,
                                'company_email' => $company['email'],
                                'notify_mobile' => $company['mobile'],
                                'notify_email' => $company['email'],
                                'telephone' => $company['phone'],
                                'alt_telephone' => $company['alt_phone'],
                                'area' => $company['area'],
                                'lng' => $company['lng'],
                                'lat' => $company['lat'],
                                'status' => 2,
                                'created_at' => Carbon::now(),
                                'approved' => 1,
                            ]);

                }

                /**
                 * Branches
                 */
                if ($professional_id) {
                    if (!$branch_id = DB::table('professional_branches')->select('id')->where('professional_id', $professional_id)->where('area', $branch['area'])->value('id')) {
                        DB::table('professional_branches')
                            ->insertGetId(
                                [
                                    'professional_id' => $professional_id,
                                    'distance_travelled' => 20,
                                    'notify_mobile' => $branch['mobile'],
                                    'notify_email' => $branch['email'],
                                    'telephone' => $branch['telephone'],
                                    'area' => $branch['area'],
                                    'lng' => $branch['lng'],
                                    'lat' => $branch['lat'],
                                ]);

                    }
                }


                #TAGS
                /**
                 *  ONLY ON NEW INSERT
                 */
                if ($NEW_INSERT && $professional_id && $company['tags']) {

                    #Delete all first
                    DB::table('professional_service')->where('professional_id', $professional_id)->delete();

                    $tags = explode(",", $company['tags']);
                    foreach ($tags AS $tag)
                    {
                        $tag = trim(strtolower($tag));
                        if (!$tag_id = DB::table('tags')->select('id')->where('name', $tag)->value('id')) {
                            $tag_id = DB::table('tags')
                                ->insertGetId(
                                    [
                                        'name' => $tag,
                                    ]);
                        }

                        DB::table('taggables')
                            ->insertGetId(
                                [
                                    'tag_id' => $tag_id,
                                    'taggable_id' => $professional_id,
                                    'taggable_type' => 'professional',
                                ]);

                        /**
                         * FIND SERVICE TO MATCH THIS TAG
                         * SELECT *, MATCH(tags) AGAINST ('beds and bedding accessories') AS relevance FROM `services` WHERE MATCH(tags) AGAINST ('beds and bedding accessories')
                         */


                        $proservices = DB::table('services')->select('id')->whereRaw('MATCH(tags) AGAINST (?)', [$tag])->limit(21)->get();
                        if (!$proservices->isEmpty() && COUNT($proservices) < 20) {
                            foreach ($proservices AS $proservice) {
                                DB::table('professional_service')
                                    ->insertGetId(
                                        [
                                            'service_id' => $proservice->id,
                                            'professional_id' => $professional_id,
                                        ]);
                            }
                        }


                    }
                }

                echo '*';


                $row++;


            }
            fclose($handle);
        }
    }
}
