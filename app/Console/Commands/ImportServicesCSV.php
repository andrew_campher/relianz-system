<?php

namespace App\Console\Commands;

use App\Models\Category\Category;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportServicesCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'services:import_csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports services from CSV file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /*
         * RUN THIS to unpublish sqm questions
         * UPDATE questions LEFT JOIN questions_options ON questions_options.question_id = questions.id SET questions.status = 0 WHERE questions_options.title LIKE '%sqm%'
         *
         * THESE DONT WORK CAUSE THE BOTTONS REPLACE THE TOP VALUES
         * UPDATE questions_options SET title = REPLACE(title, '5 000', '1 500') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '10 000', '3 000') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '20 000', '6 000') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '40 000', '12 000') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '2 500', '750') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '1 000', '300') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '2 000', '600') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '3 000', '900') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '4 000', '1 200') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '6 000', '1 800') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '7 000', '2 100') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '8 000', '2 500') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '9 000', '2 700') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '1 500', '450') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '150', '45') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '500', '150') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '400', '120') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '100', '30') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '200', '60') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '75', '22') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '76', '22') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '50', '15') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '51', '15') WHERE title LIKE '%sqm%';
UPDATE questions_options SET title = REPLACE(title, '25', '6') WHERE title LIKE '%sqm%';
*/


        //
        $csvFile = public_path('data/service_sheet_1.csv');

        $row = 1;
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            #Loop through each row
            while (($data = fgetcsv($handle, 0, ";", "\"")) !== FALSE) {

                $NEW_SERVICE = 0;


                #Clean Data
                foreach ($data AS $key => $item) {
                    $item = str_replace("Thumbtack", config('app.name'), $item);
                    if ($key >= 3) {
                        $data['questions'][] = trim($item);
                    } else {
                        $data[$key] = trim($item);
                    }
                }

                /**
                 * Categories:
                 */
                if ($data[0] == 'Event') {
                    $data[0] = 'Events';
                } elseif ($data[0] == 'Lesson') {
                    $data[0] = 'Lessons';
                }
                #Main
                if (!$top_cat_id = DB::table('categories')->select('id')->where('title',$data[0])->value('id')) {
                    $top_cat_id = DB::table('categories')
                        ->insertGetId(
                            [
                                'title' => $data[0],
                                'slug' => str_slug($data[0]),
                                'parent_id' => 0,
                                'level' => 1,
                            ]);
                }

                if (!$top_cat_id) {
                    echo 'NO TOP CAT FOUND';
                    exit;
                }

                #Sub Cat
                if (isset($data[1]) && $data[1]) {
                    if (!$sub_cat_id = DB::table('categories')->select('id')->where('title', $data[1])->value('id')) {
                        $sub_cat_id = DB::table('categories')
                            ->insertGetId(
                                [
                                    'title' => $data[1],
                                    'slug' => str_slug($data[1]),
                                    'parent_id' => $top_cat_id,
                                    'level' => 2,
                                ]);
                    }
                } else {
                    var_dump($data);
                    exit;
                }

                if (!$sub_cat_id) {
                    echo 'NO SUB CAT FOUND';
                    exit;
                }


                #Service
                if ($data[2]) {
                    if (!$service_id = DB::table('services')->select('id')->where('title', $data[2])->value('id')) {
                        $service_id = DB::table('services')
                            ->insertGetId(
                                [
                                    'title' => $data[2],
                                    'slug' => str_slug($data[2]),
                                    'status' => 1,
                                ]);

                        $NEW_SERVICE = 1;
                    }
                }

                if (!$service_id) {
                    echo 'NO SERVICE FOUND';
                    exit;
                }

                /**
                 * LINK SERVICE TO CATEGORY
                 */
                if (!DB::table('category_service')->select('category_id')->where('service_id', $service_id)->where('category_id', $sub_cat_id)->value('service_id')) {
                    DB::table('category_service')->insertGetId(['service_id' => $service_id,'category_id' => $sub_cat_id]);
                }

                /**
                 * IF THERE are already questions then lets skip.
                 */
                if (DB::table('question_service')->select('question_id')->where('service_id', $service_id)->value('question_id')) {
                    continue;
                }

                /**
                 * HANDLE questions
                 */
                foreach ($data['questions'] AS $q_key => $question_string) {
                    #clean double pipe
                    $question_string = str_replace("||","|",$question_string);

                    $q_parts = explode("|", $question_string);

                    if ($q_parts && $q_parts[0]) {

                        $q_parts[0] = trim($q_parts[0]);

                        /**
                         * SKIP THESE
                         */
                        #SKIP DEFAULT QUESTIONS
                        $end = 0;
                        # First one starts with ""
                        if (strpos($q_parts[0], 'When do you need ') !== false) {
                            #GET OCCUPATION:
                            $next_key = $q_key+1;
                            $end = 1;
                        } elseif (strpos($q_parts[0], 'Anything else the ') !== false) {
                            $next_key = $q_key;
                            $end = 1;
                        } elseif (strpos($q_parts[0], 'How would you like to meet with the ') !== false) {
                            $next_key = $q_key;
                            $end = 1;
                        } elseif (strpos($q_parts[0], 'Would you like to add photos ') !== false) {
                            continue;
                        }

                        /**
                         * END question capture (these are default questions)
                         */
                        if ($end) {

                            if (isset($data['questions'][$next_key]) && $data['questions'][$next_key]) {
                                if (preg_match('/Anything else the ([a-zA-Z\/ ]+) should know/i', $data['questions'][$next_key], $matches)) {
                                    if (isset($matches[1]) && $matches[1]) {
                                        $occupation = $matches[1];

                                        if (!$occupation_id = DB::table('occupations')->select('id')->where('title', $matches[1])->value('id')) {
                                            $occupation_id = DB::table('occupations')
                                                ->insertGetId(
                                                    [
                                                        'title' => $matches[1],
                                                        'slug' => str_slug($matches[1]),
                                                    ]);
                                        }

                                        #UPDATE service with occupation
                                        if ($occupation_id) {
                                            DB::table('services')
                                                ->where('id', $service_id)
                                                ->update(['occupation_id' => $occupation_id]);
                                        }
                                    }
                                }

                            }

                            ##END LOOP
                            break;
                        }

                        $question_type = 'checkbox';
                        $optional = 0;
                        /**
                         * Figure out QUESTION Type
                         */
                        if (strpos($q_parts[0], 'How many ') !== false) {
                            $question_type = 'radio';
                        } else if (strpos($q_parts[0], 'How would you like to ') !== false) {
                            $question_type = 'radio';
                        } else if (strpos($q_parts[0], 'How many ') !== false) {
                            $question_type = 'radio';
                        } else if (strpos($q_parts[0], 'What is your budget ') !== false) {
                            $question_type = 'radio';
                        } else if (strpos($q_parts[0], 'What type of ') !== false) {
                            $question_type = 'radio';
                        } else if (strpos($q_parts[0], 'Where are you in your hiring') !== false) {
                            $question_type = 'radio';
                        }

                        /**
                         * Figure OPtional status of question
                         */
                        if (strpos(strtolower(trim($q_parts[0])), 'optional') !== false) {
                            $optional = 1;
                        }

                        /**
                         * QUESTION ANSWERS array
                         */
                        $questions_options = array();
                        $question_special = '';
                        $question_answers =array();


                        if (isset($q_parts[1]) && $q_parts[1]) {


                            /*
                             * Replace commas inside brackets with or in
                             */
                            $q_parts[1] = preg_replace_callback('/\(.+?\)/', function($e) {
                                return str_replace(',', ' or ', $e[0]);
                            }, $q_parts[1]);


                            /**
                             * Feet to Metres
                             */
                            $q_parts[1] = preg_replace_callback('/([0-9]+) ? ?feet/', function($e) {
                                $new_string = $e[0];

                                if (isset($e[1]) && $e[1]) {
                                    $new_meter = floor($e[1]*0.3048);

                                    $new_string =  str_replace($e[1], $new_meter, $e[0]);

                                    $new_string =  str_replace('feet', 'metres', $new_string);
                                }

                                return $new_string;
                            }, $q_parts[1]);

                            /**
                             * Feet to Metres
                             */
                            /*$q_parts[1] = preg_replace_callback('/([0-9 -]+) ?sqm/', function($e) {
                                $new_string = $e[0];

                                if (isset($e[1]) && $e[1]) {
                                    $vals = explode("-", $e[1]);
                                    foreach ($vals AS $val) {
                                        $val_ol = str_replace(" ", "", $val);
                                        $new_val = intval($val_ol);
                                        $new_val = floor($new_val*0.3048);

                                        $new_string = str_replace($val,$new_val,  $new_string);
                                    }
                                }



                                return $new_string;
                            }, $q_parts[1]);*/


                            /**
                             * $$$$ to RRRR
                             */
                            $q_parts[1] = preg_replace_callback('/\$([0-9 ])+/', function($e) {
                                $new_string = $e[0];

                                if (isset($e[1]) && $e[1]) {
                                    $val = str_replace(" ","", $e[1]);

                                    $new_val = floatval($val);
                                    $new_val = round($new_val*0.076);

                                    $new_string = str_replace($e[1],$new_val,  $new_string);
                                }

                                return $new_string;
                            }, $q_parts[1]);

                            /**
                             * CHECK for price value as options - Replace with budget scale
                             * $ (basic), $$ (standard), $$$ (premium), $$$$ (high-end)
                             */

                            #$81 - $100 per person, $71 - $80 per person, $61 - $70 per
                            if (strpos($q_parts[0], ' budget') !== false) {

                                $question_type = 'radio';

                                $question_answers = ['$ (Entry)', '$$ (Standard)', '$$$ (Premium)', '$$$$ (High-end)'];
                            } else {
                                #Find semi colon ;
                                if (strpos($q_parts[1], ';') !== false) {
                                    $question_answers = explode(";", trim($q_parts[1]));
                                } else if(strpos($q_parts[1], ',') !== false) {
                                    $question_answers = explode(",", trim($q_parts[1]));
                                }
                            }



                            if ($question_answers) {
                                #clean empty values
                                $question_answers = array_filter($question_answers, function($value) { return $value !== ''; });

                                if (count($question_answers) == 1) {
                                    $question_type = 'textarea';
                                    continue;
                                }

                                #IF 3 or less answers options then lets say its radio
                                if (count($question_answers) <= 3) {
                                    $question_type = 'radio';
                                }

                                foreach ($question_answers AS $key => $question_answer){
                                    $question_answer = trim($question_answer);

                                    if (strtolower($question_answer) == 'optional') {
                                        unset($question_answers[$key]);
                                        continue;
                                    }

                                    if ($question_answer) {

                                        $question_special = '';
                                        if (strpos($question_answer, 'Other') !== false
                                            || strpos($question_answer, 'please describe') !== false
                                            || strpos($question_answer, 'please enter') !== false
                                            || strpos($question_answer, 'please list') !== false) {
                                            $question_special = 'show|textarea|Other|Tell us more';
                                            $question_answer = 'Other';
                                        }

                                        if (strpos($question_answer, 'Click to pick a date') !== false) {
                                            $question_special = 'show|datepicker|specific_date';
                                        }
                                        if (strpos($question_answer, 'Number of passengers') !== false) {
                                            $question_special = 'show|text|enter_number|Enter Number';
                                        }


                                        #5 miles, 15 miles, 25 miles, 50 miles
                                        /*$question_answer = str_replace(" miles,"," kms", $question_answer);*/

                                        $question_answer = str_replace(" US "," South Africa ", $question_answer);

                                        $questions_options[] = ['special' => $question_special, 'title' => trim($question_answer)];
                                    }
                                }

                                if (count($question_answers) <= 1) {
                                    $question_type = 'textarea';
                                    continue;
                                }
                            }
                        } else {
                            /**
                             * ANSWERS formating
                             */
                            #Means question is expecting text input as answer.
                            if (!isset($q_parts[1]) || !$q_parts[1]) {
                                $question_type = 'textarea';
                            }
                        }

                        ###INSERT QUESTION
                        $question_id = null;
                        $question_id = DB::table('questions')->select('id')->where('title', $q_parts[0])->value('id');
                        if ($questions_options && $question_id) {

                            if(!DB::table('questions_options')->select('id')->where('question_id', $question_id)->where('title', $questions_options[0]['title'])->value('id')) {
                                $question_id = null;
                            }
                        }

                        #INSERT QUESTION
                        if (!$question_id) {
                            $question_id = DB::table('questions')
                                ->insertGetId([
                                    'title' => $q_parts[0],
                                    'question_type' => $question_type,
                                    'optional' => 1,
                                ]);

                            /*
                             * ONLY IF NEW QUESTION
                             */
                            ##LINK question to service
                            if ($question_id && $service_id) {
                                if (!DB::table('question_service')->select('id')->where('question_id', $question_id)->where('service_id', $service_id)->value('id')) {
                                    DB::table('question_service')
                                        ->insertGetId([
                                            'question_id' => $question_id,
                                            'service_id' => $service_id,
                                        ]);
                                }
                            }

                        } else {
                            echo '-';
                        }

                        if ($question_id && $questions_options) {
                            foreach ($questions_options AS $key_option => $questions_option) {
                                $questions_options[$key_option]['question_id'] = $question_id;
                            }

                            ##DELETE ALL OPTIONS FIRST
                            DB::table('questions_options')->where('question_id', $question_id)->delete();

                            ###INSERT QUESTION OPTIONS
                            DB::table('questions_options')->insert($questions_options);
                        }


                        #POSSIBLE ANSWERS
                    } else {
                        echo "\n".$row." [".$question_string. "] - Not proper question\n";
                    }
                }

                $row++;

                echo '*';

            }
            fclose($handle);
        }

    }
}
