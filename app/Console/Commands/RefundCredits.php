<?php

namespace App\Console\Commands;

use App\Models\Quote\Quote;
use App\Repositories\Backend\Transaction\TransactionRepository;
use App\Services\Account\Billing;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RefundCredits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quotes:refund';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refund credits if not viewed in 48 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Find all quotes that have NOT been read and that created date is older than 48 hours (should be a approved_at date but dont have that yet)
        #Todo: add approved_at date to quotes

        $unrefunded_quotes = Quote::with(['transactions' => function ($query) {
            $query->where('refunded', 0);
        }])->where('read_at','null')->where('created_at', '<', Carbon::now()->subHours(config('app.settings.credit_refund_hours')));


        foreach ($unrefunded_quotes AS $unopened_quote) {

            foreach($unopened_quote->transactions AS $trans)
            {
                Billing::refundTransaction($trans->id);
            }

        }
    }
}
