<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Models\NotificationTracking\NotificationTracking;
use App\Models\Project\Project;
use App\Models\Quote\Quote;
use App\Notifications\Professional\NewProjectAlert;
use App\Services\Professionals\ProfessionalsService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemindProfessionalOfRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:remind_professionals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminds professionals of projects waiting to be quoted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Find all projects that have NOT been read and that created date is older than 48 hours

        $open_projects = Project::where('status',1)
            ->where('quote_count','<',config('app.settings.quote_limit'))->get();

        foreach ($open_projects AS $open_project) {

            /**
             * Get Quotes
             */
            $quotes = Quote::where('project_id', $open_project->id)->get()->toArray();
            $quotes_key = Helper::make_key_id_array($quotes, 'professional_id');

            $professional_branches = ProfessionalsService::BestForJob($open_project, true);
            $already = array();

            if ($professional_branches) {

                $answers = $open_project->getAnswers();

                foreach ($professional_branches AS $branch) {

                    #Skip if professional already quoted
                    if (isset($quotes_key[$branch->professional_id]))
                        continue;

                    /**
                     * Ensure Dates for reminders are right
                     * 1st = Upon project approve (initial alert)
                     * 2st = 1 day after initial created
                     * 3nd = 3 day after
                     */

                    // Get notifications already sent
                    $sent_notifs = NotificationTracking::where('entity_id', $open_project->id)->where('entity_type', 'project')
                        ->where('notifiable_type', 'professional_branch')->where('notifiable_id', $branch->id)
                        ->orderBy('id', 'desc')->get()->toArray();

                    /**
                     * Ensure atleast 1 notification was sent before triggering reminders
                     */
                    if ($sent_notifs) {

                        $sendReminder = false;

                        $dt = Carbon::now();

                        #Skip sending if not at set intervals
                        switch (count($sent_notifs)) {
                            case 1:
                                if ($sent_notifs[0]['created_at'] < $dt->subDays(1) && $sent_notifs[0]['created_at'] > $dt->subDays(2)) {
                                    $sendReminder = true;
                                }
                                break;
                            case 2:
                                if ($sent_notifs[0]['created_at'] < $dt->subDays(3) && $sent_notifs[0]['created_at'] > $dt->subDays(4)) {
                                    $sendReminder = true;
                                }
                                break;
                            case 3:

                                break;
                        }

                        /**
                         * GO ahead and send
                         */
                        if ($sendReminder) {

                            echo '*';

                            /**
                             * DO NOT send to same mail twice
                             */
                            if (!isset($already[$branch->notify_email])) {
                                $already[$branch->notify_email] = true;
                            } else {
                                continue;
                            }

                            #$branch->notify(new NewProjectAlert($open_project, $branch, $answers));
                        }

                    }
                }
            }

        }
    }
}
