<?php

namespace App\Console\Commands;

use App\Models\Access\User\User;
use App\Models\Quote\Quote;
use App\Notifications\Project\RequestClientReviewNotify;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class RequestClientReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:request_client_review';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a notification reminder to client at set intervals to review professional';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $hired_quotes = Quote::select('quotes.*', 'users.email')
            ->where('quotes.status', 3)
            ->join('users', 'quotes.user_id', '=', 'users.id')
            ->where('hired','>=', Carbon::now()->addDays(config('app.settings.first_notify_after_days')))
            ->where('hired','<', Carbon::now()->addDays(config('app.settings.first_notify_after_days')+1))->get();

        if ($hired_quotes->count())
            Notification::send($hired_quotes, new RequestClientReviewNotify($quotes));

        $second_hired_quotes = Quote::select('quotes.*', 'users.email')
            ->where('quotes.status', 3)
            ->join('users', 'quotes.user_id', '=', 'users.id')
            ->where('hired','>=', Carbon::now()->addDays(config('app.settings.second_notify_after_days')))
            ->where('hired','<', Carbon::now()->addDays(config('app.settings.second_notify_after_days')+1))->get();
        if ($hired_quotes->count())
            Notification::send($second_hired_quotes, new RequestClientReviewNotify($quotes));


        $third_hired_quotes = Quote::select('quotes.*', 'users.email')
            ->where('quotes.status', 3)
            ->join('users', 'quotes.user_id', '=', 'users.id')
            ->where('hired','>=', Carbon::now()->addDays(config('app.settings.third_notify_after_days')))
            ->where('hired','<', Carbon::now()->addDays(config('app.settings.third_notify_after_days')+1))->get();
        if ($third_hired_quotes->count())
            Notification::send($third_hired_quotes, new RequestClientReviewNotify($quotes));

    }
}
