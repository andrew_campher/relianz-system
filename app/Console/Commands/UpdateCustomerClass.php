<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCustomerClass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customers:update_customer_class';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set customer Classes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Pull All sales from invoicelineitems - Include the Downpacked adjustments for bulk items.
         * Exclude items that were creditted.
         */

        $customers = DB::connection('qbdb')
            ->select("SELECT c.ID, b3.Qty, Price, DemandValue FROM Customers c 
LEFT JOIN 
(SELECT il.CustomerId, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty, MAX(i.Price) Price, SUM((il.ItemQuantity-IFNULL(cl.ItemQuantity,0))*i.Price) DemandValue FROM InvoiceLineItems il 
  LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId
  LEFT JOIN Items i ON i.ID = il.ItemId 
 WHERE il.Date > date_sub(now(), interval 1 YEAR) GROUP BY il.CustomerId) b3 ON b3.CustomerId = c.ID
  WHERE b3.Qty > 0 ORDER BY DemandValue DESC");

        if($customers) {
            $totvalue = 0;

            foreach ($customers AS $customer) {

                $totvalue += $customer->DemandValue;

            }

            $addperc = 0;

            foreach ($customers AS $customer) {

                $demandperc = $customer->DemandValue/$totvalue;

                $addperc += $demandperc;

                if ($addperc >= 0.8) {
                    if ($addperc >= 0.95) {
                        $class = 'C';
                    } else {
                        $class = 'B';
                    }
                } else {
                    $class = 'A';
                }

                //Update the sales history table
                $updatePriceSQL = "INSERT INTO customers_data (CustomersId, Class) 
VALUES ('" . $customer->ID . "', '" . $class . "') ON DUPLICATE KEY UPDATE Class = '" . $class . "'";

                DB::connection('qbdb')->insert($updatePriceSQL);
            }
        }



    }
}
