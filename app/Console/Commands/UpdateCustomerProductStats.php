<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCustomerProductStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:update_customer_sales_stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all the sales quantities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $dateinterval = "12 MONTH";

        $items = DB::connection('qbdb')
            ->select("SELECT il.CustomerId, MAX(il.ItemId) AS ItemId, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty, DATE_FORMAT(MAX(il.Date), '%Y%m') AS Month, MAX(il.Itemrate) AS MaxPrice FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
LEFT JOIN Items i ON i.ID = il.ItemId 
WHERE i.Type = 'Inventory' AND il.ItemQuantity-IFNULL(cl.ItemQuantity,0) > 0 AND il.Date > DATE_FORMAT(DATE_SUB(NOW(),INTERVAL {$dateinterval}),'%Y-%m-01 00:00:00') 
GROUP BY il.CustomerId, CONCAT(YEAR(il.Date),MONTH(il.Date)), il.ItemId");

        if($items) {
            foreach ($items AS $item) {
                //Update the sales history table
                if ($item->Qty > 0) {
                    $updatePriceSQL = "INSERT INTO item_customer_sales_history (CustomerId, ItemId, Monthdate, Qty, MaxPrice) 
VALUES ('" . $item->CustomerId . "', '" . $item->ItemId . "', '" . $item->Month . "', '" . $item->Qty . "', '" . $item->MaxPrice . "') 
  ON DUPLICATE KEY UPDATE Qty = '" . $item->Qty . "', MaxPrice = '" . $item->MaxPrice . "'";
                }


                DB::connection('qbdb')->insert($updatePriceSQL);
            }
        }



    }
}
