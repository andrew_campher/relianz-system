<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateItemClass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:update_item_class';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set item Classes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Pull All sales from invoicelineitems - Include the Downpacked adjustments for bulk items.
         * Exclude items that were creditted.
         */

        $items = DB::connection('qbdb')
            ->select("SELECT i.ID, b3.Qty, i.PurchaseCost, b3.Qty*i.PurchaseCost AS DemandValue FROM Items i 
LEFT JOIN 
(SELECT il.ItemId, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId
 WHERE il.Date > date_sub(now(), interval 1 YEAR) GROUP BY il.ItemId) b3 ON b3.ItemId = i.ID
  WHERE b3.Qty > 0 ORDER BY DemandValue DESC");

        if($items) {

            //RESET all marks
            $query = "UPDATE items_data SET Class = NULL";
            DB::connection('qbdb')->update($query);

            $totvalue = 0;

            foreach ($items AS $item) {

                $totvalue += $item->DemandValue;

            }

            $addperc = 0;

            foreach ($items AS $item) {

                $demandperc = $item->DemandValue/$totvalue;

                $addperc += $demandperc;

                if ($addperc >= 0.8) {
                    if ($addperc >= 0.95) {
                        $class = 'C';
                    } else {
                        $class = 'B';
                    }
                } else {
                    $class = 'A';
                }

                //Update the sales history table
                $updatePriceSQL = "INSERT INTO items_data (ItemId, Class) 
VALUES ('" . $item->ID . "', '" . $class . "') ON DUPLICATE KEY UPDATE Class = '" . $class . "'";

                DB::connection('qbdb')->insert($updatePriceSQL);
            }
        }



    }
}
