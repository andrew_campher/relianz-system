<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateItemProClass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:update_item_pro_class';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Procurement item Classes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Pull All sales from invoicelineitems - Include the Downpacked adjustments for bulk items.
         * Exclude items that were creditted.
         */

        $items = DB::connection('qbdb')
            ->select("SELECT ItemId, SUM(Qty) AS Qty, i.PurchaseCost, SUM(Qty)*i.PurchaseCost AS DemandValue FROM ( 
    (SELECT ItemId, ABS(SUM(ir.ItemQuantity)) AS Qty FROM ItemReceiptLineItems ir LEFT JOIN Items i ON i.ID = ir.ItemId 
    WHERE i.Type = 'Inventory' AND ir.ItemQuantity < 0 AND ir.ItemQuantity > -5000 AND ir.Date > date_sub(now(), interval 1 YEAR) GROUP BY ItemId 
) UNION ALL ( 
    SELECT il.ItemId, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il 
    LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId
WHERE il.ItemQuantity > 0 AND il.Date > date_sub(now(), interval 1 YEAR) 
GROUP BY il.ItemId ) 
    ) t1
    LEFT JOIN Items i ON i.ID = t1.ItemId
    WHERE i.Name NOT LIKE '% CP'
    GROUP BY ItemId ORDER BY DemandValue DESC");

        if($items) {

            //RESET all marks
            $query = "UPDATE items_data SET Pro_Class = NULL";
            DB::connection('qbdb')->update($query);

            $totvalue = 0;

            foreach ($items AS $item) {

                $totvalue += $item->DemandValue;

            }

            $addperc = 0;

            foreach ($items AS $item) {

                $demandperc = $item->DemandValue/$totvalue;

                $addperc += $demandperc;

                if ($addperc >= 0.8) {
                    if ($addperc >= 0.95) {
                        $class = 'C';
                    } else {
                        $class = 'B';
                    }
                } else {
                    $class = 'A';
                }

                //Update the sales history table
                $updatePriceSQL = "INSERT INTO items_data (ItemId, Pro_Class) 
VALUES ('" . $item->ItemId . "', '" . $class . "') ON DUPLICATE KEY UPDATE Pro_Class = '" . $class . "'";

                DB::connection('qbdb')->insert($updatePriceSQL);
            }
        }



    }
}
