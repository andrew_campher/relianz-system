<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateProductStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:update_sales_stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all the sales quantities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Pull All sales from invoicelineitems - Include the Downpacked adjustments for bulk items.
         * Exclude items that were creditted.
         */

        $dateinterval = "12 MONTH";

        $items = DB::connection('qbdb')
            ->select("SELECT MAX(il.ItemId) AS ItemId, i.Name, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty, DATE_FORMAT(il.Date, '%Y%m') AS Month, ((il.ItemQuantity-IFNULL(cl.ItemQuantity,0))*i.AverageCost) AS TotUnitCost, ((il.ItemQuantity-IFNULL(cl.ItemQuantity,0))*il.ItemRate) AS Amount 
FROM InvoiceLineItems il 
LEFT JOIN Items i ON i.ID = il.ItemId 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE i.Type = 'Inventory' AND il.ItemQuantity-IFNULL(cl.ItemQuantity,0) > 0 
GROUP BY CONCAT(YEAR(il.Date),MONTH(il.Date)), il.ItemId");

        if($items) {
            foreach ($items AS $item) {
                //Update the sales history table
                if ($item->Qty > 0) {
                    $updatePriceSQL = "INSERT INTO item_sales_history (ItemId, Monthdate, Qty, Amount, StockName, TotUnitCost) 
VALUES ('" . $item->ItemId . "', '" . $item->Month . "', '" . $item->Qty . "', '" . $item->Amount . "', ?, '" . $item->TotUnitCost . "') 
  ON DUPLICATE KEY UPDATE Qty = '" . $item->Qty . "', Monthdate = '" . $item->Month . "', Amount = '" . $item->Amount . "', StockName = ?, TotUnitCost = '" . $item->TotUnitCost . "'";
                }

                DB::connection('qbdb')->insert($updatePriceSQL,[$item->Name, $item->Name]);
            }
        }



    }
}
