<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateReOrderPoint extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:update_reorder_point';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set item ReOrderPoint';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Pull All sales from invoicelineitems - Include the Downpacked adjustments for bulk items.
         * Exclude items that were creditted.
         */

        $items = DB::connection('qbdb')
            ->select("SELECT ItemId, SUM(Qty) AS SumQtyItems, AVG(Qty) AvgQty, MAX(Qty) AS MaxQty, MAX(BiggestOrder) AS BiggestOrder FROM ( 
    (SELECT MAX(i.ID) AS ItemId, ABS(SUM(ir.ItemQuantity)) AS Qty, MAX(ir.ItemQuantity) AS BiggestOrder FROM ItemReceiptLineItems ir LEFT JOIN Items i ON i.ID = ir.ItemId 
    WHERE i.Type = 'Inventory' AND ir.ItemQuantity < 0 AND ir.ItemQuantity > -5000 AND ir.Date >= DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 13 MONTH), '%Y-%m-01') AND ir.Date < LAST_DAY(DATE_SUB(NOW(),INTERVAL 9 MONTH)) GROUP BY MONTH(ir.Date), ir.ItemId ORDER BY MAX(FullName) ASC 
) UNION ALL ( 
    SELECT MAX(i.ID) AS ItemId, SUM(il.ItemQuantity) AS Qty, MAX(il.ItemQuantity) AS BiggestOrder FROM InvoiceLineItems il 
LEFT JOIN Items i ON i.ID = il.ItemId 
WHERE i.Type = 'Inventory' AND il.ItemQuantity > 0 AND  il.Date >= DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 13 MONTH), '%Y-%m-01') AND il.Date < LAST_DAY(DATE_SUB(NOW(),INTERVAL 9 MONTH)) 
GROUP BY MONTH(il.Date), il.ItemId ORDER BY MAX(FullName) ASC ) 
    ) t1 GROUP BY ItemId");


        $leadtimesRaw = DB::connection('qbdb')
        ->select("SELECT  MAX(i.ID) ItemId, MAX(pl.VendorId) VendorId, CASE WHEN MAX(datediff(pt2.FirstTrans,pl.Date)) <= 1 THEN 2 ELSE MAX(datediff(pt2.FirstTrans,pl.Date)) END AS MaxLeadDays, MIN(datediff(pt2.FirstTrans,pl.Date)) MinLeadDays, AVG(datediff(pt2.FirstTrans,pl.Date)) AvgLeadDays FROM PurchaseOrderLineItems pl 
INNER JOIN PurchaseOrderLinkedTransactions pt 
        ON pt.PurchaseOrderId = pl.PurchaseOrderID 
INNER JOIN ( 
        SELECT PurchaseOrderID, MIN(TransactionDate) FirstTrans 
        FROM PurchaseOrderLinkedTransactions 
        GROUP BY PurchaseOrderId 
    ) pt2 ON pt2.PurchaseOrderId = pt.PurchaseOrderId AND 
            pt.TransactionDate = pt2.FirstTrans 
LEFT JOIN Items i ON i.ID = pl.ItemId 
WHERE pl.VendorName = i.PreferredVendor AND datediff(pt2.FirstTrans,pl.Date) > 1 AND pl.ItemQuantity > 0 AND pl.ItemId != '' AND TransactionType = 'Bill' AND pl.Date > date_sub(NOW(), INTERVAL 1 YEAR) GROUP BY pl.ItemID, pl.VendorId");

        $leadtimes = array();

        foreach ($leadtimesRaw AS $leadtimeRaw) {
            $leadtimes[$leadtimeRaw->ItemId] = $leadtimeRaw;
        }

        // GET CONSECUTIVE months purchases
        $itempasts = DB::connection('qbdb')
            ->select("SELECT is1.ItemId, i.Name, JSON_UNQUOTE(CustomFields->'$.Unit') Unit, is1.Qty past1, is2.Qty past2, is3.Qty past3, is4.Qty past4 FROM item_sales_history is1 
LEFT JOIN Items i ON i.ID = is1.ItemId 
 LEFT JOIN item_sales_history is2 ON is1.ItemId = is2.ItemId AND is2.Monthdate = ".date('Ym', strtotime('-2 Month'))." 
 LEFT JOIN item_sales_history is3 ON is1.ItemId = is3.ItemId AND is3.Monthdate = ".date('Ym', strtotime('-3 Month'))." 
 LEFT JOIN item_sales_history is4 ON is1.ItemId = is4.ItemId AND is4.Monthdate = ".date('Ym', strtotime('-4 Month'))."
 WHERE is1.Monthdate = ".date('Ym', strtotime('-1 Month'))." ");

        $qtypast = array();

        if($itempasts) {
            foreach ($itempasts AS $itempast) {
                $qtypast[$itempast->ItemId] = [$itempast->past1, $itempast->past2, $itempast->past3, $itempast->past4];
            }
        }

        if($items) {

            foreach ($items AS $item) {

                if(!isset($leadtimes[$item->ItemId])) {
                    echo '-';
                    continue;
                }

                $ReOrderPoint = 0;

                /**
                 * SAFETY STOCK
                 * ROUNDUP(((MaxQty/30)*((AvgLead+MaxLead)/2))-((AvgQty/30)*AvgLead))
                 */
                $safetystock = ceil((($item->MaxQty/30)*(($leadtimes[$item->ItemId]->AvgLeadDays+$leadtimes[$item->ItemId]->MaxLeadDays)/2))-(($item->AvgQty/30)*$leadtimes[$item->ItemId]->AvgLeadDays));

                /**
                 * LEAD time stock
                 * ROUNDUP(AvgLead*(AvgQty/30))
                 */
                $leadstock = ceil($leadtimes[$item->ItemId]->AvgLeadDays*($item->AvgQty/30));

                /**
                 * ReOrderPoint Final Calculations
                 * IF(ManualPoint>LeadTime+SafetyStock,ManualPoint,
                 * IF(AND(LeadTime+SafetyStock<MinReorderPoint,AvgQty>MinReorderPoint),MinReorderPoint,
                 * IF(OR(TimesPurchasedInPast4>2,Past2Months=2,TotalMonthsSold>7,DownPackedQtysold>2),IF(LeadTime+SafetyStock>AvgQty,ROUNDUP(LeadTime+SafetyStock),ROUNDUP(AvgQty)),)))
                 */
                $ReOrderPoint = $safetystock+$leadstock;

                if ($ReOrderPoint < $item->BiggestOrder) {
                    $ReOrderPoint = $item->BiggestOrder;
                } elseif(count($qtypast[$item->ItemId])>2 ||
                    ($qtypast[$item->ItemId][0]>0 && $qtypast[$item->ItemId][1]>0) ||
                    DownPackedQtysold>2) {

                } else {

                }

                //Update the sales history table
                $updatePriceSQL = "INSERT INTO items_data (ItemId, ReOrderPoint) 
VALUES ('" . $item->ItemId . "', '" . $ReOrderPoint . "') ON DUPLICATE KEY UPDATE ReOrderPoint = '" . $ReOrderPoint . "'";

                DB::connection('qbdb')->insert($updatePriceSQL);
            }
        }



    }
}
