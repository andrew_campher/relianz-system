<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateSupplierClass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vendors:update_class';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set supplier Classes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Pull All sales from invoicelineitems - Include the Downpacked adjustments for bulk items.
         * Exclude items that were creditted.
         */

        $suppliers = DB::connection('qbdb')
            ->select("SELECT v.ID, DemandValue FROM Vendors v 
LEFT JOIN 
(SELECT i.PreferredVendorId, SUM((il.ItemQuantity-IFNULL(cl.ItemQuantity,0))*i.PurchaseCost) AS DemandValue, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il
  LEFT JOIN Items i ON i.ID = il.ItemId
 LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId
 WHERE il.Date > date_sub(now(), interval 1 YEAR) GROUP BY i.PreferredVendorId) b3 ON b3.PreferredVendorId = v.ID
  WHERE b3.Qty > 0 ORDER BY DemandValue DESC");

        if($suppliers) {
            $totvalue = 0;

            foreach ($suppliers AS $supplier) {

                $totvalue += $supplier->DemandValue;

            }

            $addperc = 0;

            foreach ($suppliers AS $supplier) {

                $demandperc = $supplier->DemandValue/$totvalue;

                $addperc += $demandperc;

                if ($addperc >= 0.8) {
                    if ($addperc >= 0.95) {
                        $class = 'C';
                    } else {
                        $class = 'B';
                    }
                } else {
                    $class = 'A';
                }

                //Update the sales history table
                $updatePriceSQL = "INSERT INTO vendors_data (VendorId, Class) 
VALUES ('" . $supplier->ID . "', '" . $class . "') ON DUPLICATE KEY UPDATE Class = '" . $class . "'";

                DB::connection('qbdb')->insert($updatePriceSQL);
            }
        }



    }
}
