<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateYearProductStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:update_year_sales_stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates 1 year back sales quantities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $items = DB::connection('qbdb')
            ->select("SELECT ItemId, SUM(Qty) AS QtyItems, Month FROM ( 
    (SELECT MAX(ir.ItemId) AS ItemId, ABS(SUM(ir.ItemQuantity)) AS Qty, DATE_FORMAT(ir.Date, '%Y%m') AS Month FROM ItemReceiptLineItems ir LEFT JOIN Items i ON i.ID = ir.ItemId 
    WHERE i.Type = 'Inventory' AND ir.ItemQuantity < 0 AND ir.ItemQuantity > -5000  AND ir.TimeCreated > DATE_SUB(NOW(),INTERVAL 1 YEAR) GROUP BY CONCAT(YEAR(ir.Date),MONTH(ir.Date)), ItemId 
) UNION ALL ( 
    SELECT MAX(il.ItemId) AS ItemId, SUM(il.ItemQuantity) AS Qty, DATE_FORMAT(il.Date, '%Y%m') AS Month FROM InvoiceLineItems il 
LEFT JOIN Items i ON i.ID = il.ItemId 
WHERE i.Type = 'Inventory' AND il.ItemQuantity > 0 AND il.TimeCreated > DATE_SUB(NOW(),INTERVAL 1 YEAR)
GROUP BY CONCAT(YEAR(il.Date),MONTH(il.Date)), ItemId ORDER BY MAX(FullName) ASC ) 
    ) t1 GROUP BY Month, ItemId");

        if($items) {
            foreach ($items AS $item) {
                //Update the sales history table
                $updatePriceSQL = "INSERT INTO item_sales_history (ItemId, Monthdate, Qty) 
VALUES ('" . $item->ItemId . "', '" . $item->Month . "', '" . $item->QtyItems . "') 
  ON DUPLICATE KEY UPDATE Qty = '" . $item->QtyItems . "', Monthdate = '" . $item->Month . "'";

                DB::connection('qbdb')->insert($updatePriceSQL);
            }
        }



    }
}
