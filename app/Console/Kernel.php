<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\UpdateProductStats',
        'App\Console\Commands\UpdateYearProductStats',
        'App\Console\Commands\UpdateCustomerProductStats',
        'App\Console\Commands\UpdateItemClass',
        'App\Console\Commands\UpdateItemProClass',
        'App\Console\Commands\UpdateReOrderPoint',
        'App\Console\Commands\UpdateCustomerClass',
        'App\Console\Commands\UpdateSupplierClass',
        'App\Console\Commands\FixBlankCustomer',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('items:update_sales_stats')->daily();

        $schedule->command('items:update_customer_sales_stats')->daily();

        $schedule->command('items:update_item_pro_class')->daily();

        $schedule->command('items:update_year_sales_stats')->monthly();

        $schedule->command('customers:update_customer_class')->monthly();

        $schedule->command('items:update_item_class')->monthly();

        $schedule->command('vendors:update_class')->monthly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
