<?php

namespace App\Events\Common\ClientReview;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class ClientReviewCreated
 * @package App\Events\Common\ClientReview
 */
class ClientReviewCreated extends Event
{
	use SerializesModels;

	/**
	 * @var $project
	 */
	public $client_review;

	/**
	 * @param $project
	 */
	public function __construct($client_review)
	{
		$this->client_review = $client_review;
	}
}