<?php

namespace App\Events\Common\Comment;

use App\Events\Event;
use App\Models\Access\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CommentCreated extends Event implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $comment;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($comment)
    {
        //
        $this->comment = $comment;
        $this->user = $comment->user;
        $this->dontBroadcastToCurrentUser();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat_room.'.$this->comment->comments_room_id);

    }


}
