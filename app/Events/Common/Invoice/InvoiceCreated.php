<?php

namespace App\Events\Common\Invoice;

use App\Events\Event;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InvoiceCreated extends Event
{
    use InteractsWithSockets, SerializesModels;

    public $invoice;

    /**
     * InvoiceCreated constructor.
     * @param $invoice
     */
    public function __construct($invoice)
    {
        //
        $this->invoice = $invoice;
    }


}
