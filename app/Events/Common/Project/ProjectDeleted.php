<?php

namespace App\Events\Common\Project;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class ProjectDeleted
 * @package App\Events\Common\Project
 */
class ProjectDeleted extends Event
{
	use SerializesModels;

	/**
	 * @var $project
	 */
	public $project;

	/**
	 * @param $project
	 */
	public function __construct($project)
	{
		$this->project = $project;
	}
}