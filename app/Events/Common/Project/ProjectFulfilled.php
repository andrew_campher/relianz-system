<?php

namespace App\Events\Common\Project;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Queue\SerializesModels;

/**
 * Class ProjectCreated
 * @package App\Events\Common\Project
 */
class ProjectFulfilled extends Event implements ShouldBroadcast
{
	use SerializesModels, InteractsWithSockets;

	/**
	 * @var $project
	 */
	public $project;

    public $data;

	/**
	 * @param $project
	 */
	public function __construct($project)
	{
		$this->project = $project;
	}

    public function broadcastOn()
    {
        return ['projects'];
    }
}