<?php

namespace App\Events\Common\Project;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class ProjectCreated
 * @package App\Events\Common\Project
 */
class ProjectRelist extends Event
{
	use SerializesModels;

	/**
	 * @var $project
	 */
	public $project;

	/**
	 * @param $project
	 */
	public function __construct($project)
	{
		$this->project = $project;
	}
}