<?php

namespace App\Events\Common\ProjectReview;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class ProjectReviewPublicCreated
 * @package App\Events\Common\ProjectReview
 */
class ProjectReviewPublicCreated extends Event
{
	use SerializesModels;

	/**
	 * @var $project
	 */
	public $project_review;

	/**
	 * @param $project
	 */
	public function __construct($project_review)
	{
		$this->project_review = $project_review;
	}
}