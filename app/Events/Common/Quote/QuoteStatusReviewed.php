<?php

namespace App\Events\Common\Quote;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class QuoteCreated
 * @package App\Events\Common\Quote
 */
class QuoteStatusReviewed extends Event
{
	use SerializesModels;

	/**
	 * @var $quote
	 */
	public $quote;

	/**
	 * @param $quote
	 */
	public function __construct($quote)
	{
		$this->quote = $quote;
	}
}