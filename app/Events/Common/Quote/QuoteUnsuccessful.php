<?php

namespace App\Events\Common\Quote;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class QuoteCreated
 * @package App\Events\Common\Quote
 */
class QuoteUnsuccessful extends Event
{
	use SerializesModels;

	/**
	 * @var $quote
	 */
	public $quote;

    /**
     * QuoteUnsuccessful constructor.
     * @param $quote
     * @param array $data
     */
	public function __construct($quote, $data = array())
	{
		$this->quote = $quote;

        if (isset($data['decline_reason']) && $data['decline_reason']) {
            $this->quote->decline_reason = $data['decline_reason'];
        } elseif (isset($data['decline_custom']) && $data['decline_custom']) {
            $this->quote->decline_reason = $data['decline_custom'];
        } elseif (isset($data['close_reason']) && $data['close_reason']) {
            $this->quote->decline_reason = $data['close_reason'];
        }

	}
}