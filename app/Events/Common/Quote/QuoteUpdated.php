<?php

namespace App\Events\Common\Quote;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class QuoteUpdated
 * @package App\Events\Common\Quote
 */
class QuoteUpdated extends Event
{
	use SerializesModels;

	/**
	 * @var $quote
	 */
	public $quote;

	/**
	 * @param $quote
	 */
	public function __construct($quote)
	{
		$this->quote = $quote;
	}
}