<?php

namespace App\Events\Common\Transaction;

use App\Events\Event;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TransactionRefunded extends Event
{
    use InteractsWithSockets, SerializesModels;

    public $transaction;

    /**
     * TransactionCreated constructor.
     * @param $transaction
     */
    public function __construct($transaction)
    {
        //
        $this->transaction = $transaction;
    }


}
