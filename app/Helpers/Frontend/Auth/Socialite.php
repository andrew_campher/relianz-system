<?php

namespace App\Helpers\Frontend\Auth;

/**
 * Class Socialite
 * @package App\Helpers\Frontend\Auth
 */
class Socialite {

	/**
	 * Generates social login links based on what is enabled
	 *
	 * @return string
	 */
	public function getSocialLinks()
	{
		$socialite_enable = [];
		$socialite_links  = '';

		if (strlen(getenv('FACEBOOK_CLIENT_ID'))) {
			$socialite_enable[] = '<a class="btn btn-facebook" onclick="SocialLogin(\''.route('frontend.auth.social.login', ['facebook']).'\')" href="javascript:void(0)"><i class="fa fa-facebook"></i> '.trans('labels.frontend.auth.login_with', ['social_media' => 'Facebook']).'</a>';
		}

		if (strlen(getenv('GOOGLE_CLIENT_ID'))) {
            $socialite_enable[] = '<a class="btn btn-google" onclick="SocialLogin(\''.route('frontend.auth.social.login', ['google']).'\')" href="javascript:void(0)"><i class="fa fa-google"></i> '.trans('labels.frontend.auth.login_with', ['social_media' => 'Google']).'</a>';
		}

		#TODO: fix these
        if (strlen(getenv('LINKEDIN_CLIENT_ID'))) {
            $socialite_enable[] = link_to_route('frontend.auth.social.login', trans('labels.frontend.auth.login_with', ['social_media' => 'Linked In']), 'linkedin');
        }

        if (strlen(getenv('TWITTER_CLIENT_ID'))) {
            $socialite_enable[] = link_to_route('frontend.auth.social.login', trans('labels.frontend.auth.login_with', ['social_media' => 'Twitter']), 'twitter');
        }

        if (strlen(getenv('BITBUCKET_CLIENT_ID'))) {
            $socialite_enable[] = link_to_route('frontend.auth.social.login', trans('labels.frontend.auth.login_with', ['social_media' => 'Bit Bucket']), 'bitbucket');
        }

		if (strlen(getenv('GITHUB_CLIENT_ID'))) {
			$socialite_enable[] = link_to_route('frontend.auth.social.login', trans('labels.frontend.auth.login_with', ['social_media' => 'Github']), 'github');
		}

		for ($i = 0; $i < count($socialite_enable); $i++) {
			$socialite_links .= ($socialite_links != '' ? '&nbsp;|&nbsp;' : '') . $socialite_enable[$i];
		}

		return $socialite_links;
	}

	/**
	 * List of the accepted third party provider types to login with
	 *
	 * @return array
	 */
	public function getAcceptedProviders() {
		return [
			'facebook',
			'google',
		];
	}
}