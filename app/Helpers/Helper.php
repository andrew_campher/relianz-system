<?php

namespace App\Helpers;

class Helper
{

    public static function getKGUnit($unit, $weight = 0) {
        $kgunit = '';

        if (preg_match("/([0-9]+)x([0-9]+)x/i", $unit, $matches)) {
            $kgunit = $matches[1][0]*$matches[2][0];
        } elseif (preg_match("/([0-9]+)x/i", $unit, $matches)) {
            $kgunit = $matches[1];
        } elseif (preg_match("/([0-9]+)L/i", $unit, $matches)) {
            $kgunit = 1;
        } elseif (preg_match("/([0-9.]+)kg/i", $unit, $matches)) {
            $kgunit = $matches[1];
        } elseif ($weight > 0) {
            $kgunit = $weight;
        }

        return $kgunit;
    }

    public static function OrderStockCheck($order) {

        if(date("d-m-Y") < date("d-m-Y", strtotime($order->ShipDate))) {
            return '';
        }

        $no_stock = 0;
        $no_stock_bad = 0;
        $itemchecks = explode(",", $order->StockCheck);
        foreach ($itemchecks AS $itemcheck) {
            if((float)$itemcheck < 0) {
                $no_stock++;
            }
        }

        //If All items on order are no stocks Then set NO stock
        if(count($itemchecks) == $no_stock) {
            $no_stock_bad = 1;
        }

        //If it has been invoiced
        if($order->InvoiceNo) {
            if($no_stock) {
                return '<span class="badge text-xs label-danger" title="Invoiced more stock than on system - INVESTIGATE"><i class="fa fa-question-circle"></i> STOCK</span>';
            }
        } else {
            if($no_stock_bad) {
                return '<span class="text-danger" title="Not Enough Stock on System for all items in Order"><i class="fa fa-info-circle"></i></span>';
            } elseif($no_stock) {
                return '<span class="text-orange" title="Possible Stock Shortage detected"><i class="fa fa-info-circle"></i></span>';
            }
        }
    }

    public static function genColorCodeFromText($text,$min_brightness=100,$spec=6)
    {
        // Check inputs
        if(!is_int($min_brightness)) throw new Exception("$min_brightness is not an integer");
        if(!is_int($spec)) throw new Exception("$spec is not an integer");
        if($spec < 2 or $spec > 10) throw new Exception("$spec is out of range");
        if($min_brightness < 0 or $min_brightness > 255) throw new Exception("$min_brightness is out of range");


        $hash = md5($text);  //Gen hash of text
        $colors = array();
        for($i=0;$i<3;$i++)
            $colors[$i] = max(array(round(((hexdec(substr($hash,$spec*$i,$spec)))/hexdec(str_pad('',$spec,'F')))*255),$min_brightness)); //convert hash into 3 decimal values between 0 and 255

        if($min_brightness > 0)  //only check brightness requirements if min_brightness is about 100
            while( array_sum($colors)/3 < $min_brightness )  //loop until brightness is above or equal to min_brightness
                for($i=0;$i<3;$i++)
                    $colors[$i] += 10;	//increase each color by 10

        $output = '';

        for($i=0;$i<3;$i++)
            $output .= str_pad(dechex($colors[$i]),2,0,STR_PAD_LEFT);  //convert each color to hex and append to output

        return '#'.$output;
    }


    public static function make_tree_array($arrays, $parent_key)
    {
        if (!is_array($arrays) && !is_object($arrays))
            return false;

        $new_arr = array();
        #Build up parent tree array
        if (is_object($arrays)) {
            foreach ($arrays AS $array) {
                $new_arr[$array->$parent_key][] = $array;
            }
        } else {
            foreach ($arrays AS $array) {
                $new_arr[$array[$parent_key]][] = $array;
            }
        }

        return $new_arr;
    }

    /**
     * Returns the array with the id as its key
     * @param $arrays
     * @param $key
     * @return array|bool
     */
    public static function make_key_id_array($arrays, $key) {
        if (!is_array($arrays) && !is_object($arrays))
            return false;

        $new_arr = array();
        #Build up parent tree array
        if (is_object($arrays)) {
            foreach ($arrays AS $array) {
                $new_arr[$array->$key] = $array;
            }
        } else {
            foreach ($arrays AS $array) {
                $new_arr[$array[$key]] = $array;
            }
        }



        return $new_arr;
    }


        public static function distance($lat1, $lon1, $lat2, $lon2, $unit ="K") {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * Format price with currency
     * @return string
     */
    public static function formatPrice($price, $html=true) {
        $price = number_format($price, 2);
        $price_split = explode(".", $price);

        $price_formatted = config('app.currency.symbol').$price_split[0].'<span>.'.$price_split[1].'</span>';
        if (!$html) {
            $price_formatted = strip_tags($price_formatted);
        }
        return $price_formatted;
    }

    public static function showStars($rating, $outof = 5) {

        $rating = round($rating,1);

        $html = '<div class="br-wrapper br-theme-fontawesome-stars-o"><div class="br-widget br-readonly">';
        for ($i = 1; $i <= $outof; $i++) {

            $is_decimal = 0;
            $remain = $rating - ($i-1);
            if ($remain > 0 && $remain < 1) {
                #It is decimal
                $is_decimal = 1;
            }


            $html .= '<a rel="nofollow" href="javascript:void(0)" data-rating-value="'.$i.'" data-rating-text="'.$i.'" class="'.($is_decimal?'br-fractional':'').($i <= $rating?'br-selected':'').'"></a>';
        }
        $html .= '</div></div>';

        return $html;

    }

    public static function helpTip($title, $class = '') {
        return '<i data-toggle="popover" data-trigger="hover" title="'.$title.'" class="fa fa-question-circle text-primary fa-lg '.$class.'"></i>';
    }

    /**
     * TODO: does not work well with brackets and other characters
     * CREATES acronym
     * @param $string
     * @param bool $onlyCapitals
     * @return null|string
     */
    public static function createAcronym($string, $onlyCapitals = false) {
        $output = null;
        $string = strtoupper($string);
        $string = str_replace(" AND ", " ", $string);
        $token  = strtok($string, ' ');


        while ($token !== false) {
            $character = mb_substr($token, 0, 1);

            #skip characters
            $skip_chars = ["&", "-", "+", "$", ")", "("];
            if (in_array($character, $skip_chars)) {
                $token = strtok(' ');
                continue;
            }

            if ($onlyCapitals and mb_strtoupper($character) !== $character) {
                $token = strtok(' ');
                continue;
            }
            $output .= $character;
            $token = strtok(' ');
        }

        return substr($output, 0, 5);
    }

    /**
     * ENSURE package is preLoadded
     * @param $account
     * @return array
     */
    public static function CreditPackages($package) {

        $purchase_options = array(
            3 => [
                'credit_quantity' => 3,
                'discount' => 0,
                'active' => 0,
                'class' => '',
                'per_credit' => $package->per_credit,
                'amount' => $package->per_credit*3,
                'description' => config('app.name').' - 3 Credits purchase @'.self::formatPrice($package->per_credit, false).'/credit',
            ],
            12 => [
                'credit_quantity' => 12,
                'discount' => 10,
                'active' => 1,
                'class' => 'price-success',
                'per_credit' => $disc_per = $package->per_credit-($package->per_credit*0.1),
                'amount' => $disc_per*12,
                'description' => config('app.name').' - 12 Credits purchase @'.self::formatPrice($package->per_credit, false).'/credit',
            ],
            24 =>[
                'credit_quantity' => 24,
                'discount' => 12,
                'active' => 0,
                'class' => 'price-warning',
                'per_credit' => $disc_per = $package->per_credit-($package->per_credit*0.12),
                'amount' => $disc_per*24,
                'description' => config('app.name').' - 24 Credits purchase @'.self::formatPrice($package->per_credit, false).'/credit',
            ],
        );

        return $purchase_options;
    }

    public static function trackHit() {
        if (stristr($_SERVER["HTTP_USER_AGENT"], "Googlebot") === true) {
            return false;
        } elseif (stristr($_SERVER["HTTP_USER_AGENT"], "crawl") === true) {
            return false;
        } elseif (stristr($_SERVER["HTTP_USER_AGENT"], "spider") === true) {
            return false;
        } elseif (stristr($_SERVER["HTTP_USER_AGENT"], "bot") === true) {
            return false;
        } elseif (access()->id() && access()->user()->isRole('Administrator')) {
            return false;
        }

        return true;
    }

}
