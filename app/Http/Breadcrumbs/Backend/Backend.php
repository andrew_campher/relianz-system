<?php

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
});

require __DIR__ . '/Search.php';
require __DIR__ . '/Access.php';
require __DIR__ . '/Question.php';
require __DIR__ . '/Professionals.php';
require __DIR__ . '/Service.php';
require __DIR__ . '/Category.php';
require __DIR__ . '/Projects.php';
require __DIR__ . '/Quotes.php';
require __DIR__ . '/LogViewer.php';