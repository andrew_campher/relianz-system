<?php

Breadcrumbs::register('admin.category.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Category Management', route('admin.category.index'));
});

Breadcrumbs::register('admin.category.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.category.index');
    $breadcrumbs->push('Deactivated Categories', route('admin.category.deactivated'));
});

Breadcrumbs::register('admin.category.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.category.index');
    $breadcrumbs->push('Deleted Categories', route('admin.category.deleted'));
});

Breadcrumbs::register('admin.category.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.category.index');
    $breadcrumbs->push('Create Category', route('admin.category.create'));
});

Breadcrumbs::register('admin.category.show', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('admin.category.index');
	$breadcrumbs->push('View Category', route('admin.category.show', $id));
});

Breadcrumbs::register('admin.category.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.category.index');
    $breadcrumbs->push('Edit Category', route('admin.category.edit', $id));
});
