<?php

Breadcrumbs::register('admin.professional.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Professionals Management', route('admin.professional.index'));
});

Breadcrumbs::register('admin.professional.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.professional.index');
    $breadcrumbs->push('Deactivated Professionals', route('admin.professional.deactivated'));
});

Breadcrumbs::register('admin.professional.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.professional.index');
    $breadcrumbs->push('Deleted Professionals', route('admin.professional.deleted'));
});

Breadcrumbs::register('admin.professional.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.professional.index');
    $breadcrumbs->push('Create Professional', route('admin.professional.create'));
});

Breadcrumbs::register('admin.professional.show', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('admin.professional.index');
	$breadcrumbs->push('View Professional', route('admin.professional.show', $id));
});

Breadcrumbs::register('admin.professional.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.professional.index');
    $breadcrumbs->push('Edit Professional', route('admin.professional.edit', $id));
});
