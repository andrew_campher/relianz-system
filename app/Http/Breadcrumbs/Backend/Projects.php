<?php

Breadcrumbs::register('admin.projects.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Projectss Management', route('admin.projects.index'));
});

Breadcrumbs::register('admin.projects.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.projects.index');
    $breadcrumbs->push('Deactivated Projectss', route('admin.projects.deactivated'));
});

Breadcrumbs::register('admin.projects.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.projects.index');
    $breadcrumbs->push('Deleted Projectss', route('admin.projects.deleted'));
});

Breadcrumbs::register('admin.projects.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.projects.index');
    $breadcrumbs->push('Create Projects', route('admin.projects.create'));
});

Breadcrumbs::register('admin.projects.show', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('admin.projects.index');
	$breadcrumbs->push('View Projects', route('admin.projects.show', $id));
});

Breadcrumbs::register('admin.projects.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.projects.index');
    $breadcrumbs->push('Edit Projects', route('admin.projects.edit', $id));
});
