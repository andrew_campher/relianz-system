<?php

Breadcrumbs::register('admin.question.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Questions Management', route('admin.question.index'));
});

Breadcrumbs::register('admin.question.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.question.index');
    $breadcrumbs->push('Deactivated Questions', route('admin.question.deactivated'));
});

Breadcrumbs::register('admin.question.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.question.index');
    $breadcrumbs->push('Deleted Questions', route('admin.question.deleted'));
});

Breadcrumbs::register('admin.question.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.question.index');
    $breadcrumbs->push('Create Question', route('admin.question.create'));
});

Breadcrumbs::register('admin.question.show', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('admin.question.index');
	$breadcrumbs->push('View Question', route('admin.question.show', $id));
});

Breadcrumbs::register('admin.question.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.question.index');
    $breadcrumbs->push('Edit Question', route('admin.question.edit', $id));
});
