<?php

Breadcrumbs::register('admin.quotes.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Quotes Management', route('admin.quotes.index'));
});

Breadcrumbs::register('admin.quotes.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.quotes.index');
    $breadcrumbs->push('Deactivated Quotes', route('admin.quotes.deactivated'));
});

Breadcrumbs::register('admin.quotes.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.quotes.index');
    $breadcrumbs->push('Deleted Quotes', route('admin.quotes.deleted'));
});

Breadcrumbs::register('admin.quotes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.quotes.index');
    $breadcrumbs->push('Create Quote', route('admin.quotes.create'));
});

Breadcrumbs::register('admin.quotes.show', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('admin.quotes.index');
	$breadcrumbs->push('View Quote', route('admin.quotes.show', $id));
});

Breadcrumbs::register('admin.quotes.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.quotes.index');
    $breadcrumbs->push('Edit Quote', route('admin.quotes.edit', $id));
});
