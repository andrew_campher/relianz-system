<?php

Breadcrumbs::register('admin.service.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Services Management', route('admin.service.index'));
});

Breadcrumbs::register('admin.service.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.service.index');
    $breadcrumbs->push('Deactivated Services', route('admin.service.deactivated'));
});

Breadcrumbs::register('admin.service.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.service.index');
    $breadcrumbs->push('Deleted Services', route('admin.service.deleted'));
});

Breadcrumbs::register('admin.service.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.service.index');
    $breadcrumbs->push('Create Service', route('admin.service.create'));
});

Breadcrumbs::register('admin.service.show', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('admin.service.index');
	$breadcrumbs->push('View Service', route('admin.service.show', $id));
});

Breadcrumbs::register('admin.service.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.service.index');
    $breadcrumbs->push('Edit Service', route('admin.service.edit', $id));
});
