<?php

Breadcrumbs::register('frontend.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('frontend.user.dashboard'));
});

require __DIR__ . '/Project.php';
require __DIR__ . '/Pages.php';
require __DIR__ . '/Quote.php';
require __DIR__ . '/User.php';
require __DIR__ . '/Professional.php';
require __DIR__ . '/Service.php';