<?php

Breadcrumbs::register('frontend.professional.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('frontend.professional.dashboard'));
});