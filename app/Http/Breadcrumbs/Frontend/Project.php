<?php

Breadcrumbs::register('frontend.project.index', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push('Projects Management', route('frontend.project.index'));
});

Breadcrumbs::register('frontend.project.show', function ($breadcrumbs) {
    if (access()->user()->isRole('professional')) {
        $breadcrumbs->parent('frontend.professional.dashboard');
    } else {
        $breadcrumbs->parent('frontend.dashboard');
    }

    $breadcrumbs->push('Projects Management');
});

Breadcrumbs::register('frontend.project.manage', function ($breadcrumbs, $project) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push('Project: '.$project->title, route('frontend.project.manage', $project->id));
});

Breadcrumbs::register('frontend.project.all', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push('All Projects', route('frontend.project.all'));
});
