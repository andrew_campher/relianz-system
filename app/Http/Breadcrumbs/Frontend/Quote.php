<?php

Breadcrumbs::register('quote.frontend.project.manage', function ($breadcrumbs, $quote) {
    if (access()->id() && access()->id() == $quote->user_id) {
        #If professional
        $breadcrumbs->push('All Quotes', route('frontend.professional.quotes', $quote->professional_id));
        $breadcrumbs->push('Project Details', route('frontend.project.show', $quote->project_id));
    } else {
        $breadcrumbs->parent('frontend.user.dashboard');
        $breadcrumbs->push('Project: '.$quote->project->title, route('frontend.project.manage', $quote->project_id));
    }

});

Breadcrumbs::register('frontend.quote.show', function ($breadcrumbs, $quote) {
    $breadcrumbs->parent('quote.frontend.project.manage', $quote);
    $breadcrumbs->push($quote->professional->title.'\'s Quote', route('admin.quote.show', $quote));
});
