<?php

Breadcrumbs::register('public.service.show', function ($breadcrumbs, $slug) {
    $breadcrumbs->push('Home', route('public.index'));
    $breadcrumbs->push('Service', route('public.service.show', $slug));
});

Breadcrumbs::register('public.service.occupation', function ($breadcrumbs, $slug) {
    $breadcrumbs->push('Home', route('public.index'));
    $breadcrumbs->push('Service', route('public.service.show', $slug));
});

Breadcrumbs::register('public.service.service_location', function ($breadcrumbs, $slug) {
    $breadcrumbs->push('Home', route('public.index'));
    $breadcrumbs->push('Service', route('public.service.show', $slug));
});
