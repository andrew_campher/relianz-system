<?php

Breadcrumbs::register('frontend.user.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('My Projects', route('frontend.user.dashboard'));
});
