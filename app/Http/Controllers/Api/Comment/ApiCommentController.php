<?php

namespace App\Http\Controllers\Api\Comment;

use App\Events\Common\Comment\CommentCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Comment\StoreCommentRequest;
use App\Models\Comment\Comment;
use App\Models\CommentRoom\CommentRoom;
use App\Models\CommentRoomParticipant\CommentRoomParticipant;
use App\Services\MediaManager\MediaManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ApiCommentController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($model, $itemid)
    {

        /**
         * Create the Chat Room and Add members
         */
        $model_path = 'App\\Models\\' . ucfirst($model) . '\\' . ucfirst($model);
        $model_obj = $model_path::with('comments_room.comments.user')->find($itemid);

        #If model exists - lets check if room exists
        if (is_null($model_obj->comments_room)) {
            #If empty lets create it.
            $comment_room = $model_obj->comments_room()->save(new CommentRoom());

            /**
             * ADD MEMBERS to Room
             */
            switch ($model) {
                case "quote":
                    $participants = [
                        new CommentRoomParticipant(array('user_id' => $model_obj->user_id)),
                        new CommentRoomParticipant(array('user_id' => $model_obj->project->user_id)),
                    ];
                    break;

                default:
                    #If we dont know who all should be in the chat room then lets just add the user that owns the object model
                    #TODO: send through room members array
                    $participants = [
                        new CommentRoomParticipant(array('user_id' => $model_obj->user_id)),
                    ];
                    break;
            }

            $comment_room->participants()->saveMany($participants);

            ##GET single object
            $model_obj->load('comments_room')->comments_room->load(['comments.user', 'comments.media']);

        } else {
            $model_obj->comments_room->load(['comments.user', 'comments.media']);
        }

        #Update Room to Read
        CommentRoomParticipant::where('comments_room_id', $model_obj->comments_room->id)
            ->where('user_id', Auth::id())
            ->update(array('last_read' => Carbon::now()));

        return $model_obj->comments_room;
    }


    public function show(Comment $comment) {

        return $comment;
    }

    /**
     * @param StoreCommentRequest $request
     * @return string
     */
    public function store(StoreCommentRequest $request)
    {

        $comment_room = CommentRoom::find($request->comments_room_id);

        /**
         * ENSURE user is a participant of this room before allowing comment
         */
        if (!DB::table('comments_room_participants')->where('comments_room_id', $comment_room->id)->where('user_id', Auth::id())->first()) {
            return response([
                'status'=> 'error',
                'message'=> 'Your are not allowed to post in this room'
            ], 200);
        }


        $data = $request->all();

        #New comment Model
        $comment_model = new Comment();

        $comment_model->body = $data['body'];
        $comment_model->user_id = Auth::id();

        $comment = $comment_room->comments()->save($comment_model);

        var_dump($request->all());

        MediaManager::uploadfiles($request, [ get_class($comment), $comment->id]);


        event(new CommentCreated($comment));

        return '1';

    }

}