<?php

namespace App\Http\Controllers\Api\Pricing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiPricingController extends Controller
{

    public function __construct()
    {

    }

    public function update(Request $request)
    {
        if(!$request->name || (!$request->itemID && !$request->pricelevelID && !$request->customerID)) {

            return response([
                'status'=> 'error',
                'message'=> 'Error occurred - missing field'
            ], 200);
        }

        $updatePriceSQL = '';


        if($request->pricelevelID) {
            $pricelevelsql = "'".$request->pricelevelID."'";
        } else {
            $request->pricelevelID = 1;
        }




        if ($updatePriceSQL) {
            $insert_id = DB::connection('qbdb')
                ->insert($updatePriceSQL);

            return response([
                'status'=> 'success',
                'message'=> 'Price level value updated'
            ], 200);
        }
    }


}
