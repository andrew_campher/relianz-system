<?php

namespace App\Http\Controllers\Api\Logistics;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiLogisticsController extends Controller
{

    public function __construct()
    {

    }

    public function change_status(Request $request)
    {
        if(!$request->SONumber || !$request->SalesOrderID) {

            return response([
                'status'=> 'error',
                'message'=> 'Error occurred - No SO'
            ], 200);
        }

        /*WHEN ss.Status = 2 OR LOWER(MAX(s.Memo)) LIKE '%cancelled%' THEN 'Cancelled'
         WHEN ss.Status = 1 OR LOWER(MAX(s.Memo)) LIKE '%complete%' THEN 'Complete'
         WHEN ss.Status = 3 THEN 'Printed'
         WHEN ss.Status = 4 THEN 'Invoiced'
         WHEN ss.Status = 5 THEN 'Picking'
         WHEN ss.Status = 6 THEN 'NO STOCK'
         WHEN ss.Status = 7 THEN 'ON HOLD'*/

        $updateCompleteSQL = '';
        $status = 0;
        switch ($request->change_status) {
            case 'Complete':
                $status = 1;
            break;
            case 'Printed':
                $status = 3;
            break;
            case 'Cancelled':
                $status = 2;
            break;
            case 'NO STOCK':
                $status = 6;
                break;
            case 'Invoiced':
                $status = 4;
                break;
            case 'Picking':
                $status = 5;
                break;
            case 'ON HOLD':
                $status = 7;
                break;
            case 'Shipped':
                $status = 8;
                break;
            case 'Paid':
                $status = 9;
                break;
        }

        $updateCompleteSQL = "INSERT INTO SalesOrdersStatus (SalesOrderNumber, StatusText, TransactionCount, status, SalesOrderID, LastModified) 
                            VALUES ('".$request->SONumber."', '".$request->change_status."', '".$request->transcount."', {$status}, '".$request->SalesOrderID."', NOW()) ON DUPLICATE KEY UPDATE status={$status}, StatusText='".$request->change_status."', SalesOrderID='".$request->SalesOrderID."', LastModified=NOW()";

        $insert_id = DB::connection('qbdb')
            ->insert($updateCompleteSQL);

        if ($insert_id) {

            $updateLogSQL = "INSERT INTO SalesOrdersStatus_log (SalesOrderNumber, StatusText, TransactionCount, status, SalesOrderID, Created_at) VALUES ('".$request->SONumber."', '".$request->change_status."', '".$request->transcount."', {$status}, '".$request->SalesOrderID."', NOW())";
            DB::connection('qbdb')
                ->insert($updateLogSQL);

            return response([
                'status'=> 'success',
                'message'=> 'Order status was successfully updated'
            ], 200);
        } else {
            return response([
                'status'=> 'error',
                'message'=> 'Error occurred - Insert failed'
            ], 200);
        }


    }


}
