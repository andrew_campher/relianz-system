<?php

namespace App\Http\Controllers\Api\Media;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Media\ManageMediaRequest;
use App\Models\Media\Media;
use App\Repositories\Backend\Media\MediaRepository;
use Illuminate\Support\Facades\Request;

class ApiMediaController extends Controller
{

    public function __construct(MediaRepository $media_rep)
    {
        $this->media_rep = $media_rep;
    }

    public function destroy(Media $medium, ManageMediaRequest $request)
    {

        if ($this->media_rep->delete($medium)) {
            return response([
                'status'=> 'success',
                'message'=> 'file was removed'
            ], 200);
        } else {
            return response([
                'status'=> 'error',
                'message'=> 'remove failed'
            ], 200);
        }
    }

    public function show(Media $medium, Request $request)
    {

        return view('frontend.media.show')
            ->with(compact('media'));

    }

}
