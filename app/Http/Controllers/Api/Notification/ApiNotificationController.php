<?php

namespace App\Http\Controllers\Api\Notification;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Notification\Notification;
use Carbon\Carbon;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;


/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ApiNotificationController extends Controller
{
    public $unread_comments = 0;
    public $unread_notifications = 0;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(User $user)
    {

        $notifications = Notification::where('notifiable_type', 'App\Models\Access\User\User')->where('notifiable_id', $user->id)->orderBy('created_at', 'desc')->limit(10)->get();


        if ($notifications) {
            $notifications->map(function ($notification) {
                if (!$notification->read_at) {
                    $this->unread_notifications++;
                    $notification['new'] = 1;
                }


                #Set date view
                $notification['human_date'] = $notification->created_at->diffForHumans();

                $trans = 'strings.notifications.'.class_basename($notification->type);


                if (Lang::has($trans, 'en')) {

                    $notification['text'] = trans($trans.'.text');
                    $notification['url'] = trans($trans.'.url',$notification->data);
                } else {
                    $notification['text'] = trans('strings.notifications.general.text',['type' => class_basename($notification->type)]);
                }

                return $notification;
            });
        }

        $unread_comments = 0;

        if ($user->comments->load(array('user' => function($query)
        {
            $query->select('first_name','last_name', 'id');

        }))) {
            $user->comments->map(function ($comment) {

                if ($comment->last_read < $comment->created_at) {
                    $comment['new'] = 1;
                    $this->unread_comments++;
                }

                #Set date view
                $comment['human_date'] = $comment->created_at->diffForHumans();


                $comment['text'] = str_limit($comment['body'],60);

                if ($comment->roomtable_type)
                $comment['url'] = route('frontend.'.$comment->roomtable_type.'.show',  $comment->roomtable_id);

                return $comment;
            });
        }

        return response([
            'status'=> 'success',
            'data'=> [
                'notifications_count' => $this->unread_notifications,
                'notifications' => $notifications,

                'comments' => $user->comments,
                'comments_count' => $this->unread_comments,
            ]
        ], 200);
    }

    public function mark_read(DatabaseNotification $notification) {

        if (Auth::id() == $notification->notifiable_id) {
            $notification->update(['read_at' => Carbon::now()]);

            return response([
                'status'=> 'success'
            ], 200);
        } else {
            return response([
                'status'=> 'error',
                'message' => 'This notification is not yours'
            ], 200);
        }
    }

    public function mark_all_read() {

        access()->user()->unreadNotifications()->update(['read_at' => Carbon::now()]);

        return response([
            'status'=> 'success'
        ], 200);
    }

}