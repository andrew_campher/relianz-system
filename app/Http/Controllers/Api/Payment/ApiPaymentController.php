<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Payment\ProcessPaymentRequest;
use App\Models\Account\Account;
use App\Models\Invoice\Invoice;
use App\Models\Package\Package;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use App\Repositories\Backend\Payment\PaymentRepository;
use App\Services\Account\Billing;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;


/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ApiPaymentController extends Controller
{

    public function process(ProcessPaymentRequest $request, InvoiceRepository $invoices) {

        $data = $request->all();

        /*
         * Handle Credit package purchase >
         */
        $package = Package::find($request->package_id)->first();

        #get package array
        $purchase_options = \Helper::CreditPackages($package);

        $data = array_merge($data, $purchase_options[$request->credit_quantity]);

        if ($request->input('payment_method') == 'eft') {
            #past request data to save
            if ($invoice = $invoices->create($data)) {
                return [
                    'allow_continue' => 0,
                    'alert_type' => 'success',
                    'alert' => 'Success! Invoice has been created',
                    'redirect' => route('frontend.invoice.show', $invoice->id),
                ];
            }

        } elseif ($request->input('payment_method') == 'payfast') {

            /**
             * PROCEED WITH CREDIT CARD gateway payment
             */

            \Omnipay::setGateway($data['payment_method']);

            $cardInput = [
                'firstName' => access()->user()->first_name,
                'lastName' => access()->user()->first_name,
                'email' => access()->user()->email,
                'company' => access()->user()->professional->title,
            ];

            $card = \Omnipay::creditCard($cardInput);

            $response = \Omnipay::purchase([
                'amount' => isset($data['own_amount']) && $data['own_amount'] ? $data['own_amount'] : $data['amount'],
                'description' => $data['description'],
                'custom_str1' => $data['account_id'],
                'returnUrl' => route('frontend.account.index'),
                'cancelUrl' => route('frontend.account.credits_purchase', $data['account_id']),
                'notifyUrl' => route('api.payment.notify'),
                'card' => $cardInput
            ])->send();

            if ($response->isRedirect()) {
                // redirect to offsite payment gateway
                return [
                    'allow_continue' => 0,
                    'alert_type' => 'success',
                    'alert' => 'We will now redirect you to complete the payment',
                    'redirect' => $response->getRedirectUrl(),
                    'inframe' => 1,
                ];

            } else {
                // payment failed: display message to customer
                // echo $response->getMessage();
                return [
                    'allow_continue' => 0,
                    'alert_type' => 'danger',
                    'alert' => 'Your payment attempt failed with the following: <p>' . $response->getMessage() . '</p><p>Please try again</p>',
                    'redirect' => '',
                    'inframe' => 0,
                ];

            }
        }


        return [
            'allow_continue' => 0,
            'alert_type' => 'danger',
            'alert' => 'Unable to process this purchase',
            'redirect' => '',
            'inframe' => 0,
        ];

    }

    /**
     * @param Request $request
     * @return string
     */
    public function notify(Request $request)
    {


        define('SANDBOX_MODE', config('laravel-omnipay.gateways.payfast.options.testMode'));
        $pfHost = SANDBOX_MODE ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
        $pfParamString = '';
        // Posted variables from ITN
        $pfData = $_POST;

        // Strip any slashes in data
        foreach ($pfData as $key => $val) {
            $pfData[$key] = stripslashes($val);
        }


        // $pfData includes of ALL the fields posted through from PayFast, this includes the empty strings
        foreach ($pfData as $key => $val) {
            if ($key != 'signature') {
                $pfParamString .= $key . '=' . urlencode($val) . '&';
            }
        }

        // Remove the last '&' from the parameter string
        $pfParamString = substr($pfParamString, 0, -1);
        $pfTempParamString = $pfParamString;
        // If a passphrase has been set in the PayFast Settings, then it needs to be included in the signature string.
        $passPhrase = ''; //You need to get this from a constant or stored in you website database
        /// !!!!!!!!!!!!!! If you testing your integration in the sandbox, the passPhrase needs to be empty !!!!!!!!!!!!
        if (!empty($passPhrase) && !SANDBOX_MODE) {
            $pfTempParamString .= '&passphrase=' . urlencode($passPhrase);
        }
        $signature = md5($pfTempParamString);

        if ($signature != $pfData['signature']) {
            Log::warning('CC Payment Failed {'.$pfData['pf_payment_id'].'} - Invalid Signature');
            die('Invalid Signature');
        }


        // Variable initialization
        $validHosts = array(
            'www.payfast.co.za',
            'sandbox.payfast.co.za',
            'w1w.payfast.co.za',
            'w2w.payfast.co.za',
        );

        $validIps = array();

        foreach ($validHosts as $pfHostname) {
            $ips = gethostbynamel($pfHostname);

            if ($ips !== false) {
                $validIps = array_merge($validIps, $ips);
            }
        }

        // Remove duplicates
        $validIps = array_unique($validIps);

        if (!in_array($_SERVER['REMOTE_ADDR'], $validIps)) {
            Log::warning('CC Payment Failed {'.$pfData['pf_payment_id'].'} - Source IP not Valid');
            die('Source IP not Valid');
        }

        if (in_array('curl', get_loaded_extensions())) {
            // Variable initialization
            $url = 'https://' . $pfHost . '/eng/query/validate';

            // Create default cURL object
            $ch = curl_init();

            // Set cURL options - Use curl_setopt for freater PHP compatibility
            // Base settings
            curl_setopt($ch, CURLOPT_USERAGENT, PF_USER_AGENT);  // Set user agent
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      // Return output as string rather than outputting it
            curl_setopt($ch, CURLOPT_HEADER, false);             // Don't include header in output
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Standard settings
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $pfParamString);
            curl_setopt($ch, CURLOPT_TIMEOUT, PF_TIMEOUT);

            // Execute CURL
            $response = curl_exec($ch);
            curl_close($ch);
        } else {
            $header = '';
            $res = '';
            $headerDone = false;

            // Construct Header
            $header = "POST /eng/query/validate HTTP/1.0\r\n";
            $header .= "Host: " . $pfHost . "\r\n";
            $header .= "User-Agent: " . PF_USER_AGENT . "\r\n";
            $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $header .= "Content-Length: " . strlen($pfParamString) . "\r\n\r\n";

            // Connect to server
            $socket = fsockopen('ssl://' . $pfHost, 443, $errno, $errstr, PF_TIMEOUT);

            // Send command to server
            fputs($socket, $header . $pfParamString);

            $response = '';

            // Read the response from the server
            while (!feof($socket)) {
                $line = fgets($socket, 1024);

                // Check if we are finished reading the header yet
                if (strcmp($line, "\r\n") == 0) {
                    // read the header
                    $headerDone = true;
                } // If header has been processed
                else if ($headerDone) {
                    // Read the main response
                    $response .= $line;
                }
            }
        }
        $lines = explode("\r\n", $response);
        $verifyResult = trim($lines[0]);

        if (strcasecmp($verifyResult, 'VALID') != 0) {
            Log::warning('CC Payment Failed {'.$pfData['pf_payment_id'].'} - Data not valid');
            die('Data not valid');
        }

        $invoice = null;
        $pfPaymentId = $pfData['m_payment_id'];
        if ($pfPaymentId) {
            $invoice = Invoice::find($pfPaymentId)->where('status', 0)->get();

            if (!$invoice) {
                Log::warning('CC Payment Failed {'.$pfData['pf_payment_id'].'} - Invoice already paid: id='.$pfPaymentId);
                die('Data not valid');
            }

            #WE have invoice - lets
            $cartTotal = $invoice->amount; //This amount needs to be sourced from your application
            if (abs(floatval($cartTotal) - floatval($pfData['amount_gross'])) > 0.01) {
                Log::warning('CC Payment Failed {'.$pfData['pf_payment_id'].'} - Amounts Mismatch');
                die('Amounts Mismatch');
            }
        }

        //query your database and compare in order to ensure you have not processed this payment allready


        switch ($pfData['payment_status']) {
            case 'COMPLETE':

                #If no INVOICE then lets create it and record all payment info
                if (!$invoice) {
                    $invoiceRep = new InvoiceRepository();

                    $account = Account::find($pfData['custom_str1'])->get();

                    $data = [
                        'amount' => $pfData['amount_gross'],
                        'description' => $pfData['item_name'],
                        'account_id' => $pfData['custom_str1'],
                        'professional_id' => $account->professional_id,
                        'user_id' => $account->professional->user_id,
                        'package_id' => $account->package_id,
                        ];

                    $invoice = $invoiceRep->create($data);
                }

                ##THEN WITH INVOICE LETS CAPTURE PAYMENT and set to PAID status
                if ($invoice) {
                    #Existing invoice - lets set it to paid

                    $payments = new PaymentRepository();

                    $payment_data = [
                        'user_id' => $invoice->account->user_id,
                        'invoice_id' => $invoice->id,
                        'note' => $invoice->description,
                        'amount_paid' => $invoice->amount,
                    ];

                    if ($payment = $payments->create($payment_data)) {
                        #process the Payment
                        Billing::InvoiceSettled($payment);
                    }
                }
                break;
            case 'FAILED':
                // There was an error, update your application and contact a member of PayFast's support team for further assistance
                Log::warning('CC Payment Failed {'.$pfData['pf_payment_id'].'} - There was an error, update your application and contact a member of PayFast\'s support team for further assistance');
                break;
            case 'PENDING':
                // The transaction is pending, please contact a member of PayFast's support team for further assistance
                Log::warning('CC Payment Error {'.$pfData['pf_payment_id'].'} -The transaction is pending, please contact a member of PayFast\'s support team for further assistance');
                break;
            default:
                // If unknown status, do nothing (safest course of action)
                Log::warning('CC Payment Error {'.$pfData['pf_payment_id'].'} - Unknown error occured');
                break;
        }

    }

}