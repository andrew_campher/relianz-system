<?php

namespace App\Http\Controllers\Api\Pricing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiPricingController extends Controller
{

    public function __construct()
    {

    }

    public function get_item(Request $request)
    {
        if ($request->itemID) {
            $item = DB::connection('qbdb')
                ->select("SELECT Description, PurchaseCost, Price, AverageCost, QuantityOnHand, 
CASE WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9]+x[0-9]+x' THEN (LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1) * LEFT((SELECT REPLACE(JSON_UNQUOTE(CustomFields->'$.Unit'), CONCAT((SELECT LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1)), 'x'), '')),LOCATE('x',(SELECT REPLACE(JSON_UNQUOTE(CustomFields->'$.Unit'), CONCAT((SELECT LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1)), 'x'), ''))) - 1)) 
       WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9]+x'  THEN LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1) 
       WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '^[0-9]+L$' THEN 1 
       WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '^[0-9.]+kg$' THEN LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('kg',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1) 
       WHEN JSON_UNQUOTE(CustomFields->'$.KgWeight') > 0 AND (JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9.]+kg$' OR JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9.]+g$') THEN JSON_UNQUOTE(CustomFields->'$.KgWeight') ELSE 1 END AS KGUnit, 
JSON_UNQUOTE(CustomFields->'$.Unit') AS Unit, 
JSON_UNQUOTE(CustomFields->'$.KgWeight') AS Weight FROM Items WHERE ID = '".$request->itemID."'");

            if($item) {
                return response([
                    'status'=> 'success',
                    'data' =>
                        $item[0]
                    ,
                    'message'=> 'Item found'
                ], 200);
            }
        }

    }

    public function update(Request $request)
    {
        if(!$request->name || (!$request->itemID && !$request->pricelevelID && !$request->customerID)) {

            return response([
                'status'=> 'error',
                'message'=> 'Error occurred - missing field'
            ], 200);
        }

        $updatePriceSQL = '';


        if($request->pricelevelID) {
            $pricelevelsql = "'".$request->pricelevelID."'";
        } else {
            $request->pricelevelID = 1;
        }

        switch ($request->name) {
            case "price_notes":
                $updatePriceSQL = "INSERT INTO PriceLevels_settings (PriceLevelID, notes) VALUES ('".$request->pricelevelID."', '".$request->value."') 
       ON DUPLICATE KEY UPDATE notes = '".$request->value."'";
                break;

            case "RperKG":

                if ($request->customerID) {
                    $updatePriceSQL = "INSERT INTO customers_data (CustomersId, rands_per_kg) VALUES ('".$request->customerID."', '".$request->value."') 
       ON DUPLICATE KEY UPDATE rands_per_kg = '".$request->value."'";
                } else {
                    $updatePriceSQL = "INSERT INTO PriceLevels_settings (PriceLevelID, RperKG) VALUES ('".$request->pricelevelID."', '".$request->value."') 
       ON DUPLICATE KEY UPDATE RperKG = '".$request->value."'";
                }

                break;
            case "perc_change":

                if ($request->customerID) {
                    $updatePriceSQL = "INSERT INTO customers_data (CustomersId, discount_perc) VALUES ('".$request->customerID."', '".$request->value."') 
       ON DUPLICATE KEY UPDATE discount_perc = '".$request->value."'";
                } else {
                    $updatePriceSQL = "INSERT INTO PriceLevels_settings (PriceLevelID, perc_change) VALUES ('".$request->pricelevelID."', '".$request->value."') 
       ON DUPLICATE KEY UPDATE perc_change = '".$request->value."'";
                }

                break;
            case "ad_perc":
                if ($request->customerID) {
                    $updatePriceSQL = "INSERT INTO customers_data (CustomersId, marketing_perc) VALUES ('".$request->customerID."', '".$request->value."') 
       ON DUPLICATE KEY UPDATE marketing_perc = '".$request->value."'";
                } else {
                    $updatePriceSQL = "INSERT INTO PriceLevels_settings (PriceLevelID, ad_perc) VALUES ('".$request->pricelevelID."', '".$request->value."') 
       ON DUPLICATE KEY UPDATE ad_perc = '".$request->value."'";
                }

                break;

            case "comments":
                $updatePriceSQL = "INSERT INTO PriceLevel_data (ItemId, PriceLevelID, pricelist_comments) 
VALUES ('".$request->itemID."', '".$request->pricelevelID."', '".$request->value."') 
  ON DUPLICATE KEY UPDATE pricelist_comments = '".$request->value."'";
            break;
            case "new_price":
                $updatePriceSQL = "INSERT INTO PriceLevel_data (ItemId, PriceLevelID, new_price) 
VALUES ('".$request->itemID."', '".$request->pricelevelID."', '".$request->value."') 
  ON DUPLICATE KEY UPDATE new_price = '".$request->value."'";
                break;
            case "new_cost":
                $updatePriceSQL = "INSERT INTO PriceLevel_data (ItemId, PriceLevelID, new_cost) 
VALUES ('".$request->itemID."', '".$request->pricelevelID."', '".$request->value."') 
  ON DUPLICATE KEY UPDATE new_cost = '".$request->value."'";
                break;
            case "internal_comments":
                $updatePriceSQL = "INSERT INTO PriceLevel_data (ItemId, PriceLevelID, internal_comments) 
VALUES ('".$request->itemID."', '".$request->pricelevelID."', '".$request->value."') 
  ON DUPLICATE KEY UPDATE internal_comments = '".$request->value."'";
                break;

            case "special_price":
                $updatePriceSQL = "INSERT INTO PriceLevel_data (ItemId, PriceLevelID, special_price) 
VALUES ('".$request->itemID."', '".$request->pricelevelID."', '".$request->value."') 
  ON DUPLICATE KEY UPDATE special_price = '".$request->value."'";
                break;
            case "special_end":
                $updatePriceSQL = "INSERT INTO PriceLevel_data (ItemId, PriceLevelID, special_end) 
VALUES ('".$request->itemID."', '".$request->pricelevelID."', '".$request->value."') 
  ON DUPLICATE KEY UPDATE special_end = '".$request->value."'";
                break;
            case "list_price":
                $updatePriceSQL = "INSERT INTO PriceLevel_data (ItemId, PriceLevelID, list_price) 
VALUES ('".$request->itemID."', '".$request->pricelevelID."', '".$request->value."') 
  ON DUPLICATE KEY UPDATE list_price = '".$request->value."'";
                break;
        }

        if ($updatePriceSQL) {
            $insert_id = DB::connection('qbdb')
                ->insert($updatePriceSQL);

            return response([
                'status'=> 'success',
                'message'=> 'Price level value updated'
            ], 200);
        }
    }


}
