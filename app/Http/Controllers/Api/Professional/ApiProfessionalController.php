<?php

namespace App\Http\Controllers\Api\Professional;

use App\Http\Controllers\Controller;
use App\Models\Professional\Professional;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ApiProfessionalController extends Controller
{

    public function search(Request $request)
    {

        if ($request->input('string')) {
            $professionals = Professional::select('title', 'slug', 'id')->where('title', 'LIKE', $request->input('string').'%')
                ->orderBy('title', 'asc')
                ->limit(10)->get()->toArray();

            return response()->json($professionals);
        }


    }

}