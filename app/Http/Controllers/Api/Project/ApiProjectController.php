<?php

namespace App\Http\Controllers\Api\Project;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Project\StoreProjectRequest;
use App\Models\Access\User\User;
use App\Models\QuestionAnswer\QuestionAnswer;
use App\Repositories\Backend\Project\ProjectRepository;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Services\MediaManager\MediaManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ApiProjectController extends Controller
{

    public function __construct()
    {

    }


}
