<?php

namespace App\Http\Controllers\Api\Quote;

use App\Http\Controllers\Controller;
use App\Models\Quote\Quote;
use App\Repositories\Backend\Quote\QuoteRepository;

class ApiQuoteController extends Controller
{

    public function __construct(QuoteRepository $quotes)
    {
        $this->quotes = $quotes;
    }


    public function hired(Quote $quote)
    {
        /**
         * NO other quotes have been Hired
         * 1 sent
         * 2 read
         * 6 interacted
         */
        if ($quote->status == 2 || $quote->status == 1 || $quote->status == 6 || $quote->status == 7) {
            /**
             * if Logged in user is CLIENT
             */
            if (access()->id() && access()->id() == $quote->project->user_id) {

                $quote = $this->quotes->hired($quote);
                return response([
                    'status'=> 'success',
                    'data' => [
                        'type' => 'hired'
                    ],
                    'message'=> 'Quote was marked as hired'
                ], 200);
            } elseif (access()->id() && access()->id() == $quote->user_id) {

                /**
                 * if Logged in user is Professional (requesting hire)
                 */
                $quote = $this->quotes->verify_hired($quote);

                return response([
                    'status'=> 'success',
                    'data' => [
                        'type' => 'verify_hire'
                    ],
                    'message'=> 'Quote was marked as hired'
                ], 200);
            }
        }

        return response([
            'status'=> 'error',
            'message'=> 'Error occurred'
        ], 200);
    }

}
