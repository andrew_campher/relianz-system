<?php

namespace App\Http\Controllers\Api\Service;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Service\Service;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ApiServiceController extends Controller
{

    public function search(Request $request)
    {

        if ($request->input('string')) {

            /**
             * Find matching service
             */
            $services = Service::select('title','id')
                ->selectRaw("match(tags) against (? in boolean mode) AS rel", [$request->input('string').'*'])
                ->selectRaw("CASE WHEN title LIKE ? THEN 1 ELSE 0 END AS hasit", ['%'.$request->input('string').'%'])
                ->whereRaw("match(tags) against (? in boolean mode)", [$request->input('string').'*'])
                ->orderBy('rel', 'desc')
                ->orderBy('hasit', 'desc')
                ->orderBy('hits', 'desc')
                ->limit(10)->get()->toArray();

            /**
             * Log Search string
             */
            Service::logSearch($request->input('string'), count($services));

        } else {
            $services = Service::select('title','id')
                ->orderBy('hits', 'desc')
                ->limit(10)->get()->toArray();
        }

        return response()->json($services);
    }

    public function category_services(Request $request)
    {
        if ($request->input('category_id')) {
            $category = Category::find($request->input('category_id'));

            $category->load(array('services'=>function($query){
                $query->select('id','title');
            }));

            return response()->json($category->services);
        }
    }

}