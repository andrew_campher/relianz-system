<?php

namespace App\Http\Controllers\Api\User;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Exceptions\GeneralException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class UserController
 * @package App\Http\Controllers\Auth
 */
class ApiUserController extends Controller
{

    public function isMobile(Request $request)
    {
        if (!$request->input('mobile')) {
            return response([
                'status' => 'error',
                'message' => 'No mobile provided'
            ], 200);
        }

        $format_mobile = $request->input('mobile');

        if (strlen($format_mobile) != 10) {
            return response([
                'status' => 'error',
                'message' => 'Please provide 10 digits.'
            ], 200);
        }

        $format_mobile = substr($format_mobile, 1);
        $format_mobile = '+27'.$format_mobile;

        $url = 'https://api.nexmo.com/ni/basic/json?' . http_build_query(
                [
                    'api_key' => config('services.nexmo.key'),
                    'api_secret' => config('services.nexmo.secret'),
                    'number' => $format_mobile
                ]
            );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        if ($response) {
            $decoded_response = json_decode($response, true);

            if ($decoded_response['status'] == 0) {
                return response([
                    'status' => 'success',
                    'message' => $decoded_response['international_format_number'] . ' is in ' . $decoded_response['country_name']
                ], 200);
            } else {
                return response([
                    'status' => 'error',
                    'message' => 'Error: ' . $decoded_response['status'] . ": " . $decoded_response['status_message']
                ], 200);
            }
        }
        return response([
            'status' => 'success',
            'message' => 'Check failed'
        ], 200);
    }

    public function isEmailInUse(Request $request)
    {
        if (!$request->input('email')) {
            return response([
                'status' => 'error',
                'message' => 'Email not passed'
            ], 200);
        }

        $email = $request->input('email');

        if (filter_var( $email, FILTER_VALIDATE_EMAIL )) {
            if ($user = User::where('email', $email)->first()) {
                if (!$user->social_logins->isEmpty()) {
                    return response([
                        'status'=> 'success',
                        'data' => ['is_used' => 2, 'group' => $user->isRole('Professional')],
                        'message'=> 'This account is linked to a social network account. Use the social network login buttons to authenticate this user.',
                    ], 200);
                }

                return response([
                    'status' => 'success',
                    'data' => ['is_used' => 1, 'group' => $user->isRole('Professional')],
                    'message' => 'Existing user account found'
                ], 200);
            }
        }

        return response([
            'status' => 'success',
            'data' => ['is_used' => 0],
            'message' => 'No account found with this email address.'
        ], 200);
    }

    public function addContact(Request $request)
    {
        if ($request->input('professional_id')) {
            access()->user()->contacts()->toggle(['professional_id' => $request->input('professional_id')]);

            if (access()->user()->IsContact($request->input('professional_id'))) {
                return response([
                    'status' => 'success',
                    'data' => 1,
                    'message' => 'Professional was added to contacts'
                ], 200);
            } else {
                return response([
                    'status' => 'success',
                    'data' => '0',
                    'message' => 'Professional was removed from contacts'
                ], 200);
            }
        } else {
            return response([
                'status' => 'error',
                'message' => 'Professional ID not found'
            ], 200);
        }
    }
}