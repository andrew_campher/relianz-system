<?php

namespace App\Http\Controllers\Api\Widgets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiWidgetsController extends Controller
{
    public $days = 30;

    public function __construct()
    {

    }

    /*
     * ANALYTICS STUFF
     */

    public function credits_tracking() {

        $DateWhere = "Date > DATE_SUB(NOW(),INTERVAL 1 MONTH)";

        $query = "SELECT SUM(Amount) AS totalCredits, COUNT(Amount) AS totalCountCredits FROM CreditMemos WHERE {$DateWhere} AND Memo NOT LIKE '%SETTLEMENT%' AND Memo NOT LIKE '%TARGET%' AND Memo NOT LIKE '%DISCOUNT%'";
        $totals = DB::connection('qbdb')
            ->select($query);

        $query = "SELECT MAX(LEFT(Memo,LOCATE(':',Memo) - 1)) AS Department, SUM(Amount) AS totalCredits, COUNT(Amount) AS CreditCount FROM CreditMemos WHERE {$DateWhere} AND Memo NOT LIKE '%SETTLEMENT%' AND Memo NOT LIKE '%TARGET%' AND Memo NOT LIKE '%DISCOUNT%' GROUP BY LEFT(Memo,LOCATE(':',Memo) - 1) ORDER BY CreditCount DESC";
        $departments = DB::connection('qbdb')
            ->select($query);

        return response([
            'status'=> 'success',
            'totals' => $totals,
            'departments' => $departments,
            'message'=> 'Item found'
        ], 200);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * Show up stats to see how we are doing on the sales front
     */
    public function ship_tracking() {

        //=AND(U:U>T:T,J:J>(U:U/30*DAY(TODAY())))
        $query = "SELECT Monthdate, FORMAT(SUM(s.Qty*JSON_UNQUOTE(i.CustomFields->'$.KgWeight'))/1000,2) AS kgshipped FROM item_sales_history s LEFT JOIN Items i ON i.ID = s.ItemId WHERE Monthdate >= ".date("Ym", strtotime('-12 Months'))." AND Monthdate <= ".date("Y")."12 GROUP BY Monthdate ORDER BY Monthdate ASC";

        $months = DB::connection('qbdb')
            ->select($query);

        $thismonth = 0;
        $lastper = 0;

        foreach ($months AS $k => $month) {
            if ($month->Monthdate == date("Ym")) {

                $thismonth = $month->kgshipped;
                unset($months[$k]);
                continue;
            }

            if ($month->Monthdate == date("Ym", strtotime('-12 Months'))) {
                $lastper = $month->kgshipped;
            }


            $labels[] = date('M',strtotime($month->Monthdate.'01'));
            $data[] = $month->kgshipped;
        }

        $lstpertoday = ($lastper/30)*date("j");

        return response([
            'status'=> 'success',
            'data' => $data,
            'AxisLabels' => $labels,
            'thismonth' => $thismonth,
            'diff' => $thismonth-$lstpertoday,
        ], 200);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * Show up stats to see how we are doing on the sales front
     */
    public function sales_tracking() {


        return response([
            'status'=> 'success',
            'totals' => $totals,
            'departments' => $departments,
            'message'=> 'Item found'
        ], 200);
    }

    public function item_sales_increase() {

        //=AND(U:U>T:T,J:J>(U:U/30*DAY(TODAY())))
        $query = "SELECT REPLACE(REPLACE(REPLACE(i.Name, IFNULL(JSON_UNQUOTE(i.CustomFields->'$.Unit'),''), ''),' CP',''),'  ',' ') AS BaseName, MAX(i.ID) AS ID, 
        SUM((is1.Qty-((is4.Qty/".$this->days.")*".date("j")."))*i.PurchaseCost) ValueDiff, 
        SUM(is1.Qty*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) CurrrentQty, 
        SUM(is2.Qty*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) LastMonthQty, 
        SUM(is3.Qty*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) LastLastMonth, 
        SUM(is4.Qty*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) PastPeriodQty, 
        SUM((is1.Qty-((is4.Qty/".$this->days.")*".date("j")."))*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) QtyDiff, 
        SUM((is1.Qty/((is4.Qty/".$this->days.")*".date("j")."))*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) AS PercDiff 
         FROM Items i
LEFT JOIN `item_sales_history` is1 ON is1.ItemId = i.ID AND is1.Monthdate = ".date("Ym")."
LEFT JOIN item_sales_history is2 ON is2.ItemId = i.ID AND is2.Monthdate = ".date("Ym",strtotime('-1 Month'))."
LEFT JOIN item_sales_history is3 ON is3.ItemId = i.ID AND is3.Monthdate = ".date("Ym",strtotime('-2 Month'))."
LEFT JOIN item_sales_history is4 ON is4.ItemId = i.ID AND is4.Monthdate = ".date("Ym",strtotime('-12 Month'))."
WHERE is4.Qty > 0 AND is1.Qty > 0 AND i.IsActive = 1 AND Type = 'Inventory' 
GROUP BY `BaseName`
ORDER BY `PercDiff` DESC LIMIT 10";

        $items = DB::connection('qbdb')
            ->select($query);

        if($items) {
            return response([
                'status'=> 'success',
                'data' =>
                    $items
                ,
                'message'=> 'Item found'
            ], 200);
        }
    }

    public function item_sales_decrease() {

        //=AND(U:U>T:T,J:J>(U:U/30*DAY(TODAY())))
        $query = "SELECT REPLACE(REPLACE(REPLACE(i.Name, IFNULL(JSON_UNQUOTE(i.CustomFields->'$.Unit'),''), ''),' CP',''),'  ',' ') AS BaseName, MAX(i.ID) AS ID, 
        SUM((is1.Qty-((IFNULL(is4.Qty,0)/".$this->days.")*".date("j")."))*i.PurchaseCost) ValueDiff, 
        SUM(is1.Qty*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) CurrrentQty, 
        SUM(IFNULL(is2.Qty,0)*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) LastMonthQty, 
        SUM(IFNULL(is3.Qty,0)*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) LastLastMonth, 
        SUM(IFNULL(is4.Qty,0)*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) PastPeriodQty, 
        SUM((is1.Qty-((IFNULL(is4.Qty,0)/".$this->days.")*".date("j")."))*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) QtyDiff, 
        SUM((is1.Qty/((IFNULL(is4.Qty,0)/".$this->days.")*".date("j")."))*JSON_UNQUOTE(i.CustomFields->'$.KgWeight')) AS PercDiff  
        FROM Items i
LEFT JOIN `item_sales_history` is1 ON is1.ItemId = i.ID AND is1.Monthdate = ".date("Ym")."
LEFT JOIN item_sales_history is2 ON is2.ItemId = i.ID AND is2.Monthdate = ".date("Ym",strtotime('-1 Month'))."
LEFT JOIN item_sales_history is3 ON is3.ItemId = i.ID AND is3.Monthdate = ".date("Ym",strtotime('-2 Month'))."
LEFT JOIN item_sales_history is4 ON is4.ItemId = i.ID AND is4.Monthdate = ".date("Ym",strtotime('-12 Month'))."
WHERE is4.Qty > 0 AND i.IsActive = 1 AND Type = 'Inventory' 
GROUP BY `BaseName`
ORDER BY `PercDiff` ASC LIMIT 10";

        $items = DB::connection('qbdb')
            ->select($query);

        if($items) {
            return response([
                'status'=> 'success',
                'data' =>
                    $items
                ,
                'message'=> 'Item found'
            ], 200);
        }
    }

    public function get_latest_nostocks() {

     $query = "SELECT GROUP_CONCAT(si.ReferenceNumber) AS RefNUmber, DATE_FORMAT(MAX(si.TimeCreated),'%e-%m-%y') AS Date, MAX(i.Description) AS Name, MAX(i.ID) AS ID, MAX(i.QuantityOnHand) AS QtyOnHand, SUM(ItemQuantity-ItemInvoicedAmount) AS NSQty, MAX(i.QuantityOnOrder) AS QtyOnOrder, DATE_FORMAT(MAX(PODate),'%e-%m-%y') OrderedOn, DATE_FORMAT(MAX(b3.Pdate),'%e-%m-%y') AS LastRecieveDate, SUM((ItemQuantity - ItemInvoicedAmount)*ItemRate) AS SaleValue FROM `SalesOrderLineItems` si 
LEFT JOIN Items i ON i.ID = si.Itemid 
LEFT JOIN SalesOrders s ON s.ReferenceNumber = si.ReferenceNumber AND s.ShipDate = si.ShipDate AND s.CustomerId = si.CustomerId 
LEFT JOIN 
(SELECT ItemId, MIN(TimeCreated) AS PODate FROM PurchaseOrderLineItems WHERE IsManuallyClosed = 0 AND IsFullyReceived = 0 AND ItemIsManuallyClosed = 0 AND ItemId != '' GROUP BY ItemId) p3 ON p3.ItemId = i.ID 
LEFT JOIN 
(SELECT ItemId, MAX(BillId) AS BillId, MAX(date) AS Pdate FROM BillLineItems WHERE ItemId != '' GROUP BY ItemId) b3 ON b3.ItemId = i.ID 
WHERE si.ItemManuallyClosed = 0 AND si.IsManuallyClosed = 0 AND si.IsFullyInvoiced = 0 AND ItemInvoicedAmount < ItemQuantity AND (s.TransactionCount > 0 OR (si.memo LIKE '%no stock%')) AND si.TimeCreated > date_sub(NOW(), INTERVAL 2 WEEK) GROUP BY ItemName ORDER BY MAX(si.TimeCreated) DESC LIMIT 15";

        $items = DB::connection('qbdb')
            ->select($query);

        if($items) {
            return response([
                'status'=> 'success',
                'data' =>
                    $items
                ,
                'message'=> 'Item found'
            ], 200);
        }
    }
}
