<?php

namespace App\Http\Controllers\Backend\Account;

use App\Models\Account\Account;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class AccountController extends Controller
{

    protected $accounts;

    /**
     * @param AccountRepository $accounts
     */
    public function __construct()
    {
    }

    //
    public function index()
    {
        return view('backend.account.index');
    }

    public function show(Account $account, Request $request) {
        return view('backend.account.show')
            ->withAccount($account);
    }

}
