<?php

namespace App\Http\Controllers\Backend\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Account\ManageAccountRequest;
use App\Repositories\Backend\Account\AccountRepository;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class UserTableController
 */
class AccountTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $accounts;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(AccountRepository $accounts)
	{
		$this->accounts = $accounts;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageAccountRequest $request) {
		return Datatables::of($this->accounts->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($account) {
                return $account->action_buttons;
            })
             ->addColumn('name', function($account) {
                return $account->professional->title;
            })
            ->addColumn('status_label', function($account) {
                return $account->status_label;
            })
			->withTrashed()
			->make(true);
	}
}