<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class CalcController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $customers = DB::connection('qbdb')
            ->select("SELECT FullName, ID, JSON_UNQUOTE(CustomFields->'$.perc_off_statement') AS SettlementDisc FROM Customers WHERE isActive = 1 ORDER BY FullName ASC");

        $items = DB::connection('qbdb')
            ->select("SELECT Description, ID FROM Items WHERE isActive = 1 AND Type = 'Inventory' ORDER BY Description ASC");

        return view('backend.calc.index')
            ->with(compact('customers', 'items'));
    }
}