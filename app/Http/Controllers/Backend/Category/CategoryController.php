<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Requests\Backend\Category\StoreCategoryRequest;
use App\Repositories\Backend\Category\CategoryRepository;
use App\Http\Requests\Backend\Category\ManageCategoryRequest;
use App\Models\Category\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class CategoryController extends Controller
{

    protected $categories;

    /**
     * @param CategoryRepository $categories
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    //
    public function index()
    {
        $categories_raw = Category::get();
        $categories = \Helper::make_tree_array($categories_raw, 'parent_id');
        return view('backend.category.index')
            ->with(compact('categories'));
    }

    public function show(Category $category, ManageCategoryRequest $request) {
        return view('backend.category.show')
            ->withCategory($category);
    }

    public function edit(Category $category, ManageCategoryRequest $request)
    {
        $categories = Category::where('level',0);

        return view('backend.category.edit')
            ->withCategory($category)
            ->withCategories($categories->pluck('title', 'id'));
    }

    public function create(ManageCategoryRequest $request)
    {
        $categories = Category::where('level',0);

        return view('backend.category.edit')
            ->withCategories($categories->pluck('title', 'id'));
    }

    public function update(Category $category, ManageCategoryRequest $request)
    {
        $this->categories->update($category, ['data' => $request->all()]);
        return redirect()->route('admin.category.index')->withFlashSuccess('Update success');
    }

    public function store(StoreCategoryRequest $request)
    {
        $this->categories->create(['data' => $request->except('assignees_roles'), 'roles' => $request->only('assignees_roles')]);
        return redirect()->route('admin.category.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Category $category, ManageCategoryRequest $request)
    {
        $this->categories->delete($category);
        return redirect()->route('admin.category.deleted')->withFlashSuccess('Item Deleted');
    }

    public function getDeactivated(ManageCategoryRequest $request)
    {
        return view('backend.category.deactivated');
    }

    public function getDeleted(ManageCategoryRequest $request)
    {
        return view('backend.category.deleted');
    }

    public function mark(Category $category, $status, ManageCategoryRequest $request)
    {
        $this->category->mark($category, $status);
        return redirect()->route($status == 1 ? "admin.category.index" : "admin.category.deactivated")->withFlashSuccess('Items Updated');
    }
}
