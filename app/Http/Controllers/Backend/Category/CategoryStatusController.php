<?php

namespace App\Http\Controllers\Backend\Category;

use App\Models\Category\Category;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Category\CategoryRepository;
use App\Http\Requests\Backend\Category\ManageCategoryRequest;

/**
 * Class CategoryStatusController
 */
class CategoryStatusController extends Controller
{
	/**
	 * @var CategoryRepository
	 */
	protected $categorys;

	/**
	 * @param CategoryRepository $categorys
	 */
	public function __construct(CategoryRepository $categorys)
	{
		$this->categorys = $categorys;
	}

    /**
     * @param ManageCategoryRequest $request
     * @return mixed
     */
    public function getUnapproved(ManageCategoryRequest $request)
    {
        return view('backend.category.unapproved');
    }

	/**
	 * @param ManageCategoryRequest $request
	 * @return mixed
	 */
	public function getDeactivated(ManageCategoryRequest $request)
	{
		return view('backend.category.deactivated');
	}

	/**
	 * @param ManageCategoryRequest $request
	 * @return mixed
	 */
	public function getDeleted(ManageCategoryRequest $request)
	{
		return view('backend.category.deleted');
	}

	/**
	 * @param Category $user
	 * @param $status
	 * @param ManageCategoryRequest $request
	 * @return mixed
	 */
	public function mark(Category $category, $status, ManageCategoryRequest $request)
	{
		$this->categorys->mark($category, $status);
		return redirect()->route($status == 1 ? "admin.category.index" : "admin.category.deactivated")->withFlashSuccess(trans('alerts.backend.general.updated'));
	}

    public function approve(Category $category, $approved, ManageCategoryRequest $request)
    {
        $this->categorys->approve($category, $approved);

        return redirect()->route($approved == 1 ? "admin.category.index" : "admin.category.unapproved")->withFlashSuccess(trans('alerts.backend.general.updated'));
    }

	/**
	 * @param Category $deletedCategory
	 * @param ManageCategoryRequest $request
	 * @return mixed
	 */
	public function delete(Category $deletedCategory, ManageCategoryRequest $request)
	{
		$this->categorys->forceDelete($deletedCategory);
		return redirect()->route('admin.category.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
	}

	/**
	 * @param Category $deletedCategory
	 * @param ManageCategoryRequest $request
	 * @return mixed
	 */
	public function restore($deletedCategory, ManageCategoryRequest $request)
	{
	    #User component has a special RouteCategoryProvider to make this easier...
        $item = Category::withTrashed()->where('id',$deletedCategory)->first();
		$this->categorys->restore($item);
		return redirect()->route('admin.category.index')->withFlashSuccess(trans('alerts.backend.general.restored'));
	}
}