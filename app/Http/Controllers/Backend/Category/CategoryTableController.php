<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Category\CategoryRepository;
use App\Http\Requests\Backend\Category\ManageCategoryRequest;

/**
 * Class UserTableController
 */
class CategoryTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $categories;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(CategoryRepository $categories)
	{
		$this->categories = $categories;

	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageCategoryRequest $request) {
		return Datatables::of($this->categories->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('parent', function($category) {
                return $category->parent ?
                    $category->parent->title :
                    trans('labels.general.none');
            })
            ->addColumn('actions', function($category) {
                return $category->action_buttons;
            })
			->withTrashed()
			->make(true);
	}
}