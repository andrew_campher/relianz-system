<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Credit\Credit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class CreditsController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        if($request->dept) {
            $credits = Credit::where('Memo','LIKE',$request->dept.':%')->orderby('Date','DESC');
        } else {
            $credits = Credit::orderby('Date','DESC');
        }



        $credits = $credits->paginate(50);

        return view('backend.credits.index')
            ->with(compact('credits', 'request'));
    }


    public function show(Credit $credit, Request $request) {

        return view('backend.credits.show')
            ->with(compact('credit'));
    }

}