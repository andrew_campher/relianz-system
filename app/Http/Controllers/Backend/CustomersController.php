<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class CustomersController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $customers = Customer::with('data')->where("isActive",1);

        $reps = DB::connection('qbdb')
            ->select("SELECT SalesRepEntityRef_FullName, Initial FROM SalesReps WHERE Initial != ''  ORDER BY SalesRepEntityRef_FullName ASC");


        if($request->search) {
            $customers = $customers->where('Name', 'LIKE', '%'.$request->search.'%');
        }

        if($request->rep) {
            $customers = $customers->where('SalesRep', $request->rep);
        }

        if($request->class) {
            $customers = $customers->whereHas('data', function ($query) use ($request) {
                $query->where('Class', $request->class);
            });
        }

        $customers = $customers->orderby("Name","ASC");

        $customers = $customers->paginate(50);

        return view('backend.customers.index')
            ->with(compact('customers', 'request', 'reps'));
    }

    public function edit(Customer $customer, Request $request)
    {

        return view('backend.customers.edit')
            ->with(compact('customer'));
    }

    public function update(Customer $customer, Request $request)
    {
        if($customer->data->notes != $request->data['notes']) {

            /*
             * TODO: TO BE REPLACED WITH LARAVEL MAILABLES
             */
            mail(config('app.customers.send_notes_updated'), "NOTES UPDATED: ".$customer->Name, "The notes of the customer have been updated to:\n\n---------------------\n".$request->data['notes']."\n---------------------");

            //set notes_updated to today.
            $customer->data->update([
                'notes' => $request->data['notes'],
                'notes_updated' => date("Y-m-d h:i:s"),
            ]);
        }

        return redirect()->route('admin.customers.index')->withFlashSuccess('Update success');
    }

    public function notes(Request $request)
    {
        $customers = Customer::with('data')->where("Notes",'!=','');

        $reps = DB::connection('qbdb')
            ->select("SELECT SalesRepEntityRef_FullName, Initial FROM SalesReps WHERE Initial != ''  ORDER BY SalesRepEntityRef_FullName ASC");


        if($request->search) {
            $customers = $customers->where('Name', 'LIKE', '%'.$request->search.'%');
        }

        if($request->rep) {
            $customers = $customers->where('SalesRep', $request->rep);
        }

        if($request->class) {
            $customers = $customers->whereHas('data', function ($query) use ($request) {
                $query->where('Class', $request->class);
            });
        }

        $customers = $customers->orderby("Name","ASC");

        $customers = $customers->paginate(300);


        return view('backend.customers.notes')
            ->with(compact('customers', 'request', 'reps'));
    }

    public function show(Customer $customer, Request $request) {

        $orderQry = "SELECT MAX(s.CustomerName) AS Customer, MAX(s.PONumber) PONumber, MAX(ss.Status) AS Status,  MAX(il.ReferenceNumber) AS InvoiceNo, MAX(s.ShipDate) ShipDate, MAX(s.CustomerId) AS CustomerId, MAX(s.ID) AS SalesOrderID, MAX(JSON_UNQUOTE(c.CustomFields->'$.DeliveryArea')) AS Area, 
  CASE WHEN MAX(il.ItemQuantity) > 0 THEN CEIL(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*il.ItemQuantity)) 
      ELSE CEIL(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*IF(i.Name LIKE '% CP',si.ItemQuantity-si.ItemInvoicedAmount,IF(i.QuantityOnHand<0,0,IF(i.QuantityOnHand-(si.ItemQuantity-si.ItemInvoicedAmount)<0,i.QuantityOnHand,si.ItemQuantity-si.ItemInvoicedAmount)) ))) END AS Weight, CEIL(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(si.ItemQuantity-si.ItemInvoicedAmount))) AS kgweight,
   ss.vehicle AS V, 
   GROUP_CONCAT(CASE WHEN i.Name LIKE '% CP' THEN 0 ELSE i.QuantityOnHand-(si.ItemQuantity-si.ItemInvoicedAmount) END) AS StockCheck,
   s.ReferenceNumber AS 'SONumber', 
   MAX(il.ItemQuantity) AS InvoicedQty,
    CASE WHEN MAX(il.ItemQuantity) IS NULL AND MAX(s.IsManuallyClosed) = 1 THEN 'Closed' 
         WHEN ss.Status = 2 THEN 'Cancelled' 
         WHEN ss.Status = 1 THEN 'Complete' 
         WHEN ss.Status = 9 THEN 'Paid' 
         WHEN ss.Status = 8 THEN 'Shipped' 
         WHEN ss.Status = 3 THEN 'Printed' 
         WHEN ss.Status = 4 THEN 'Invoiced' 
         WHEN ss.Status = 5 THEN 'Picking' 
         WHEN ss.Status = 6 THEN 'NO STOCK' 
         WHEN ss.Status = 7 THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%cancelled%' THEN 'Cancelled' 
         WHEN LOWER(MAX(c.Company)) LIKE '%on hold' THEN 'ON HOLD' 
         WHEN LOWER(MAX(c.Terms)) LIKE '%on hold%' THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%on hold%' THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%no stock%' THEN 'NO STOCK' 
         WHEN MAX(si.IsFullyInvoiced) = 1 THEN 'Invoiced' 
         WHEN MAX(s.TransactionCount) > 0 AND MAX(s.IsManuallyClosed) = 1 THEN 'Invoiced' 
         WHEN MAX(il.ItemQuantity) > 0 THEN 'Invoiced' 
         ELSE 'Picking' END AS Status, 
     CASE WHEN LOWER(MAX(JSON_UNQUOTE(s.CustomFields->'$.Other'))) LIKE '%col%' OR MAX(JSON_UNQUOTE(c.CustomFields->'$.DeliveryArea')) LIKE '%col%' THEN 'COLLECTION' ELSE 'DELIVERY' END AS Type, 
    CASE WHEN LOWER(MAX(s.Memo)) LIKE '%urgent%' THEN 'URGENT' WHEN LOWER(MAX(s.Memo)) LIKE '%today%' THEN 'TODAY' ELSE '' END AS Urgent, 
       MAX(JSON_UNQUOTE(s.CustomFields->'$.Other')) AS User, 
      MAX(c.Terms) AS Terms, 
      MAX(JSON_UNQUOTE(c.CustomFields->'$.COA')) AS COA, 
        MAX(s.TimeCreated) As Created, MAX(il.TimeCreated) AS InvCreated, MAX(s.Memo) AS Notes, MAX(s.TransactionCount) AS TransCount
FROM SalesOrders s 
LEFT JOIN SalesOrdersStatus ss ON ss.SalesOrderID = s.ID 
LEFT JOIN SalesOrderLineItems si ON si.SalesOrderId = s.ID AND si.ItemQuantity > 0 
LEFT JOIN Items i ON i.ID = si.ItemId 
LEFT JOIN Customers c ON c.ID = s.CustomerId 
LEFT JOIN SalesOrderLinkedTransactions st ON st.SalesOrderId = s.ID  
LEFT JOIN InvoiceLineItems il ON st.TransactionReferenceNumber = il.ReferenceNumber AND st.TransactionDate = il.Date AND il.ItemId = si.ItemId AND TransactionType = 'Invoice' AND il.ShipDate = s.ShipDate AND il.ItemQuantity <= si.ItemInvoicedAmount AND il.CustomerId = st.CustomerId";

        $orders = DB::connection('qbdb')
            ->select($orderQry." WHERE s.CustomerId = '".$customer->ID."' GROUP BY s.ReferenceNumber, il.ReferenceNumber ORDER BY s.ShipDate DESC LIMIT 20");

        return view('backend.customers.show')
            ->with(compact('customer','orders'));
    }

    public function report(Customer $customer, Request $request) {

        $CustomerArr = array();
        $curr = array();

        /*
         * TOTAL SALES
         */
        $customer_alls = DB::connection('qbdb')
            ->select("SELECT MAX(i.Description) AS ItemDescription, SUM(il.ItemQuantity) AS Qty, MAX(c.SalesRep) AS SalesRep, MAX(ItemId) AS ItemId 
FROM InvoiceLineItems il LEFT JOIN Customers c ON c.ID = il.CustomerId 
LEFT JOIN Items i ON i.ID = il.ItemId
WHERE il.CustomerId = '".$customer->ID."' AND il.ItemName NOT LIKE '.%' AND c.IsActive = 1 AND il.Date >= DATE_SUB(NOW(), INTERVAL 5 YEAR) GROUP BY il.ItemId ORDER BY SUM(il.ItemQuantity) DESC");

        if ($customer_alls) {
            foreach ($customer_alls AS $customer_all) {
                $CustomerArr[$customer_all->ItemDescription] = 1;

                $curr[$customer_all->ItemDescription]['t'] = $customer_all->Qty;
                $curr[$customer_all->ItemDescription]['rep'] = $customer_all->SalesRep;
                $curr[$customer_all->ItemDescription]['ItemId'] = $customer_all->ItemId;
            }
        }

        /*
         * THIS YEAR
         */
        $customer_current = DB::connection('qbdb')
            ->select("SELECT MAX(i.Description) AS ItemDescription, SUM(il.ItemQuantity) AS Qty, MAX(ItemId) AS ItemId 
FROM InvoiceLineItems il LEFT JOIN Customers c ON c.ID = il.CustomerId 
LEFT JOIN Items i ON i.ID = il.ItemId
WHERE il.CustomerId = '".$customer->ID."' AND c.IsActive = 1 AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 YEAR), '%Y-%m-01') AND il.Date <= NOW() GROUP BY ItemId ORDER BY SUM(il.ItemQuantity) DESC");

        if ($customer_current) {
            foreach ($customer_current AS $customer_curr) {
                $curr[$customer_curr->ItemDescription]['y'] = $customer_curr->Qty;
            }
        }

        /*
         * THIS MONTH SALES
         */
        $customer_current = DB::connection('qbdb')
            ->select("SELECT MAX(i.Description) AS ItemDescription, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty, MAX(il.ItemId) AS ItemId FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
LEFT JOIN Items i ON i.ID = il.ItemId
WHERE il.CustomerId = '".$customer->ID."' AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(NOW(), '%Y-%m-01') AND il.Date <= NOW() GROUP BY il.ItemId ORDER BY Qty DESC");

        if ($customer_current) {
            foreach ($customer_current AS $customer_curr) {
                $curr[$customer_curr->ItemDescription]['m'] = $customer_curr->Qty;
            }
        }

        /*
         * PAST YEAR
         */
        $customer_history = DB::connection('qbdb')
            ->select("SELECT MAX(i.Description) AS ItemDescription, Month(il.Date) AS Mnth, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty, MAX(il.ItemId) AS ItemId FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
LEFT JOIN Items i ON i.ID = il.ItemId
WHERE il.CustomerId = '".$customer->ID."' AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 11 MONTH), '%Y-%m-01') AND il.Date <= NOW() 
GROUP BY il.ItemId, Month(il.Date)");

        $monthArray = array(
            date("n")=>0,
            date("n",strtotime(date('Y-m')." -1 Month"))=>0,
            date("n",strtotime(date('Y-m')." -2 Month"))=>0,
            date("n",strtotime(date('Y-m')." -3 Month"))=>0,
            date("n",strtotime(date('Y-m')." -4 Month"))=>0,
            date("n",strtotime(date('Y-m')." -5 Month"))=>0,
            date("n",strtotime(date('Y-m')." -6 Month"))=>0,
            date("n",strtotime(date('Y-m')." -7 Month"))=>0,
            date("n",strtotime(date('Y-m')." -8 Month"))=>0,
            date("n",strtotime(date('Y-m')." -9 Month"))=>0,
            date("n",strtotime(date('Y-m')." -10 Month"))=>0,
            date("n",strtotime(date('Y-m')." -11 Month"))=>0,
        );

        $monthTots = $monthArray;

        if ($customer_history) {
            foreach ($customer_history AS $customer_hist) {
                $curr[$customer_hist->ItemDescription][$customer_hist->Mnth] = $customer_hist->Qty;
                $monthTots[$customer_hist->Mnth] += $customer_hist->Qty;

            }
        }

        /*
         * PAST 2 YEAR
         */
        $hist_yrs = DB::connection('qbdb')
            ->select("SELECT MAX(i.Description) AS ItemDescription, Month(il.Date) AS Mnth, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty, MAX(il.ItemId) AS ItemId FROM InvoiceLineItems il 
LEFT JOIN Items i ON i.ID = il.ItemId 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE il.CustomerId = '".$customer->ID."' AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 YEAR), INTERVAL 11 MONTH), '%Y-%m-01') AND il.Date <= DATE_SUB(NOW(), INTERVAL 1 YEAR) GROUP BY il.ItemId, Month(il.Date)");
        $PrevMonthTots = $monthArray;

        $past = array();

        if ($hist_yrs) {
            foreach ($hist_yrs AS $hist_yr) {
                $past[$hist_yr->ItemDescription][$hist_yr->Mnth] = $hist_yr->Qty;
                $PrevMonthTots[$hist_yr->Mnth] += $hist_yr->Qty;
            }
        }

        return view('backend.customers.report')
            ->with(compact('customer','CustomerArr', 'curr', 'bills', 'monthTots', 'PrevMonthTots', 'past'));
    }
}