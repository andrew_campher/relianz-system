<?php

namespace App\Http\Controllers\Backend\Invoice;

use App\Events\Common\Payment\PaymentCreated;
use App\Http\Requests\Backend\Invoice\StoreInvoiceRequest;
use App\Http\Requests\Backend\Payment\StorePaymentRequest;
use App\Models\Access\User\User;
use App\Models\Professional\Professional;
use App\Models\Service\Service;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use App\Http\Requests\Backend\Invoice\ManageInvoiceRequest;
use App\Models\Invoice\Invoice;
use App\Repositories\Backend\Payment\PaymentRepository;
use App\Services\Account\Billing;
use App\Services\MediaManager\MediaManager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class InvoiceController extends Controller
{

    protected $invoices;

    /**
     * @param InvoiceRepository $invoices
     */
    public function __construct(InvoiceRepository $invoices)
    {
        $this->invoices = $invoices;
    }

    //
    public function index()
    {
        return view('backend.invoice.index');
    }

    public function show(Invoice $invoice, Request $request) {
        return view('backend.invoice.show')
            ->withInvoice($invoice);
    }

    public function edit(Invoice $invoice, ManageInvoiceRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');

        return view('backend.invoice.edit')
            ->withInvoice($invoice)
            ->with(compact('users','professionals'));
    }

    public function create(ManageInvoiceRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');
        $services = Service::orderBy('title')->get()->pluck('title', 'id');

        return view('backend.invoice.edit')
            ->with(compact('users','services', 'professionals'));
    }

    public function update(Invoice $invoice, ManageInvoiceRequest $request)
    {

        $data = $request->all();

        $this->invoices->update($invoice, ['data' => $data]);
        return redirect()->route('admin.invoice.index')->withFlashSuccess('Update success');
    }


    /**
     * @param PaymentRepository $payments
     * @param StorePaymentRequest $request
     * @return mixed
     */
    public function store_payment(PaymentRepository $payments, StorePaymentRequest $request)
    {

        $data = $request->all();

        if ($payment = $payments->create($data)) {

        }

        return redirect()->route('admin.invoice.index')->withFlashSuccess('Payment Successful');
    }

    public function store(StoreInvoiceRequest $request)
    {

        $data = $request->all();

        #past request data to save
        if ($invoice = $this->invoices->create(['data' => $data])) {

        }
        return redirect()->route('admin.invoice.index')->withFlashSuccess('Invoice Created');
    }

    public function destroy(Invoice $invoice, ManageInvoiceRequest $request)
    {
        $this->invoices->delete($invoice);
        return redirect()->route('admin.invoice.deleted')->withFlashSuccess('Item Deleted');
    }
}
