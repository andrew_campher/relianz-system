<?php

namespace App\Http\Controllers\Backend\Invoice;

use App\Models\Invoice\Invoice;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use App\Http\Requests\Backend\Invoice\ManageInvoiceRequest;
use Illuminate\Support\Facades\Request;

/**
 * Class InvoiceStatusController
 */
class InvoiceStatusController extends Controller
{
	/**
	 * @var InvoiceRepository
	 */
	protected $invoices;

    public $status_filter = 0;

	/**
	 * @param InvoiceRepository $invoices
	 */
	public function __construct(InvoiceRepository $invoices)
	{

		$this->invoices = $invoices;
        switch (Request::segment(3)) {
            case "unpaid":
                $this->status_filter = 0;
            break;
            case "paid":
                $this->status_filter = 1;
                break;
            case "cancelled":
                $this->status_filter = 2;
                break;
        }
	}

	/**
	 * @param ManageInvoiceRequest $request
	 * @return mixed
	 */
	public function getUnpaid(ManageInvoiceRequest $request)
	{

		return view('backend.invoice.index')
            ->with('status_filter', $this->status_filter);
	}

	/**
	 * @param ManageInvoiceRequest $request
	 * @return mixed
	 */
	public function getPaid(ManageInvoiceRequest $request)
	{
		return view('backend.invoice.index')
            ->with('status_filter', $this->status_filter);
	}

    public function getCancelled(ManageInvoiceRequest $request)
    {
        return view('backend.invoice.index')
            ->with('status_filter', $this->status_filter);
    }

	/**
	 * @param Invoice $user
	 * @param $status
	 * @param ManageInvoiceRequest $request
	 * @return mixed
	 */
	public function mark(Invoice $invoice, $status, ManageInvoiceRequest $request)
	{
		$this->invoices->mark($invoice, $status);
		return redirect()->route($status == 1 ? "admin.invoice.index" : "admin.invoice.deactivated")->withFlashSuccess(trans('alerts.backend.general.updated'));
	}

	/**
	 * @param Invoice $deletedInvoice
	 * @param ManageInvoiceRequest $request
	 * @return mixed
	 */
	public function delete(Invoice $deletedInvoice, ManageInvoiceRequest $request)
	{
		$this->invoices->forceDelete($deletedInvoice);
		return redirect()->route('admin.invoice.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
	}

	/**
	 * @param Invoice $deletedInvoice
	 * @param ManageInvoiceRequest $request
	 * @return mixed
	 */
	public function restore($deletedInvoice, ManageInvoiceRequest $request)
	{
	    #User component has a special RouteInvoiceProvider to make this easier...
        $item = Invoice::withTrashed()->where('id',$deletedInvoice)->first();
		$this->invoices->restore($item);
		return redirect()->route('admin.invoice.index')->withFlashSuccess(trans('alerts.backend.general.restored'));
	}
}