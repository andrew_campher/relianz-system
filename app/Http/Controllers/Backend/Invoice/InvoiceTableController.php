<?php

namespace App\Http\Controllers\Backend\Invoice;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use App\Http\Requests\Backend\Invoice\ManageInvoiceRequest;

/**
 * Class UserTableController
 */
class InvoiceTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $invoices;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(InvoiceRepository $invoices)
	{
		$this->invoices = $invoices;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageInvoiceRequest $request) {
		return Datatables::of($this->invoices->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($invoice) {
                return $invoice->action_buttons;
            })
            ->addColumn('image_html', function($invoice) {
                return $invoice->image_html;
            })
            ->addColumn('name', function($invoice) {
                return $invoice->user->name;
            })
            ->addColumn('status_label', function($invoice) {
                return $invoice->status_label;
            })
			->withTrashed()
			->make(true);
	}
}