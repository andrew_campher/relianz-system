<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Item\Item;
use App\Models\Item\ItemData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class ItemsController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $items = Item::with('items_data')->where("Type","Inventory");
        if (!$request->isactive) {
            $items = $items->where("isActive",1);
        }

        $items = $items->orderby("Description","ASC");
        if($request->search) {
            $items = $items->where('Description', 'LIKE', '%'.$request->search.'%');
        }

        $items = $items->paginate(50);

        return view('backend.items.index')
            ->with(compact('items', 'request'));
    }

    public function edit(Item $item, Request $request)
    {

        return view('backend.items.edit')
            ->with(compact('item'));
    }

    public function update(Item $item, Request $request)
    {
        if (!isset($item->items_data)) {
            $item->items_data()->create([
                'pricelist' => $request->items_data['pricelist'],
                'stock_type' => $request->items_data['stock_type'],
            ]);
        } else {
            $item->items_data->update([
                'pricelist' => $request->items_data['pricelist'],
                'stock_type' => $request->items_data['stock_type'],
            ]);
        }


        return redirect()->route('admin.items.index')->withFlashSuccess('Update success');
    }

    public function show(Item $item, Request $request) {
        return view('backend.items.show')
            ->with(compact('item'));
    }

    public function nostocks(Item $item, Request $request) {

        $items = DB::connection('qbdb')
            ->select("SELECT i.Description, id.Class, FirstOpenPO, LastPurchaseOrder, CASE WHEN i.QuantityOnHand < 0 THEN 0 ELSE i.QuantityOnHand END AS QuantityOnHand, FirstBackOrder, 
BackOrderQty, 
i.QuantityOnOrder AS QuantityOnOrder, CASE WHEN i.QuantityOnOrder > 0 THEN DATE_FORMAT(OrderExpectedDate,'%d/%m/%Y') ELSE '' END AS ETA, i.ID as ItemId, 
id.procurement_notes 
FROM Items i 
LEFT JOIN items_data id ON id.ItemId = i.ID 
LEFT JOIN 
(SELECT ItemId, SUM(il.ItemQuantity) AS PastYearQtySales FROM InvoiceLineItems il WHERE il.ItemQuantity > 0 AND il.TimeCreated >= DATE_SUB(NOW(),INTERVAL 6 MONTH) GROUP BY il.ItemId) sa ON sa.ItemId = i.ID 
LEFT JOIN 
(SELECT bi.ItemId, VendorName, Pdate AS LastRecieve FROM 
   (SELECT ItemId, MAX(BillId) AS BillId, MAX(date) AS Pdate FROM BillLineItems WHERE ItemId != '' GROUP BY ItemId) AS bi 
 INNER JOIN Bills b ON bi.BillId = b.ID) b2 ON b2.ItemId = i.ID 
LEFT JOIN 
  (SELECT ItemId, MIN(s.Date) AS FirstBackOrder, SUM(si.ItemQuantity-si.ItemInvoicedAmount) AS BackOrderQty FROM SalesOrderLineItems si LEFT JOIN SalesOrders s ON s.ID = si.SalesOrderId WHERE si.ItemQuantity > si.ItemInvoicedAmount AND si.IsFullyInvoiced = 0 AND si.IsManuallyClosed = 0 AND si.ItemManuallyClosed = 0 AND s.IsManuallyClosed = 0 AND s.IsFullyInvoiced = 0 GROUP BY si.ItemId) so ON so.ItemId = i.ID 
LEFT JOIN 
(SELECT pi.ItemId, VendorName, Pdate AS LastPurchaseOrder, firstPdate AS FirstOpenPO, Terms AS PaymentTerms, TotalPurchased, MaxShortDelivered, TotalShortDelivered, OrderExpectedDate, Memo 
        FROM 
        (SELECT ItemId, MAX(pl.PurchaseOrderID) AS PurchaseOrderID, MIN(pl.date) AS firstPdate, MAX(pl.date) AS Pdate, MAX(pl.ExpectedDate) AS OrderExpectedDate, SUM(pl.ItemQuantity) as TotalPurchased, MAX(ItemQuantity-ItemReceivedQuantity) as     MaxShortDelivered, SUM(pl.ItemQuantity-pl.ItemReceivedQuantity) as TotalShortDelivered 
         FROM PurchaseOrderLineItems pl LEFT JOIN PurchaseOrders p ON pl.PurchaseOrderID = p.ID WHERE pl.ItemId != '' AND pl.IsManuallyClosed != 1 AND pl.IsFullyReceived != 1 AND pl.IsFullyReceived != 1 AND p.IsManuallyClosed != 1 GROUP BY ItemId) AS pi 
        INNER JOIN PurchaseOrders p ON pi.PurchaseOrderID = p.ID) p2 ON p2.ItemId = i.ID 
 WHERE i.IsActive = 1 AND i.Type = 'Inventory' AND i.FullName NOT LIKE '% CP' AND (i.QuantityOnHand-i.QuantityOnSalesOrder) <= 0 AND (i.QuantityOnSalesOrder > 0 OR i.QuantityOnOrder > 0 OR sa.PastYearQtySales > 10) ORDER BY (CASE WHEN i.QuantityOnSalesOrder > 0 THEN 1 ELSE 0 END) DESC, sa.PastYearQtySales DESC LIMIT 300");

        return view('backend.items.nostocks')
            ->with(compact('items'));
    }

    public function report(Item $item, Request $request) {

        $CustomerArr = array();
        $curr = array();
        $bills = array();

        //Get Cost changes from Bills
        $billquery = "SELECT `Date`, ItemCost, ReferenceNumber, v.Name AS Supplier, ItemQuantity FROM BillLineItems bl 
LEFT JOIN Vendors v ON v.ID = bl.VendorId
WHERE ItemId = '".$item->ID."' AND ItemQuantity > 0 AND `Date` >= DATE_SUB(NOW(), INTERVAL 10 YEAR) ORDER BY Date DESC";

        $billlines = DB::connection('qbdb')
            ->select($billquery);

        $lastprice = 0;
        foreach ($billlines AS $billline) {
            if($lastprice != $billline->ItemCost) {
                $bills[] = $billline;
            }

            $lastprice = $billline->ItemCost;
        }

        /*
         * TOTAL SALES for 5 years
         */
        $customer_alls = DB::connection('qbdb')
            ->select("SELECT MAX(il.CustomerName) AS CustomerName, SUM(il.ItemQuantity) AS Qty, MAX(c.SalesRep) AS SalesRep, MAX(il.CustomerId) AS CustomerId FROM InvoiceLineItems il 
LEFT JOIN Customers c ON c.ID = il.CustomerId WHERE il.ItemId = '".$item->ID."' AND c.IsActive = 1 AND il.Date >= DATE_SUB(NOW(), INTERVAL 5 YEAR) GROUP BY il.CustomerId ORDER BY SUM(il.ItemQuantity) DESC");

        if ($customer_alls) {
            foreach ($customer_alls AS $customer_all) {
                $CustomerArr[$customer_all->CustomerName] = 1;

                $curr[$customer_all->CustomerName]['t'] = $customer_all->Qty;
                $curr[$customer_all->CustomerName]['rep'] = $customer_all->SalesRep;
                $curr[$customer_all->CustomerName]['CustomerId'] = $customer_all->CustomerId;
            }
        }

        /*
         * THIS YEAR - from beginning
         */
        $customer_current = DB::connection('qbdb')
            ->select("SELECT MAX(CustomerName) AS CustomerName, SUM(il.ItemQuantity) AS Qty FROM InvoiceLineItems il 
              LEFT JOIN Customers c ON c.ID = il.CustomerId 
              WHERE il.ItemId = '".$item->ID."' AND c.IsActive = 1 AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 YEAR), '%Y-%m-01') AND il.Date <= NOW() GROUP BY CustomerId ORDER BY SUM(il.ItemQuantity) DESC");

        if ($customer_current) {
            foreach ($customer_current AS $customer_curr) {
                $curr[$customer_curr->CustomerName]['y'] = $customer_curr->Qty;
            }
        }

        /*
         * THIS MONTH SALES
         */
        $customer_current = DB::connection('qbdb')
            ->select("SELECT MAX(il.CustomerName) AS CustomerName, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE il.ItemId = '".$item->ID."' AND il.Date >= DATE_FORMAT(NOW(), '%Y-%m-01') AND il.Date <= NOW() GROUP BY il.CustomerId ORDER BY Qty DESC");

        if ($customer_current) {
            foreach ($customer_current AS $customer_curr) {
                $curr[$customer_curr->CustomerName]['m'] = $customer_curr->Qty;
            }
        }

        /*
         * PAST YEAR
         */
        $customer_history = DB::connection('qbdb')
            ->select("SELECT MAX(il.CustomerName) AS CustomerName, Month(il.Date) AS Mnth, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE il.ItemId = '".$item->ID."' AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 11 MONTH), '%Y-%m-01') AND il.Date <= NOW() GROUP BY il.CustomerId, Month(il.Date)");

        $monthTotsVar = array(
            date("n")=>0,
            date("n",strtotime(date('Y-m')." -1 Month"))=>0,
            date("n",strtotime(date('Y-m')." -2 Month"))=>0,
            date("n",strtotime(date('Y-m')." -3 Month"))=>0,
            date("n",strtotime(date('Y-m')." -4 Month"))=>0,
            date("n",strtotime(date('Y-m')." -5 Month"))=>0,
            date("n",strtotime(date('Y-m')." -6 Month"))=>0,
            date("n",strtotime(date('Y-m')." -7 Month"))=>0,
            date("n",strtotime(date('Y-m')." -8 Month"))=>0,
            date("n",strtotime(date('Y-m')." -9 Month"))=>0,
            date("n",strtotime(date('Y-m')." -10 Month"))=>0,
            date("n",strtotime(date('Y-m')." -11 Month"))=>0,
        );

        $monthTots = $monthTotsVar;

        if ($customer_history) {
            foreach ($customer_history AS $customer_hist) {
                $curr[$customer_hist->CustomerName][$customer_hist->Mnth] = $customer_hist->Qty;

                $monthTots[$customer_hist->Mnth] += $customer_hist->Qty;
            }
        }

        /*
         * PAST 2 YEAR
         */
        $hist_yrs = DB::connection('qbdb')
            ->select("SELECT MAX(il.CustomerName) AS CustomerName, Month(il.Date) AS Mnth, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE il.ItemId = '".$item->ID."' AND il.Date >= DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 YEAR), INTERVAL 11 MONTH), '%Y-%m-01') AND il.Date <= DATE_SUB(NOW(), INTERVAL 1 YEAR) GROUP BY il.CustomerId, Month(il.Date)");
        $PrevMonthTots = $monthTotsVar;

        $past = array();

        if ($hist_yrs) {
            foreach ($hist_yrs AS $hist_yr) {
                $past[$hist_yr->CustomerName][$hist_yr->Mnth] = $hist_yr->Qty;
                $PrevMonthTots[$hist_yr->Mnth] += $hist_yr->Qty;
            }
        }

        return view('backend.items.report')
            ->with(compact('item','CustomerArr', 'curr', 'bills', 'monthTots', 'PrevMonthTots', 'past'));
    }

    public function group_report(Request $request) {

        if ($request->name) {

            $CustomerArr = array();
            $curr = array();
            $bills = array();


            $billlines = DB::connection('qbdb')
                ->select("SELECT MAX(bl.Date) AS Date, MAX(bl.ItemCost) AS ItemCost, MAX(v.Name) AS Supplier, MAX(bl.ReferenceNumber) ReferenceNumber FROM BillLineItems bl 
LEFT JOIN Items i 
LEFT JOIN Vendors v ON v.ID = bl.VendorId WHERE i.FullName LIKE '".$request->name."%' AND bl.ItemCost > 0 AND bl.Date >= DATE_SUB(NOW(), INTERVAL 10 YEAR) GROUP BY CASE WHEN i.FullName LIKE '".$request->name."%' THEN '".$request->name."' END ORDER BY bl.Date DESC");

            $lastprice = 0;
            foreach ($billlines AS $billline) {
                if($lastprice && $lastprice != $billline->ItemCost) {
                    $bills[] = $billline;
                }

                $lastprice = $billline->ItemCost;
            }

            /*
             * TOTAL SALES
             */
            $customer_alls = DB::connection('qbdb')
                ->select("SELECT MAX(il.CustomerName) AS CustomerName, SUM(il.ItemQuantity) AS Qty, MAX(c.SalesRep) AS SalesRep, MAX(il.CustomerId) AS CustomerId FROM InvoiceLineItems il LEFT JOIN Items i ON i.ID = il.ItemId LEFT JOIN Customers c ON c.ID = il.CustomerId WHERE il.ItemId = '".$item->ID."' AND c.IsActive = 1 AND il.Date >= DATE_SUB(NOW(), INTERVAL 5 YEAR) GROUP BY il.CustomerId ORDER BY SUM(il.ItemQuantity) DESC");

            if ($customer_alls) {
                foreach ($customer_alls AS $customer_all) {
                    $CustomerArr[$customer_all->CustomerName] = 1;

                    $curr[$customer_all->CustomerName]['t'] = $customer_all->Qty;
                    $curr[$customer_all->CustomerName]['rep'] = $customer_all->SalesRep;
                    $curr[$customer_all->CustomerName]['CustomerId'] = $customer_all->CustomerId;
                }
            }

            /*
             * THIS YEAR
             */
            $customer_current = DB::connection('qbdb')
                ->select("SELECT MAX(CustomerName) AS CustomerName, SUM(il.ItemQuantity) AS Qty FROM InvoiceLineItems il 
              LEFT JOIN Customers c ON c.ID = il.CustomerId 
              WHERE il.ItemId = '".$item->ID."' AND c.IsActive = 1 AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 YEAR), '%Y-%m-01') AND il.Date <= NOW() GROUP BY CustomerId ORDER BY SUM(il.ItemQuantity) DESC");

            if ($customer_current) {
                foreach ($customer_current AS $customer_curr) {
                    $curr[$customer_curr->CustomerName]['y'] = $customer_curr->Qty;
                }
            }

            /*
             * THIS MONTH SALES
             */
            $customer_current = DB::connection('qbdb')
                ->select("SELECT MAX(il.CustomerName) AS CustomerName, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE il.ItemId = '".$item->ID."' AND il.Date >= DATE_FORMAT(NOW(), '%Y-%m-01') AND il.Date <= NOW() GROUP BY il.CustomerId ORDER BY Qty DESC");

            if ($customer_current) {
                foreach ($customer_current AS $customer_curr) {
                    $curr[$customer_curr->CustomerName]['m'] = $customer_curr->Qty;
                }
            }

            /*
             * PAST YEAR
             */
            $customer_history = DB::connection('qbdb')
                ->select("SELECT MAX(il.CustomerName) AS CustomerName, Month(il.Date) AS Mnth, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE il.ItemId = '".$item->ID."' AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 12 MONTH), '%Y-%m-01') AND il.Date <= LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 MONTH)) GROUP BY il.CustomerId, Month(il.Date)");

            $monthTotsVar = array(
                date("n")=>0,
                date("n",strtotime(date('Y-m')." -1 Month"))=>0,
                date("n",strtotime(date('Y-m')." -2 Month"))=>0,
                date("n",strtotime(date('Y-m')." -3 Month"))=>0,
                date("n",strtotime(date('Y-m')." -4 Month"))=>0,
                date("n",strtotime(date('Y-m')." -5 Month"))=>0,
                date("n",strtotime(date('Y-m')." -6 Month"))=>0,
                date("n",strtotime(date('Y-m')." -7 Month"))=>0,
                date("n",strtotime(date('Y-m')." -8 Month"))=>0,
                date("n",strtotime(date('Y-m')." -9 Month"))=>0,
                date("n",strtotime(date('Y-m')." -10 Month"))=>0,
                date("n",strtotime(date('Y-m')." -11 Month"))=>0,
            );

            $monthTots = $monthTotsVar;

            if ($customer_history) {
                foreach ($customer_history AS $customer_hist) {
                    $curr[$customer_hist->CustomerName][$customer_hist->Mnth] = $customer_hist->Qty;

                    $monthTots[$customer_hist->Mnth] += $customer_hist->Qty;
                }
            }

            /*
             * PAST 2 YEAR
             */
            $hist_yrs = DB::connection('qbdb')
                ->select("SELECT Month(il.Date) AS Mnth, SUM(il.ItemQuantity-IFNULL(cl.ItemQuantity,0)) AS Qty FROM InvoiceLineItems il 
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
WHERE il.ItemId = '".$item->ID."' AND il.Date >= DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 YEAR), INTERVAL 12 MONTH), '%Y-%m-01') AND il.Date <= LAST_DAY(DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 YEAR), INTERVAL 1 MONTH)) GROUP BY Month(il.Date)");
            $PrevMonthTots = $monthTotsVar;

            if ($hist_yrs) {
                foreach ($hist_yrs AS $hist_yr) {
                    $PrevMonthTots[$hist_yr->Mnth] += $hist_yr->Qty;
                }
            }
        }


        return view('backend.items.report')
            ->with(compact('item','CustomerArr', 'curr', 'bills', 'monthTots', 'PrevMonthTots'));
    }
}