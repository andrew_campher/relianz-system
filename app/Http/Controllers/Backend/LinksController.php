<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Link\Link;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class LinksController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $links = Link::paginate(50);

        return view('backend.links.index')
            ->with(compact('links', 'request'));
    }

    public function create(Link $link, Request $request)
    {
        $users = User::pluck('name','id');

        return view('backend.links.edit')
            ->with(compact('link', 'users'));
    }

    public function edit(Link $link, Request $request)
    {
        $users = User::pluck('name','id');

        return view('backend.links.edit')
            ->with(compact('link', 'users'));
    }

    public function update(Link $link, Request $request)
    {

        $link->update([
            'menu_title' => $request->menu_title,
            'url' => $request->url,
            'sort' => $request->sort,
            'roles' => $request->roles,
            'user_id' => $request->user_id,
            'active' => $request->active,
            'icon' => $request->active,
        ]);
        return redirect()->route('admin.links.index')->withFlashSuccess('Update success');
    }

    public function store(Link $link, Request $request)
    {

        $link->insert([
            'menu_title' => $request->menu_title,
            'url' => $request->url,
            'roles' => $request->roles,
            'sort' => $request->sort,
            'user_id' => $request->user_id,
            'active' => $request->active,
            'icon' => $request->active,
        ]);
        return redirect()->route('admin.links.index')->withFlashSuccess('Update success');
    }

    public function show(Link $link, Request $request) {
        return view('backend.links.show')
            ->with(compact('link'));
    }

}