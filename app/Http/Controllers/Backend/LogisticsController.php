<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class LogisticsController extends Controller
{

    public $orderQry = "SELECT MAX(s.CustomerName) AS Customer, MAX(s.ShipDate) as ShipDate, MAX(il.ReferenceNumber) AS InvoiceNo, MAX(s.CustomerId) AS CustomerId, MAX(s.ID) AS SalesOrderID, MAX(JSON_UNQUOTE(c.CustomFields->'$.DeliveryArea')) AS Area, 
CASE WHEN MAX(il.ItemQuantity) > 0 THEN SUM(il.ItemRate*il.ItemQuantity) 
      ELSE CEIL(SUM(si.ItemRate*IF(i.Name LIKE '% CP',si.ItemQuantity-si.ItemInvoicedAmount,IF(i.QuantityOnHand<0,0,IF(i.QuantityOnHand-(si.ItemQuantity-si.ItemInvoicedAmount)<0,i.QuantityOnHand,si.ItemQuantity-si.ItemInvoicedAmount)) ))) END AS SalesValue,
  CASE WHEN MAX(il.ItemQuantity) > 0 THEN CEIL(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*il.ItemQuantity)) 
      ELSE CEIL(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*IF(i.Name LIKE '% CP',si.ItemQuantity-si.ItemInvoicedAmount,IF(i.QuantityOnHand<0,0,IF(i.QuantityOnHand-(si.ItemQuantity-si.ItemInvoicedAmount)<0,i.QuantityOnHand,si.ItemQuantity-si.ItemInvoicedAmount)) ))) END AS Weight, 
      CASE WHEN MAX(il.ItemQuantity) > 0 THEN CEIL(SUM((JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*il.ItemQuantity)+(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(si.ItemQuantity-si.ItemInvoicedAmount)))) ELSE CEIL(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(si.ItemQuantity-si.ItemInvoicedAmount))) END AS kgweight,
   ss.vehicle AS V, 
   CASE WHEN MAX(il.ItemQuantity) > 0 THEN GROUP_CONCAT(CASE WHEN i.Name LIKE '% CP' OR il.ItemQuantity IS NULL THEN 1 ELSE i.QuantityOnHand+il.ItemQuantity END) 
   ELSE GROUP_CONCAT(CASE WHEN i.Name LIKE '% CP' THEN 1 ELSE i.QuantityOnHand-(si.ItemQuantity-si.ItemInvoicedAmount) END) 
   END AS StockCheck,
   s.ReferenceNumber AS 'SONumber', 
   MAX(il.ItemQuantity) AS InvoicedQty,
    CASE WHEN MAX(il.ItemQuantity) IS NULL AND MAX(s.IsManuallyClosed) = 1 THEN 'Closed' 
         WHEN ss.Status = 2 THEN 'Cancelled' 
         WHEN ss.Status = 1 THEN 'Complete' 
         WHEN ss.Status = 9 THEN 'Paid' 
         WHEN ss.Status = 8 THEN 'Shipped' 
         WHEN ss.Status = 3 THEN 'Printed' 
         WHEN ss.Status = 4 THEN 'Invoiced' 
         WHEN ss.Status = 5 THEN 'Picking' 
         WHEN ss.Status = 6 THEN 'NO STOCK' 
         WHEN ss.Status = 7 THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%cancelled%' THEN 'Cancelled' 
         WHEN LOWER(MAX(c.Company)) LIKE '%on hold' THEN 'ON HOLD' 
         WHEN LOWER(MAX(c.Terms)) LIKE '%on hold%' THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%on hold%' THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%no stock%' THEN 'NO STOCK' 
         WHEN MAX(si.IsFullyInvoiced) = 1 THEN 'Invoiced' 
         WHEN MAX(s.TransactionCount) > 0 AND MAX(s.IsManuallyClosed) = 1 THEN 'Invoiced' 
         WHEN MAX(il.ItemQuantity) > 0 THEN 'Invoiced' 
         ELSE 'Picking' END AS Status, 
     CASE WHEN LOWER(MAX(JSON_UNQUOTE(s.CustomFields->'$.Other'))) LIKE '%col%' OR MAX(JSON_UNQUOTE(c.CustomFields->'$.DeliveryArea')) LIKE '%col%' THEN 'COLLECTION' ELSE 'DELIVERY' END AS Type, 
    CASE WHEN LOWER(MAX(s.Memo)) LIKE '%urgent%' THEN 'URGENT' WHEN LOWER(MAX(s.Memo)) LIKE '%today%' THEN 'TODAY' ELSE '' END AS Urgent, 
       MAX(JSON_UNQUOTE(s.CustomFields->'$.Other')) AS User, 
      MAX(c.Terms) AS Terms, 
      MAX(JSON_UNQUOTE(c.CustomFields->'$.COA')) AS COA, 
        MAX(s.TimeCreated) As Created, MAX(il.TimeCreated) AS InvCreated, MAX(s.Memo) AS Notes, MAX(s.TransactionCount) AS TransCount
FROM SalesOrders s 
LEFT JOIN SalesOrdersStatus ss ON ss.SalesOrderID = s.ID 
LEFT JOIN SalesOrderLineItems si ON si.SalesOrderId = s.ID AND si.ItemQuantity > 0 
LEFT JOIN Items i ON i.ID = si.ItemId 
LEFT JOIN Customers c ON c.ID = s.CustomerId 
LEFT JOIN SalesOrderLinkedTransactions st ON st.SalesOrderId = s.ID  
LEFT JOIN InvoiceLineItems il ON st.TransactionReferenceNumber = il.ReferenceNumber AND st.TransactionDate = il.Date AND il.ItemId = si.ItemId AND TransactionType = 'Invoice' AND il.ShipDate = s.ShipDate AND il.ItemQuantity <= si.ItemInvoicedAmount AND il.CustomerId = st.CustomerId ";
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $getDate = date("Y-m-d");

        if($request->show_date) {
            $getDate = $request->show_date;
        }

        $this->orderQry .= "WHERE s.ShipDate = '".$getDate." 00:00:00' AND LOWER(s.CustomerName) NOT LIKE '%private%' GROUP BY s.ReferenceNumber, s.CustomerId ORDER BY s.CustomerName ASC, s.ReferenceNumber ASC";

        $orders = DB::connection('qbdb')
            ->select($this->orderQry);

        /**
         * Create weight in area array
         */
        $order_weight = array();

        foreach ($orders AS $order) {
            if ($order->Type == "COLLECTION") {
                $order->Area = 'COL';
            }

            $order_weight[$order->SONumber][$order->Customer][$order->Area] = $order->Weight;

        }

        /**
         * Create Orders Array in Areas
         */
        $area_orders = array();
        $collection_orders = array();

        $salesvalue = 0;

        //Count Orders
        $totalOrders = count($orders);
        $totalWeight = 0;
        $AllTotalWeight = 0;

        foreach ($orders AS $order) {

            if($order->Type == "COLLECTION") {
                $collection_orders['orders'][] = $order;
                $collection_orders['areas']['COL'] = 1;
                continue;
            }

            $totalWeight += $order->Weight;

            $AllTotalWeight += $order->kgweight;

            if(isset($order->SalesValue) && $order->SalesValue) {
                $salesvalue += $order->SalesValue;
            }

            switch ($order->Area) {
                case "KG1":
                case "MG1":
                    $area_orders[1]['areas'][$order->Area] = 1;
                    $area_orders[1]['orders'][] = $order;
                    break;
                case "S01":
                case "S02":
                case "S03":
                    $area_orders[2]['areas'][$order->Area] = 1;
                    $area_orders[2]['orders'][] = $order;
                    break;
                case "N01":
                case "N02":
                case "N03":
                    $area_orders[3]['areas'][$order->Area] = 1;
                    $area_orders[3]['orders'][] = $order;
                    break;
                case "M01":
                case "M02":
                case "M03":
                    $area_orders[4]['areas'][$order->Area] = 1;
                    $area_orders[4]['orders'][] = $order;
                    break;
                case "E01":
                case "E02":
                case "E03":
                    $area_orders[5]['areas'][$order->Area] = 1;
                    $area_orders[5]['orders'][] = $order;
                    break;
                case "GRDN":
                    $area_orders[6]['areas'][$order->Area] = 1;
                    $area_orders[6]['orders'][] = $order;
                    break;
                case "JHB":
                case "ITT":
                case "COURIER":
                    $area_orders[7]['areas'][$order->Area] = 1;
                    $area_orders[7]['orders'][] = $order;
                    break;
                default:
                    $area_orders[8]['areas'][$order->Area] = 1;
                    $area_orders[8]['orders'][] = $order;
                break;
            }

        }

        #dd($area_orders);

        return view('backend.logistics.index')
            ->with(compact('orders', 'area_orders', 'salesvalue', 'collection_orders', 'getDate', 'order_weight', 'totalOrders', 'totalWeight', 'AllTotalWeight'));
    }

    public function collections(Request $request)
    {

        $orderQuery = $this->orderQry." WHERE ";

        if($request->Status) {
            $orderQuery .= "ss.Status = '".$request->Status."' AND ";
        } else {
            $orderQuery .= "ss.Status != 1 AND ss.Status != 8 AND ";
        }

        $getDate = '';

        if($request->show_date) {
            $getDate = $request->show_date;
            $orderQuery .= "s.ShipDate = '".$getDate." 00:00:00' AND ";
        }

        $orderQuery .= " LOWER(s.CustomerName) NOT LIKE '%private%' AND (LOWER(JSON_UNQUOTE(s.CustomFields->'$.Other')) LIKE '%col%' OR JSON_UNQUOTE(c.CustomFields->'$.Collection') != '') GROUP BY s.ReferenceNumber, s.CustomerId ORDER BY s.ShipDate DESC LIMIT 100";

        $orders = DB::connection('qbdb')->select($orderQuery);

        /**
         * Create weight in area array
         */
        $order_weight = array();

        /**
         * Create Orders Array in Areas
         */

        //Count Orders
        $totalOrders = count($orders);
        $totalWeight = 0;
        $AllTotalWeight = 0;

        foreach ($orders AS $order) {

            $totalWeight += $order->Weight;

            $AllTotalWeight += $order->kgweight;

        }

        #dd($area_orders);

        return view('backend.logistics.collections')
            ->with(compact('orders', 'getDate', 'order_weight', 'totalOrders', 'totalWeight', 'AllTotalWeight'));
    }
}