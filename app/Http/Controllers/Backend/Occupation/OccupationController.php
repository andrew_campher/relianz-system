<?php

namespace App\Http\Controllers\Backend\Occupation;

use App\Http\Requests\Backend\Occupation\StoreOccupationRequest;
use App\Models\Category\Category;
use App\Models\Occupation\Occupation;
use App\Models\Question\Question;
use App\Repositories\Backend\Occupation\OccupationRepository;
use App\Http\Requests\Backend\Occupation\ManageOccupationRequest;
use App\Http\Controllers\Controller;
use App\Services\Images\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Helpers\Helper;

class OccupationController extends Controller
{

    protected $occupations;

    /**
     * @param OccupationRepository $occupations
     */
    public function __construct(OccupationRepository $occupations)
    {
        $this->occupations = $occupations;
    }

    //
    public function index(Request $request)
    {

        if ($request->input('q')) {
            $occupations = Occupation::where('title','LIKE','%'.$request->input('q').'%')->orderBy('title', 'asc')->paginate(30);
        } else {
            $occupations = Occupation::orderBy('title', 'asc')->paginate(30);
        }

        return view('backend.occupation.index')
            ->with(compact('occupations'));
    }

    public function show(Occupation $occupation, ManageOccupationRequest $request) {

        $occupation_questions = $occupation->questions()->get();

        $questions = Question::all()->pluck('title', 'id');

        return view('backend.occupation.show')
            ->with(compact('occupation', 'occupation_questions', 'questions'));
    }

    public function edit(Occupation $occupation, ManageOccupationRequest $request)
    {
        $categories = Helper::make_tree_array(Category::get()->toArray(),'parent_id');

        $occupations = Occupation::get();

        return view('backend.occupation.edit')
            ->with(compact('occupation','categories', 'occupations'));
    }

    public function create(ManageOccupationRequest $request)
    {
        $categories = Helper::make_tree_array(Category::get()->toArray(),'parent_id');

        return view('backend.occupation.edit')
            ->withCategories($categories);
    }

    public function update(Occupation $occupation, StoreOccupationRequest $request)
    {

        $data = $request->all();

        $this->occupations->update($occupation, ['data' => $data]);
        return redirect()->route('admin.occupation.index')->withFlashSuccess('Update success');
    }

    public function store(StoreOccupationRequest $request)
    {

        $data = $request->all();


        #past request data to save
        $this->occupations->create(['data' => $data]);
        return redirect()->route('admin.occupation.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Occupation $occupation, ManageOccupationRequest $request)
    {
        $this->occupations->delete($occupation);
        return redirect()->route('admin.occupation.deleted')->withFlashSuccess('Item Deleted');
    }

}
