<?php

namespace App\Http\Controllers\Backend\Occupation;

use App\Models\Occupation\Occupation;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Occupation\OccupationRepository;
use App\Http\Requests\Backend\Occupation\ManageOccupationRequest;

/**
 * Class OccupationStatusController
 */
class OccupationStatusController extends Controller
{
	/**
	 * @var OccupationRepository
	 */
	protected $occupations;

	/**
	 * @param OccupationRepository $occupations
	 */
	public function __construct(OccupationRepository $occupations)
	{
		$this->occupations = $occupations;
	}

	/**
	 * @param ManageOccupationRequest $request
	 * @return mixed
	 */
	public function getDeactivated(ManageOccupationRequest $request)
	{
		return view('backend.occupation.deactivated');
	}

	/**
	 * @param ManageOccupationRequest $request
	 * @return mixed
	 */
	public function getDeleted(ManageOccupationRequest $request)
	{
		return view('backend.occupation.deleted');
	}

	/**
	 * @param Occupation $user
	 * @param $status
	 * @param ManageOccupationRequest $request
	 * @return mixed
	 */
	public function mark(Occupation $occupation, $status, ManageOccupationRequest $request)
	{
		$this->occupations->mark($occupation, $status);
		return redirect()->route($status == 1 ? "admin.occupation.index" : "admin.occupation.deactivated")->withFlashSuccess(trans('alerts.backend.general.updated'));
	}

	/**
	 * @param Occupation $deletedOccupation
	 * @param ManageOccupationRequest $request
	 * @return mixed
	 */
	public function delete(Occupation $deletedOccupation, ManageOccupationRequest $request)
	{
		$this->occupations->forceDelete($deletedOccupation);
		return redirect()->route('admin.occupation.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
	}

	/**
	 * @param Occupation $deletedOccupation
	 * @param ManageOccupationRequest $request
	 * @return mixed
	 */
	public function restore($deletedOccupation, ManageOccupationRequest $request)
	{
	    #User component has a special RouteOccupationProvider to make this easier...
        $item = Occupation::withTrashed()->where('id',$deletedOccupation)->first();
		$this->occupations->restore($item);
		return redirect()->route('admin.occupation.index')->withFlashSuccess(trans('alerts.backend.general.restored'));
	}
}