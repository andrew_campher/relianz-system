<?php

namespace App\Http\Controllers\Backend\Occupation;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Occupation\OccupationRepository;
use App\Http\Requests\Backend\Occupation\ManageOccupationRequest;

/**
 * Class UserTableController
 */
class OccupationTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $occupation;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(OccupationRepository $occupations)
	{
		$this->occupations = $occupations;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageOccupationRequest $request) {
		return Datatables::of($this->occupations->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($occupation) {
                return $occupation->action_buttons;
            })
            ->addColumn('image_html', function($occupation) {
                return $occupation->image_html;
            })
            ->addColumn('fee', function($occupation) {
                return Helper::formatPrice($occupation->fee);
            })
			->withTrashed()
			->make(true);
	}
}