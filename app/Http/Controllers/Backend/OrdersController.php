<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class OrdersController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        if($request->search) {
            $orders = Order::where('ReferenceNumber', 'LIKE', '%'.trim($request->search).'%')->orderby('TimeCreated', 'DESC');
        } else {
            $orders = Order::orderby('TimeCreated', 'DESC');
        }

        if($request->search) {
            $orders = Order::where('ReferenceNumber', 'LIKE', '%'.trim($request->search).'%')->orderby('TimeCreated', 'DESC');
        }

        $orders = $orders->paginate(30);

        return view('backend.orders.index')
            ->with(compact('orders', 'request'));
    }

    public function showso($sonumber) {

        $order = Order::where('ReferenceNumber', $sonumber)->first();
        if($order) {
            return redirect()->route('admin.orders.show',[$order->ID]);
        }
    }

    public function show(Order $order, Request $request) {



        return view('backend.orders.show')
            ->with(compact('order'));
    }

    public function showbo(Request $request) {
        $customer = Customer::find($request->ID);

        $order_items = array();
        $order_items_ns = array();
        $has_order_items = array();

        $items = DB::connection('qbdb')
            ->select("SELECT s.ID, si.ItemId, i.Name, i.Price,si.SalesOrderId, si.TimeCreated, si.ReferenceNumber, i.QuantityOnHand, i.QuantityOnOrder, i2.QuantityOnHand As BulkQty, i2.Name AS BulkName, i.PurchaseCost, si.ItemRate, si.ItemQuantity, si.ItemRate, si.ItemInvoicedAmount, i.CustomFields, i.AverageCost 
FROM SalesOrderLineItems si
LEFT JOIN SalesOrders s ON s.ID = si.SalesOrderId
LEFT JOIN Items i ON i.ID = si.ItemId
LEFT JOIN Items i2 ON REPLACE(i2.Name, IFNULL(JSON_UNQUOTE(i2.CustomFields->'$.Unit'),''), '') = REPLACE(REPLACE(i.Name, ' CP', ''), IFNULL(JSON_UNQUOTE(i.CustomFields->'$.Unit'),''), '') AND i2.Name NOT LIKE '% CP' AND i.Name LIKE '% CP'
 WHERE si.CustomerId = '".$customer->ID."' AND si.ItemManuallyClosed = 0 AND TransactionCount > 0 AND si.ItemQuantity > si.ItemInvoicedAmount AND i.Name != '' ORDER BY si.TimeCreated DESC");

        foreach ($items AS $item){

            $getnextOrder = "SELECT SUM(il.ItemQuantity) AS NewOrderQty, MIN(il.Date) AS OrderDate, MIN(il.ShipDate) As Shipdate 
FROM InvoiceLineItems il INNER JOIN SalesOrderLinkedTransactions si ON si.TransactionReferenceNumber = il.ReferenceNumber AND si.SalesOrderId != '".$item->SalesOrderId."'
WHERE ItemId = '".$item->ItemId."' AND il.CustomerId = '".$customer->ID."' AND si.ID IS NOT NULL AND ItemQuantity > 0 AND il.TimeCreated > '".$item->TimeCreated."' GROUP BY ItemId";

            $hasOrder = DB::connection('qbdb')
                ->select($getnextOrder);

            if (isset($hasOrder[0])) {
                $has_order_items[$item->Name] = $hasOrder[0];
            }

            //IN stock items
            if ($item->QuantityOnHand > 0 || $item->BulkQty > 0) {
                $order_items[$item->Name]['items'][] = $item;
                $order_items[$item->Name]['name'] = $item->Name;
            } else {

                //Else NO STOCK
                $order_items_ns[$item->Name]['items'][] = $item;
                $order_items_ns[$item->Name]['name'] = $item->Name;
                $order_items_ns[$item->Name]['ItemId'] = $item->ItemId;
                $order_items_ns[$item->Name]['QuantityOnOrder'] = $item->QuantityOnOrder;

            }

        }

        /**
         * GET ETA query on NO STOCK lines
         */
        foreach ($order_items_ns AS $key => $val) {
            //GET ETA
            if ($val['QuantityOnOrder'] > 0) {

                $getETA = "SELECT MIN(pl.ExpectedDate) AS ExpectedDate, SUM(pl.ItemQuantity) as TotalPurchased 
FROM PurchaseOrderLineItems pl INNER JOIN PurchaseOrders p ON pl.PurchaseOrderID = p.ID 
WHERE pl.ItemId = '" . $val['ItemId'] . "' AND pl.IsManuallyClosed != 1 AND pl.IsFullyReceived != 1 AND pl.IsFullyReceived != 1 AND p.IsManuallyClosed != 1 AND ItemQuantity > 0 GROUP BY ItemId";

                $eta = DB::connection('qbdb')
                    ->select($getETA);
                if ($eta) {
                    $order_items_ns[$key]['eta'] = $eta[0];
                }

            }
        }

        $futureorders = Order::where('CustomerId',$customer->ID)
            ->where('TransactionCount',0)
            ->where('ShipDate','>=',date("Y-m-d 00:00:00"))->get();

        return view('backend.orders.showbo')
            ->with(compact('customer', 'order_items', 'has_order_items', 'order_items_ns', 'futureorders'));
    }

    public function backorders($instock = 1,Request $request) {

        $reps = DB::connection('qbdb')
            ->select("SELECT SalesRepEntityRef_FullName, Initial FROM SalesReps WHERE Initial != ''  ORDER BY SalesRepEntityRef_FullName ASC");

        $jobtypes = DB::connection('qbdb')
            ->select("SELECT JobType, COUNT(ID) AS nocustomers FROM Customers WHERE isActive = 1 AND JobType != '' GROUP BY JobType");

        $check_jobtype = '';
        if($request->JobType) {
            $check_jobtype = " AND JobType = '".$request->JobType."'";
        } elseif($request->JobType == 'All') {
            $check_jobtype = " AND JobType = ''";
        }

        $check_rep = '';
        if($request->rep) {
            $check_rep = "AND c.SalesRep = '".$request->rep."'";
        }

        $stockCheckSQL = "AND i2.QuantityOnHand >= si.ItemQuantity";
        $stockCheckSQL2 = "i.QuantityOnHand >= si.ItemQuantity";
        if ($instock == 0) {
            $stockCheckSQL = "AND i2.QuantityOnHand < si.ItemQuantity";
            $stockCheckSQL2 = "i.QuantityOnHand < si.ItemQuantity";
        }

        $query = "SELECT ReferenceNumber, Name, CustomerName, CustomerId, ReceivedDate, PurchaseCost, ItemQuantity, ItemInvoicedAmount, Memo, QuantityOnHand, bulkQty FROM (
SELECT si.ReferenceNumber, si.CustomerName, si.CustomerId, i.Name, ReceivedDate, i.PurchaseCost, si.ItemQuantity, si.ItemInvoicedAmount, s.Memo, i.QuantityOnHand, i2.QuantityOnHand AS bulkQty FROM `Items` i 
LEFT JOIN Items i2 ON REPLACE(i2.Name, IFNULL(JSON_UNQUOTE(i2.CustomFields->'$.Unit'),''), '') = REPLACE(REPLACE(i.Name, ' CP', ''), IFNULL(JSON_UNQUOTE(i.CustomFields->'$.Unit'),''), '') 
LEFT JOIN SalesOrderLineItems si ON si.ItemId = i.ID 
LEFT JOIN SalesOrders s ON s.ID = si.SalesOrderId 
LEFT JOIN SalesOrdersStatus ss ON ss.SalesOrderID = s.ID 
RIGHT JOIN Customers c ON c.ID = s.CustomerId {$check_jobtype} {$check_rep}
LEFT JOIN 
(SELECT ItemId, MAX(Date) AS ReceivedDate FROM BillLineItems WHERE ItemQuantity > 0 AND DATE(Date) >= DATE(NOW() - INTERVAL 3 MONTH) GROUP BY ItemId ) b ON b.ItemId = i2.ID
WHERE i.Name LIKE '% CP' ".$stockCheckSQL."  
AND si.ItemQuantity > si.ItemInvoicedAmount AND si.IsFullyInvoiced = 0 AND si.ItemManuallyClosed = 0  AND si.CustomerName NOT LIKE 'Private%' AND (s.ShipDate < DATE(NOW() - INTERVAL 1 DAY) OR s.ShipDate is NULL)
UNION ALL 
        SELECT si.ReferenceNumber, si.CustomerName, si.CustomerId, i.Name, ReceivedDate, i.PurchaseCost, si.ItemQuantity, si.ItemInvoicedAmount, s.Memo, i.QuantityOnHand, '' AS bulkQty FROM `Items` i 
LEFT JOIN SalesOrderLineItems si ON si.ItemId = i.ID 
LEFT JOIN SalesOrders s ON s.ID = si.SalesOrderId 
LEFT JOIN SalesOrdersStatus ss ON ss.SalesOrderID = s.ID 
RIGHT JOIN Customers c ON c.ID = s.CustomerId {$check_jobtype} {$check_rep}
LEFT JOIN 
(SELECT ItemId, MAX(Date) AS ReceivedDate FROM BillLineItems WHERE ItemQuantity > 0 AND DATE(Date) >= DATE(NOW() - INTERVAL 3 MONTH) GROUP BY ItemId ) b ON b.ItemId = i.ID
WHERE ".$stockCheckSQL2." 
AND si.ItemQuantity > si.ItemInvoicedAmount AND si.IsFullyInvoiced = 0 AND si.ItemManuallyClosed = 0  AND si.CustomerName NOT LIKE 'Private%' AND (s.ShipDate < DATE(NOW() - INTERVAL 1 DAY) OR s.ShipDate is NULL)
) AS t1 
ORDER BY CASE WHEN ReceivedDate > DATE_SUB(NOW(), INTERVAL 7 DAY) THEN ReceivedDate ELSE 0 END DESC, PurchaseCost*ItemQuantity DESC";

        $items = DB::connection('qbdb')
            ->select($query);

        $boItems = array();

        foreach ($items AS $skey => $item) {

            if (!isset($boItems[$item->CustomerName]['SaleValue'])) {
                $boItems[$item->CustomerName]['SaleValue'] = 0;
                $boItems[$item->CustomerName]['OnSOBackorder'] = 0;
                $boItems[$item->CustomerName]['QuantityOnHand'] = 0;
            }

            $boItems[$item->CustomerName]['CustomerName'] = $item->CustomerName;
            $boItems[$item->CustomerName]['SaleValue'] += $item->PurchaseCost*$item->ItemQuantity;
            $boItems[$item->CustomerName]['CustomerId'] = $item->CustomerId;


            $boItems[$item->CustomerName]['items'][] = $item;
        }

        return view('backend.orders.backorders')
            ->with(compact('boItems', 'jobtypes', 'request', 'instock', 'reps'));
    }

}