<?php

namespace App\Http\Controllers\Backend\Package;

use App\Http\Requests\Backend\Common\ManageListRequest;
use App\Http\Requests\Backend\Package\StorePackageRequest;
use App\Models\Access\User\User;
use App\Models\Professional\Professional;
use App\Models\Service\Service;
use App\Repositories\Backend\Package\PackageRepository;
use App\Http\Requests\Backend\Package\ManagePackageRequest;
use App\Models\Package\Package;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class PackageController extends Controller
{

    protected $packages;

    /**
     * @param PackageRepository $packages
     */
    public function __construct(PackageRepository $packages)
    {
        $this->packages = $packages;
    }

    //
    public function index()
    {
        $packages = Package::paginate(15);
        return view('backend.package.index')
            ->with(compact('packages'));
    }

    public function action(ManageListRequest $request)
    {

        $packages = Package::find($request->selected);
        switch ($request->action) {
            case "delete":
                $this->destroy($packages, $request);
            break;
        }
    }

    public function show(Package $package, Request $request) {
        return view('backend.package.show')
            ->withPackage($package);
    }

    public function edit(Package $package, ManagePackageRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');

        return view('backend.package.edit')
            ->withPackage($package)
            ->with(compact('users','professionals'));
    }

    public function create(ManagePackageRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');
        $services = Service::orderBy('title')->get()->pluck('title', 'id');

        return view('backend.package.edit')
            ->with(compact('users','services', 'professionals'));
    }

    public function update(Package $package, ManagePackageRequest $request)
    {

        $data = $request->all();

        $this->packages->update($package, ['data' => $data]);
        return redirect()->route('admin.package.index')->withFlashSuccess('Update success');
    }


    public function store(StorePackageRequest $request)
    {

        $data = $request->all();

        #past request data to save
        if ($package = $this->packages->create($data)) {

        }
        return redirect()->route('admin.package.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Package $package, ManagePackageRequest $request)
    {
        $this->packages->delete($package);
        return redirect()->route('admin.package.deleted')->withFlashSuccess('Item Deleted');
    }
}
