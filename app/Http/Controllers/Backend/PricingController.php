<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class PricingController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $customers = DB::connection('qbdb')
            ->select("SELECT FullName, ID FROM Customers WHERE isActive = 1 ORDER BY FullName ASC");

        $CustomerTypes = DB::connection('qbdb')
            ->select("SELECT Name, ID FROM CustomerTypes WHERE isActive = 1 ORDER BY Name ASC");

        $pricelevels = DB::connection('qbdb')
            ->select("SELECT pl.ID, pl.Name, pl.PriceLevelFixedPercentage, ps.RperKG, ps.perc_change, ps.ad_perc FROM PriceLevels pl LEFT JOIN PriceLevels_settings ps ON ps.PriceLevelID = pl.ID WHERE isActive = 1 ORDER BY Name ASC");


        $thepricelvl = array();

        $thepricelvl['perc_change'] = 0;
        $thepricelvl['RperKG'] = 0;
        $thepricelvl['ad_perc'] = 0;

        $sqlPricelvl = "PriceLevelID = 1";
        $sqlPricelvlID = "ID = 1";

        $onlylvl = "";
        $onlysales = "";
        $sqlpricelist = "";

        if($request->onlysales) {
            $onlysales = " AND PastYearQtySales > 0";
        }

        if($request->pricelist) {
            $sqlpricelist = " AND d.pricelist LIKE '%".$request->pricelist."%'";
        }

        if ($request->pricelevelID) {

            if($request->onlylevel) {
                $onlylvl = " AND id.new_price > 0";
            }

            $thepricelvls = DB::connection('qbdb')
                ->select("SELECT pl.ID, pl.Name, pl.PriceLevelFixedPercentage, ps.RperKG, ps.perc_change, ps.ad_perc, ps.notes FROM PriceLevels pl LEFT JOIN PriceLevels_settings ps ON ps.PriceLevelID = pl.ID WHERE pl.ID = '" . $request->pricelevelID . "'");
            if ($thepricelvls) {
                $thepricelvlclass = $thepricelvls[0];

                $thepricelvl['RperKG'] = $thepricelvlclass->RperKG;
                $thepricelvl['perc_change'] = $thepricelvlclass->perc_change;
                $thepricelvl['ad_perc'] = $thepricelvlclass->ad_perc;
                $thepricelvl['notes'] = $thepricelvlclass->notes;
            }

            $sqlPricelvl = "PriceLevelID = '".$request->pricelevelID."'";
            $sqlPricelvlID = "ID = '".$request->pricelevelID."'";
        }

        $ItemsSQL = "SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Description, ' CP', ''), IFNULL(JSON_UNQUOTE(CustomFields->'$.Unit'),''), ''), ' **LAUNCH DEAL**', ''), ' **SPECIAL DEAL**', ''), '  ', ' ') AS Product, 
  JSON_UNQUOTE(CustomFields->'$.Unit') AS Unit, 
  CASE WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9]+x[0-9]+x' THEN (LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1) * LEFT((SELECT REPLACE(JSON_UNQUOTE(CustomFields->'$.Unit'), CONCAT((SELECT LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1)), 'x'), '')),LOCATE('x',(SELECT REPLACE(JSON_UNQUOTE(CustomFields->'$.Unit'), CONCAT((SELECT LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1)), 'x'), ''))) - 1)) 
       WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9]+x'  THEN LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('x',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1) 
       WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '^[0-9]+L$' THEN 1 
       WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '^[0-9.]+kg$' THEN LEFT(JSON_UNQUOTE(CustomFields->'$.Unit'),LOCATE('kg',JSON_UNQUOTE(CustomFields->'$.Unit')) - 1) 
       WHEN JSON_UNQUOTE(CustomFields->'$.KgWeight') > 0 AND (JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9.]+kg$' OR JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9.]+g$') THEN JSON_UNQUOTE(CustomFields->'$.KgWeight') ELSE 1 END AS KGUnit, 
  JSON_UNQUOTE(CustomFields->'$.KgWeight') AS 'Weight', PastYearQtySales, 
  QuantityOnHand, 
  CASE WHEN JSON_UNQUOTE(CustomFields->'$.Unit') RLIKE '[0-9]+x' THEN 'Yes' END AS SingleUnits, 
  LastPaid, PurchaseCost,  
  Price, 
  (Price-PurchaseCost)/Price AS 'Cost GP %', 
  CASE WHEN pl.PriceLevelFixedPercentage > 0 THEN CEIL((PriceLevelFixedPercentage/100+1)*Price/0.10)*0.10 
    ELSE PriceLevelPerItemRet_CustomPrice END as CustomPrice, 
  i.ID, id.pricelist_comments AS 'Comments', id.special_price, id.special_end, id.new_price, id.list_price, id.new_cost, id.internal_comments, id2.pricelist_comments AS ListComments, 
  d.pricelist AS PriceList 
  FROM Items i 
  LEFT JOIN PriceLevel_data id2 ON id2.ItemId = i.ID AND PriceLevelID = 1
  LEFT JOIN PriceLevelPerItem pi ON pi.PriceLevelPerItemRet_ItemRef_ListID = i.ID AND pi.".$sqlPricelvl." 
  LEFT JOIN PriceLevels pl ON pl.".$sqlPricelvlID." 
  LEFT JOIN PriceLevel_data id ON id.ItemId = i.ID AND id.".($request->pricelevelID?$sqlPricelvl:"PriceLevelID = 1")." 
  LEFT JOIN items_data d ON d.ItemId = i.ID 
  LEFT JOIN 
(SELECT ItemId, SUM(il.ItemQuantity) AS PastYearQtySales, SUBSTRING_INDEX( GROUP_CONCAT(CAST(ItemRate AS CHAR) ORDER BY Date DESC), ',', 1 ) As LastPaid FROM InvoiceLineItems il INNER JOIN Customers c ON c.ID = il.CustomerId ";

        $is_customerType = 0;
        if($request->customerID) {
            foreach ($CustomerTypes AS $CustomerType) {
                if ($request->customerType == $CustomerType->Name) {
                    $is_customerType = 1;
                }
            }

            if ($is_customerType) {
                $ItemsSQL = $ItemsSQL." WHERE c.Type = '".$request->customerType."' ";
            } else {
                $ItemsSQL = $ItemsSQL." WHERE c.ID = '".$request->customerID."' ";
            }
        }

        $ItemsSQL = $ItemsSQL." AND il.ItemQuantity > 0 AND il.Date >= DATE_SUB(NOW(),INTERVAL 1 YEAR) GROUP BY il.ItemId) sa ON sa.ItemId = i.ID 
WHERE Type = 'Inventory' AND i.IsActive = 1 {$onlysales} ".$onlylvl." ".$sqlpricelist." ORDER BY Description ASC, KGUnit ASC";

        $items = DB::connection('qbdb')
            ->select($ItemsSQL);

        $SettlementDisc = 0;

        $thecustomer = array();

        if($request->customerID) {
            if ($thecustomer = DB::connection('qbdb')
                ->select("SELECT JSON_UNQUOTE(CustomFields->'$.perc_off_statement') AS SettlementDisc, settlement_discount, rands_per_kg, discount_perc, marketing_perc FROM Customers c LEFT JOIN customers_data cd ON cd.CustomersId = c.ID WHERE ID = '".$request->customerID."'")) {
                $thecustomer = $thecustomer[0];

                $SettlementDisc = $thecustomer->SettlementDisc;
            }
        }

        return view('backend.pricing.index')
            ->with(compact('items', 'pricelevels', 'SettlementDisc', 'customers', 'request', 'thecustomer', 'thepricelvl', 'CustomerTypes'));
    }

    public function specials() {

        $items = DB::connection('qbdb')
            ->select("SELECT ID, Description, d.pricelist, PreferredVendor, id.special_price, id.special_end, sa.PastYearQtySales, JSON_UNQUOTE(CustomFields->'$.Unit') AS unit, JSON_UNQUOTE(CustomFields->'$.KgWeight') AS weight 
FROM Items i 
 LEFT JOIN PriceLevel_data id ON id.ItemId = i.ID AND id.PriceLevelID = 1
 LEFT JOIN items_data d ON d.ItemId = i.ID
 LEFT JOIN 
(SELECT ItemId, SUM(il.ItemQuantity) AS PastYearQtySales FROM InvoiceLineItems il WHERE il.ItemQuantity > 0 AND il.TimeCreated >= DATE_SUB(NOW(),INTERVAL 6 Month) GROUP BY il.ItemId) sa ON sa.ItemId = i.ID
  WHERE id.special_end >= NOW() AND isActive = 1 ORDER BY Description ASC");

        return view('backend.pricing.specials')
            ->with(compact('items'));
    }
}