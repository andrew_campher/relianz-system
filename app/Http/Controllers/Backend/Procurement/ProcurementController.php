<?php

namespace App\Http\Controllers\Backend\Procurement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class ProcurementController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $getDate = date("Y-m-d");

        if($request->show_date) {
            $getDate = $request->show_date;
        }

        $orders = DB::connection('qbdb')
            ->select("SELECT MAX(s.CustomerName) AS Customer, MAX(s.ID) AS SalesOrderID, MAX(JSON_UNQUOTE(c.CustomFields->'$.DeliveryArea')) AS Area, 
  CASE WHEN MAX(il.ItemQuantity) > 0 THEN ROUND(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*il.ItemQuantity)) 
   WHEN MAX(s.TransactionCount) = 0 THEN ROUND(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*si.ItemQuantity)) 
   WHEN MAX(s.TransactionCount = 1) THEN ROUND(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(si.ItemQuantity-si.ItemInvoicedAmount))) 
   WHEN MAX(si.ItemQuantity) > 0 AND MAX(si.IsManuallyClosed) = 0 AND MAX(si.IsFullyInvoiced) = 0 AND MAX(si.ItemInvoicedAmount) != MAX(si.ItemQuantity) THEN ROUND(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(si.ItemQuantity-si.ItemInvoicedAmount))) ELSE ROUND(SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(si.ItemQuantity-si.ItemInvoicedAmount))) END AS Weight, 
   ss.vehicle AS V, 
   s.ReferenceNumber AS 'SONumber', 
    CASE WHEN MAX(il.ItemQuantity) IS NULL AND MAX(s.IsManuallyClosed) = 1 THEN 'Closed' 
         WHEN ss.Status = 2 OR LOWER(MAX(s.Memo)) LIKE '%cancelled%' THEN 'Cancelled' 
         WHEN ss.Status = 1 OR LOWER(MAX(s.Memo)) LIKE '%complete%' THEN 'Complete' 
         WHEN ss.Status = 8 THEN 'Shipped' 
         WHEN ss.Status = 3 THEN 'Printed' 
         WHEN ss.Status = 4 THEN 'Invoiced' 
         WHEN ss.Status = 5 THEN 'Picking' 
         WHEN ss.Status = 6 THEN 'NO STOCK' 
         WHEN ss.Status = 7 THEN 'ON HOLD' 
         WHEN LOWER(MAX(c.Company)) LIKE '%on hold' THEN 'ON HOLD' 
         WHEN LOWER(MAX(c.Terms)) LIKE '%on hold%' THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%on hold%' THEN 'ON HOLD' 
         WHEN LOWER(MAX(s.Memo)) LIKE '%no stock%' THEN 'NO STOCK' 
         WHEN MAX(si.IsFullyInvoiced) = 1 THEN 'Invoiced' 
         WHEN MAX(s.TransactionCount) > 0 AND MAX(s.IsManuallyClosed) = 1 THEN 'Invoiced' 
         WHEN MAX(il.ItemQuantity) > 0 THEN 'Invoiced' 
         ELSE 'Picking' END AS Status, 
     CASE WHEN LOWER(MAX(JSON_UNQUOTE(s.CustomFields->'$.Other'))) LIKE '%col%' OR MAX(JSON_UNQUOTE(c.CustomFields->'$.Collection')) != '' THEN 'COLLECTION' ELSE 'DELIVERY' END AS Type, 
    CASE WHEN LOWER(MAX(s.Memo)) LIKE '%urgent%' THEN 'URGENT' ELSE '' END AS Urgent, 
       MAX(JSON_UNQUOTE(s.CustomFields->'$.Other')) AS User, 
      MAX(c.Terms) AS Terms, 
      MAX(JSON_UNQUOTE(c.CustomFields->'$.COA')) AS COA, 
        MAX(s.TimeCreated) As Created, MAX(s.Memo) AS Notes, MAX(s.TransactionCount) AS TransCount
FROM SalesOrders s 
LEFT JOIN SalesOrdersStatus ss ON ss.SalesOrderNumber = s.ReferenceNumber 
LEFT JOIN SalesOrderLineItems si ON si.SalesOrderId = s.ID AND si.ItemQuantity > 0  
LEFT JOIN Items i ON i.ID = si.ItemId 
LEFT JOIN Customers c ON c.ID = s.CustomerId 
LEFT JOIN SalesOrderLinkedTransactions st ON st.SalesOrderId = s.ID  
LEFT JOIN InvoiceLineItems il ON st.TransactionReferenceNumber = il.ReferenceNumber AND st.TransactionDate = il.Date AND il.ItemId = si.ItemId AND TransactionType = 'Invoice' AND il.ShipDate = s.ShipDate AND il.ItemQuantity <= si.ItemInvoicedAmount AND il.CustomerId = st.CustomerId  
WHERE s.ShipDate = '".$getDate." 00:00:00' AND LOWER(s.CustomerName) NOT LIKE '%private%' GROUP BY s.ReferenceNumber, s.CustomerId ORDER BY s.CustomerName ASC, s.ReferenceNumber ASC");

        /**
         * Create weight in area array
         */
        $order_weight = array();

        foreach ($orders AS $order) {
            if ($order->Type == "COLLECTION") {
                $order->Area = 'COL';
            }

            $order_weight[$order->SONumber][$order->Customer][$order->Area] = $order->Weight;

        }

        /**
         * Create Orders Array in Areas
         */
        $area_orders = array();
        $collection_orders = array();

        //Count Orders
        $totalOrders = count($orders);
        $totalWeight = 0;

        foreach ($orders AS $order) {

            if($order->Type == "COLLECTION") {
                $collection_orders['orders'][] = $order;
                $collection_orders['areas']['COL'] = 1;
                continue;
            }

            $totalWeight += $order->Weight;

            switch ($order->Area) {
                case "KG1":
                case "MG1":
                    $area_orders[1]['areas'][$order->Area] = 1;
                    $area_orders[1]['orders'][] = $order;
                    break;
                case "S01":
                case "S02":
                case "S03":
                    $area_orders[2]['areas'][$order->Area] = 1;
                    $area_orders[2]['orders'][] = $order;
                    break;
                case "N01":
                case "N02":
                case "N03":
                    $area_orders[3]['areas'][$order->Area] = 1;
                    $area_orders[3]['orders'][] = $order;
                    break;
                case "M01":
                case "M02":
                case "M03":
                    $area_orders[4]['areas'][$order->Area] = 1;
                    $area_orders[4]['orders'][] = $order;
                    break;
                case "E01":
                case "E02":
                case "E03":
                    $area_orders[5]['areas'][$order->Area] = 1;
                    $area_orders[5]['orders'][] = $order;
                    break;
                case "GRDN":
                    $area_orders[6]['areas'][$order->Area] = 1;
                    $area_orders[6]['orders'][] = $order;
                    break;
                case "JHB":
                case "ITT":
                case "COURIER":
                    $area_orders[7]['areas'][$order->Area] = 1;
                    $area_orders[7]['orders'][] = $order;
                    break;
            }

        }

        #dd($area_orders);

        return view('backend.Procurement.index')
            ->with(compact('orders', 'area_orders', 'collection_orders', 'getDate', 'order_weight', 'totalOrders', 'totalWeight'));
    }
}