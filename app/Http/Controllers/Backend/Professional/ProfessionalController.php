<?php

namespace App\Http\Controllers\Backend\Professional;

use App\Http\Requests\Backend\Common\ManageListRequest;
use App\Http\Requests\Backend\Professional\StoreProfessionalRequest;
use App\Models\Access\User\User;
use App\Models\Category\Category;
use App\Models\Service\Service;
use App\Repositories\Backend\Professional\ProfessionalRepository;
use App\Http\Requests\Backend\Professional\ManageProfessionalRequest;
use App\Models\Professional\Professional;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Services\Images\Images;
use App\Helpers\Helper;

class ProfessionalController extends Controller
{

    protected $professionals;

    /**
     * @param ProfessionalRepository $professionals
     */
    public function __construct(ProfessionalRepository $professionals)
    {
        $this->professionals = $professionals;
    }

    //
    public function index(Request $request)
    {
        if ($request->input('q')) {
            $professionals = Professional::where('title','LIKE','%'.$request->input('q').'%')->orWhere('company_email','LIKE','%'.$request->input('q').'%')->orWhere('notify_email','LIKE','%'.$request->input('q').'%')->orderBy('title', 'asc')->paginate(30);

            $professionals->appends(['q' => $request->input('q')]);
        } else {
            $professionals = Professional::paginate(20);
        }

        return view('backend.professional.index')
            ->with(compact('professionals'));
    }

    public function action(ManageListRequest $request)
    {
        $professionals = Professional::find($request->selected);
        switch ($request->action) {
            case "delete":
                $this->destroy($professionals, $request);
                break;
        }
    }

    public function show(Professional $professional, ManageProfessionalRequest $request) {

        return view('backend.professional.show')
            ->withProfessional($professional);
    }

    public function edit(Professional $professional, ManageProfessionalRequest $request)
    {
        $users = User::orderBy('name')->get()->pluck('name', 'id');

        #Get service list
        $users = User::orderBy('name')->get()->pluck('name', 'id');
        $servicelist = Category::servicelist();

        return view('backend.professional.edit')
            ->withProfessional($professional->load('branches'))
            ->with(compact('servicelist', 'users'));
    }

    public function create(ManageProfessionalRequest $request)
    {

        #Get service list
        $users = User::orderBy('name')->get()->pluck('name', 'id');
        #Fill service list
        $servicelist = Category::servicelist();

        return view('backend.professional.edit')
            ->with(compact('servicelist', 'users'));
    }

    public function update(Professional $professional, ManageProfessionalRequest $request)
    {

        $data = $request->all();

        if ($imagename = Images::upload($request, ['logo', $professional->getTable()])) {
            $data['logo'] = $imagename['image'];
        } else {
            #If no image then exclude image from update value array
            unset($data['logo']);
        }

        $this->professionals->update($professional, ['data' => $data]);
        return redirect()->back()->withFlashSuccess('Update success');
    }


    public function quickStore(Request $request)
    {

        $data = $request->all();

        $data['company_email'] = $data['branch']['notify_email'];
        $data['telephone'] = $data['branch']['telephone'];

        #past request data to save
        $this->professionals->create($data);
        return redirect()->back()->withFlashSuccess('Professional QUICK Created');
    }

    public function store(StoreProfessionalRequest $request)
    {

        $data = $request->all();

        if ($imagename = Images::upload($request, ['logo', (new Professional)->getTable()])) {
            $data['logo'] = $imagename['image'];
        }

        #past request data to save
        $this->professionals->create($data);
        return redirect()->route('admin.professional.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Professional $professional, ManageProfessionalRequest $request)
    {
        $this->professionals->delete($professional);
        return redirect()->route('admin.professional.deleted')->withFlashSuccess('Item Deleted');
    }
}
