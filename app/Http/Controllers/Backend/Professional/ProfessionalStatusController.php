<?php

namespace App\Http\Controllers\Backend\Professional;

use App\Events\Common\Professional\ProfessionalApproved;
use App\Models\Professional\Professional;
use App\Http\Controllers\Controller;
use App\Models\Service\Service;
use App\Repositories\Backend\Professional\ProfessionalRepository;
use App\Http\Requests\Backend\Professional\ManageProfessionalRequest;
use Illuminate\Support\Facades\DB;

/**
 * Class ProfessionalStatusController
 */
class ProfessionalStatusController extends Controller
{
	/**
	 * @var ProfessionalRepository
	 */
	protected $professionals;

	/**
	 * @param ProfessionalRepository $professionals
	 */
	public function __construct(ProfessionalRepository $professionals)
	{
		$this->professionals = $professionals;
	}

	public function removeService(Professional $professional, Service $service) {

        if (DB::table('professional_service')->where('professional_id', $professional->id)->where('service_id', $service->id)->delete()) {
            return redirect()->back()->withFlashSuccess('Professional removed from that service');
        } else {
            return redirect()->back()->withFlashWarning('Did not remove that professional from service');
        }
    }

    /**
     * @param ManageProfessionalRequest $request
     * @return mixed
     */
    public function getUnapproved(ManageProfessionalRequest $request)
    {
        $professionals = Professional::where('approved', 0)->paginate(30);
        return view('backend.professional.index')
            ->with(compact('professionals'));
    }

	/**
	 * @param ManageProfessionalRequest $request
	 * @return mixed
	 */
	public function getDeactivated(ManageProfessionalRequest $request)
	{
        $professionals = Professional::where('status', 0)->paginate(30);
        return view('backend.professional.index')
            ->with(compact('professionals'));
	}

	/**
	 * @param ManageProfessionalRequest $request
	 * @return mixed
	 */
	public function getDeleted(ManageProfessionalRequest $request)
	{
		return view('backend.professional.deleted');
	}

	/**
	 * @param Professional $user
	 * @param $status
	 * @param ManageProfessionalRequest $request
	 * @return mixed
	 */
	public function mark(Professional $professional, $status, ManageProfessionalRequest $request)
	{
		$this->professionals->mark($professional, $status);
		return redirect()->route($status == 1 ? "admin.professional.index" : "admin.professional.deactivated")->withFlashSuccess(trans('alerts.backend.general.updated'));
	}

    public function approve(Professional $professional, $approved, ManageProfessionalRequest $request)
    {
        $this->professionals->approve($professional, $approved);

        return redirect()->route($approved == 1 ? "admin.professional.index" : "admin.professional.unapproved")->withFlashSuccess(trans('alerts.backend.general.updated'));
    }

    public function verify(Professional $professional, $verified)
    {
        $this->professionals->verify($professional, $verified);

        return redirect()->back()->withFlashSuccess('Professional was marked as Verified');
    }

	/**
	 * @param Professional $deletedProfessional
	 * @param ManageProfessionalRequest $request
	 * @return mixed
	 */
	public function delete(Professional $deletedProfessional, ManageProfessionalRequest $request)
	{
		$this->professionals->forceDelete($deletedProfessional);
		return redirect()->route('admin.professional.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
	}

	/**
	 * @param Professional $deletedProfessional
	 * @param ManageProfessionalRequest $request
	 * @return mixed
	 */
	public function restore($deletedProfessional, ManageProfessionalRequest $request)
	{
	    #User component has a special RouteProfessionalProvider to make this easier...
        $item = Professional::withTrashed()->where('id',$deletedProfessional)->first();
		$this->professionals->restore($item);
		return redirect()->route('admin.professional.index')->withFlashSuccess(trans('alerts.backend.general.restored'));
	}
}