<?php

namespace App\Http\Controllers\Backend\Professional;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Professional\ProfessionalRepository;
use App\Http\Requests\Backend\Professional\ManageProfessionalRequest;

/**
 * Class UserTableController
 */
class ProfessionalTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $professional;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(ProfessionalRepository $professionals)
	{
		$this->professionals = $professionals;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageProfessionalRequest $request) {
	    if (!isset($request->approved))
            $request->approved = 1;

		return Datatables::of($this->professionals->getForDataTable($request->status, $request->trashed, $request->approved))
            ->addColumn('actions', function($professional) {
                return $professional->action_buttons;
            })
            ->addColumn('status', function($professional) {
                return $professional->status_label;
            })
            ->addColumn('image_html', function($professional) {
                return $professional->image_html;
            })
			->withTrashed()
			->make(true);
	}
}