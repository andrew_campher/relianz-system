<?php

namespace App\Http\Controllers\Backend\Professional;

use App\Http\Requests\Backend\Common\ManageListRequest;
use App\Models\Access\User\User;
use App\Models\Professional\Professional;
use App\Models\ProjectReview\ProjectReview;
use App\Models\Service\Service;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\ProjectReview\ProjectReviewRepository;
use Illuminate\Http\Request;

class ProjectReviewController extends Controller
{

    protected $project_reviews;

    /**
     * ProjectReviewController constructor.
     * @param ProjectReviewRepository $project_reviews
     */
    public function __construct(ProjectReviewRepository $project_reviews)
    {
        $this->project_reviews = $project_reviews;
    }

    //
    public function index()
    {
        $project_reviews = ProjectReview::paginate(15);
        return view('backend.project_review.index')
            ->with(compact('project_reviews'));
    }

    public function getUnapproved()
    {
        $project_reviews = ProjectReview::where('status', 0)->paginate(30);
        return view('backend.project_review.index')
            ->with(compact('project_reviews'));
    }

    public function action(ManageListRequest $request)
    {

        $project_reviews = ProjectReview::find($request->selected);
        switch ($request->action) {
            case "delete":
                $this->destroy($project_reviews, $request);
            break;
        }
    }

    public function show(ProjectReview $project_review, Request $request) {
        return view('backend.project_review.show')
            ->withProjectReview($project_review);
    }

    public function edit(ProjectReview $project_review, ManageProjectReviewRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');

        return view('backend.project_review.edit')
            ->withProjectReview($project_review)
            ->with(compact('users','professionals'));
    }

    public function create(ManageProjectReviewRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');
        $services = Service::orderBy('title')->get()->pluck('title', 'id');

        return view('backend.project_review.edit')
            ->with(compact('users','services', 'professionals'));
    }

    public function update(ProjectReview $project_review, ManageProjectReviewRequest $request)
    {

        $data = $request->all();

        $this->project_reviews->update($project_review, ['data' => $data]);
        return redirect()->route('admin.project_review.index')->withFlashSuccess('Update success');
    }


    public function store(StoreProjectReviewRequest $request)
    {

        $data = $request->all();

        #past request data to save
        if ($project_review = $this->project_reviews->create($data)) {

        }
        return redirect()->route('admin.project_review.index')->withFlashSuccess('Item Created');
    }

    public function destroy(ProjectReview $project_review, ManageProjectReviewRequest $request)
    {
        $this->project_reviews->delete($project_review);
        return redirect()->route('admin.project_review.deleted')->withFlashSuccess('Item Deleted');
    }

    public function approve(ProjectReview $project_review, $approved)
    {
        $data['approved'] = $approved;
        $this->project_reviews->approve($project_review, $data);



        return redirect()->route($approved == 1 ? "admin.project_review.index" : "admin.project_review.unapproved")->withFlashSuccess('Review Approved Success');
    }


}
