<?php

namespace App\Http\Controllers\Backend\Project;

use App\Http\Requests\Backend\Common\ManageListRequest;
use App\Http\Requests\Backend\Project\StoreProjectRequest;
use Illuminate\Http\Request;
use App\Models\Access\User\User;
use App\Models\Category\Category;
use App\Models\Professional\Professional;
use App\Models\ProfessionalBranch\ProfessionalBranch;
use App\Models\Service\Service;
use App\Notifications\Professional\NewProjectAlert;
use App\Repositories\Backend\Project\ProjectRepository;
use App\Http\Requests\Backend\Project\ManageProjectRequest;
use App\Models\Project\Project;
use App\Services\MediaManager\MediaManager;
use App\Http\Controllers\Controller;
use App\Services\Professionals\ProfessionalsService;

class ProjectController extends Controller
{

    protected $projects;

    /**
     * @param ProjectRepository $projects
     */
    public function __construct(ProjectRepository $projects)
    {
        $this->projects = $projects;
    }

    //
    public function index($status = 1)
    {
        $projects = Project::with('user', 'service');
        if (!is_null($status)) {
            $projects = $projects->where('status', $status);
        }
        $projects = $projects->paginate(30);

        #Pending Project count
        $pending_projects_count = Project::where('status', 0)->count();

        return view('backend.project.index')
            ->with(compact('projects', 'pending_projects_count', 'status'));
    }


    public function resendAlert(Project $project, Request $request)
    {
        if (!$project->status)
            return redirect()->route('admin.project.show',$project->id )->withFlashDanger('Project not approved yet');


        if ($request->input('selected') && is_array($request->input('selected'))) {

            $answers = $project->getAnswers();

            foreach ($request->input('selected') AS $branch_id) {

                $branch = ProfessionalBranch::find($branch_id)->load('professional');
                /**
                 * DO NOT send to same mail twice
                 */
                if (!isset($already[$branch->notify_email])) {
                    $already[$branch->notify_email] = true;
                } else {
                    continue;
                }

                $branch->notify(new NewProjectAlert($project, $branch, $answers));
            }

            return redirect()->route('admin.project.show',$project->id )->withFlashSuccess('Project alert notification was resent');
        }
    }

    public function action(ManageListRequest $request)
    {

        $projects = Project::find($request->selected);
        switch ($request->action) {
            case "delete":
                $this->destroy($projects, $request);
                break;
            case "Close":
                $this->destroy($projects, $request);
                break;
        }
    }

    public function show(Project $project, ManageProjectRequest $request) {

        $servicelist = Category::select('title','id','parent_id')->servicelist();

        $pros = ProfessionalsService::BestForJob($project);

        $answers = $project->getAnswers();

        return view('backend.project.show')
            ->with(compact('project','pros', 'answers', 'servicelist'));
    }

    public function edit(Project $project, ManageProjectRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');
        $services = Service::orderBy('title')->get()->pluck('title', 'id');

        $answers = $project->getAnswers();

        return view('backend.project.edit')
            ->withProject($project)
            ->with(compact('users','services', 'professionals', 'answers'));
    }

    public function create(ManageProjectRequest $request)
    {
        $professionals = Professional::orderBy('title')->get()->pluck('title', 'id');
        $users = User::orderBy('name')->get()->pluck('name', 'id');
        $services = Service::orderBy('title')->get()->pluck('title', 'id');

        return view('backend.project.edit')
            ->with(compact('users','services', 'professionals'));
    }

    public function update(Project $project, ManageProjectRequest $request)
    {

        $data = $request->all();

        $this->projects->update($project, ['data' => $data]);

        /**
         * SAVE FILES
         */
        MediaManager::uploadfiles($request, [get_class($project), $project->id]);

        return redirect()->route('admin.project.index')->withFlashSuccess('Update success');
    }

    public function store(StoreProjectRequest $request)
    {

        $data = $request->all();

        #past request data to save
        if ($project = $this->projects->create(['data' => $data])) {
            #Manage File Uploads
            MediaManager::uploadfiles($request, [get_class($project), $project->id]);
        }
        return redirect()->route('admin.project.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Project $project, ManageProjectRequest $request)
    {
        $this->projects->delete($project);
        return redirect()->route('admin.project.deleted')->withFlashSuccess('Item Deleted');
    }
}
