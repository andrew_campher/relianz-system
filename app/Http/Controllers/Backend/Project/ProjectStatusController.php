<?php

namespace App\Http\Controllers\Backend\Project;

use App\Models\Project\Project;
use App\Http\Controllers\Controller;
use App\Notifications\Professional\NewProjectAlert;
use App\Repositories\Backend\Project\ProjectRepository;
use App\Http\Requests\Backend\Project\ManageProjectRequest;
use App\Services\Professionals\ProfessionalsService;

/**
 * Class ProjectStatusController
 */
class ProjectStatusController extends Controller
{
	/**
	 * @var ProjectRepository
	 */
	protected $projects;

	/**
	 * @param ProjectRepository $projects
	 */
	public function __construct(ProjectRepository $projects)
	{
		$this->projects = $projects;
	}

	/**
	 * @param ManageProjectRequest $request
	 * @return mixed
	 */
	public function getDeactivated(ManageProjectRequest $request)
	{
		return view('backend.project.deactivated');
	}

    public function approve(Project $project, $approved, ManageProjectRequest $request)
    {
        /**
         * ALERT to ALL matching Professionals
         */

        if (!$project->service_id) {
            return redirect()->route('admin.project.index')->withFlashDanger('No service linked to this project');
        }

        if(!$approved) {
            $this->projects->unapprove($project);
            return redirect()->route('admin.project.index')->withFlashWarning('Project was Un-Approved');
        }

        $professional_branches = ProfessionalsService::BestForJob($project);
        $already = array();

        if ($professional_branches) {
            $this->projects->approve($project);

            $answers = $project->getAnswers();

            foreach ($professional_branches AS $branch) {
                /**
                 * DO NOT send to same mail twice
                 */
                if (!isset($already[$branch->notify_email])) {
                    $already[$branch->notify_email] = true;
                } else {
                    continue;
                }

                $branch->notify(new NewProjectAlert($project, $branch, $answers));
            }

            return redirect()->route('admin.project.index')->withFlashSuccess('Project was Approved');
        }

        return redirect()->route('admin.project.index')->withFlashDanger('No professionals matching this project!');

    }

	/**
	 * @param ManageProjectRequest $request
	 * @return mixed
	 */
	public function getDeleted(ManageProjectRequest $request)
	{
		return view('backend.project.deleted');
	}


	/**
	 * @param Project $deletedProject
	 * @param ManageProjectRequest $request
	 * @return mixed
	 */
	public function delete(Project $deletedProject, ManageProjectRequest $request)
	{
		$this->projects->forceDelete($deletedProject);
		return redirect()->route('admin.project.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
	}

	/**
	 * @param Project $deletedProject
	 * @param ManageProjectRequest $request
	 * @return mixed
	 */
	public function restore($deletedProject, ManageProjectRequest $request)
	{
	    #User component has a special RouteProjectProvider to make this easier...
        $item = Project::withTrashed()->where('id',$deletedProject)->first();
		$this->projects->restore($item);
		return redirect()->route('admin.project.index')->withFlashSuccess(trans('alerts.backend.general.restored'));
	}
}