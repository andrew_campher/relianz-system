<?php

namespace App\Http\Controllers\Backend\Project;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Project\ProjectRepository;
use App\Http\Requests\Backend\Project\ManageProjectRequest;

/**
 * Class UserTableController
 */
class ProjectTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $projects;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(ProjectRepository $projects)
	{
		$this->projects = $projects;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageProjectRequest $request) {
		return Datatables::of($this->projects->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($project) {
                return $project->action_buttons;
            })
            ->addColumn('image_html', function($project) {
                return $project->image_html;
            })
            ->addColumn('name', function($project) {
                return $project->user->name;
            })
			->withTrashed()
			->make(true);
	}
}