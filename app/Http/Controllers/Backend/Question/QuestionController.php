<?php

namespace App\Http\Controllers\Backend\Question;

use App\Http\Requests\Backend\Question\StoreQuestionRequest;
use App\Models\Category\Category;
use App\Models\QuestionOption\QuestionOption;
use App\Repositories\Backend\Question\QuestionRepository;
use App\Http\Requests\Backend\Question\ManageQuestionRequest;
use App\Models\Question\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class QuestionController extends Controller
{

    protected $question;

    /**
     * @param QuestionRepository $question
     */
    public function __construct(QuestionRepository $question)
    {
        $this->question = $question;
    }

    public function index(Request $request)
    {
        if ($request->input('q')) {
            $questions = Question::where('title','LIKE','%'.$request->input('q').'%')->orderBy('title', 'asc')->paginate(30);

            $questions->appends(['q' => $request->input('q')]);
        } else {
            $questions = Question::paginate(15);
        }

        return view('backend.question.index')
            ->with(compact('questions'));
    }

    public function action(ManageListRequest $request)
    {
        $professionals = Professional::find($request->selected);
        switch ($request->action) {
            case "delete":
                $this->destroy($professionals, $request);
                break;
        }
    }

    public function show(Question $question, ManageQuestionRequest $request) {

        $options = QuestionOption::where('question_id', $question->id)->get();

        return view('backend.question.show')
            ->withQuestion($question)
            ->withOptions($options);
    }

    public function edit(Question $question, ManageQuestionRequest $request)
    {
        $servicelist = Category::servicelist();

        return view('backend.question.edit')
            ->with(compact('question', 'servicelist'));
    }

    public function create(ManageQuestionRequest $request)
    {
        $servicelist = Category::servicelist();

        return view('backend.question.edit')
            ->with(compact('servicelist'));
    }

    public function update(Question $question, ManageQuestionRequest $request)
    {
        $this->question->update($question, ['data' => $request->all()]);
        return redirect()->route('admin.question.index')->withFlashSuccess('Update success');
    }

    public function store(StoreQuestionRequest $request)
    {
        $this->question->create(['data' => $request->all()]);
        return redirect()->route('admin.question.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Question $question, ManageQuestionRequest $request)
    {
        $this->question->delete($question);
        return redirect()->route('admin.question.deleted')->withFlashSuccess('Item Deleted');
    }
}
