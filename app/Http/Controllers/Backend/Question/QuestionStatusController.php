<?php

namespace App\Http\Controllers\Backend\Question;

use App\Models\Question\Question;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Question\QuestionRepository;
use App\Http\Requests\Backend\Question\ManageQuestionRequest;

/**
 * Class QuestionStatusController
 */
class QuestionStatusController extends Controller
{
	/**
	 * @var QuestionRepository
	 */
	protected $questions;

	/**
	 * @param QuestionRepository $questions
	 */
	public function __construct(QuestionRepository $questions)
	{
		$this->questions = $questions;
	}

    public function getDeactivated(ManageQuestionRequest $request)
    {
        return view('backend.question.deactivated');
    }

    public function getDeleted(ManageQuestionRequest $request)
    {
        return view('backend.question.deleted');
    }


    public function mark(Question $question, $status, ManageQuestionRequest $request)
    {
        $this->questions->mark($question, $status);
        return redirect()->route($status == 1 ? "admin.question.index" : "admin.question.deactivated")->withFlashSuccess('Items Updated');
    }

    public function delete(Question $deletedQuestion, ManageQuestionRequest $request)
    {
        $this->questions->forceDelete($deletedQuestion);
        return redirect()->route('admin.question.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
    }

    public function restore($deletedQuestion, ManageQuestionRequest $request)
    {
        $item = Question::withTrashed()->where('id',$deletedQuestion)->first();
        $this->questions->restore($item);

        return redirect()->route('admin.question.index')->withFlashSuccess('Items Restored');
    }
}