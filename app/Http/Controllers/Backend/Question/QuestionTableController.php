<?php

namespace App\Http\Controllers\Backend\Question;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Question\QuestionRepository;
use App\Http\Requests\Backend\Question\ManageQuestionRequest;

/**
 * Class UserTableController
 */
class QuestionTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $question;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(QuestionRepository $question)
	{
		$this->question = $question;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageQuestionRequest $request) {
		return Datatables::of($this->question->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($question) {
                return $question->action_buttons;
            })
			->withTrashed()
			->make(true);
	}
}