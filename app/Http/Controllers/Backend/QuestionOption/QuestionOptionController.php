<?php

namespace App\Http\Controllers\Backend\QuestionOption;

use App\Http\Requests\Backend\QuestionOption\StoreQuestionOptionRequest;
use App\Models\Question\Question;
use App\Models\QuestionOption\QuestionOption;
use App\Repositories\Backend\QuestionOption\QuestionOptionRepository;
use App\Http\Requests\Backend\QuestionOption\ManageQuestionOptionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class QuestionOptionController extends Controller
{

    protected $options;

    /**
     * @param QuestionOptionRepository $options
     */
    public function __construct(QuestionOptionRepository $options)
    {
        $this->options = $options;
    }

    public function edit(Question $question, QuestionOption $option, ManageQuestionOptionRequest $request)
    {
        return view('backend.question.option.edit')
            ->withoption($option)
            ->withquestion($question);
    }

    public function create(Question $question, ManageQuestionOptionRequest $request)
    {
        return view('backend.question.option.edit')
        ->withQuestion($question);
    }

    public function update(Question $question, QuestionOption $option, ManageQuestionOptionRequest $request)
    {
        $this->options->update($option, ['data' => $request->all()]);
        return redirect()->route('admin.question.show', array('question' => $request->question_id))->withFlashSuccess('Update success');
    }

    public function store(StoreQuestionOptionRequest $request)
    {
        $this->options->create(['data' => $request->all()]);
        return redirect()->route('admin.question.show', array('question' => $request->question_id))
            ->withFlashSuccess('Item Created');
    }

    public function destroy(Questions $question, QuestionOption $option, ManageQuestionOptionRequest $request)
    {
        $this->options->delete($option);
        return redirect()->route('admin.question.show', $question->id)->withFlashSuccess('Item Deleted');
    }
}
