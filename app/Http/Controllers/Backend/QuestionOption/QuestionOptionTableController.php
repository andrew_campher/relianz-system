<?php

namespace App\Http\Controllers\Backend\QuestionOption;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\QuestionOption\QuestionOptionRepository;
use App\Http\Requests\Backend\QuestionOption\ManageQuestionOptionRequest;

/**
 * Class UserTableController
 */
class QuestionOptionTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $questions;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(QuestionOptionRepository $question)
	{
		$this->question = $question;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageQuestionOptionRequest $request) {
		return Datatables::of($this->question->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($question) {
                return $question->action_buttons;
            })
			->withTrashed()
			->make(true);
	}
}