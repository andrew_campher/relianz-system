<?php

namespace App\Http\Controllers\Backend\Quote;

use App\Http\Requests\Backend\Quote\StoreQuoteRequest;
use App\Models\Category\Category;
use App\Repositories\Backend\Quote\QuoteRepository;
use App\Http\Requests\Backend\Quote\ManageQuoteRequest;
use App\Models\Quote\Quote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Quotes\Images\Images;
use App\Helpers\Helper;

class QuoteController extends Controller
{

    protected $quotes;

    /**
     * @param QuoteRepository $quotes
     */
    public function __construct(QuoteRepository $quotes)
    {
        $this->quotes = $quotes;
    }

    //
    public function index()
    {
        return view('backend.quote.index');
    }

    public function show(Quote $quote, ManageQuoteRequest $request) {
        return view('backend.quote.show')
            ->withQuote($quote);
    }

    public function edit(Quote $quote, ManageQuoteRequest $request)
    {
        $categories = Helper::make_tree_array(Category::get()->toArray(),'parent_id');

        return view('backend.quote.edit')
            ->withQuote($quote)
            ->withCategories($categories);
    }

    public function create(ManageQuoteRequest $request)
    {
        $categories = Helper::make_tree_array(Category::get()->toArray(),'parent_id');

        return view('backend.quote.create')
            ->withCategories($categories);
    }

    public function update(Quote $quote, ManageQuoteRequest $request)
    {

        $data = $request->all();

        if ($imagename = Images::upload($request, ['image', $quote->getTable()])) {
            $data['image'] = $imagename['image'];
        } else {
            #If no image then exclude image from update value array
            unset($data['image']);
        }

        $this->quotes->update($quote, ['data' => $data]);
        return redirect()->route('admin.quote.index')->withFlashSuccess('Update success');
    }

    public function store(StoreQuoteRequest $request)
    {

        $data = $request->all();

        if ($imagename = Images::upload($request, ['image', (new Quote)->getTable()])) {
            $data['image'] = $imagename['image'];
        }

        #past request data to save
        $this->quotes->create(['data' => $data]);
        return redirect()->route('admin.quote.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Quote $quote, ManageQuoteRequest $request)
    {
        $this->quotes->delete($quote);
        return redirect()->route('admin.quote.deleted')->withFlashSuccess('Item Deleted');
    }
}
