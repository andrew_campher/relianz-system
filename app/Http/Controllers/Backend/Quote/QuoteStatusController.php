<?php

namespace App\Http\Controllers\Backend\Quote;

use App\Models\Quote\Quote;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Quote\QuoteRepository;
use App\Http\Requests\Backend\Quote\ManageQuoteRequest;

/**
 * Class QuoteStatusController
 */
class QuoteStatusController extends Controller
{
	/**
	 * @var QuoteRepository
	 */
	protected $quotes;

	/**
	 * @param QuoteRepository $quotes
	 */
	public function __construct(QuoteRepository $quotes)
	{
		$this->quotes = $quotes;
	}

	/**
	 * @param ManageQuoteRequest $request
	 * @return mixed
	 */
	public function getDeactivated(ManageQuoteRequest $request)
	{
		return view('backend.quote.deactivated');
	}

	/**
	 * @param ManageQuoteRequest $request
	 * @return mixed
	 */
	public function getDeleted(ManageQuoteRequest $request)
	{
		return view('backend.quote.deleted');
	}

	/**
	 * @param Quote $user
	 * @param $status
	 * @param ManageQuoteRequest $request
	 * @return mixed
	 */
	public function mark(Quote $quote, $status, ManageQuoteRequest $request)
	{
		$this->quotes->mark($quote, $status);
		return redirect()->route($status == 1 ? "admin.quote.index" : "admin.quote.deactivated")->withFlashSuccess(trans('alerts.backend.general.updated'));
	}

	/**
	 * @param Quote $deletedQuote
	 * @param ManageQuoteRequest $request
	 * @return mixed
	 */
	public function delete(Quote $deletedQuote, ManageQuoteRequest $request)
	{
		$this->quotes->forceDelete($deletedQuote);
		return redirect()->route('admin.quote.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
	}

	/**
	 * @param Quote $deletedQuote
	 * @param ManageQuoteRequest $request
	 * @return mixed
	 */
	public function restore($deletedQuote, ManageQuoteRequest $request)
	{
	    #User component has a special RouteQuoteProvider to make this easier...
        $item = Quote::withTrashed()->where('id',$deletedQuote)->first();
		$this->quotes->restore($item);
		return redirect()->route('admin.quote.index')->withFlashSuccess(trans('alerts.backend.general.restored'));
	}
}