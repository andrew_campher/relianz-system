<?php

namespace App\Http\Controllers\Backend\Quote;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Quote\QuoteRepository;
use App\Http\Requests\Backend\Quote\ManageQuoteRequest;

/**
 * Class UserTableController
 */
class QuoteTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $quote;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(QuoteRepository $quotes)
	{
		$this->quotes = $quotes;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageQuoteRequest $request) {
		return Datatables::of($this->quotes->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($quote) {
                return $quote->action_buttons;
            })
            ->addColumn('image_html', function($quote) {
                return $quote->image_html;
            })
			->withTrashed()
			->make(true);
	}
}