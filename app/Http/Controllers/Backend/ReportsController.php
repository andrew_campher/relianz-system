<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Item\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class ReportsController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

    }

    public function monthtodate(Request $request) {

        $customers = DB::connection('qbdb')
            ->select("SELECT c.ID, c.Name, c.SalesRep, c.Type, CASE WHEN sa1.Sales IS NULL THEN 0-IFNULL(cm1.Credits,0) ELSE IFNULL(sa1.Sales,0)-IFNULL(cm1.Credits,0) END AS ThisYear, 
  CASE WHEN sa2.Sales IS NULL THEN 0-IFNULL(cm2.Credits,0) ELSE IFNULL(sa2.Sales,0)-IFNULL(cm2.Credits,0) END AS LastYear, 
  (IFNULL(sa1.Sales,0)-IFNULL(cm1.Credits,0))-(IFNULL(sa2.Sales,0)-IFNULL(cm2.Credits,0)) AS RandDiff, 
  CASE WHEN sa1.Sales IS NULL AND sa2.Sales IS NULL THEN 0 WHEN sa1.Sales IS NULL THEN -1 WHEN sa2.Sales IS NULL THEN 1 ELSE (sa1.Sales-sa2.Sales)/sa1.Sales END AS PercDiff,  
  IFNULL(prev1.Sales,0)-IFNULL(pc1.Credits,0) AS LastMonth, ThisYearGP, LastYearGP
  FROM Customers c 
      LEFT JOIN 
(SELECT CustomerId, SUM(ItemAmount) AS Sales, SUM((ItemRate-i.AverageCost)*ItemQuantity)/SUM(ItemAmount) AS ThisYearGP FROM InvoiceLineItems il INNER JOIN Items i ON i.ID = il.ItemId WHERE ItemName NOT LIKE '.Subtotal%' AND Date between DATE_FORMAT(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW())-1 DAY), '%Y-%m-%d') AND NOW() GROUP BY il.CustomerId) sa1 ON sa1.CustomerId = c.ID 
    LEFT JOIN 
(SELECT CustomerId, SUM(ItemAmount) AS Sales FROM InvoiceLineItems il WHERE ItemName NOT LIKE '.Subtotal%' AND Date between DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW())-1 DAY), INTERVAL 1 MONTH ), '%Y-%m-%d') AND DATE_SUB(NOW(), INTERVAL 1 MONTH) GROUP BY il.CustomerId) prev1 ON prev1.CustomerId = c.ID 
    LEFT JOIN 
(SELECT CustomerId, SUM(ItemAmount) AS Sales, SUM((ItemRate-i.AverageCost)*ItemQuantity)/SUM(ItemAmount) AS LastYearGP FROM InvoiceLineItems il INNER JOIN Items i ON i.ID = il.ItemId WHERE ItemName NOT LIKE '.Subtotal%' AND Date between DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW())-1 DAY), INTERVAL 1 YEAR), '%Y-%m-%d') AND DATE_SUB(NOW(), INTERVAL 1 YEAR) GROUP BY il.CustomerId) sa2 ON sa2.CustomerId = c.ID 
    LEFT JOIN 
(SELECT CustomerId, SUM(cm.Subtotal) AS Credits FROM CreditMemos cm WHERE Date between DATE_FORMAT(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW())-1 DAY), '%Y-%m-%d') AND NOW() GROUP BY cm.CustomerId) cm1 ON cm1.CustomerId = c.ID 
    LEFT JOIN 
(SELECT CustomerId, SUM(cm.Subtotal) AS Credits FROM CreditMemos cm WHERE Date between DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW())-1 DAY), INTERVAL 1 MONTH ), '%Y-%m-%d') AND DATE_SUB(NOW(), INTERVAL 1 MONTH) GROUP BY cm.CustomerId) pc1 ON pc1.CustomerId = c.ID 
    LEFT JOIN 
(SELECT CustomerId, SUM(cm.Subtotal) AS Credits FROM CreditMemos cm WHERE Date between DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW())-1 DAY), INTERVAL 1 YEAR), '%Y-%m-%d') AND DATE_SUB(NOW(), INTERVAL 1 YEAR) GROUP BY cm.CustomerId) cm2 ON cm2.CustomerId = c.ID 
WHERE sa1.Sales != 0 OR sa2.Sales != 0 OR cm1.Credits != 0 OR cm2.Credits != 0 ORDER BY RandDiff ASC");

        $ThisYear = 0;
        $LastYear = 0;
        $TotalGP = 0;
        $numCustomerSales = 0;

        $AvgGP = 0;

        if($customers) {
            foreach ($customers AS $customer) {
                if($customer->ThisYear) {
                    $numCustomerSales++;
                }
                $TotalGP += $customer->ThisYearGP;
                $ThisYear += $customer->ThisYear;
                $LastYear += $customer->LastYear;
            }
        }

        if($numCustomerSales && $TotalGP) {
            $AvgGP = $TotalGP/$numCustomerSales;
        }

        return view('backend.reports.monthtodate')
            ->with(compact('customers', 'LastYear', 'ThisYear', 'AvgGP'));
    }

    public function sales(Request $request) {

        $reps = DB::connection('qbdb')
            ->select("SELECT SalesRepEntityRef_FullName, Initial FROM SalesReps WHERE Initial != ''  ORDER BY SalesRepEntityRef_FullName ASC");

        $customertypes = DB::connection('qbdb')
            ->select("SELECT ID, `Name` FROM CustomerTypes WHERE IsActive = 1 ORDER BY Name ASC");



        $ThisYear = 0;
        $LastYear = 0;

        $customers = null;

        $repSQL = '';

        if ($request->rep) {
            $repSQL = "AND c.SalesRep = '".$request->rep."'";
        }

        $customertypeSQL = '';
        if($request->customertype) {
            $customertypeSQL = "AND c.Type = '".$request->customertype."'";
        }

        if ($request->fromDate && $request->toDate) {

            $customers = DB::connection('qbdb')
                ->select("SELECT c.ID, c.Name, c.SalesRep, c.Type, c.JobType AS JobType, CASE WHEN sa1.Sales IS NULL THEN 0-IFNULL(cm1.Credits,0) ELSE IFNULL(sa1.Sales,0)-IFNULL(cm1.Credits,0) END AS ThisYear, 
  CASE WHEN sa2.Sales IS NULL THEN 0-IFNULL(cm2.Credits,0) ELSE IFNULL(sa2.Sales,0)-IFNULL(cm2.Credits,0) END AS LastYear, 
  (IFNULL(sa1.Sales,0)-IFNULL(cm1.Credits,0))-(IFNULL(sa2.Sales,0)-IFNULL(cm2.Credits,0)) AS RandDiff,
  CASE WHEN sa1.Sales IS NULL AND sa2.Sales IS NULL THEN 0 WHEN sa1.Sales IS NULL THEN -1 WHEN sa2.Sales IS NULL THEN 1 ELSE (sa1.Sales-sa2.Sales)/sa1.Sales END AS PercDiff, 
   c.TimeCreated AS CustomerCreated, ThisYearGP, LastYearGP
  FROM Customers c 
      LEFT JOIN 
(SELECT CustomerId, SUM(ItemAmount) AS Sales, SUM((ItemRate-i.AverageCost)*ItemQuantity)/SUM(ItemAmount) AS ThisYearGP FROM InvoiceLineItems il INNER JOIN Items i ON i.ID = il.ItemId
        LEFT JOIN Customers c ON c.ID = il.CustomerId
        WHERE ItemName NOT LIKE '.Subtotal%' ".$repSQL." ".$customertypeSQL." AND Date >= '" . $request->fromDate . "' AND Date <= '" . $request->toDate . "' GROUP BY il.CustomerId) sa1 ON sa1.CustomerId = c.ID 
LEFT JOIN 
(SELECT CustomerId, SUM(ItemAmount) AS Sales, SUM((ItemRate-i.AverageCost)*ItemQuantity)/SUM(ItemAmount) AS LastYearGP FROM InvoiceLineItems il INNER JOIN Items i ON i.ID = il.ItemId 
        LEFT JOIN Customers c ON c.ID = il.CustomerId
        WHERE ItemName NOT LIKE '.Subtotal%' ".$repSQL." ".$customertypeSQL." AND Date >= DATE_SUB('" . $request->fromDate . "', INTERVAL 1 YEAR) AND Date <= DATE_SUB('" . $request->toDate . "', INTERVAL 1 YEAR) GROUP BY il.CustomerId) sa2 ON sa2.CustomerId = c.ID 
LEFT JOIN 
(SELECT CustomerId, SUM(cm.Subtotal) AS Credits FROM CreditMemos cm 
    LEFT JOIN Customers c ON c.ID = cm.CustomerId
    WHERE Date >= '" . $request->fromDate . "' AND Date <= '" . $request->toDate . "' ".$repSQL." ".$customertypeSQL." GROUP BY cm.CustomerId) cm1 ON cm1.CustomerId = c.ID 
LEFT JOIN 
(SELECT CustomerId, SUM(cm.Subtotal) AS Credits FROM CreditMemos cm 
    LEFT JOIN Customers c ON c.ID = cm.CustomerId
    WHERE Date >= DATE_SUB('" . $request->fromDate . "', INTERVAL 1 YEAR) AND Date <= DATE_SUB('" . $request->toDate . "', INTERVAL 1 YEAR) ".$repSQL." ".$customertypeSQL." GROUP BY cm.CustomerId) cm2 ON cm2.CustomerId = c.ID 
WHERE sa1.Sales > 0 OR sa2.Sales > 0 OR cm1.Credits > 0 OR cm2.Credits > 0 ".$repSQL." ".$customertypeSQL." ORDER BY RandDiff ASC");

            if($customers) {
                foreach ($customers AS $customer) {
                    $ThisYear += $customer->ThisYear;
                    $LastYear += $customer->LastYear;
                }
            }

        } else {
            $request->session()->put('flash_warning', 'Please select a date range');
        }

        return view('backend.reports.sales')
            ->with(compact('customers', 'LastYear', 'ThisYear', 'request', 'reps', 'customertypes'));
    }

    public function customers(Request $request) {

        $reps = DB::connection('qbdb')
            ->select("SELECT SalesRepEntityRef_FullName, Initial FROM SalesReps WHERE Initial != ''  ORDER BY SalesRepEntityRef_FullName ASC");

        $customertypes = DB::connection('qbdb')
            ->select("SELECT ID, `Name` FROM CustomerTypes WHERE IsActive = 1 ORDER BY Name ASC");

        if(!$request->toDate) {
            $request->toDate = date('Y-m-d');
        }

        if(!$request->fromDate) {
            $request->fromDate = date('Y-m-d', strtotime('Last Year'));
        }

        $InvDateFilter = "il.Date >= '" . $request->fromDate . "' AND il.Date <= '" . $request->toDate . "'";

        $DateFilter = "Date >= '" . $request->fromDate . "' AND Date <= '" . $request->toDate . "'";

        $repSQL = '';

        if ($request->rep) {
            $repSQL = "AND c.SalesRep = '".$request->rep."'";
        }

        $customertypeSQL = '';
        if($request->customertype) {
            $customertypeSQL = "AND c.Type = '".$request->customertype."'";
        }

        $query = "SELECT c.ID, c.Name, Type, b1.TotalAmount, b2.OrderCount, b1.AvgOrder, b1.GPmade, b1.TotalKg, pc1.CreditAmount, JSON_UNQUOTE(CustomFields->'$.DeliveryArea') AS 'DeliveryArea', CreditLimit, BillingCity, c.SalesRep, ca.ShipToAddr1, c.Terms, c.JobType, BillingAddress FROM Customers c 
  LEFT JOIN CustomerShippingAddresses ca ON ca.CustomerId = c.ID AND ShipToName = 'Ship To 1' 
  LEFT JOIN 
  (SELECT CustomerId, COUNT(CustomerId) AS OrderCount FROM Invoices 
WHERE {$DateFilter} GROUP BY CustomerId) b2 ON b2.CustomerId = c.ID 
LEFT JOIN 
(SELECT il.CustomerId, SUM((il.ItemRate-i.AverageCost)*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AS GPmade, 
SUM(il.ItemRate*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) TotalAmount, AVG(il.ItemRate*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AvgOrder,
SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AS TotalKg FROM InvoiceLineItems il 
 LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
LEFT JOIN Items i ON i.ID = il.ItemId
WHERE {$InvDateFilter} GROUP BY il.CustomerId) b1 ON b1.CustomerId = c.ID 
LEFT JOIN 
(SELECT CustomerId, SUM(cm.Subtotal) AS CreditAmount FROM CreditMemos cm WHERE {$DateFilter} GROUP BY cm.CustomerId) pc1 ON pc1.CustomerId = c.ID
  WHERE c.Name NOT LIKE 'Private%' AND c.IsActive = 1 {$customertypeSQL} {$repSQL} ORDER BY c.Name ASC";

            $customers = DB::connection('qbdb')
                ->select($query);

        return view('backend.reports.customers')
            ->with(compact('customers', 'request', 'reps', 'customertypes'));
    }


    public function items(Request $request) {

        if ($request->rep) {
            $repSQL = "AND c.SalesRep = '".$request->rep."'";
        }

        if(!$request->toDate) {
            $request->toDate = date('Y-m-d');
        }

        if(!$request->fromDate) {
            $request->fromDate = date('Y-m-d', strtotime('Last Year'));
        }

        $InvDateFilter = "il.Date >= '" . $request->fromDate . "' AND il.Date <= '" . $request->toDate . "'";

        $DateFilter = "AND Date >= '" . $request->fromDate . "' AND Date <= '" . $request->toDate . "'";


        $query = "SELECT i.ID, id.internal_notes, CASE WHEN i.Name LIKE '% CP' THEN 'CP' ELSE '' END AS LineType, i.QuantityOnHand, i.QuantityOnSalesOrder, id.pricelist, i.TimeCreated, id.stock_type, i.Description, b1.TotalAmount, b1.OrderCount, b1.AvgOrder, b1.GPmade, b1.TotalKg, pc1.CreditAmount, pc1.NumberCredits, b1.LastSale, NumberCustomrs 
FROM Items i 
LEFT JOIN items_data id ON id.ItemId = i.ID
LEFT JOIN 
(SELECT il.ItemId, SUM(il.ItemRate*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AS TotalAmount, GROUP_CONCAT(il.CustomerId) AS NumberCustomrs, SUM((il.ItemRate-i.AverageCost)*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AS GPmade, SUM(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AS TotalKg, AVG(JSON_UNQUOTE(i.CustomFields->'$.KgWeight')*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AS AvgKg, COUNT(il.ItemId) OrderCount, AVG(il.ItemRate*(il.ItemQuantity-IFNULL(cl.ItemQuantity,0))) AS AvgOrder, MAX(il.Date) AS LastSale FROM InvoiceLineItems il  
LEFT JOIN CreditMemoLineItems cl ON JSON_UNQUOTE(cl.CustomFields->'$.Other') = il.ReferenceNumber AND cl.ItemId = il.ItemId AND cl.CustomerId = il.CustomerId
LEFT JOIN Items i ON i.ID = il.ItemId 
WHERE il.ItemQuantity > 0 AND {$InvDateFilter} GROUP BY il.ItemId) b1 ON b1.ItemId = i.ID 
LEFT JOIN 
(SELECT ItemId, SUM(ItemRate*ItemQuantity) AS CreditAmount, COUNT(ItemId) AS NumberCredits FROM CreditMemoLineItems cm WHERE Memo NOT LIKE '%DUPLICATE%' {$DateFilter} GROUP BY cm.ItemId) pc1 ON pc1.ItemId = i.ID
  WHERE i.IsActive = 1 AND Type = 'Inventory' ORDER BY Description ASC";

        $items = DB::connection('qbdb')
            ->select($query);

        return view('backend.reports.items')
            ->with(compact('items', 'request'));
    }
}