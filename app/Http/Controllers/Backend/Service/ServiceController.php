<?php

namespace App\Http\Controllers\Backend\Service;

use App\Http\Requests\Backend\Service\StoreServiceRequest;
use App\Models\Category\Category;
use App\Models\Occupation\Occupation;
use App\Models\Question\Question;
use App\Repositories\Backend\Occupation\OccupationRepository;
use App\Repositories\Backend\Service\ServiceRepository;
use App\Http\Requests\Backend\Service\ManageServiceRequest;
use App\Models\Service\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Services\Images\Images;
use App\Helpers\Helper;

class ServiceController extends Controller
{

    protected $services;

    /**
     * @param ServiceRepository $services
     */
    public function __construct(ServiceRepository $services)
    {
        $this->services = $services;
    }

    //
    public function index(Request $request)
    {
        $category = array();

        if ($request->input('category_id')) {
            $catid = $request->input('category_id');

            $category = Category::find($catid);

            $services = Service::with('occupation')->whereHas('categories', function ($query) use ($catid) {
                $query->where('category_id', '=', $catid);
            })->orderBy('title', 'asc')->paginate(30);
        } elseif ($request->input('q')) {
            $services = Service::with('occupation')->where('title','LIKE','%'.$request->input('q').'%')->orderBy('title', 'asc')->paginate(30);
        } else {
            $services = Service::with('occupation')->paginate(30);
        }

        return view('backend.service.index')
            ->with(compact('services', 'category'));
    }

    public function show(Service $service, ManageServiceRequest $request) {

        $service_questions = $service->questions()->get();

        $questions = Question::all()->pluck('title', 'id');

        return view('backend.service.show')
            ->with(compact('service', 'service_questions', 'questions'));
    }

    public function edit(Service $service, ManageServiceRequest $request)
    {
        $categories = Helper::make_tree_array(Category::get()->toArray(),'parent_id');

        $occupations = Occupation::orderBy('title', 'asc')->get();

        return view('backend.service.edit')
            ->with(compact('service','categories', 'occupations'));
    }

    public function create(ManageServiceRequest $request)
    {
        $categories = Helper::make_tree_array(Category::get()->toArray(),'parent_id');

        $occupations = Occupation::orderBy('title', 'asc')->get();

        return view('backend.service.edit')
            ->with(compact('categories', 'occupations'));
    }

    public function update(Service $service, StoreServiceRequest $request)
    {

        $data = $request->all();

        if ($imagename = Images::upload($request, ['image', $service->getTable()])) {
            $data['image'] = $imagename['image'];
        } else {
            #If no image then exclude image from update value array
            unset($data['image']);
        }

        if ($data['new_occupation']) {
            $occ['title'] = $data['new_occupation'];
            $occupations = new OccupationRepository();
            $occupation = $occupations->create(['data' => $occ]);

            $data['occupation_id'] = $occupation->id;
        }

        $this->services->update($service, ['data' => $data]);
        return redirect()->route('admin.service.index')->withFlashSuccess('Update success');
    }

    public function store(StoreServiceRequest $request)
    {

        $data = $request->all();

        if ($imagename = Images::upload($request, ['image', (new Service)->getTable()])) {
            $data['image'] = $imagename['image'];
        }

        if ($data['new_occupation']) {
            $occ['title'] = $data['new_occupation'];
            $occupations = new OccupationRepository();
            $occupation = $occupations->create(['data' => $occ]);

            $data['occupation_id'] = $occupation->id;
        }

        #past request data to save
        $this->services->create(['data' => $data]);
        return redirect()->route('admin.service.index')->withFlashSuccess('Item Created');
    }

    public function destroy(Service $service, ManageServiceRequest $request)
    {
        $this->services->delete($service);
        return redirect()->route('admin.service.deleted')->withFlashSuccess('Item Deleted');
    }

}
