<?php

namespace App\Http\Controllers\Backend\Service;

use App\Http\Requests\Backend\Service\StoreServiceQuestionRequest;
use App\Models\Service\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ServiceQuestionController extends Controller
{

    /**
     *
     */
    public function __construct()
    {
    }

    public function store(StoreServiceQuestionRequest $request)
    {
        Service::find($request->service_id)->questions()->sync([$request->question_id], $detach = false);
        return redirect()->route('admin.service.show', $request->service_id)->withFlashSuccess('Question Added Successful');
    }

    public function destroy(Service $service, Request $request)
    {
        $service->questions()->detach($request->question_id);

        return redirect()->route('admin.service.show', $service->id)->withFlashSuccess('Item Deleted');
    }


}
