<?php

namespace App\Http\Controllers\Backend\Service;

use App\Models\Service\Service;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Service\ServiceRepository;
use App\Http\Requests\Backend\Service\ManageServiceRequest;

/**
 * Class ServiceStatusController
 */
class ServiceStatusController extends Controller
{
	/**
	 * @var ServiceRepository
	 */
	protected $services;

	/**
	 * @param ServiceRepository $services
	 */
	public function __construct(ServiceRepository $services)
	{
		$this->services = $services;
	}

	/**
	 * @param ManageServiceRequest $request
	 * @return mixed
	 */
	public function getDeactivated(ManageServiceRequest $request)
	{
		return view('backend.service.deactivated');
	}

	/**
	 * @param ManageServiceRequest $request
	 * @return mixed
	 */
	public function getDeleted(ManageServiceRequest $request)
	{
		return view('backend.service.deleted');
	}

	/**
	 * @param Service $user
	 * @param $status
	 * @param ManageServiceRequest $request
	 * @return mixed
	 */
	public function mark(Service $service, $status, ManageServiceRequest $request)
	{
		$this->services->mark($service, $status);
		return redirect()->route($status == 1 ? "admin.service.index" : "admin.service.deactivated")->withFlashSuccess(trans('alerts.backend.general.updated'));
	}

	/**
	 * @param Service $deletedService
	 * @param ManageServiceRequest $request
	 * @return mixed
	 */
	public function delete(Service $deletedService, ManageServiceRequest $request)
	{
		$this->services->forceDelete($deletedService);
		return redirect()->route('admin.service.deleted')->withFlashSuccess(trans('alerts.backend.general.deleted_permanently'));
	}

	/**
	 * @param Service $deletedService
	 * @param ManageServiceRequest $request
	 * @return mixed
	 */
	public function restore($deletedService, ManageServiceRequest $request)
	{
	    #User component has a special RouteServiceProvider to make this easier...
        $item = Service::withTrashed()->where('id',$deletedService)->first();
		$this->services->restore($item);
		return redirect()->route('admin.service.index')->withFlashSuccess(trans('alerts.backend.general.restored'));
	}
}