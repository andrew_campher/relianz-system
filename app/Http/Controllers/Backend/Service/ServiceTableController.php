<?php

namespace App\Http\Controllers\Backend\Service;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Service\ServiceRepository;
use App\Http\Requests\Backend\Service\ManageServiceRequest;

/**
 * Class UserTableController
 */
class ServiceTableController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $service;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(ServiceRepository $services)
	{
		$this->services = $services;
	}

	/**#
	 * @return mixed
	 */
	public function __invoke(ManageServiceRequest $request) {
		return Datatables::of($this->services->getForDataTable($request->get('status'), $request->get('trashed')))
            ->addColumn('actions', function($service) {
                return $service->action_buttons;
            })
            ->addColumn('image_html', function($service) {
                return $service->image_html;
            })
            ->addColumn('fee', function($service) {
                return Helper::formatPrice($service->fee);
            })
			->withTrashed()
			->make(true);
	}
}