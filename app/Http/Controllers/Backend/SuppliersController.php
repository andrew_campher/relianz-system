<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Supplier\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class SuppliersController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $suppliers = Supplier::with('data')->where("isActive",1);

        $suppliers = $suppliers->orderby("Name","ASC");
        if($request->search) {
            $suppliers = $suppliers->where('Name', 'LIKE', '%'.$request->search.'%');
        }

        $suppliers = $suppliers->paginate(50);

        return view('backend.suppliers.index')
            ->with(compact('suppliers', 'request'));
    }
    public function show(Supplier $supplier, Request $request) {

        return view('backend.suppliers.show')
            ->with(compact('supplier'));
    }

    public function report(Supplier $supplier, Request $request) {

        $SupplierArr = array();
        $curr = array();

        /*
         * TOTAL SALES
         */
        $supplier_alls = DB::connection('qbdb')
            ->select("SELECT i.Description AS ItemDescription, Qty, QuantityOnHand*PurchaseCost AS StockValue, ID AS ItemId, QuantityOnHand FROM Items i 
LEFT JOIN 
(SELECT ItemId, SUM(ItemQuantity) AS Qty FROM InvoiceLineItems WHERE ItemName NOT LIKE '.%' AND Date >= DATE_SUB(NOW(), INTERVAL 5 YEAR) GROUP BY ItemId) b3 ON b3.ItemId = i.ID 
WHERE i.PreferredVendorId = '".$supplier->ID."' ORDER BY Qty DESC");

        $totalStock = 0;

        if ($supplier_alls) {
            foreach ($supplier_alls AS $supplier_all) {
                $SupplierArr[$supplier_all->ItemDescription] = 1;

                $curr[$supplier_all->ItemDescription]['onhand'] = $supplier_all->QuantityOnHand;
                $curr[$supplier_all->ItemDescription]['t'] = $supplier_all->Qty;
                $curr[$supplier_all->ItemDescription]['ItemId'] = $supplier_all->ItemId;

                $totalStock += $supplier_all->StockValue;
            }
        }

        /*
         * THIS YEAR
         */
        $supplier_current = DB::connection('qbdb')
            ->select("SELECT MAX(i.Description) AS ItemDescription, SUM(il.ItemQuantity) AS Qty, MAX(ItemId) AS ItemId 
FROM InvoiceLineItems il 
INNER JOIN Items i ON i.ID = il.ItemId
INNER JOIN Vendors v ON v.ID = i.PreferredVendorId 
WHERE i.PreferredVendorId = '".$supplier->ID."' AND v.IsActive = 1 AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 YEAR), '%Y-%m-01') AND il.Date <= NOW() GROUP BY ItemId ORDER BY SUM(il.ItemQuantity) DESC");

        if ($supplier_current) {
            foreach ($supplier_current AS $supplier_curr) {
                $curr[$supplier_curr->ItemDescription]['y'] = $supplier_curr->Qty;
            }
        }

        /*
         * THIS MONTH SALES
         */
        $supplier_current = DB::connection('qbdb')
            ->select("SELECT MAX(ItemDescription) AS ItemDescription, SUM(Qty) AS Qty, MAX(ItemId) AS ItemId FROM (
SELECT MAX(i.Description) AS ItemDescription, SUM(il.ItemQuantity) AS Qty, MAX(ItemId) AS ItemId FROM InvoiceLineItems il 
INNER JOIN Items i ON i.ID = il.ItemId 
INNER JOIN Vendors v ON v.ID = i.PreferredVendorId 
WHERE i.PreferredVendorId = '".$supplier->ID."' AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(NOW(), '%Y-%m-01') AND il.Date <= NOW() GROUP BY ItemId 
UNION ALL 
SELECT MAX(i.Description) AS ItemDescription, 0-SUM(cl.ItemQuantity) AS Qty, MAX(ItemId) AS ItemId FROM CreditMemoLineItems cl 
INNER JOIN Items i ON i.ID = cl.ItemId 
INNER JOIN Vendors v ON v.ID = i.PreferredVendorId 
WHERE i.PreferredVendorId = '".$supplier->ID."' AND cl.Date >= DATE_FORMAT(NOW(), '%Y-%m-01') AND cl.Date <= NOW() GROUP BY i.PreferredVendorId
) g1 GROUP BY ItemDescription ORDER BY Qty DESC");

        if ($supplier_current) {
            foreach ($supplier_current AS $supplier_curr) {
                $curr[$supplier_curr->ItemDescription]['m'] = $supplier_curr->Qty;
            }
        }

        /*
         * PAST YEAR
         */
        $supplier_history = DB::connection('qbdb')
            ->select("SELECT MAX(ItemDescription) AS ItemDescription, Mnth, SUM(Qty) AS Qty, SUM(Amount) AS Amount, MAX(ItemId) AS ItemId FROM (
SELECT MAX(i.Description) AS ItemDescription, Month(il.Date) AS Mnth, SUM(il.ItemQuantity) AS Qty, SUM(i.PurchaseCost*il.ItemQuantity) AS Amount, MAX(ItemId) AS ItemId FROM InvoiceLineItems il 
INNER JOIN Items i ON i.ID = il.ItemId 
INNER JOIN Vendors v ON v.ID = i.PreferredVendorId 
WHERE i.PreferredVendorId = '".$supplier->ID."' AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 11 MONTH), '%Y-%m-01') AND il.Date <= NOW() GROUP BY ItemId, Month(il.Date)
UNION ALL 
SELECT MAX(i.Description) AS ItemDescription, Month(cl.Date) AS Mnth, 0-SUM(cl.ItemQuantity) AS Qty, 0-SUM(i.PurchaseCost*cl.ItemQuantity) AS Amount, MAX(ItemId) AS ItemId FROM CreditMemoLineItems cl 
INNER JOIN Items i ON i.ID = cl.ItemId 
INNER JOIN Vendors v ON v.ID = i.PreferredVendorId 
WHERE i.PreferredVendorId = '".$supplier->ID."' AND cl.Date >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 11 MONTH), '%Y-%m-01') AND cl.Date <= NOW() GROUP BY ItemId, Month(cl.Date)
) g1 GROUP BY ItemDescription, Mnth");

        $monthArray = array(
            date("n")=>0,
            date("n",strtotime(date('Y-m')." -1 Month"))=>0,
            date("n",strtotime(date('Y-m')." -2 Month"))=>0,
            date("n",strtotime(date('Y-m')." -3 Month"))=>0,
            date("n",strtotime(date('Y-m')." -4 Month"))=>0,
            date("n",strtotime(date('Y-m')." -5 Month"))=>0,
            date("n",strtotime(date('Y-m')." -6 Month"))=>0,
            date("n",strtotime(date('Y-m')." -7 Month"))=>0,
            date("n",strtotime(date('Y-m')." -8 Month"))=>0,
            date("n",strtotime(date('Y-m')." -9 Month"))=>0,
            date("n",strtotime(date('Y-m')." -10 Month"))=>0,
            date("n",strtotime(date('Y-m')." -11 Month"))=>0,
        );

        $monthTots = $monthArray;

        $totalRand = 0;

        if ($supplier_history) {
            foreach ($supplier_history AS $supplier_hist) {
                $curr[$supplier_hist->ItemDescription][$supplier_hist->Mnth] = $supplier_hist->Qty;
                $monthTots[$supplier_hist->Mnth] += $supplier_hist->Amount;

                $totalRand += $supplier_hist->Amount;
            }
        }

        /*
         * PAST 2 YEAR
         */
        $hist_yrs = DB::connection('qbdb')
            ->select("SELECT MAX(ItemDescription) AS ItemDescription, Mnth, SUM(Qty) AS Qty, SUM(Amount) AS Amount, MAX(ItemId) AS ItemId FROM (
SELECT MAX(i.Description) AS ItemDescription, Month(il.Date) AS Mnth, SUM(il.ItemQuantity) AS Qty, SUM(i.PurchaseCost*il.ItemQuantity) AS Amount, MAX(ItemId) AS ItemId 
FROM InvoiceLineItems il 
INNER JOIN Items i ON i.ID = il.ItemId 
INNER JOIN Vendors v ON v.ID = i.PreferredVendorId 
WHERE i.PreferredVendorId = '".$supplier->ID."' AND il.ItemName NOT LIKE '.%' AND il.Date >= DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 YEAR), INTERVAL 11 MONTH), '%Y-%m-01') AND il.Date <= DATE_SUB(NOW(), INTERVAL 1 YEAR) GROUP BY ItemId, Month(il.Date)
UNION ALL 
SELECT MAX(i.Description) AS ItemDescription, Month(cl.Date) AS Mnth, 0-SUM(cl.ItemQuantity) AS Qty, 0-SUM(i.PurchaseCost*cl.ItemQuantity) AS Amount, MAX(ItemId) AS ItemId 
FROM CreditMemoLineItems cl 
INNER JOIN Items i ON i.ID = cl.ItemId 
INNER JOIN Vendors v ON v.ID = i.PreferredVendorId 
WHERE i.PreferredVendorId = '".$supplier->ID."' AND cl.Date >= DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 YEAR), INTERVAL 11 MONTH), '%Y-%m-01') AND cl.Date <= DATE_SUB(NOW(), INTERVAL 1 YEAR) GROUP BY ItemId, Month(cl.Date)
) g1 GROUP BY ItemDescription, Mnth");
        $PrevMonthTots = $monthArray;


        if ($hist_yrs) {
            foreach ($hist_yrs AS $hist_yr) {
                $past[$hist_yr->ItemDescription][$hist_yr->Mnth] = $hist_yr->Qty;
                $PrevMonthTots[$hist_yr->Mnth] += $hist_yr->Amount;
            }
        }

        return view('backend.suppliers.report')
            ->with(compact('supplier','SupplierArr', 'totalRand', 'curr', 'bills', 'monthTots', 'PrevMonthTots', 'totalStock', 'past'));
    }
}