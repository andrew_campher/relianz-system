<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class ToolsController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

    }

    public function dropoffs(Request $request) {

        $reps = DB::connection('qbdb')
            ->select("SELECT SalesRepEntityRef_FullName, Initial FROM SalesReps WHERE Initial != ''  ORDER BY SalesRepEntityRef_FullName ASC");

        $customers = DB::connection('qbdb')
            ->select("SELECT ID, Name FROM Customers WHERE isActive = 1 ORDER BY Name ASC");

        //Years Back
        $years = $request->years?$request->years:1;

        $monthback = $request->years?date("Ym",strtotime("-".$request->years." Years")):date("Ym",strtotime("-".$years." Years"));

        $monthend = $request->monthend?date("Ym",$request->monthend):date("Ym",strtotime('Last Month'));


        $monthArray = array();
        for ($i = 0;$i <= $years*12; $i++) {
            $monthArray[date("Ym",strtotime('-'.$i.' months'))] = $i;
        }



        $dropline = array();
        $cnt = 1;

        $getQuery = "SELECT c.Name, i.Description, c.SalesRep, MAX(LatestSale) AS LastSale, MIN(ic.Monthdate) AS firstmonth, MAX(ic.Monthdate) AS lastmonth , SUM(ic.Qty*i.Price) AS SalesValue, COUNT(ic.Qty) AS OrderCount, ic.CustomerId, ic.ItemId, GROUP_CONCAT(ic.Monthdate) AS MonthSold, GROUP_CONCAT(ic.Qty) AS QtyMonthSold FROM `item_customer_sales_history` ic
LEFT JOIN Customers c ON c.ID = ic.CustomerId
LEFT JOIN Items i ON i.ID = ic.ItemId
LEFT JOIN 
(SELECT MAX(Monthdate) AS LatestSale, CustomerId, ItemId FROM item_customer_sales_history GROUP BY CustomerId, ItemId) t1 ON t1.CustomerId = ic.CustomerId AND t1.ItemId = ic.ItemId
WHERE ic.Monthdate < {$monthend} AND ic.Monthdate >= {$monthback} AND c.Name NOT LIKE 'Private %' AND t1.LatestSale < {$monthend} AND t1.LatestSale >= {$monthback} AND LatestSale IS NOT NULL";

        if ($request->rep) {
            $getQuery .= " AND c.SalesRep = '".$request->rep."' ";
        }

        if ($request->CustomerId) {
            $getQuery .= " AND ic.CustomerId = '".$request->CustomerId."' ";
        }

        $getQuery .= " AND c.IsActive = 1 
GROUP BY ic.CustomerId, ic.ItemId HAVING COUNT(ic.Qty) > 5
ORDER BY SUM(ic.Qty*i.Price) DESC";

        $drops = DB::connection('qbdb')
            ->select($getQuery);

        foreach ($drops AS $drop) {

            $qtysold = array();

            $months = explode(",",$drop->MonthSold);
            $qtysold = explode(",",$drop->QtyMonthSold);

            $totalmonths = count($months);

            $consec_nums = 0;
            $dropsmonths = 0;

            $lineMonth = array();

            $totmon = count($months);


            foreach ($months AS $key => $mnth) {
                $qtyarray[$drop->CustomerId][$drop->ItemId][$mnth] = $qtysold[$key];

                $lineMonth[$mnth] = 1;
            }

            $hasmnth = 0;

            foreach ($monthArray AS $mkey => $monthArr) {
                if(isset($lineMonth[$mkey])) {
                    $hasmnth++;
                }

                if(isset($lineMonth[date("Ym",strtotime($mkey.'01 +1 Month'))]) ) {

                    $consec_nums++;
                } else {
                    if ($consec_nums > 0) {
                        $dropsmonths++;
                    }
                }

                if($hasmnth == $totmon) {
                    break;
                }
            }

            $cnt++;

            if($consec_nums > 4 && $dropsmonths/$consec_nums <= 0.5) {
                $dropline[] = $drop;
            }

            //echo $totalmonths.' | '.$consec_nums.' - '.$dropsmonths.' | '.$drop->firstmonth.' -- '.$drop->Name.' > '.$drop->Description.'<br/>';

        }

        return view('backend.tools.drops')
            ->with(compact('dropline', 'monthArray', 'qtyarray', 'reps', 'request', 'customers'));
    }


    public function sittingstock(Request $request) {

        $monthsback = 6;
        if ($request->months) {
            $monthsback = $request->months;
        }

        $getQuery = "SELECT i.Description, id.Class, i.Price, i.PurchaseCost, LastPurchaseOrder, sa.LastSale, CASE WHEN i.QuantityOnHand < 0 THEN 0 ELSE i.QuantityOnHand END AS QuantityOnHand, FirstBackOrder, 
BackOrderQty, LastRecieve, 
i.QuantityOnOrder AS QuantityOnOrder, CASE WHEN i.QuantityOnOrder > 0 THEN DATE_FORMAT(OrderExpectedDate,'%d/%m/%Y') ELSE '' END AS ETA, i.ID as ItemId, 
id.procurement_notes 
FROM Items i 
LEFT JOIN items_data id ON id.ItemId = i.ID 
LEFT JOIN 
(SELECT ItemId, SUM(il.ItemQuantity) AS PastYearQtySales, MAX(il.Date) AS LastSale FROM InvoiceLineItems il WHERE il.ItemQuantity > 0 AND il.TimeCreated >= DATE_SUB(NOW(),INTERVAL 6 MONTH) GROUP BY il.ItemId) sa ON sa.ItemId = i.ID 
LEFT JOIN 
(SELECT bi.ItemId, VendorName, Pdate AS LastRecieve FROM 
   (SELECT ItemId, MAX(BillId) AS BillId, MAX(date) AS Pdate FROM BillLineItems WHERE ItemId != '' GROUP BY ItemId) AS bi 
 INNER JOIN Bills b ON bi.BillId = b.ID) b2 ON b2.ItemId = i.ID 
LEFT JOIN 
  (SELECT ItemId, MIN(s.Date) AS FirstBackOrder, SUM(si.ItemQuantity-si.ItemInvoicedAmount) AS BackOrderQty FROM SalesOrderLineItems si LEFT JOIN SalesOrders s ON s.ID = si.SalesOrderId WHERE si.ItemQuantity > si.ItemInvoicedAmount AND si.IsFullyInvoiced = 0 AND si.IsManuallyClosed = 0 AND si.ItemManuallyClosed = 0 AND s.IsManuallyClosed = 0 AND s.IsFullyInvoiced = 0 GROUP BY si.ItemId) so ON so.ItemId = i.ID 
LEFT JOIN 
(SELECT pi.ItemId, VendorName, Pdate AS LastPurchaseOrder, firstPdate AS FirstOpenPO, Terms AS PaymentTerms, TotalPurchased, MaxShortDelivered, TotalShortDelivered, OrderExpectedDate, Memo 
        FROM 
        (SELECT ItemId, MAX(pl.PurchaseOrderID) AS PurchaseOrderID, MIN(pl.date) AS firstPdate, MAX(pl.date) AS Pdate, MAX(pl.ExpectedDate) AS OrderExpectedDate, SUM(pl.ItemQuantity) as TotalPurchased, MAX(ItemQuantity-ItemReceivedQuantity) as     MaxShortDelivered, SUM(pl.ItemQuantity-pl.ItemReceivedQuantity) as TotalShortDelivered 
         FROM PurchaseOrderLineItems pl LEFT JOIN PurchaseOrders p ON pl.PurchaseOrderID = p.ID WHERE pl.ItemId != '' AND pl.IsManuallyClosed != 1 AND pl.IsFullyReceived != 1 AND pl.IsFullyReceived != 1 AND p.IsManuallyClosed != 1 GROUP BY ItemId) AS pi 
        INNER JOIN PurchaseOrders p ON pi.PurchaseOrderID = p.ID) p2 ON p2.ItemId = i.ID 
 WHERE i.QuantityOnHand > 0 AND b2.LastRecieve <= DATE_SUB(NOW(),INTERVAL ".$monthsback." MONTH) AND i.Type = 'Inventory' AND i.FullName NOT LIKE '% CP' ORDER BY LastRecieve ASC";

        $items = DB::connection('qbdb')
            ->select($getQuery);

        return view('backend.tools.sittingstock')
            ->with(compact('items', 'request'));
    }


}