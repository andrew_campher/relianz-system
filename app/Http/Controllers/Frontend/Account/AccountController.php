<?php

namespace App\Http\Controllers\Frontend\Account;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Account\StoreAccountRequest;
use App\Models\Account\Account;
use App\Models\Quote\Quote;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class AccountController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $account;

    /**
     * AccountController constructor.
     * @param AccountRepository $account
     */
    public function __construct() {

    }


    public function index(Account $account) {

        return view('frontend.account.index');
    }

    public function invoices(Account $account) {

        return view('frontend.account.invoices');
    }

    public function credits_purchase(Account $account, Request $request) {

        $quote = null;
        if (isset($request->quote)) {
            $quote = Quote::find($request->quote);
        }

        #Build up purchase options
        $purchase_options = Helper::CreditPackages($account->package);

        return view('frontend.account.credits_purchase')
            ->with(compact('purchase_options', 'quote'));
    }

    public function purchase(StoreAccountRequest $request)
    {
        $data = $request->all();

        return Redirect::back()->withFlashDanger('Unsuccesful - There was a problem creating this invoice');

    }



}