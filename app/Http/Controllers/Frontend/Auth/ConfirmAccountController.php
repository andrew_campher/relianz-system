<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Notifications\Frontend\Auth\ProClaimConfirmation;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

/**
 * Class ConfirmAccountController
 * @package App\Http\Controllers\Frontend\Auth
 */
class ConfirmAccountController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $user;

	/**
	 * ConfirmAccountController constructor.
	 * @param UserRepository $user
	 */
	public function __construct(UserRepository $user)
	{
		$this->user = $user;
	}

	/**
	 * @param $token
	 * @return mixed
	 */
	public function confirm($token)
	{
		if ($this->user->confirmAccount($token)) {
            return redirect()->route('frontend.auth.login')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.success'));
        } else {
            return redirect()->route('frontend.auth.login')->withFlashSuccess('Account already confirmed - please login below');
        }
	}

	/**
	 * @param $user
	 * @return mixed
	 */
	public function sendConfirmationEmail(User $user)
	{
        /**
         * CHECK if user wants to claim a PRofessional Account
         */
        if ($user->isRole('Professional') && $user->professional) {

            $user->notify(new ProClaimConfirmation($user->confirmation_code, $user->professional));
            return redirect()->route('frontend.auth.login')->withFlashSuccess('A confirmation email has been sent to the associated email address on file <b>"___'.substr($user->email, 3).'"</b> - please check SPAM folders and click the link within email.');

        } else {
            $user->notify(new UserNeedsConfirmation($user->confirmation_code));
            return redirect()->route('frontend.auth.login')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.resent'));
        }


	}
}