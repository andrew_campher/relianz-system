<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Helpers\Frontend\Auth\Socialite;
use App\Http\Controllers\Controller;
use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Requests\Frontend\Auth\RegisterProfessionalRequest;
use App\Models\Access\User\User;
use App\Models\Category\Category;
use App\Models\Professional\Professional;
use App\Repositories\Backend\Professional\ProfessionalRepository;
use App\Services\Images\Images;
use App\Services\MediaManager\MediaManager;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Repositories\Frontend\Access\User\UserRepository;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Frontend\Auth
 */
class RegisterController extends Controller
{
    use RegistersUsers;

	/**
	 * @var UserRepository
	 */
	protected $user;

	/**
	 * RegisterController constructor.
	 * @param UserRepository $user
	 */
	public function __construct(UserRepository $user)
	{
		// Where to redirect users after registering
		$this->redirectTo = route('public.index');

		$this->user = $user;
	}

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showRegistrationForm()
	{
		return view('frontend.auth.register')
            ->withSocialiteLinks((new Socialite())->getSocialLinks());
	}

    public function showProfessionalRegistrationForm()
    {
        $servicelist = Category::servicelist();

        $professional_reg = 1;

        return view('frontend.auth.register')
            ->withSocialiteLinks((new Socialite())->getSocialLinks())
            ->with(compact('servicelist', 'professional_reg'));
    }

	/**
	 * @param RegisterRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function register(RegisterRequest $request)
	{
        $data = $request->all();

        #Store Image
        if ($imagename = Images::upload($request, ['image', (new User())->getTable(), 'fit' => true])) {
            $data['image'] = $imagename['image'];
        }

		if (config('access.users.confirm_email')) {
			$user = $this->user->create($data);
			event(new UserRegistered($user));
			return redirect($this->redirectPath())->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
		} else {
			auth()->login($this->user->create($data));
			event(new UserRegistered(access()->user()));
			return redirect($this->redirectPath());
		}
	}


    /**
     * @param RegisterProfessionalRequest $request
     * @param ProfessionalRepository $professionals
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register_professional(RegisterProfessionalRequest $request, ProfessionalRepository $professionals)
    {

        $data = $request->except(['image', 'logo', 'file_resources']);
        #Store Image
        if ($imagename = Images::upload($request, ['image', (new User())->getTable(), 'fit' => true])) {

            $data = array_merge($data, $imagename);
        }

        #Professional logo
        if ($image_name = Images::upload($request, ['logo', (new Professional())->getTable()])) {
            $data = array_merge($data, $image_name);
        }

        $user = $this->user->create_professional_user($data);

        if ($user) {
            #create professional
            $data['user_id'] = $user->id;

            if ($professional = $professionals->create($data)) {
                #Handle Media for 'resources'
                MediaManager::uploadfiles($request, [get_class($professional), $professional->id, 'resources', 'user_id'=> $user->id]);
            }

            event(new UserRegistered($user));

            /*
             * Lets not login him yet... let him first be approved.
             */
            #auth()->login($user);

            return redirect(route('public.index'))->withFlashSuccess('Success! Congratulations your application has been submitted! It will be reviewed shortly.');

        }

        #Take to professional Dashboard
        return redirect(route('public.index'))->withFlashDanger('There was a issue submitting your application - please contact support.');
    }
}