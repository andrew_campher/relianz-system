<?php

namespace App\Http\Controllers\Frontend\Category;

use App\Repositories\Backend\Category\CategoryRepository;
use App\Models\Category\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryPublicController extends Controller
{

    protected $categories;

    /**
     * @param CategoryRepository $categories
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    public function services($category_slug)
    {
        $categories = Category::where('parent_id', 0)->where('status', 1)->orderBy('hits', 'desc')->limit(6)->get();

        $category = Category::where('slug', $category_slug)->with('subcategories.services')->first();

        if ($category) {
            Category::where('id', $category->id)->UpdateHits();
        }

        #var_dump($category);
        #dd($category);

        return view('public.category.services')
            ->with(compact('category', 'categories'));
    }

}
