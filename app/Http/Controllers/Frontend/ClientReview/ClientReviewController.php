<?php

namespace App\Http\Controllers\Frontend\ClientReview;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ClientReview\StoreClientReviewRequest;
use App\Models\ClientReview\ClientReview;
use App\Models\Quote\Quote;
use App\Repositories\Backend\ClientReview\ClientReviewRepository;
use Carbon\Carbon;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ClientReviewController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $client_reviews;

    /**
     * ClientReviewController constructor.
     * @param ClientReviewRepository $client_reviews
     */
    public function __construct(ClientReviewRepository $client_reviews) {
        $this->client_reviews = $client_reviews;
    }


    public function show(ClientReview $review) {

        #If read_at is NULL lets mark it as read.
        if ($review->project->user_id == access()->id() && !$review->read_at) {
            $review->read_at = Carbon::now();
            $review->save();

            #Fire read event
            event(new ClientReviewRead($review));
        }

        return view('frontend.review.show')
            ->with(compact('review'));
    }

    public function create(Quote $quote) {

        if ($quote->client_review)
            return redirect('/professional/dashboard')->withFlashDanger('Client review already found');

        if ($quote->user_id != access()->id())
            return redirect('/')->withFlashWarning('Your are not allowed to go there');

        return view('frontend.client_review.edit')
            ->with(compact('quote'));
    }

    /**
     * @param StoreClientReviewRequest $request
     * @param ClientReviewRepository $client_reviews
     * @return mixed
     */
    public function store(StoreClientReviewRequest $request)
    {
        $quote = Quote::find($request->input('quote_id'))->first();

        if ($quote->client_review)
            return redirect('/')->withFlashDanger('Client review already found');

        $data = $request->all();

        #past request data to save
        if ($client_review = $this->client_reviews->create($data)) {

        }

        return redirect('/professional/dashboard')->withFlashSuccess('Your review has been submitted');

    }

}