<?php

namespace App\Http\Controllers\Frontend\Comment;


use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Comment\StoreCommentRequest;
use App\Models\Comment\Comment;



/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class CommentController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /**
         * API is used for comments
         */
        //return view('frontend.comment.index');
    }


    public function show(Comment $comment) {

        /**
         * API is used for comments
         */
    }

    /**
     * @param StoreCommentRequest $request
     * @param $item_id
     * @return $this
     */
    public function store(StoreCommentRequest $request)
    {
        /**
         * API is used for comments
         */
    }

}