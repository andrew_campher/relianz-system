<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Location\Location;
use App\Models\Occupation\Occupation;
use App\Models\Service\Service;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller
{
	/**
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
        /**
         * Needed to logout a user when his logged in but his entry in users table has been deleted
         * TODO: find a CORE place to put this
         */
	    if (access() && !access()->user()) {
            auth()->logout();
        }

        $categories = Category::where('parent_id', 0)->where('status', 1)->orderBy('hits', 'desc')->limit(8)->get();

	    $occupations = Occupation::orderBy('hits', 'desc')->limit(20)->get();

        $cities = Location::where('type', 'administrative_area_level_3')->orderBy('hits', 'desc')->limit(52)->get();

		return view('public.index')
            ->with(compact('occupations', 'categories', 'cities'));
	}

}
