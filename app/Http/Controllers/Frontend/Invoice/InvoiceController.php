<?php

namespace App\Http\Controllers\Frontend\Invoice;

use App\Events\Common\Invoice\InvoiceRead;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Invoice\StoreInvoiceRequest;
use App\Models\Invoice\Invoice;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use Carbon\Carbon;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class InvoiceController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $invoice;

    /**
     * InvoiceController constructor.
     * @param InvoiceRepository $invoice
     */
    public function __construct(InvoiceRepository $invoice) {
        $this->invoice = $invoice;
    }

    public function index() {

        $invoices = Invoice::where('professional_id', access()->user()->professional->id)->orderBy('id', 'desc')->get();

        return view('frontend.invoice.index')
            ->with(compact('invoices'));
    }

    public function show(Invoice $invoice) {

        $container_class = 'fullwidth';

        return view('frontend.invoice.show')
            ->with(compact('invoice', 'container_class'));
    }

    public function download(Invoice $invoice) {

        $pdf = \PDF::loadView('pdfs.invoice', ['invoice' => $invoice]);
        return $pdf->setPaper('A4', 'portrait')->download('invoice-'.$invoice->id.'-'.$invoice->created_at->format(config('app.date.short')).'.pdf');
    }

}