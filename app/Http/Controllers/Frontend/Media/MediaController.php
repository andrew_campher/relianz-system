<?php

namespace App\Http\Controllers\Frontend\Media;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Media\ManageMediaRequest;
use App\Models\Media\Media;
use App\Repositories\Backend\Media\MediaRepository;
use Illuminate\Support\Facades\Request;

class MediaController extends Controller
{

    public function __construct(MediaRepository $media_rep)
    {
        $this->media_rep = $media_rep;
    }

    public function destroy(Media $media, ManageMediaRequest $request)
    {

        if ($this->media_rep->delete($media)) {
            return 1;
        }

    }

    public function show(Media $media, Request $request)
    {


        return view('frontend.media.show')
            ->with(compact('media'));

    }

}
