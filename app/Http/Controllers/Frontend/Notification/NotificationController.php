<?php

namespace App\Http\Controllers\Frontend\Notification;

use App\Http\Controllers\Controller;
use App\Models\Notification\Notification;

/**
 * Class AccountController
 * @package App\Http\Controllers\Frontend
 */
class NotificationController extends Controller
{

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function all()
	{
	    $notifications = Notification::where('notifiable_type', 'App\Models\Access\User\User')->where('notifiable_id', access()->id())->orderBy('created_at', 'desc')->paginate(15);

		return view('frontend.notification.all')
            ->with(compact('notifications'));
	}
}