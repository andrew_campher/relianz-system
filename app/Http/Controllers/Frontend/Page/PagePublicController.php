<?php

namespace App\Http\Controllers\Frontend\Page;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Page\ContactSubmitRequest;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;

class PagePublicController extends Controller
{

    public function __construct()
    {

    }

    public function page($page) {

        $page_view = 'public.page.'.$page;

        if(view()->exists($page_view)) {
            return view($page_view);
        }


    }

    public function feedback() {

        return view('public.feedback.form');
    }

    public function contact_submit(ContactSubmitRequest $request) {

        $email = (new MailMessage)
            ->success()
            ->subject(app_name().' Contact Message')
            ->line($request->input('message'))
            ->line($request->name)
            ->line($request->tel)
            ->line(trans('strings.emails.auth.thank_you_for_using_app'));

        $email = $request->email;
        $name = $request->name;


        Mail::send('emails.contactmessage', ['name'=> $name, 'email'=>$email, 'the_message'=>$request->input('message'), 'tel'=> $request->tel], function ($message) use ($name, $email) {

            $message->from($email, $name);
            $message->to(env('MAIL_FROM'), app_name());

            $message->subject(app_name().' Contact Message');
        });


        return back()->withFlashSuccess('Success! Your message has been sent');

    }

}
