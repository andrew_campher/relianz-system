<?php

namespace App\Http\Controllers\Frontend\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Payment\ProcessPaymentRequest;
use Illuminate\Support\Facades\Redirect;


/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class PaymentController extends Controller
{

    /**
     * !!! NOT USED - rather use the api.payment.process
     * @param ProcessPaymentRequest $request
     * @return mixed
     */
    public function process(ProcessPaymentRequest $request) {

        $data = $request->all();

        if ($request->input('payment_method') == 'eft') {
            #past request data to save
            if ($invoice = $this->invoice->create($data)) {
                return redirect()->route('frontend.invoice.show', $invoice->id)->withFlashSuccess('Success! Invoice has been created');
            }

        } elseif ($request->input('payment_method') == 'payfast') {

            /**
             * PROCEED WITH CREDIT CARD gateway payment
             */

            \Omnipay::setGateway($request->input('payment_method'));

            $cardInput = [
                'firstName' => access()->user()->first_name,
                'lastName' => access()->user()->first_name,
                'email' => access()->user()->email,
                'company' => access()->user()->professional->title,
            ];

            $card = \Omnipay::creditCard($cardInput);

            $response = \Omnipay::purchase([
                'amount' => $request->input('own_amount') ? $request->input('own_amount') : $request->input('amount'),
                'description' => $request->input('description'),
                'custom_str1' => $request->input('account_id'),
                'returnUrl' => route('frontend.account.index'),
                'cancelUrl' => route('frontend.account.credits_purchase', $request->input('account_id')),
                'notifyUrl' => route('api.payment.notify'),
                'card' => $cardInput
            ])->send();


            if ($response->isSuccessful()) {

                return Redirect::route('frontend.account.index')->withFlashDanger('Payment was successful - Thank you');
            } elseif ($response->isRedirect()) {
                // redirect to offsite payment gateway
                $response->redirect();
            } else {
                // payment failed: display message to customer
                // echo $response->getMessage();
                return Redirect::route('frontend.account.credits_purchase')->withFlashDanger('Your payment attempt failed with the following: <p>' . $response->getMessage() . '</p><p>Please try again</p>');
            }
        }


        return Redirect::back()->withFlashDanger('Unable to process this order.');

    }

}