<?php

namespace App\Http\Controllers\Frontend\Professional;

use App\Http\Requests\Frontend\Professional\ManageProfessionalRequest;
use App\Http\Requests\Frontend\Professional\StoreProfessionalRequest;
use App\Http\Requests\Frontend\Professional\UpdateProfessionalRequest;
use App\Models\Access\User\User;
use App\Models\Category\Category;
use App\Repositories\Backend\Professional\ProfessionalRepository;
use App\Models\Professional\Professional;
use App\Services\MediaManager\MediaManager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Services\Images\Images;

class ProfessionalController extends Controller
{

    protected $professionals;

    /**
     * @param ProfessionalRepository $professionals
     */
    public function __construct(ProfessionalRepository $professionals)
    {
        $this->professionals = $professionals;
    }

    public function show(Professional $professional, ManageProfessionalRequest $request) {

        if ($professional->user_id != access()->id())
        Professional::where('id', $professional->id)->UpdateHits();

        return view('public.professional.show')
            ->withProfessional($professional);
    }

    public function create()
    {
        $servicelist = Category::servicelist();

        return view('frontend.professional.edit')
            ->with(compact('servicelist'));
    }

    /**
     * @param StoreProfessionalRequest $request
     * @param ProfessionalRepository $professional
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreProfessionalRequest $request, ProfessionalRepository $professional)
    {
        $data = $request->all();

        #Professional logo
        if ($image_name = Images::upload($request, ['logo', (new Professional())->getTable()])) {
            $data['logo'] = $image_name['image'];
        }

        #create professional
        $data['user_id'] = access()->id();

        $professional->create($data);

        #Take to professional Dashboard
        return redirect(route('frontend.professional.dashboard'));
    }

    public function edit(Professional $professional)
    {

        #Get service list
        $servicelist = Category::servicelist();

        return view('frontend.professional.edit')
            ->withProfessional($professional)
            ->with(compact('servicelist'));
    }

    public function update(Professional $professional, UpdateProfessionalRequest $request)
    {

        $data = $request->all();

        if ($uploaded_images = Images::upload($request, [['logo', 'hero_image'], $professional->getTable()])) {

            $data = array_merge($data, $uploaded_images);
        }

        if ($professional->status == 2) {
            $data['status'] = 1;
        }

        #Handle Media for gallery
        MediaManager::uploadfiles($request, [ get_class($professional), $professional->id]);

        #Handle Media for 'resources'
        MediaManager::uploadfiles($request, [get_class($professional), $professional->id, 'resources']);

        $this->professionals->update($professional, ['data' => $data]);
        return redirect()->route('public.professional.show', [$professional->id, $professional->slug])->withFlashSuccess('Update success');
    }

}
