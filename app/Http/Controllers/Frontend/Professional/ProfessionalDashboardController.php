<?php

namespace App\Http\Controllers\Frontend\Professional;

use App\Http\Controllers\Controller;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\Service\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ProfessionalDashboardController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        #TODO: make this a route in API and call via ajax / vue
        $user = Auth::user();

        if(!$user->professional)
            return redirect('/');


        $pro_services = DB::table( 'professional_service' )
            ->select('service_id')
        ->where( 'professional_id', '=', $user->professional->id )->get();

        if ($pro_services) {
            $pro_services = array_pluck($pro_services, 'service_id');
        }

        /**
         * GEt all projects that are Quotable that match the professionals services
         * TODO: do this in a better way without repeating whole query
        */
        #If professional travels THEN:
        if ($user->professional->distance_travelled > 0)
        {
            #limit by distance travelled - order by latest first
            $projects = Project::ProfessionalQuotable()
                ->whereIn('service_id',$pro_services)
                ->with('quotes')
                ->WithinDistanceToBranches($user->professional->branches)
                ->orderBy('id', 'DESC')
                ->Paginate(20);
        } else {
            #No travel static location
            # ORder by latest
            $projects = Project::ProfessionalQuotable()->whereIn('service_id',$pro_services)->Distance($user->professional->lat, $user->professional->lng)->with('quotes')->orderBy('id', 'DESC')->Paginate(20);
        }

        $services = Service::join('professional_service', 'professional_service.service_id', 'services.id')->where('professional_id', $user->professional->id)->orderBy('hits', 'desc')->limit(13)->get();

        return view('frontend.professional.dashboard')
            ->with(compact('projects', 'services'));
    }
}