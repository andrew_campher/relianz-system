<?php

namespace App\Http\Controllers\Frontend\Professional;

use App\Http\Requests\Frontend\Professional\ClaimListingRequest;
use App\Http\Requests\Frontend\Professional\ManageProfessionalRequest;
use App\Http\Requests\Frontend\Professional\UpdateConfirmProfessionalRequest;
use App\Models\Access\User\User;
use App\Models\Category\Category;
use App\Notifications\Frontend\Auth\ProClaimConfirmation;
use App\Notifications\Professional\AdminNotifyNewProfessional;
use App\Repositories\Backend\Professional\ProfessionalRepository;
use App\Models\Professional\Professional;
use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Services\Images\Images;
use App\Services\MediaManager\MediaManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ProfessionalPublicController extends Controller
{

    /**
     * @param ProfessionalRepository $professionals
     */
    public function __construct()
    {

    }

    public function show(Professional $professional, $slug, ManageProfessionalRequest $request) {

        #Record Hits
        if ($professional->user_id != access()->id())
            Professional::where('id', $professional->id)->UpdateHits();

        $professional->load(['services' => function ($query) {
            $query->limit(15);
        }]);

        return view('public.professional.show')
            ->withProfessional($professional);
    }

    public function show_reviews(Professional $professional, $slug, ManageProfessionalRequest $request) {

        return view('public.professional.show_reviews')
            ->withProfessional($professional);
    }


    /**
     * @param $user
     * @return mixed
     */
    public function claimListing(Professional $professional, ClaimListingRequest $request)
    {

        if ($professional->status == 2) {
            /**
             * CHECK if user wants to claim a PRofessional Account
             */
            $hash = md5($professional->id.$professional->notify_email);

            $professional->notify(new ProClaimConfirmation($hash, $professional));
            return redirect('/')->withFlashSuccess('A confirmation email has been sent to the associated email address on file <b>"___' . substr($professional->notify_email, 3) . '"</b> - please check SPAM folders and click the link within email.');
        } else {
            return redirect()->route('frontend.auth.login')->withFlashWarning('Listing already claimed - login with associated account');
        }
    }


    public function edit(Professional $professional, $hash, Request $request)
    {

        /**
         * Security for accessing
         */
        if(access()->id()) {

            if (access()->id() && access()->id() == $professional->user_id) {
                return redirect()->route('frontend.professional.edit', [$professional->id]);
            } else {
                return redirect('/')->withFlashWarning('Not allowed to access that page - contact support.');
            }
        }

        if ($professional->status == 1)
            return redirect()->route('frontend.auth.login')->withFlashWarning('The professional account "'.$professional->title.'" has already been confirmed - please login with the associated account.');

        if ($professional->status == 0)
            return redirect()->route('frontend.auth.login')->withFlashWarning('The professional account "'.$professional->title.'" is pending approval - please login with the associated account.');


        $project_id = $request->input('project_id');

        $pro_hash = md5($professional->id.$professional->notify_email);

        if ($pro_hash == $hash) {

            $servicelist = Category::select('title','id','parent_id')->servicelist();

            return view('public.professional.confirm_edit')
                ->with(compact('professional','servicelist', 'user', 'confirmation_code', 'project_id'));
        }

        return redirect('/')->withFlashWarning('There does not seem to be a matching professional account. Please try login or create a new account.');
    }

    public function update_confirm(Professional $professional, UpdateConfirmProfessionalRequest $request, ProfessionalRepository $professionals, UserRepository $users)
    {

        $data = $request->all();

        /**
         * USER PROCESS
         */
        if ($imagename = Images::upload($request, ['image', (new User)->getTable(), 'fit' => true])) {
            $data['image'] = $imagename['image'];
        } else {
            #If no image then exclude image from update value array
            unset($data['image']);
        }

        if (!$request->input('user_id')) {
            /**
             * USER NEW
             */
            $data['confirmed'] = 1;
            $data['status'] = 1;
            $data['role_id'] = 2;

            $user = $users->create($data);


        } else {
            #Get user that matches confirmation code and where he is not confirmed.
            $user = User::find($request->input('user_id'));

            /**
             * USER UPDATE
             */

            $users->update_confirm($user, ['data' => $data]);
        }

        if (!$user)
            return redirect()->back()->withFlashDanger('There was a problem creating a user account.');
        /**
         * SECURED - go ahead and update
         */

        if ($uploaded_images = Images::upload($request, [['logo', 'hero_image'], $professional->getTable()])) {

            $data = array_merge($data, $uploaded_images);
        }

        #Handle Media for gallery
        MediaManager::uploadfiles($request, [ get_class($professional), $professional->id, null, 'user_id'=> $user->id]);

        #Handle Media for 'resources'
        MediaManager::uploadfiles($request, [get_class($professional), $professional->id, 'resources', 'user_id'=> $user->id]);

        /**
         * PROFESSIONAL UPDATE
         */
        #Lets just set to a confirmed professional

        $admins = User::IsAdmins()->get();
        Notification::send($admins, new AdminNotifyNewProfessional($professional, $data));

        $data['status'] = 1;
        $data['user_id'] = $user->id;
        $professionals->update($professional, ['data' => $data]);

        #Create Account entry (No Package)
        if (!$professional->account()->exists()) {
            $professional->account()->create(['credits_remain'=>0, 'package_id'=> 1]);
        }

        /**
         * Log user in
         */
        auth()->login($user, true);

        /**
         * If project lets redirect to the project page
         */
        if($request->input('project_id')) {
            return redirect()->route('frontend.project.show', $request->input('project_id'))->withFlashSuccess('Go ahead and quote this customer...');
        }

        return redirect()->route('public.professional.show', [$professional->id, $professional->slug])->withFlashSuccess('Update success');
    }




}
