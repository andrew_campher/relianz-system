<?php

namespace App\Http\Controllers\Frontend\Professional;

use App\Http\Requests\Frontend\Professional\ManageProfessionalRequest;
use App\Http\Requests\Frontend\Professional\StoreProfessionalRequest;
use App\Http\Requests\Frontend\Professional\UpdateProfessionalRequest;
use App\Models\Access\User\User;
use App\Models\Category\Category;
use App\Repositories\Backend\Professional\ProfessionalRepository;
use App\Models\Professional\Professional;
use App\Services\MediaManager\MediaManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Services\Images\Images;
use Illuminate\Support\Facades\Notification;

class ProfessionalQuotesController extends Controller
{

    protected $professionals;

    /**
     * @param ProfessionalRepository $professionals
     */
    public function __construct(ProfessionalRepository $professionals)
    {
        $this->professionals = $professionals;
    }

    public function quotes(Professional $professional, $status = 'pending') {

        switch ($status) {
            case "draft":
                $quotes = $professional->draft_project_quotes()->paginate(15);
                break;
            case "pending":
                $quotes = $professional->open_project_quotes()->paginate(15);
                break;
            case "hired":
                $quotes = $professional->hired_project_quotes()->paginate(15);
                break;
            case "unsuccessful":
                $quotes = $professional->unsuccessful_project_quotes()->paginate(15);
                break;
        }

        return view('frontend.professional.quotes')
            ->with(compact('professional', 'quotes', 'status'));
    }

}
