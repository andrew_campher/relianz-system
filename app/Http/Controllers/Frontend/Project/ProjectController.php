<?php

namespace App\Http\Controllers\Frontend\Project;


use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Project\ManageProjectRequest;
use App\Http\Requests\Frontend\Project\ShowProjectRequest;
use App\Models\Project\Project;
use App\Models\Quote\Quote;
use App\Repositories\Backend\Project\ProjectRepository;
use App\Repositories\Backend\Quote\QuoteRepository;
use App\Services\MediaManager\MediaManager;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ProjectController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.project.index');
    }

    public function all($status = 'active') {

        switch ($status) {
            case "completed":
                $projects = access()->user()->projects()->CompletedProjects()->paginate(15);
                break;
            case "closed":
                $projects = access()->user()->projects()->ClosedProjects()->paginate(15);
                break;
            case "open":
                $projects = access()->user()->projects()->OpenProjects()->paginate(15);
                break;
            case "expired":
                $projects = access()->user()->projects()->ExpiredProjects()->paginate(15);
                break;
            case "active":
                $projects = access()->user()->projects()->ActiveProjects()->paginate(15);
                break;
        }


        return view('frontend.project.all')
            ->with(compact('projects', 'status'));
    }

    public function status_change(Project $project, ManageProjectRequest $request) {

        return view('frontend.project.status_change')
            ->with(compact('project'));
    }

    /**
     * Allow Client to manually update the Project Status
     * @param Project $project
     * @param ManageProjectRequest $request
     * @param ProjectRepository $projects
     * @return mixed
     */
    public function update_status(Project $project, ManageProjectRequest $request, ProjectRepository $projects) {

        #IF the client has selected a HIRE professional for this project
        if ($request->input('quote_id')) {


            $quote = Quote::find($request->input('quote_id'));
            $quotes = new QuoteRepository();

            #Run hired method
            $quotes->hired($quote);

            return redirect(route('frontend.quote.show', $quote->id))->withFlashSuccess('Success - You have marked this Professional as hired!');
        } else {
            #No quotes received

            $data['status'] = $project->getStatusKey('closed');
            $data['close_reason'] = $request->input('close_reason');

            $projects->status_closed($project, $data);

            return redirect(route('frontend.user.dashboard'))->withFlashSuccess('Success - You have closed the project');
        }

    }

    public function show(Project $project) {

        if ($project->user_id != access()->id() && access()->user()->hasRole('User') && !access()->user()->isRole('Administrator')) {
            return back()->withFlashWarning('You do not have permission to access that');
        }

        if (access()->user()->isRole('Professional') && access()->user()->professional->status == 2) {
               return redirect()->route('frontend.professional.edit',[access()->user()->professional->id])->withFlashSuccess('Please take a few minutes to confirm your professional account.');
        }

        #add Hits = if not my project
        if ($project->user_id != access()->id())
            Project::where('id', $project->id)->UpdateHits(['timestamps' => false]);

        #if professional then lets get his quote on this item
        $existing_quote = array();
        if (access()->user()->isRole('Professional') && isset(access()->user()->professional)) {
            $existing_quote = Quote::where('project_id', $project->id)->where('professional_id', access()->user()->professional->id)->first();
        }

        $answers = $project->getAnswers();

        return view('frontend.project.show')
            ->with(compact('project', 'existing_quote', 'answers'));
    }


    public function manage(Project $project, ManageProjectRequest $request) {

        $answers = $project->getAnswers();

        return view('frontend.project.manage')
            ->with(compact('project', 'answers'));
    }

    public function profile(Project $project) {

        return view('frontend.project.show')
            ->with(compact('project', 'history'));
    }


}