<?php

namespace App\Http\Controllers\Frontend\Project;


use App\Http\Controllers\Controller;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\Quote\Quote;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ProjectPublicController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.project.index');
    }

    /**
     * Method for publicly viewing project - only when clicked via special hash link in email
     * @param Project $project
     * @param Professional $professional
     * @param $hash
     * @return mixed
     */
    public function details(Project $project, Professional $professional, $hash) {

        if (access()->id() && access()->id() && access()->user() && access()->user()->isRole('professional')) {

            return redirect()->route('frontend.project.show',[$project->id]);
        }

        if ($hash) {
            $made_hash = md5($professional->notify_email.$project->id);

            if ($hash != $made_hash) {
                return redirect('/')->withFlashWarning('You are accessing a protected project link.');
            }

        }

        /**
         * Lets redirect UNCONFIRMED Professional to the the edit page
         */
        if ($professional->status == 2) {
            $pro_hash = md5($professional->id.$professional->notify_email);

            return redirect()->route('public.professional.edit',[$professional->id, $pro_hash, 'project_id='.$project->id])->withFlashSuccess('Please take a few minutes to confirm your professional account.');
        }


        #if professional then lets get his quote on this item
        $existing_quote = array();
        $existing_quote = Quote::where('project_id', $project->id)->where('professional_id', $professional->id)->first();

        $answers = $project->getAnswers();

        return view('frontend.project.show')
            ->with(compact('project', 'professional', 'existing_quote', 'answers'));
    }


    public function quote(Project $project, Request $request) {

        $invite_code = $request->input('invite_code');

        Project::where('id', $project->id)->UpdateHits(['timestamps' => false]);

        $answers = $project->getAnswers();

        return view('public.project.quote')
            ->with(compact('project', 'answers', 'invite_code'));
    }


}