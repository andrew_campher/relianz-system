<?php

namespace App\Http\Controllers\Frontend\Project;

use App\Exceptions\GeneralException;
use App\Helpers\Frontend\Auth\Socialite;
use App\Helpers\Helper;
use App\Helpers\SearchHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Project\StoreProjectRequest;
use App\Models\Access\User\User;
use App\Models\Occupation\Occupation;
use App\Models\QuestionAnswer\QuestionAnswer;
use App\Models\QuestionOption\QuestionOption;
use App\Models\SearchLog\SearchLog;
use App\Models\Service\Service;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Repositories\Backend\Project\ProjectRepository;
use App\Services\MediaManager\MediaManager;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ProjectWizardController extends Controller
{

    public function select_service(Request $request)
    {
        $found_services = array();

        $searchString = $request->input('searchstring');
        if ($searchString) {
            $cleaned_string = SearchHelper::removeCommonWords($searchString);


            $found_services = Service::whereRaw("match(tags) against (? in boolean mode)", [$cleaned_string . '*'])->orderBy('hits', 'desc')->limit(10)->get();
        }

        return view('frontend.wizard.found_services')
            ->with(compact('found_services', 'searchString'));
    }

    /**
     * Wizard route Data and Html
     * @param Request $request
     * @return $this
     */
    public function wizard(Request $request)
    {
        if ($request->input('searchstring')) {
            $searchString = $request->input('searchstring');

            $cleaned_string = SearchHelper::removeCommonWords($searchString);

            $found_it = 0;

            $found_services = Service::whereRaw("match(tags) against (? in boolean mode)", [$cleaned_string.'*'])->orderBy('hits', 'desc')->limit(10)->get();
            if (!$found_services->isEmpty())
                $found_it = 1;

            #Log Search
            if (strlen($cleaned_string) > 3) {
                DB::statement('INSERT INTO search_log SET search_term = ?, searches = 1, searches_week = 1, searches_month = 1, searches_year = 1, `found` = ?, created_at = NOW()
                 ON DUPLICATE KEY UPDATE 
                 searches= searches+1, 
                 searches_week = searches_week+1, searches_month = searches_month+1, searches_year = searches_year+1, 
                 `found` = ?, 
                 updated_at = NOW()',
                    [$searchString, $found_it, $found_it]);
            }

            if (!$found_services->isEmpty()) {
                return view('frontend.wizard.found_services')
                    ->with(compact('found_services', 'searchString'));
            }
        } elseif ($request->input('string')) {
            $string = $request->input('string');

            #Log Search
            if (strlen($string) > 3) {
                DB::statement('INSERT INTO search_log SET search_term = ?, searches = 1, searches_week = 1, searches_month = 1, searches_year = 1, `found` = 0, created_at = NOW()
                 ON DUPLICATE KEY UPDATE 
                 searches= searches+1, 
                 searches_week = searches_week+1, searches_month = searches_month+1, searches_year = searches_year+1, 
                 `found` = 0, 
                 updated_at = NOW()',
                    [$string]);
            }
        }

        $services['questions'] = array();

        if ($request->input('service')) {
            Service::where('id', $request->input('service'))->UpdateHits();

            #Get all Service Questions
            $services = Service::where('id', $request->input('service'))->with('questions')->first()->toArray();

            $occupation = Occupation::find($services['occupation_id']);

            #Get questions IDs so as to pull their options
            $question_ids_only = array_pluck($services['questions'], 'id');

            #Get all options for questions above
            $options_array = QuestionOption::wherein('question_id', $question_ids_only)->get()->toArray();

            #Reformat options to have question_id as key.
            $options = Helper::make_tree_array($options_array, 'question_id');

        }

        /**
         * FIRST QUESTION STATIC
         */
        $landing_questions = array('id'=>'landing','custom'=>1,'title'=>'Find the Pros','question_type'=>'custom', 'custom'=> 'landing' );

        array_unshift($services['questions'], $landing_questions);

        /**
         * ADD STATIC default questions to array
         * TODO: should be a better more dynamic way todo this ~tried global questions setting but lots of code overhead to make all work
         */
        $static_questions =
            [
                array('id'=>'files', 'manual'=>1, 'name'=>'files', 'title'=>'Would you like to add photos to describe your '.(isset($services['title'])?$services['title']:'the service').' project?','question_type'=>'radio' ),
                array('id'=>'when', 'manual'=>1, 'name'=>'when', 'title'=>'When do you need '.(isset($services['title'])?$services['title']:'the service').'?','question_type'=>'select' ),
                array('id'=>'has_description', 'manual'=>1,'title'=>'Anything else the '.(isset($occupation->title)?$occupation->title:'professional').' should know?','question_type'=>'radio' ),
                array('id'=>'area', 'manual'=>1, 'title'=>'Please confirm where you need the '.(isset($occupation->title)?$occupation->title:'professional'),'question_type'=>'custom', 'custom'=> 'geocode' ),
                array('id'=>'name_it', 'manual'=>1, 'name'=> 'title','title'=>'What do you want to call this project? ','question_type'=>'text'),
                array('id'=>'get_quotes_method', 'manual'=>1, 'title'=>'How would you like to receive quotes?','question_type'=>'radio' ),
            ];

            #'special' - see blade templates
        ## static ids are very important - DO NOT CHANGE - only add


        $options['files'] = [
            ['id'=>'No', 'title'=>'No', 'special'=>''],
            ['id'=>'Yes', 'checked'=>'1', 'set_value'=>'Yes', 'title'=>'Yes', 'special'=>'show|files|upload_files'],
        ];

        $options['when'] = [
            ['id'=>'when1', 'set_value'=>'1', 'title'=>'As soon as possible', 'special'=>''],
            ['id'=>'when2', 'set_value'=>'2','checked'=>'1', 'title'=>'I\'m flexible', 'special'=>''],
            ['id'=>'when3', 'set_value'=>'3','title'=>'In the next few days', 'special'=>''],
            ['id'=>'when4', 'set_value'=>'4','title'=>'On one particular date', 'special'=>'show|datepicker|specific_date'],
            ['id'=>'when5', 'set_value'=>'5','title'=>'Other (I\'d need to describe)', 'special'=>'show|textarea|when_describe|When would you like to schedule this for?'],
        ];

        $options['has_description'] = [
            ['id'=>'Yes', 'title'=>'Yes', 'checked'=>'1', 'special'=>'show|textarea|description|Details about the project:'],
            ['id'=>'No', 'title'=>'No', 'special'=>''],
        ];

        $name = '';
        if (isset($services['title']) && $services['title'] && isset($occupation->title) && $occupation->title) {
            $name = ucfirst($occupation->title).' needed for my '.$services['title'].' project';
        }

        if (isset($string) && strlen($string) > 7) {
            $name = $string;
        }

        $options['name_it'] = [
            ['id'=>'No', 'title'=>'', 'placeholder'=>'Name this project', 'set_value'=>$name, 'special'=>'', 'onclick' => 'this.select()'],
        ];

        ##FYI: you cannot pre-check this custom special options (special file loads outside of option foreach - see Wizard view)
        $options['get_quotes_method'] = [
            ['id'=>'by_email', 'set_value'=>'by_email', 'title'=>'By email only', 'special'=>'contact_method'],
            ['id'=>'by_email_mobile', 'checked'=>'1', 'set_value'=>'by_email_mobile', 'title'=>'By email and text message', 'special'=>'contact_method'],
        ];

        /**
         * Add Register Account Step - for guest
         */
        if (!Auth::user()) {
            $static_questions[] = array('id'=>'new_account','custom'=>1, 'manual'=>1,'title'=>'Account Details','question_type'=>'custom', 'custom'=> 'new_account' );

            $options['new_account'] = [
                ['name'=>'first_name', 'id'=>'first_name', 'title'=>'First Name','placeholder'=>'First name', 'special'=>''],
                ['name'=>'last_name', 'id'=>'last_name', 'title'=>'Last Name','placeholder'=>'Last name', 'special'=>''],
                ['name'=>'password', 'id'=>'password', 'title'=>'Password to login','placeholder'=>'Password', 'special'=>'','question_type'=>'password'],
            ];
        }


        #Combine Static questions with DB questions
        $services['questions'] = array_merge($services['questions'], $static_questions);

        return view('frontend.wizard.wizard')
            ->with(compact('services', 'options'))
            ->withSocialiteLinks((new Socialite())->getSocialLinks());
    }



    /**
     * @param StoreProjectRequest $request
     * @param ProjectRepository $projects
     * @param UserRepository $users
     * @return array
     * @throws GeneralException
     */
    public function store(StoreProjectRequest $request, ProjectRepository $projects, UserRepository $users)
    {
        $data = $request->except('file');

        /**
         * USER registration
         * IF NOt logged in
         */
        if (!Auth::user()) {

            #POSTED data
            $user_input = array();
            $user_input['first_name'] = $data['first_name'];
            $user_input['last_name'] = $data['last_name'];
            $user_input['email'] = $data['customer_email'];
            $user_input['mobile'] = $data['customer_mobile'];
            $user_input['password'] = $data['password'];
            $user_input['area'] = $data['area'];

            $user_input['lat'] = $data['lat'];
            $user_input['lng'] = $data['lng'];

            /**
             * Existing Account Check (via email)
             */
            if (!$user = User::where('email', $user_input['email'])->first()){
                /**
                 * CREATE ACCOUNT - no user found
                 */

                #ELSE if no existing account - CRE
                $v = Validator::make($user_input, [
                    'first_name' => 'required|filled',
                    'last_name' => 'required|filled',
                    'password' => 'required|filled',
                    'email' => 'required|filled|email'
                ]);
                #Should never fail due to JS validation - just incase.
                if ($v->fails())
                    return response([
                        'status'=> 'error',
                        'message'=> 'Could not register new user, ensure all details are filled in - First name, last name, password and email.',
                    ], 200);

                #Create User
                $user = $users->create($user_input);
            }

            if (!$user->social_logins->isEmpty()) {
                return response([
                    'status'=> 'error',
                    'message'=> 'This account is linked to a social network account. Use the social network login buttons to authenticate this user.',
                ], 200);
            }

            /**
             * PREVENT professional posting projects
             * NOt supported yet
             */
            if ($user->hasRole(2)) {
                return response([
                    'status'=> 'error',
                    'message'=> 'That account belongs to a Professional. It is not possible to create projects under a professional account. Please use a different email.',
                ], 200);
            }

            ##LOG USER IN
            if (!Auth::attempt(['email' => $user->email, 'password' => $data['password']])) {
                return response([
                    'status'=> 'error',
                    'message'=> 'Could not login, please ensure correct password.',
                ], 200);
            }

        } else {
            #User is already logged in
            $user = Auth::user();

            /**
             * Restrict Professional from creating projects
             */
            if ($user->isRole('Professional')) {
                return response([
                    'status'=> 'error',
                    'message'=> 'Unfortunately professional users are not able to create quote requests. Please register as a normal user in order to post requests. Thanks!',
                ], 200);
            }

            /**
             * Update his lat/lng if they are empty
             */
            $update_user = array();

            if ($data['customer_mobile'] && !$user->mobile) {
                $update_user['mobile'] = $data['customer_mobile'];
            }
            if (!$user->lat && !$user->lng) {
                $update_user['lat'] = $data['lat'];
                $update_user['lng'] = $data['lng'];

            }
            #if update is filled then lets save
            if ($update_user) {
                foreach ($update_user AS $key => $val) {
                    $user->{$key} = $val;
                }
                $user->save();
            }
        }

        ##LETS check If User is logged in
        if (!Auth::user()) {
            return response([
                'status'=> 'error',
                'message'=> 'Failed to log you in, check email and password',
            ], 200);
        }

        /**
         * Assign user_id
         */
        $data['user_id'] = Auth::id();

        /**
         * SAVE PROJECT
         * 1) CLEAN input values
         */

        if (isset($data['specific_date']) && $data['specific_date']) {
            $data['specific_date'] = Carbon::createFromFormat('d-m-Y H:i', $data['specific_date'])->toDateTimeString();
        }

        #past request data to save
        if ($project = $projects->create_from_wizard(['data' => $data])) {
            /**
             * SAVE QUESTIONS
             */
            $now = Carbon::now('utc')->toDateTimeString();

            $db_answers = array();
            foreach ($data AS $key => $value)
            {

                $poss_id = str_replace("q_", "", $key);
                if (is_numeric($poss_id)) {
                    #DB QUESTIONS
                    $question_id = $poss_id;

                    if (is_array($value)) {
                        foreach ($value AS $vkey => $val) {

                            if (!is_numeric($vkey))
                                continue;

                            $db_answers[] = ['project_id' => $project->id, 'question_id' => $question_id, 'answer_value' => $val, 'created_at' => $now, 'updated_at' => $now];



                            #Check for OTher Fields
                            $otherlower = strtolower($val);

                            if(isset($data['special'][$poss_id][$otherlower]) && $data['special'][$poss_id][$otherlower]) {
                                $db_answers[] = ['project_id' => $project->id, 'question_id' => $question_id, 'answer_value' => $data['special'][$poss_id][$otherlower], 'created_at' => $now, 'updated_at' => $now];
                            }
                        }

                    } else {
                        $db_answers[] = ['project_id'=>$project->id, 'question_id'=>$question_id, 'answer_value' => $value, 'created_at'=> $now, 'updated_at'=> $now];

                        #Check Special
                        $otherlower = strtolower($value);

                        if(isset($data['special'][$poss_id][$otherlower]) && $data['special'][$poss_id][$otherlower]) {
                            $db_answers[] = ['project_id' => $project->id, 'question_id' => $question_id, 'answer_value' => $data['special'][$poss_id][$otherlower], 'created_at' => $now, 'updated_at' => $now];
                        }
                    }
                }
            }

            /**
             * ATTACH Dynamic Answers
             */
            if ($db_answers)
                QuestionAnswer::insert($db_answers);

            /**
             * SAVE FILES
             */
            MediaManager::uploadfiles($request, [get_class($project), $project->id, 'user_id'=> $user->id]);

            return response([
                'status'=> 'success',
                'message'=> 'Project successfully created',
                'data' => ['project_id' => $project->id]
            ], 200);

        }

        throw new GeneralException('Project creation error.');

    }


}