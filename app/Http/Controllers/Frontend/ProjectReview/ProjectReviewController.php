<?php

namespace App\Http\Controllers\Frontend\ProjectReview;


use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ProjectReview\StoreProjectReviewRequest;
use App\Models\ProjectReview\ProjectReview;
use App\Models\Quote\Quote;
use App\Repositories\Backend\ProjectReview\ProjectReviewRepository;


/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ProjectReviewController extends Controller
{

    /**
     * @var
     */
    protected $project_reviews;

    /**
     * ProjectReviewController constructor.
     * @param ProjectReviewRepository $project_reviews
     */
    public function __construct(ProjectReviewRepository $project_reviews) {
        $this->project_reviews = $project_reviews;
    }


    public function show(ProjectReview $project_review) {

        $project_review->load('professional');

        return view('frontend.project_review.show')
            ->with(compact('project_review'));
    }

    public function create(Quote $quote) {

        if ($quote->review)
            return redirect('/my-projects')->withFlashDanger('Project review already found');

        $professional = $quote->professional;

        return view('frontend.project_review.edit')
            ->with(compact('quote', 'professional'));
    }

    /**
     * @param StoreProjectReviewRequest $request
     * @param ProjectReviewRepository $project_reviews
     * @return mixed
     */
    public function store(StoreProjectReviewRequest $request)
    {

        if ($request->input('quote_id') && $request->input('quote_id')) {
            $quote = Quote::find($request->input('quote_id'));
            if ($quote->review)
                return redirect('/')->withFlashDanger('Project review already found');
        }
        $data = $request->all();

        #Set to Approved / Published
        $data['status'] = 1;

        #past request data to save
        if ($project_review = $this->project_reviews->create($data)) {

            $this->project_reviews->approve($project_review, $data);

        }

        return redirect('/my-projects')->withFlashSuccess('Your review has been submitted');

    }

}