<?php

namespace App\Http\Controllers\Frontend\ProjectReview;

use App\Events\Common\ProjectReview\ProjectReviewPublicCreated;
use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ProjectReview\StoreProjectReviewPublicRequest;
use App\Models\Professional\Professional;
use App\Models\ProjectReview\ProjectReview;
use App\Repositories\Backend\ProjectReview\ProjectReviewRepository;
use App\Repositories\Frontend\Access\User\UserRepository;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class ProjectReviewPublicController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $project_reviews;

    /**
     * ProjectReviewController constructor.
     * @param ProjectReviewRepository $project_reviews
     */
    public function __construct(ProjectReviewRepository $project_reviews) {
        $this->project_reviews = $project_reviews;
    }


    public function show(ProjectReview $review) {

        return view('frontend.review.show')
            ->with(compact('review'));
    }

    public function professional_review(Professional $professional) {

        $existing_review = null;
        if (access()->id())
            $existing_review = ProjectReview::where('professional_id', $professional->id)->where('user_id', access()->id())->first();

        return view('frontend.project_review.edit')
            ->with(compact('professional', 'existing_review'));
    }

    /**
     * @param Professional $professional
     * @param StoreProjectReviewPublicRequest $request
     * @return mixed
     */
    public function store(Professional $professional, StoreProjectReviewPublicRequest $request, UserRepository $users)
    {
        $data = $request->all();
        /**
         * Register new User Account
         */
        $user = $users->create($data);
        if ($user) {
            event(new UserRegistered($user));
            #ADd user id to array
            $data['user_id'] = $user->id;

            #past request data to save
            if ($project_review = $this->project_reviews->create($data)) {

                /**
                 * EVENTS
                 */

                event(new ProjectReviewPublicCreated($project_review));

            }
            return redirect('public.professional.show', [$professional->id, $professional->slug])->withFlashSuccess('Your review has been submitted');
        }

    }

}