<?php

namespace App\Http\Controllers\Frontend\Quote;

use App\Events\Common\Quote\QuoteRead;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Quote\StoreQuoteRequest;
use App\Models\Project\Project;
use App\Models\Quote\Quote;
use App\Repositories\Backend\Project\ProjectRepository;
use App\Repositories\Backend\Quote\QuoteRepository;
use App\Services\Account\Billing;
use App\Services\MediaManager\MediaManager;
use App\Services\Professionals\ProfessionalsService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class QuoteController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $quote;

    /**
     * QuoteController constructor.
     * @param QuoteRepository $quote
     */
    public function __construct(QuoteRepository $quote) {
        $this->quote = $quote;
    }


    public function show(Quote $quote) {

        if ($quote->user_id != access()->id() && $quote->project->user_id != access()->id() && !access()->user()->hasRole(1)) {
            return back()->withFlashWarning('You do not have permission to access that');
        }

        #If read_at is NULL lets mark it as read.
        if ($quote->project->user_id == access()->id() && !$quote->read_at) {
            $quote->read_at = Carbon::now();
            $quote->status = 2;
            $quote->save();

            #Fire read event
            event(new QuoteRead($quote));
        }

        return view('frontend.quote.show')
            ->with(compact('quote'));
    }

    public function decline(Quote $quote) {

        return view('frontend.quote.decline')
            ->with(compact('quote'));
    }

    public function decline_store(Quote $quote, Request $request)
    {

        $data = $request->all();

        $this->quote->decline($quote, $data);

        return redirect()->route('frontend.project.manage', $quote->project_id)->withFlashSuccess('Quote declined - Success');
    }


    /**
     * @param StoreQuoteRequest $request
     * @param ProjectRepository $projects
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreQuoteRequest $request, ProjectRepository $projects)
    {

        $data = $request->all();

        #Check if professional has not ALREADY submitted a quote in this job
        if (DB::table('quotes')->select('id')
            ->where('project_id', $request->project_id)
            ->where('professional_id', access()->user()->professional->id)
            ->value('id')) {
            return back()->withFlashSuccess('You have already written a quote on this project.');
        }

        #Just set to default 0 = Pending (no credits)
        $data['status'] = 0;

        /**
         * ENSURE professional can submit quote:
         * Has enough credits
         * project service is free
         * Package is free
         */
        $project = Project::find($request->project_id);
        if (ProfessionalsService::canSendQuote($project)) {
            ## Set to Status Active
            $data['status'] = 1;
        }

        #past request data to save
        if ($quote = $this->quote->create(['data' => $data])) {

            #Subtract credits / record transaction
            Billing::recordTransaction($quote);

            /**
             * If Project has config Limit of quotes then lets Close it.
             */
            if (($project->quote_count+1) >= config('app.settings.quote_limit')) {

                $projects->status_fulfilled($project);
            }

            /**
             * SAVE FILES
             */
            MediaManager::uploadfiles($request, [get_class($quote), $quote->id]);

        }

        /**
         * Final redirect
         */
        if ($data['status'] == 1) {
            return redirect()->route('frontend.professional.dashboard')->withFlashSuccess('Quote has been sent');
        } else {
            return redirect()->route('frontend.account.credits_purchase', [access()->user()->professional->account->id, 'quote'=> $quote->id]);
        }
    }

    public function update(Quote $quote, Request $request)
    {

        $data = $request->all();

        $this->quote->update($quote, ['data' => $data]);

        /**
         * SAVE FILES
         */
        MediaManager::uploadfiles($request, [get_class($quote), $quote->id]);

        return redirect()->route('frontend.professional.quotes', [$quote->professional_id])->withFlashSuccess('Update success');
    }

    /**
     * Route clicked on by Client to confirm the Professionals request on his hire
     * @param Quote $quote
     * @return mixed
     */
    public function confirm(Quote $quote) {
        #Todo: implement SMS way to do this.
        if (access()->id() && access()->id() == $quote->project->user_id) {
            $quote = $this->quote->hired($quote);

            return redirect()->route('frontend.professional.manage', $quote->id)->withFlashSuccess('You have verified this hire. You may now leave a review.');
        }

        return redirect()->route('/')->withFlashDanger('You are not allowed todo that');
    }

}