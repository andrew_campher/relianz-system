<?php

namespace App\Http\Controllers\Frontend\Service;

use App\Repositories\Backend\Service\ServiceRepository;
use App\Models\Service\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{

    protected $services;

    /**
     * @param ServiceRepository $services
     */
    public function __construct(ServiceRepository $services)
    {
        $this->services = $services;
    }

}
