<?php

namespace App\Http\Controllers\Frontend\Service;

use App\Helpers\Helper;
use App\Models\Category\Category;
use App\Models\Location\Location;
use App\Models\Occupation\Occupation;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Repositories\Backend\Service\ServiceRepository;
use App\Models\Service\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicePublicController extends Controller
{

    protected $services;

    /**
     * @param ServiceRepository $services
     */
    public function __construct(ServiceRepository $services)
    {
        $this->services = $services;
    }

    /**
     * Details page of a Service
     * eg. /electricians
     * @param $pro_slug
     * @param Request $request
     * @return $this
     */
    public function show($slug, Request $request) {
        #get first one
        $service = Service::where('slug', $slug)->first();

        if ($service) {
            if (Helper::trackHit())
            Service::where('id', $service->id)->UpdateHits();
        } else {
            return response()->view('errors.404', [], 404);
        }

        if ($service) {
            return view('public.service.show')
                ->with(compact('service'));
        }

    }

    public function occupation($slug, Request $request) {
        #get first one
        $occupation = Occupation::where('slug', $slug)->first();
        if ($occupation) {
            if (Helper::trackHit())
                Occupation::where('id', $occupation->id)->UpdateHits();
        } else {
            return response()->view('errors.404', [], 404);
        }

        $professionals = Professional::WhereOccupation($occupation->id)->paginate(15);

        $projects = Project::WhereOccupation($occupation->id)->orderBy('id', 'desc')->limit(5)->get();

        if ($occupation) {
            return view('public.service.occupation')
                ->with(compact('occupation', 'professionals', 'projects'));
        }

    }

    public function all_occupations() {
        $categories = Category::where('parent_id', 0)->where('status', 1)->orderBy('hits', 'desc')->limit(6)->get();

        $occupations = Occupation::orderBy('title', 'asc')->paginate(50);

        return view('public.service.all_occupations')
            ->with(compact('occupations', 'categories'));
    }



    /**
     * Public Details page of a Location Service
     * eg. /electricians/bellville
     * @param $pro_slug
     * @param $location_slug
     * @return $this\
     */
    public function service_location($pro_slug, $location_slug) {
        $occupation = Occupation::where('slug', $pro_slug)->first();
        if (!$occupation)
            return redirect('/')->withFlashDanger('Service not found "'.$pro_slug.'"');

        $location = Location::where('slug', $location_slug)->first();
        if (!$location)
            return redirect('/')->withFlashDanger('Location not found "'.$location_slug.'"');


        if ($location) {
            if (Helper::trackHit())
            Location::where('id', $location->id)->UpdateHits();
        }
        if ($occupation) {
            if (Helper::trackHit())
            Occupation::where('id', $occupation->id)->UpdateHits();
        }

        switch ($location->type) {
            case "sublocality_level_2":
                $radius = 4;
                break;
            case "sublocality_level_1":
                $radius = 8;
                break;
            case "administrative_area_level_3":
                $radius = 40;
                break;
            case "administrative_area_level_2":
                $radius = 100;
                break;
            case "country":
                $radius = 1000;
                break;
            case "locality":
                $radius = 8;
                break;
        }

        $professionals = Professional::OccupationServiceRange($location->lat, $location->lng, $occupation->id)->simplePaginate(5);

        $projects = Project::DistanceLimitRadius($location->lat, $location->lng, $radius)->limit(5)->get();

        $nearby_locations = $location->nearby_locations(10)->get();

        return view('public.service.service_location')
            ->with(compact('occupation', 'location', 'nearby_locations', 'professionals', 'projects'));
    }

    /**
     * This Shows a Overview of a particular Location
     * @param $location_slug
     * @return $this
     */
    public function location($location_slug)
    {
        $location = Location::where('slug', $location_slug)->first();
        if ($location) {
            if (Helper::trackHit())
            Location::where('id', $location->id)->UpdateHits();
        }

        $categories = Category::where('parent_id', 0)->where('status', 1)->orderBy('hits', 'desc')->limit(6)->get();

        $occupations = Occupation::orderBy('hits', 'desc')->limit(30)->get();

        $suburbs = Location::where('parent_id', $location->id)->orderBy('hits', 'desc')->limit(32)->get();

        return view('public.service.location')
            ->with(compact('occupations', 'categories', 'suburbs', 'location'));
    }

    /**
     * Details page of a specific pros service
     * @param $pro_slug
     * @param $service
     * @return $this
     */
    public function pro_service($pro_slug, $service) {
        $service = Service::where('pro_slug', $pro_slug)->where('slug', $service)->first();

        if ($service) {
            if (Helper::trackHit())
            Service::where('id', $service->id)->UpdateHits();
        }

        return view('public.service.pro_service')
            ->with(compact('service'));
    }

}
