<?php

namespace App\Http\Controllers\Frontend\User;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Quote\Quote;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class DashboardController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        access()->user()->comments;

        $unopened_quotes_row = Quote::where('quotes.status', 1)
            ->join('projects', 'projects.id', '=', 'quotes.project_id')
            ->where('projects.user_id', access()->id())
            ->select('quotes.id', 'quotes.project_id')
            ->get()->toArray();

        $unopened_quotes = Helper::make_tree_array($unopened_quotes_row, 'project_id');

        $active_projects = access()->user()->projects()->ActiveProjects()->with('service')->get();

        $open_projects = access()->user()->projects()->OpenProjects()->with('service')->get();

        $projects_complete = access()->user()->projects()->FinishedProjects()->count();

        $expired_projects = access()->user()->projects()->ExpiredProjects()->with('service')->get();

        return view('frontend.user.dashboard')
            ->with(compact('active_projects', 'open_projects', 'projects_complete', 'unopened_quotes', 'expired_projects'));
    }
}