<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Models\Access\User\User;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Services\Images\Images;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class ProfileController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $user;

	/**
	 * ProfileController constructor.
	 * @param UserRepository $user
	 */
	public function __construct(UserRepository $user) {
		$this->user = $user;
	}

	/**
	 * @param UpdateProfileRequest $request
	 * @return mixed
	 */
	public function update(UpdateProfileRequest $request)
    {
        $data = $request->all();

        if ($imagename = Images::upload($request, ['image', (new User())->getTable(), 'fit' => true])) {
            $data['image'] = $imagename['image'];
        } else {
            #If no image then exclude image from update value array
            unset($data['image']);
        }

        $this->user->updateProfile(access()->id(), $data);
        return redirect()->route('frontend.user.account')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }
}