<?php

namespace App\Http\Controllers\Frontend\User;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;

/**
 * Class AccountController
 * @package App\Http\Controllers\Frontend
 */
class UserController extends Controller
{

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		return view('frontend.user.account');
	}

    public function contacts() {

        $contacts = access()->user()->contacts;

        return view('frontend.user.contacts')
            ->with(compact('contacts'));
    }

    public function isLoggedIn() {
        if(access()->id()) {
            return response([
                'status'=> 'success',
                'message'=> 'Welcome, '.access()->user()->name.'!',
            ], 200);
        } else {
            return response([
                'status'=> 'error',
                'message'=> 'Not logged in',
            ], 200);
        }
    }
}