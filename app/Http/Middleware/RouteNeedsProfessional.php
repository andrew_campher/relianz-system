<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class RouteNeedsRole
 * @package App\Http\Middleware
 */
class RouteNeedsProfessional
{

	/**
     * @param $request
     * @param Closure $next
     * @param $permission
     * @param bool $needsAll
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * Professional array
         */
        $access = access()->isRole('Professional');

        if (! $access) {
            return redirect()
                ->route('public.index')
                ->withFlashDanger('No professional account found');
        }

        return $next($request);
    }
}
