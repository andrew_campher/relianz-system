<?php

namespace App\Http\Requests\Backend\Invoice;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreInvoiceRequest
 * @package App\Http\Requests\Invoices
 */
class StoreInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount'     => 'required|filled',
            'account_id'     => 'required|filled',
            'description'     => 'required|filled',
        ];
    }
}
