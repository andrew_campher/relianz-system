<?php

namespace App\Http\Requests\Backend\Package;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StorePackageRequest
 * @package App\Http\Requests\Packages
 */
class StorePackageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        #Todo: add permissions
        return access()->allow('manage-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             => 'required|filled',
            'cost'              => 'numeric',
            'per_credit'        => 'numeric',
            'discount_percent'  => 'numeric',
            'free'              => 'numeric',
        ];
    }
}
