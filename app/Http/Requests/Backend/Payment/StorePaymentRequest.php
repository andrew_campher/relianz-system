<?php

namespace App\Http\Requests\Backend\Payment;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StorePaymentRequest
 * @package App\Http\Requests\Payments
 */
class StorePaymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount_paid'       => 'required|filled',
            'user_id'           => 'required|filled',
            'invoice_id'        => 'required|filled',
        ];
    }
}
