<?php

namespace App\Http\Requests\Backend\Project;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreProjectRequest
 * @package App\Http\Requests\Projects
 */
class StoreProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'     => 'required|filled',
            'service_id'     => 'required|filled',
        ];
    }
}
