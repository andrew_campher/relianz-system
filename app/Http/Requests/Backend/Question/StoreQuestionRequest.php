<?php

namespace App\Http\Requests\Backend\Question;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreQuestionRequest
 * @package App\Http\Requests\Backend\Access\Question
 */
class StoreQuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|filled|max:255',
            'question_type' => 'required|filled',
        ];
    }
}
