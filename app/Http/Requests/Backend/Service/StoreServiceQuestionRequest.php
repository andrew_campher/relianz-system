<?php

namespace App\Http\Requests\Backend\Service;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreServiceRequest
 * @package App\Http\Requests\Services
 */
class StoreServiceQuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_id' => 'required|filled',
            'service_id' => 'required|filled',
        ];
    }
}
