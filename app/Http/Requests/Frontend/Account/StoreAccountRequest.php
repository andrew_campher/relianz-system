<?php

namespace App\Http\Requests\Frontend\Account;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreAccountRequest
 * @package App\Http\Requests\Accounts
 */
class StoreAccountRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
