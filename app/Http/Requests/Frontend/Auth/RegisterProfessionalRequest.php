<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class RegisterProfessionalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|filled|max:255',
            'last_name' => 'required|filled|max:255',
            'email' => ['required','filled', 'email', 'max:255', Rule::unique('users')],
            'password' => 'required|filled|min:6|confirmed',
            'g-recaptcha-response' => 'required_if:captcha_status,true|captcha',

            'title'     => 'required|filled|max:255',
            'services.0'     => 'required|filled|max:255',
            'description' => 'required|filled',
            'branch.area' => 'required|filled',
            'logo'      => 'mimes:jpeg,jpg,png,gif|max:10000',
        ];
    }

	/**
     * @return array
     */
    public function messages() {
        return [
            'g-recaptcha-response.required_if' => trans('validation.required', ['attribute' => 'captcha']),
        ];
    }
}