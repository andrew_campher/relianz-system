<?php

namespace App\Http\Requests\Frontend\ClientReview;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreServiceRequest
 * @package App\Http\Requests\Services
 */
class StoreClientReviewRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id'            => 'required|filled',
            'user_id'               => 'required|filled|min:1',
            'quote_id'              => 'required|filled',
            'professional_id'        => 'required|filled',
            'rating_pay'             => 'required|filled',
            'rating_understanding'   => 'required|filled',
            'rating_clear'           => 'required|filled',
        ];
    }
}
