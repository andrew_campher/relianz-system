<?php

namespace App\Http\Requests\Frontend\Invoice;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreInvoiceRequest
 * @package App\Http\Requests\Invoices
 */
class StoreInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount'          => 'required_if:own_amount,|numeric|min:1',
            'own_amount'      => 'required_if:amount,|numeric|min:1',
            'description'      => 'required|filled',
            'account_id'      => 'numeric|min:1',
            'professional_id' => 'numeric|min:1',
            'user_id'         => 'numeric|min:1',
        ];
    }
}
