<?php

namespace App\Http\Requests\Frontend\Media;

use App\Http\Requests\Request;
use App\Models\Media\Media;

/**
 * Class ManageUserRequest
 * @package App\Http\Requests\Backend\Medias
 */
class ManageMediaRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		#TODO: implement permissions
        $media = $this->route('medium');

        if($media->user_id == access()->id() ||
            $media->model->user_id == access()->id() ||
            access()->allow('delete-media')) {

                return true;
        }
        return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            //
        ];
	}
}
