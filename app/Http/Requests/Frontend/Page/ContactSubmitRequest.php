<?php

namespace App\Http\Requests\Frontend\Page;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreServiceRequest
 * @package App\Http\Requests\Services
 */
class ContactSubmitRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|filled',
            'email'                  => 'required|filled',
            'tel'                   => 'required|filled|min:1',
            'message'               => 'required|filled',
            'g-recaptcha-response'  => 'required_if:captcha_status,true|captcha',
        ];
    }
}
