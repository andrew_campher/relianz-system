<?php

namespace App\Http\Requests\Frontend\Payment;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreInvoiceRequest
 * @package App\Http\Requests\Invoices
 */
class ProcessPaymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'credit_quantity'          => 'required|filled',
            'payment_method'   => 'required|filled',
            'account_id'   => 'required|filled',
            'package_id'   => 'required|filled',
        ];
    }
}
