<?php

namespace App\Http\Requests\Frontend\Professional;

use App\Http\Requests\Request;

/**
 * Class ManageUserRequest
 * @package App\Http\Requests\Backend\Professionals
 */
class ManageProfessionalRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{

	    if (access()->id() && access()->id() && access()->user()->hasRole(1)) {
	        return true;
        }
        if ($this->professional->approved || access()->id() == $this->professional->user_id) {
            return true;
        }

        return false;

	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            //
        ];
	}
}
