<?php

namespace App\Http\Requests\Frontend\Professional;

use App\Http\Requests\Request;

/**
 * Class ManageUserRequest
 * @package App\Http\Requests\Backend\Professionals
 */
class UpdateProfessionalRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		#TODO: implement permissions
        #return access()->allow('create-professional');
        return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            'title'     => 'required|filled|max:255',
            'services'     => 'required|filled',
        ];
	}
}
