<?php

namespace App\Http\Requests\Frontend\Project;

use App\Http\Requests\Request;

/**
 * Class ManageUserRequest
 * @package App\Http\Requests\Backend\Projects
 */
class ManageProjectRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		#If this project is his let him Manage
        if ($this->project->user_id == access()->id()) {
            return true;
        } else {
            return false;
        }

	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            //
        ];
	}
}
