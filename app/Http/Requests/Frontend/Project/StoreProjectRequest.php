<?php

namespace App\Http\Requests\Frontend\Project;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreProjectRequest
 * @package App\Http\Requests\Projects
 */
class StoreProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'when'              => 'required|filled',
            'area'              => 'required|filled',
            'title'              => 'required|filled',
            'service_id'        => 'required|filled',
            'customer_email'    => 'required|filled',
        ];
    }
}
