<?php

namespace App\Http\Requests\Frontend\ProjectReview;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreServiceRequest
 * @package App\Http\Requests\Services
 */
class StoreProjectReviewPublicRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'required|filled|max:255',
            'last_name' => 'required|filled|max:255',
            'email' => ['required', 'email', 'max:255', Rule::unique('users')],
            'password' => 'required|filled|min:6|confirmed',
            'professional_id'       => 'required|filled',
            'rating_overall'        => 'required|filled',
            'rating_cooperative'    => 'required|filled',
            'rating_quality'        => 'required|filled',
            'rating_time'           => 'required|filled',
            'rating_price'          => 'required|filled',
            'g-recaptcha-response' => 'required_if:captcha_status,true|captcha',
        ];
    }

    /**
     * @return array
     */
    public function messages() {
        return [
            'g-recaptcha-response.required_if' => trans('validation.required', ['attribute' => 'captcha']),
        ];
    }
}
