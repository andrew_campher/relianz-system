<?php

namespace App\Http\Requests\Frontend\ProjectReview;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreServiceRequest
 * @package App\Http\Requests\Services
 */
class StoreProjectReviewRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'               => 'required|filled',
            'professional_id'       => 'required|filled',
            'rating_overall'        => 'required|filled',
            'rating_cooperative'    => 'required|filled',
            'rating_quality'        => 'required|filled',
            'rating_time'           => 'required|filled',
            'rating_price'          => 'required|filled',
        ];
    }
}
