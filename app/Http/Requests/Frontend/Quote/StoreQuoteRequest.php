<?php

namespace App\Http\Requests\Frontend\Quote;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreQuoteRequest
 * @package App\Http\Requests\Quotes
 */
class StoreQuoteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price'           => 'required_if:need_more_info,|numeric|min:1',
            'price_type'      => 'required_if:need_more_info,|required',
            'description'     => 'required|filled',

            'user_id'           => 'required|filled',
            'professional_id'   => 'required|filled',
            'project_id'        => 'required|filled',
            'service_id'        => 'required|filled',
        ];
    }
}
