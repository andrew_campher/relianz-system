<?php

namespace App\Listeners\Common\ClientReview;
use App\Notifications\ClientReview\UserNotifyNewClientReview;

/**
 * Class ClientReviewEventListener
 * @package App\Listeners\Common\ClientReview
 */
class ClientReviewEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'ClientReview';

	/**
	 * @param $event
	 */
	public function onCreated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.client_review.created")',
            'create',
			$event->client_review->id,
			'plus',
			'bg-green'
		);

        /**
         * Notify Professional of Review
         */
        $event->client_review->user->notify(new UserNotifyNewClientReview($event->client_review));
	}


	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			\App\Events\Common\ClientReview\ClientReviewCreated::class,
			'App\Listeners\Common\ClientReview\ClientReviewEventListener@onCreated'
		);

	}
}