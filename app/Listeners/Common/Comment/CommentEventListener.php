<?php

namespace App\Listeners\Common\Comment;
use App\Models\Comment\Comment;
use App\Models\Quote\Quote;
use App\Notifications\Comment\UserNotifyComment;

/**
 * Class CommentEventListener
 * @package App\Listeners\Common\Comment
 */
class CommentEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Comment';

    public function onCreated($event) {

        switch ($event->comment->comments_room->roomtable_type) {
            case "quote":
                /**
                 * Notify users of new Comment
                 */

                $quote = Quote::find($event->comment->comments_room->roomtable_id);

                #IF comment is FROM CLIENT
                if ($quote->project->user_id == $event->comment->user_id) {
                    #Update quote to INteracted status if not yet
                    if ($quote->status == 1 || $quote->status == 2) {
                        $quote->status = 6;
                        $quote->save();
                    }
                    #only notify if NOT online
                    $quote->user->notify(new UserNotifyComment($event->comment));
                }
                elseif ($quote->user_id == $event->comment->user_id) {
                    #ELSE if from PROFESSIONAL
                    /**
                     * ONLY notify Client if on applicable status
                     */
                    $quote->project->user->notify(new UserNotifyComment($event->comment));

                }

            break;
            default:
                $event->comment->comments_room->roomtable->notify(new UserNotifyComment($event->comment));
            break;
        }

    }
	
	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			\App\Events\Common\Comment\CommentCreated::class,
			'App\Listeners\Common\Comment\CommentEventListener@onCreated'
		);

	}
}