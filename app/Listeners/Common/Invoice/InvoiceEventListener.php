<?php

namespace App\Listeners\Common\Invoice;
use App\Notifications\Invoice\UserNotifyInvoice;
use App\Notifications\Invoice\UserNotifyInvoiceSettled;

/**
 * Class InvoiceEventListener
 * @package App\Listeners\Common\Invoice
 */
class InvoiceEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Invoice';

    public function onSettled($event) {
        history()->log(
            $this->history_slug,
            'trans("history.invoice.settled")',
            'create',
            $event->invoice->id,
            'plus',
            'bg-green'
        );

        $event->invoice->user->notify(new UserNotifyInvoiceSettled($event->invoice));
    }

	/**
	 * @param $event
	 */
	public function onCreated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.invoice.created")',
            'create',
			$event->invoice->id,
			'plus',
			'bg-green'
		);

        $event->invoice->user->notify(new UserNotifyInvoice($event->invoice));
	}

	/**
	 * @param $event
	 */
	public function onUpdated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.invoice.updated") '.$event->invoice->professional->title,
            'update',
			$event->invoice->id,
			'save',
			'bg-aqua'
		);
	}



	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
        $events->listen(
            \App\Events\Common\Invoice\InvoiceSettled::class,
            'App\Listeners\Common\Invoice\InvoiceEventListener@onSettled'
        );

		$events->listen(
			\App\Events\Common\Invoice\InvoiceCreated::class,
			'App\Listeners\Common\Invoice\InvoiceEventListener@onCreated'
		);

		$events->listen(
			\App\Events\Common\Invoice\InvoiceUpdated::class,
			'App\Listeners\Common\Invoice\InvoiceEventListener@onUpdated'
		);

	}
}