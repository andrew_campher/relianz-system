<?php

namespace App\Listeners\Common\Notification;
use Illuminate\Support\Facades\DB;

/**
 * Class NotificationEventListener
 * @package App\Listeners\Common\Notification
 */
class NotificationEventListener
{
	/**
	 * @var string
	 */

    public function onSent($event) {

        /**
         * UPDATE the recorded sent_email table with entity type and id.
         */
        #TODO: record for all channel types

        if ($event->channel == 'mail') {
            $notifiable_type = snake_case(class_basename(get_class($event->notifiable)));

            $notification_class = class_basename(get_class($event->notification));

            $entity_type = null;
            $entity_id = null;

            if (isset($event->notification->entity->id)) {
                $entity_type = snake_case(class_basename(get_class($event->notification->entity)));
                $entity_id = $event->notification->entity->id;
            }

            DB::table('sent_emails')
                ->where('recipient', (isset($event->notifiable->email) && $event->notifiable->email?$event->notifiable->email:$event->notifiable->routeNotificationForMail()))
                ->limit(1)
                ->orderBy('id', 'desc')
                ->update([
                    'entity_type' => $entity_type,
                    'entity_id' => $entity_id,
                    'notifiable_type' => $notifiable_type,
                    'notifiable_id' => $event->notifiable->id,
                    'notification_class' => $notification_class,
                    'notification_type' => $event->channel,
                ]);
        }
    }

}