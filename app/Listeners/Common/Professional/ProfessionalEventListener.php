<?php

namespace App\Listeners\Common\Professional;
use App\Notifications\Professional\NotifyProfessionalVerified;
use App\Notifications\Professional\UserNotifyProfessionalApprove;

/**
 * Class ProfessionalEventListener
 * @package App\Listeners\Common\Professional
 */
class ProfessionalEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Professional';

    public function onVerified($event) {
        history()->log(
            $this->history_slug,
            'trans("history.professional.verified")',
            'verified',
            $event->professional->id,
            'plus',
            'bg-green'
        );

        $event->professional->user->notify(new NotifyProfessionalVerified($event->professional));

    }

    public function onApproved($event) {
        history()->log(
            $this->history_slug,
            'trans("history.professional.created") '.$event->professional->title,
            'approved',
            $event->professional->id,
            'plus',
            'bg-green'
        );

        $event->professional->user->notify(new UserNotifyProfessionalApprove($event->professional));

    }

	/**
	 * @param $event
	 */
	public function onCreated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.professional.created") '.$event->professional->title,
            'create',
			$event->professional->id,
			'plus',
			'bg-green'
		);
	}

	/**
	 * @param $event
	 */
	public function onUpdated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.professional.updated") '.$event->professional->title,
            'update',
			$event->professional->id,
			'save',
			'bg-aqua'
		);
	}

	/**
	 * @param $event
	 */
	public function onDeleted($event) {
		history()->log(
			$this->history_slug,
			'trans("history.professional.deleted") '.$event->professional->title,
            'delete',
			$event->professional->id,
			'trash',
			'bg-maroon'
		);
	}

	/**
	 * @param $event
	 */
	public function onRestored($event) {
		history()->log(
			$this->history_slug,
			'trans("history.professional.restored") '.$event->professional->title,
            'restore',
			$event->professional->id,
			'refresh',
			'bg-aqua'
		);
	}

	/**
	 * @param $event
	 */
	public function onPermanentlyDeleted($event) {
		history()->log(
			$this->history_slug,
			'trans("history.professional.permanently_deleted") '.$event->professional->title,
            'destroy',
			$event->professional->id,
			'trash',
			'bg-maroon'
		);
	}

	/**
	 * @param $event
	 */
	public function onDeactivated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.professional.deactivated") '.$event->professional->title,
            'deactivate',
			$event->professional->id,
			'times',
			'bg-yellow'
		);
	}

	/**
	 * @param $event
	 */
	public function onReactivated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.professional.reactivated") '.$event->professional->title,
            'reactivate',
			$event->professional->id,
			'check',
			'bg-green'
		);
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
        $events->listen(
            \App\Events\Common\Professional\ProfessionalVerified::class,
            'App\Listeners\Common\Professional\ProfessionalEventListener@onVerified'
        );

        $events->listen(
            \App\Events\Common\Professional\ProfessionalApproved::class,
            'App\Listeners\Common\Professional\ProfessionalEventListener@onApproved'
        );

		$events->listen(
			\App\Events\Common\Professional\ProfessionalCreated::class,
			'App\Listeners\Common\Professional\ProfessionalEventListener@onCreated'
		);

		$events->listen(
			\App\Events\Common\Professional\ProfessionalUpdated::class,
			'App\Listeners\Common\Professional\ProfessionalEventListener@onUpdated'
		);

		$events->listen(
			\App\Events\Common\Professional\ProfessionalDeleted::class,
			'App\Listeners\Common\Professional\ProfessionalEventListener@onDeleted'
		);

		$events->listen(
			\App\Events\Common\Professional\ProfessionalDeactivated::class,
			'App\Listeners\Common\Professional\ProfessionalEventListener@onDeactivated'
		);

		$events->listen(
			\App\Events\Common\Professional\ProfessionalReactivated::class,
			'App\Listeners\Common\Professional\ProfessionalEventListener@onReactivated'
		);
	}
}