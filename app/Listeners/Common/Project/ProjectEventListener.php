<?php

namespace App\Listeners\Common\Project;
use App\Models\Access\User\User;
use App\Notifications\Project\AdminNotifyNewProject;
use App\Notifications\Project\ProjectClosedNotifyProfessional;
use App\Notifications\Project\UserNotifyProjectApproved;
use App\Notifications\Project\UserNotifyProjectCreated;
use App\Notifications\Project\UserNotifyProjectExpired;
use Illuminate\Support\Facades\Notification;

/**
 * Class ProjectEventListener
 * @package App\Listeners\Common\Project
 */
class ProjectEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Project';

    public function onApproved($event) {
        history()->log(
            $this->history_slug,
            'trans("history.project.approved") ',
            'create',
            $event->project->id,
            'plus',
            'bg-green'
        );

        #$event->project->user->notify(new UserNotifyProjectApproved($event->project));
    }

    public function onExpired($event) {
        history()->log(
            $this->history_slug,
            'trans("history.project.expired") ',
            'expired',
            $event->project->id,
            'plus',
            'bg-green'
        );

        $event->project->user->notify(new UserNotifyProjectExpired($event->project));
    }

	/**
	 * @param $event
	 */
	public function onCreated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project.created") ',
            'create',
			$event->project->id,
			'plus',
			'bg-green'
		);


        $admins = User::IsAdmins()->get();
        Notification::send($admins, new AdminNotifyNewProject($event->project));

        #notify user of new project
        $event->project->user->notify(new UserNotifyProjectCreated($event->project));
	}

    public function onClosed($event) {
        history()->log(
            $this->history_slug,
            'trans("history.project.closed") ',
            'closed',
            $event->project->id,
            'plus',
            'bg-green'
        );

        if($event->project->quotes)
        {
            foreach ($event->project->quotes AS $quote)
            {
                $quote->user->notify(new ProjectClosedNotifyProfessional($event->project, $event->data));
            }
        }

    }

    public function onFulfilled($event) {
        history()->log(
            $this->history_slug,
            'trans("history.project.fulfilled")',
            'fulfilled',
            $event->project->id,
            'plus',
            'bg-green'
        );

        //$event->project->user->notify(new UserNotifyProjectFulfilled($event->project));
    }

	/**
	 * @param $event
	 */
	public function onUpdated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project.updated") ',
            'update',
			$event->project->id,
			'save',
			'bg-aqua'
		);
	}

	/**
	 * @param $event
	 */
	public function onDeleted($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project.deleted") '.$event->project->service->title,
            'delete',
			$event->project->id,
			'trash',
			'bg-maroon'
		);
	}

	/**
	 * @param $event
	 */
	public function onRestored($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project.restored") '.$event->project->service->title,
            'restore',
			$event->project->id,
			'refresh',
			'bg-aqua'
		);
	}

	/**
	 * @param $event
	 */
	public function onPermanentlyDeleted($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project.permanently_deleted") '.$event->project->service->title,
            'destroy',
			$event->project->id,
			'trash',
			'bg-maroon'
		);
	}

	/**
	 * @param $event
	 */
	public function onDeactivated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project.deactivated") '.$event->project->service->title,
            'deactivate',
			$event->project->id,
			'times',
			'bg-yellow'
		);
	}

	/**
	 * @param $event
	 */
	public function onReactivated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project.reactivated") '.$event->project->service->title,
            'reactivate',
			$event->project->id,
			'check',
			'bg-green'
		);
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{

        $events->listen(
            \App\Events\Common\Project\ProjectApproved::class,
            'App\Listeners\Common\Project\ProjectEventListener@onApproved'
        );

		$events->listen(
			\App\Events\Common\Project\ProjectCreated::class,
			'App\Listeners\Common\Project\ProjectEventListener@onCreated'
		);

		$events->listen(
			\App\Events\Common\Project\ProjectUpdated::class,
			'App\Listeners\Common\Project\ProjectEventListener@onUpdated'
		);

		$events->listen(
			\App\Events\Common\Project\ProjectDeleted::class,
			'App\Listeners\Common\Project\ProjectEventListener@onDeleted'
		);

		$events->listen(
			\App\Events\Common\Project\ProjectDeactivated::class,
			'App\Listeners\Common\Project\ProjectEventListener@onDeactivated'
		);

		$events->listen(
			\App\Events\Common\Project\ProjectReactivated::class,
			'App\Listeners\Common\Project\ProjectEventListener@onReactivated'
		);

        $events->listen(
            \App\Events\Common\Project\ProjectExpired::class,
            'App\Listeners\Common\Project\ProjectEventListener@onExpired'
        );

        $events->listen(
            \App\Events\Common\Project\ProjectClosed::class,
            'App\Listeners\Common\Project\ProjectEventListener@onClosed'
        );

        $events->listen(
            \App\Events\Common\Project\ProjectFulfilled::class,
            'App\Listeners\Common\Project\ProjectEventListener@onFulfilled'
        );

        
	}
}