<?php

namespace App\Listeners\Common\ProjectReview;
use App\Notifications\ProjectReview\AdminNotifyNewProjectReview;
use App\Notifications\ProjectReview\UserNotifyNewProjectReview;
use Illuminate\Support\Facades\Notification;

/**
 * Class ProjectReviewEventListener
 * @package App\Listeners\Common\ProjectReview
 */
class ProjectReviewEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'ProjectReview';

	/**
	 * @param $event
	 */
	public function onCreated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.project_review.created")',
            'create',
			$event->project_review->id,
			'plus',
			'bg-green'
		);

        /**
         * Notify Professional of Review
         */
        $event->project_review->professional->notify(new UserNotifyNewProjectReview($event->project_review));
	}

    public function onPublicCreated($event) {
        history()->log(
            $this->history_slug,
            'trans("history.project_review.public_created")',
            'create',
            $event->project_review->id,
            'plus',
            'bg-green'
        );

        /**
         * Notify Professional of Review
         */
        $admins = User::IsAdmins()->get();
        Notification::send($admins, new AdminNotifyNewProjectReview($event->project_review));
    }


	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			\App\Events\Common\ProjectReview\ProjectReviewCreated::class,
			'App\Listeners\Common\ProjectReview\ProjectReviewEventListener@onCreated'
		);

        $events->listen(
            \App\Events\Common\ProjectReview\ProjectReviewPublicCreated::class,
            'App\Listeners\Common\ProjectReview\ProjectReviewEventListener@onPublicCreated'
        );

	}
}