<?php

namespace App\Listeners\Common\Quote;
use App\Notifications\Quote\NotifyQuoteUnsuccessful;
use App\Notifications\Quote\UserNotifyNewQuote;
use App\Notifications\Quote\UserNotifyQuoteHired;
use App\Notifications\Quote\UserNotifyReadQuote;
use App\Notifications\Quote\UserNotifyConfirmHired;

/**
 * Class QuoteEventListener
 * @package App\Listeners\Common\Quote
 */
class QuoteEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Quote';

    /**
     * This Event fires off when the quote was unsuccessful
     * @param $event
     */
    public function onUnsuccessful($event) {
        history()->log(
            $this->history_slug,
            'trans("history.quote.unsuccessful") '.(isset($event->quote->decline_reason) ? $event->quote->decline_reason: '' ),
            $event->quote->status,
            $event->quote->id,
            'plus',
            'bg-green'
        );

        /**
         * Notify users of read quote
         */
        $event->quote->user->notify(new NotifyQuoteUnsuccessful($event->quote));
    }

    /**
     * When the Client opens the quote for the first time this event fires off
     * @param $event
     */
    public function onRead($event) {
        history()->log(
            $this->history_slug,
            'trans("history.quote.read") ',
            'read',
            $event->quote->id,
            'plus',
            'bg-green'
        );

        /**
         * Notify users of read quote
         */
        $event->quote->user->notify(new UserNotifyReadQuote($event->quote));
    }

    /**
     * This Event fires off when the Professional marks the quote as hired - THis requires the Client to CONFIRM it first.
     * @param $event
     */
    public function onConfirmHired($event) {
        history()->log(
            $this->history_slug,
            'trans("history.quote.confirm_hired") ',
            'verify_hired',
            $event->quote->id,
            'plus',
            'bg-green'
        );

        /**
         * Notify Client to approve
         */
        $event->quote->project->user->notify(new UserNotifyConfirmHired($event->quote));
    }


    public function onHired($event) {
        history()->log(
            $this->history_slug,
            'trans("history.quote.hired") ',
            'hired',
            $event->quote->id,
            'plus',
            'bg-green'
        );

        /**
         * Record Project Status Update
         */
        history()->log(
            'Project',
            'trans("history.project.hired") ',
            'hired',
            $event->quote->project->id,
            'plus',
            'bg-green'
        );

        #CLIENT
        /**
         * Notify Professional
         */
        $event->quote->user->notify(new UserNotifyQuoteHired($event->quote));
    }

	/**
	 * @param $event
	 */
	public function onCreated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.quote.created") ',
            'create',
			$event->quote->id,
			'plus',
			'bg-green'
		);

        history()->log(
            'Project',
            'trans("history.project.new_quote") ',
            'new_quote',
            $event->quote->project_id,
            'plus',
            'bg-green',
            [$event->quote->id]
        );

        /**
         * Notify user of project there is new quote
         */
        $event->quote->project->user->notify(new UserNotifyNewQuote($event->quote));
	}

	/**
	 * @param $event
	 */
	public function onUpdated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.quote.updated") ',
            'update',
			$event->quote->id,
			'save',
			'bg-aqua'
		);
	}

	/**
	 * @param $event
	 */
	public function onDeleted($event) {
		history()->log(
			$this->history_slug,
			'trans("history.quote.deleted") ',
            'delete',
			$event->quote->id,
			'trash',
			'bg-maroon'
		);
	}


	/**
	 * @param $event
	 */
	public function onPermanentlyDeleted($event) {
		history()->log(
			$this->history_slug,
			'trans("history.quote.permanently_deleted") ',
            'destroy',
			$event->quote->id,
			'trash',
			'bg-maroon'
		);
	}


	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
        $events->listen(
            \App\Events\Common\Quote\QuoteComment::class,
            'App\Listeners\Common\Quote\QuoteEventListener@onComment'
        );

        $events->listen(
            \App\Events\Common\Quote\QuoteConfirmHired::class,
            'App\Listeners\Common\Quote\QuoteEventListener@onConfirmHired'
        );

        $events->listen(
            \App\Events\Common\Quote\QuoteHired::class,
            'App\Listeners\Common\Quote\QuoteEventListener@onHired'
        );

        $events->listen(
            \App\Events\Common\Quote\QuoteRead::class,
            'App\Listeners\Common\Quote\QuoteEventListener@onRead'
        );

        $events->listen(
            \App\Events\Common\Quote\QuoteUnsuccessful::class,
            'App\Listeners\Common\Quote\QuoteEventListener@onUnsuccessful'
        );

		$events->listen(
			\App\Events\Common\Quote\QuoteCreated::class,
			'App\Listeners\Common\Quote\QuoteEventListener@onCreated'
		);

		$events->listen(
			\App\Events\Common\Quote\QuoteUpdated::class,
			'App\Listeners\Common\Quote\QuoteEventListener@onUpdated'
		);

		$events->listen(
			\App\Events\Common\Quote\QuoteDeleted::class,
			'App\Listeners\Common\Quote\QuoteEventListener@onDeleted'
		);

	}
}