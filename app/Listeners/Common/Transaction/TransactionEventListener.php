<?php

namespace App\Listeners\Common\Transaction;
use App\Notifications\Transaction\AdminNotifyNewTransaction;
use App\Notifications\Transaction\UserNotifyNewTransaction;
use Illuminate\Support\Facades\Notification;

/**
 * Class TransactionEventListener
 * @package App\Listeners\Common\Transaction
 */
class TransactionEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Transaction';

	/**
	 * @param $event
	 */
	public function onRefunded($event) {
		history()->log(
			$this->history_slug,
			'trans("history.transaction.created")',
            'refund',
			$event->transaction->id,
			'plus',
			'bg-green'
		);

        /**
         * Notify Professional of Review
         */
        $event->transaction->user->notify(new TransactionRefundedNotify($event->transaction));
	}


    /**
     * @param $events
     */
	public function subscribe($events)
	{
		$events->listen(
			\App\Events\Common\Transaction\TransactionRefunded::class,
			'App\Listeners\Common\Transaction\TransactionEventListener@onRefunded'
		);

	}
}