<?php

namespace App\Models\Access\User\Traits\Relationship;

use App\Models\Access\User\SocialLogin;
use App\Models\Access\User\User;
use App\Models\ClientReview\ClientReview;
use App\Models\CommentRoomParticipant\CommentRoomParticipant;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\Quote\Quote;
use App\Models\UserStat\UserStat;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait UserRelationship
{

    public function social_logins()
    {
        return $this->hasMany(SocialLogin::class);
    }

    public function reviews()
    {
        return $this->hasMany(ClientReview::class);
    }

    public function quotes()
    {
        return $this->hasManyThrough(Quote::class, Project::class);
    }

    public function friends()
    {
        return $this->belongsToMany(User::class, 'friends', 'friend_id')->where('accepted', 1);
    }

    public function pending_friends()
    {
        return $this->belongsToMany(User::class, 'friends', 'friend_id')->where('accepted', 0);
    }

    public function stats()
    {
        return $this->hasOne(UserStat::class);
    }

    /**
     * For getting all the contacts (professionals) for this user
     * @return mixed
     */
    public function contacts()
    {
        return $this->belongsToMany(Professional::class);
    }

    /**
     * Many-to-Many relations with Projects.
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    /**
     * Many-to-Many relations with Professional Profile.
     */
    public function professional()
    {
        return $this->hasOne(Professional::class);
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.assigned_roles_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * @return mixed
     */
    public function comments($limit = 10)
    {
        return $this->hasMany(CommentRoomParticipant::class)
            ->leftjoin('comments', 'comments.comments_room_id' , '=', 'comments_room_participants.comments_room_id')
            ->leftjoin('comments_room', 'comments_room.id' , '=', 'comments_room_participants.comments_room_id')
            ->select('comments.*', 'last_read', 'roomtable_type', 'roomtable_id')
            ->where('comments.user_id', '!=', access()->id())
            ->orderBy('comments.id', 'desc')
            ->limit($limit);
    }

    /**
     * @return mixed
     */
    public function unread_comment_rooms()
    {
        return $this->hasMany(CommentRoomParticipant::class)
            ->join('comments_room', 'comments_room.id' , '=', 'comments_room_participants.comments_room_id')
            ->leftJoin('comments',function($query){
                $query->on('comments.id', '=', DB::raw('(SELECT id FROM comments WHERE comments.comments_room_id = comments_room.id ORDER BY id DESC LIMIT 1)'));
            })
            ->select('comments.*', 'last_read', 'roomtable_id', 'roomtable_type')
            ->whereNull('last_read')
            ->orderBy('comments.id', 'desc');
    }
}