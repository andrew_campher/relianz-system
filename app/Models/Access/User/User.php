<?php

namespace App\Models\Access\User;

use Illuminate\Notifications\Notifiable;
use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\UserSendPasswordReset;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable
{
    use UserScope,
		UserAccess,
		Notifiable,
		SoftDeletes,
		UserAttribute,
		UserRelationship,
		UserSendPasswordReset;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'image',
        'email',
        'mobile',
        'password',
        'status',
        'confirmation_code',
        'confirmed',
        'lat',
        'lng',
        'area',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $appends = array('icon_url');

	/**
	 * @param array $attributes
	 */
	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
		$this->table = config('access.users_table');
	}

    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function routeNotificationForNexmo()
    {
        $format_mobile = substr($this->mobile, 1);
        $format_mobile = '+27'.$format_mobile;
        return $format_mobile;
    }

	public function IsContact($professional_id)
    {
        return DB::table('professional_user')->where('user_id', $this->id)->where('professional_id', $professional_id)->first();
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }
}
