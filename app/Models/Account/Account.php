<?php

namespace App\Models\Account;

use App\Models\Account\Traits\Attribute\AccountAttribute;
use App\Models\Account\Traits\Relationship\AccountRelationship;
use App\Models\Account\Traits\Scope\AccountScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use AccountScope,
        SoftDeletes,
        AccountAttribute,
        AccountRelationship;
    //
    protected $fillable = [
        'credits_remain',
        'professional_id',
        'package_id',
        'status',
    ];

    protected $dates = ['deleted_at', 'created_at'];

    protected $table = 'accounts';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
