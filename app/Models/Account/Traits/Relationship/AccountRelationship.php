<?php

namespace App\Models\Account\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\ClientReview\ClientReview;
use App\Models\Comment\Comment;
use App\Models\CommentRoom\CommentRoom;
use App\Models\Media\Media;
use App\Models\Package\Package;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\ProjectReview\ProjectReview;
use App\Models\Service\Service;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait AccountRelationship
{

    public function professional()
    {
        return $this->belongsTo(Professional::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

}