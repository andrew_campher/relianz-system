<?php

namespace App\Models\Category;

use App\Models\Category\Traits\Attribute\CategoryAttribute;
use App\Models\Category\Traits\Relationship\CategoryRelationship;
use App\Models\Category\Traits\Scope\CategoryScope;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use CategoryScope,
        SoftDeletes,
        CategoryAttribute,
        CategoryRelationship,
        Sluggable;
    //
    protected $fillable = [
        'title',
        'parent_id',
        'slug',
        'tags',
        'icon',
        'level',
        'status'
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = 'categories';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
