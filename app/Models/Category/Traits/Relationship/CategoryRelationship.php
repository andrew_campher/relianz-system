<?php

namespace App\Models\Category\Traits\Relationship;
use App\Models\Category\Category;
use App\Models\Service\Service;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait CategoryRelationship
{

    /**
     * Many-to-Many relations with Categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id')->orderBy('title', 'asc');
    }


    /**
     * Get services for catgegory
     *
     * @return mixed
     */
    public function services()
    {
        return $this->belongsToMany(Service::class)->orderBy('title', 'asc');
    }

}