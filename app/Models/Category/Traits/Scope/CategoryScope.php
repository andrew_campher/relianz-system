<?php

namespace App\Models\Category\Traits\Scope;
use App\Helpers\Helper;
use Illuminate\Support\Facades\DB;

/**
 * Class UserScope
 * @package App\Models\Access\User\Traits\Scope
 */
trait CategoryScope
{

	/**
	 * @param $query
	 * @param bool $confirmed
	 * @return mixed
	 */
	public function scopeConfirmed($query, $confirmed = true) {
		return $query->where('confirmed', $confirmed);
	}

	/**
	 * @param $query
	 * @param bool $status
	 * @return mixed
	 */
	public function scopeActive($query, $status = true) {
		return $query->where('status', $status);
	}

    /**
     * Get the Categories with their services array in Tree Format (parent etc)
     * @param $query
     * @return array|bool
     */
    public function scopeServicelist($query) {
        return Helper::make_tree_array($query->with('services')->orderBy('title', 'asc')->get()->toArray(), 'parent_id');
    }


    public function scopeUpdateHits($query) {
        return $query->update([
            'hits'      => DB::raw('hits + 1'),
            'hits_day'  => DB::raw('hits_day + 1'),
            'hits_month'=> DB::raw('hits_month + 1'),
            'hits_year' => DB::raw('hits_year + 1'),
        ]);
    }



}