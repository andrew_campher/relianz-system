<?php

namespace App\Models\ClientReview;

use App\Models\ClientReview\Traits\Attribute\ClientReviewAttribute;
use App\Models\ClientReview\Traits\Relationship\ClientReviewRelationship;
use App\Models\ClientReview\Traits\Scope\ClientReviewScope;
use Illuminate\Database\Eloquent\Model;

class ClientReview extends Model
{
    use ClientReviewScope,
        ClientReviewAttribute,
        ClientReviewRelationship;
    //
    protected $fillable = [
        'project_id',
        'professional_id',
        'quote_id',
        'user_id',
        'comments',
        'recommended',
        'rating_pay',
        'rating_understanding',
        'rating_clear',
        'status',
    ];

    protected $table = 'users_reviews';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
