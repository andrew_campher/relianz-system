<?php

namespace App\Models\Comment;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Comment\Traits\Attribute\CommentAttribute;
use App\Models\Comment\Traits\Relationship\CommentRelationship;
use App\Models\Comment\Traits\Scope\CommentScope;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use CommentScope,
        SoftDeletes,
        CommentAttribute,
        CommentRelationship;
    //

    /**
     * @var array
     */
    protected $dates = ['deleted_at', 'read_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';


    protected $appends = array('human_date');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
        'user_id',
        'comments_room_id',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
