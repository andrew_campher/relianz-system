<?php

namespace App\Models\Comment\Traits\Attribute;


trait CommentAttribute
{

    public function getHumanDateAttribute() {
        return $this->created_at->diffForHumans();
    }

}