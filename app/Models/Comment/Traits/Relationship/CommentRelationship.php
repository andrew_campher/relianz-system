<?php

namespace App\Models\Comment\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\CommentRoom\CommentRoom;
use App\Models\Media\Media;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait CommentRelationship
{

    public function comments_room()
    {
        return $this->belongsTo(CommentRoom::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select('first_name','last_name', 'id', 'image', 'name');
    }

    public function media()
    {
        return $this->morphMany(Media::class, 'modelname', null, 'modelname_id', 'id')->where('group','general')->select('filename', 'id', 'details', 'title', 'modelname_id', 'modelname_type');
    }

}