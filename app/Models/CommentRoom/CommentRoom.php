<?php

namespace App\Models\CommentRoom;

use App\Models\CommentRoom\Traits\Relationship\CommentRoomRelationship;
use Illuminate\Database\Eloquent\Model;

class CommentRoom extends Model
{
    use CommentRoomRelationship;
    //

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments_room';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'roomtable_id',
        'roomtable_type',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
