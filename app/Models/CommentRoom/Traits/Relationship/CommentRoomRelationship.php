<?php

namespace App\Models\CommentRoom\Traits\Relationship;
use App\Models\Comment\Comment;
use App\Models\CommentRoomParticipant\CommentRoomParticipant;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait CommentRoomRelationship
{
    /**
     * Get all of the owning roomtable models.
     */
    public function roomtable()
    {
        return $this->morphTo();
    }

    public function participants()
    {
        return $this->hasMany(CommentRoomParticipant::class, 'comments_room_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'comments_room_id')->orderBy('id', 'desc');
    }

    public function unread_comments()
    {
        return $this->hasMany(Comment::class, 'comments_room_id')->where('read_at', null);
    }




}