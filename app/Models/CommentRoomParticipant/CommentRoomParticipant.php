<?php

namespace App\Models\CommentRoomParticipant;

use App\Models\CommentRoomParticipant\Traits\Relationship\CommentRoomParticipantRelationship;
use Illuminate\Database\Eloquent\Model;

class CommentRoomParticipant extends Model
{
    use CommentRoomParticipantRelationship;
    //

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments_room_participants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comments_room_id',
        'user_id',
        'last_read',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
