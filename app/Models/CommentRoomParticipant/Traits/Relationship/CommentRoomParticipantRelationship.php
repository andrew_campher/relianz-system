<?php

namespace App\Models\CommentRoomParticipant\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\CommentRoom\CommentRoom;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait CommentRoomParticipantRelationship
{
    /**
     * Get all of the owning commentable models.
     */
    public function comments_room()
    {
        return $this->hasOne(CommentRoom::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

}