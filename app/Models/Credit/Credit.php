<?php

namespace App\Models\Credit;

use App\Models\Credit\Traits\Relationship\CreditRelationship;
use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    use CreditRelationship;

    protected $connection = 'qbdb';

    protected $primaryKey = 'ID';

    protected $table = 'CreditMemos';

    public $incrementing = false;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
