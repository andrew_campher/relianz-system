<?php

namespace App\Models\Credit;

use Illuminate\Database\Eloquent\Model;

class CreditItems extends Model
{

    protected $connection = 'qbdb';

    protected $primaryKey = 'ID';

    protected $table = 'CreditMemoLineItems';

    public $incrementing = false;

    public $keyType = 'string';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
