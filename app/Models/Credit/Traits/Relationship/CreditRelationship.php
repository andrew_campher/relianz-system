<?php

namespace App\Models\Credit\Traits\Relationship;
use App\Models\Credit\CreditItems;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait CreditRelationship
{

    public function items()
    {
        return $this->hasMany(CreditItems::class, 'CreditMemoId', 'ID');
    }

}