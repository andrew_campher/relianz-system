<?php

namespace App\Models\Customer;

use App\Models\Customer\Traits\Attribute\CustomerAttribute;
use App\Models\Customer\Traits\Relationship\CustomerRelationship;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use CustomerAttribute, CustomerRelationship;

    protected $connection = 'qbdb';

    protected $primaryKey = 'ID';

    protected $table = 'Customers';

    public $incrementing = false;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
