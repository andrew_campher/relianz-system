<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerData extends Model
{

    protected $connection = 'qbdb';

    protected $primaryKey = 'CustomersId';

    protected $table = 'customers_data';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['notes', 'notes_updated', 'settlement_discount', 'rands_per_kg', 'discount_perc', 'marketing_perc'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
