<?php

namespace App\Models\Customer\Traits\Attribute;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait CustomerAttribute
{

    public function showCustomerClass() {
        if (isset($this->data) && $this->data) {
            switch ($this->data->Class) {
                case "A":
                    return '<span title="80% of Sales Value" class="label label-success label-sm">A</span>';
                    break;
                case "B":
                    return '<span title="15% of Sales Value" class="label label-warning label-sm">B</span>';
                    break;
                case "C":
                    return '<span title="5% of Sales Value" class="label label-danger label-sm">C</span>';
                    break;
            }
        }
    }
}