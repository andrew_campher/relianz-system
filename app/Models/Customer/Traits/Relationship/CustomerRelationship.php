<?php

namespace App\Models\Customer\Traits\Relationship;
use App\Models\Customer\CustomerData;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait CustomerRelationship
{

    public function data()
    {
        return $this->hasOne(CustomerData::class, 'CustomersId', 'ID');
    }

}