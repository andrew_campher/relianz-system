<?php namespace App\Models\History;

use App\Models\History\Traits\Attribute\HistoryAttribute;
use Illuminate\Database\Eloquent\Model;
use App\Models\History\Traits\Relationship\HistoryRelationship;

/**
 * Class History
 * package App
 */
class History extends Model {

	use HistoryRelationship,
        HistoryAttribute;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'history';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['type_id', 'action', 'user_id', 'entity_id', 'icon', 'class', 'text', 'assets'];
}