<?php

namespace App\Models\History\Traits\Attribute;


trait HistoryAttribute
{

	/**
	 * @return string
	 */
	public function getAssetsAttribute($value)
	{

		return json_decode($value);
	}

}