<?php

namespace App\Models\Invoice;

use App\Models\Invoice\Traits\Attribute\InvoiceAttribute;
use App\Models\Invoice\Traits\Relationship\InvoiceRelationship;
use App\Models\Invoice\Traits\Scope\InvoiceScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use InvoiceScope,
        SoftDeletes,
        InvoiceAttribute,
        InvoiceRelationship;
    //
    protected $fillable = [
        'amount',
        'per_credit',
        'credit_quantity',
        'description',
        'account_id',
        'professional_id',
        'user_id',
        'extra_amount',
        'package_id',
        'status',
    ];

    protected $dates = ['deleted_at', 'created_at'];

    protected $table = 'invoices';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
