<?php

namespace App\Models\Invoice\Traits\Attribute;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait InvoiceAttribute
{

    protected $status_keys = [
        0 => 'unpaid',
        1 => 'paid',
        2 => 'cancelled',
    ];

    public function getStatusKey($find, $always_return_string = false) {
        if (is_numeric($find)) {
            return $this->status_keys[$find];
        } else {
            #Just return the string given if we always want the string
            if ($always_return_string)
                return $find;

            #Else lets search the array for the key id
            return array_search($find, $this->status_keys);
        }
    }
	/**
	 * @return string
	 */
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                return "<span class='badge label-danger'>Unpaid</span>";
                break;

            case 1:
                return "<span data-toggle='popover' data-trigger='hover' title='Invoice has been paid' class='badge label-success'>Paid</span>";
                break;

            case 2:
                return "<span data-toggle='popover' data-trigger='hover' title='Invoice has been paid' class='badge label-warning'>Cancelled</span>";
                break;

        }
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.invoice.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }


    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.invoice.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }



    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {


        return
            $this->getShowButtonAttribute(). $this->getEditButtonAttribute();
    }
}