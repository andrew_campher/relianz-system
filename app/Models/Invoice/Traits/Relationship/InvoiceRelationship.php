<?php

namespace App\Models\Invoice\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\Account\Account;
use App\Models\Payment\Payment;
use App\Models\Professional\Professional;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait InvoiceRelationship
{

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function professional()
    {
        return $this->belongsTo(Professional::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

}