<?php

namespace App\Models\Item;

use App\Models\Item\Traits\Attribute\ItemAttribute;
use App\Models\Item\Traits\Relationship\ItemRelationship;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use ItemAttribute, ItemRelationship;

    protected $connection = 'qbdb';

    protected $primaryKey = 'ID';

    protected $table = 'Items';

    public $incrementing = false;

    public $timestamps = false;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
