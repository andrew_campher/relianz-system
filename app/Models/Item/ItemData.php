<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Model;

class ItemData extends Model
{

    protected $connection = 'qbdb';

    protected $primaryKey = 'ItemId';

    protected $table = 'items_data';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['pricelist', 'stock_type'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
