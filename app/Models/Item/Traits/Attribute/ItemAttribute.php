<?php

namespace App\Models\Item\Traits\Attribute;
use App\Helpers\Helper;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait ItemAttribute
{
    public function getKGUnit() {
        $CustomFields = json_decode($this->CustomFields, true);

        return Helper::getKGUnit($CustomFields['Unit'], $CustomFields['KgWeight']);

    }

    public function showStockType() {
        if (isset($this->items_data) && $this->items_data) {
            switch ($this->items_data->stock_type) {
                case 1:
                    return '<span class="label label-success label-sm">STOCKED</span>';
                    break;
                case 2:
                    return '<span class="label label-warning label-sm">ON REQUEST</span>';
                    break;
                default:
                    return '<span class="label label-default label-sm">N/A</span>';
                    break;
            }
        }
    }

    public function showClass() {
        if (isset($this->items_data) && $this->items_data) {
            switch ($this->items_data->Class) {
                case "A":
                    return '<span class="label label-success label-sm">A</span>';
                    break;
                case "B":
                    return '<span class="label label-warning label-sm">B</span>';
                    break;
                case "C":
                    return '<span class="label label-danger label-sm">C</span>';
                    break;
            }
        }
    }


}