<?php

namespace App\Models\Item\Traits\Relationship;
use App\Models\Item\ItemData;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ItemRelationship
{

    public function items_data()
    {
        return $this->hasOne(ItemData::class, 'ItemId', 'ID');
    }

}