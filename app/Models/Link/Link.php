<?php

namespace App\Models\Link;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{

    protected $connection = 'qbdb';

    protected $table = 'links';

    public $timestamps = false;

    //
    protected $fillable = [
        'ID',
        'menu_title',
        'url',
        'sort',
        'roles',
        'user_id',
        'active',
        'icon',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
