<?php

namespace App\Models\Location;

use App\Models\Location\Traits\Relationship\LocationRelationship;
use App\Models\Location\Traits\Scope\LocationScope;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use LocationRelationship, LocationScope;
    //
    protected $fillable = [
        'slug',
        'title',
        'lat',
        'lng',
        'province_id',
        'city_id',
        'google_place_id',
        'type',
    ];

    protected $table = 'locations';

    public $timestamps = false;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
