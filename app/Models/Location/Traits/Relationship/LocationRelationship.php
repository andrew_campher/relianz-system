<?php

namespace App\Models\Location\Traits\Relationship;
use App\Models\Location\Location;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait LocationRelationship
{

    public function province()
    {
        return $this->hasOne(Location::class, 'province_id', 'province_id');
    }

    public function nearby_locations($distance, $limit=20)
    {
        return $this->selectRaw('locations.*, ( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$this->lat, $this->lng, $this->lat])
            #Exclude himself
            ->where('id','!=',$this->id)
            ->where('type','sublocality_level_2')
            ->havingRaw("distance <= ".$distance)
            ->orderBy('distance', 'asc')
            ->limit($limit);
    }



}