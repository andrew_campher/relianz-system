<?php

namespace App\Models\Location\Traits\Scope;
use Illuminate\Support\Facades\DB;

/**
 * Class UserScope
 * @package App\Models\Access\User\Traits\Scope
 */
trait LocationScope
{

    public function scopeUpdateHits($query) {
        return $query->update([
            'hits'      => DB::raw('hits + 1'),
            'hits_day'  => DB::raw('hits_day + 1'),
            'hits_month'=> DB::raw('hits_month + 1'),
            'hits_year' => DB::raw('hits_year + 1'),
        ]);
    }

}