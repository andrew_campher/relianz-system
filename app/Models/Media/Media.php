<?php

namespace App\Models\Media;

use App\Models\Media\Traits\Attribute\MediaAttribute;
use App\Models\Media\Traits\Relationship\MediaRelationship;
use App\Models\Media\Traits\Scope\MediaScope;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use MediaScope,
        MediaAttribute,
        MediaRelationship;
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename',
        'group',
        'title',
        'orig_filename',
        'details',
        'file_type',
        'modelname_id',
        'modelname_title',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
