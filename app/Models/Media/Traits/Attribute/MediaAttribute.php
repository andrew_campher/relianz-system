<?php

namespace App\Models\Media\Traits\Attribute;


trait MediaAttribute
{

    public function getImageHtmlAttribute() {
        return '<img src="/images/'.$this->table.'/thumbnail/'.$this->image.'" />';
    }


}