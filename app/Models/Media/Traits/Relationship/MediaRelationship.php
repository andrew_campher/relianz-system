<?php

namespace App\Models\Media\Traits\Relationship;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\Quote\Quote;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait MediaRelationship
{

    /**
     * Get project that are assigned this tag.
     */
    public function model()
    {
        return $this->morphTo('modelname');
    }

}