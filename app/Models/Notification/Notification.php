<?php

namespace App\Models\Notification;

use App\Models\Notification\Traits\Attribute\NotificationAttribute;
use App\Models\Notification\Traits\Relationship\NotificationRelationship;
use App\Models\Notification\Traits\Scope\NotificationScope;
use Illuminate\Notifications\DatabaseNotification;

class Notification extends DatabaseNotification
{
    use NotificationScope,
        NotificationAttribute,
        NotificationRelationship;
    //

    protected $appends = array('human_date');

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
