<?php

namespace App\Models\Notification\Traits\Attribute;

use Illuminate\Support\Facades\Lang;

trait NotificationAttribute
{

    public function getTextAttribute()
    {
        $text = '';
        $trans = 'strings.notifications.'.class_basename($this->type);

        if (Lang::has($trans, 'en')) {
            $text = trans($trans.'.text');
        } else {
            $text = trans('strings.notifications.general.text', ['type' => class_basename($this->type)]);
        }

        return $text;
    }

    public function getHumanDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getUrlAttribute()
    {
        $trans = 'strings.notifications.'.class_basename($this->type);

        return trans($trans.'.url',$this->data);
    }
}