<?php

namespace App\Models\NotificationTracking;

use App\Models\NotificationTracking\Traits\Relationship\NotificationTrackingRelationship;
use App\Models\NotificationTracking\Traits\Scope\NotificationTrackingScope;
use Illuminate\Database\Eloquent\Model;

class NotificationTracking extends Model
{
    use NotificationTrackingRelationship, NotificationTrackingScope;
    //
    protected $fillable = [
        'opens',
        'clicks',
        'notification_class',
        'notification_type',
        'created_at',
    ];

    protected $table = 'sent_emails';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
