<?php

namespace App\Models\NotificationTracking\Traits\Relationship;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait NotificationTrackingRelationship
{

    public function entity()
    {
        return $this->morphTo('entity');
    }

    public function notifiable()
    {
        return $this->morphTo('notifiable');
    }

}