<?php

namespace App\Models\Occupation;

use App\Models\Occupation\Traits\Attribute\OccupationAttribute;
use App\Models\Occupation\Traits\Relationship\OccupationRelationship;
use App\Models\Occupation\Traits\Scope\OccupationScope;
use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    use OccupationRelationship, OccupationScope, OccupationAttribute;
    //
    protected $fillable = [
        'slug',
        'title',
        'tags',
    ];

    protected $table = 'occupations';

    public $timestamps = false;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
