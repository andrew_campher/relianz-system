<?php

namespace App\Models\Occupation\Traits\Attribute;


trait OccupationAttribute
{


    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.occupation.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {

        return '<a href="' . route('admin.occupation.destroy', $this) . '"
             data-method="delete"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';

    }

    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.occupation.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="' . route('admin.occupation.restore', $this) . '" name="restore_item" class="btn btn-sm btn-info"><i class="fa fa-refresh" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.restore') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="' . route('admin.occupation.delete-permanently', $this) . '" name="delete_item_perm" class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.delete_permanently') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {

        return
			$this->getEditButtonAttribute() .
            $this->getShowButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }

}