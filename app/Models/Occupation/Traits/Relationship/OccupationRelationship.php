<?php

namespace App\Models\Occupation\Traits\Relationship;
use App\Models\Occupation\Occupation;
use App\Models\Service\Service;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait OccupationRelationship
{

    public function services()
    {
        return $this->hasMany(Service::class);
    }

}