<?php

namespace App\Models\Occupation\Traits\Scope;
use Illuminate\Support\Facades\DB;

/**
 * Class UserScope
 * @package App\Models\Access\User\Traits\Scope
 */
trait OccupationScope
{

    public function scopeProfessionals($query) {
        return $query
            ->join('services', 'occupations.id', '=', 'services.occupation_id')
            ->join('professional_service', 'services.id', '=', 'professional_service.service_id')
            ->join('professionals', 'professionals.id', '=', 'professional_service.professional_id');
    }

    public function scopeUpdateHits($query) {
        return $query->update([
            'hits'      => DB::raw('hits + 1'),
        ]);
    }
}