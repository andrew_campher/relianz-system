<?php

namespace App\Models\Order;

use App\Models\Order\Traits\Relationship\OrderRelationship;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use OrderRelationship;

    protected $connection = 'qbdb';

    protected $primaryKey = 'ID';

    protected $table = 'SalesOrders';

    public $incrementing = false;

    public $keyType = 'string';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
