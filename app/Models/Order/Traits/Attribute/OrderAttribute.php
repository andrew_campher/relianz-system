<?php

namespace App\Models\Order\Traits\Attribute;

use App\Helpers\Helper;

trait OrderAttribute
{

    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                #Pending approval
                return "<span title='Pending Status' class='badge label-warning' >Pending</span>";
                break;

            case 1:
                #Active
                return "<span title='Active Status' class='badge label-success'>Active</span>";
                break;

            case 2:
                #unconfirmed
                return "<span title='Unconfirmed' class='badge label-warning'>Unconfirmed</span>";
                break;
        }
    }

    public function getVerify($style = 'icon') {
        if ($this->verify_status) {
            switch ($style) {
                case 'icon':
                    return '<span data-toggle="popover" data-title="Verified Order" data-content="' . $this->title . ' has been verified by ' . config('app.name') . '." class="pro_verify_icon fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                        </span>';
                    break;
                case 'big':
                    return '<div class="badge label-primary"><i class="fa fa-check"></i> VERIFIED</div>';
                    break;
            }

            return '<span data-toggle="popover" data-title="Verified Order" data-content="' . $this->title . ' has been verified by ' . config('app.name') . '." class="pro_verify_icon fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-check fa-stack-1x fa-inverse"></i>
            </span>';
        }

        return '';
    }

    public function getVerifyLabelAttribute()
    {
        if ($this->verify_status) {

            return $this->getVerify('icon');
        }
        return '';
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getApprovedLabelAttribute()
    {
        if ($this->isApproved())
            return "<i class='text-success fa fa-thumbs-up fa-lg'></i>";
        return "<i class='text-gray fa fa-remove fa-lg'></i>";
    }

    public function getVerifiedLabelAttribute()
    {
        if ($this->verify_status)
            return "<i class='text-success fa fa-check-circle fa-lg'></i>";
        return "<i class='text-gray fa fa-remove fa-lg'></i>";
    }

    public function getOverallRatingStarsAttribute()
    {

        $rating = round($this->rating_overall_tot, 1);

        if ($rating) {
            return Helper::showStars($rating);
        } else {
            return '<span data-toggle="popover" data-content="This professional has not yet been rated on '.config('app.name').'." class="badge label-default">no ratings</span>';
        }
    }

    public function getLogoHtml($size='sm', $embed = false) {
        if ($this->logo && file_exists(public_path('/images/'.$this->table.'/thumbnail/'.$this->logo))) {
            if ($embed) {
                return '<img alt="'.$this->title.'" src="'.public_path('/images/'.$this->table.'/thumbnail/'.$this->logo).'" />';
            }
            return '<div class="pro-logo-cell logo-container '.$size.'-square" style="background-image:url(/images/'.$this->table.'/thumbnail/'.$this->logo.')"></div>';
        } else {
            if ($embed) {
                return '<div style="border-radius: 50%;width: 180px;height: 180px;line-height: 180px;font-size: 60px;display: inline-block;font-weight: 700;white-space: nowrap;text-align: center;color: #fff;background-color:'.Helper::genColorCodeFromText($this->title).'">'.Helper::createAcronym($this->title).'</div>';
            }
            return '<div class="img-circle logo-string '.$size.'-square" style="background-color:'.Helper::genColorCodeFromText($this->title).'">'.Helper::createAcronym($this->title).'</div>';
        }
    }

    public function getThumbUrlAttribute() {
        return asset('/images/'.$this->table.'/thumbnail/'.$this->logo);
    }

    public function getLogoUrlAttribute() {
        if ($this->logo)
            return asset('/images/'.$this->table.'/'.$this->logo);
    }

    public function getHeroImageUrlAttribute() {
        if ($this->hero_image)
            return asset('/images/'.$this->table.'/'.$this->hero_image);
    }

    /**
     * @return bool
     */
    public function isApproved() {
        return $this->approved == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.professional.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<a href="' . route('admin.professional.mark', [
                    $this,
                    1
                ]) . '" class="btn btn-sm btn-success"><i class="fa fa-play" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.activate') . '"></i></a> ';
            // No break

            case 1:
                return '<a href="' . route('admin.professional.mark', [
                    $this,
                    0
                ]) . '" class="btn btn-sm btn-warning"><i class="fa fa-pause" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.deactivate') . '"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }

        return '';
    }

    /**
     * @return string
     */
    public function getApprovedButtonAttribute()
    {
        switch ($this->approved) {
            case 0:
                return '<a href="' . route('admin.professional.approve', [
                    $this,
                    1
                ]) . '" class="btn btn-sm btn-success"><i class="fa fa-thumbs-up" data-toggle="popover" data-trigger="hover" data-placement="top" title="Approve Item"></i></a> ';
            // No break

            case 1:
                return '<a href="' . route('admin.professional.approve', [
                    $this,
                    0
                ]) . '" class="btn btn-sm btn-danger"><i class="fa fa-thumbs-down" data-toggle="popover" data-trigger="hover" data-placement="top" title="Un-Approve item"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }

        return '';
    }

    public function getVerifiedButtonAttribute()
    {
        switch ($this->verify_status) {
            case 0:
                return '<a href="' . route('admin.professional.verify', [
                    $this,
                    1
                ]) . '" class="btn btn-sm btn-success"><i class="fa fa-check-circle" data-toggle="popover" data-trigger="hover" data-placement="top" title="Verify Order"></i></a> ';
            // No break

            case 1:
                return '<a href="' . route('admin.professional.verify', [
                    $this,
                    0
                ]) . '" class="btn btn-sm btn-danger"><i class="fa fa-remove" data-toggle="popover" data-trigger="hover" data-placement="top" title="Remove Verified"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {

        return '<a href="' . route('admin.professional.destroy', $this) . '"
             data-method="delete"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';

    }

    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.professional.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="' . route('admin.professional.restore', $this) . '" name="restore_item" class="btn btn-sm btn-info"><i class="fa fa-refresh" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.restore') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="' . route('admin.professional.delete-permanently', $this) . '" name="delete_item_perm" class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.delete_permanently') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->getRestoreButtonAttribute() .
                $this->getDeletePermanentlyButtonAttribute();
        }

        return
			$this->getEditButtonAttribute() .
            $this->getShowButtonAttribute() .
            $this->getStatusButtonAttribute() .
            $this->getApprovedButtonAttribute() .
            $this->getVerifiedButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }

}