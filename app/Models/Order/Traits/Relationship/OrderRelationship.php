<?php

namespace App\Models\Order\Traits\Relationship;
use App\Models\Customer\Customer;
use App\Models\OrderItems\OrderItems;
use App\Models\OrderStatus\OrderStatus;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait OrderRelationship
{

    public function stats()
    {
        return $this->hasOne(OrderStatus::class, 'SalesOrderID', 'ID');
    }

    public function items()
    {
        return $this->hasMany(OrderItems::class, 'SalesOrderId', 'ID')
            ->where('ItemName','NOT LIKE','.%')
            ->where('ItemName','!=','');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'ID', 'CustomerId');
    }

}