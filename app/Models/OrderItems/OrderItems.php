<?php

namespace App\Models\OrderItems;

use App\Models\OrderItems\Traits\Relationship\OrderItemsRelationship;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    use OrderItemsRelationship;

    protected $connection = 'qbdb';

    protected $primaryKey = 'ID';

    protected $table = 'SalesOrderLineItems';

    public $incrementing = false;

    public $keyType = 'string';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
