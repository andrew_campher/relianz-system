<?php

namespace App\Models\OrderItems\Traits\Relationship;
use App\Models\Item\Item;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait OrderItemsRelationship
{

    public function item()
    {
        return $this->hasOne(Item::class, 'ID', 'ItemId');
    }

}