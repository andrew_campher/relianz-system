<?php

namespace App\Models\OrderStatus;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $connection = 'qbdb';

    protected $primaryKey = 'SalesOrderID';

    protected $table = 'SalesOrdersStatus';

    public $incrementing = false;

    public $keyType = 'string';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
