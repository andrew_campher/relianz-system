<?php

namespace App\Models\Package;

use App\Models\Package\Traits\Attribute\PackageAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use PackageAttribute,
        SoftDeletes;
    //
    protected $fillable = [
        'title',
        'cost',
        'discount_percent',
        'per_credit',
        'free_Credits',
        'free'
    ];

    protected $table = 'packages';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
