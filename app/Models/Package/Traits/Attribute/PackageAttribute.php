<?php

namespace App\Models\Package\Traits\Attribute;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait PackageAttribute
{
	/**
	 * @return string
	 */
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                return "<span class='badge label-danger'>Unpaid</span>";
                break;

            case 1:
                return "<span data-toggle='popover' data-trigger='hover' title='Package has been paid' class='badge label-success'>Paid</span>";
                break;

            case 2:
                return "<span data-toggle='popover' data-trigger='hover' title='Package has been paid' class='badge label-warning'>Cancelled</span>";
                break;

        }
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.package.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }


    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.package.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }



    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {


        return
            $this->getShowButtonAttribute(). $this->getEditButtonAttribute();
    }
}