<?php

namespace App\Models\Payment;

use App\Models\Payment\Traits\Relationship\PaymentRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes,
        PaymentRelationship;
    //
    protected $fillable = [
        'amount_paid',
        'user_id',
        'invoice_id',
        'note',
    ];

    protected $table = 'payments';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
