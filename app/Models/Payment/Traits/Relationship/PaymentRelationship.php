<?php

namespace App\Models\Payment\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\Invoice\Invoice;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait PaymentRelationship
{
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}