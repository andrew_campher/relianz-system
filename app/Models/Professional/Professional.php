<?php

namespace App\Models\Professional;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Professional\Traits\Attribute\ProfessionalAttribute;
use App\Models\Professional\Traits\Relationship\ProfessionalRelationship;
use App\Models\Professional\Traits\Scope\ProfessionalScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Professional extends Model
{
    use ProfessionalScope,
        SoftDeletes,
        Notifiable,
        ProfessionalAttribute,
        ProfessionalRelationship,
        Sluggable;
    //

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'professionals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'company_name',
        'description',
        'logo',
        'hero_image',
        'website_url',
        'street',
        'city',
        'province',
        'postcode',
        'social_google',
        'social_twitter',
        'social_facebook',
        'year_started',
        'distance_travelled',
        'id_number',
        'company_email',
        'telephone',
        'notify_mobile',
        'notify_email',
        'lat',
        'lng',
        'vat_registered',
        'company_type',
        'company_registration',
        'rating_overall_tot',
        'area',
        'verify_status',
        'successful_projects',
        'approved',
        'user_id',
        'status'
    ];

    public function routeNotificationForMail()
    {
        return $this->notify_email;
    }

    public function routeNotificationForNexmo()
    {
        return $this->notify_mobile;
    }

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
