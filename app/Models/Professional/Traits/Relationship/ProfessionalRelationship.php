<?php

namespace App\Models\Professional\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\Account\Account;
use App\Models\Invoice\Invoice;
use App\Models\Media\Media;
use App\Models\ProfessionalBranch\ProfessionalBranch;
use App\Models\ProfessionalStat\ProfessionalStat;
use App\Models\ProjectReview\ProjectReview;
use App\Models\Quote\Quote;
use App\Models\Service\Service;
use App\Models\Transaction\Transaction;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ProfessionalRelationship
{
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function branches()
    {
        return $this->hasMany(ProfessionalBranch::class);
    }

    public function branch()
    {
        return $this->hasOne(ProfessionalBranch::class);
    }

    public function outstanding_invoices()
    {
        return $this->hasMany(Invoice::class)->where('status', 0);
    }

    public function account()
    {
        return $this->hasOne(Account::class);
    }

    public function stats()
    {
        return $this->hasOne(ProfessionalStat::class);
    }
    /**
     * Many-to-Many relations with Categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Many-to-Many with services
     * @return mixed
     */
    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    /**
     * Many-to-Many with services
     * @return mixed
     */
    public function quotes()
    {
        return $this->hasMany(Quote::class)->orderBy('id', 'DESC');
    }

    public function reviews()
    {
        return $this->hasMany(ProjectReview::class)->orderBy('id', 'DESC');
    }

    public function approved_reviews()
    {
        return $this->hasMany(ProjectReview::class)->where('status', 1)->orderBy('id', 'DESC');
    }

    public function draft_project_quotes()
    {
        return $this->hasMany(Quote::class)->where('quotes.status',0)->orderBy('id', 'DESC');
    }

    /**
     * Where Status = Sent[1], Read[2], Interacted[6]
     * @return mixed
     */
    public function open_project_quotes()
    {
        return $this->hasMany(Quote::class)->where(function($query) {
            $query->where('quotes.status',1)->orwhere('quotes.status', 2)->orwhere('quotes.status', 6);
        })->orderBy('id', 'DESC');
    }

    public function in_progress_projects()
    {
        #Hired, Reviewed, Hire_Requested
        return $this->hasMany(Quote::class)->where(function($query) {
            $query->where('quotes.status',3);
        })->orderBy('id', 'DESC');
    }

    public function hired_project_quotes()
    {
        #Hired, Reviewed, Hire_Requested
        return $this->hasMany(Quote::class)->where(function($query) {
            $query->where('quotes.status',3)->orwhere('quotes.status', 5)->orwhere('quotes.status', 7);
        })->orderBy('id', 'DESC');
    }

    public function unsuccessful_project_quotes()
    {
        return $this->hasMany(Quote::class)->where('quotes.status',4)->orderBy('id', 'DESC');
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'modelowner');
    }

    /**
     * Get all of the professional's media
     * "general" group
     */
    public function media()
    {
        return $this->morphMany(Media::class, 'modelname')->where('group','general');
    }

    /**
     * "resources" group
     */
    public function media_resources()
    {
        return $this->morphMany(Media::class, 'modelname')->where('group','resources');
    }

}