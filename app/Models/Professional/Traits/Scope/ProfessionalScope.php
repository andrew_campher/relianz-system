<?php

namespace App\Models\Professional\Traits\Scope;
use Illuminate\Support\Facades\DB;

/**
 * Class UserScope
 * @package App\Models\Access\User\Traits\Scope
 */
trait ProfessionalScope
{

	/**
     * Only Active items
     *
	 * @param $query
	 * @param bool $status
	 * @return mixed
	 */
	public function scopeActive($query, $status = true) {
		return $query->where('status', $status);
	}

    /**
     * Get only approved professionals
     *
     * @param $query
     * @param bool $status
     * @return mixed
     */
    public function scopeApproved($query, $approved = true) {
        return $query->where('status', 1);
    }

    public function scopeDistanceLimitRadius($query, $lat, $lng, $radius) {
        $distance_sql = "6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )";
        return $query->select('professionals.*')->selectRaw($distance_sql.' AS distance', [$lat, $lng, $lat])
            ->whereRaw($distance_sql." <= ?", [$lat, $lng, $lat, $radius])
            ->where('professionals.approved', 1)
            ->where('professionals.status', 1);
    }

    /**
     * Include distance from item to point
     * @param $query
     * @param $lat
     * @param $lng
     * @return mixed
     */
    public function scopeDistance($query, $lat, $lng) {
        return $query->select('projects.*')->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$lat, $lng, $lat])
            ->where('professionals.approved', 1)
            ->where('professionals.status', 1);
    }

    public function scopeProInRange($query, $lat, $lng) {

        return $query->select('professionals.*')->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$lat, $lng, $lat])
            ->havingRaw("distance <= professionals.distance_travelled")
            ->where('professionals.approved', 1)
            ->where('professionals.status', '>', 0)
            ->groupBy('professionals.id')
            ->orderBy('distance', 'asc');
    }

    public function scopeOccupationServiceRange($query, $lat, $lng, $occupation_id) {

        $query = $query->join('occupations', 'occupations.id', DB::raw($occupation_id));

        $query = $query->join('services', 'services.occupation_id','=','occupations.id');

        $query = $query->join('professional_service', function($join)
        {
            $join->on('professional_service.service_id', '=', 'services.id')->on('professionals.id', '=', 'professional_service.professional_id');
        });

        return $query->select('professionals.*')->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$lat, $lng, $lat])

            ->havingRaw("distance <= professionals.distance_travelled")
            ->where('professionals.approved', 1)
            ->where('professionals.status', '>',0)
            ->groupBy('professionals.id')
            ->orderBy('distance', 'asc');
    }

    public function scopeServiceRange($query, $project) {

        $query = $query->join('professional_service', 'professional_service.professional_id', 'professionals.id');

        $query = $query->join('professional_branches', 'professional_branches.professional_id', 'professionals.id');

        return $query->select(
            'professionals.id',
            'professionals.title',
            'professionals.user_id',
            'professionals.status',
            'professional_branches.telephone',
            'professional_branches.area',
            'professional_branches.distance_travelled',
            'professional_branches.notify_mobile',
            'professional_branches.notify_email',
            'professional_branches.lat',
            'professional_branches.lng'
            )->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( professional_branches.lat ) )
                               * cos( radians( professional_branches.lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( professional_branches.lat ) ) )
                             ) AS distance', [$project->lat, $project->lng, $project->lat])

            ->havingRaw("distance <= professional_branches.distance_travelled")
            ->where('professional_service.service_id', $project->service_id)
            ->where('professionals.approved', 1)
            ->where('professionals.status', '>', 0)
            ->groupBy('professionals.id')
            ->orderBy('distance', 'asc');
    }

    public function scopeUpdateHits($query) {
        return $query->update([
            'hits'      => DB::raw('hits + 1'),
            'hits_day'  => DB::raw('hits_day + 1'),
            'hits_month'=> DB::raw('hits_month + 1'),
            'hits_year' => DB::raw('hits_year + 1'),
        ]);
    }

    public function scopeWhereOccupation($query, $occupation_id) {
        return $query
            ->join('professional_service', 'professionals.id', '=', 'professional_service.professional_id')
            ->join('services', 'professional_service.service_id', '=', 'services.id')
            ->select('professionals.*')
            ->where('professionals.approved', 1)
            ->where('professionals.status', '>', 0)
            ->where('services.occupation_id', $occupation_id)
            ->groupBy('professionals.id')
            ->orderBy('professionals.hits', 'desc');
    }



}