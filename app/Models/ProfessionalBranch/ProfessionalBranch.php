<?php

namespace App\Models\ProfessionalBranch;

use App\Models\ProfessionalBranch\Traits\Attribute\ProfessionalBranchAttribute;
use App\Models\ProfessionalBranch\Traits\Relationship\ProfessionalBranchRelationship;
use App\Models\ProfessionalBranch\Traits\Scope\ProfessionalBranchScope;
use App\Services\NotificationTracking\NotificationTracking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProfessionalBranch extends Model
{
    use ProfessionalBranchScope,
        Notifiable,
        ProfessionalBranchAttribute,
        ProfessionalBranchRelationship;
    //

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'professional_branches';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_title',
        'branch_slug',
        'lat',
        'lng',
        'area',
        'telephone',
        'professional_id',
        'distance_travelled',
        'notify_email',
        'notify_mobile',
    ];

    public function routeNotificationForMail()
    {
        return $this->notify_email;
    }

    public function routeNotificationForNexmo()
    {
        return $this->notify_mobile;
    }

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function emailSentStats($email = null) {
        if(!$email)
            $email = $this->notify_email;

        return NotificationTracking::emailSentStats($this->notify_email);
    }

    public function entityNotifiableSentStats($entity_type, $entity_id, $notifiable_type, $notifiable_id) {

        return NotificationTracking::entityNotifiableSentStats($entity_type, $entity_id, $notifiable_type, $notifiable_id);
    }

}
