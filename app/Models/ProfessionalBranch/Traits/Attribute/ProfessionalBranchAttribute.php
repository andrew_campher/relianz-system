<?php

namespace App\Models\ProfessionalBranch\Traits\Attribute;

trait ProfessionalBranchAttribute
{

    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                #Pending approval
                return "<span title='Pending Status' class='badge label-warning' >Pending</span>";
                break;

            case 1:
                #Active
                return "<span title='Active Status' class='badge label-success'>Active</span>";
                break;

            case 2:
                #unconfirmed
                return "<span title='Unconfirmed' class='badge label-warning'>Unconfirmed</span>";
                break;
        }
    }

}