<?php

namespace App\Models\ProfessionalBranch\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\Account\Account;
use App\Models\Invoice\Invoice;
use App\Models\Media\Media;
use App\Models\Professional\Professional;
use App\Models\ProfessionalBranchStat\ProfessionalBranchStat;
use App\Models\ProjectReview\ProjectReview;
use App\Models\Quote\Quote;
use App\Models\Service\Service;
use App\Models\Transaction\Transaction;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ProfessionalBranchRelationship
{

    public function professional()
    {
        return $this->belongsTo(Professional::class);
    }


}