<?php

namespace App\Models\ProfessionalBranch\Traits\Scope;

/**
 * Class UserScope
 * @package App\Models\Access\User\Traits\Scope
 */
trait ProfessionalBranchScope
{


    public function scopeServiceRange($query, $project) {

        $query = $query->join('professionals', 'professional_branches.professional_id', 'professionals.id');

        $query = $query->join('professional_service', 'professional_service.professional_id', 'professionals.id');

        return $query->select(
            'professionals.title',
            'professionals.user_id',
            'professionals.status',
            'professional_branches.id',
            'professional_branches.professional_id',
            'professional_branches.telephone',
            'professional_branches.area',
            'professional_branches.distance_travelled',
            'professional_branches.notify_mobile',
            'professional_branches.notify_email',
            'professional_branches.lat',
            'professional_branches.lng'
        )->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( professional_branches.lat ) )
                               * cos( radians( professional_branches.lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( professional_branches.lat ) ) )
                             ) AS distance', [$project->lat, $project->lng, $project->lat])

            ->havingRaw("distance <= professional_branches.distance_travelled")
            ->where('professional_service.service_id', $project->service_id)
            ->where('professionals.approved', 1)
            ->groupBy('professional_branches.id')
            ->orderBy('distance', 'asc');
    }


}