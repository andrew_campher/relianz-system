<?php

namespace App\Models\ProfessionalStat;

use App\Models\ProfessionalStat\Traits\Attribute\ProfessionalStatAttribute;
use App\Models\ProfessionalStat\Traits\Relationship\ProfessionalStatRelationship;
use App\Models\ProfessionalStat\Traits\Scope\ProfessionalStatScope;
use Illuminate\Database\Eloquent\Model;

class ProfessionalStat extends Model
{
    use ProfessionalStatScope,
        ProfessionalStatAttribute,
        ProfessionalStatRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'professionals_stats';


    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'professional_id',
        'hires_count',
        'hires_success',
        'stat_overall',
        'stat_cooperative',
        'stat_quality',
        'stat_time',
        'stat_price',
        'recommends_count',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
