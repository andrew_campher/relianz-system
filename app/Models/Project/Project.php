<?php

namespace App\Models\Project;

use App\Helpers\Helper;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Project\Traits\Attribute\ProjectAttribute;
use App\Models\Project\Traits\Relationship\ProjectRelationship;
use App\Models\Project\Traits\Scope\ProjectScope;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use ProjectScope,
        SoftDeletes,
        ProjectAttribute,
        ProjectRelationship,
        Sluggable;
    //

    /**
     * @var array
     */
    protected $dates = ['deleted_at', 'specific_date', 'approved_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'professional_id',
        'status',
        'service_id',
        'customer_email',
        'customer_mobile',
        'lng',
        'lat',
        'area',
        'quote_count',
        'postcode',
        'when',
        'when_describe',
        'specific_date',
        'service_id',
        'description',
    ];

    public function routeNotificationForMail()
    {
        return $this->customer_email;
    }

    public function routeNotificationForNexmo()
    {
        return $this->customer_mobile;
    }

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = 'projects';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Returns formatted answers for a project
     * @return array|bool
     */
    public function getAnswers()
    {
        $answers = array();
        $answers_arr = $this->answers->load('question');
        if($answers_arr) {
            $answers = Helper::make_tree_array($answers_arr, 'question_id');

            return $answers;
        }

        return false;
    }
}
