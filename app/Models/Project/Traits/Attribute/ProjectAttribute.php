<?php

namespace App\Models\Project\Traits\Attribute;


use Carbon\Carbon;

trait ProjectAttribute
{

    protected $status_keys = [
        0 => 'pending',
        1 => 'open',
        2 => 'hired',
        3 => 'completed',
        4 => 'closed',
        5 => 'expired',
        6 => 'fulfilled',
    ];

    /**
     * @param $find
     * @param bool $always_return_string
     * @return mixed
     */
    public function getStatusKey($find, $always_return_string = false) {
        if (is_numeric($find)) {
            return $this->status_keys[$find];
        } else {
            #Just return the string given if we always want the string
            if ($always_return_string)
                return $find;

            #Else lets search the array for the key id
            return array_search($find, $this->status_keys);
        }
    }

	/**
	 * @return string
	 */
	public function getStatusLabelAttribute()
	{
	    switch ($this->status) {
            case 0:
                #Pending approval
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.projects.status.'.access()->group().'.pending_description')."' title='Pending Status' class='badge label-warning' >Processing</span>";
                break;

            case 1:
                #Open
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.projects.status.'.access()->group().'.open_description')."' title='Open Status' class='badge label-info'>Open</span>";
                break;

            case 2:
                #Hired
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.projects.status.'.access()->group().'.hired_description')."' title='Hired Status' class='badge label-success'><i class='fa fa-handshake-o'></i> Hired</span>";
                break;

            case 3:
                #Complete
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.projects.status.'.access()->group().'.completed_description')."' title='Completed Status' class='badge label-success'>Completed</span>";
                break;

            case 4:
                #Closed
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.projects.status.'.access()->group().'.closed_description')."' title='Closed Status' class='badge label-danger'>Closed</span>";
                break;

            case 5:
                #Expired
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.projects.status.'.access()->group().'.expired_description')."' title='Expired Status' class='badge label-danger'>Expired</span>";
                break;

            case 6:
                #Fulfilled
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.projects.status.'.access()->group().'.fulfilled_description')."' title='Fulfilled Status' class='badge label-danger'>Fulfilled</span>";
                break;
        }
	}

    public function getWhenLabelAttribute()
    {
        switch ($this->when) {
            case 1:
                #As soon as possible
                return '<div data-toggle="popover" data-content="When the client needs this work done" class="badge label-danger"><i class="fa fa-warning"></i> As soon as possible</div>';
                break;

            case 2:
                #I'm flexible
                return '<div data-toggle="popover" data-content="When the client needs this work done" class="badge label-info">I\'m flexible</div>';
                break;

            case 3:
                #In the next few days
                return '<div data-toggle="popover" data-content="When the client needs this work done" class="badge label-warning">In the next few days</div>';
                break;

            case 4:
                #On a Specific date
                return 'Specific date: <div data-toggle="popover" data-content="When the client needs this work done" class="badge label-primary"><i class="fa fa-clock-o"></i> '.$this->specific_date->format(config('app.date.long_min')).'</div>';
                break;

            case 5:
                ## Other - explain
                return '<div data-toggle="popover" data-content="When the client needs this work done" title="'.htmlentities($this->when_describe).'" class="badge label-info">More Info</div>';
                break;

        }
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.project.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getApprovedButtonAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<a href="' . route('admin.project.approve', [
                    $this,
                    1
                ]) . '" class="btn btn-sm btn-success"><i class="fa fa-thumbs-up" data-toggle="popover" data-trigger="hover" data-placement="top" title="Approve this project"></i></a> ';
            // No break

            case 1:
                return '<a href="' . route('admin.project.approve', [
                    $this,
                    0
                ]) . '" class="btn btn-sm btn-warning"><i class="fa fa-thumbs-down" data-toggle="popover" data-trigger="hover" data-placement="top" title="Unapprove this project"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }

        return '';
    }


    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {

        return '<a href="' . route('admin.project.destroy', $this) . '"
             data-method="delete"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';

    }

    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.project.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="' . route('admin.project.restore', $this) . '" name="restore_item" class="btn btn-sm btn-info"><i class="fa fa-refresh" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.restore') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="' . route('admin.project.delete-permanently', $this) . '" name="delete_item_perm" class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.delete_permanently') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->getRestoreButtonAttribute() .
                $this->getDeletePermanentlyButtonAttribute();
        }

        return
			$this->getEditButtonAttribute() .
            $this->getShowButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }

    public function getImageHtmlAttribute() {
        return '<img src="/images/'.$this->table.'/thumbnail/'.$this->image.'" />';
    }
}