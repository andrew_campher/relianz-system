<?php

namespace App\Models\Project\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\Media\Media;
use App\Models\Professional\Professional;
use App\Models\QuestionAnswer\QuestionAnswer;
use App\Models\Quote\Quote;
use App\Models\Service\Service;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ProjectRelationship
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function hired_professional()
    {
        return $this->belongsTo(Professional::class, 'professional_id', 'id');
    }

    public function hired_quote()
    {
        return $this->hasOne(Quote::class, 'project_id')
            ->join('projects', 'projects.id', '=', 'quotes.project_id')
            ->select('quotes.*')
            ->whereRaw('quotes.professional_id = projects.professional_id');
    }
    /**
     * Get all of the project's media
     */
    public function media()
    {
        return $this->morphMany(Media::class, 'modelname');
    }

    public function answers()
    {
        return $this->hasMany(QuestionAnswer::class);
    }

    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }

    public function unopened_quotes()
    {
        return $this->hasMany(Quote::class)->where('status', 1)->where('read_at', null);
    }

    public function opened_quotes()
    {
        return $this->hasMany(Quote::class)->where('status', '>',1);
    }



}