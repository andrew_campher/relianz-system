<?php

namespace App\Models\Project\Traits\Scope;
use Illuminate\Support\Facades\DB;

/**
 * Class UserScope
 * @package App\Models\Access\User\Traits\Scope
 */
trait ProjectScope
{


    /**
     * Scope to pull projects where no professional_id and its not his own
     * + Status is OPEN
     * @param $query
     * @return mixed
     */
    public function scopeQuotable($query) {
        return $query->where('status',1)->where('professional_id', 0)->where('user_id','!=', access()->id());
    }

    /**
     * All quotable projects excluding logged in users (professionals) already quoted projects
     * @param $query
     * @return mixed
     */
    public function scopeProfessionalQuotable($query) {
        return $query->where('status',1)->where('professional_id', 0)->where('user_id','!=', access()->id())->whereDoesntHave('quotes', function ($query) {
            $query->where('user_id', access()->id());
        });
    }

    /**
     * OLD - unuseade
     * Find distance between point and limit to radius distance
     * @param $query
     * @param $lat
     * @param $lng
     * @param $radius
     * @return mixed
     */
    public function scopeDistanceLimitRadius($query, $lat, $lng, $radius) {
        $distance_sql = "6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )";
        return $query->select('projects.*')->selectRaw($distance_sql.' AS distance', [$lat, $lng, $lat])
            ->whereRaw($distance_sql." <= ?", [$lat, $lng, $lat, $radius])
            ->where('projects.status', '>', 0);
    }

    public function scopeWithinDistanceToBranches($query, $branches) {
        $distance_sql = "6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )";

        $query = $query->select('projects.*');

        if (count($branches) == 1) {
            $query = $query->selectRaw($distance_sql . ' AS distance' . $branches[0]->id, [$branches[0]->lat, $branches[0]->lng, $branches[0]->lat])
                ->whereRaw($distance_sql." <= ?", [$branches[0]->lat, $branches[0]->lng, $branches[0]->lat, $branches[0]->distance_travelled]);
        } else {

            foreach ($branches AS $branch) {
                $query = $query->selectRaw($distance_sql . ' AS distance' . $branch->id, [$branch->lat, $branch->lng, $branch->lat]);
            }

            $query = $query->where(function ($query) use ($distance_sql, $branches) {
                foreach ($branches AS $branch) {
                    $query = $query->orWhereRaw($distance_sql." <= ?", [$branch->lat, $branch->lng, $branch->lat, $branch->distance_travelled]);
                }
            });
        }

        return $query;
    }



    /**
     * Include distance from item to point
     * @param $query
     * @param $lat
     * @param $lng
     * @return mixed
     */
    public function scopeDistance($query, $lat, $lng) {
        return $query->select('projects.*')
            ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )
                             ) AS distance', [$lat, $lng, $lat])
            ->where('projects.status', '>', 0);
    }

    /**
     * Get Active - Where project is
     * @param $query
     * @return mixed
     */
    public function scopeActiveProjects($query) {
        return $query->where('status', 2);
    }

    public function scopeCompletedProjects($query) {
        return $query->where('status', 3)->orderBy('id', 'desc');
    }

    public function scopeClosedProjects($query) {
        return $query->where('status', 4)->orderBy('id', 'desc');
    }

    /**
     * 1 = OPen
     * 0 = Pending
     * 6 = Fulfilled
     * @param $query
     * @return mixed
     */
    public function scopeOpenProjects($query) {
        return $query->where(function($query) {
            $query->where('status', 1)->orwhere('status', 0)->orwhere('status', 6);
        })->orderBy('id', 'desc');
    }

    public function scopeExpiredProjects($query) {
        return $query->where('status', 5)->orderBy('id', 'desc');
    }

    public function scopePendingProjects($query) {
        return $query->where('status', 0)->orderBy('id', 'desc');
    }

    public function scopeFinishedProjects($query) {
        return $query->where(function($query) {
            $query->where('status', 3)->orwhere('status', 4);
        })->orderBy('id', 'desc');
    }

    public function scopeUpdateHits($query) {
        return $query->update([
            'hits'      => DB::raw('hits + 1'),
            'hits_day'  => DB::raw('hits_day + 1'),
            'hits_month'=> DB::raw('hits_month + 1'),
            'hits_year' => DB::raw('hits_year + 1'),
        ]);
    }

    public function scopeWhereOccupation($query, $occupation_id) {
        return $query
            ->join('services', 'projects.service_id', '=', 'services.id')
            ->select('projects.*')
            ->where('projects.status', '>', 0)
            ->where('services.occupation_id', $occupation_id)
            ->orderBy('projects.id', 'desc');
    }

}