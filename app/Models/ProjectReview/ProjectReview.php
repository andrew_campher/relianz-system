<?php

namespace App\Models\ProjectReview;

use App\Models\ProjectReview\Traits\Attribute\ProjectReviewAttribute;
use App\Models\ProjectReview\Traits\Relationship\ProjectReviewRelationship;
use App\Models\ProjectReview\Traits\Scope\ProjectReviewScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectReview extends Model
{
    use ProjectReviewScope,
        SoftDeletes,
        ProjectReviewAttribute,
        ProjectReviewRelationship;
    //
    protected $fillable = [
        'project_id',
        'professional_id',
        'quote_id',
        'user_id',
        'title',
        'comments',
        'recommended',
        'rating_overall',
        'rating_coopertaive',
        'rating_quality',
        'rating_time',
        'rating_price',
        'project_success',
        'status',
        'approved',
        'verified',
    ];

    protected $table = 'projects_reviews';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
