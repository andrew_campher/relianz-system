<?php

namespace App\Models\ProjectReview\Traits\Attribute;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait ProjectReviewAttribute
{
	/**
	 * @return string
	 */
	public function getStatusLabelAttribute()
	{
		if ($this->isActive())
			return "<label class='label label-success'>".trans('labels.general.active')."</label>";
		return "<label class='label label-danger'>".trans('labels.general.inactive')."</label>";
	}

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.project_review.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        if ($this->user_id != access()->id()) {
            switch ($this->status) {
                case 0:
                    return '<a href="' . route('admin.project_review.approve', [
                        $this,
                        1
                    ]) . '" class="btn btn-sm btn-success"><i class="fa fa-thumbs-up fa-lg" data-toggle="popover" data-trigger="hover" data-placement="top" title="Approve"></i></a> ';
                // No break

                case 1:
                    return '<a href="' . route('admin.project_review.approve', [
                        $this,
                        0
                    ]) . '" class="btn btn-sm btn-default"><i class="fa fa-thumbs-down fa-lg" data-toggle="popover" data-trigger="hover" data-placement="top" title="Un-Approve"></i></a> ';
                // No break

                default:
                    return '';
                // No break
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if ($this->user_id != access()->id()) {
            return '<a href="' . route('admin.project_review.destroy', $this) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
                 data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
                 class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
        }

        return '';
    }

    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.project_review.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="' . route('admin.project_review.restore', $this) . '" name="restore_user" class="btn btn-sm btn-info"><i class="fa fa-refresh" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.project_review.restore_user') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="' . route('admin.project_review.delete-permanently', $this) . '" name="delete_user_perm" class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.project_review.delete_permanently') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->getRestoreButtonAttribute() .
                $this->getDeletePermanentlyButtonAttribute();
        }

        return
			$this->getEditButtonAttribute() .
            $this->getShowButtonAttribute() .
            $this->getStatusButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }
}