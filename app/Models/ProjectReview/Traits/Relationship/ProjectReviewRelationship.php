<?php

namespace App\Models\ProjectReview\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\CommentRoom\CommentRoom;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\Quote\Quote;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ProjectReviewRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function professional()
    {
        return $this->belongsTo(Professional::class);
    }

    public function quote()
    {
        return $this->belongsTo(Quote::class);
    }

    /**
     * Get all review comments room
     */
    public function comments_room()
    {
        return $this->morphMany(CommentRoom::class, 'roomtable');
    }


}