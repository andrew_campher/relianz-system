<?php

namespace App\Models\Question;

use App\Models\Question\Traits\Attribute\QuestionAttribute;
use App\Models\Question\Traits\Relationship\QuestionRelationship;
use App\Models\Question\Traits\Scope\QuestionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use QuestionScope,
        SoftDeletes,
        QuestionAttribute,
        QuestionRelationship;
    //
    protected $fillable = [
        'title',
        'description',
        'status',
        'global',
        'question_type'
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = 'questions';
    }
}
