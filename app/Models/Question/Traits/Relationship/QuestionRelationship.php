<?php

namespace App\Models\Question\Traits\Relationship;
use App\Models\QuestionOption\QuestionOption;
use App\Models\Service\Service;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait QuestionRelationship
{

    public function services()
    {
        return $this->belongsToMany(Service::class)->withTimestamps();
    }

    public function options()
    {
        return $this->hasMany(QuestionOption::class);
    }

}