<?php

namespace App\Models\QuestionAnswer;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\QuestionAnswer\Traits\Attribute\QuestionAnswerAttribute;
use App\Models\QuestionAnswer\Traits\Relationship\QuestionAnswerRelationship;
use App\Models\QuestionAnswer\Traits\Scope\QuestionAnswerScope;
use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    use QuestionAnswerScope,
        SoftDeletes,
        QuestionAnswerAttribute,
        QuestionAnswerRelationship;
    //

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_question_answer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'question_service_id',
        'question_option_id',
        'answer_value',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
