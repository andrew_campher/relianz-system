<?php

namespace App\Models\QuestionAnswer\Traits\Relationship;
use App\Models\Question\Question;
use App\Models\QuestionOption\QuestionOption;
use App\Models\Service\Service;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait QuestionAnswerRelationship
{

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function option()
    {
        return $this->belongsTo(QuestionOption::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

}