<?php

namespace App\Models\QuestionOption;

use App\Models\QuestionOption\Traits\Attribute\QuestionOptionAttribute;
use App\Models\QuestionOption\Traits\Relationship\QuestionOptionRelationship;
use App\Models\QuestionOption\Traits\Scope\QuestionOptionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionOption extends Model
{
    use QuestionOptionScope,
        SoftDeletes,
        QuestionOptionAttribute,
        QuestionOptionRelationship;
    //
    protected $fillable = [
        'title',
        'set_value',
        'description',
        'question_id',
        'special'
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = 'questions_options';
    }
}
