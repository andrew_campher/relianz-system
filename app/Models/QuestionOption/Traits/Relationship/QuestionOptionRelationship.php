<?php

namespace App\Models\QuestionOption\Traits\Relationship;
use App\Models\Question\Question;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait QuestionOptionRelationship
{
    public function services()
    {
        return $this->belongsToMany(Question::class)->withTimestamps();
    }

}