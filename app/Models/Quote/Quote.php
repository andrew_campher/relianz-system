<?php

namespace App\Models\Quote;

use App\Models\Quote\Traits\Attribute\QuoteAttribute;
use App\Models\Quote\Traits\Relationship\QuoteRelationship;
use App\Models\Quote\Traits\Scope\QuoteScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quote extends Model
{
    use QuoteScope,
        SoftDeletes,
        QuoteAttribute,
        QuoteRelationship;
    //
    protected $fillable = [
        'price',
        'price_type',
        'description',
        'user_id',
        'professional_id',
        'service_id',
        'status',
        'need_more_info',
        'project_id',
        'hired',
    ];

    protected $dates = ['deleted_at', 'read_at'];

    protected $table = 'quotes';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
