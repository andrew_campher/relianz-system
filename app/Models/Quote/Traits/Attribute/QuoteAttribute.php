<?php

namespace App\Models\Quote\Traits\Attribute;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Request;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait QuoteAttribute
{

    protected $route_prefix = 'frontend';

    protected $status_keys = [
        0 => 'draft',
        1 => 'sent',
        2 => 'read',
        3 => 'hired',
        4 => 'unsuccessful',
        5 => 'reviewed',
        6 => 'interacted',
        7 => 'request_hired',
    ];

    private function __construct() {
        $this->getRoutePrefix();
    }

    /**
     * If passed a ID returns the specific status name.
     * If passed a status name it will return the status key
     * @param $find
     * @return mixed
     */
    public function getStatusKey($find) {
        if (is_numeric($find)) {
            return $this->status_keys[$find];
        } else {
            return array_search($find, $this->status_keys);
        }
    }
	/**
	 * @return string
	 */
    public function getStatusLabelAttribute()
    {

        switch ($this->status) {
            case 0:
                #Draft pending Credits purchase
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.draft_description')."' title='Draft Status' class='label label-danger'>Draft</span>";
                break;

            case 1:
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.sent_description')."' title='Sent Status' class='label label-warning'>Sent</span>";
                break;

            case 2:
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.read_description')."' title='Read Status' class='label label-info'>Read</label>";
                break;

            case 3:
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.hired_description')."' title='Hired Status' class='label label-success'><i class='fa fa-handshake-o'></i> Hired</span>";
                break;

            case 4:
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.unsuccessful_description')."' title='Unsuccessful Status' class='label label-danger'>Unsuccessful</span>";
                break;

            case 5:
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.reviewed_description')."' title='Reviewed Status' class='label label-success'><i class='fa fa-handshake-o'></i> Reviewed</span>";
                break;

            case 6:
                return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.interacted_description')."' title='Interacted Status' class='label label-info'>Interacted</span>";
                break;

            case 7:
                #if this is professional
                if(access()->id() == $this->user_id) {
                    return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.request_hired_description')."' title='Pending Hire Status' class='label label-info'>Pending Hire</span>";
                } else {
                    #else client
                    return "<span data-toggle='popover' data-content='".trans('strings.frontend.quotes.status.'.access()->group().'.request_hired_description')."' title='Confirm Hire Status' class='label label-danger'>Confirm Hire?</span>";
                }

                break;
        }
    }

    public function getPriceLabelAttribute()
    {
        if ($this->need_more_info) {
            return '<div data-toggle="popover" data-content="Professional needs more information in order to quote you" class="badge label-warning"><i class="fa fa-question-circle"></i> more info</div>';
        } else {
            return '<div class="text-bold">'.Helper::formatPrice($this->price).' '.trans('strings.frontend.quotes.price_types.'.$this->price_type).'</div>';
        }

    }

    public function getRoutePrefix() {
        if (Request::is('*admin*')) {
            $this->route_prefix = 'admin';
        }
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        #Only allow edit of Quote before client has read it.
        if ($this->status < 2) {
            #Go to quote/edit on BACKEND
            if ($this->route_prefix == 'admin') {
                return '<a href="' . route($this->route_prefix . '.quote.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
            } else {
                #Go to project page
                return '<a href="' . route($this->route_prefix . '.project.show', [$this->project_id, 'quote_id'=>$this->id]) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
            }
        }
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        if ($this->user_id != access()->id()) {
            switch ($this->status) {
                case 0:
                    return '<a href="' . route($this->route_prefix.'.quote.mark', [
                        $this,
                        1
                    ]) . '" class="btn btn-sm btn-success"><i class="fa fa-play" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.quotess.activate') . '"></i></a> ';
                // No break

                case 1:
                    return '<a href="' . route($this->route_prefix.'.quote.mark', [
                        $this,
                        0
                    ]) . '" class="btn btn-sm btn-warning"><i class="fa fa-pause" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.quotess.deactivate') . '"></i></a> ';
                // No break

                default:
                    return '';
                // No break
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if ($this->route_prefix == 'admin') {
            return '<a href="' . route($this->route_prefix.'.quote.destroy', $this) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
                 data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
                 class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
        }

        return '';
    }

    public function getShowButtonAttribute()
    {
        if ($this->route_prefix == 'admin')
            return '<a href="' . route($this->route_prefix.'.quote.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="' . route($this->route_prefix.'.quote.restore', $this) . '" name="restore_user" class="btn btn-sm btn-info"><i class="fa fa-refresh" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.quotess.restore_user') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="' . route($this->route_prefix.'.quote.delete-permanently', $this) . '" name="delete_user_perm" class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.quotess.delete_permanently') . '"></i></a> ';
    }


    public function getMarkHiredButtonAttribute()
    {
        if ($this->status == 2 || $this->status == 6)
            return '<a data-toggle="popover" data-trigger="hover" data-content="Marking this quote as hired will send a notification to client to confirm this." id="qt-btn-'.$this->id.'" href="javascript:void(0)" onclick="MarkQuoteHired('.$this->id.', this)" class="btn btn-success btn-sm"><i class="fa fa-handshake-o"></i> Mark as Hired</a> ';
    }

    public function isPending()
    {
        if ($this->status == 1 || $this->status == 2 || $this->status == 6) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->getRestoreButtonAttribute() .
                $this->getDeletePermanentlyButtonAttribute();
        }

        return
			$this->getEditButtonAttribute() .
            $this->getShowButtonAttribute() .
            $this->getMarkHiredButtonAttribute() .
            $this->getStatusButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }
}

