<?php

namespace App\Models\Quote\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\ClientReview\ClientReview;
use App\Models\CommentRoom\CommentRoom;
use App\Models\Media\Media;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\ProjectReview\ProjectReview;
use App\Models\Service\Service;
use App\Models\Transaction\Transaction;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait QuoteRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function professional()
    {
        return $this->belongsTo(Professional::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function review()
    {
        return $this->hasOne(ProjectReview::class);
    }

    public function client_review()
    {
        return $this->hasOne(ClientReview::class);
    }

    /**
     * Get all of the quote's media
     */
    public function media()
    {
        return $this->morphMany(Media::class, 'modelname');
    }

    public function comments_room()
    {
        return $this->morphOne(CommentRoom::class, 'roomtable');
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'modelowner');
    }


}