<?php

namespace App\Models\SearchLog;

use Illuminate\Database\Eloquent\Model;

class SearchLog extends Model
{
    //
    protected $fillable = [
        'search_term',
        'hits',
        'searches',
        'IP',
        'searches_week',
        'searches_month',
        'searches_year',
        'found',
    ];

    protected $table = 'locations';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
