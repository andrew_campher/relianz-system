<?php

namespace App\Models\Service;

use App\Helpers\SearchHelper;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Service\Traits\Attribute\ServiceAttribute;
use App\Models\Service\Traits\Relationship\ServiceRelationship;
use App\Models\Service\Traits\Scope\ServiceScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Service extends Model
{
    use ServiceScope,
        SoftDeletes,
        ServiceAttribute,
        ServiceRelationship,
        Sluggable;
    //

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'pro_title',
        'description',
        'occupation_id',
        'slug',
        'tags',
        'pro_slug',
        'hits_year',
        'hits_month',
        'hits_day',
        'hits',
        'fee',
        'image',
        'status'
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = 'services';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ],
            'pro_slug' => [
                'source' => 'pro_title'
            ]
        ];
    }

    /**
     * Pull the Fee for this service - check package details etc.
     * @return int|mixed
     */
    public function getFee($string = false)
    {
        if (access()->isRole('Professional') && access()->user()->professional->approved) {
            if (access()->user()->isRole('Professional') && access()->user()->professional->account->package->free) {
                if ($string) {
                    return 'free';
                } else {
                    return 0;
                }

            }
        }

        return $this->fee;
    }

    public static function logSearch($searchString, $found_it=0) {
        if(!$searchString)
            return false;

        $cleaned_string = SearchHelper::removeCommonWords($searchString);

        #Log Search if MOre than 3 characters
        if (strlen($cleaned_string) > 3) {
            DB::statement('INSERT INTO search_log SET search_term = ?, searches = 1, searches_week = 1, searches_month = 1, searches_year = 1, `found` = ?, created_at = NOW(), updated_at = NOW()
                 ON DUPLICATE KEY UPDATE 
                 searches= searches+1, 
                 searches_week = searches_week+1, searches_month = searches_month+1, searches_year = searches_year+1, 
                 `found` = ?, 
                 updated_at = NOW()',
                [$searchString, $found_it, $found_it]);
        }

        return true;
    }

    public function fillTags($service)
    {
        $removewords = ['services', 'service'];

        $tags_string = $service->title;

        $tags_string .= ' '.$service->tags;

        if ($service->categories) {
            foreach ($service->categories AS $cat) {
                $tags_string .= ' ' . $cat->title;
                $tags_string .= ' ' . $cat->tags;
            }
        }

        if ($service->occupation) {
            $tags_string .= ' ' . $service->occupation->title;
            $tags_string .= ' ' . $service->occupation->tags;
        }

        $tags_string = SearchHelper::removeCommonWords($tags_string, $removewords);

        $tags_string = str_replace("/", " ", $tags_string);

        $single_words = explode(" ", $tags_string);

        foreach ($single_words AS $sword) {
            if ($sword) {
                if (ctype_alpha($sword)) {
                    $tags_string .= ' ' . str_singular($sword);
                    $tags_string .= ' ' . str_plural($sword);
                }
            }
        }

        $tags_string = strtolower($tags_string);

        //REMOVE DUPLICATE WORDS:
        $tags_string = implode(' ', array_unique(explode(' ', $tags_string)));

        $tags_string = trim($tags_string);
        $tags_string = preg_replace('/\s\s+/', ' ', $tags_string);


        DB::table('services')
            ->where('id', $service->id)
            ->update(['tags' => strtolower($tags_string)]);
    }
}
