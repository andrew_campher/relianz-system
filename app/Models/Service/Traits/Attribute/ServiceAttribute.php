<?php

namespace App\Models\Service\Traits\Attribute;


trait ServiceAttribute
{

	/**
	 * @return string
	 */
	public function getStatusLabelAttribute()
	{
		if ($this->isActive())
			return "<label class='label label-success'>".trans('labels.general.active')."</label>";
		return "<label class='label label-danger'>".trans('labels.general.inactive')."</label>";
	}

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.service.edit', $this) . '" class="btn btn-sm btn-primary"><i class="fa fa-pencil" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<a href="' . route('admin.service.mark', [
                    $this,
                    1
                ]) . '" class="btn btn-sm btn-success"><i class="fa fa-play" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.activate') . '"></i></a> ';
            // No break

            case 1:
                return '<a href="' . route('admin.service.mark', [
                    $this,
                    0
                ]) . '" class="btn btn-sm btn-warning"><i class="fa fa-pause" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.deactivate') . '"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {

        return '<a href="' . route('admin.service.destroy', $this) . '"
             data-method="delete"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';

    }

    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.service.show', $this) . '" class="btn btn-sm btn-info"><i class="fa fa-search" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="' . route('admin.service.restore', $this) . '" name="restore_item" class="btn btn-sm btn-info"><i class="fa fa-refresh" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.restore') . '"></i></a> ';
    }

	/**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="' . route('admin.service.delete-permanently', $this) . '" name="delete_item_perm" class="btn btn-sm btn-danger"><i class="fa fa-trash" data-toggle="popover" data-trigger="hover" data-placement="top" title="' . trans('buttons.backend.general.delete_permanently') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->getRestoreButtonAttribute() .
                $this->getDeletePermanentlyButtonAttribute();
        }

        return
			$this->getEditButtonAttribute() .
            $this->getShowButtonAttribute() .
            $this->getStatusButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }

    public function getImageHtmlAttribute() {
        return '<img src="/images/'.$this->table.'/thumbnail/'.$this->image.'" />';
    }
}