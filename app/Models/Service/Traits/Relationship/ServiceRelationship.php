<?php

namespace App\Models\Service\Traits\Relationship;
use App\Models\Category\Category;
use App\Models\Occupation\Occupation;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\Question\Question;
use App\Models\QuestionOption\QuestionOption;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ServiceRelationship
{

    /**
     * Many-to-Many relations with Categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Many-to-Many relations with Categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions()
    {
        return $this->belongsToMany(Question::class)->where('questions.status', 1);
    }

    public function occupation()
    {
        return $this->belongsTo(Occupation::class);
    }

    public function professionals()
    {
        return $this->belongsToMany(Professional::class)->with('stats')->orderBy('professionals.hits', 'desc')->limit(5);
    }

    public function projects()
    {
        return $this->hasMany(Project::class)->with('user')->orderBy('projects.id', 'desc')->limit(5);
    }
}