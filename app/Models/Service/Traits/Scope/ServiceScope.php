<?php

namespace App\Models\Service\Traits\Scope;
use Illuminate\Support\Facades\DB;

/**
 * Class UserScope
 * @package App\Models\Access\User\Traits\Scope
 */
trait ServiceScope
{

	/**
	 * @param $query
	 * @param bool $status
	 * @return mixed
	 */
	public function scopeActive($query, $status = true) {
		return $query->where('status', $status);
	}

    public function scopeUpdateHits($query) {
        return $query->update([
            'hits'      => DB::raw('hits + 1'),
            'hits_day'  => DB::raw('hits_day + 1'),
            'hits_month'=> DB::raw('hits_month + 1'),
            'hits_year' => DB::raw('hits_year + 1'),
        ]);
    }

    public function scopeProfessionalsByDistance($query, $lat, $lng, $radius) {
        $distance_sql = "6371 * acos( cos( radians(?) ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( lat ) ) )";
        return $query->select('professionals.*')->selectRaw($distance_sql.' AS distance', [$lat, $lng, $lat])
            ->whereRaw($distance_sql." <= ?", [$lat, $lng, $lat, $radius]);
    }

    public function scopeProfessionalsByLocation($query, $location_id) {
        return $query->where('status', $status);
    }
}