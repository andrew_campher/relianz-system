<?php

namespace App\Models\Supplier;


use App\Models\Supplier\Traits\Attribute\SupplierAttribute;
use App\Models\Supplier\Traits\Relationship\SupplierRelationship;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use SupplierAttribute, SupplierRelationship;

    protected $connection = 'qbdb';

    protected $primaryKey = 'ID';

    protected $table = 'Vendors';

    public $incrementing = false;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
