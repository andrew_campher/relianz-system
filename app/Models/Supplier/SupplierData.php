<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;

class SupplierData extends Model
{

    protected $connection = 'qbdb';

    protected $primaryKey = 'VendorId';

    protected $table = 'vendors_data';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['VendorId', 'Class'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
