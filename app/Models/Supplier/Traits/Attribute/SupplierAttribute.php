<?php

namespace App\Models\Supplier\Traits\Attribute;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait SupplierAttribute
{

    public function showClass() {
        if (isset($this->data) && $this->data) {
            switch ($this->data->Class) {
                case "A":
                    return '<span class="label label-success label-sm">A</span>';
                    break;
                case "B":
                    return '<span class="label label-warning label-sm">B</span>';
                    break;
                case "C":
                    return '<span class="label label-danger label-sm">C</span>';
                    break;
            }
        }
    }
}