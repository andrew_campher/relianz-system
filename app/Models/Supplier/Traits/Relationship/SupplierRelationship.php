<?php

namespace App\Models\Supplier\Traits\Relationship;
use App\Models\Supplier\SupplierData;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait SupplierRelationship
{

    public function data()
    {
        return $this->hasOne(SupplierData::class, 'VendorId', 'ID');
    }

}