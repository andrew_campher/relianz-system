<?php

namespace App\Models\Transaction\Traits\Relationship;
use App\Models\Access\User\User;
use App\Models\Account\Account;
use App\Models\ClientReview\ClientReview;
use App\Models\Comment\Comment;
use App\Models\CommentRoom\CommentRoom;
use App\Models\Media\Media;
use App\Models\Professional\Professional;
use App\Models\Project\Project;
use App\Models\ProjectReview\ProjectReview;
use App\Models\Service\Service;


/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait TransactionRelationship
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }


}