<?php

namespace App\Models\Transaction;

use App\Models\Transaction\Traits\Relationship\TransactionRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes,
        TransactionRelationship;
    //
    protected $fillable = [
        'account_id',
        'user_id',
        'professional_id',
        'cost',
        'servicetable_id',
        'servicetable_type',
    ];

    protected $table = 'transactions';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
