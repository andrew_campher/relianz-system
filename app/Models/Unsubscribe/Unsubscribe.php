<?php

namespace App\Models\Unsubscribe;

use Illuminate\Database\Eloquent\Model;

class Unsubscribe extends Model
{
    //
    protected $fillable = [
        'user_id',
        'email',
        'subscribed',
        'notification',
    ];

    protected $table = 'unsubscribes';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
