<?php

namespace App\Models\UserStat\Traits\Attribute;


trait UserStatAttribute
{
    public function getStatPayStarsAttribute()
    {
        $rating = round($this->stat_pay,1);

        return Helper::showStars($rating).' '.$rating;
    }

    public function getStatUnderstandingStarsAttribute()
    {
        $rating = round($this->stat_understanding,1);

        return Helper::showStars($rating).' '.$rating;
    }

    public function getStatClearStarsAttribute()
    {
        $rating = round($this->stat_clear,1);

        return Helper::showStars($rating).' '.$rating;
    }
}