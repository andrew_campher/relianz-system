<?php

namespace App\Models\UserStat;

use App\Models\UserStat\Traits\Attribute\UserStatAttribute;
use App\Models\UserStat\Traits\Relationship\UserStatRelationship;
use App\Models\UserStat\Traits\Scope\UserStatScope;
use Illuminate\Database\Eloquent\Model;

class UserStat extends Model
{
    use UserStatScope,
        UserStatAttribute,
        UserStatRelationship;
 
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_stats';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'hires_count',
        'reviews_count',
        'stat_pay',
        'stat_understanding',
        'stat_clear',
        'recommends_count',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
