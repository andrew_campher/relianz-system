<?php

namespace App\Notifications\ClientReview;

use App\Models\ClientReview\ClientReview;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class UserNotifyNewClientReview extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $client_review;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(ClientReview $client_review)
    {
        $this->client_review = $client_review;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject('The professional has reviewed you')
			->line($this->client_review->professional->title.' has reviewed you.')
			->action('View Review', route('frontend.user.index'))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'project_id' => $this->client_review->project_id,
            'professional_id' => $this->client_review->professional_id,
            'user_id' => $this->client_review->user_id,
            'quote_id' => $this->client_review->id,
        ];
    }
}
