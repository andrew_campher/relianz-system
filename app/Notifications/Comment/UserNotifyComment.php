<?php

namespace App\Notifications\Comment;

use App\Models\Comment\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserNotifyComment extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($notifiable->isOnline()) {
            return ['broadcast'];
        } else {
            return ['mail', 'broadcast'];
        }

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You have a new message via HandyHire')
            ->line('<b>'.$this->comment->user->name.'</b> has sent you a message:')
            ->line('<hr><blockquote>'.$this->comment->body.'</blockquote><hr>')
            ->action('View ' . ucfirst($this->comment->comments_room->roomtable_type), route('frontend.' . $this->comment->comments_room->roomtable_type . '.show', $this->comment->comments_room->roomtable_id))
            ->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {

    }


    public function toBroadcast($notifiable)
    {
        return [
            'message' => $this->comment->body,
            'user' => $this->comment->user->name,
            'text' => '<a href="'.route('frontend.'.$this->comment->comments_room->roomtable_type.'.show', $this->comment->comments_room->roomtable_id).'"><h2 class="no-top-margin">💬 New Message</h2><b>'.$this->comment->user->name.'</b><p>'.str_limit($this->comment->body, 50).'</p></a>'
        ];
    }

}
