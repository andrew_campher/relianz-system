<?php

namespace App\Notifications\Frontend\Auth;

use App\Models\Professional\Professional;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class ProClaimConfirmation extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $confirmation_code;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct($hash, Professional $professional)
    {
        $this->hash = $hash;
        $this->professional = $professional;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /**
         * Simple has of professional ID and notify_email
         */
        $hash = md5($this->professional->id. $this->professional->notify_email);

        return (new MailMessage)
            ->success()
			->subject('Is '.$this->professional.' your business?')
			->line('Click the link below to claim your listing:')
			->action('Claim Listing', route('public.professional.edit',[$this->professional->id, $this->hash]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }
}
