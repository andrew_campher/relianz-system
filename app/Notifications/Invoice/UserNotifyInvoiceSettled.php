<?php

namespace App\Notifications\Invoice;

use App\Models\Invoice\Invoice;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserNotifyInvoiceSettled extends Notification
{
    use Queueable;

    protected $invoice;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->subject('Payment Received - Thank you')
            ->line('Your payment on invoice '.config('app.invoice.prefix').$this->invoice->id.' has been received.');

        #if Credit Purchase
        if ($this->invoice->credit_quantity) {
            $mail = $mail
                ->line('Your '.$this->invoice->credit_quantity.' credits have been added to your account.')
                ->action('Send Quotes Now!', route('frontend.professional.dashboard', $this->invoice->professional_id))
                ->line(trans('strings.emails.auth.thank_you_for_using_app'));
        }
        $mail = $mail
            ->line(trans('strings.emails.auth.thank_you_for_using_app'));

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'account_id' =>  $this->invoice->account_id,
            'professional_id' =>  $this->invoice->professional_id,
            'user_id' =>  $this->invoice->user_id
        ];
    }


    /**
     * SET the channel to broad cast to the professional -> user
     * @return PrivateChannel
     */
    public function broadcastOn() {
        return new PrivateChannel('App.Models.Access.User.User.'.$this->invoice->professional->user_id);
    }


    public function toBroadcast($notifiable)
    {
        return [
            'credit_quantity' => $this->invoice->credit_quantity,
            'text' => '<h2 class="no-top-margin">New Credits Added</h2><b>Your paymnent has been received and your credits have been assigned.</b><p>Go ahead and send some quotes!</p>'
        ];
    }


}
