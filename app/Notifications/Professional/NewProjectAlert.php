<?php

namespace App\Notifications\Professional;

use App\Helpers\Helper;
use App\Models\ProfessionalBranch\ProfessionalBranch;
use App\Models\Project\Project;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\View;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class NewProjectAlert extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	public $entity;

    public $branch;

    public $answers;

    /**
     * NewProjectAlert constructor.
     * @param Project $project
     * @param $professional
     */
	public function __construct(Project $project,ProfessionalBranch $branch, $answers)
    {
        $this->entity = $project;
        $this->branch = $branch;
        $this->answers = $answers;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * SET the channel to broad cast to the professional -> user
     * @return PrivateChannel
     */
    public function broadcastOn() {
        if ($this->branch->professional->user_id) {
            return new PrivateChannel('App.Models.Access.User.User.' . $this->branch->professional->user_id);
        } else {
            return false;
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $view = View::make('frontend.project.includes.partials.answers', ['answers' => $this->answers, 'project' => $this->entity]);
        $ans_content = $view->render();

        $distance = Helper::distance($this->entity->lat, $this->entity->lng, $this->branch->lat, $this->branch->lng);

        $project_details = '<div class="answer_wrap">
                <h3>Area</h3>
                <p>'.$this->entity->area.'</p>
                <p><b>'.$this->entity->user->first_name.' is '.round($distance, 2).'KM from you</b></p>
            </div>';

        $project_details .= '<div class="answer_wrap">
                <h3>When</h3>
                <p>'.$this->entity->whenlabel.' '.$this->entity->when_describe.'</p>
            </div>';

        if ($this->entity->description) {
            $project_details .= '<div class="answer_wrap">
                <h3>Description</h3>
                <p>' . $this->entity->description . '</p>
            </div>';
        }

        $project_url = route('frontend.project.show', $this->entity->id);

        /**
         * UNCONFIRMED PROFESSIONAL
         * MAKE URL with Hash
         */
        if ($this->branch->professional->status == 2) {
            $hash = md5($this->branch->professional->notify_email . $this->entity->id);
            $project_url = route('public.project.details', [$this->entity->id, $this->branch->professional_id, $hash]);
        }
        $mail_message = (new MailMessage)
            ->success()
            ->subject('Quote Request from '.$this->entity->user->first_name.' via '.app_name())
            ->greeting('To: '.$this->branch->professional->title)
            ->line('A customer has requested a quote for a service that you offer via HandyHire.co.za. Please see his details below:')
            ->line('<h3>'.$this->entity->user->first_name.' requests '.(strpos(strtolower($this->entity->service->title),'services')?$this->entity->service->title:$this->entity->service->title.' services').'</h3>')
            ->line($project_details)
            ->line($ans_content)
            ->line('<p><b><a href="'.$project_url.'">View full request online (Photos etc.)</a></b></p>')
            ->action('Respond to Request', $project_url);


        if ($this->branch->professional->status == 2) {
            $mail_message =  $mail_message->line('<h3>Questions?</h3>')
                ->line('<p style="padding:10px;background:#f1f2f6"><strong>How do you have my details?</strong><br/>When customers request the service of a professional we look online and offline for businesses like yours that could assist them.</p>')
                ->line('<p style="padding:10px;background:#f1f2f6"><strong>Why no customer contact details?</strong><br/>Customers prefer to first review your business profile and offer before sharing their direct contact details.</p>')
                ->line('<p style="padding:10px;background:#f1f2f6"><strong>How to respond to Customer?</strong><br/>Please click on the above <b>"Respond to Request"</b> button - this will take you to the HandyHire website and allow you to contact this customer.</p>')
                ->line('<p style="padding:10px;background:#f1f2f6"><strong>How much does it cost?</strong><br/>'.(!config('app.settings.enable_credits')?'<b>As part of our launch phase all leads are FREE to contact.</b><br/>':'').'Its free to join and view leads on HandyHire, we will only charge a small fee when you wish to contact the lead.</p>')
                ->line('<p style="padding:10px;background:#f1f2f6"><strong>What is HandyHire?</strong><br/>We market your services to customers and provide a platform for them to create Quote Requests like this one. HandyHire makes requesting quotes convenient for the customer which encourages them to request more services.</p>')
                ->line('More questions on our <a href="'.route('public.page', 'faq-professionals').'">FAQ</a>');
        }

        $mail_message = $mail_message->line('<hr>')
            ->line('For support please call us on <b>'.env('COMPANY_PHONE', '021 917 1719').'</b> during office hours:<br/>Mon - Fri 8am - 5pm.');

        return $mail_message;
    }

    public function toBroadcast($notifiable)
    {
        return [
            'message' => 'New Request: '.$this->entity->title,
            'user' => $this->entity->user->short_name,
            'text' => '<a href="'.route('frontend.project.show', $this->entity->id).'"><h2 class="no-top-margin">New Quote Request</h2><b>'.$this->entity->user->short_name.'</b> has requested your services with his project:<p>'.str_limit($this->entity->title, 50).'</p></a>'
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'project_id' =>  $this->entity->id,
        ];
    }
}
