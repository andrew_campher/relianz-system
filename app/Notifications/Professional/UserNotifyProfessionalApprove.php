<?php

namespace App\Notifications\Professional;

use App\Models\Professional\Professional;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class UserNotifyProfessionalApprove extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $professional;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(Professional $professional)
    {
        $this->professional = $professional;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject(app_name() . ': Professional Approved')
			->line('Your professional profile was approved')
			->action('View Profile', route('public.professional.show', [$this->professional->id, $this->professional->slug]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    public function toDatabase($notifiable)
    {
        return [
            'professional_id' => $this->professional->id,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.professional.dashboard').'"><h2 class="no-top-margin">Professional Account Approved</h2></a>'
        ];
    }
}
