<?php

namespace App\Notifications\Project;

use App\Models\Project\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class AdminNotifyNewProject extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $project;
    protected $when_status;


	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(Project $project)
    {
        $this->project = $project;
        $this->when_status = $project->when;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['mail', 'database', 'broadcast'];
        if ($this->when_status == 1 && $this->project->customer_mobile) {
            $via[] = 'nexmo';
        }

        return $via;
    }

    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
            ->content('#'.$this->project->id.' PENDING PROJECT: '.$this->project->user->name.'. "'.$this->project->title.'"');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject(app_name() . ': New Project Pending')
			->line('A user has added a new project and is pending approval.')
            ->line('Title: '.$this->project->title)
            ->line('User: '.$this->project->user->name)
			->action('View Project', route('frontend.project.show', [$this->project->id]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    public function toDatabase($notifiable)
    {
        return [
            'project_id' => $this->project->id,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.project.show', $this->project->id).'"><h2 class="no-top-margin">New Pending Project</h2><p>A user has added a new project.</p></a>'
        ];
    }
}
