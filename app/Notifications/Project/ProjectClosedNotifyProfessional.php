<?php

namespace App\Notifications\Project;

use App\Models\Project\Project;
use App\Models\Quote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class ProjectClosedNotifyProfessional extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $project;

    protected $quote;

    protected $data;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(Project $project, $data = array())
    {
        $this->project = $project;

        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject(app_name() . ': Project Closed')
			->line('A project that you quoted has been closed by the client.')
            ->line('Reason: '.$this->data['close_reason'])
			->action('View Project', route('frontend.project.show', [$this->project->id]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }
}
