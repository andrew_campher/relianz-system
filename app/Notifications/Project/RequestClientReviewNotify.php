<?php

namespace App\Notifications\Project;

use App\Models\Project\Project;
use App\Models\Quote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class RequestClientReviewNotify extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $quote;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject('Review '.$notifiable->professional->title)
			->line('You recently selected '.$notifiable->professional->title.' to work on a project of yours. If they have completed, would you mind reviewing them?')
			->action('Write Review', route('frontend.quote.show', [$notifiable->id]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }
}
