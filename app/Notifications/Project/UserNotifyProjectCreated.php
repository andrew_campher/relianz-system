<?php

namespace App\Notifications\Project;

use App\Models\Project\Project;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class UserNotifyProjectCreated extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $project;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    public function broadcastOn() {
        return new PrivateChannel('App.Models.Access.User.User.'.$this->project->user_id);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject(app_name() . ': Project Created')
			->line('Your project "'.$this->project->title.'" was created!')
            ->line('We will soon notify all the relevant professionals in your area. Sit back and wait for them to send you quotes.')
			->action('View Project', route('frontend.project.show', [$this->project->id]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'project_id' => $this->project->id,
            'text' => '<h2 class="no-top-margin">Project Created</h2><b>Your project <b>'.$this->project->title.'</b> has been saved.</p>',
        ];
    }

}
