<?php

namespace App\Notifications\Project;

use App\Models\Project\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class UserNotifyProjectFulfilled extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $project;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject(app_name() . ': Project Expired')
			->line('Your project has expired. This means that it has been listed for the allowed '.config('app.settings.request_days_expire').' days. You should consider hiring any of the proposed professionals. Otherwise consider relisting your project to alert new professionals of your needs.')
			->action('View Project', route('frontend.project.show', [$this->project->id]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    public function toArray($notifiable)
    {
        return [
            'project_id' => $this->project->id,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.project.show', $this->project->id).'"><h2 class="no-top-margin">Project Expired</h2><b>Your project has reached the maximum days alloed to be listed. You may relist.</b></a>'
        ];
    }
}
