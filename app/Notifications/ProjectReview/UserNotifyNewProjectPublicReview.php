<?php

namespace App\Notifications\ProjectReview;

use App\Models\ProjectReview\ProjectReview;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class UserNotifyNewProjectPublicReview extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $project_review;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(ProjectReview $project_review)
    {
        $this->project_review = $project_review;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject('Review Pending - You have a new review')
			->line('A user has submitted a new review of your business. It is waiting to be approved.')
			->action('View Review', route('frontend.project_review.show', $this->project_review->id))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'project_review_id' => $this->project_review->id,
        ];
    }
}
