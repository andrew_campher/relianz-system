<?php

namespace App\Notifications\ProjectReview;

use App\Models\ProjectReview\ProjectReview;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class UserNotifyNewProjectReview extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $project_review;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(ProjectReview $project_review)
    {
        $this->project_review = $project_review;
    }

    public function broadcastOn() {
        return new PrivateChannel('App.Models.Access.User.User.'.$this->project_review->professional->user_id);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject('Your job was reviewed')
			->line($this->project_review->user->name.' has reviewed your job.')
			->action('View Review', route('public.professional.show_reviews', [$this->project_review->professional->id, $this->project_review->professional->slug]))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'project_review_id' => $this->project_review->id,
            'project_id' => $this->project_review->project_id,
            'quote_id' => $this->project_review->id,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.quote.show', $this->project_review->quote_id).'"><h2 class="no-top-margin">Your job has been reviewed</h2>The client has reviewed your job.</a>'
        ];
    }

}
