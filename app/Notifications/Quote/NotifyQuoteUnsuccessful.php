<?php

namespace App\Notifications\Quote;

use App\Models\Quote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class NotifyQuoteUnsuccessful extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $quote;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
			->subject('Quote was Unsuccessful')
			->line('Unfortunately your quote to '.$this->quote->project->user->shortname.' was unsuccessful.')
            ->line('Reason: '.(isset($this->quote->decline_reason)?$this->quote->decline_reason:'The client has chosen another professional.'))
            ->line('Ensure that your profile is complete and that your quotes are written with enough information.')
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'project_id' => $this->quote->project_id,
            'service_id' => $this->quote->service_id,
            'professional_id' => $this->quote->professional_id,
            'quote_id' => $this->quote->id,
            'price' => $this->quote->price,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.quote.show', $this->quote->id).'"><h2 class="no-top-margin">Quote Unsuccessful</h2><p>'.(isset($this->quote->decline_reason)?$this->quote->decline_reason:'The client has chosen another service provider').'.</p></a>'
        ];
    }


}
