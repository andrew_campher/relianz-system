<?php
/**
 * Notification to let PROFESSIONAL know that the client has marked their quote as HIRED
 */

namespace App\Notifications\Quote;

use App\Models\Quote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyReviewHire extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Review professional')
            ->line('You have recently chosen a Professional on '.config('app.name').'. If the professional has completed the job please review his work.')
            ->action('Review '.$this->quote->professional->title, route('frontend.quote.show', [$this->quote->id]))
            ->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.quote.show', $this->quote->id).'"><h2 class="no-top-margin">Review Professional</h2><b>Please review your recent chosen professional.</b></a>'
        ];
    }
}
