<?php
/**
 * Notify CLIENT to validate the Hire from the professional.
 */

namespace App\Notifications\Quote;

use App\Helpers\Helper;
use App\Models\Quote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserNotifyConfirmHired extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Please verify this hire?')
                    ->line($this->quote->professional->title)
                    ->line(Helper::formatPrice($this->quote->price, false))
                    ->action('View Quote', route('frontend.quote.show', [$this->quote->id]))
                    ->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'project_id' => $this->quote->project_id,
            'professional_id' => $this->quote->project->professional_id,
            'client_id' => $this->quote->project->user_id,
            'quote_id' => $this->quote->id,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.quote.show', $this->quote->id).'"><h2 class="no-top-margin">Confirm Hire</h2><p>A professional requests the confirmation of his hire.</p></a>'
        ];
    }
}
