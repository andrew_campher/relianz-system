<?php

namespace App\Notifications\Quote;

use App\Helpers\Helper;
use App\Models\Quote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class UserNotifyNewQuote extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $quote;

	/**
	 * UserNeedsConfirmation constructor.
	 * @param $confirmation_code
	 */
	public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast', 'nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
			->subject('Quote Received from '.$this->quote->professional->title)
            ->line('Your project:')
            ->line('<h2>'.$this->quote->project->title.'</h2>')
            ->line('has received a new quote from:')
            ->line('<h2>'.$this->quote->professional->getLogoHtml('lg').' '.$this->quote->professional->title.'</h2>')
			->action('View Quote', route('frontend.quote.show', $this->quote->id))
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'project_id' => $this->quote->project_id,
            'service_id' => $this->quote->service_id,
            'professional_id' => $this->quote->professional_id,
            'quote_id' => $this->quote->id,
            'price' => $this->quote->price,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.quote.show', $this->quote->id).'"><h2 class="no-top-margin">Quote Received</h2><b>From '.$this->quote->professional->title.'</b><p>'.str_limit($this->quote->description, 50).'</p></a>'
        ];
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
            ->content('NEW QUOTE #'.$this->quote->id.': from '.strtoupper(str_limit($this->quote->professional->title,40)).'.'.($this->quote->price>0?' Price: '.Helper::formatPrice($this->quote->price, false):'More info needed before quote').'. View on '.config('app.website').'. Contact: '.strtoupper($this->quote->professional->user->first_name).' - '.$this->quote->professional->telephone.'.');
    }


}
