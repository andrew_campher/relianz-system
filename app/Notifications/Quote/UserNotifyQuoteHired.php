<?php
/**
 * Notification to let PROFESSIONAL know that the client has marked their quote as HIRED
 */

namespace App\Notifications\Quote;

use App\Models\Quote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserNotifyQuoteHired extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You have been Hired!')
            ->line($this->quote->project->user->name.' has marked your quote as hired '.$this->quote->project->user->first_name.' is pleased with your estimate and is interested in the next step.')
            ->line('<h3>Customer Contact Details</h3>')
            ->line('Email: <strong><a href="mailto:'.$this->quote->project->user->email.'">'.$this->quote->project->user->email.'</a></strong>')
            ->line('Mobile: <strong>'.$this->quote->project->user->mobile.'</strong>')
            ->line('It is recommended to be in contact with the client as soon as possible.')
            ->action('View the Project', route('frontend.quote.show', [$this->quote->id]))
            ->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'project_id' => $this->quote->project_id,
            'professional_id' => $this->quote->project->professional_id,
            'client_id' => $this->quote->project->user_id,
            'quote_id' => $this->quote->id,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'text' => '<a href="'.route('frontend.quote.show', $this->quote->id).'"><h2 class="no-top-margin">You are Hired!</h2><b>Your estimate on '.$this->quote->project->title.' was chosen by the client.</b></a>'
        ];
    }
}
