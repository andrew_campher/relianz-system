<?php

namespace App\Notifications\Transaction;

use App\Models\Transaction\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation
 * @package App\Notifications\Frontend\Auth
 */
class TransactionRefundedNotify extends Notification
{
    use Queueable;

	/**
	 * @var
	 */
	protected $transaction;

    /**
     * TransactionRefundedNotify constructor.
     * @param Transaction $transaction
     */
	public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
			->subject('Your credits were refunded')
			->line('The client has taken too long to view your quote and therefore we are refunding you your credits. If the project is still open he may still view it and get back to you.')
			->line(trans('strings.emails.auth.thank_you_for_using_app'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'transaction_id' => $this->transaction->id,
            'professional_id' => $this->transaction->account_id,
        ];
    }
}
