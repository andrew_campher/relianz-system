<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

/**
 * Class BroadcastServiceProvider
 * @package App\Providers
 */
class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::routes();

        /*
         * Authenticate the user's personal channel...
         */
        Broadcast::channel('App.Models.Access.User.User.*', function ($user, $userId) {
            return (int) $user->id === (int) $userId;
        });

        Broadcast::channel('chat_room.*', function ($user, $quote_id) {
            if (true) { //TODO: Replace with real ACL
                return ['id' => $user->id, 'name' => $user->name, 'icon_url' => $user->icon_url];
            }
        });

        Broadcast::channel('all-channel', function ($user) {
            if (true) { //TODO: Replace with real ACL
                return true;
            }
        });
    }
}
