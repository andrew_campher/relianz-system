<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Notifications\Events\NotificationSent' => [
            'App\Listeners\Common\Notification\NotificationEventListener@onSent',
        ],
    ];

	/**
	 * Class event subscribers
	 * @var array
	 */
	protected $subscribe = [
        /**
         * Common Subscribers
         */
        \App\Listeners\Common\Project\ProjectEventListener::class,
        \App\Listeners\Common\ProjectReview\ProjectReviewEventListener::class,
        \App\Listeners\Common\Quote\QuoteEventListener::class,
        \App\Listeners\Common\Comment\CommentEventListener::class,
        \App\Listeners\Common\Invoice\InvoiceEventListener::class,
        \App\Listeners\Common\ClientReview\ClientReviewEventListener::class,
        \App\Listeners\Common\Professional\ProfessionalEventListener::class,

		/**
		 * Frontend Subscribers
		 */

		/**
		 * Auth Subscribers
		 */
		\App\Listeners\Frontend\Auth\UserEventListener::class,

		/**
		 * Backend Subscribers
		 */

		/**
		 * Access Subscribers
		 */
		\App\Listeners\Backend\Access\User\UserEventListener::class,
		\App\Listeners\Backend\Access\Role\RoleEventListener::class,
	];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
