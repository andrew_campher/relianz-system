<?php

namespace App\Repositories\Backend\Account;

use App\Models\Account\Account;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountRepository
 * @package App\Repositories\Account
 */
class AccountRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Account::class;

    public function __construct()
    {

    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($data)
    {

        $Account = $this->createAccountStub($data);

        if ($Account->save()) {

            event(new AccountCreated($Account));

            return $Account;
        }

        throw new GeneralException('Unknown error');
    }

    /**
     * @param Model $Account
     * @param array $input
     */
	public function update(Model $Account, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($Account, $data) {
			if (parent::update($Account, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$Account->status = isset($data['status']) ? 1 : 0;
				parent::save($Account);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $Account
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $Account)
    {
        if (access()->id() && access()->id() == $Account->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if (parent::delete($Account)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $Account
	 * @throws GeneralException
	 */
	public function forceDelete(Model $Account)
    {
        if (is_null($Account->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($Account) {
			if (parent::forceDelete($Account)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $Account
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $Account)
    {
        if (is_null($Account->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($Account))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $Account
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $Account, $status)
    {
        if (access()->id() && access()->id() == $Account->id && $status == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $Account->status = $status;

        if (parent::save($Account)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function createAccountStub($data)
    {
    	$Account					= self::MODEL;
        $Account                    = new $Account;
        $Account->credits_remain    = $data['credits_remain'];
        $Account->professional_id   = $data['professional_id'];
        $Account->user_id           = $data['package_id'];
        $Account->account_id        = $data['status'];
        return $Account;
    }

}
