<?php

namespace App\Repositories\Backend\Category;

use App\Models\Category\Category;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\Frontend\Auth\CategoryNeedsConfirmation;

/**
 * Class CategoryRepository
 * @package App\Repositories\Category
 */
class CategoryRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Category::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Category getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
            ->with('parent')
			->select([
				'categories.id',
				'categories.title',
				'categories.parent_id',
                'categories.level',
				'categories.status',
				'categories.created_at',
				'categories.updated_at',
				'categories.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the CategoryScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

	/**
	 * @param Model $input
	 */
	public function create($input)
    {
		$data = $input['data'];

        $category = $this->createCategoryStub($data);

		DB::transaction(function() use ($category, $data) {
			if (parent::save($category)) {
				return true;
			}

        	throw new GeneralException('Unknown error');
		});
    }

	/**
	 * @param Model $category
	 * @param array $input
	 */
	public function update(Model $category, array $input)
    {
    	$data = $input['data'];

        $data['slug'] = str_slug($data['title']);

        if($data['parent_id']>0)
            $data['level'] = 1;

		DB::transaction(function() use ($category, $data) {
			if (parent::update($category, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$category->status = isset($data['status']) ? 1 : 0;
				parent::save($category);

                /**
                 * UPDATE Service tags
                 */
                if (!$category->services->isEmpty()) {
                    foreach ($category->services AS $service) {
                        $service->fillTags($service);
                    }
                }


				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $category
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $category)
    {
        if (parent::delete($category)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $category
	 * @throws GeneralException
	 */
	public function forceDelete(Model $category)
    {
        if (is_null($category->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($category) {
			if (parent::forceDelete($category)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $category
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $category)
    {
        if (is_null($category->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($category))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $category
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $category, $status)
    {
        $category->status = $status;

        if (parent::save($category)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createCategoryStub($input)
    {
    	$category					 = self::MODEL;
        $category                    = new $category;
        $category->title             = $input['title'];
        $category->slug              = str_slug($input['title']);
        $category->parent_id         = $input['parent_id'] ? $input['parent_id'] : 0 ;
        #Set level to 1 if its a sub
        $category->level             = isset($input['parent_id']) && $input['parent_id'] ? 1 : 0;
        $category->status            = isset($input['status']) ? 1 : 0;
        return $category;
    }
}
