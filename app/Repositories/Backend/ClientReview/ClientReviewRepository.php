<?php

namespace App\Repositories\Backend\ClientReview;

use App\Events\Common\ClientReview\ClientReviewCreated;
use App\Models\ClientReview\ClientReview;
use App\Models\UserStat\UserStat;
use App\Repositories\Backend\UserStat\UserStatRepository;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientReviewRepository
 * @package App\Repositories\ClientReview
 */
class ClientReviewRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = ClientReview::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the ClientReview getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'users_reviews.id',
				'users_reviews.user_id',
				'users_reviews.status',
				'users_reviews.created_at',
				'users_reviews.updated_at',
				'users_reviews.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the ClientReviewScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($data)
    {
        $client_review = $this->createClientReviewStub($data);

        if ($client_review->save()) {

            #Update Users stats
            UserStatRepository::calc_stats($client_review->user_id);

            event(new ClientReviewCreated($client_review));

            return $client_review;
        }

        throw new GeneralException('Unknown error');
    }

    /**
     * @param Model $client_review
     * @param array $input
     */
	public function update(Model $client_review, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($client_review, $data) {
			if (parent::update($client_review, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$client_review->status = isset($data['status']) ? 1 : 0;
				parent::save($client_review);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $client_review
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $client_review)
    {
        if (access()->id() && access()->id() == $client_review->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if (parent::delete($client_review)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $client_review
	 * @throws GeneralException
	 */
	public function forceDelete(Model $client_review)
    {
        if (is_null($client_review->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($client_review) {
			if (parent::forceDelete($client_review)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $client_review
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $client_review)
    {
        if (is_null($client_review->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($client_review))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $client_review
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $client_review, $status)
    {
        if (access()->id() && access()->id() == $client_review->id && $status == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $client_review->status = $status;

        if (parent::save($client_review)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createClientReviewStub($input)
    {
    	$client_review					   = self::MODEL;
        $client_review                    = new $client_review;
        $client_review->quote_id          = $input['quote_id'];
        $client_review->project_id          = $input['project_id'];
        $client_review->professional_id   = $input['professional_id'];
        $client_review->user_id           = $input['user_id'];
        $client_review->comments           = $input['comments'];

        $client_review->rating_pay              = $input['rating_pay'];
        $client_review->rating_understanding    = $input['rating_understanding'];
        $client_review->rating_clear            = $input['rating_clear'];
        $client_review->rating_overall          = $input['rating_overall'];

        $client_review->recommended          = $input['recommended'];

        $client_review->status            = isset($input['status']) ? $input['status'] : 1;
        return $client_review;
    }

}
