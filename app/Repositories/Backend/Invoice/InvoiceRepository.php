<?php

namespace App\Repositories\Backend\Invoice;

use App\Events\Common\Invoice\InvoiceCreated;
use App\Models\Invoice\Invoice;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InvoiceRepository
 * @package App\Repositories\Invoice
 */
class InvoiceRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Invoice::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Invoice getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'invoices.id',
				'invoices.amount',
                'invoices.description',
                'invoices.user_id',
				'invoices.status',
				'invoices.created_at',
				'invoices.updated_at',
				'invoices.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		if ($status != 'null') {
            $items = $dataTableQuery->where('status', $status)->get();
        } else {
            $items = $dataTableQuery->get();
        }

		// active() is a scope on the InvoiceScope trait
		return $items;
    }

    public function create_from_wizard($input)
    {

        $data = $input['data'];

        $Invoice                    = new Invoice();
        $Invoice->service_id        = $data['service_id'];

        $Invoice = $this->createWizardInvoiceStub($data);

        if ($Invoice->save()) {

            event(new InvoiceCreated($Invoice));

            return $Invoice;
        }

        throw new GeneralException('Unknown error');
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($data)
    {

        $Invoice = $this->createInvoiceStub($data);

        if ($Invoice->save()) {

            event(new InvoiceCreated($Invoice));

            return $Invoice;
        }

        throw new GeneralException('Unknown error');
    }

	/**
	 * @param Model $Invoice
	 * @param array $input
	 */
	public function update(Model $Invoice, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($Invoice, $data) {
			if (parent::update($Invoice, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$Invoice->status = isset($data['status']) ? 1 : 0;
				parent::save($Invoice);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $Invoice
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $Invoice)
    {
        if (access()->id() && access()->id() == $Invoice->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if (parent::delete($Invoice)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $Invoice
	 * @throws GeneralException
	 */
	public function forceDelete(Model $Invoice)
    {
        if (is_null($Invoice->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($Invoice) {
			if (parent::forceDelete($Invoice)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $Invoice
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $Invoice)
    {
        if (is_null($Invoice->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($Invoice))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $Invoice
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $Invoice, $status)
    {
        if (access()->id() && access()->id() == $Invoice->id && $status == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $Invoice->status = $status;

        if (parent::save($Invoice)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createInvoiceStub($data)
    {
    	$Invoice					 = self::MODEL;
        $Invoice                    = new $Invoice;
        $Invoice->amount            = isset($data['own_amount']) && $data['own_amount'] ? $data['own_amount'] : $data['amount'];
        $Invoice->professional_id   = isset($data['professional_id']) ? $data['professional_id'] : 0;
        $Invoice->package_id        = isset($data['package_id']) ? $data['package_id'] : 0;
        $Invoice->credit_quantity   = isset($data['credit_quantity']) ? $data['credit_quantity'] : 0;
        $Invoice->user_id           = isset($data['user_id']) ? $data['user_id'] : 0;
        $Invoice->account_id        = isset($data['account_id']) ? $data['account_id'] : 0;
        $Invoice->description       = isset($data['description']) && $data['description'] ? $data['description'] : 'Credit Purchase' ;
        $Invoice->status            = isset($data['status']) && $data['status'] ? $data['status'] : 0;
        return $Invoice;
    }

}
