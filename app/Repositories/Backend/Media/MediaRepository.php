<?php

namespace App\Repositories\Backend\Media;

use App\Models\Media\Media;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MediaRepository
 * @package App\Repositories\Media
 */
class MediaRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Media::class;

    public function __construct()
    {

    }


    /**
     * @param $input
     * @return bool|static
     * @throws GeneralException
     */
	public function create($input)
    {
        #ensure Status = 1
        $input['status'] = 1;

        if ($media = Media::create($input)) {

            return $media;
        } else {
            throw new GeneralException('Media did not create');
        }

        return false;
    }

    /**
     * @param Model $media
     * @param array $input
     */
	public function update(Model $media, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($media, $data) {
			if (parent::update($media, $data)) {

				parent::save($media);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.media.update_error'));
		});
    }

    /**
     * @param Model $media
     * @return bool
     * @throws GeneralException
     */
	public function delete(Model $media)
    {
        if (parent::delete($media)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.media.delete_error'));
    }

}
