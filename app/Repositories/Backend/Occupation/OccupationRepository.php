<?php

namespace App\Repositories\Backend\Occupation;

use App\Models\Occupation\Occupation;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OccupationRepository
 * @package App\Repositories\Occupation
 */
class OccupationRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Occupation::class;

    public function __construct()
    {

    }

	/**
	 * @param Model $input
	 */
	public function create($input)
    {
        $data = $input['data'];
        $data['slug'] = str_slug($data['title']);

        $occupation = $this->createOccupationStub($data);

		return DB::transaction(function() use ($occupation, $data) {
			if (parent::save($occupation)) {

				return $occupation;
			}

        	throw new GeneralException('Unknown error');
		});
    }

	/**
	 * @param Model $occupation
	 * @param array $input
     * @return bool
	 */
	public function update(Model $occupation, array $input)
    {
    	$data = $input['data'];
        $data['slug'] = str_slug($data['title']);

		DB::transaction(function() use ($occupation, $data) {
			if (parent::update($occupation, $data)) {

                /**
                 * UPDATE Service tags
                 */
                if (!$occupation->services->isEmpty()) {
                    foreach ($occupation->services AS $service) {
                        $service->fillTags($service);
                    }
                }

				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.occupations.update_error'));
		});
    }

		/**
	 * @param Model $occupation
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $occupation)
    {
        if (parent::delete($occupation)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.occupations.delete_error'));
    }

	/**
	 * @param Model $occupation
	 * @throws GeneralException
     * @return bool
	 */
	public function forceDelete(Model $occupation)
    {
        if (is_null($occupation->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($occupation) {
			if (parent::forceDelete($occupation)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.occupations.delete_error'));
		});
    }

	/**
	 * @param Model $occupation
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $occupation)
    {
        if (is_null($occupation->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($occupation))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.occupations.restore_error'));
    }

	/**
	 * @param Model $occupation
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $occupation, $status)
    {
        if (parent::save($occupation)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.occupations.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createOccupationStub($input)
    {
    	$occupation					            = self::MODEL;
        $occupation                                = new $occupation;
        $occupation->title                         = $input['title'];
        $occupation->slug                          = str_slug($input['title']);
        return $occupation;
    }

}
