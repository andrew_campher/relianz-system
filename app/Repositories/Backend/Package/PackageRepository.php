<?php

namespace App\Repositories\Backend\Package;

use App\Models\Package\Package;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PackageRepository
 * @package App\Repositories\Package
 */
class PackageRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Package::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Package getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'packages.id',
				'packages.title',
                'packages.cost',
                'packages.per_credit',
				'packages.discount_percent',
                'packages.free',
				'packages.created_at',
				'packages.updated_at',
				'packages.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

        $items = $dataTableQuery->get();

		// active() is a scope on the PackageScope trait
		return $items;
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($data)
    {

        $Package = $this->createPackageStub($data);

        if ($Package->save()) {

            return $Package;
        }

        throw new GeneralException('Unknown error');
    }

	/**
	 * @param Model $Package
	 * @param array $input
	 */
	public function update(Model $Package, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($Package, $data) {
			if (parent::update($Package, $data)) {

				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $Package
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $Package)
    {
        if (parent::delete($Package)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $Package
	 * @throws GeneralException
	 */
	public function forceDelete(Model $Package)
    {
        if (is_null($Package->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($Package) {
			if (parent::forceDelete($Package)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $Package
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $Package)
    {
        if (is_null($Package->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($Package))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createPackageStub($data)
    {
    	$Package					= self::MODEL;
        $Package                    = new $Package;
        $Package->title             = $data['title'];
        $Package->cost              = $data['cost'];
        $Package->per_credit        = $data['per_credit'];
        $Package->free              = isset($data['free']) && $data['free'] ? $data['free'] : 0;
        $Package->discount_percent  = isset($data['discount_percent']) && $data['discount_percent'] ? $data['discount_percent'] : 0;
        return $Package;
    }

}
