<?php

namespace App\Repositories\Backend\Payment;

use App\Events\Common\Payment\PaymentCreated;
use App\Models\Payment\Payment;
use App\Repositories\Repository;
use App\Services\Account\Billing;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentRepository
 * @package App\Repositories\Payment
 */
class PaymentRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Payment::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Payment getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'payments.id',
				'payments.amount',
                'payments.description',
                'payments.user_id',
				'payments.status',
				'payments.created_at',
				'payments.updated_at',
				'payments.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		if ($status != 'null') {
            $items = $dataTableQuery->where('status', $status)->get();
        } else {
            $items = $dataTableQuery->get();
        }

		// active() is a scope on the PaymentScope trait
		return $items;
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($data)
    {

        $payment = $this->createPaymentStub($data);

        DB::transaction(function() use ($payment) {
            if (parent::save($payment)) {
                #process the Payment
                Billing::InvoiceSettled($payment);

                #record Event
                event(new PaymentCreated($payment));
                return true;
            }

            throw new GeneralException('Payment Received error');
        });

    }

	/**
	 * @param Model $payment
	 * @param array $input
	 */
	public function update(Model $payment, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($payment, $data) {
			if (parent::update($payment, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$payment->status = isset($data['status']) ? 1 : 0;
				parent::save($payment);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $payment
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $payment)
    {
        if (access()->id() && access()->id() == $payment->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if (parent::delete($payment)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $payment
	 * @throws GeneralException
	 */
	public function forceDelete(Model $payment)
    {
        if (is_null($payment->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($payment) {
			if (parent::forceDelete($payment)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $payment
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $payment)
    {
        if (is_null($payment->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($payment))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createPaymentStub($data)
    {
    	$payment					 = self::MODEL;
        $payment                    = new $payment;
        $payment->amount_paid       = $data['amount_paid'];
        $payment->credit_amount     = isset($data['credit_amount']) ? $data['credit_amount'] : 0;
        $payment->user_id           = $data['user_id'];
        $payment->invoice_id        = $data['invoice_id'];
        $payment->note              = isset($data['note']) ? $data['note'] : '';
        return $payment;
    }

}
