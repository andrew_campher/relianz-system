<?php

namespace App\Repositories\Backend\Professional;

use App\Events\Common\Professional\ProfessionalApproved;
use App\Events\Common\Professional\ProfessionalVerified;
use App\Models\Access\User\User;
use App\Models\Professional\Professional;
use App\Notifications\Professional\AdminNotifyNewProfessional;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

/**
 * Class ProfessionalRepository
 * @package App\Repositories\Professional
 */
class ProfessionalRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Professional::class;

    public function __construct()
    {

    }

    /**
     * @param int $approved
     * @param int $status
     * @param bool $trashed
     * @return mixed
     */
	public function getForDataTable($status = 1, $trashed = false, $approved = 1)
    {
        /**
         * Note: You must return deleted_at or the Professional getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'professionals.id',
				'professionals.title',
				'professionals.status',
                'professionals.logo',
                'professionals.approved',
				'professionals.created_at',
				'professionals.updated_at',
				'professionals.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the ProfessionalScope trait
		return $dataTableQuery->where('status', $status)->where('approved', $approved)->get();
    }

    /**
     * @param $input
     * @return bool|static
     * @throws GeneralException
     */
	public function create($input)
    {
        #ensure Status = 1
        $input['status'] = isset($input['status']) && $input['status']?$input['status']: 1;

        $input['branch']['distance_travelled'] = isset($input['branch']['distance_travelled'])?$input['branch']['distance_travelled']:20;
        $input['distance_travelled'] = $input['branch']['distance_travelled'];

        if (!isset($input['user_id']) || !$input['user_id'])
            $input['user_id'] = null;

        if (isset($input['branch']) && $input['branch']) {
            $input['lat'] = $input['branch']['lat'];
            $input['lng'] = $input['branch']['lng'];
            $input['area'] = $input['branch']['area'];
        }

        if ($professional = Professional::create($input)) {

            #Insert stats base ()
            $professional->stats()->create(['hires_count' => '0']);

            #Populate services
            $input['services'] = isset($input['services'])?$input['services']:array();

            #remove duplicates
            $input['services'] = array_unique($input['services']);
            $professional->services()->sync($input['services']);

            if (is_array($input['branch'])) {
                if (isset($input['branch']['branch_title']) && $input['branch']['branch_title']) {
                    $input['branch']['slug'] == str_slug($input['branch']['branch_title']);
                }



                $professional->branch()->create($input['branch']);
            }

            #Create Account entry (No Package)
            #TODO: have package chosen by registering User
            $professional->account()->create(['credits_remain'=>0, 'status'=> 0, 'package_id'=> 1]);

            #Notify Admins of new account
            if (!access()->id() || !access()->user()->hasRole('Administrator')) {
                $admins = User::IsAdmins()->get();
                Notification::send($admins, new AdminNotifyNewProfessional($professional, $input));
            }

            return $professional;
        } else {
            throw new GeneralException('Professional did not create');
        }

        return false;
    }

    /**
     * @param Model $professional
     * @param array $input
     */
	public function update(Model $professional, array $input)
    {
    	$data = $input['data'];

        if (!isset($data['user_id']) || !$data['user_id'])
            $data['user_id'] = null;

        $data['slug'] = str_slug($data['title']);

        if (isset($data['branch']) && $data['branch']) {
            $data['lat'] = $data['branch']['lat'];
            $data['lng'] = $data['branch']['lng'];
            $data['area'] = $data['branch']['area'];
            $data['distance_travelled'] = isset($data['branch']['distance_travelled'])?$data['branch']['distance_travelled']:20;
        }


		DB::transaction(function() use ($professional, $data) {
			if (parent::update($professional, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
                if (isset($data['status']))
				    $professional->status =  $data['status'];

                parent::save($professional);

                if (!$professional->stats)
                    $professional->stats()->create(['hires_count' => '0']);

                $data['services'] = isset($data['services'])?$data['services']:array();

                #remove duplicate values
                $data['services'] = array_unique($data['services']);
                $professional->services()->sync($data['services']);

                if (isset($data['branch']) && is_array($data['branch'])) {
                    if (isset($data['branch']['branch_title']) && $data['branch']['branch_title']) {
                        $data['branch']['slug'] == str_slug($data['branch']['branch_title']);
                    }

                    $data['branch']['distance_travelled'] = isset($data['branch']['distance_travelled']) && $data['branch']['distance_travelled']?$data['branch']['distance_travelled']:20;

                    $professional->branch()->update($data['branch']);
                }

				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.professional.update_error'));
		});
    }

    /**
     * @param Model $professional
     * @return bool
     * @throws GeneralException
     */
	public function delete(Model $professional)
    {
        if (parent::delete($professional)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.professional.delete_error'));
    }

/**
	public function forceDelete(Model $professional)
    {
        if (is_null($professional->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($professional) {
			if (parent::forceDelete($professional)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.professional.delete_error'));
		});
    }

	/**
	 * @param Model $professional
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $professional)
    {
        if (is_null($professional->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($professional))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.professional.restore_error'));
    }

	/**
	 * @param Model $professional
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $professional, $status)
    {

        $professional->status = $status;

        if (parent::save($professional)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.professional.mark_error'));
    }

    public function approve(Model $professional, $approved)
    {
        $professional->approved = $approved;

        #When approved then ensure status = 1
        $professional->status = 1;

        if (parent::save($professional)) {

            /**
             * Create Accounts record if none exist
             */
            if (!$professional->accounts) {
                $professional->account()->create(['credits_remain'=>0, 'status'=> 0, 'package_id'=> 1]);
            }

            /**
             * Assign any Free Credits
             */
            $free_credits = $professional->account->package->free_Credits;
            if ($free_credits) {
                DB::table('accounts')
                    ->where('id', $professional->account->id)
                    ->update(['credits_remain' => $free_credits]);
            }

            #Fire event
            event(new ProfessionalApproved($professional));

            return true;

        }

        throw new GeneralException(trans('exceptions.backend.professional.mark_error'));
    }

    /**
     * @param Model $professional
     * @param $verified
     * @return bool
     * @throws GeneralException
     */
    public function verify(Model $professional, $verified)
    {
        $professional->verify_status = $verified;

        if (parent::save($professional)) {

            #Fire event
            event(new ProfessionalVerified($professional));

            return true;

        }

        throw new GeneralException(trans('exceptions.backend.professional.mark_error'));
    }
}
