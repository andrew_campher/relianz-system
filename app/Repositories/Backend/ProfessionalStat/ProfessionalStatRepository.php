<?php

namespace App\Repositories\Backend\ProfessionalStat;

use App\Models\Professional\Professional;
use App\Models\ProfessionalStat\ProfessionalStat;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfessionalStatRepository
 * @package App\Repositories\ProfessionalStat
 */
class ProfessionalStatRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = ProfessionalStat::class;

    public function __construct()
    {

    }

    /**
     * @param $professional_id
     * @return bool
     */
    public static function calc_stats($professional_id) {

        $all_stats = DB::table('projects_reviews')
            ->select(
                DB::raw('count(*) as reviews_count'),
                DB::raw('SUM(recommended) as recommends_count'),
                DB::raw('AVG(rating_overall) as stat_overall'),
                DB::raw('AVG(rating_cooperative) as stat_cooperative'),
                DB::raw('AVG(rating_quality) as stat_quality'),
                DB::raw('AVG(rating_time) as stat_time'),
                DB::raw('AVG(rating_price) as stat_price'),
                DB::raw('SUM(project_success) as hires_success')
            )
            ->where('professional_id', $professional_id )
            ->where('status', 1 )
            ->groupBy('professional_id')
            ->get()->toArray();

        if ($all_stats) {
            $all_stats = json_decode(json_encode((array) $all_stats[0]), true);

            ProfessionalStat::where('professional_id', $professional_id)
                ->update($all_stats);

            DB::table('professionals')
                ->join('professionals_stats', 'professionals_stats.professional_id', 'professionals.id')
                ->where('professionals.id', $professional_id)
                ->update([
                    'recommended_tot' => DB::raw('professionals_stats.recommends_count'),
                    'rating_overall_tot' => DB::raw('professionals_stats.stat_overall'),
                    'rating_cooperative_tot' => DB::raw('professionals_stats.stat_cooperative'),
                    'rating_quality_tot' => DB::raw('professionals_stats.stat_quality'),
                    'rating_time_tot' => DB::raw('professionals_stats.stat_time'),
                    'rating_price_tot' => DB::raw('professionals_stats.stat_price'),
                    'successful_projects' => DB::raw('professionals_stats.hires_success'),
                ]);
        } else {
            return false;
        }
    }

}
