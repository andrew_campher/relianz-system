<?php

namespace App\Repositories\Backend\Project;

use App\Events\Common\Project\ProjectApproved;
use App\Events\Common\Project\ProjectClosed;
use App\Events\Common\Project\ProjectCreated;
use App\Events\Common\Project\ProjectFulfilled;
use App\Models\Project\Project;
use App\Repositories\Backend\Quote\QuoteRepository;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectRepository
 * @package App\Repositories\Project
 */
class ProjectRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Project::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Project getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'projects.id',
				'projects.user_id',
				'projects.title',
                'projects.service_id',
                'projects.description',
                'projects.service_id',
				'projects.created_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the ProjectScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

    public function create_from_wizard($input)
    {

        $data = $input['data'];

        $project                    = new Project();
        $project->service_id        = $data['service_id'];

        $project = $this->createWizardProjectStub($data);

        if ($project->save()) {

            event(new ProjectCreated($project));

            return $project;
        }

        throw new GeneralException('Unknown error');
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($input)
    {
		$data = $input['data'];

        $project = $this->createProjectStub($data);

        if ($project->save()) {

            event(new ProjectCreated($project));

            return $project;
        }

        throw new GeneralException('Unknown error');
    }

    /**
     * @param Model $project
     * @param array $input
     */
	public function update(Model $project, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($project, $data) {
			if (parent::update($project, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
                if (isset($data['status']))
                    $project->status =  $data['status'];

				parent::save($project);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

    /**
     * Method to mark project as closed
     * @param Model $project
     * @param $data3
     */
    public function status_closed(Model $project, $data)
    {
        DB::transaction(function() use ($project, $data) {
            if (parent::update($project, $data)) {

                event(new ProjectClosed($project, $data));

                #Unsuccessful the quotes
                $quotes = new QuoteRepository();
                if (!$project->quotes->isEmpty()) {
                    foreach ($project->quotes AS $quote)
                    {

                        $quotes->decline($quote, $data);
                    }

                }

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });
    }

    public function status_fulfilled(Model $project)
    {

        $data['status'] = 6;

        DB::transaction(function() use ($project, $data) {
            if (parent::update($project, $data)) {

                #Fulfill Project
                $project->status_reason = 'Project received the maximum allowed quotes.';

                event(new ProjectFulfilled($project));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });
    }



		/**
	 * @param Model $project
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $project)
    {

        if (parent::delete($project)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $project
	 * @throws GeneralException
	 */
	public function forceDelete(Model $project)
    {
        if (is_null($project->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($project) {
			if (parent::forceDelete($project)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $project
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $project)
    {
        if (is_null($project->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($project))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

    public function approve(Model $project)
    {
        #When approved then ensure status = 1
        $project->status = 1;
        $project->approved_at = Carbon::now();

        if (parent::save($project)) {

            #Fire event
            event(new ProjectApproved($project));

            return true;

        }

        throw new GeneralException(trans('exceptions.backend.project.mark_error'));
    }

    public function unapprove(Model $project)
    {
        #When approved then ensure status = 1
        $project->status = 0;
        $project->approved_at = null;

        if (parent::save($project)) {

            #Fire event
            #event(new ProjectUnApproved($project));

            return true;

        }

        throw new GeneralException(trans('exceptions.backend.project.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createProjectStub($input)
    {
    	$project					 = self::MODEL;
        $project                    = new $project;
        $project->user_id           = $input['user_id'];
        $project->title             = $input['title'];
        $project->project_id   = isset($input['project_id']) ? $input['project_id'] : 0;
        $project->service_id        = $input['service_id'];
        $project->description       = $input['description'];
        $project->status            = isset($input['status']) ? 1 : 0;
        return $project;
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createWizardProjectStub($input)
    {
        $project					 = self::MODEL;
        $project                    = new $project;
        $project->title             = $input['title'];
        $project->user_id           = $input['user_id'];
        $project->service_id        = isset($input['service_id']) && $input['service_id'] ? $input['service_id'] : 0;
        $project->description       = isset($input['description']) && $input['description']? $input['description'] :'';
        $project->status            = isset($input['status']) && $input['status'] ? $input['status'] : 0;

        $project->customer_mobile   = isset($input['customer_mobile']) ? $input['customer_mobile'] : null;
        $project->customer_email    = isset($input['customer_email']) ? $input['customer_email'] : null;
        $project->specific_date     = isset($input['specific_date']) && $input['specific_date']? $input['specific_date'] : null;
        $project->lat               = isset($input['lat']) && $input['lat']? $input['lat'] : null;
        $project->lng               = isset($input['lng']) && $input['lng'] ? $input['lng'] : null;
        $project->area              = isset($input['area']) && $input['area'] ? $input['area'] : null;
        $project->postcode          = isset($input['postcode']) && $input['postcode'] ? $input['postcode'] : null;
        $project->when_describe     = isset($input['when_describe']) && $input['when_describe']? $input['when_describe'] : null;
        $project->when              = isset($input['when']) && $input['when']? $input['when'] : null;

        return $project;
    }
}
