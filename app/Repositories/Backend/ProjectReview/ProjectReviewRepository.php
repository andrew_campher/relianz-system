<?php

namespace App\Repositories\Backend\ProjectReview;

use App\Events\Common\Project\ProjectRelist;
use App\Events\Common\ProjectReview\ProjectReviewCreated;
use App\Events\Common\Quote\QuoteStatusReviewed;
use App\Models\ProjectReview\ProjectReview;
use App\Repositories\Backend\ProfessionalStat\ProfessionalStatRepository;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectReviewRepository
 * @package App\Repositories\ProjectReview
 */
class ProjectReviewRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = ProjectReview::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the ProjectReview getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'projects.id',
				'projects.user_id',
				'projects.status',
				'projects.created_at',
				'projects.updated_at',
				'projects.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the ProjectReviewScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($data)
    {
        $project_review = $this->createProjectReviewStub($data);

        if ($project_review->save()) {

            return $project_review;
        }

        throw new GeneralException('Unknown error');
    }

    /**
     * @param Model $project_review
     * @param array $input
     */
	public function update(Model $project_review, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($project_review, $data) {
			if (parent::update($project_review, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$project_review->status = isset($data['status']) && $data['status'] ? $data['status'] : 0;
				parent::save($project_review);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $project_review
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $project_review)
    {
        if (access()->id() && access()->id() == $project_review->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if (parent::delete($project_review)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $project_review
	 * @throws GeneralException
	 */
	public function forceDelete(Model $project_review)
    {
        if (is_null($project_review->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($project_review) {
			if (parent::forceDelete($project_review)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $project_review
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $project_review)
    {
        if (is_null($project_review->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($project_review))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $project_review
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function approve(Model $project_review, $data)
    {
        if (access()->id() && access()->id() == $project_review->id && $data['approved'] == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $project_review->status = 1;



        /**
         * Calculate Professional Stats
         */
        ProfessionalStatRepository::calc_stats($project_review->professional_id);

        #IF has QUOTE
        if ($project_review->quote_id) {
            /**
             * Update quote status to Reviewed (5)
             */
            $project_review->quote->status = 5;
            $project_review->quote->save();
            event(new QuoteStatusReviewed($project_review->quote));

            /**
             * Update project status to Complete or Closed
             */
            if ($data['project_success']) {
                #if Successful mark as Complete
                $project_review->project->status = 3;
            } else {
                switch ($data['project_status']) {
                    case 1:
                        /*#Relist Project
                        $project_review->project->status = 1;
                        event(new ProjectRelist($project_review->project));*/
                        break;
                    case 3:
                        #Complete
                        $project_review->project->status = 3;
                        break;
                    default:
                        #Close
                        $project_review->project->status = 4;

                        break;
                }
            }
            $project_review->project->save();
        }


        /**
         * EVENTS
         */

        event(new ProjectReviewCreated($project_review));


        if (parent::save($project_review)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createProjectReviewStub($input)
    {
    	$project_review					   = self::MODEL;
        $project_review                    = new $project_review;
        $project_review->quote_id          = isset($input['quote_id']) && $input['quote_id']?$input['quote_id']:0;
        $project_review->project_id          = isset($input['project_id']) && $input['project_id']?$input['project_id']:0;
        $project_review->professional_id   = $input['professional_id'];
        $project_review->user_id           = $input['user_id'];
        $project_review->comments           = $input['comments'];
        $project_review->recommended        = $input['recommended'];

        $project_review->rating_price        = $input['rating_price'];
        $project_review->rating_time         = $input['rating_time'];
        $project_review->rating_cooperative  = $input['rating_cooperative'];
        $project_review->rating_quality      = $input['rating_quality'];

        $project_review->rating_overall      = $input['rating_overall'];

        $project_review->project_success      = $input['project_success'];

        $project_review->status            = isset($input['status']) ? $input['status'] : 0;

        $project_review->approved            = isset($input['approved']) ? $input['approved'] : 0;
        $project_review->verified            = isset($input['verified']) ? $input['verified'] : 0;
        return $project_review;
    }

}
