<?php

namespace App\Repositories\Backend\Question;

use App\Models\Question\Question;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\Frontend\Auth\QuestionNeedsConfirmation;

/**
 * Class QuestionRepository
 * @package App\Repositories\Question
 */
class QuestionRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Question::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Question getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'questions.id',
				'questions.title',
				'questions.description',
				'questions.status',
				'questions.created_at',
				'questions.updated_at',
				'questions.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the QuestionScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

	/**
	 * @param Model $input
	 */
	public function create($input)
    {
		$data = $input['data'];

        $question = $this->createQuestionsStub($data);

		DB::transaction(function() use ($question, $data) {
			if (parent::save($question)) {
                #Store Service relations
                if (isset($data['services']) && $data['services'])
                    $question->services()->sync($data['services']);

				return true;
			}

        	throw new GeneralException('Unknown error');
		});
    }

	/**
	 * @param Model $question
	 * @param array $input
	 */
	public function update(Model $question, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($question, $data) {
			if (parent::update($question, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$question->status = isset($data['status']) ? 1 : 0;
                #Store Service relations
                if (isset($data['services']) && $data['services'])
                    $question->services()->sync($data['services']);

				parent::save($question);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $question
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $question)
    {
         if (parent::delete($question)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $question
	 * @throws GeneralException
	 */
	public function forceDelete(Model $question)
    {
        if (is_null($question->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($question) {
			if (parent::forceDelete($question)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $question
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $question)
    {
        if (is_null($question->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($question))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $question
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $question, $status)
    {
        $question->status = $status;

        if (parent::save($question)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createQuestionsStub($input)
    {
    	$question					 = self::MODEL;
        $question                    = new $question;
        $question->title             = $input['title'];
        $question->description       = $input['description'];
        $question->global            = isset($input['global']) ? 1 : 0;
        $question->question_type     = $input['question_type'];
        $question->status            = isset($input['status']) ? 1 : 0;
        return $question;
    }
}
