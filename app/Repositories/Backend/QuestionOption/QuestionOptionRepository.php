<?php

namespace App\Repositories\Backend\QuestionOption;

use App\Models\QuestionOption\QuestionOption;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\Frontend\Auth\QuestionOptionNeedsConfirmation;

/**
 * Class QuestionRepository
 * @package App\Repositories\Question
 */
class QuestionOptionRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = QuestionOption::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Question getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'questions_options.id',
				'questions_options.title',
				'questions_options.description',
				'questions_options.status',
				'questions_options.created_at',
				'questions_options.updated_at',
				'questions_options.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the QuestionScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

	/**
	 * @param Model $input
	 */
	public function create($input)
    {
		$data = $input['data'];

        $option = $this->createQuestionOptionStub($data);

		DB::transaction(function() use ($option, $data) {
			if (parent::save($option)) {
				return true;
			}

        	throw new GeneralException('Unknown error');
		});
    }

	/**
	 * @param Model $option
	 * @param array $input
	 */
	public function update(Model $option, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($option, $data) {
			if (parent::update($option, $data)) {
				parent::save($option);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $option
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $option)
    {
         if (parent::delete($option)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $option
	 * @throws GeneralException
	 */
	public function forceDelete(Model $option)
    {
        if (is_null($option->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($option) {
			if (parent::forceDelete($option)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $option
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $option)
    {
        if (is_null($option->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($option))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $option
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $option, $status)
    {
        $option->status = $status;

        if (parent::save($option)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createQuestionOptionStub($input)
    {
    	$option					 = self::MODEL;
        $option                    = new $option;
        $option->title             = $input['title'];
        $option->set_value         = $input['set_value'];
        $option->description       = $input['description'];
        $option->question_id       = $input['question_id'];
        $option->special            = $input['special'];
        return $option;
    }
}
