<?php

namespace App\Repositories\Backend\Quote;

use App\Events\Common\Quote\QuoteConfirmHired;
use App\Events\Common\Quote\QuoteCreated;
use App\Events\Common\Quote\QuoteHired;
use App\Events\Common\Quote\QuoteUnsuccessful;
use App\Events\Common\Quote\QuoteUpdated;
use App\Models\Project\Project;
use App\Models\Quote\Quote;
use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class QuoteRepository
 * @package App\Repositories\Quote
 */
class QuoteRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Quote::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Quote getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'quotes.id',
				'quotes.title',
                'quotes.pro_title',
                'quotes.image',
				'quotes.description',
				'quotes.status',
				'quotes.created_at',
				'quotes.updated_at',
				'quotes.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the QuoteScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

	/**
	 * @param Model $input
	 */
	public function create($input)
    {
        $data = $input['data'];

        $quote = $this->createQuoteStub($data);

        if ($quote->save()) {
            #ONly Fire off NewQuote if its been status = 1 (ie. it passed our credit checks)
            if ($quote->status == 1) {
                #Update Quote count on project
                Project::where('id', $data['project_id'])->update([
                    'quote_count'      => DB::raw('quote_count + 1')
                ]);


                event(new QuoteCreated($quote));
            }

            return $quote;

        }

        throw new GeneralException('Unknown error');
    }

	/**
	 * @param Model $quote
	 * @param array $input
     * @return bool
	 */
	public function update(Model $quote, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($quote, $data) {
			if (parent::update($quote, $data)) {

			    #Fire new Quote event if its now been Activated
			    if ($quote->status == 0 && isset($data['status']) && $data['status'] == 1) {
                    #Update Quote count on project
                    Project::where('id', $data['project_id'])->update([
                        'quote_count'      => DB::raw('quote_count + 1')
                    ]);

                    #Record Event
                    event(new QuoteCreated($quote));
                }

                event(new QuoteUpdated($quote));

				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.quotes.update_error'));
		});
    }

    public function decline(Model $quote, array $data)
    {

        #set status to Unsuccessful
        $data['status'] = 4;

        DB::transaction(function() use ($quote, $data) {
            if (parent::update($quote, $data)) {

                event(new QuoteUnsuccessful($quote, $data));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.quotes.update_error'));
        });
    }



		/**
	 * @param Model $quote
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $quote)
    {
        if (parent::delete($quote)) {
            #Update Quote count on project
            Project::where('id', $quote->project_id)->update([
                'quote_count'      => DB::raw('quote_count - 1')
            ]);

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.quotes.delete_error'));
    }

	/**
	 * @param Model $quote
	 * @throws GeneralException
     * @return bool
	 */
	public function forceDelete(Model $quote)
    {
        if (is_null($quote->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($quote) {
			if (parent::forceDelete($quote)) {

                Project::where('id', $quote->project_id)->update([
                    'quote_count'      => DB::raw('quote_count - 1')
                ]);

				return true;
			}

			throw new GeneralException(trans('exceptions.backend.quotes.delete_error'));
		});
    }

	/**
	 * @param Model $quote
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $quote)
    {
        if (is_null($quote->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($quote))) {

            Project::where('id', $quote->project_id)->update([
                'quote_count'      => DB::raw('quote_count + 1')
            ]);

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.quotes.restore_error'));
    }

    /**
     * Method to record that  professional has marked the quote has hired - Client needs to confirm this.
     * @param Model $quote
     * @return bool
     */
    public function verify_hired(Model $quote)
    {
        #PROFESSIONAL has marked quote has hired -
        #Clients needs to verify
        $quote->status = 7;
        $quote->verify_hired = Carbon::now();
        $quote->save();

        event(new QuoteConfirmHired($quote));

        return true;
    }


    /**
     * Mark as hired - Update all the different statuses
     * @param Model $quote
     * @return bool
     */
    public function hired(Model $quote)
    {
        #GEt project Model
        $project = Project::find($quote->project_id);

        /**
         * Mark all other quotes Unsuccessful
         */
        $unsuccessful_quotes = $project->quotes()->where('id','!=', $quote->id)->get();
        if ($unsuccessful_quotes) {
            #update to UNsuccessful status
            $project->quotes()->where('id','!=', $quote->id)->update(['status' => 4]);
            foreach ($unsuccessful_quotes AS $unsuccessful_quote) {
                #Record Event
                event(new QuoteUnsuccessful($unsuccessful_quote));
            }
        }

        /**
         * Update Quote hired date + Status = 3 (Successful)
         */
        $quote->hired = Carbon::now();
        #3 = hired
        $quote->status = 3;
        $quote->save();

        /**
         * Update Project status to 2 (Hired)
         */
        $quote->project->update(['status'=>2, 'professional_id' => $quote->professional_id]);

        event(new QuoteHired($quote));

        #Fire Event

        return true;
    }

	/**
	 * @param Model $quote
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $quote, $status)
    {
        $quote->status = $status;

        if (parent::save($quote)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.quotes.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createQuoteStub($input)
    {
    	$quote					            = self::MODEL;
        $quote                                = new $quote;
        $quote->price                         = $input['price'];
        $quote->price_type                    = $input['price_type'];
        $quote->description                   = $input['description'];
        $quote->need_more_info                = isset($input['need_more_info']) ? $input['need_more_info'] : 0;
        $quote->user_id                       = $input['user_id'];
        $quote->service_id                    = $input['service_id'];
        $quote->professional_id               = $input['professional_id'];
        $quote->project_id                    = $input['project_id'];
        #Ensure quote is DRAFT if not given 1 for status
        $quote->status                        = isset($input['status']) ? $input['status'] : 0;
        return $quote;
    }

}
