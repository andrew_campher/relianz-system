<?php

namespace App\Repositories\Backend\Service;

use App\Models\Service\Service;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceRepository
 * @package App\Repositories\Service
 */
class ServiceRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Service::class;

    public function __construct()
    {

    }

	/**
	 * @param int $status
	 * @param bool $trashed
	 * @return mixed
	 */
	public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the Service getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
		$dataTableQuery = $this->query()
			->select([
				'services.id',
				'services.title',
                'services.pro_title',
                'services.fee',
                'services.image',
				'services.description',
				'services.status',
				'services.created_at',
				'services.updated_at',
				'services.deleted_at',
			]);

		if ($trashed == "true") {
			return $dataTableQuery->onlyTrashed()->get();
		}

		// active() is a scope on the ServiceScope trait
		return $dataTableQuery->where('status', $status)->get();
    }

	/**
	 * @param Model $input
	 */
	public function create($input)
    {
        $data = $input['data'];

        $service = $this->createServiceStub($data);

		DB::transaction(function() use ($service, $data) {
			if (parent::save($service)) {

			    #categorories
                $service->categories()->sync($data['categories']);

				return true;
			}

        	throw new GeneralException('Unknown error');
		});
    }

	/**
	 * @param Model $service
	 * @param array $input
     * @return bool
	 */
	public function update(Model $service, array $input)
    {
    	$data = $input['data'];
        $service->status = isset($data['status']) ? 1 : 0;

        if (!$service->tags && !$data['tags']) {
            $data['tags'] = $service->title.' '.$service->pro_title.' '.$service->description;
        }

		DB::transaction(function() use ($service, $data) {
			if (parent::update($service, $data)) {

                #categorories
                $service->categories()->sync($data['categories']);

				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.services.update_error'));
		});
    }

		/**
	 * @param Model $service
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $service)
    {
        if (parent::delete($service)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.services.delete_error'));
    }

	/**
	 * @param Model $service
	 * @throws GeneralException
     * @return bool
	 */
	public function forceDelete(Model $service)
    {
        if (is_null($service->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($service) {
			if (parent::forceDelete($service)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.services.delete_error'));
		});
    }

	/**
	 * @param Model $service
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $service)
    {
        if (is_null($service->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($service))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.services.restore_error'));
    }

	/**
	 * @param Model $service
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $service, $status)
    {
        $service->status = $status;

        if (parent::save($service)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.services.mark_error'));
    }

    /**
     * @param  $input
     * @return mixed
     */
    protected function createServiceStub($input)
    {
    	$service					            = self::MODEL;
        $service                                = new $service;
        $service->title                         = $input['title'];
        $service->slug                          = str_slug($input['title']);
        $service->pro_title                     = $input['pro_title'];
        $service->pro_slug                      = str_slug(str_plural($input['pro_title']));
        $service->fee                           = isset($input['fee']) ? $input['fee'] : null;
        $service->tags                           = isset($input['tags']) && $input['tags'] ? $input['tags'] : $input['title'].' '.$input['pro_title'].' '.$input['description'];
        $service->description                   = $input['description'];
        $service->image                         = isset($input['image']) ? $input['image'] : null;
        $service->status                        = isset($input['status']) ? 1 : 0;
        return $service;
    }

}
