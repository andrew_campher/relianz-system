<?php

namespace App\Repositories\Backend\UserStat;

use App\Models\Access\User\User;
use App\Models\Project\Project;
use App\Models\Quote\Quote;
use App\Models\UserStat\UserStat;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserStatRepository
 * @package App\Repositories\UserStat
 */
class UserStatRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = UserStat::class;

    public function __construct()
    {

    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function calc_stats($user_id) {

        $all_stats = DB::table('users_reviews')
            ->select(
                DB::raw('count(*) as reviews_count'),
                DB::raw('SUM(recommended) as recommends_count'),
                DB::raw('AVG(rating_pay) as stat_pay'),
                DB::raw('AVG(rating_understanding) as stat_understanding'),
                DB::raw('AVG(rating_clear) as stat_clear'),
                DB::raw('AVG(rating_overall) as rating_overall'),

                DB::raw('SUM(rating_pay) as sum_rating_pay'),
                DB::raw('SUM(rating_understanding) as sum_rating_understanding'),
                DB::raw('SUM(rating_clear) as sum_stat_clear')
            )
            ->where('user_id', $user_id )
            ->where('status', 1 )
            ->groupBy('user_id')
            ->get()->toArray();

        if ($all_stats) {
            $all_stats = json_decode(json_encode((array) $all_stats[0]), true);

            $update_fields = [
                'reviews_count'     => $all_stats['reviews_count'],
                'stat_pay'          => $all_stats['stat_pay'],
                'stat_understanding' => $all_stats['stat_understanding'],
                'stat_clear'        => $all_stats['stat_clear'],
                'recommends_count'  => $all_stats['recommends_count'],
            ];

            UserStat::where('user_id', $user_id)
                ->update($update_fields);

            #Update the overall rating total on Users table
            User::where('id', $user_id)
                ->update(['rating_overall_tot' => $all_stats['rating_overall']]);
        } else {
            return false;
        }
    }

    public static function calc_hires($user_id) {

        $all_stats = DB::table('quotes')
            ->join('projects', 'projects.id', '=', 'quotes.user_id');

        if ($all_stats) {
            UserStat::where('id', $user_id)
                ->update($all_stats);
        } else {
            return false;
        }
    }


}
