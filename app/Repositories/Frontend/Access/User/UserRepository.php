<?php

namespace App\Repositories\Frontend\Access\User;

use App\Models\Access\User\User;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Hash;
use App\Models\Access\User\SocialLogin;
use App\Events\Frontend\Auth\UserConfirmed;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

/**
 * Class UserRepository
 * @package App\Repositories\Frontend\User
 */
class UserRepository extends Repository
{
	/**
	 * Associated Repository Model
	 */
	const MODEL = User::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param $email
     * @return bool
     */
    public function findByEmail($email) {
        return $this->query()->where('email', $email)->first();
    }

    /**
     * @param $token
     * @return mixed
     * @throws GeneralException
     */
    public function findByToken($token) {
        return $this->query()->where('confirmation_code', $token)->first();
    }

	/**
	 * @param $token
	 * @return mixed
	 * @throws GeneralException
	 */
	public function getEmailForPasswordToken($token) {
		if ($row = DB::table('password_resets')->where('token', $token)->first())
			return $row->email;
		throw new GeneralException(trans('auth.unknown'));
	}

    /**
     * @param array $data
     * @param bool $provider
     * @return static
     */
    #TODO: move this all to sinlge repository
    public function create(array $data, $provider = false)
    {
    	$user = self::MODEL;
    	$user = new $user;
		$user->first_name   = $data['first_name'];
        $user->last_name    = $data['last_name'];
        $user->api_token    = str_random(60);
        $user->name         = $user->name;
		$user->email    = $data['email'];

        $user->image = isset($data['image']) ? $data['image'] : null;

        $user->mobile            = isset($input['mobile']) ? $input['mobile'] : null;
		$user->confirmation_code = md5(uniqid(mt_rand(), true));
		$user->status = 1;
		$user->password = $provider ? null : bcrypt($data['password']);
		$user->confirmed = $provider || (isset($data['confirmed']) &&  $data['confirmed'])? 1 : 0;
        $user->approved = isset($data['role_id']) && $data['role_id'] ? 0 : 1;

        if (isset($data['lat']) && isset($data['lng'])) {
            $user->lat = isset($data['lat']) ? $data['lat'] : 'null';
            $user->lng = isset($data['lng']) ? $data['lng'] : 'null';
        }

        if (isset($data['area']) && isset($data['area'])) {
            $user->area = isset($data['area']) ? $data['area'] : 'null';
        }

		DB::transaction(function() use ($user, $data) {
			if (parent::save($user, $data)) {

                #Populate relation tables
                $user->stats()->create(['hires_count' => '0']);

				/**
				 * Add the default site role to the new user
				 */
				if (isset($data['role_id']) && $data['role_id']) {
                    $user->attachRole($data['role_id']);
                } else {
                    $user->attachRole($this->role->getDefaultUserRole());
                }


			}
		});

		/**
		 * If users have to confirm their email and this is not a social account,
		 * send the confirmation email
		 *
		 * If this is a social account they are confirmed through the social provider by default
		 */
		if (!$user->confirmed) {
			$user->notify(new UserNeedsConfirmation($user->confirmation_code));
		}

		/**
		 * Return the user object
		 */
		return $user;
    }


    /**
     * @param Model $user
     * @param array $input
     */
    public function update_confirm(Model $user, array $input)
    {
        $data = $input['data'];

        $this->checkUserByEmail($data, $user);

        $data['password']   = bcrypt($data['password']);
        $data['name']       = $user->name;
        $data['api_token']       = str_random(60);
        $data['confirmation_code']       = md5(uniqid(mt_rand(), true));

        DB::transaction(function() use ($user, $data) {
            if (parent::update($user, $data)) {
                //For whatever reason this just wont work in the above call, so a second is needed for now
                $user->status = 1;
                $user->name       = $user->name;
                $user->confirmed = 1;
                parent::save($user);

                event(new UserUpdated($user));

                return $user;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });
    }

    /**
     * @param array $data
     * @param bool $provider
     * @return static
     */
    public function create_professional_user(array $data, $provider = false)
    {
        $user = self::MODEL;
        $user = new $user;
        $user->first_name   = $data['first_name'];
        $user->last_name    = $data['last_name'];
        $user->name         = $user->name;
        $user->email    = $data['email'];

        $user->image = isset($data['image']) ? $data['image'] : null;

        $user->api_token    = str_random(60);
        $user->mobile            = isset($input['mobile']) ? $input['mobile'] : null;
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->status = 1;
        $user->password = $provider ? null : bcrypt($data['password']);
        $user->confirmed = $provider ? 1 : 0;
        $user->approved = 0;

        return DB::transaction(function() use ($user, $provider) {
            if (parent::save($user)) {

                #Populate relation tables
                $user->stats()->create(['hires_count' => '0']);
                /**
                 * Add Professional Role to user
                 */
                $user->attachRole($this->role->find(2));

                /**
                 * If users have to confirm their email and this is not a social account,
                 * send the confirmation email
                 *
                 * If this is a social account they are confirmed through the social provider by default
                 */
                if ($provider === false) {
                    $user->notify(new UserNeedsConfirmation($user->confirmation_code));
                }

                /**
                 * Return the user object
                 */
                return $user;
            }

            throw new GeneralException('New user creation failed');
        });

    }

	/**
	 * @param $data
	 * @param $provider
	 * @return UserRepository|bool
	 */
	public function findOrCreateSocial($data, $provider)
    {
        /**
         * User email may not provided.
         */
        $user_email = $data->email ? : "{$data->id}@{$provider}.com";

        /**
         * Check to see if there is a user with this email first
         */
        $user = $this->findByEmail($user_email);

        /**
         * If the user does not exist create them
         * The true flag indicate that it is a social account
         * Which triggers the script to use some default values in the create method
         */
        if (! $user) {

            $user = $this->create([
                'first_name'  => $data->user['first_name'],
                'last_name'  => $data->user['last_name'],
                'email' => $user_email,
            ], true);
        }

        /**
         * See if the user has logged in with this social account before
         */
        if (! $user->hasProvider($provider)) {
            /**
             * Gather the provider data for saving and associate it with the user
             */
            $user->providers()->save(new SocialLogin([
                'provider'    => $provider,
                'provider_id' => $data->id,
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]));
        } else {
            /**
             * Update the users information, token and avatar can be updated.
             */
            $user->providers()->update([
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]);
        }

        /**
         * Return the user object
         */
        return $user;
    }

    protected function checkUserByEmail($input, $user)
    {
        //Figure out if email is not the same
        if ($user->email != $input['email']) {
            //Check to see if email exists
            if ($this->query()->where('email', '=', $input['email'])->first()) {
                return false;
            }
        }
    }

    /**
     * @param $token
     * @return bool
     * @throws GeneralException
     */
    public function confirmAccount($token)
    {
        $user = $this->findByToken($token);
        if ($user) {

            if ($user->confirmed == 1) {
                return false;
            }

            if ($user->confirmation_code == $token) {
                $user->confirmed = 1;

                event(new UserConfirmed($user));
                return parent::save($user);
            }
        }

        throw new GeneralException(trans('exceptions.frontend.auth.confirmation.mismatch'));
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function updateProfile($id, $input)
    {
        $user = parent::find($id);
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        #Prob not the best way todo this - but using the setNameAtribute to combine.
        $user->name         = $user->name;
        if (isset($input['image']))
        $user->image = $input['image'];

        if ($user->canChangeEmail()) {
            //Address is not current address
            if ($user->email != $input['email']) {
                //Emails have to be unique
                if ($this->findByEmail($input['email'])) {
                    throw new GeneralException(trans('exceptions.frontend.auth.email_taken'));
                }

                $user->email = $input['email'];
                #Update confirmed to 0
                $user->confirmation_code = md5(uniqid(mt_rand(), true));
                $user->confirmed = 0;

                $user->notify(new UserNeedsConfirmation($user->confirmation_code));
            }
        }

        return parent::save($user);
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function changePassword($input)
    {
        $user = parent::find(access()->id());

        if (Hash::check($input['old_password'], $user->password)) {
            $user->password = bcrypt($input['password']);
            return parent::save($user);
        }

        throw new GeneralException(trans('exceptions.frontend.auth.password.change_mismatch'));
    }
}