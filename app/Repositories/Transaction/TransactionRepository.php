<?php

namespace App\Repositories\Backend\Transaction;

use App\Models\Transaction\Transaction;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TransactionRepository
 * @package App\Repositories\Transaction
 */
class TransactionRepository extends Repository
{

    /**
     * Associated Repository Model
     */
    const MODEL = Transaction::class;

    public function __construct()
    {

    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
	public function create($data)
    {

        $Transaction = $this->createTransactionStub($data);

        if ($Transaction->save()) {

            event(new TransactionCreated($Transaction));

            return $Transaction;
        }

        throw new GeneralException('Unknown error');
    }

    /**
     * @param Model $Transaction
     * @param array $input
     */
	public function update(Model $Transaction, array $input)
    {
    	$data = $input['data'];

		DB::transaction(function() use ($Transaction, $data) {
			if (parent::update($Transaction, $data)) {
				//For whatever reason this just wont work in the above call, so a second is needed for now
				$Transaction->status = isset($data['status']) ? 1 : 0;
				parent::save($Transaction);
				return true;
			}

        	throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
		});
    }

		/**
	 * @param Model $Transaction
	 * @return bool
	 * @throws GeneralException
	 */
	public function delete(Model $Transaction)
    {
        if (access()->id() && access()->id() == $Transaction->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if (parent::delete($Transaction)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

	/**
	 * @param Model $Transaction
	 * @throws GeneralException
	 */
	public function forceDelete(Model $Transaction)
    {
        if (is_null($Transaction->deleted_at)) {
            throw new GeneralException("This item must be deleted first before it can be destroyed permanently.");
        }

		DB::transaction(function() use ($Transaction) {
			if (parent::forceDelete($Transaction)) {
				return true;
			}

			throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
		});
    }

	/**
	 * @param Model $Transaction
	 * @return bool
	 * @throws GeneralException
	 */
	public function restore(Model $Transaction)
    {
        if (is_null($Transaction->deleted_at)) {
            throw new GeneralException("This item is not deleted so it can not be restored.");
        }

        if (parent::restore(($Transaction))) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

	/**
	 * @param Model $Transaction
	 * @param $status
	 * @return bool
	 * @throws GeneralException
	 */
	public function mark(Model $Transaction, $status)
    {
        if (access()->id() && access()->id() == $Transaction->id && $status == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $Transaction->status = $status;

        if (parent::save($Transaction)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function createTransactionStub($data)
    {
    	$Transaction					= self::MODEL;
        $Transaction                    = new $Transaction;
        $Transaction->account_id        = $data['account_id'];
        $Transaction->refunded        = $data['refunded'];
        $Transaction->user_id           = $data['user_id'];
        $Transaction->cost              = $data['cost'];
        $Transaction->modelowner_type   = $data['modelowner_type'];
        $Transaction->modelowner_id     = $data['modelowner_id'];
        $Transaction->servicetable_type = $data['servicetable_type'];
        $Transaction->servicetable_id   = $data['servicetable_id'];
        return $Transaction;
    }

}
