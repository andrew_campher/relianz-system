<?php

namespace App\Services\Account;
use App\Events\Common\Invoice\InvoiceSettled;
use App\Events\Common\Transaction\TransactionRefunded;
use App\Models\Payment\Payment;
use App\Exceptions\GeneralException;
use App\Models\Service\Service;
use App\Models\Transaction\Transaction;
use Illuminate\Support\Facades\DB;

/**
 * Class Billing
 * @package App\Services\Billing
 */
class Billing
{
    /**
     * Billing constructor.
     */
    public function __construct()
    {

    }

    /**
     * Get the currently authenticated user or null.
     */
    public static function user()
    {
        return auth()->user();
    }

    /**
     * Get the currently authenticated user's id
     * @return mixed
     */
    public static function id()
    {
        return auth()->id();
    }


    public static function InvoiceSettled(Payment $payment)
    {
        #Ensure invoice covers invoice
        if ($payment->amount_paid == $payment->invoice->amount) {

            #Mark invoice as paid
            $payment->invoice->update(['status'=>1]);

            #Add Credits to Professional: 'hits'      => DB::raw('hits + 1')
            $payment->invoice->account->update(['credits_remain' => DB::raw('credits_remain + '.$payment->invoice->credit_quantity)]);

            #record Invoice Settled event
            event(new InvoiceSettled($payment->invoice));

            return true;
        }

        throw new GeneralException('Payment amount('.$payment->amount_paid.') != Invoice Amount ('.$payment->invoice->amount.')');
    }

    public static function recordTransaction($model) {
        if (!config('app.settings.enable_credits'))
            return true;

        $credit_cost = $model->service->fee;

        /**
         * FREE PACKAGE CHECK
         */
        if (self::user()->professional->account->package->free) {

            $credit_cost = 0;

        } else {

            #Subtract the credits from Professionals Total
            self::user()->professional->account->update([
                'credits_remain'      => DB::raw('credits_remain - '.$model->service->fee)
            ]);
        }



        #Record transaction
        $model->transactions()->create([
            'servicetable_id' => $model->service_id,
            'servicetable_type' => Service::class,
            'cost' => $credit_cost,
            'user_id' => auth()->id(),
            'account_id' => auth()->user()->professional->account->id
        ]);

        return true;
    }

    public static function refundTransaction($transaction_id) {

        $transaction = Transaction::find($transaction_id);

        $transaction->user->account->update([
            'credits_remain'      => DB::raw('credits_remain + '.$transaction->cost)
        ]);

        #Record Event
        event(new TransactionRefunded($transaction));

        $transaction->refunded = 1;

        return true;
    }


}