<?php

namespace App\Services\Images;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

/**
 * Class Access
 * @package App\Services\Access
 */
class Images
{
    /**
     * Laravel application
     *
     * @var \Illuminate\Foundation\Application
     */
    public $app;

    /**
     * Create a new confide instance.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }


    /**
     * Uploads image - requires the input fieldname of the file and the component name (subdirectory)
     *
     * @param $request
     * @param $params
     * @return bool|string
     */
    public static function upload($request, $params) {
        #Ensure fieldname is give [0]
        if (!isset($params[0]) || !$params[0])
            return false;

        $fit = false;
        if (isset($params['fit']) && $params['fit']) {
            $fit = true;
        }

        #Default subdirectory key [1]
        $model_table = isset($params[1]) && $params[1] ? $params[1] : 'general';

        $return_img_names = array();

        if (is_array($params[0])) {
            foreach ($params[0] AS $file) {
                ##Handle IMAGE upload
                $image = $request->file($file);
                if ($image)
                    $return_img_names[$file] = self::saveImages($image, $model_table, $fit);
            }

        } else {
            ##Handle IMAGE upload
            $image = $request->file($params[0]);
            if ($image)
                $return_img_names[$params[0]] = self::saveImages($image, $model_table, $fit);
        }

        return $return_img_names;

    }

    /**
     * Handle the Resizing of thumbnails for each image
     * @param $image
     * @param $model_table
     * @return bool|string
     *
     */
    public static function saveImages($image, $model_table, $fit = false) {

        $imagename = time().'.'.$image->getClientOriginalExtension();

        #Create Directory
        #TODO: values to config
        $image_path = public_path('/images/'.$model_table.'/');
        if (!$result = is_dir($image_path))
        {
            $result = File::makeDirectory($image_path . '/thumbnail', 0777, true);
        }

        $method_to_use = 'resize';
        if ($fit) {
            $method_to_use = 'fit';
        }

        #If directory then lets proceed.
        if ($result) {

            #MAIN IMAGE
            $img = Image::make($image->getRealPath());
            $img->orientate();
            $destinationPath = public_path('/images/'.$model_table.'/');

            $img->$method_to_use(config('app.images.max_width'), config('app.images.max_height'), function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($destinationPath . '/' .$imagename, 80);

            #THUMBNAIL
            $destinationPath = public_path('/images/'.$model_table.'/thumbnail');
            $img = Image::make($image->getRealPath());

            $img->$method_to_use(config('app.images.thumb_width'), config('app.images.thumb_height'), function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($destinationPath . '/' . $imagename);

            ##ICON
            $destinationPath = public_path('/images/'.$model_table.'/thumbnail');

            $img->$method_to_use(config('app.images.icon_width'), config('app.images.icon_height'), function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($destinationPath . '/' .'icon_'. $imagename);



            #Set image input to new saved filename
            return $imagename;
        }

        return false;
    }

    /**
     * @param $data
     * @param $model_obj
     * @param string $image_attr
     * @return bool
     */
    public static function saveCrop($data, $model_obj, $image_attr='image') {

        #Build path
        $image_dir = public_path('/images/'.$model_obj->getTable().'/');
        $main_Image = $image_dir.$model_obj->{$image_attr};
        $thumbimage = $image_dir.'thumbnail/'.$model_obj->{$image_attr};

        if (!$data['img_width'] || !$data['img_height'])
            return false;

        $data['img_x'] = isset($data['img_x']) ? ceil($data['img_x']) : 0 ;
        $data['img_y'] = isset($data['img_y']) ? ceil($data['img_y']) : 0 ;

        if (file_exists($main_Image)) {

            $background = Image::canvas($data['img_width'], $data['img_height']);
            $background->fill('#fff');

            $img = Image::make($main_Image);

            $background->insert($img, 'top-right', -intval($data['img_x']), -intval($data['img_y']));

            $img->crop(intval($data['img_width']), intval($data['img_height']), $data['img_x'], $data['img_y'])
                ->resize(config('app.images.thumb_height'), config('app.images.thumb_width'), function ($constraint) {
                $constraint->aspectRatio();
            })->trim()->save($thumbimage);
        }

        return false;
    }


}