<?php

namespace App\Services\MediaManager;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\File;
use App\Models\Media\Media;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser as MimeGuesser;

/**
 * Class Media
 * @package App\Services\Media
 */
class MediaManager
{
    /**
     * Laravel application
     *
     * @var \Illuminate\Foundation\Application
     */
    public $app;

    /**
     * Create a new confide instance.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @param $request
     * @param $params
     * @return bool
     * @throws GeneralException
     */
    public static function uploadfiles($request, $params) {
        #Ensure fieldname is give [0]
        if (!isset($request) || !$request) {
            throw new GeneralException('Request empty');
            return false;
        }

        if (!isset($params[0]) || !$params[0]) {
            throw new GeneralException('Model empty');
            return false;
        } else {
            $modelname_type = strtolower((substr($params[0], strrpos($params[0], '\\') + 1)));
        }

        if (!isset($params[1]) || !$params[1]) {
            throw new GeneralException('Item id not provided');
            return false;
        }

        $input_field = 'file';

        #$param[3] = the group name
        $group = 'general';
        if (isset($params[2]) && $params[2]) {
            $group = strtolower($params[2]);

            $input_field = 'file_'.$group;
        }

        $files = $request->file($input_field);

        if (!isset($files) || !$files) {
            return false;
        }

        #Loop through and upload
        $guesser = new MimeGuesser();
        foreach ($files AS $key => $file) {

            if(!$file) {
                continue;
            }

            if (!$file_type = $guesser->guess($file->getMimeType())) {
                continue;
            }

            ##Upload sent file
            if ($filename = self::upload($file, $group, $modelname_type, $params[1])) {
                #insert into Files table
                $media = new Media();
                $media->filename = $filename;
                $media->file_type = $file_type;
                $media->user_id = isset($params['user_id']) && $params['user_id'] ? $params['user_id'] : access()->id();
                $media->group = $group;
                $media->orig_filename = $file->getClientOriginalName();

                $media->modelname_id = $params[1];
                $media->modelname_type = strtolower($modelname_type);

                $title_field = $input_field.'_title';
                $details_field = $input_field.'_details';

                $title_field_arr = $request->$title_field;
                $details_field_arr = $request->$details_field;

                if (isset($title_field_arr[$key]) && $title_field_arr[$key]) {
                    $media->title = $title_field_arr[$key];
                }

                if (isset($details_field_arr[$key]) && $details_field_arr[$key]) {
                    $media->details = $details_field_arr[$key];
                }

                $media->save();

            }
        }

    }

    public static function upload($file, $group, $modelname, $item_id) {

        $img_arr = ['jpg', 'gif', 'png', 'jpeg'];

        $Dir = '/files/'.$modelname.'/'.$item_id.'/'.$group.'/';

        #Check Directory exists
        $image_path = public_path($Dir);
        if (!$result = is_dir($image_path))
        {
            $result = File::makeDirectory($image_path, 0777, true);
        }

        #If directory Exists
        if ($result)
        {

            $filename = str_slug($file->getClientOriginalName()).'-'.Str::random(3).'.'.$file->getClientOriginalExtension();

            #If image - process thumbnails
            #If image set maindir to images
            if (in_array(strtolower($file->getClientOriginalExtension()), $img_arr)) {

                $image_thumb_path = public_path($Dir.'thumbnail/');
                if (!$result = is_dir($image_thumb_path))
                {
                    #else make directory
                    $result = File::makeDirectory($image_thumb_path, 0777, true);
                }

                if ($result) {


                    #MAIN IMAGE
                    $img = Image::make($file->getRealPath());
                    $img->orientate();
                    $destinationPath = public_path($Dir);

                    $img->resize(config('app.images.max_width'), config('app.images.max_height'), function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($destinationPath . $filename, 80);

                    #THUMBNAIL
                    $img = Image::make($file->getRealPath());
                    $img->resize(config('app.images.thumb_width'), config('app.images.thumb_height'), function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($image_thumb_path . $filename);
                }

            } else {
                #NORMAL FILES
                $destinationPath = public_path($Dir);
                $file->move($destinationPath, $filename);
            }

            return $filename;
        }

    }


}