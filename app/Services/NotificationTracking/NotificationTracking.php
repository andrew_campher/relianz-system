<?php

namespace App\Services\NotificationTracking;
use jdavidbakr\MailTracker\Model\SentEmail;


/**
 * Class Professionals
 * @package App\Services\Professionals
 */
class NotificationTracking
{
    /**
     * Billing constructor.
     */
    public function __construct()
    {

    }

    public static function emailSentStats($email) {
        if ($sents = SentEmail::selectRaw('COUNT(*) AS sent, MAX(created_at) as last_sent, opens, clicks')->where('recipient', 'LIKE', $email)->groupBy('opens')->get()->toArray()) {
            $sent_stats = array();

            $sent_stats['sent']= 0;
            $sent_stats['opened']= 0;
            $sent_stats['clicks']= 0;
            $sent_stats['last_sent']= null;

            foreach ($sents As $sent) {
                if ($sent['opens']) {
                    $sent_stats['opened'] = $sent['sent'];
                    $sent_stats['sent'] = $sent['sent']+$sent_stats['sent'];
                } else {
                    $sent_stats['sent'] = $sent['sent']+$sent_stats['sent'];
                }

                #Clicks
                $sent_stats['clicks'] = $sent['clicks']+$sent_stats['clicks'];

                if (!$sent_stats['last_sent'] || strtotime($sent_stats['last_sent']) < $sent['last_sent']) {
                    $sent_stats['last_sent'] = $sent['last_sent'];
                }
            }

            return $sent_stats;
        }

        return false;
    }

    /**
     * GETS sent stats for a entity and notifiable
     * @param $entity_type
     * @param $entity_id
     * @param $notifiable_type
     * @param $notifiable_id
     * @return array|bool
     */
    public static function entityNotifiableSentStats($entity_type, $entity_id, $notifiable_type, $notifiable_id) {
        if ($sents = SentEmail::selectRaw('COUNT(*) AS sent, MAX(created_at) as last_sent, opens, clicks')
            ->where('entity_type', $entity_type)
            ->where('entity_id', $entity_id)
            ->where('notifiable_type', $notifiable_type)
            ->where('notifiable_id', $notifiable_id)
            ->groupBy('opens')->get()->toArray()) {

            $sent_stats = array();

            $sent_stats['sent']= 0;
            $sent_stats['clicks']= 0;
            $sent_stats['opened']= 0;
            $sent_stats['last_sent']= null;

            foreach ($sents As $sent) {
                if ($sent['opens']) {
                    $sent_stats['opened'] = $sent['sent'];
                    $sent_stats['sent'] = $sent['sent']+$sent_stats['sent'];
                } else {
                    $sent_stats['sent'] = $sent['sent']+$sent_stats['sent'];
                }

                #Clicks
                $sent_stats['clicks'] = $sent['clicks']+$sent_stats['clicks'];

                if (!$sent_stats['last_sent'] || strtotime($sent_stats['last_sent']) < $sent['last_sent']) {
                    $sent_stats['last_sent'] = $sent['last_sent'];
                }
            }

            return $sent_stats;
        }

        return false;
    }

}