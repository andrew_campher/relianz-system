<?php

namespace App\Services\Professionals;
use App\Models\Professional\Professional;
use App\Models\ProfessionalBranch\ProfessionalBranch;
use Illuminate\Support\Facades\Notification;


/**
 * Class Professionals
 * @package App\Services\Professionals
 */
class ProfessionalsService
{
    /**
     * Billing constructor.
     */
    public function __construct()
    {

    }

    public static function BestForJob($project, $only_active = false)
    {
        #Get all pros in the radius of JOB
        if ($only_active) {
            $pros = ProfessionalBranch::ServiceRange($project)->where('professionals.status', 1)->get();
        } else {
            $pros = ProfessionalBranch::ServiceRange($project)->where('professionals.status', '>', 0)->get();
        }

        if (!$pros->isEmpty()) {
            return $pros;
        }

        /**
         * Broaden Search ---
         * TODO
         */
    }

    public static function canSendQuote($project) {
        /**
         * ENSURE professional can submit quote:
         * Has enough credits
         * project service is free
         * Package is free
         */
        if (!auth()->user()->professional->approved)
            return false;

        /**
         * IF credits system is disabled
         */
        if (!config('app.settings.enable_credits'))
            return true;

        $yes = 0;
        if (auth()->user()->professional->account->package->free == 1) {
            $yes = 1;
        } else {
            if (auth()->user()->professional->account->credits_remain >= $project->service->fee) {
                $yes = 1;
            }
        }

        if ($yes) {
            return true;
        } else {
            return false;
        }

    }


}