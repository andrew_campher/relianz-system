<?php

return [

    'customers' => [
        'send_notes_updated' => 'rhiannon@relianz.co.za',
    ],

    'date' => [
        'long' => 'd M Y H:i:s',
        'long_min' => 'd M Y H:i',
        'short' => 'd M Y',
    ],

    'images' => [
        'thumb_width' => 200,
        'thumb_height' => 200,

        'icon_width' => 50,
        'icon_height' => 50,

        'max_width' => 1600,
        'max_height' => null,
    ],

    'defaults' => [
        'hero_image' => 'default_hero.jpg',
    ],

    'settings' => [
        'quote_limit' => 5,
        'quote_days_expire' => 5,
        'request_days_expire' => env('HH_REQUEST_DAYS_EXPIRE', 5),
        #Send first request to client about review
        'first_notify_after_days' => 3,
        'second_notify_after_days' => 14,
        'third_notify_after_days' => 30,
        #After how many hours of the quote not being read does he get a refund of credits
        'credit_refund_hours' => 48,
        'verify_mobile' => true,
        'enable_credits' => env('HH_ENABLE_CREDITS', true)
    ],

    'currency' => [
        'symbol' => 'R',
    ],

    'google' => [
        'api_maps_key' => env('GOOGLE_API_MAPS_KEY', 'AIzaSyBcEukYkYEi-rZQiPUBmapGVI7Rb5FxWX8')
    ],

    'company' => [
        'name' => env('COMPANY_NAME', 'RELIANZ FOODS CC'),
        'address' => env('COMPANY_ADDRESS', 'Unit H1, Maitland Park, 733 Voortrekker Road, Maitland'),
        'registration' => env('COMPANY_REF', '2008/1518518/15'),
    ],


    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */

    'name' => 'Relianz',
    'website' => 'relianz.co.za',

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', true),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Africa/Johannesburg',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => env('APP_LOCALE', 'en'),

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),

	/*
    |--------------------------------------------------------------------------
    | PHP Locale Code
    |--------------------------------------------------------------------------
    |
    | The PHP locale determines the default locale that will be used
    | by the Carbon library when setting Carbon's localization.
    |
    */
	'locale_php' => env('APP_LOCALE_PHP', 'en_US'),

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'daily'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */
		Arcanedev\LogViewer\LogViewerServiceProvider::class,
		Arcanedev\NoCaptcha\NoCaptchaServiceProvider::class,
		Collective\Html\HtmlServiceProvider::class,
		DaveJamesMiller\Breadcrumbs\ServiceProvider::class,
		HieuLe\Active\ActiveServiceProvider::class,
		Laravel\Socialite\SocialiteServiceProvider::class,
		Yajra\Datatables\DatatablesServiceProvider::class,

		/*
		 * Has to override the Collective\Html\HtmlServiceProvider form singleton
		 */
		App\Providers\MacroServiceProvider::class,

		/*
         * Application Service Providers...
         */
		App\Providers\AccessServiceProvider::class,
		App\Providers\AppServiceProvider::class,
		App\Providers\AuthServiceProvider::class,
		App\Providers\BladeServiceProvider::class,
		App\Providers\BroadcastServiceProvider::class,
		App\Providers\ComposerServiceProvider::class,
		App\Providers\EventServiceProvider::class,
		App\Providers\HistoryServiceProvider::class,
		App\Providers\RouteServiceProvider::class,
        /*
         *  Custom Providers
         */
        Intervention\Image\ImageServiceProvider::class,
        Cviebrock\EloquentSluggable\ServiceProvider::class,
        Barryvdh\DomPDF\ServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

		/*
         * Third Party Aliases
         */

		'Active'      => HieuLe\Active\Facades\Active::class,
		'Breadcrumbs' => DaveJamesMiller\Breadcrumbs\Facade::class,
		'Captcha'     => Arcanedev\NoCaptcha\Facades\NoCaptcha::class,
		'Form'        => Collective\Html\FormFacade::class,
		'Html'        => Collective\Html\HtmlFacade::class,
		'Socialite'   => Laravel\Socialite\Facades\Socialite::class,
        'Image'       => Intervention\Image\Facades\Image::class,
        'Helper'      => App\Helpers\Helper::class,
        'Omnipay'     => Ignited\LaravelOmnipay\Facades\OmnipayFacade::class,
        'PDF'         => Barryvdh\DomPDF\Facade::class,
    ],
];
