<?php

return [

	// The default gateway to use
	'default' => 'payfast',

	// Add in each gateway here
	'gateways' => [
        'payfast' => [
            'driver'  => 'PayFast',
            'options' => [
                'merchantId'   => '10000100',
                'merchantKey'    => '46f0cd694581a',
                'pdtKey' => '',
                'testMode' => true
            ]
        ],
		'paypal' => [
			'driver'  => 'PayPal_Express',
			'options' => [
				'solutionType'   => '',
				'landingPage'    => '',
				'headerImageUrl' => ''
			]
		],
	]

];