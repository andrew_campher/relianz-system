<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServiceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('pro_title');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->integer('parent_id')->unsigned();
            $table->tinyInteger('level')->unsigned();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('question_type')->default(null)->nullable();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('questions_options', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('question_id')->unsigned()->index();
            $table->string('title');
            $table->text('description');
            $table->string('special');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('question_id')
                ->references('id')
                ->on('questions')
                ->onDelete('cascade');
        });

        Schema::create('question_service', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('question_id')->unsigned()->index();
            $table->integer('service_id')->unsigned()->index();
            $table->string('title')->default(null);
            $table->string('pro_title')->default(null);
            $table->text('description')->default(null);
            $table->integer('question_order')->default(1)->unsigned();
            $table->integer('require_question_id')->default(0)->unsigned();
            $table->timestamps();

            $table->foreign('question_id')
                ->references('id')
                ->on('questions')
                ->onDelete('cascade');

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');
        });

        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->integer('service_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });

        Schema::create('project_service', function (Blueprint $table) {
            $table->integer('service_id')->unsigned()->index();
            $table->integer('project_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');
        });

        Schema::create('project_question_answer', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('project_id')->unsigned()->nullable()->index();
            $table->integer('question_service_id')->unsigned()->nullable()->index();
            $table->integer('question_option_id')->unsigned()->nullable()->index();
            $table->text('answer_value');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('question_service_id')
                ->references('id')
                ->on('question_service')
                ->onDelete('set null');

            $table->foreign('question_option_id')
                ->references('id')
                ->on('questions_options')
                ->onDelete('set null');

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('set null');
        });

        Schema::create('files', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('filename');
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
        });

        Schema::create('professionals', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->string('title');
            $table->string('logo')->nullable()->default(null);
            $table->text('description');
            $table->float('rating',5,5)->default(0);
            $table->integer('successful_projects')->default(0)->unsigned();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->tinyInteger('approved')->default(0)->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });

        Schema::create('professional_service', function (Blueprint $table) {
            $table->integer('service_id')->unsigned()->index();
            $table->integer('professional_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('cascade');
        });

        Schema::create('projects_reviews', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('project_id')->unsigned()->index();
            $table->integer('professional_id')->unsigned()->nullable()->index();
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('comments');
            $table->tinyInteger('project_success')->unsigned();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->boolean('approved')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('set null');
        });

        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->float('price',15,2);
            $table->text('description');
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->integer('professional_id')->unsigned()->nullable()->index();
            $table->integer('project_id')->unsigned()->nullable()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('set null');

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('services', function (Blueprint $table) {
            $table->dropForeign('services_type_id_foreign');
            $table->dropForeign('services_user_id_foreign');
        });

        Schema::dropIfExists('services');
    }
}
