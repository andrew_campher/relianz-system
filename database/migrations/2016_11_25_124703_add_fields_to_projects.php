<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('projects', function($table) {
            $table->tinyInteger('when')->after('description')->nullable();
            $table->text('when_describe')->after('description')->nullable();
            $table->string('postcode')->after('description')->nullable();
            $table->decimal('lat', 10, 7)->after('description')->nullable();
            $table->decimal('lng', 10, 7)->after('description')->nullable();
            $table->dateTime('specific_date')->after('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
