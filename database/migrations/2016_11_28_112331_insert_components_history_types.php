<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertComponentsHistoryTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('history_types')->insert([
            [
                'name' => 'Project', 'created_at' => DB::raw('now()'), 'updated_at' => DB::raw('now()'),
            ],
            [
                'name' => 'Message', 'created_at' => DB::raw('now()'), 'updated_at' => DB::raw('now()'),
            ],
            [
                'name' => 'Professional', 'created_at' => DB::raw('now()'), 'updated_at' => DB::raw('now()'),
            ],
            [
                'name' => 'Quote', 'created_at' => DB::raw('now()'), 'updated_at' => DB::raw('now()'),
            ],
            [
                'name' => 'Credit', 'created_at' => DB::raw('now()'), 'updated_at' => DB::raw('now()'),
            ],
            [
                'name' => 'Notification', 'created_at' => DB::raw('now()'), 'updated_at' => DB::raw('now()'),
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
