<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfessionalProfileFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('professionals', function($table) {
            $table->decimal('lat', 10, 7)->after('rating')->nullable();
            $table->decimal('lng', 10, 7)->after('rating')->nullable();
            $table->string('telephone')->after('rating')->nullable();
            $table->string('email')->after('rating')->nullable();
            $table->integer('distance_travelled')->after('rating')->nullable();
            $table->integer('year_started')->after('rating')->nullable();
            $table->text('service_stand_out')->after('rating')->nullable();
            $table->text('service_enjoy_what')->after('rating')->nullable();
            $table->string('social_facebook')->after('rating')->nullable();
            $table->string('social_twitter')->after('rating')->nullable();
            $table->string('social_linkedin')->after('rating')->nullable();
            $table->string('social_google')->after('rating')->nullable();
            $table->string('website_url')->after('rating')->nullable();
        });

        Schema::table('projects_reviews', function($table) {
            $table->tinyInteger('verified')->after('approved')->default(0);
        });

        Schema::table('media', function($table) {
            $table->integer('modelname_id')->default(null)->nullable();
            $table->string('modelname_type')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
