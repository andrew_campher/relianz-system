<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingsToReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('projects_reviews', function($table) {
            $table->tinyInteger('rating_price')->after('comments')->nullable()->default(NULL);
            $table->tinyInteger('rating_time')->after('comments')->nullable()->default(NULL);
            $table->tinyInteger('rating_quality')->after('comments')->nullable()->default(NULL);
            $table->tinyInteger('rating_cooperative')->after('comments')->nullable()->default(NULL);
            $table->tinyInteger('rating_overall')->after('comments')->nullable()->default(NULL);

            $table->tinyInteger('recommended')->after('comments')->nullable()->default(NULL);

            $table->integer('quote_id')->after('professional_id')->unsigned()->index();

            $table->foreign('quote_id')
                ->references('id')
                ->on('quotes')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::table('professionals', function($table) {
            $table->float('rating_price_tot', 5, 5)->after('description')->nullable()->default(NULL);
            $table->float('rating_time_tot', 5, 5)->after('description')->nullable()->default(NULL);
            $table->float('rating_quality_tot', 5, 5)->after('description')->nullable()->default(NULL);
            $table->float('rating_cooperative_tot', 5, 5)->after('description')->nullable()->default(NULL);
            $table->float('rating_overall_tot', 5, 5)->after('description')->nullable()->default(NULL);

            $table->integer('recommended_tot')->after('description')->nullable()->default(NULL);
        });


        Schema::create('users_reviews', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('project_id')->unsigned()->index();
            $table->integer('professional_id')->unsigned()->nullable()->index();
            $table->integer('quote_id')->unsigned()->index();

            $table->tinyInteger('rating_pay')->nullable()->default(NULL);
            $table->tinyInteger('rating_understanding')->nullable()->default(NULL);
            $table->tinyInteger('rating_clear')->nullable()->default(NULL);
            $table->tinyInteger('recommended')->nullable()->default(NULL);

            $table->integer('user_id')->unsigned();
            $table->text('comments');

            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('set null');

            $table->foreign('quote_id')
                ->references('id')
                ->on('quotes')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
