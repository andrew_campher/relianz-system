<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHitsToAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('professionals', function($table) {
            $table->integer('hits')->after('status')->default(0);
            $table->integer('hits_day')->after('status')->default(0);
            $table->integer('hits_month')->after('status')->default(0);
            $table->integer('hits_year')->after('status')->default(0);
        });

        Schema::table('categories', function($table) {
            $table->integer('hits')->after('status')->default(0);
            $table->integer('hits_day')->after('status')->default(0);
            $table->integer('hits_month')->after('status')->default(0);
            $table->integer('hits_year')->after('status')->default(0);
        });

        Schema::table('projects', function($table) {
            $table->integer('hits')->after('status')->default(0);
            $table->integer('hits_day')->after('status')->default(0);
            $table->integer('hits_month')->after('status')->default(0);
            $table->integer('hits_year')->after('status')->default(0);
        });

        Schema::table('projects_reviews', function($table) {
            $table->integer('hits')->after('status')->default(0);
            $table->integer('hits_day')->after('status')->default(0);
            $table->integer('hits_month')->after('status')->default(0);
            $table->integer('hits_year')->after('status')->default(0);
        });

        Schema::table('quotes', function($table) {
            $table->integer('hits')->after('status')->default(0);
            $table->integer('hits_day')->after('status')->default(0);
            $table->integer('hits_month')->after('status')->default(0);
            $table->integer('hits_year')->after('status')->default(0);
        });

        Schema::table('services', function($table) {
            $table->integer('hits')->after('status')->default(0);
            $table->integer('hits_day')->after('status')->default(0);
            $table->integer('hits_month')->after('status')->default(0);
            $table->integer('hits_year')->after('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
