<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersProfessionalReputationStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_stats', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('hires_count')->nullable()->unsigned();
            $table->integer('reviews_count')->nullable()->unsigned();
            $table->integer('stat_pay')->nullable()->unsigned();
            $table->integer('stat_understanding')->nullable()->unsigned();
            $table->integer('stat_clear')->nullable()->unsigned();
            $table->integer('recommends_count')->nullable()->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('professionals_stats', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('professional_id')->unsigned();
            $table->integer('hires_count')->nullable()->unsigned();
            $table->integer('hires_success')->nullable()->unsigned();
            $table->integer('stat_overall')->nullable()->unsigned();
            $table->integer('stat_cooperative')->nullable()->unsigned();
            $table->integer('stat_quality')->nullable()->unsigned();
            $table->integer('stat_time')->nullable()->unsigned();
            $table->integer('stat_price')->nullable()->unsigned();
            $table->integer('recommends_count')->nullable()->unsigned();

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
