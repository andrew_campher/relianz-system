<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyStatTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users_stats', function($table) {
            $table->integer('hires_count')->default(0)->unsigned()->change();
            $table->integer('reviews_count')->default(0)->unsigned()->change();
            $table->float('stat_pay', 5, 5)->default(0)->unsigned()->change();
            $table->float('stat_understanding', 5, 5)->default(0)->unsigned()->change();
            $table->float('stat_clear', 5, 5)->default(0)->unsigned()->change();
            $table->integer('recommends_count')->default(0)->unsigned()->change();
        });

        Schema::table('professionals_stats', function($table) {
            $table->integer('hires_count')->default(0)->unsigned()->change();
            $table->integer('hires_success')->default(0)->unsigned()->change();
            
            $table->float('stat_overall', 5, 5)->default(0)->unsigned()->change();
            $table->float('stat_cooperative', 5, 5)->default(0)->unsigned()->change();
            $table->float('stat_quality', 5, 5)->default(0)->unsigned()->change();
            $table->float('stat_time', 5, 5)->default(0)->unsigned()->change();
            $table->float('stat_price', 5, 5)->default(0)->unsigned()->change();
            
            $table->integer('recommends_count')->default(0)->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
