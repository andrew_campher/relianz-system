<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixDoubleValueProfessionalsOther extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('professionals', function($table) {
            $table->float('rating_overall_tot', 10, 5)->default(0)->unsigned()->change();
            $table->float('rating_cooperative_tot', 10, 5)->default(0)->unsigned()->change();
            $table->float('rating_quality_tot', 10, 5)->default(0)->unsigned()->change();
            $table->float('rating_time_tot', 10, 5)->default(0)->unsigned()->change();
            $table->float('rating_price_tot', 10, 5)->default(0)->unsigned()->change();
            $table->float('rating', 10, 5)->default(0)->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
