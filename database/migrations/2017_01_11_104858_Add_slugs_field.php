<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('professionals', function($table) {
            $table->string('slug')->after('title')->nullable()->index();
        });

        Schema::table('categories', function($table) {
            $table->string('slug')->after('title')->nullable()->index();
        });

        Schema::table('services', function($table) {
            $table->string('slug')->after('title')->nullable()->index();
            $table->string('pro_slug')->after('pro_title')->nullable();
        });

        Schema::table('projects', function($table) {
            $table->string('slug')->after('title')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
