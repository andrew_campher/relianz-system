<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactDetailsToProjectReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('projects_reviews', function($table) {
            $table->string('client_last_name')->after('comments')->nullable();
            $table->string('client_first_name')->after('comments')->nullable();
            $table->string('client_email')->after('comments')->nullable();
            $table->string('client_mobile')->after('comments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
