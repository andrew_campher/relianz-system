<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->float('cost',15,2)->default(0);
            $table->float('discount_percent',5,2)->nullable();
            $table->tinyInteger('free')->default(0)->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->float('credits_remain',15,2);
            $table->integer('professional_id')->nullable()->unsigned()->index();
            $table->integer('package_id')->nullable()->unsigned();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('set null');

            $table->foreign('package_id')
                ->references('id')
                ->on('packages')
                ->onDelete('set null');
        });


        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->float('amount',15,2);
            $table->text('description');
            $table->integer('account_id')->nullable()->unsigned()->index();
            $table->integer('professional_id')->nullable()->unsigned()->index();
            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->float('extra_amount',15,2)->default(0);
            $table->integer('package_id')->nullable()->unsigned();
            $table->tinyInteger('status')->default(0)->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('set null');

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('set null');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->foreign('package_id')
                ->references('id')
                ->on('packages')
                ->onDelete('set null');
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->float('amount_paid',15,2);
            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->integer('invoice_id')->nullable()->unsigned()->index();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });


        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('account_id')->nullable()->unsigned()->index();
            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->integer('professional_id')->nullable()->nullable()->unsigned()->index();
            $table->float('cost',15,2);
            $table->integer('servicetable_id')->unsigned()->index();
            $table->string('servicetable_type')->index();

            $table->timestamps();
            $table->softDeletes();
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
