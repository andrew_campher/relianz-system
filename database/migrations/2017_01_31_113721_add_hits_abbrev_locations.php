<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHitsAbbrevLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('locations', function($table) {
            $table->string('short')->after('title');
            $table->integer('pro_count')->after('title')->unsigned();
            $table->integer('hits')->after('type')->unsigned();
            $table->integer('hits_day')->after('type')->unsigned();
            $table->integer('hits_month')->after('type')->unsigned();
            $table->integer('hits_year')->after('type')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
