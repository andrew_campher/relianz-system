<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRatingOverall extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('projects_reviews', function (Blueprint $table) {
            $table->float('rating_overall', 2, 1)->default(0)->unsigned()->change();
        });

        Schema::table('users_reviews', function (Blueprint $table) {
            $table->float('rating_overall', 2, 1)->default(0)->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
