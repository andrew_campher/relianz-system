<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('search_log', function (Blueprint $table) {
            $table->string('search_term')->unique();
            $table->integer('hits')->default(0);
            $table->integer('searches')->default(0);
            $table->string('IP')->nullable();
            $table->integer('searches_week')->default(0);
            $table->integer('searches_month')->default(0);
            $table->integer('searches_year')->default(0);
            $table->tinyInteger('found')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
