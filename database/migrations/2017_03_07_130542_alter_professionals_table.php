<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProfessionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('professionals', function($table) {
            $table->string('street')->after('description')->nullable();
            $table->string('city')->after('description')->nullable();
            $table->string('province')->after('description')->nullable();
            $table->integer('postcode')->after('description')->nullable();

            $table->dropColumn('service_enjoy_what');
            $table->dropColumn('service_stand_out');
            $table->dropColumn('rating');
            $table->dropColumn('social_linkedin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
