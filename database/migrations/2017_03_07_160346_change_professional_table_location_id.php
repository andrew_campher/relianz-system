<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProfessionalTableLocationId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('professionals', function($table) {
            $table->integer('location_id')->after('description')->nullable();

            $table->dropColumn('street');
            $table->dropColumn('city');
            $table->dropColumn('province');
            $table->dropColumn('postcode');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
