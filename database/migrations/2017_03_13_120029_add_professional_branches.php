<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfessionalBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('professional_branches', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('professional_id')->unsigned()->index();
            $table->string('slug')->nullable();
            $table->string('title')->nullable();
            $table->string('telephone')->nullable();
            $table->string('notify_email')->nullable();
            $table->string('notify_mobile')->nullable();
            $table->integer('distance_travelled');
            $table->decimal('lat', 10, 7)->nullable();
            $table->decimal('lng', 10, 7)->nullable();
            $table->string('area');

            $table->foreign('professional_id')
                ->references('id')
                ->on('professionals')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
