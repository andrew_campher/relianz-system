<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMorphToSentEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sent_emails', function($table) {
            $table->integer('entity_id')->after('hash')->nullable();
            $table->string('entity_type')->after('hash')->nullable();

            $table->integer('notifiable_id')->after('entity_type')->nullable();
            $table->string('notifiable_type')->after('notifiable_id')->nullable();

            $table->string('notification_class')->after('entity_type')->nullable();
            $table->string('notification_type')->after('notification_class')->nullable();

            $table->index(['notifiable_type', 'notifiable_id']);
            $table->index(['entity_type', 'entity_id']);
            $table->index('recipient');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
