<?php

use Illuminate\Database\Seeder;

class ServiceAppSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /**
         * Access Permissions
         */
        $permission_model          = config('access.permission');
        $manageUsers               = new $permission_model;
        $manageUsers->name         = 'manage-questions';
        $manageUsers->display_name = 'Manage Questions';
        $manageUsers->sort         = 2;
        $manageUsers->created_at   = Carbon::now();
        $manageUsers->updated_at   = Carbon::now();
        $manageUsers->save();


        $permission_model          = config('access.permission');
        $manageUsers               = new $permission_model;
        $manageUsers->name         = 'manage-services';
        $manageUsers->display_name = 'Manage Services';
        $manageUsers->sort         = 2;
        $manageUsers->created_at   = Carbon::now();
        $manageUsers->updated_at   = Carbon::now();
        $manageUsers->save();


        $permission_model          = config('access.permission');
        $manageUsers               = new $permission_model;
        $manageUsers->name         = 'manage-projects';
        $manageUsers->display_name = 'Manage Projects';
        $manageUsers->sort         = 2;
        $manageUsers->created_at   = Carbon::now();
        $manageUsers->updated_at   = Carbon::now();
        $manageUsers->save();


        $permission_model          = config('access.permission');
        $manageUsers               = new $permission_model;
        $manageUsers->name         = 'manage-professionals';
        $manageUsers->display_name = 'Manage Professionals';
        $manageUsers->sort         = 2;
        $manageUsers->created_at   = Carbon::now();
        $manageUsers->updated_at   = Carbon::now();
        $manageUsers->save();


        $permission_model          = config('access.permission');
        $manageUsers               = new $permission_model;
        $manageUsers->name         = 'manage-quotes';
        $manageUsers->display_name = 'Manage Quotes';
        $manageUsers->sort         = 2;
        $manageUsers->created_at   = Carbon::now();
        $manageUsers->updated_at   = Carbon::now();
        $manageUsers->save();
    }
}
