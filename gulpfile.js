const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');
require('./elixir-extensions');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

//noinspection BadExpressionStatementJS
elixir((mix) => {
    /**
     * Copy needed files from /node directories
     * to /public directory.
     */
    mix.copy(
        'node_modules/font-awesome/fonts',
        'public/build/fonts/font-awesome'
    )

    .copy(
        'resources/assets/img/bootflat',
        'public/build/bootflat/img'
    )

    .copy(
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        'resources/assets/js/bootstrap/bootstrap.min.js'
    )

    .copy(
        'node_modules/bootstrap-sass/assets/fonts/bootstrap',
        'public/build/fonts/bootstrap'
    )

    .copy(
        'node_modules/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js',
        'resources/assets/js/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.all.min.js'
    )

    .copy(
        'node_modules/socket.io-client/dist/socket.io.js',
        'resources/assets/js/socket.io-client/socket.io.js'
    )

    .copy(
        'node_modules/socket.io-client/dist/socket.io.min.js',
        'resources/assets/js/socket.io-client/socket.io.min.js'
    )

    .copy(
        'node_modules/noty/js/noty/packaged/jquery.noty.packaged.min.js',
        'resources/assets/js/noty/jquery.noty.packaged.min.js'
    )

    .copy(
        'node_modules/jquery-bar-rating/dist/jquery.barrating.min.js',
        'resources/assets/js/plugin/jquery-bar-rating/jquery.barrating.min.js'
    )
    .copy(
        'node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css',
        'resources/assets/css/frontend/jquery-bar-rating/fontawesome-stars-o.css'
    )

    .copy(
        'node_modules/bootstrap-validator/dist/validator.min.js',
        'resources/assets/js/plugin/bootstrap-validator/validator.min.js'
    )

    .copy(
        'node_modules/jquery/dist/jquery.min.js',
        'resources/assets/js/jquery/jquery.min.js'
    )

    /**
     * Process frontend SCSS stylesheets
     */
    .sass([
        'plugin/sweetalert/sweetalert.scss'
    ], 'resources/assets/css/frontend/app.css')

    /**
     * Process frontend LESS stylesheets
     */
    .less([
        'datetimepicker.less',
    ], 'resources/assets/css/frontend/app_less.css')

    /**
     * Combine pre-processed frontend CSS files
     */
    .styles([
        'frontend/bootstrap.css',
        'bootflat/bootflat.min.css',
        'frontend/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.css',
        'frontend/app.css',
        'frontend/app_less.css',
        'frontend/datetimepicker.css',
        'frontend/jquery-bar-rating/fontawesome-stars.css',
        'frontend/site.css',
        'frontend/site_plugins.css'
    ], 'public/css/frontend.css')


    .webpack('frontend/app.js', './resources/assets/js/dist/frontend.js')


    /**
     * Combine PUBLIC scripts
     */
    .scripts([
        'jquery/jquery.min.js',
        'bootstrap/bootstrap.min.js',
        'plugin/datetimepicker/bootstrap-datetimepicker.min.js',
        'plugin/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
        'plugin/jquery-bar-rating/jquery.barrating.min.js',
        'bootflat/icheck.min.js',
        'bootflat/jquery.fs.selecter.min.js',
        'bootflat/jquery.fs.stepper.min.js',
        'plugin/bootstrap-validator/validator.min.js',
        'bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.all.min.js',
        'plugin/sweetalert/sweetalert.min.js',
        'frontend/site.js',
    ], 'public/js/public.js')

    /**
     * Combine frontend scripts
     */
    .scripts([
        'socket.io-client/socket.io.js',
        'dist/frontend.js',
        'noty/jquery.noty.packaged.min.js',
    ], 'public/js/frontend.js')


    /******************************************************************************************************************
     * BACKEND
     * Process backend SCSS stylesheets
     */
    .sass([
        'backend/app.scss',
        'plugin/toastr/toastr.scss',
        'plugin/sweetalert/sweetalert.scss'
    ], 'resources/assets/css/backend/app.css')

    /**
     * Combine pre-processed backend CSS files
     */
    .styles([
        'backend/app.css',
        'backend/backend_site.css',
    ], 'public/css/backend.css')

    /**
     * Pack it up
     * Saves to a dist folder in resources, it is then combined and placed in public
     */
    .webpack('backend/app.js', './resources/assets/js/dist/backend.js')

    /**
     * Make RTL (Right To Left) CSS stylesheet for the backend
     */
    .rtlCss()

    /**
     * Combine backend scripts
     */
    .scripts([
        'socket.io-client/socket.io.js',
        'dist/backend.js',
        'plugin/sweetalert/sweetalert.min.js',
        'plugin/toastr/toastr.min.js',
        'bootstrap-wysiwyg/bootstrap-wysiwyg.min.js',
        'plugins.js',
        'backend/custom.js'
    ], 'public/js/backend.js')

    /**
     * Combine pre-processed rtl CSS files
     */
    .styles([
        'rtl/bootstrap-rtl.css'
    ], 'public/css/rtl.css')

    /**
     * Apply version control
     */
    .version([
        "public/css/frontend.css",
        "public/js/public.js",
        "public/js/frontend.js",
        "public/css/backend.css",
        "public/css/backend-rtl.css",
        "public/js/backend.js",
        "public/css/rtl.css"
    ]);

    /**
     * Run tests
     */
    //.phpUnit();
});