const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')('3000');

const Redis = require('ioredis');

const redis = new Redis();

redis.subscribe('all-channel');

redis.subscribe('quote.3');

redis.on('message', function (channel, message) {
    console.log('New message: '+channel+' |'+message);
    const event = JSON.parse(message);
    io.emit(event.event, channel, event.data);
});
