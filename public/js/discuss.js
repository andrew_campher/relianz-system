Vue.component('comments', {
    template: '#comments-template',

    props: ['list'],

    created() {
        this.list = this.list;
    }

});

new Vue({
    el: '#app'
})