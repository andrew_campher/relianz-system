* Run `composer install`
* Run `npm install`
* create .env file - copy under config
* `php artisan migrate`
* seeds `php artisan db:seed`
* Create "fonts" directory under /storage (777)
* Starting The Scheduler  `* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1`
* Install laravel echo server: `npm install -g laravel-echo-server`
* laravel-echo-server.txt to rename to laravel-echo-server.json
* Make sure host is same in laravel-echo=server.json and in resources/assets/js/app.js
* Start echo server `laravel-echo-server start` or Follow below to create background task:

`npm install -g pm2` to install pm2 to run background task

`pm2 start echo-server.json`

* gd.jpeg_ignore_warning = 1 in  php.ini after [gd]
* Install redis server `apt-get install redis-server`


