window._ = require('lodash');


import Echo from "laravel-echo";

var host = window.location.hostname;

if(host == '10.0.0.54' || host == 'localhost') {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: 'http://'+host+':3000'
    });
} else {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: 'https://handyhire.co.za:3001'
    });
}



window.Vue = require('vue');
require('vue-resource');
//Vue.config.devtools = false
//Vue.config.debug = false
//Vue.config.silent = true

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);
    next();
});


Vue.component('comments', require('./components/frontend/Comment.vue'));
Vue.component('notifications', require('./components/frontend/Notification.vue'));

const app = new Vue({
    el: '#app'
});