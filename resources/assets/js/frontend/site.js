if ($(window).height() > $('.sidebar_in').height()) {
    if ($('.sidebar_in').length > 0 && $('.sidebar_in').height() < $('.sidebar_content').height()) {
        $('.sidebar_in').on('affix.bs.affix', function () {
            $(this).addClass('col-xs-12 col-sm-4 col-md-4 col-lg-4');
        });
        $('.sidebar_in').on('affixed-top.bs.affix', function () {
            $(this).removeClass('col-xs-12 col-sm-4 col-md-4 col-lg-4');
        });
        $('.sidebar_in').affix({
            offset: {top: $('.sidebar_in').offset().top}
        });
    }
}

function openWizard(e, service, search) {
    e.preventDefault();
    if(!search)
        var search = '';

    if (service) {
        fireModal('/project/wizard?service=' + service);
    } else {
        window.location = '/project/wizard?searchstring='+encodeURIComponent(search);
    }
}

/*Method for initiating any elements*/
function init() {
    $('input').icheck({checkboxClass:"icheckbox_flat",increaseArea:"20%",radioClass:"iradio_flat"});

    $("[data-toggle=\"popover\"]").popover({
        trigger: 'hover click',
        placement: 'auto right',
        container: 'body'
    });

    $("[data-toggle=\"firemodal\"]").on('click', function(event) {
        event.preventDefault();
        fireModal($(this).attr('href'), $(this).attr('data-itemprop'));
    });

    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });

    $('#sModal').on('show.bs.modal', function(e) {
        window.location.hash = "modal";
    });

    $(window).on('hashchange', function (event) {
        if(window.location.hash != "#modal") {
            $('#sModal').modal('hide');
        }
    });
}

function fireModal(href, itemprop) {
    var imageTypesRegexp = new RegExp("\\.([png|jpg|jpeg|gif])(\\?.*)?$", "i");
    var classid = '#sModal .modal-body';

    $('#sModal').modal({backdrop: 'static', show: true});

    $(classid).html('<div class="loader"><i class="fa fa-gear fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span></div>');

    if (href.indexOf('#') == 0) {
        $(classid).html(jQuery(href).html());
    } else if (href.match(imageTypesRegexp) || itemprop == 'image') {
        var image = new Image();
        image.onload = function () {
            $(classid).html('<div class="image"><img src="' + image.src + '" /></div>');
        }
        image.src = href;
    } else if (itemprop == 'external') {
        $(classid).html('<iframe width="100%" height="100%" class="bootframe" src="' + href + '"></iframe>');
    } else {
        if (href.indexOf('?') == -1) {
            href = href + '?ajax=1';
        } else {
            href = href + '&ajax=1';
        }
        $.get(href, function (data) {
            $(classid).html(data);
            init();
        });
    }
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function addContact(professional_id) {
    jQuery.ajax({
        url: "/api/user/addContact",
        data: { professional_id: professional_id },
        type: "POST",
        headers: {
            'Authorization':'Bearer '+window.Laravel.api_token,
        },
        success:function(resp){
            if (resp.status == 'success') {
                if (resp.data == 1) {
                    $('.contact'+professional_id).removeClass('fa-heart-o').addClass('fa-heart');
                    swal("Contact Added", resp.message, "success");
                } else {
                    $('.contact'+professional_id).removeClass('fa-heart').addClass('fa-heart-o');
                    swal("Contact Removed", resp.message, "success");
                }

            } else {
                swal("Error", resp.message, "error");
            }
        }
    });
}

function MarkQuoteHired(id, el) {
    swal({
        title: "Mark as Hired",
        text: $(el).attr('data-content'),
        type: "info",
        html: true,
        customClass: 'MarkHired',
        showCancelButton: true,
        confirmButtonColor: "#6aa84f",
        confirmButtonText: "Yes, Proceed!",
        closeOnConfirm: false
    }, function(isConfirmed){
        if (isConfirmed){
            jQuery.ajax({
                url: "/api/quote/"+id+"/hired",
                type: "POST",
                headers: {
                    'Authorization':'Bearer '+window.Laravel.api_token,
                },
                success:function(resp){
                    if (resp.status == 'success') {
                        if (resp.data.type == 'hired') {
                            swal("Professional marked as Hired!", "That's great! We have alerted the Professional.", "success");
                        } else {
                            swal("Professional marked as Hired", "Client contacted in order to confirm.", "success");
                        }

                        setTimeout(function()
                        {
                            location.reload();
                        }, 2500);

                    } else {
                        swal("Error", "Something went wrong", "error");
                    }
                }
            });
        }
    });
}