<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'general' => [
            'approve' => 'Approve',
            'unapprove' => 'Un-approve',
            'activate' => 'Activate',
            'deactivate' => 'Deactivate',
            'delete_permanently' => 'Delete Permanently',

            'cancel' => 'Cancel',

            'crud' => [
                'create' => 'Create',
                'delete' => 'Delete',
                'edit' => 'Edit',
                'update' => 'Update',
                'view' => 'View',
            ],

            'restore' => 'Restore Item',

            'save' => 'Save',
            'view' => 'View',
        ],
        'access' => [
            'users' => [
                'activate' => 'Activate',
                'change_password' => 'Change Password',
                'deactivate' => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'login_as' => 'Login As :user',
                'resend_email' => 'Resend Confirmation E-mail',
                'restore_user' => 'Restore User',
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirm Email',
            'reset_password' => 'Reset Password',
        ],
    ],

    'general' => [

        'activate' => 'Activate',
        'deactivate' => 'Deactivate',
        'delete_permanently' => 'Delete Permanently',

        'cancel' => 'Cancel',

        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit' => 'Edit',
            'update' => 'Update',
			'view' => 'View',
        ],

        'restore' => 'Restore Item',

        'save' => 'Save',
        'view' => 'View',
    ],
];
