<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'notifications' => [
        'UserNotifyConfirmHired' => [
            'text' => 'Can you confirm this hire?',
            'url' => '/quote/:quote_id',
        ],
        'UserNotifyInvoiceSettled' => [
            'text' => 'Your invoice was settled - Thank you',
            'url' => '/invoice/:invoice_id',
        ],
        'UserNotifyNewQuote' => [
            'text' => 'You have received a new quote',
            'url' => '/project/:project_id/manage',
        ],
        'UserNotifyReadQuote' => [
            'text' => 'Your quote has been read by client',
            'url' => '/project/:project_id',
        ],
        'UserNotifyQuoteHired' => [
            'text' => 'Your quote has been chosen by the client',
            'url' => '/project/:project_id',
        ],
        'UserNotifyNewClientReview' => [
            'text' => 'You have been rated by the professional',
            'url' => '/project/:project_id',
        ],
        'UserNotifyNewProjectReview' => [
            'text' => 'The client has reviewed your work',
            'url' => '/project/:project_id',
        ],
        'UserNotifyInvoice' => [
            'text' => 'A new invoice has been created for the amount requested',
            'url' => '/invoice/:invoice_id',
        ],
        'AdminNotifyNewProfessional' => [
            'text' => 'New professional account has been created',
            'url' => '/professional/:professional_id',
        ],
        'AdminNotifyNewProject' => [
            'text' => 'New project has been posted',
            'url' => '/project/:project_id',
        ],
        'UserNotifyProjectExpired' => [
            'text' => 'Your project has expired',
            'url' => '/project/:project_id/manage',
        ],
        'NotifyQuoteUnsuccessful' => [
            'text' => 'Your quote was unsuccessful',
            'url' => '/project/:project_id',
        ],
        'UserNotifyProfessionalApprove' => [
            'text' => 'Your professional account has been approved!',
            'url' => '/professional/:professional_id',
        ],

        'general' => [
            'text' => 'Notification: :type',
            'url' => '',
        ],
    ],


    'frontend' => [
        'quotes' => [
            'price_types' => [
                'fixed' => 'Fixed Price',
                'per_hour' => 'Per Hour',
                'per_day' => 'Daily',
                'per_month' => 'Monthly',
            ],
            'status' => [
                'user' => [
                    'draft_description' => 'Quote is still in draft.',
                    'sent_description' => 'This is a unread quote.',
                    'read_description' => 'You have read this quote.',
                    'hired_description' => 'This professional was hired for the project.',
                    'unsuccessful_description' => 'This quote was unsuccessful.',
                    'reviewed_description' => 'You have reviewed this professionals work.',
                    'interacted_description' => 'You have interacted with this professional.',
                    'request_hired_description' => 'The professional requests to confirm his hire. Please click on the Confirm Hire.',
                ],
                'guest' => [
                    'draft_description' => 'Quote is still in draft.',
                    'sent_description' => 'This is a unread quote.',
                    'read_description' => 'This quote has been read by client.',
                    'hired_description' => 'This professional was hired for the project.',
                    'unsuccessful_description' => 'This quote was unsuccessful.',
                    'reviewed_description' => 'The client has reviewed this professionals work.',
                    'interacted_description' => 'The client has interacted with this professional.',
                    'request_hired_description' => 'The professional requests to confirm his hire.',
                ],
                'professional' => [
                    'draft_description' => 'Quote is still in draft.',
                    'sent_description' => 'This is a unread quote.',
                    'read_description' => 'This quote has been read by client.',
                    'pending_description' => 'Quotes waiting for response from client.',
                    'hired_description' => 'You have been hired for the project.',
                    'unsuccessful_description' => 'This quote was unsuccessful.',
                    'reviewed_description' => 'The client has reviewed your work.',
                    'interacted_description' => 'The client has interacted with you.',
                    'request_hired_description' => 'You have requested the client confirm your hire.',
                ],
            ],
            'draft_description' => 'These are quotes that have not been sent yet.',
            'pending_description' => 'These quotes have been sent to the client.',
            'hired_description' => 'You have been marked as hired on these quotes. You should be in contact with the client in order to proceed with the job.',
            'unsuccessful_description' => 'You have been marked as hired on these quotes. You should be in contact with the client in order to proceed with the job.',
        ],
        'projects' => [
            'draft_description' => 'These are quotes that have not been sent yet.',
            'status' => [
                'user' => [
                    'open_description' => 'Your project is available to receive quotes from professionals.',
                    'pending_description' => 'Your project is being processed.',
                    'hired_description' => 'Your have selected to hire a professional on this project.',
                    'completed_description' => 'You have hired a professional and have reviewed his work.',
                    'closed_description' => 'You have closed this project.',
                    'expired_description' => 'The project was listed for the maximum allowed days and has been expired. This means that no new quotes will be submitted.',
                    'fulfilled_description' => 'The project has received the maximum allowed quotes. Go ahead and review the quotes received.',
                ],
                'guest' => [
                    'open_description' => 'This project is available to be quoted on.',
                    'pending_description' => 'Currently waiting to be approved.',
                    'hired_description' => 'The client has made a hire on this project.',
                    'completed_description' => 'A professional was successfully hired and his work was completed.',
                    'closed_description' => 'The project was closed by the client.',
                    'expired_description' => 'The project expired.',
                    'fulfilled_description' => 'The project has received the maximum allowed quotes.',
                ],
                'professional' => [
                    'open_description' => 'You are able to submit a quote on this project.',
                    'pending_description' => 'Currently waiting to be approved.',
                    'hired_description' => 'A professional was hired on this project.',
                    'completed_description' => 'A professional was hired and his work has been reviewed.',
                    'closed_description' => 'Client decided to close this project.',
                    'expired_description' => 'The project was automatically expired after being listed for the maximum days.',
                    'fulfilled_description' => 'The project has received the maximum allowed quotes.',
                ],
            ],
        ],

        'test' => 'Test',

        'tests' => [
            'based_on' => [
                'permission' => 'Permission Based - ',
                'role' => 'Role Based - ',
            ],

            'js_injected_from_controller' => 'Javascript Injected from a Controller',

            'using_blade_extensions' => 'Using Blade Extensions',

            'using_access_helper' => [
                'array_permissions' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does have to possess all.',
                'array_permissions_not' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does not have to possess all.',
                'array_roles' => 'Using Access Helper with Array of Role Names or ID\'s where the user does have to possess all.',
                'array_roles_not' => 'Using Access Helper with Array of Role Names or ID\'s where the user does not have to possess all.',
                'permission_id' => 'Using Access Helper with Permission ID',
                'permission_name' => 'Using Access Helper with Permission Name',
                'role_id' => 'Using Access Helper with Role ID',
                'role_name' => 'Using Access Helper with Role Name',
            ],

            'view_console_it_works' => 'View console, you should see \'it works!\' which is coming from FrontendController@index',
            'you_can_see_because' => 'You can see this because you have the role of \':role\'!',
            'you_can_see_because_permission' => 'You can see this because you have the permission of \':permission\'!',
        ],

        'user' => [
            'profile_updated' => 'Profile successfully updated.',
            'password_updated' => 'Password successfully updated.',
        ],
        'welcome_to' => 'Welcome to :place',
    ],


    'backend' => [
        'access' => [
            'users' => [
                'delete_user_confirm' => 'Are you sure you want to delete this user permanently? Anywhere in the application that references this user\'s id will most likely error. Proceed at your own risk. This can not be un-done.',
                'if_confirmed_off' => '(If confirmed is off)',
                'restore_user_confirm' => 'Restore this user to its original state?',
            ],
        ],

        'dashboard' => [
            'title' => 'Administrative Dashboard',
            'welcome' => 'Welcome',
        ],

        'general' => [
            'delete_item_confirm' => 'Are you sure you want to delete this item permanently? Anywhere in the application that references this item\'s id will most likely error. Proceed at your own risk. This can not be un-done.',
            'if_confirmed_off' => '(If confirmed is off)',
            'restore_item_confirm' => 'Restore this item to its original state?',
            'all_rights_reserved' => 'I keep my eyes always on the LORD. With him at my right hand, I will not be shaken. Psalms 16:8',
            'are_you_sure' => 'Are you sure?',
            'boilerplate_link' => 'Laravel 5 Boilerplate',
            'continue' => 'Continue',
            'member_since' => 'Member since',
            'minutes' => ' minutes',
            'search_placeholder' => 'Search...',
            'timeout' => 'You were automatically logged out for security reasons since you had no activity in ',

            'see_all' => [
                'messages' => 'See all messages',
                'notifications' => 'View all',
                'tasks' => 'View all tasks',
            ],

            'status' => [
                'online' => 'Online',
                'offline' => 'Offline',
            ],

            'you_have' => [
                'messages' => '{0} You don\'t have messages|{1} You have 1 message|[2,Inf] You have :number messages',
                'notifications' => '{0} You don\'t have notifications|{1} You have 1 notification|[2,Inf] You have :number notifications',
                'tasks' => '{0} You don\'t have tasks|{1} You have 1 task|[2,Inf] You have :number tasks',
            ],
        ],

		'search' => [
			'empty' => 'Please enter a search term.',
			'incomplete' => 'You must write your own search logic for this system.',
			'title' => 'Search Results',
			'results' => 'Search Results for :query',
		],
    ],

    'emails' => [
        'auth' => [
            'error' => 'Whoops!',
            'greeting' => 'Hi there,',
            'regards' => 'Kind regards,',
            'company_sign' => 'HandyHire is a registered South African company ONE INTERNET MEDIA CC t/a HandyHire (CK2008/162526/23). Address: Tyger Chambers, Building 5, Willie van Schoor Ave, Bellville, Cape Town.',
            'trouble_clicking_button' => 'If you’re having trouble clicking the ":action_text" button, copy and paste the URL below into your web browser:',
            'thank_you_for_using_app' => 'Thank you for using our application!',
            'disclaimer' => 'The information contained in this communication from the sender is confidential. It is intended solely for use by the recipient and others authorized to receive it. If you are not the recipient, you are hereby notified that any disclosure, copying, distribution or taking action in relation of the contents of this information is strictly prohibited and may be unlawful.',

            'password_reset_subject' => 'Reset Password',
            'password_cause_of_email' => 'You are receiving this email because we received a password reset request for your account.',
            'password_if_not_requested' => 'If you did not request a password reset, no further action is required.',
            'reset_password' => 'Click here to reset your password',

            'click_to_confirm' => 'Click here to confirm your email:',
        ],
    ],


];
