<div class="pull-right mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
           Accounts <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.account.index', 'All Accounts') }}</li>

        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>
