@extends ('backend.layouts.app')

@section('after-styles-end')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@stop

@section('page-header')
    <h1>
        Accounts Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Accounts</h3>

            <div class="box-tools pull-right">
                @include('backend.account.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="service-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Professional</th>
                        <th>Credits Remain</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop

@section('after-scripts-end')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

    <script>
        $(function() {
            $('#service-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.account.get") }}',
                    type: 'post',
                    data: {status: '{{ isset($status_filter) ?  $status_filter: 'null' }}', trashed: false}
                },
                columns: [
                    {data: 'id', name: 'account.id'},
                    {data: 'name', name: 'account.name', render: $.fn.dataTable.render.text()},
                    {data: 'credits_remain', name: 'account.credits_remain', render: $.fn.dataTable.render.text()},
                    {data: 'status_label', name: 'account.status_label'},
                    {data: 'created_at', name: 'account.created_at'},
                    {data: 'updated_at', name: 'account.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@stop