@extends ('backend.layouts.app')

@section ('title', 'Accounts | Show')

@section('page-header')
    <h1>
        Account Management
        <small>View Account</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $account->professional->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            {!! $account->status_label !!}



        </div><!-- /.box-body -->
    </div><!--box-->


@stop