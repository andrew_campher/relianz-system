@extends ('backend.layouts.app')

@section ('title', 'Service Category | Management')

@section('page-header')
    <h1>
        Service Category Management
        <small>Edit</small>
    </h1>
@endsection

@section('content')

    @if( isset($category) )
        {{ Form::model($category, ['route' => ['admin.category.update', $category], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.category.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif



        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Category</h3>

            </div><!-- /.box-header -->

            <div class="form-group">
                {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('tags', 'Tags', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::text('tags', null, ['class' => 'form-control', 'placeholder' => 'Tags must be a string']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('parent_id', 'Parent Category', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('parent_id', $categories, null, ['placeholder' => '-- No Parent --']) }}<br/>
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-1">
                    {{ Form::checkbox('status', '1', true) }}
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.category.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

        {{ Form::hidden('status', 1) }}

    {{ Form::close() }}
@stop

