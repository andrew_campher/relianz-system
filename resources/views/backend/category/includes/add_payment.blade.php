{{ Form::open(['route' => 'admin.package.store_payment', 'class' => '', 'role' => 'form', 'method' => 'post']) }}

    <div class="form-group">
        {{ Form::label('amount_paid', 'Amount Paid', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('amount_paid', $package->amount, ['class' => 'form-control', 'placeholder' => 'Amount paid', 'disabled']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('note', 'Note', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('note', null, ['class' => 'form-control', 'placeholder' => 'Enter note']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::submit('Submit', ['class' => 'btn btn-success btn-xs']) }}
    </div><!--pull-right-->


    {{ Form::hidden('user_id', $logged_in_user->id) }}
    {{ Form::hidden('package_id', $package->id) }}


{{ Form::close() }}
