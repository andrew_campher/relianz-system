<ul class="list-group">
@if(isset($categories) && $categories)
    @foreach( $categories[0] AS $level0 )

        <li class="list-group-item">{{$level0['title']}}

        @if(isset($categories[$level0['id']]))
            <ul class="list-group">
            @foreach($categories[$level0['id']] AS $level1)

                <li class="list-group-item">{{Form::checkbox('categories[]', $level1['id'])}} {{$level1['title']}}

                    @if(isset($categories[$level1['id']]))
                     <ul class="list-group">
                    @foreach($categories[$level1['id']] AS $level2)

                         <li class="list-group-item">{{Form::checkbox('categories[]', $level2['id'])}} {{$level2['title']}}

                             @if(isset($categories[$level2['id']]))
                                 <ul class="list-group">
                                     @foreach($categories[$level2['id']] AS $level3)

                                         <li class="list-group-item">{{Form::checkbox('categories[]', $level3['id'])}} {{$level3['title']}}</li>

                                     @endforeach
                                 </ul>
                             @endif
                         </li>

                    @endforeach
                     </ul>
                    @endif
                </li>
            @endforeach
            </ul>
        @endif

        </li>

    @endforeach
@endif
</ul>