@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Categories Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Categories</h3>

            <div class="box-tools pull-right">
                @include('backend.category.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.category.action') }}" method="post" class="">
                    <div id="category-table" class="table table-condensed table-hover">
                        <div class="row">
                            <div class="col-xs-12 col-sm-1"></div>
                            <div class="col-xs-12 col-sm-1">ID</div>
                            <div class="col-xs-12 col-sm-3">Title</div>
                            <div class="col-xs-12 col-sm-2">Slug</div>
                            <div class="col-xs-12 col-sm-2">Services</div>
                            <div class="col-xs-12 col-sm-1">Level</div>
                            <div class="col-xs-12 col-sm-1">Status</div>
                            <div class="col-xs-12 col-sm-1">Created At</div>
                            <div class="col-xs-12 col-sm-2"></div>
                        </div>

                            @foreach($categories[0] AS $lvl1)
                            <div class="row bg-success">
                                <div class="col-xs-12 col-sm-1"><input name="selected[]" type="checkbox" value="{{ $lvl1->id }}"></div>
                                <div class="col-xs-12 col-sm-1">{{ $lvl1->id }}</div>
                                <div class="col-xs-12 col-sm-3">{{ $lvl1->title }}</div>
                                <div class="col-xs-12 col-sm-2">{{ $lvl1->slug }}</div>
                                <div class="col-xs-12 col-sm-1">{{ $lvl1->level }}</div>
                                <div class="col-xs-12 col-sm-1">{!! $lvl1->status_label !!}</div>
                                <div class="col-xs-12 col-sm-1">{{ $lvl1->created_at }}</div>
                                <div class="col-xs-12 col-sm-2">{!! $lvl1->action_buttons !!}</div>
                            </div>
                                @if(isset($categories[$lvl1->id]))
                                <div style="margin-left: 40px;">
                                    @foreach($categories[$lvl1->id] AS $lvl2)
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-1"><input name="selected[]" type="checkbox" value="{{ $lvl2->id }}"></div>
                                            <div class="col-xs-12 col-sm-1">{{ $lvl2->id }}</div>
                                            <div class="col-xs-12 col-sm-3">{{ $lvl2->title }}</div>
                                            <div class="col-xs-12 col-sm-2">{{ $lvl2->slug }}</div>
                                            <div class="col-xs-12 col-sm-1"><a href="{{ route('admin.service.index', ['category_id='.$lvl2->id]) }}">Services</a></div>
                                            <div class="col-xs-12 col-sm-1">{{ $lvl2->level }}</div>
                                            <div class="col-xs-12 col-sm-1">{!! $lvl2->status_label !!}</div>
                                            <div class="col-xs-12 col-sm-1">{{ $lvl2->created_at }}</div>
                                            <div class="col-xs-12 col-sm-2">{!! $lvl2->action_buttons !!}</div>
                                        </div>
                                        @if(isset($categories[$lvl2->id]))
                                        @foreach($categories[$lvl2->id] AS $lvl3)
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-1"><input name="selected[]" type="checkbox" value="{{ $lvl3->id }}"></div>
                                                <div class="col-xs-12 col-sm-1">{{ $lvl3->id }}</div>
                                                <div class="col-xs-12 col-sm-3">{{ $lvl3->title }}</div>
                                                <div class="col-xs-12 col-sm-2">{{ $lvl3->slug }}</div>
                                                <div class="col-xs-12 col-sm-1"><a href="{{ route('admin.service.index', ['category_id='.$lvl3->id]) }}">Services</a></div>
                                                <div class="col-xs-12 col-sm-1">{{ $lvl3->level }}</div>
                                                <div class="col-xs-12 col-sm-1">{!! $lvl3->status_label !!}</div>
                                                <div class="col-xs-12 col-sm-1">{{ $lvl3->created_at }}</div>
                                                <div class="col-xs-12 col-sm-2">{!! $lvl3->action_buttons !!}</div>
                                            </div>
                                        @endforeach
                                        @endif
                                    @endforeach
                                </div>
                                @endif
                            @endforeach

                    </div>

                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
