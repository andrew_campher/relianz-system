@extends ('backend.layouts.app')

@section ('title', 'Questions | Show')

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>View Question</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $question->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>Description</th>
                    <td>{{ $question->description }}</td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td>{!! $question->status_label !!}</td>
                </tr>

                <tr>
                    <th>Created At</th>
                    <td>{{ $question->created_at }} ({{ $question->created_at->diffForHumans() }})</td>
                </tr>

                <tr>
                    <th>Last Update</th>
                    <td>{{ $question->updated_at }} ({{ $question->updated_at->diffForHumans() }})</td>
                </tr>

                @if ($question->trashed())
                    <tr>
                        <th>{{ trans('labels.backend.access.users.tabs.content.overview.deleted_at') }}</th>
                        <td>{{ $question->deleted_at }} ({{ $question->deleted_at->diffForHumans() }})</td>
                    </tr>
                @endif
            </table>

        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Question Options</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Special</th>
                </tr>

            </table>

        </div><!-- /.box-body -->
    </div><!--box-->
@stop