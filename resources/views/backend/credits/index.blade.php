@extends ('backend.layouts.app')

@section ('title', 'Credits')

@section('page-header')
    <h1>
        Credits Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-body">
            <div class="table-responsive">
                    <table id="item-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Customer</th>

                            <th>Department</th>

                            <th>Amount</th>

                            <th>Date</th>
                            <th>Invoice</th>

                        </tr>
                        </thead>
                        <tbody>
                            @foreach($credits AS $credit)

                                @php
                                    $credit->CreditFields = json_decode($credit->CustomFields, true);
                                @endphp

                            <tr class="">
                                <td><a href="{{ route('admin.credits.show', $credit->ID) }}">{{ $credit->CustomerName }}</a></td>

                                <td>
                                    @php
                                    $stat = explode(":", $credit->Memo);

                                    $label = 'label-default';

                                    if(isset($stat[1])) {
                                        switch (strtoupper(trim($stat[0]))) {
                                            case 'WAREHOUSE':
                                                $label = 'label-danger';
                                            break;
                                            case 'ORDERS':
                                                $label = 'label-warning';
                                            break;
                                            case 'CUSTOMER':
                                                $label = 'label-default';
                                            break;
                                            case 'PROCUREMENT':
                                                $label = 'label-primary';
                                            break;
                                            case 'SALES':
                                                $label = 'label-success';
                                            break;
                                            case 'QC':
                                                $label = 'label-success';
                                            break;
                                            case 'FINANCE':
                                                $label = 'label-default';
                                            break;
                                            case 'REP':
                                                $label = 'label-success';
                                            break;
                                        }

                                        echo '<span class="label '.$label.'">'.$stat[0].'</span> '.trim($stat[1]);
                                    } else{
                                        echo $credit->Memo;
                                    }
                                    @endphp
                                </td>

                                <td>{!! \Helper::formatPrice($credit->Amount) !!}</td>

                                <td>{!! $credit->Date !!}</td>

                                <td>{!! isset($credit->CreditFields['Other']) ?$credit->CreditFields['Other']:'' !!}</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                    <div class="pull-right">{{ $credits->appends($request::capture()->except('page'))->links() }}</div>

                    <div class="clearfix"></div>


            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
