@extends ('backend.layouts.app')

@section ('title', $credit->ReferenceNumber. ' Credit')

@section('page-header')
    <h1>
        <span class="label label-primary">{{ $credit->ReferenceNumber }}</span> Credit Note - {{ $credit->Amount }}
        <small>View Credit</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ $credit->CustomerName }}
            </h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-hover">
                        <tr>
                            <td colspan="2">
                                <div class="h4">Order Note</div>
                                {{ $credit->Memo }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">

                </div>
            </div>


        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Items</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">

                <tr>
                    <th>Product</th>

                    <th>Quantity</th>

                    <th>Price</th>
                    <th>SubTotal</th>
                </tr>
                @if($credit->items)
                    @foreach($credit->items AS $item)

                        <tr>
                            <td><a href="{{ route('admin.items.show', $item->ItemId) }}">{{ $item->ItemName }}</a></td>

                            <td><b>{{ $item->ItemQuantity }}</b></td>

                            <td>{!! \Helper::formatPrice($item->ItemRate) !!}</td>

                            <td>{!! \Helper::formatPrice($item->ItemRate*$item->ItemQuantity) !!}</td>

                        </tr>
                    @endforeach

                @endif

            </table>
            </div>

        </div><!-- /.box-body -->
    </div><!--box-->

@stop