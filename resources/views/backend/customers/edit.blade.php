@extends ('backend.layouts.app')

@section ('title', 'Service items | Management')

@section('page-header')
    <h1>
       {{ $customer->Name }}
        <small>Edit</small>
    </h1>
@endsection

@section('content')

    @if( isset($customer) )
        {{ Form::model($customer, ['route' => ['admin.customers.update', $customer], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.customers.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.items.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Customers</h3>

                <div class="pull-right">{{ $customer->ID }}</div>

            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    {{ Form::label('notes', 'Notes', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('data[notes]', null, ['class' => 'form-control', 'placeholder' => 'notes must be a string', 'style' => 'height:700px;']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

            </div>
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.items.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

