    <table class="table table-orders table-bordered table-striped table-hover" width="100%">
        <tr>
            <th>SO</th>

            <th>Del/Col</th>
            <th>Invoice</th>
            <th>PO</th>
            <th>Ship Date</th>

            <th>Status</th>


            <th>Weight</th>
            <th>Backorder</th>

            <th>User</th>


            <th>Invoiced</th>
            <th>Created</th>
            <th>Notes</th>

        </tr>
        @if(isset($orders) && $orders)
            @foreach($orders AS $order)
                <tr class="{{ $order->Terms == 'COD'?'bg-danger':'' }}">
                    <td><a href="{{ route('admin.orders.show', $order->SalesOrderID) }}"><b>{{ $order->SONumber }}</b></a></td>

                    <td>{{ $order->Type }}</td>
                    <td><b>{{ $order->InvoiceNo?$order->InvoiceNo:'' }}</b></td>
                    <td>{{ $order->PONumber }}</td>
                    <td>{{ date("d-m-Y",strtotime($order->ShipDate)) }}</td>

                    <td>
                        @include('backend.logistics.includes.status_show')
                    </td>

                    <td><b>{{ $order->Weight }}</b></td>
                    <td>{{ $order->kgweight }}</td>

                    <td>{{ $order->User }}</td>


                    <td>{{ $order->InvCreated }}</td>

                    <td>{{ $order->Created }}</td>
                    <td>{{ $order->Notes }}</td>

                </tr>
            @endforeach

        @endif
    </table>

