@extends ('backend.layouts.app')

@section ('title', 'Customers')

@section('page-header')
    <h1>
        Customers Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">

            <div class="col-md-4">
                <h2 class="box-title">Customers</h2>
            </div>
            <div class="col-md-8 text-right">
                <form class="form-inline" action="" method="GET">
                    <input class="form-control" name="search" placeholder="Search..." type="text" value="{{ isset($request->search) && $request->search ? $request->search : "" }}">
                    <input name="isactive" type="checkbox" value="1"> Incl DeActivated

                    <select name="rep" class="form-control inline">
                        <option value="">-- All Reps --</option>
                        @foreach($reps AS $rep)
                            <option {{ $request->rep == $rep->Initial?'selected':'' }} value="{{ $rep->Initial }}">{{ $rep->SalesRepEntityRef_FullName }}</option>
                        @endforeach
                    </select>

                    <select class="form-control" name="class">
                        <option value="">All Classes</option>
                        <option value="A" {{ $request->class == 'A'?'selected':'' }}>A - 80%</option>
                        <option value="B" {{ $request->class == 'B'?'selected':'' }}>B - 15%</option>
                        <option value="C" {{ $request->class == 'C'?'selected':'' }}>C - 5%</option>
                    </select>

                    <button class="form-control btn-primary" type="submit">GO</button>
                </form>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                    <table id="item-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Class</th>
                            <th>Name</th>
                            <th></th>

                            <th>Terms</th>
                            <th>Balance</th>
                            <th>Credit Limit</th>
                            <th>Phone</th>

                            <th>SalesRep</th>
                            <th>Area</th>
                            <th>COA</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($customers AS $customer)
                                @php
                                $customer->CustomerFields = json_decode($customer->CustomFields, true);
                                @endphp
                            <tr class="">
                                <td>{!! $customer->showCustomerClass() !!}</td>
                                <td><a href="{{ route('admin.customers.show', $customer->ID) }}">{{ $customer->Name }}</a></td>
                                <td>
                                    @if(access()->user()->hasRoles(['Sales','Procurement']))
                                    <a title="Insights" class="btn btn-primary btn-xs" href="{{ route('admin.customers.report', $customer->ID) }}"><i class="fa fa-bar-chart"></i></a>
                                    @endif
                                    @if(access()->user()->hasRoles(['Sales','Procurement','Orders']))
                                        <a title="BackOrders" class="btn btn-danger btn-xs" href="{{ route('admin.orders.showbo', $customer->ID) }}"><i class="fa fa-ambulance"></i></a>
                                    @endif

                                    @if(access()->user()->hasRoles(['Administrator', 'Sales']))
                                        <a class="btn btn-default btn-xs" href="{{ route('admin.customers.edit', $customer->ID) }}"><i class="fa fa-pencil"></i></a>
                                    @endif
                                </td>

                                <td>{{ $customer->Terms }}</td>
                                <td class="{{ $customer->CreditLimit<$customer->Balance?'bg-danger':'' }}">{!! \Helper::formatPrice($customer->Balance) !!}</td>
                                <td>{!! \Helper::formatPrice($customer->CreditLimit) !!}</td>

                                <td>{{ $customer->Phone }}</td>

                                <td>{{ $customer->SalesRep }}</td>
                                <td>{{ isset($customer->CustomerFields['DeliveryArea']) ?  $customer->CustomerFields['DeliveryArea'] : "" }}</td>
                                <td>{{ isset($customer->CustomerFields['COA']) ? $customer->CustomerFields['COA'] : "" }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                    <div class="pull-right">{{ $customers->appends($request::capture()->except('page'))->links() }}</div>

                    <div class="clearfix"></div>


            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
