@extends ('backend.layouts.app')

@section ('title', $customer->Name.' | Show')

@section('page-header')
    <h1>
        Customers Management
        <small>View Customer</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{!! $customer->showCustomerClass() !!} {{ $customer->Name }}
                @if(access()->user()->hasRoles(['Sales','Procurement']))
                <a class="btn btn-primary btn-sm" href="{{ route('admin.customers.report', $customer->ID) }}"><i class="fa fa-bar-chart"></i></a>
                @endif
                @if(access()->user()->hasRoles(['Sales','Procurement','Orders']))
                     <a title="BackOrders" class="btn btn-danger btn-sm" href="{{ route('admin.orders.showbo', $customer->ID) }}"><i class="fa fa-ambulance"></i></a>
                @endif
                @if(access()->user()->hasRoles(['Administrator', 'Sales']))
                    <a class="btn btn-default btn-sm" href="{{ route('admin.customers.edit', $customer->ID) }}"><i class="fa fa-pencil"></i></a>
                @endif
            </h3>

            <div class="pull-right">
                {!! $customer->ID !!}
            </div>

        </div><!-- /.box-header -->

        @php
            $customer->CustomFields = json_decode($customer->CustomFields);
        @endphp

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-hover">

                        <tr>
                            <th>Phone</th>
                            <td>{{ $customer->Phone }}</td>
                        </tr>

                        <tr>
                            <th>Rep</th>
                            <td>{!! $customer->SalesRep !!}</td>
                        </tr>
                        <tr>
                            <th>Billing Address</th>
                            <td>{!! $customer->BillingAddress !!}</td>
                        </tr>
                        <tr>
                            <th>Shipping Address</th>
                            <td>{!! $customer->ShippingAddress !!}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-hover">

                        @if(isset($customer->CustomFields->DeliveryArea))
                        <tr>
                            <th>Delivery Code</th>
                            <td>{{ $customer->CustomFields->DeliveryArea }}</td>
                        </tr>
                        @endif

                        @if(isset($customer->CustomFields->COA))
                        <tr>
                            <th>COA</th>
                            <td>{{ $customer->CustomFields->COA }}</td>
                        </tr>
                        @endif



                    </table>
                </div>
            </div>


        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Notes</h3>

            <div class="pull-right">
            @if(access()->user()->hasRoles(['Administrator', 'Sales']))
                <a class="pull-right btn btn-default btn-sm" href="{{ route('admin.customers.edit', $customer->ID) }}"><i class="fa fa-pencil"></i></a>
            @endif
            </div>
        </div>
        <div class="box-body">
            {{ $customer->Notes }}
        </div>
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Orders</h3>
        </div>
        <div class="box-body">
            @if(isset($orders) && $orders)

                @include('backend.customers.includes.past_orders')

            @endif
        </div>
    </div>
@stop