@extends('backend.layouts.app')

@section ('title', 'Dashboard')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')

    @if(access()->user()->hasRoles(['Sales', 'Procurement','Logistics','Warehouse', 'Orders']))
        <div class="row">
            <div class="col-lg-6">
                @include('backend.dashboard.includes.credits_tracking')

                @include('backend.dashboard.includes.ship_tracking')
            </div>
            <div class="col-lg-6">
                @include('backend.dashboard.includes.latest_nostocks')
            </div>
        </div>
    @endif

    @if(access()->user()->hasRoles(['Sales', 'Procurement']))
    <div class="row">
        <div class="col-lg-6">
            @include('backend.dashboard.includes.item_sales_increase')
        </div>
        <div class="col-lg-6">
            @include('backend.dashboard.includes.item_sales_decrease')
        </div>
    </div>
    @endif




    @if(access()->user()->hasRoles(['Administrator']))
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                {!! history()->render() !!}
            </div><!-- /.box-body -->
        </div><!--box box-success-->
    @endif


@endsection

@section('after-scripts-end')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
@append