<div class="box box-warning" id="credits_track">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-money"></i> Credits - Past 30 Days</h3>
        <div class="pull-right">
            <span id="credits_TotalCount"></span> @ <span id="credits_Total" class="text-bold"></span>
        </div>
    </div>
    <div class="box-body">
        <div class="row" id="widgetCredits">

        </div>
    </div>
</div>


@section('after-scripts-end')
    <script>
        $.ajax({
            url: "{{ route("api.widgets.credits_tracking") }}",
            type: "get",
            dataType: 'json',
            success: function(d) {
                if (d) {
                    totalCredits = 'R' + d.totals[0].totalCredits.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                    $('#credits_Total').html(totalCredits);
                    $('#credits_TotalCount').html(d.totals[0].totalCountCredits);

                    $.each(d.departments, function(k, v) {
                        totalCredits = 'R' + v.totalCredits.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                        $('#widgetCredits').append('<div class="col-md-3 text-center bg-gray-light margin-bottom"><div class="text-bold"><a href="{{ route('admin.credits.index') }}?dept='+v.Department+'">'+v.Department+'</a></div><div class="h1 no-margin">'+v.CreditCount+'</div><div>'+totalCredits+'</div></div>');
                    });
                }
            }
        });
    </script>
@append