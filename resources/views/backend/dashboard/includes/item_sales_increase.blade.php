<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-arrow-up text-green"></i> Top Item Increases (Kg's)</h3>
    </div>
    <div class="box-body">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Product</th>
                <th>Curr Sales</th>
                <th>Last Mnth</th>
                <th>Last Year</th>
                <th>Value</th>
                <th>Qty Diff</th>
                <th>% Diff</th>
            </tr>
        </thead>
        <tbody id="item_increase_t">

        </tbody>
    </table>
    </div>
</div>


@section('after-scripts-end')
    <script>
        $.ajax({
            url: "{{ route("api.widgets.item_sales_increase") }}",
            type: "get",
            dataType: 'json',
            success: function(d) {
                $.each(d.data, function(k, v) {
                    value = 'R' + v.ValueDiff.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    perc = parseFloat(v.PercDiff).toFixed(2);

                    $(item_increase_t).append('<tr><td><a href="{{ route('admin.items.index') }}?search='+v.BaseName+'">'+v.BaseName+'</a></td><td><b>'+parseFloat(v.CurrrentQty).toFixed(0)+'</b></td><td>'+parseFloat(v.LastMonthQty).toFixed(0)+'</td><td>'+parseFloat(v.PastPeriodQty).toFixed(0)+'</td><td>'+value+'</td><td><i class="fa fa-arrow-up text-green"></i> '+parseFloat(v.QtyDiff).toFixed(0)+'</td><td>'+perc+'%</td></tr>');
                });

            }
        });
    </script>
@append