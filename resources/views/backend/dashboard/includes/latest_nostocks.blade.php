<div class="box box-warning" id="latestns">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-warning"></i> Latest NS pulled</h3>
    </div>
    <div class="box-body">
    <table id="lastest_nostocks" class="table table-condensed small table-striped">
        <thead>
        <tr>
            <th>Date</th>
            <th>Name</th>
            <th>Qty On HAND</th>
            <th>NS Qty</th>
            <th>Value</th>
            <th>Orders</th>
        </tr>
        </thead>
        <tbody ></tbody>
    </table>
    </div>
</div>


@section('after-scripts-end')
    <script>
        $.ajax({
            url: "{{ route("api.widgets.get_latest_nostocks") }}",
            type: "get",
            dataType: 'json',
            success: function(d) {
                if (d) {
                    $.each(d.data, function(k, v) {
                        SaleValue = 'R' + v.SaleValue.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                        var sos = v.RefNUmber.split(',');

                        var soshtml = "";

                        $.each(sos, function(k, v) {
                            soshtml += ' <a href="{{ route('admin.orders.index') }}/showso/'+v+'">'+v+'</a>';
                        })

                        $('#lastest_nostocks tbody').append('<tr><td nowrap>'+v.Date+'</td><td><a href="{{ route('admin.items.index') }}/report/'+v.ID+'"><b>'+v.Name+'</b></a></td><td><b>'+parseFloat(v.QtyOnHand).toFixed(0)+'</b></td><td>'+parseFloat(v.NSQty).toFixed(0)+'</td><td>'+SaleValue+'</td><td>'+soshtml+'</td></tr>');
                    });
                }
            }
        });
    </script>
@append