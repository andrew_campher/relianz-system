<div class="box box-warning" id="latestns">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-truck"></i> Shipped Ton's</h3>

        <div class="pull-right" id="busyStat"></div>
    </div>
    <div class="box-body">
        <canvas id="shiptrackingDiv" width="100%" height="200px"></canvas>
    </div>
</div>


@section('after-scripts-end')

    <script>

        function drawLineChart() {

            var jsonData = $.ajax({
                url: '{{ route("api.widgets.ship_tracking") }}',
                dataType: 'json',
            }).done(function (d) {

                diff = parseFloat(d.diff);

                if(d.diff>0) {
                    $('#busyStat').html('<i class="fa fa-arrow-up text-success"></i> UP '+diff.toFixed(2)+'t');
                } else {
                    $('#busyStat').html('<i class="fa fa-arrow-down text-danger"></i> DOWN '+diff.toFixed(2)+'t');
                }

                var chartdata = {
                    labels : d.AxisLabels,
                    datasets: [{
                        label: 'Past Year',
                        fill: false,
                        data: d.data,
                        borderWidth: 2
                    },{
                        label: 'This Year',
                        fill: true,
                        backgroundColor: "blue",
                        borderColor: "blue",
                        data: [d.thismonth],
                        borderWidth: 4
                    }]
                };

                var ctx = document.getElementById("shiptrackingDiv");
                var myLineChart = new Chart(ctx, {
                    type: 'bar',
                    options: {
                        tooltips: {
                            callbacks: {
                                afterLabel: function(tooltipItem, data) {
                                    var dataset = data['datasets'][0];
                                    var avg = Math.round(dataset['data'][tooltipItem['index']] / 20)
                                    return 'Daily Avg: ' + avg + 't';
                                }
                            },
                            backgroundColor: '#FFF',
                            titleFontSize: 16,
                            titleFontColor: '#0066ff',
                            bodyFontColor: '#000',
                            bodyFontSize: 14,
                            displayColors: false
                        },
                        maintainAspectRatio: false,
                        responsive: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    },
                    data: chartdata
                });
            });
        }

        drawLineChart();

    </script>
@append