<header class="main-header">

    <a href="{{ route('admin.dashboard') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
           {{ substr(app_name(), 0, 1) }}
        </span>

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            {{ app_name() }}
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
        </a>

        <div class="navbar-custom-menu">


            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-search"></i>
                    </a>

                    <ul class="dropdown-menu">
                        <li style="line-height: 25px;padding:8px 10px 0 0">
                            <form action="{{ route('admin.orders.index') }}" method="get" class="form-inline">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-shopping-cart"></i></div>
                                        <input name="search" class="form-control" style="width: 120px" value="" placeholder="Sales Order..." />
                                        <div class="input-group-addon"><button class="btn btn-info btn-sm" type="submit">GO</button></div>
                                    </div>
                                </div>

                            </form>
                        </li>
                        <li style="line-height: 25px;padding:8px 10px 0 0">
                            <form action="{{ route('admin.items.index') }}" method="get" class="form-inline">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-list"></i></div>
                                        <input name="search" class="form-control" style="width: 120px" value="" placeholder="Items..." />
                                        <div class="input-group-addon"><button class="btn btn-info btn-sm" type="submit">GO</button></div>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li style="line-height: 25px;padding:8px 10px 0 0">
                            <form action="{{ route('admin.customers.index') }}" method="get" class="form-inline">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                        <input name="search" class="form-control" style="width: 120px" value="" placeholder="Customers..." />
                                        <div class="input-group-addon"><button class="btn btn-info btn-sm" type="submit">GO</button></div>
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li><!-- /.notifications-menu -->

                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-default">0</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="header">{{ trans_choice('strings.backend.general.you_have.notifications', 0) }}</li>
                        <li class="footer">
                            {{ link_to('#', trans('strings.backend.general.see_all.notifications')) }}
                        </li>
                    </ul>
                </li><!-- /.notifications-menu -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {!! access()->user()->getImageHtml('icon', 'avatar-sm img-circle user-image') !!}
                        <span class="hidden-xs">{{ access()->user()->name }}</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ access()->user()->thumb_url }}" class="img-circle" alt="User Avatar" />
                            <p>
                                {{-- access()->user()->name }} - {{ implode(", ", access()->user()->roles->lists('name')->toArray()) --}}
                                <small>{{ trans('strings.backend.general.member_since') }} {{ access()->user()->created_at->format("m/d/Y") }}</small>
                            </p>
                        </li>

                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                {{ link_to('#', 'Link') }}
                            </div>
                            <div class="col-xs-4 text-center">
                                {{ link_to('#', 'Link') }}
                            </div>
                            <div class="col-xs-4 text-center">
                                {{ link_to('#', 'Link') }}
                            </div>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{!! route('public.index') !!}" class="btn btn-default btn-flat">
                                    <i class="fa fa-home"></i>
                                    {{ trans('navs.general.home') }}
                                </a>
                            </div>
                            <div class="pull-right">
                                <a href="{!! route('frontend.auth.logout') !!}" class="btn btn-danger btn-flat">
                                    <i class="fa fa-sign-out"></i>
                                    {{ trans('navs.general.logout') }}
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-custom-menu -->
    </nav>
</header>
