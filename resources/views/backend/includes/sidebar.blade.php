<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ Active::pattern('admin/dashboard') }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            @if(access()->user()->hasRoles(['Sales','Orders','Procurement','Finance']))
                <li class="{{ Active::pattern('admin/tickets') }}">
                    <a href="{{ route('admin.tickets') }}">
                        <i class="fa fa-ticket"></i>
                        <span>Tickets</span>
                    </a>
                </li>
            @endif

            @if(access()->user()->hasRoles(['Sales','Procurement','Logistics','Finance', 'Orders']))


                <li class="{{ Active::pattern('admin/logistics*') }} treeview">
                    <a href="#">
                        <i class="fa fa-truck"></i>
                        <span>Order Fulfillment</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu {{ Active::pattern('admin/logistics*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/logistics*', 'display: block;') }}">
                        <li class="{{ Active::pattern('admin/logistics') }}">
                            <a href="{{ route('admin.logistics') }}">
                                <i class="fa fa-truck"></i>
                                <span>Logistics</span>
                            </a>
                        </li>

                        <li class="{{ Active::pattern('admin/logistics/collections') }}">
                            <a href="{{ route('admin.logistics.collections') }}">
                                <i class="fa fa-truck"></i>
                                <span>Collections</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            <li class="{{ Active::pattern('admin/orders') }}">
                <a href="{{ route('admin.orders.index') }}">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Sales Orders</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/items') }}">
                <a href="{{ route('admin.items.index') }}">
                    <i class="fa fa-cubes"></i>
                    <span>All Items</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/customers') }}">
                <a href="{{ route('admin.customers.index') }}">
                    <i class="fa fa-users"></i>
                    <span>Customers</span>
                </a>
            </li>

            @if(access()->user()->hasRoles(['Procurement', 'Finance']))
            <li class="{{ Active::pattern('admin/suppliers') }}">
                <a href="{{ route('admin.suppliers.index') }}">
                    <i class="fa fa-industry"></i>
                    <span>Suppliers</span>
                </a>
            </li>
            @endif

            @if(access()->user()->hasRoles(['Procurement', 'Finance', 'Sales', 'Warehouse', 'Logistics']))
                <li class="{{ Active::pattern('admin/credits') }}">
                    <a href="{{ route('admin.credits.index') }}">
                        <i class="fa fa-frown-o"></i>
                        <span>Credits</span>
                    </a>
                </li>
            @endif

            @if(access()->user()->hasRole('Pricing'))
                <li class="{{ Active::pattern('admin/pricing*') }} treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Pricing</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu {{ Active::pattern('admin/pricing*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/pricing*', 'display: block;') }}">
                        <li class="{{ Active::pattern('admin/pricing') }}">
                            <a href="{{ route('admin.pricing') }}">
                                <i class="fa fa-money"></i>
                                <span>Manage Pricing</span>
                            </a>
                        </li>

                        <li class="{{ Active::pattern('admin/pricing*') }}">
                            <a href="{{ route('admin.pricing.specials') }}">
                                <i class="fa fa-star"></i>
                                <span>Specials</span>
                            </a>
                        </li>
                    </ul>
                </li>

            @endif


            @if(access()->user()->hasRoles(['Sales', 'Procurement', 'Accounts']))
                <li class="{{ Active::pattern('admin/reports*') }} treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart"></i>
                        <span>Analytics</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu {{ Active::pattern('admin/reports*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/reports*', 'display: block;') }}">
                        @if(access()->user()->hasRole('Sales'))
                        <li class="{{ Active::pattern('admin/reports/monthtodate') }}">
                            <a href="{{ route('admin.reports.monthtodate') }}">
                                <i class="fa fa-table"></i>
                                <span>Month To Date</span>
                            </a>
                        </li>
                        @endif

                        @if(access()->user()->hasRoles(['Sales', 'Procurement', 'Accounts']))
                        <li class="{{ Active::pattern('admin/reports/customers') }}">
                            <a href="{{ route('admin.reports.customers') }}">
                                <i class="fa fa-table"></i>
                                <span>Customers Report</span>
                            </a>
                        </li>

                        <li class="{{ Active::pattern('admin/reports/items') }}">
                            <a href="{{ route('admin.reports.items') }}">
                                <i class="fa fa-table"></i>
                                <span>Items Report</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
            @endif


            <li class="{{ Active::pattern('admin/reports*') }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Reports</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/reports*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/reports*', 'display: block;') }}">
                    @if(access()->user()->hasRole('Sales'))
                    <li class="{{ Active::pattern('admin/reports/sales') }}">
                        <a href="{{ route('admin.reports.sales') }}">
                            <i class="fa fa-table"></i>
                            <span>Sales Report</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>


            @if(access()->user()->hasRoles(['Calc','Sales', 'Orders', 'Procurement', 'Logistics']))
                <li class="header">Tools</li>

                <li class="{{ Active::pattern('admin/items/nostocks') }}">
                    <a href="{{ route('admin.items.nostocks') }}">
                        <i class="fa fa-battery-empty"></i>
                        <span>No Stocks</span>
                    </a>
                </li>

                @if(access()->user()->hasRoles(['Sales', 'Procurement']))
                <li class="{{ Active::pattern('admin/tools/dropoffs') }}">
                    <a href="{{ route('admin.tools.dropoffs') }}">
                        <i class="fa fa-level-down"></i>
                        <span>Item DropOffs</span>
                    </a>
                </li>
                @endif

                <li class="{{ Active::pattern('admin/orders/backorders') }}">
                    <a href="{{ route('admin.orders.backorders', ['instock' => 1]) }}">
                        <i class="fa fa-ambulance"></i>
                        <span>Back Orders</span>
                    </a>
                </li>

                @if(access()->user()->hasRoles(['Sales', 'Procurement']))
                    <li class="{{ Active::pattern('admin/tools/sittingstock') }}">
                        <a href="{{ route('admin.tools.sittingstock') }}">
                            <i class="fa fa-lightbulb-o"></i>
                            <span>Sitting Stock</span>
                        </a>
                    </li>
                @endif

                @if(access()->user()->hasRoles(['Sales', 'Procurement', 'Calc']))
                {{--<li class="{{ Active::pattern('admin/calc') }}">
                    <a href="{{ route('admin.calc') }}">
                        <i class="fa fa-calculator"></i>
                        <span>GP Calculator</span>
                    </a>
                </li>--}}
                @endif
            @endif

            @if(access()->user()->hasRole('Administrator'))
            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>

            @permission('manage-users')
            <li class="{{ Active::pattern('admin/access/*') }}">
                <a href="{{ route('admin.access.user.index') }}">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                </a>
            </li>
            @endauth

                <li class="{{ Active::pattern('admin/links*') }}">
                    <a href="{{ route('admin.links.index') }}">
                        <i class="fa fa-link"></i>
                        <span>Links</span>
                    </a>
                </li>


            <li class="{{ Active::pattern('admin/log-viewer*') }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/log-viewer*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{{ route('admin.log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
