@extends ('backend.layouts.app')

@section ('title', 'Invoice | Manager')

@section('page-header')
    <h1>
        Invoices Management
        <small>Edit</small>
    </h1>
@endsection

@section('content')
    @if( isset($invoice) )
        {{ Form::model($invoice, ['route' => ['admin.invoice.update', $invoice], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.invoice.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Invoice</h3>

            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    {{ Form::label('user_id', 'User', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('user_id', $users, null, ['placeholder' => '-- Select Owner --', 'class' => 'form-control']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('professional_id', 'Professional', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('professional_id', $professionals->prepend('-- None --'), null, ['class' => 'form-control']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('service_id', 'Service', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('service_id', $services, null, ['placeholder' => '-- Select Service --', 'class' => 'form-control']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('description', 'Description', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'description required']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->


                <div class="form-group">
                    {{ Form::label('files', 'Photos', ['class' => 'col-lg-2 control-label']) }}

                    <span id="filei">
                    {{Form::file('file[]', ['class'=>'fileinput', 'accept' => 'image/*', 'onchange'=>'loadFile(event)' ])}}
                    </span>
                    <img width="200px" id="output"/>
                    <a onclick="addfileinput()" href="javascript:void(0)"><i class="fa fa-plus-square"></i></a>
                    <script>
                        var loadFile = function(event) {
                            var output = document.getElementById('output');
                            output.src = '';
                            output.src = URL.createObjectURL(event.target.files[0])
                        };

                        function addfileinput() {
                            $('#filei').after($('#filei').html());
                        }
                    </script>

                </div><!--form control-->


                <div class="form-group">
                    {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-1">
                        {{ Form::checkbox('status', '1', true) }}
                    </div><!--col-lg-1-->
                </div><!--form control-->

            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.question.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

