{{ Form::open(['route' => 'admin.invoice.store_payment', 'class' => '', 'role' => 'form', 'method' => 'post']) }}

    <div class="form-group">
        {{ Form::label('amount_paid', 'Amount Paid', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('amount_paid', $invoice->amount, ['class' => 'form-control', 'placeholder' => 'Amount paid', 'readonly']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('credit_amount', 'Credits Purchased', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('credit_amount', $invoice->credit_quantity, ['class' => 'form-control', 'placeholder' => 'Amount paid', 'readonly']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('note', 'Note', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('note', null, ['class' => 'form-control', 'placeholder' => 'Enter note']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::submit('Submit', ['class' => 'btn btn-success btn-xs']) }}
    </div><!--pull-right-->


    {{ Form::hidden('user_id', $logged_in_user->id) }}
    {{ Form::hidden('invoice_id', $invoice->id) }}


{{ Form::close() }}
