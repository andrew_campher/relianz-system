<div class="pull-right mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
           Invoices <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.invoice.index', 'All Invoices') }}</li>

            @permission('manage-users')
                <li>{{ link_to_route('admin.invoice.create', 'Create Invoice') }}</li>
            @endauth

            <li class="divider"></li>
            <li>{{ link_to_route('admin.invoice.unpaid', 'Unpaid Invoices') }}</li>
            <li>{{ link_to_route('admin.invoice.paid', 'Paid Invoices') }}</li>
            <li>{{ link_to_route('admin.invoice.cancelled', 'Cancelled Invoices') }}</li>
        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>
