@extends ('backend.layouts.app')

@section ('title', 'Invoices | Show')

@section('page-header')
    <h1>
        Invoice Management
        <small>View Invoice</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $invoice->professional->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            {!! $invoice->status_label !!}

            @include('frontend.invoice.includes.invoice_template')

        </div><!-- /.box-body -->
    </div><!--box-->

    @if($invoice->status == 0)
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add Payment</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            @include('backend.invoice.includes.add_payment')

        </div><!-- /.box-body -->
    </div><!--box-->
    @endif

@stop