@extends ('backend.layouts.app')

@section ('title', 'Edit | Items')

@section('page-header')
    <h1>
       {{ $item->Description }}
        <small>Edit</small>
    </h1>
@endsection

@section('content')

    @if( isset($item) )
        {{ Form::model($item, ['route' => ['admin.items.update', $item], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.items.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit items</h3>

                <div class="pull-right">{{ $item->ID }}</div>

            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    {{ Form::label('pricelist', 'Price List', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('items_data[pricelist]', null, ['class' => 'form-control', 'placeholder' => 'pricelist must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('stock_type', 'Stock Type', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('items_data[stock_type]', [0 => 'None', 1 => 'Stocked', 2 => 'On Request'], (isset($item->items_data->stock_type)?$item->items_data->stock_type:0), ['class' => 'form-control','placeholder' => '-- None --']) }}<br/>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.items.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

