@extends ('backend.layouts.app')

@section ('title', 'Items')

@section('page-header')
    <h1>
        Items Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">

            <div class="col-md-6">
                <h2 class="box-title">Items</h2>
            </div>
            <div class="col-md-6 text-right">
                <form action="" method="GET">
                    <input name="search" type="text" value="{{ isset($request->search) && $request->search ? $request->search : "" }}"> <input name="isactive" type="checkbox" value="1"> Incl DeActivated
                    <button type="submit">GO</button>
                </form>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                    <table id="item-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Description</th>
                            <th></th>
                            @if(access()->user()->hasRoles(['Administrator']))
                            <th></th>
                            @endif
                            <th>Stock Type</th>
                            <th>Qty On Hand</th>
                            <th>Price</th>
                            <th>Price per kg/unit</th>
                            @if(access()->user()->hasRoles(['Sales','Procurement','Logistics','Finance']))
                                <th>GP</th>
                                <th>Cost</th>
                            @endif
                            <th>On Order</th>
                            <th>On SO</th>
                            @if(access()->user()->hasRoles(['Procurement','Finance']))
                                <th>Supplier</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($items AS $item)
                                @php
                                    $customfields = json_decode($item->CustomFields);
                                @endphp
                            <tr>
                                <td>{!! $item->showClass() !!}</td>
                                <td><a href="{{ route('admin.items.show', $item->ID) }}">{{ $item->Description }}</a></td>
                                <td>
                                    @if(access()->user()->hasRoles(['Sales','Procurement']))
                                    <a class="btn btn-primary btn-xs" href="{{ route('admin.items.report', $item->ID) }}"><i class="fa fa-bar-chart"></i></a>
                                    @endif
                                </td>

                                @if(access()->user()->hasRoles(['Administrator']))
                                <td>
                                    <a class="" href="{{ route('admin.items.edit', $item->ID) }}"><i class="fa fa-pencil"></i></a>
                                </td>
                                @endif
                                <td>
                                    {!! $item->showStockType() !!}
                                </td>
                                <td class="text-center">{{ $item->QuantityOnHand }}</td>
                                <td class="text-bold">{!! \Helper::formatPrice($item->Price) !!}</td>
                                <td class="text-bold">{!! isset($customfields->KgWeight) && $customfields->KgWeight?\Helper::formatPrice($item->Price/$customfields->KgWeight):'' !!}</td>
                                @if(access()->user()->hasRoles(['Sales','Procurement','Logistics','Finance']))
                                    <td>{{ $item->Price>0 ?  number_format(($item->Price-$item->AverageCost)/$item->Price*100,2 )."%" : "" }}</td>
                                    <td>{!! \Helper::formatPrice($item->PurchaseCost) !!}</td>
                                @endif
                                <td>{{ $item->QuantityOnOrder }}</td>
                                <td>{{ $item->QuantityOnSalesOrder }}</td>
                                <td>@if(access()->user()->hasRoles(['Sales','Procurement','Finance']))
                                        <a class="text-black" href="{{ route('admin.suppliers.report', $item->PreferredVendorId) }}">{{ $item->PreferredVendor }}</a>
                                    @else
                                        {{ $item->PreferredVendor }}
                                    @endif

                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                <div class="pull-right">{{ $items->appends($request::capture()->except('page'))->links() }}</div>

                    <div class="clearfix"></div>


            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
