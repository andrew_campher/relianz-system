@extends('backend.layouts.app')

@section ('title', 'No Stocks - Items')

@section('page-header')
    <div class="row">
        <div class="col-md-6">
            <h2 class="no-margin">No Stock</h2>
        </div>
    </div>
@endsection

@section('content')

    <div class="box box-primary">

        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive bg-white">
                        <table id="nostocks" class="table table-bordered table-striped table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Description</th>
                                    <th>Qty on Hand</th>
                                    <th>First BackOrder</th>
                                    <th nowrap>First PO</th>
                                    <th>BackOrder</th>
                                    <th>On Order</th>
                                    <th width="100px" class="h3">ETA</th>
                                    <th>What to tell customer</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($items AS $item)
                                    <tr>
                                        <td>
                                            <?php
                                            if (isset($item->Class) && $item->Class) {
                                                switch ($item->Class) {
                                                    case "A":
                                                        echo '<span class="label label-success label-sm">A</span>';
                                                        break;
                                                    case "B":
                                                        echo '<span class="label label-warning label-sm">B</span>';
                                                        break;
                                                    case "C":
                                                        echo '<span class="label label-danger label-sm">C</span>';
                                                        break;
                                                }
                                            }

                                            ?>
                                        </td>
                                        <td>{{ substr( $item->Description, ( $pos = strpos( $item->Description, ':' ) ) === false ? 0 : $pos + 1 )  }}</td>
                                        <td class="{{ $item->QuantityOnHand == 0?'bg-danger':'' }} text-center">{{ $item->QuantityOnHand }}</td>
                                        <td class="text-center">{{ $item->FirstBackOrder?date("d-m-Y", strtotime($item->FirstBackOrder)):"" }}</td>
                                        <td class="text-center" nowrap>{{ $item->FirstOpenPO?date("d-m-Y", strtotime($item->FirstOpenPO)):"" }}</td>
                                        <td align="center" class="text-bold {{ $item->BackOrderQty?'bg-success':'' }}">{{ $item->BackOrderQty }}</td>
                                        <td align="center" class="text-bold {{ $item->QuantityOnOrder > 0?'bg-info':'bg-danger' }}">{{ $item->QuantityOnOrder }}</td>
                                        <td class="text-bold text-center @php
                                            if ($item->ETA && strtotime(str_replace("/","-",$item->ETA)) < strtotime(date('Y-m-d'))) {
                                                echo 'btn-danger';
                                            } elseif (strtotime(str_replace("/","-",$item->ETA)) == strtotime(date('Y-m-d'))) {
                                                echo 'btn-success';
                                            } elseif (strtotime(str_replace("/","-",$item->ETA)) > strtotime(date('Y-m-d'))) {
                                                echo 'bg-warning';
                                            }
                                                @endphp">
                                            {{ $item->ETA?date("d-m-Y",strtotime(str_replace("/","-",$item->ETA))):"" }}
                                        </td>
                                        <td contenteditable="true" data-itemid="{{ $item->ItemId }}" class="changeable
                                         <?php
                                            if(!$item->QuantityOnOrder && !$item->procurement_notes) {
                                                echo 'bg-danger';
                                            }
                                         ?>
                                         ">{{ $item->procurement_notes }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts-end')

    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#nostocks').DataTable({
                'paging'      : false,
                'searching'   : true,
                'ordering'    : true,
                'order'       : [[1,'asc']],
                'info'        : false,
                'responsive' : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ]
            })
        })
    </script>

    @if(access()->user()->hasRoles(['Procurement']))
    <script>
        var contents = '';
        $('.changeable').focus(function() {
            contents = $(this).html();
        }).blur(function() {
            e = $(this);
            if (contents.trim() != e.html().trim()){

                contents = e.html();
                $.ajax({
                    url: "{{ route("api.item.update") }}",
                    type: "post",
                    data: {
                        "ItemId": e.attr('data-itemid'),
                        "name": 'procurement_notes',
                        "value": e.html()
                    },
                    beforeSend: function() {
                        e.css({'border-color':'red'});
                    },
                    success: function(d) {
                        e.css({'border-color':''});
                    },
                    error: function(d) {

                    }
                });

            }
        });
    </script>
    @endif
@append