@extends ('backend.layouts.app')

@section ('title', $item->Description.' - Analytics Report')

@section('page-header')
    <h1>
        Item Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i><span>{{ trans('menus.backend.sidebar.dashboard') }}</span></a></li>
        <li><a href="{{ route('admin.items.index') }}"><i class="fa fa-cubes"></i><span>Items</span></a></li>
        <li class="active">{{ $item->Description }}</li>
    </ol>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{!! $item->showClass() !!} {{ $item->Description }} - {!! strpos($item->FullName, " CP") ? '<span class="label label-danger"><i class="fa fa-dropbox"></i> DOWNPACKED</span>' : '' !!} {!! $item->showStockType() !!}
                @if(access()->user()->hasRoles(['Administrator']))
                    <a target="_blank" class="" href="{{ route('admin.items.edit', $item->ID) }}"><i class="fa fa-pencil"></i></a>
                @endif
            </h3>
            <div>Supplier:
                @if(access()->user()->hasRoles(['Sales','Procurement']))
                    <a class="btn btn-primary" href="{{ route('admin.suppliers.report', $item->PreferredVendorId) }}"><i class="fa fa-bar-chart"></i> {{ $item->PreferredVendor }}</a>
                @else
                    {{ $item->PreferredVendor }}
                @endif
            </div>
            <div class="pull-right">{{ $item->ID }}</div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive table-striped">
                        <tr>
                            <td>Name</td>
                            <td>Description</td>
                            <td>Stock on Hand</td>
                            <td>On SO</td>
                            <td>On Order</td>
                            <td>Price</td>
                            <td>GP %</td>
                            <td>Avg Cost</td>
                            <td>Cost</td>

                        </tr>
                        <tr>
                            <td>{!! $item->FullName !!}</td>
                            <td>{{ $item->Description }}</td>
                            <td class="text-center">{{ $item->QuantityOnHand }}</td>
                            <td>{{ $item->QuantityOnSalesOrder }}</td>
                            <td>{{ $item->QuantityOnOrder }}</td>
                            <td>{!! \Helper::formatPrice($item->Price) !!}</td>
                            <td>{{ $item->Price>0?number_format(($item->Price-$item->AverageCost)/$item->Price*100, 2):0 }}%</td>
                            <td>{!! \Helper::formatPrice($item->AverageCost) !!}</td>
                            <td>{!! \Helper::formatPrice($item->PurchaseCost) !!}</td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-9">

            <div class="box box-success">
                <div class="box-body">

                    <h4>Customer Sales History</h4>
                    <div class="table-responsive" style="position: relative">
                        <table id="report" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Rep</th>
                                    <th class="text-center">Past 5 Years</th>
                                    <th class="sum">This Year</th>
                                    <th class="sum">This Month</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-1 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-2 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-3 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-4 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-5 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-6 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-7 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-8 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-9 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-10 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-11 Months")) }}</th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach($CustomerArr AS $customer => $customerArrs)
                                <tr>
                                    <td><a class="" href="{{ route('admin.customers.report', $curr[$customer]['CustomerId']) }}">{{ $customer }}</a></td>
                                    <td class="text-center">{{ isset($curr[$customer]['rep']) ?$curr[$customer]['rep']:"" }}</td>
                                    <td class="text-center {{ isset($curr[$customer]['t']) && $curr[$customer]['t'] ?"":"bg-danger" }}">{{ isset($curr[$customer]['t']) ?$curr[$customer]['t']:0 }}</td>
                                    <td class="text-center {{ isset($curr[$customer]['y']) && $curr[$customer]['y'] ?"":"bg-danger" }}">{{ isset($curr[$customer]['y']) ?$curr[$customer]['y']:0 }}</td>
                                    @php($cnt = 1)
                                    @foreach($monthTots AS $month => $monthTot)
                                        <td data-sort="{{ isset($curr[$customer][$month]) ?$curr[$customer][$month]:0 }}" class="text-center {{ $cnt ==1 ?'bg-warning border-left border-right':'' }} {{ isset($curr[$customer][$month]) && $curr[$customer][$month] ?"":"bg-danger" }}"><b>{{ isset($curr[$customer][$month]) ?$curr[$customer][$month]:0 }}</b>
                                            <br/><span class="small {{ isset($past[$customer][$month]) && isset($curr[$customer][$month]) && $past[$customer][$month]>$curr[$customer][$month]?'text-red':'' }} text-default">{{ isset($past[$customer][$month]) ?$past[$customer][$month]:0 }}</span>
                                        </td>
                                        @php($cnt = 0)
                                    @endforeach
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-bold bg-info"></td>
                                <td class="text-bold bg-info"></td>
                                @foreach($monthTots AS $month => $monthTot)
                                    <td class="text-bold bg-info"></td>
                                @endforeach
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>

        </div><!-- /.box-body -->
        <div class="col-lg-3">
            <div class="box box-success">
                <div class="box-body">
                    <canvas id="salesChart" width="100%"></canvas>
                </div>
            </div>
        </div>
    </div><!--box-->


    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Purchase Price Change - {{ $item->Description }}</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <table class="table table-responsive table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Ref</th>
                                <th>Supplier</th>
                                <th>Qty</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($bills AS $bill)

                            <tr>
                                <td>{{ $bill->Date }}</td>
                                <td>{{ $bill->ReferenceNumber }}</td>
                                <td>{{ $bill->Supplier }}</td>
                                <td>{{ $bill->ItemQuantity }}</td>
                                <td>{!! \Helper::formatPrice($bill->ItemCost) !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!--box-->
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <canvas id="myChart" width="100%" height="200px"></canvas>
            </div>

        </div>
    </div>

@stop

@section('after-scripts-end')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>

    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#report').DataTable({
                'paging'      : false,
                'searching'   : true,
                'order' : [[ 4, 'desc' ]],
                'ordering'    : true,
                'info'        : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ],
                'initComplete': function (settings, json){
                    this.api().columns('.sum').every(function(){
                        var column = this;

                        var sum = column
                                .data()
                                .reduce(function (a, b) {
                                    a = parseInt(a, 10);
                                    if(isNaN(a)){ a = 0; }

                                    b = parseInt(b, 10);
                                    if(isNaN(b)){ b = 0; }

                                    return a + b;
                                });

                        $(column.footer()).html('Sum: ' + sum);
                    });
                }
            })
        })
    </script>

    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [<?php
                    foreach ($bills AS $bill) {
                        echo '"'.date("d-M-Y",strtotime($bill->Date)).'", ';
                    }
                    ?>],
                datasets: [{
                    label: 'Cost Price',
                    data: [<?php
                        foreach ($bills AS $bill) {
                            echo '"'.$bill->ItemCost.'", ';
                        }
                        ?>],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

        var ctx2 = document.getElementById("salesChart").getContext('2d');
        var myChart2 = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: [<?php
                    foreach ($monthTots AS $mnth => $mnthval) {
                        $yr = date("Y");
                        if ($mnth >= date("n")) {
                            $yr = date("Y", strtotime("-1 Year"));
                        }
                        echo '"'.date("M",strtotime("01-".$mnth."-".$yr)).'", ';
                    }
                    ?>],
                datasets: [{
                    label: 'Past Year',
                    fill: false,
                    backgroundColor: "blue",
                    borderColor: "blue",
                    data: [<?php
                        foreach ($monthTots AS $monthTot) {
                            echo '"'.$monthTot.'", ';
                        }
                        ?>],
                    borderWidth: 3
                },{
                    label: 'Prev Period',
                    fill: false,
                    data: [<?php
                        foreach ($PrevMonthTots AS $PrevMonthTot) {
                            echo '"'.$PrevMonthTot.'", ';
                        }
                        ?>],
                    borderWidth: 3
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endsection