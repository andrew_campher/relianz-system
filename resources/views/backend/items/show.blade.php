@extends ('backend.layouts.app')

@section ('title', $item->Description.' | Show')

@section('page-header')
    <h1>
        Items Management
        <small>View Item</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{!! $item->showClass() !!} {{ $item->Description }}
                @if(access()->user()->hasRoles(['Sales','Procurement']))
                <a class="btn btn-primary" href="{{ route('admin.items.report', $item->ID) }}"><i class="fa fa-bar-chart"></i></a>
                @endif
            </h3>
            <div class="pull-right">
                {!! $item->ID !!}
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-hover">

                        <tr>
                            <th>Description</th>
                            <td>{!! $item->Description !!}</td>
                        </tr>

                        <tr>
                            <th>Stock Type</th>
                            <td>{!! $item->showStockType() !!}</td>
                        </tr>


                        <tr>
                            <th>Kg / Unit</th>
                            <td>{!! $item->getKGUnit() !!}</td>
                        </tr>
                        <tr>
                            <th>Price</th>
                            <td>{!! \Helper::formatPrice($item->Price) !!}</td>
                        </tr>
                        <tr>
                            <th>PurchaseCost</th>
                            <td>{!! \Helper::formatPrice($item->PurchaseCost) !!}</td>
                        </tr>
                        <tr>
                            <th>Average Cost</th>
                            <td>{!! \Helper::formatPrice($item->AverageCost) !!}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-hover">
                        <tr>
                            <th>Active</th>
                            <td>{!! $item->IsActive?'<i class="fa fa-2 fa-check-circle"></i>':'<i class="fa fa-2 fa-times-circle"></i>' !!}</td>
                        </tr>
                        <tr>
                            <th>Stock On Hand</th>
                            <td>{!! $item->QuantityOnHand !!}</td>
                        </tr>
                        <tr>
                            <th>On Sales Order</th>
                            <td>{!! $item->QuantityOnSalesOrder !!}</td>
                        </tr>
                        <tr>
                            <th>On Order</th>
                            <td>{{ $item->QuantityOnOrder }}</td>
                        </tr>

                    </table>
                </div>
            </div>


        </div><!-- /.box-body -->
    </div><!--box-->

@stop