@extends ('backend.layouts.app')

@section ('title', 'Service links | Management')

@section('page-header')
    <h1>
        Links
        <small>Edit</small>
    </h1>
@endsection

@section('content')

    @if( isset($link) )
        {{ Form::model($link, ['route' => ['admin.links.update', $link], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.links.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit links</h3>

            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    {{ Form::label('menu_title', 'Menu title', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('menu_title', null, ['class' => 'form-control', 'placeholder' => 'Menu title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('icon', 'Icon', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('icon', null, ['class' => 'form-control', 'placeholder' => 'Icon must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('url', 'URL', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'URL must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('sort', 'Sort Order', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('sort', null, ['class' => 'form-control', 'placeholder' => 'Sort Order must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('roles', 'Roles', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('roles', null, ['class' => 'form-control', 'placeholder' => 'Roles must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('user_id', 'User', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::select('user_id', $users, NULL, ['class' => 'form-control','placeholder' => '-- None --']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('active', 'Active', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::select('active', ['1'=>'YES', '0'=>'NO'], 1, ['class' => 'form-control','placeholder' => '-- None --']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

            </div>
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.links.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

