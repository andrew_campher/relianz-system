@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Links Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">

            <div class="col-md-6">
                <h2 class="box-title">Links</h2>
            </div>
            <div class="col-md-6 text-right">
                 <a class="btn btn-primary" href="{{ route('admin.links.create') }}">Add</a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                    <table id="link-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Menu Title</th>
                            <th>Icon</th>
                            <th>URL</th>
                            <th>Roles</th>
                            <th>Active</th>
                            <th>Sort</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($links AS $link)
                            <tr>
                                <td>{{ $link->menu_title }}</td>
                                <td>{{ $link->icon }}</td>
                                <td>{{ $link->url }}</td>
                                <td>{{ $link->roles }}</td>
                                <td>{{ $link->active }}</td>
                                <td>{{ $link->sort }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                <div class="pull-right">{{ $links->appends($request::capture()->except('page'))->links() }}</div>

                    <div class="clearfix"></div>


            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
