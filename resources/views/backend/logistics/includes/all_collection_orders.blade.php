    <table class="table table-orders table-bordered table-striped table-hover" width="100%">
        <tr>
            <th>SO</th>
            <th>Invoice</th>
            <th>Customer</th>
            <th>Terms</th>

            <th>Status</th>
            <th>User</th>

            <th>COA</th>
            <th>Created</th>
            <th>Invoiced</th>
            <th>Notes</th>

        </tr>
        @if(isset($orders) && $orders)
            @foreach($orders AS $order)
                <tr class="{{ $order->Terms == 'COD'?'bg-danger':'' }}">
                    <td><a href="{{ route('admin.orders.show', $order->SalesOrderID) }}"><b>{{ $order->SONumber }}</b></a></td>
                    <td><b>{{ $order->InvoiceNo?$order->InvoiceNo:'' }}</b></td>
                    <td class="{{ $order->Urgent?'name_urgent':'' }}">{{ $order->Customer }} {!! $order->Urgent?'<span title="URGENT!!" class="label-danger btn-xs"><i class="fa fa-warning"></i></span>':'' !!}</td>
                    <td class="{{ $order->Terms == 'COD'?'btn-danger':'' }}">{{ $order->Terms }}</td>
                    <td>
                        @include('backend.logistics.includes.status_show')
                    </td>
                    <td>{{ $order->User }}</td>

                    <td>{{ $order->COA }}</td>
                    <td>{{ $order->Created }}</td>
                    <td>{{ $order->InvCreated }}</td>

                    <td>{{ $order->Notes }}</td>

                </tr>
            @endforeach

        @endif
    </table>

