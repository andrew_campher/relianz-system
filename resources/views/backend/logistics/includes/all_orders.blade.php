    <table id="allorders" class="table table-orders table-bordered table-striped table-hover" width="100%">
        <thead>
            <tr>
                <th>Customer</th>
                <th>Area</th>
                <th>Weight</th>
                <th>SO</th>
                <th>Type</th>
                <th>Status</th>
                <th>User</th>
                <th>Terms</th>
                <th>COA</th>
                <th>SO Created</th>
                <th>Invoice Created</th>
                <th>Notes</th>
            </tr>
        </thead>
        <tbody>
        @if(isset($orders) && $orders)
            @foreach($orders AS $order)
                <tr>
                    <td class="{{ $order->Urgent?'name_urgent':'' }}">{{ $order->Customer }} {!! $order->Urgent?'<span title="URGENT!!" class="label-danger btn-xs"><i class="fa fa-warning"></i></span>':'' !!}</td>
                    <td>{{ $order->Area }}</td>
                    <td>{{ $order->Weight }}</td>
                    <td><a href="{{ route('admin.orders.show', $order->SalesOrderID) }}"><b>{{ $order->SONumber }}</b></a></td>
                    <td>{{ $order->Type }}</td>
                    <td>
                        @include('backend.logistics.includes.status_show')
                    </td>
                    <td>{{ $order->User }}</td>
                    <td class="{{ $order->Terms == 'COD'?'btn-danger':'' }}">{{ $order->Terms }}</td>
                    <td>{{ $order->COA }}</td>
                    <td>{{ $order->Created }}</td>
                    <td>{{ $order->InvCreated }}</td>
                    <td>{{ $order->Notes }}</td>

                </tr>
            @endforeach

        @endif
        </tbody>
    </table>

    @section('after-scripts-end')
        {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
        {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

        <script>
            $(function () {
                $('#allorders').DataTable({
                    'paging'      : false,
                    'searching'   : true,
                    'ordering'    : true,
                    'info'        : false,
                    'fixedHeader': true,
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                })
            })
        </script>
@append