@php($stops=array())
@if(isset($final_orders['orders']) && $final_orders)
    @foreach($final_orders['orders'] AS $order)
        @php
            if (!isset($stops[$order->CustomerId])) {
                $stops[$order->CustomerId] = 1;
            }
        @endphp
    @endforeach
@endif

    <table class="table table-orders table-hover" width="100%">
        <tr class="orderHead">
            <th><span class="label label-primary btn-lg">{{ COUNT($stops) }}</span> Customer stops</th>
            <th class="text-center">SO</th>
            <th class="text-center">Status</th>
            @foreach($final_orders['areas'] AS $area => $val)
                <th class="text-center">{{ $area }}</th>
                @php
                    $totweights[$area] = 0;
                @endphp
            @endforeach

            @php
                $these_areas = $final_orders['areas'];
            @endphp
        </tr>
        @if(isset($final_orders['orders']) && $final_orders)
            @foreach($final_orders['orders'] AS $order)
                <tr>
                    <td class="{{ $order->Urgent == 'URGENT'?'bg-danger':'' }} {{ $order->Urgent == 'TODAY'?'bg-warning':'' }}">
                        <a class="text-black" href="{{ route('admin.customers.show', $order->CustomerId) }}">{{ $order->Customer }}</a>
                        {!! $order->Urgent == 'URGENT'?'<span title="URGENT!!" class="label-danger btn-xs"><i class="fa fa-warning"></i></span>':'' !!}
                        {!! $order->Urgent == 'TODAY'?'<span title="TODAY" class="label-warning btn-xs"><i class="fa fa-warning"></i></span>':'' !!}
                        @php

                            echo \App\Helpers\Helper::OrderStockCheck($order);

                        @endphp
                    </td>
                    <td class="text-center {{ $order->InvoicedQty == 0 && $order->TransCount>0?'bg-warning':'' }}"><a class="text-black" href="{{ route('admin.orders.show', $order->SalesOrderID) }}"><b>{{ $order->SONumber }}</b></a></td>
                    <td class="text-center">
                        @include('backend.logistics.includes.status_show')
                    </td>

                    @if(isset($these_areas))
                        @foreach($these_areas AS $area => $val)

                            @if(isset($order_weight[$order->SONumber][$order->Customer][$area]))
                                <td class="text-center" id="w{{ $order->SalesOrderID }}">
                                @php
                                    /*if($order->Status == 'Complete' || $order->Status == 'Picking' || $order->Status == 'Printed' || $order->Status == 'Invoiced') {
                                        $totweights[$area] += $order_weight[$order->SONumber][$order->Customer][$area];

                                        echo $order_weight[$order->SONumber][$order->Customer][$area];
                                    } else{
                                        echo '<s>'.$order_weight[$order->SONumber][$order->Customer][$area].'</s>';
                                    }*/
                                @endphp
                                </td>
                            @else
                                <td class="text-center"></td>
                            @endif
                        @endforeach
                    @endif
                </tr>
            @endforeach

            @if(isset($these_areas) && $these_areas)
                @php($areatot = 0)
                <tfoot>
                <tr class="totweights">
                    <td></td>
                    <td></td>
                    <td></td>
                @foreach($these_areas AS $area => $val)
                    <td class="text-center"></td>
                @endforeach
                </tr>
                </tfoot>
            @endif
        @endif
    </table>

