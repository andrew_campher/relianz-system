<div class="dropdown stat_chng_wrap">
    <button data-value="{{ $order->Status }}" data-del="{{ $order->Area }}" data-trans="{{ $order->TransCount }}" class="btn btn-default dropdown-toggle change_stat stat_{{ str_slug($order->Status) }}" type="button"
            data-id="{{ $order->SalesOrderID }}" data-sonum="{{ $order->SONumber }}" data-weight="{{ $order->Weight }}"
            id="dropdownMenu1"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        {{ $order->Status }}
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        @if(access()->user()->hasRoles(['Logistics']))
        <li><a class="chngStat" href="javascript:void(0)">Shipped</a></li>
        @endif
        <li><a class="chngStat" href="javascript:void(0)">Complete</a></li>
        <li><a class="chngStat" href="javascript:void(0)">Printed</a></li>
        <li><a class="chngStat" href="javascript:void(0)">Invoiced</a></li>
        <li><a class="chngStat" href="javascript:void(0)">Picking</a></li>
        <li><a class="chngStat" href="javascript:void(0)">NO STOCK</a></li>
        <li><a class="chngStat" href="javascript:void(0)">ON HOLD</a></li>
        <li><a class="chngStat" href="javascript:void(0)">Cancelled</a></li>
        @if(access()->user()->hasRoles(['Finance']))
        <li><a class="chngStat" href="javascript:void(0)">Paid</a></li>
        @endif
    </ul>
</div>