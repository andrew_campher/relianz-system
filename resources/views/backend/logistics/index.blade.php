@extends('backend.layouts.app')

@section ('title', 'Logistics Tool')

@section('page-header')
    <div class="row">
        <div class="col-md-6">
            <h2 class="no-margin"><span class="label label-primary">{{ $totalOrders }}</span> Deliveries for {{ $getDate?date(config('app.date.short'),strtotime($getDate)):'' }}</h2>
        </div>
        <div class="col-md-6 text-right">
                <form action="" method="get" >
                    Date: <input name="show_date" value="{{ $getDate?$getDate:'' }}" type="date">
                    <input type="submit" value="submit">
                </form>

            <div class="row">
                <div class="col-md-6">
                    <div class="h3 no-margin pull-left">{{ number_format($totalWeight,2,'.',',') }}kg / <span class="small bg-warning">{{ number_format($AllTotalWeight,2,'.',',') }}kg</span></div>
                </div>
                <div class="col-md-6">
                    @if(access()->user()->hasRoles(['Finance','Administrator']))
                        <div class="h4 text-bold"><i class="fa fa-money"></i> R{{ number_format($salesvalue,2,'.',',') }}</div>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection

@section('content')



    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#routing" aria-controls="routing" role="tab" data-toggle="tab">Routing</a></li>
        <li role="presentation"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Orders</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="routing">

            @if(isset($area_orders[8]) && $area_orders[8])
                <div class="row">
                    <div class="col-md-12">
                        <div class="h5"><b>Orders with no Delivery Code</b></div>
                        <div class="box">
                        @php
                            $final_orders = $area_orders[8];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>
                    </div>
                </div>
            @endif


            <div class="row">
                <div class="col-md-6">

                    @if(isset($area_orders[1]) && $area_orders[1])
                        <div class="box">
                        @php
                            $final_orders = $area_orders[1];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>
                    @endif

                </div>
                <div class="col-md-6">

                    @if(isset($area_orders[2]) && $area_orders[2])
                        <div class="box">
                        @php
                            $final_orders = $area_orders[2];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>

                    @endif

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    @if(isset($area_orders[3]) && $area_orders[3])
                        <div class="box">
                        @php
                            $final_orders = $area_orders[3];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>
                    @endif

                </div>
                <div class="col-md-6">

                    @if(isset($area_orders[4]) && $area_orders[4])
                        <div class="box">
                        @php
                            $final_orders = $area_orders[4];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>
                    @endif

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    @if(isset($area_orders[5]) && $area_orders[5])
                        <div class="box">
                        @php
                            $final_orders = $area_orders[5];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>
                    @endif

                </div>
                <div class="col-md-6">

                    @if(isset($area_orders[6]) && $area_orders[6])
                        <div class="box">
                        @php
                            $final_orders = $area_orders[6];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>
                    @endif

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    @if(isset($area_orders[7]) && $area_orders[7])
                        <div class="box">
                        @php
                            $final_orders = $area_orders[7];
                        @endphp
                        @include('backend.logistics.includes.area_orders')
                        </div>
                    @endif

                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2>Collections</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    @if(isset($collection_orders) && $collection_orders)
                        <div class="box">
                        @php
                            $final_orders = $collection_orders;
                        @endphp
                        @include('backend.logistics.includes.col_orders')
                        </div>
                    @endif

                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="orders">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                    @if(isset($orders) && $orders)

                        @include('backend.logistics.includes.all_orders')

                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


@section('after-scripts-end')
    <script>

        function FillWeights() {
            var theid;
            var thestat;
            $('.change_stat').each(function() {
                thestat = $(this).attr('data-value');
                theid = $(this).attr('data-id');

                $('#w' + theid).html('');

                if (thestat == 'Complete' || thestat == 'Picking' || thestat == 'Printed' || thestat == 'Invoiced') {
                    $('#w' + theid).html('<b>'+$(this).attr('data-weight')+'</b>');
                } else {
                    $('#w' + theid).html('<s>'+$(this).attr('data-weight')+'</s>');
                }
            });
        }

        function calculateColumn(index)
        {
            $('.table-orders').each(function() {
                if ($(this).find('tr td:nth-child(' + index + ')').length) {
                    var total = 0;
                    $(this).find('tr td:nth-child(' + index + ') b').each(function () {
                        var value = parseInt($(this).text());
                        if (!isNaN(value)) {
                            total += value;
                        }
                    });
                    $(this).find('tfoot td:nth-child(' + index + ')').html(total);
                }
            });
        }

        function CalcCols() {
            calculateColumn(4);
            calculateColumn(5);
            calculateColumn(6);
            calculateColumn(7);
        }

        $(document).ready(function(){

            FillWeights();

            CalcCols();

            var current;
            var e;

            $('.chngStat').on('click',function () {

                e = $(this).parent().parent().prev();
                new_status = $(this);
                current = e.attr('data-value');
                current_slug = current.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');

                var status = $(this).val();
                $.ajax({
                    url: "{{ route("api.logistics.change_status") }}",
                    type: "post",
                    data: {
                        "SalesOrderID": e.attr('data-id'),
                        "SONumber": e.attr('data-sonum'),
                        "weight": e.attr('data-weight'),
                        "transcount": e.attr('data-trans'),
                        "change_status": new_status.html()
                    },
                    beforeSend: function() {
                        e.html('<i class="statload fa fa-cog fa-spin fa-fw"></i>');
                    },
                    success: function(d) {
                        e.attr('data-value',new_status.html());
                        stat_slug = new_status.html().toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
                        e.removeClass('stat_'+current_slug).addClass('stat_'+stat_slug);
                        e.html(new_status.html()+' <span class="caret"></span>');
                        $('.statload').remove();

                        if (e.attr('data-value').toLowerCase() == 'complete' || e.attr('data-value').toLowerCase() == 'picking' || e.attr('data-value').toLowerCase() == 'printed' || e.attr('data-value').toLowerCase() == 'invoiced') {
                            $('#w' + e.attr('data-id')).html('<b>'+e.attr('data-weight')+'</b>');
                        } else {
                            $('#w' + e.attr('data-id')).html('<s>'+e.attr('data-weight')+'</s>');
                        }

                        CalcCols();

                    },
                    error: function(d) {
                        e.html(current+' <span class="caret"></span>');
                        $('.statload').remove();
                    }
                });
            });

        });
    </script>
@append


@endsection