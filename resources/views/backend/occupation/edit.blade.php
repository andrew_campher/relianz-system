@extends ('backend.layouts.app')

@section ('title', 'Occupation | Manager')

@section('page-header')
    <h1>
        Occupations Management
        <small>Edit</small>
    </h1>
@endsection

@section('content')
    @if( isset($occupation) )
        {{ Form::model($occupation, ['route' => ['admin.occupation.update', $occupation], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.occupation.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Occupation</h3>

            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('tags', 'Tags', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('tags', null, ['class' => 'form-control', 'placeholder' => 'Tags']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->


            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.question.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

