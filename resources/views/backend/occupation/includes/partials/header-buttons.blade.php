<div class="btn-toolbar mb-10">

    <div class="input-group">
        <form action="" method="GET">
        <input name="q" value="" type="text">
        <input type="submit" value="GO">
        </form>
    </div>

    <div class="btn-group">


        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
           Occupations <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.occupation.index', 'All Occupations') }}</li>

            @permission('manage-users')
                <li>{{ link_to_route('admin.occupation.create', 'Create Occupation') }}</li>
            @endauth

        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>
