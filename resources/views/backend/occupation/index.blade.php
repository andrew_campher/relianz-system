@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Occupations Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Occupations</h3>

            <div class="box-tools">
                @include('backend.occupation.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.occupation.action') }}" method="post" class="">
                    <table id="occupation-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Tags</th>
                            <th>Hits</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($occupations AS $occupation)
                            <tr>
                                <td width="30px"><input name="selected[]" type="checkbox" value="{{ $occupation->id }}"></td>
                                <td>{{ $occupation->id }}</td>
                                <td>{{ $occupation->title }}</td>
                                <td>{!! $occupation->slug !!}</td>
                                <td>{!! $occupation->tags !!}</td>
                                <td>{!! $occupation->hits !!}</td>
                                <td>{!! $occupation->action_buttons !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="pull-right">{{ $occupations->links() }}</div>

                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
