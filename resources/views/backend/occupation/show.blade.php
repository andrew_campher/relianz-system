@extends ('backend.layouts.app')

@section ('title', 'Occupations | Show')

@section('page-header')
    <h1>
        Occupations Management
        <small>View Occupation</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $occupation->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>Tags</th>
                    <td>{{ $occupation->tags }}</td>
                </tr>

                <tr>
                    <th>Slug</th>
                    <td>{!! $occupation->slug !!}</td>
                </tr>

            </table>

        </div><!-- /.box-body -->
    </div><!--box-->

@stop


