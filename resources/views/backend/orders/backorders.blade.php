@extends ('backend.layouts.app')

@section ('title', 'Backorders Tool')

@section('page-header')
    <div class="row">
        <div class="col-md-4">
            <h1 class="no-margin h3">
                <i class="fa fa-ambulance"></i> Back Orders -
                <div class="dropdown inline stat_chng_wrap">
                    <button class="text-bold btn {{ $instock?'btn-success':'btn-danger' }} btn-md dropdown-toggle" type="button" data-weight="1" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{ $instock?'IN STOCK':'NO STOCK' }}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a class="text-bold bg-success" href="{{ route('admin.orders.backorders', ['instock' => 1]) }}">IN STOCK</a></li>
                        <li><a class="text-bold bg-danger" href="{{ route('admin.orders.backorders', ['instock' => 0]) }}">NO STOCK</a></li>
                    </ul>
                </div>
            </h1>
        </div>
        <div class="col-md-8">
            <form class="form-inline">
                <select class="form-control" name="JobType">
                    <option value="">-- All In-House Reps --</option>
                    @foreach($jobtypes AS $jobtype)
                        <option value="{{ $jobtype->JobType }}" {{ $request->JobType == $jobtype->JobType?'selected':'' }}>{{ $jobtype->JobType }} ({{ $jobtype->nocustomers }} customers)</option>
                    @endforeach
                </select>

                <select name="rep" class="form-control inline">
                    <option value="">-- All Reps --</option>
                    @foreach($reps AS $rep)
                        <option {{ $request->rep == $rep->Initial?'selected':'' }} value="{{ $rep->Initial }}">{{ $rep->SalesRepEntityRef_FullName }}</option>
                    @endforeach
                </select>

                <button class="btn btn-info" type="submit">GO</button>
            </form>
        </div>
    </div>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-body">
            <div class="table-responsive">
                <table id="order-table" class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer</th>
                            <th>Sale Value</th>
                            <th>Orders</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($cnt=1)
                            @foreach($boItems AS $boItem)

                                <tr>
                                    <td>{{ $cnt++ }}</td>
                                    <td><a class="text-black" href="{{ route('admin.orders.showbo', $boItem['CustomerId']) }}"><b>{{  $boItem['CustomerName'] }}</b></a></td>
                                    <td>{!! \Helper::formatPrice($boItem['SaleValue']) !!}</td>
                                    <td>
                                    @foreach($boItem['items'] AS $item)
                                        <div>
                                            <a class="text-black" href="{{ route('admin.orders.showbo', $item->CustomerId) }}">
                                                <span style="font-size: 12px;" class="btn btn-primary btn-sm">{{ $item->ReferenceNumber }}</span>
                                                {{ $item->Name }}
                                                <span class="label label-success">{{ $item->ItemQuantity-$item->ItemInvoicedAmount }}</span>
                                                @if($instock)
                                                    <span class="badge label-primary">Stock Arrived: {{ date("d-m-Y", strtotime($item->ReceivedDate)) }}</span>
                                                @endif

                                            </a>
                                        </div>
                                    @endforeach
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>

                    <div class="clearfix"></div>

            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop

@section('after-scripts-end')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>

    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#order-table').DataTable({
                'paging'      : false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ]
            })
        })
    </script>
@endsection