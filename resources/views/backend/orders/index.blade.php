@extends ('backend.layouts.app')

@section ('title', 'Orders')

@section('page-header')
    <h1>
        Sales Orders
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">

            <div class="col-md-3">
                <h2 class="box-title">Orders</h2>
            </div>
            <div class="col-md-9 text-right">
                <form action="" class="form-inline" method="GET">
                    <input name="search" placeholder="Search..." class="form-control" type="text" value="{{ isset($request->search) && $request->search ? $request->search : "" }}">
                    <select class="form-control" name="isinvoiced">
                        <option value="">ALL</option>
                        <option>NOT INVOICED</option>
                        <option>INVOICED</option>
                    </select>
                    <button class="btn btn-primary" type="submit">GO</button>
                </form>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="order-table" class="table table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Ref</th>
                            <th>Customer</th>
                            <th>Items</th>
                            <th>Subtotal</th>
                            <th>Status</th>
                            <th>Transactions</th>
                            <th>Closed</th>
                            <th>Fully Invoiced</th>
                            <th>Ship Date</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($orders AS $order)
                            <tr>

                                <td><a class="text-black" href="{{ route('admin.orders.show', $order->ID) }}">{{ $order->ReferenceNumber }}</a></td>
                                <td><a href="{{ route('admin.customers.show', $order->CustomerId) }}">{{ $order->CustomerName }}</a></td>
                                <td>{{ $order->ItemCount }}</td>
                                <td>{!! \Helper::formatPrice($order->Subtotal) !!}</td>
                                <td>
                                    @if(isset($order->stats->StatusText) && $order->stats->StatusText)
                                        <button class="btn btn-default dropdown-toggle change_stat stat_{{ str_slug($order->stats->StatusText) }}" type="button">
                                            {{ $order->stats->StatusText }}
                                        </button>
                                    @else
                                        @if($order->TransactionCount > 0)
                                            <button class="btn btn-default dropdown-toggle change_stat stat_invoiced" type="button">Invoiced</button>
                                        @else
                                            <button class="btn btn-default dropdown-toggle change_stat stat_picking" type="button">Picking</button>
                                        @endif

                                    @endif
                                </td>
                                <td>{!! $order->TransactionCount?'<span class="label label-info">'.$order->TransactionCount.' invoices</span>':'<span class="label label-default">Not invoiced</span>' !!}</td>
                                <td>{!! $order->IsManuallyClosed?'<i class="fa fa-check-circle-o"></i>':'' !!}</td>
                                <td>{!! $order->IsFullyInvoiced?'<i class="fa fa-check-circle-o"></i>':'' !!}</td>
                                <td>{{ date("d-m-Y",strtotime($order->ShipDate)) }}</td>
                                <td>{{ $order->TimeCreated }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                <div class="pull-right">{{ $orders->appends($request::capture()->except('page'))->links() }}</div>

                    <div class="clearfix"></div>


            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
