@extends ('backend.layouts.app')

@section ('title', $order->ReferenceNumber.' - Sales Order Details')

@section('page-header')
    <h1>
        <span class="label label-primary">{{ $order->ReferenceNumber }}</span> Sales Order
        <small>View Order</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ $order->CustomerName }}
            </h3>
            @if($order->TransactionCount > 0)
                <h1 class="label label-success btn-lg"><i class="fa fa-thumbs-up"></i> INVOICED</h1>
            @endif

            @if(isset($order->stats->StatusText) && $order->stats->StatusText)
                <button class="btn btn-default dropdown-toggle change_stat stat_{{ str_slug($order->stats->StatusText) }}" type="button">
                    {{ $order->stats->StatusText }}
                </button>
            @else
                @if($order->TransactionCount > 0)
                    <button class="btn btn-default dropdown-toggle change_stat stat_invoiced" type="button">Invoiced</button>
                @else
                    <button class="btn btn-default dropdown-toggle change_stat stat_picking" type="button">Picking</button>
                @endif

            @endif

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-hover">
                        <tr>
                            <td colspan="2">
                                <div class="h4">Order Note</div>
                                {{ $order->Memo }}
                            </td>
                        </tr>
                        <tr>
                            <th>Ship Date</th>
                            <td>{{ date("d-m-Y", strtotime($order->ShipDate)) }}</td>
                        </tr>
                        <tr>
                            <th>Tax Date</th>
                            <td>{{ date("d-m-Y", strtotime($order->Date)) }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-hover">
                        <tr>
                            <th>PO Number</th>
                            <td>{{ $order->PONumber }}</td>
                        </tr>
                        <tr>
                            <th>Processor</th>
                            <td>{{ $order->Other }}</td>
                        </tr>
                        <tr>
                            <th>Created Date</th>
                            <td>{{ $order->TimeCreated }}</td>
                        </tr>
                    </table>
                </div>
            </div>


        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Items</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">

                <tr>
                    <th>Product</th>
                    <th>Cost</th>
                    <th>Weight</th>

                    <th>Quantity</th>
                    <th width="50">Qty on Hand</th>
                    <th>Invoiced</th>

                    <th>Price</th>
                    <th>SubTotal</th>
                </tr>
                @if($order->items)

                    @php
                        $totweight = 0;
                        $shippable = 0;
                        $totgp = 0;
                        $totprice = 0;

                    @endphp

                    @foreach($order->items AS $item)

                        @php
                            $downpack = false;
                        @endphp

                        <tr>
                            <td><a href="{{ route('admin.items.show', $item->ItemId) }}">{{ $item->ItemName }}</a></td>

                            @if(isset($item->item) && $item->item)
                            <td>{!! \Helper::formatPrice($item->item->AverageCost) !!}</td>
                            <td>
                                @php

                                $totgp += ($item->ItemRate-$item->item->AverageCost)*$item->ItemQuantity;
                                $totprice += $item->ItemRate*$item->ItemQuantity;

                                if(isset($item->item->CustomFields)) {
                                    $customfields = json_decode($item->item->CustomFields, true);

                                    $totweight += $customfields['KgWeight']*$item->ItemQuantity;

                                    $downpack = strpos($item->ItemName,' CP');

                                    if($order->TransactionCount == 0) {
                                        $availqty = $item->ItemQuantity-$item->ItemInvoicedAmount;
                                        if ($downpack === FALSE) {
                                            if ($item->item->QuantityOnHand > 0) {
                                                $availqty = ($item->item->QuantityOnHand-($item->ItemQuantity-$item->ItemInvoicedAmount)<0?$item->item->QuantityOnHand:$item->ItemQuantity-$item->ItemInvoicedAmount);
                                            } else {
                                                $availqty = 0;
                                            }
                                        }

                                        $shippable += $customfields['KgWeight']*$availqty;
                                    }


                                    echo $customfields['KgWeight']*$item->ItemQuantity.'kg';
                                }
                                @endphp
                            </td>
                            @endif
                            <td>
                                <b>{{ $item->ItemQuantity }}</b>
                                @php
                                //Alert if issue with invoicing more stock than on system
                                if($item->ItemInvoicedAmount && date("d-m-Y", strtotime($order->ShipDate)) >= date("d-m-Y") &&
                                strpos($item->ItemName,' CP') === FALSE && $item->item->QuantityOnHand+$item->ItemInvoicedAmount < $item->ItemInvoicedAmount) {
                                    echo '<span class="badge label-danger"><i class="fa fa-exclamation-triangle"></i> NOT ENOUGH STOCK</span> leaves '.$item->item->QuantityOnHand.' on system!';

                                }
                                @endphp
                            </td>
                            <td class="{{ $item->ItemInvoicedAmount > 0?'': ($item->item->QuantityOnHand-($item->ItemQuantity-$item->ItemInvoicedAmount)<0?'bg-danger':'bg-success') }}">
                                @if($item->ItemQuantity-$item->ItemInvoicedAmount != 0)
                                    {{ $item->item->QuantityOnHand }}
                                @else
                                    fulfilled
                                @endif
                            </td>
                            <td>
                                {!! $order->TransactionCount > 0 ?
                                ((float)$item->ItemInvoicedAmount == (float)$item->ItemQuantity && !$item->ItemManuallyClosed ?
                                    '<span class="label label-success">In Full</span>' :
                                    '<span class="label label-warning">'.($item->ItemQuantity-$item->ItemInvoicedAmount).' Backorder</span>')
                                :($item->ItemManuallyClosed?'<span class="label label-danger">Closed</span>':'<span class="label label-default">Not invoice</span>') !!}
                            </td>

                            <td>{!! \Helper::formatPrice($item->ItemRate) !!}</td>

                            <td>{!! \Helper::formatPrice($item->ItemRate*$item->ItemQuantity) !!}</td>

                        </tr>
                    @endforeach


                    <tr>
                        <td class="h4 text-right">Total Weight</td>
                        <td class="h4 text-right" colspan="2">
                            @if($shippable)
                                <span class="bg-warning small">To Be Shipped: {{ $shippable }}kg</span><br/>
                            @endif
                            {{ $totweight }}kg
                        </td>
                        <td class="h4 text-right">Totals</td>
                        <td class="h4 text-right" colspan="4">
                           Price: {!! \Helper::formatPrice($totprice) !!}<br/>
                            @if(access()->user()->hasRoles(['Sales','Procurement','Logistics','Finance']))
                               <span class="label label-success">Est GP:  {!! \Helper::formatPrice($totgp) !!}</span>
                                <span class="label label-success">GP %:  {!! number_format($totgp/$totprice*100,2) !!}</span>
                            @endif
                        </td>
                    </tr>
                @endif

            </table>
            </div>

        </div><!-- /.box-body -->
    </div><!--box-->

@stop