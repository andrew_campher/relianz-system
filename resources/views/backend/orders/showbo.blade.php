@extends ('backend.layouts.app')

@section ('title', $customer->Name.' Backorders')

@section('page-header')

    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin">
                <span class="">{{ $customer->Name }}</span>
                @if(access()->user()->hasRoles(['Sales','Procurement']))
                    <a class="btn btn-primary btn-sm" href="{{ route('admin.customers.report', $customer->ID) }}"><i class="fa fa-bar-chart"></i></a>
                @endif
            </h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-condensed table-hover">
                        <tr>
                            <th>Company</th>
                            <td>{{ $customer->Company }}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{ $customer->Phone }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $customer->Email }}</td>
                        </tr>

                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-hover">
                        <tr>
                            <th>Additional</th>
                            <td>{{ $customer->AdditionalContactInfo }}</td>
                        </tr>

                        <tr>
                            <th>Notes</th>
                            <td>{!!  str_replace("\n","<br/>",$customer->Notes) !!}</td>
                        </tr>

                    </table>
                </div>
            </div>


        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Customers Future Orders</h3>
            <p>If a new order is found for this customer then you can confirm with the customer delivery for this backorder on this ship date.</p>

        </div><!-- /.box-header -->

        <div class="box-body">

            @if(isset($futureorders[0]))
                <table class="table table-striped table-condensed table-hover">
                    <tr>
                        <th>SO Number</th>
                        <th>ShipDate</th>
                    </tr>
                    @foreach($futureorders AS $future)
                        <tr>
                            <td><a href="{{ route('admin.orders.show', $future->ID) }}">{{ $future->ReferenceNumber }}</a></td>
                            <td>{{ $future->ShipDate }}</td>
                        </tr>
                    @endforeach
                </table>
            @else
                <div class="alert alert-info">No future orders found</div>
            @endif
        </div>

    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Back Order Items - <b class="label label-success">In Stock</b></h3>
            <p>Backorder items that are in stock and can be booked out if confirmed with customer.</p>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">

                @if($order_items)

                    @php
                        $totweight = 0;
                        $totgp = 0;
                        $totprice = 0;

                    @endphp

                    <table id="backorder" class="table table-bordered table-condensed table-striped table-hover">

                        <tr>
                            <th width="90px">SO</th>
                            <th width="100px">Ordered Date</th>
                            <th width="80px">Weight</th>

                            <th width="150px">BackOrder Quantity</th>
                            <th>Stock On Hand</th>

                            <th>Price on SO</th>
                            <th>System Price</th>
                            <th>SubTotal</th>
                        </tr>
                    @foreach($order_items AS $order_item)

                        <tr>
                            <td colspan="20"><div class="h4"><b>{{ $order_item['name'] }}</b></div></td>
                        </tr>

                        @foreach($order_item['items'] AS $item)

                        <tr>
                            <td class="h4"><a class="label btn-primary text-bold" href="{{ route('admin.orders.show',$item->SalesOrderId ) }}">{{ $item->ReferenceNumber }}</a></td>

                            <td>{{ date("d-m-Y", strtotime($item->TimeCreated)) }}</td>

                            <td>
                                @php

                                $totgp += ($item->ItemRate-$item->AverageCost)*$item->ItemQuantity;
                                $totprice += $item->ItemRate*$item->ItemQuantity;

                                if(isset($item->CustomFields)) {
                                    $customfields = json_decode($item->CustomFields, true);
                                    if(isset($customfields['KgWeight']) && $customfields['KgWeight']) {
                                        $totweight += $customfields['KgWeight']*$item->ItemQuantity;

                                        echo $customfields['KgWeight']*$item->ItemQuantity-$item->ItemInvoicedAmount.'kg';
                                    }

                                }
                                @endphp
                            </td>

                            <td>
                                <b>{{ $item->ItemQuantity-$item->ItemInvoicedAmount }}</b>
                                @if(isset($has_order_items[$item->Name]))
                                    <span title="The product has been ordered and invoiced after the backorder" class="badge label-danger"><i class="fa fa-warning"></i> {{ $has_order_items[$item->Name]->NewOrderQty }} invoiced on new order <br/>{{ date("d-m-Y",strtotime($has_order_items[$item->Name]->Shipdate)) }} - confirm/close</span>
                                @endif
                            </td>

                            <td>{{ $item->QuantityOnHand }} {!! $item->BulkQty> 0 ?'<span class="label label-success h5">'.$item->BulkQty.' in '.$item->BulkName.'</span>':'' !!}</td>

                            <td>{!! \Helper::formatPrice($item->ItemRate) !!}</td>

                            <td class="{{ $item->ItemRate<$item->Price?"btn-danger":"" }}">{!! \Helper::formatPrice($item->Price) !!}</td>

                            <td>{!! \Helper::formatPrice($item->ItemRate*$item->ItemQuantity) !!}</td>

                        </tr>
                        @endforeach

                        @endforeach

                    </table>

                @endif
            </div>

        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Back Order Items - <b class="label label-danger">No Stock</b></h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">

                @if($order_items_ns)

                    @php
                        $totweight = 0;
                        $totgp = 0;
                        $totprice = 0;

                    @endphp

                    <table id="backorder" class="table table-bordered table-condensed table-striped table-hover">

                        <tr>
                            <th width="90px">SO</th>
                            <th width="100px">Ordered Date</th>
                            <th width="80px">Weight</th>

                            <th width="150px">BackOrder Quantity</th>
                            <th>Stock On Hand</th>

                            <th>Price on SO</th>
                            <th>System Price</th>
                            <th>SubTotal</th>
                        </tr>
                        @foreach($order_items_ns AS $iname => $order_item)

                            <tr>
                                <td colspan="20">
                                    <div class="h4">
                                        <b>{{ $order_item['name'] }}</b>
                                        @if(!strpos($order_item['name']," CP"))
                                            @if(isset($order_items_ns[$iname]['eta']))
                                                <span class="badge label-success"><i class="fa fa-flag"></i> ETA: {{ date("d-M-Y", strtotime($order_items_ns[$iname]['eta']->ExpectedDate)) }} of {{ $order_items_ns[$iname]['eta']->TotalPurchased }}</span>
                                            @else
                                                <span class="badge label-danger"><i class="fa fa-flag"></i> NO ETA</span>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            </tr>

                            @foreach($order_item['items'] AS $item)

                                <tr>
                                    <td class="h4">
                                        <a class="label btn-primary text-bold" href="{{ route('admin.orders.show',$item->SalesOrderId ) }}">{{ $item->ReferenceNumber }}</a>
                                    </td>

                                    <td>{{ date("d-m-Y", strtotime($item->TimeCreated)) }}</td>

                                    <td>
                                        @php

                                            $totgp += ($item->ItemRate-$item->AverageCost)*$item->ItemQuantity;
                                            $totprice += $item->ItemRate*$item->ItemQuantity;

                                            if(isset($item->CustomFields)) {
                                                $customfields = json_decode($item->CustomFields, true);
                                                if(isset($customfields['KgWeight']) && $customfields['KgWeight']) {
                                                    $totweight += $customfields['KgWeight']*$item->ItemQuantity;

                                                    echo $customfields['KgWeight']*$item->ItemQuantity-$item->ItemInvoicedAmount.'kg';
                                                }

                                            }
                                        @endphp
                                    </td>

                                    <td>
                                        <b>{{ $item->ItemQuantity-$item->ItemInvoicedAmount }}</b>
                                        @if(isset($has_order_items[$item->Name]))
                                            <span class="badge label-danger"><i class="fa fa-warning"></i> {{ $has_order_items[$item->Name]->NewOrderQty }} Already ordered <br/>{{ $has_order_items[$item->Name]->Shipdate }}</span>
                                        @endif
                                    </td>

                                    <td>
                                        {{ $item->QuantityOnHand }}
                                    </td>

                                    <td>{!! \Helper::formatPrice($item->ItemRate) !!}</td>

                                    <td class="{{ $item->ItemRate<$item->Price?"btn-danger":"" }}">{!! \Helper::formatPrice($item->Price) !!}</td>

                                    <td>{!! \Helper::formatPrice($item->ItemRate*$item->ItemQuantity) !!}</td>

                                </tr>
                            @endforeach

                        @endforeach

                    </table>

                @endif
            </div>

        </div><!-- /.box-body -->
    </div><!--box-->

@stop
