@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Packages Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Packages</h3>

            <div class="box-tools pull-right">
                @include('backend.package.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.package.action') }}" method="post" class="">
                    <table id="package-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Cost</th>
                            <th>Per Credit</th>
                            <th>Discount</th>
                            <th>Free</th>
                            <th>Created At</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($packages AS $package)
                            <tr>
                                <td width="30px"><input name="selected[]" type="checkbox" value="{{ $package->id }}"></td>
                                <td>{{ $package->id }}</td>
                                <td>{{ $package->title }}</td>
                                <td>{!! \Helper::formatPrice($package->cost) !!}</td>
                                <td>{!! \Helper::formatPrice($package->per_credit) !!}</td>
                                <td>{{ $package->discount_percent }}%</td>
                                <td>{{ $package->free }}</td>
                                <td>{{ $package->created_at }}</td>
                                <td>{!! $package->action_buttons !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="pull-right">{{ $packages->links() }}</div>

                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
