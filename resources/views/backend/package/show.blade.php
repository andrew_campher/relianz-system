@extends ('backend.layouts.app')

@section ('title', 'Packages | Show')

@section('page-header')
    <h1>
        Package Management
        <small>View Package</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $package->professional->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            {!! $package->status_label !!}

            @include('frontend.package.includes.package_template')

        </div><!-- /.box-body -->
    </div><!--box-->

    @if($package->status == 0)
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Add Payment</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            @include('backend.package.includes.add_payment')

        </div><!-- /.box-body -->
    </div><!--box-->
    @endif

@stop