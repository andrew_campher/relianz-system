@php($collapse = 1)
@extends('backend.layouts.app')

@section ('title', 'Pricing')

@section('page-header')
        <div class="row">
            <div class="col-md-2">
                <h2 class="no-margin">Pricing</h2>
            </div>
            <div class="col-md-10 text-right">
                <div>
                    <form action="" method="get" class="form-inline">
                        <div class="form-group">
                            <select class="form-control" id="pricelist" name="pricelist">
                                <option value="">ALL</option>
                                <option {{ $request->pricelist == 'B' ? 'selected' : ''  }} value="B">Bulk</option>
                                <option {{ $request->pricelist == 'C' ? 'selected' : ''  }} value="C">Cater</option>
                            </select>

                            <select class="form-control" id="pricelevelID" name="pricelevelID">
                                <option value="">-- BASE PRICING --</option>
                                @foreach($pricelevels AS $pricelevel)
                                    <option {{ $pricelevel->ID == $request->pricelevelID ? 'selected' : ''  }} value="{{ $pricelevel->ID }}">{{ $pricelevel->Name }}</option>
                                @endforeach
                            </select>

                            <span title="Only show price level items"><input {{ $request->onlylevel ? 'checked' : ''  }} type="checkbox" name="onlylevel" value="1" /> <b>Only</b></span>

                            <select class="form-control" id="customerID" name="customerID">
                                <option value="">-- Customer --</option>
                                @foreach($customers AS $customer)
                                    <option {{ $customer->ID == $request->customerID ? 'selected' : ''  }} value="{{ $customer->ID }}">{{ $customer->FullName }}</option>
                                @endforeach
                            </select>

                            <span title="Only show customers sales lines"><input {{ $request->onlysales ? 'checked' : ''  }} type="checkbox" name="onlysales" value="1" /> <b>Only Sales</b></span>

                            <select class="form-control" id="customerType" name="customerType">
                                <option value="">-- Sales Stats --</option>
                                @foreach($CustomerTypes AS $CustomerType)
                                    <option {{ $CustomerType->Name == $request->customerType ? 'selected' : ''  }} value="{{ $CustomerType->Name }}">{{ $CustomerType->Name }}</option>
                                @endforeach
                            </select>

                            <button type="submit" class="btn btn-primary">submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection

        @section('content')

            <div class="box box-primary">

                <div class="box-body">

            @if(isset($items) && $items)

                <div class="row margin-bottom">
                    <div class="col-md-12 form-inline">
                        Notes: <input class="form-control pricein text-bold" data-name="price_notes" data-item="{{ $request->pricelevelID }}" size="50" name="price_notes" id="sttlmnt" value="{{ isset($thepricelvl['notes']) && $thepricelvl['notes']?$thepricelvl['notes']:'' }}" />
                        Settlement % <input class="form-control" size="4" name="sttlmnt" id="sttlmnt" value="{{ isset($thecustomer->settlement_discount) && $thecustomer->settlement_discount>0?$thecustomer->settlement_discount:$SettlementDisc }}" />
                        Rands per KG <input class="form-control pricein" size="4" data-name="RperKG" data-item="{{ $request->pricelevelID }}" name="RperKG" value="{{ isset($thecustomer->rands_per_kg) && $thecustomer->rands_per_kg?$thecustomer->rands_per_kg:$thepricelvl['RperKG'] }}" />
                        % Discount <input class="form-control pricein" size="4" data-name="perc_change" data-item="{{ $request->pricelevelID }}" name="perc_change" value="{{ isset($thecustomer->discount_perc) && $thecustomer->discount_perc?$thecustomer->discount_perc:$thepricelvl['perc_change'] }}" />
                        Marketing % <input class="form-control pricein" size="4" data-name="ad_perc" data-item="{{ $request->pricelevelID }}" name="ad_perc" value="{{ isset($thecustomer->marketing_perc) && $thecustomer->marketing_perc?$thecustomer->marketing_perc:$thepricelvl['ad_perc'] }}" />
                    </div>
                </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="" style="width:100%" class="table-bordered table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Unit</th>
                                <th>Qty on Hand</th>
                                @if($request->customerID || $request->customerType)
                                <th>Past Sales</th>
                                @endif
                                @if($request->customerID)
                                <th>Last Paid</th>
                                @endif
                                <th>Cost</th>
                                <th>Price</th>
                                <th>Base GP</th>
                                @if($request->pricelevelID)
                                <th>Custom Price</th>
                                @endif
                                <th>Comments</th>
                                <th>New Price</th>
                                @if(!$request->pricelevelID)
                                <th>New Cost</th>
                                <th>Special Price</th>
                                @endif
                                <th>Special End</th>
                                <th>List Price</th>
                                <th>Internal Comments</th>
                                @if($request->pricelevelID)
                                <th>List Comments</th>
                                @endif
                                <th>GP %</th>
                                @if($request->pricelevelID)
                                <th>Target Price</th>
                                @endif
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($items AS $item)
                                <tr>
                                    <td>
                                        <a class="text-black" target="_blank" href="{{ route('admin.items.report', $item->ID) }}">{{ $item->Product }}</a>
                                    </td>
                                    <td>@php
                                        if (strpos(strtoupper($item->PriceList), "C") !== FALSE) {
                                            echo '<span class="label label-warning">C</span>';
                                        }
                                        if (strpos(strtoupper($item->PriceList), "B") !== FALSE) {
                                            echo '<span class="label label-primary">B</span>';
                                        }
                                    @endphp
                                        <a class="text-black" href="{{ route('admin.items.edit', $item->ID) }}">{{ $item->Unit }}</a></td>
                                    <td>{{ $item->QuantityOnHand }}</td>
                                    @if($request->customerID || $request->customerType)
                                    <td class="text-bold text-center">
                                        {{ $item->PastYearQtySales }}
                                    </td>
                                    @endif
                                    @if($request->customerID)
                                    <td>{{ $item->LastPaid }}</td>
                                    @endif
                                    <td class="text-center">{{ $item->PurchaseCost }}</td>
                                    <td class="text-bold text-center">
                                        {{ $item->Price }}
                                    </td>
                                    @php

                                    $basegp = $item->Price>0?number_format(($item->Price-$item->PurchaseCost)/$item->Price*100,2):0;
                                    @endphp
                                    <td class="{{ $item->Price>0 && $basegp <10?"bg-warning btn-warning":$item->Price>0 && $basegp <5?"bg-danger":"" }}" >{{  $basegp."%" }}</td>
                                    @if($request->pricelevelID)
                                    <td class="border-left table-bordered small">{{ $item->CustomPrice }}</td>
                                    @endif
                                    <td><textarea data-item="{{ $item->ID }}" data-name="comments" style="width: 140px" class="transin pricein small" >{{ $item->Comments }}</textarea></td>
                                    <td class="bg-info @php
                                            if (!$request->pricelevelID && $item->new_price > 0 && $item->special_end < date("Y-m-d") && $item->new_price != $item->Price) {
                                                echo 'btn-danger';
                                            }
                                            if(!$item->new_price && $item->PastYearQtySales>0 && $request->pricelevelID) {
                                                echo 'btn-danger';
                                            }
                                        @endphp"
                                    ><input data-item="{{ $item->ID }}" data-name="new_price" style="width: 80px" class="transin pricein text-bold text-center" type="text" value="{{ (float)$item->new_price }}" /></td>
                                    @if(!$request->pricelevelID)
                                    <td><input data-item="{{ $item->ID }}" data-name="new_cost" style="width: 80px" class="transin pricein text-center" type="text" value="{{ (float)$item->new_cost }}" /></td>
                                    <td class="bg-warning
                                            @php
                                                if ($item->special_price > 0 && $item->special_end >= date("Y-m-d") && ($item->special_price != $item->Price || ($request->pricelevelID && $item->special_price != $item->new_price))) {
                                                    echo "btn-danger";
                                                }
                                            @endphp">
                                        <input data-item="{{ $item->ID }}" data-name="special_price" style="width: 80px" class="transin pricein text-center" type="text" value="{{ (float)$item->special_price }}" />
                                    </td>
                                    @endif
                                    <td class="@php
                                        if ($item->special_price> 0 && $item->special_end >= date("Y-m-d")) {
                                            echo "btn-success";
                                        }
                                    @endphp">
                                        <input data-item="{{ $item->ID }}" data-name="special_end" style="width: 120px" class="transin pricein text-center" type="date" value="{{ $item->special_end != '0000-00-00' ?$item->special_end:"" }}" />
                                    </td>
                                    <td class="bg-gray @php
                                                if ($item->list_price > 0 && !$item->new_price &&  $item->special_end < date("Y-m-d") && $item->list_price != $item->Price) {
                                                    echo "btn-danger";
                                                }
                                            @endphp"
                                    ><input data-item="{{ $item->ID }}" data-name="list_price" style="width: 80px" class="transin pricein text-center" type="text" value="{{ (float)$item->list_price }}" /></td>
                                    <td><textarea data-item="{{ $item->ID }}" data-name="internal_comments" style="width: 100px" class="transin pricein small" >{{ $item->internal_comments }}</textarea></td>
                                    @if($request->pricelevelID)
                                    <td class="small">{{ trim($item->ListComments) }}</td>
                                    @endif
                                    @php
                                        $current_gp = 0;

                                        if ($request->pricelevelID) {
                                            $price = $item->new_price>0?$item->new_price:0;
                                        } else {
                                            if ($item->special_price> 0 && $item->special_end >= date("Y-m-d") && $item->special_price != $item->Price) {
                                                $price = $item->special_price;
                                            } else {
                                                $price = $item->new_price>0?$item->new_price:$item->Price;
                                            }
                                        }


                                        $cost = $item->new_cost>0?$item->new_cost:$item->PurchaseCost;

                                        if ($price>0) {
                                            $current_gp = ($price-($cost+($price*($SettlementDisc+$thepricelvl['perc_change']+$thepricelvl['ad_perc']))+($thepricelvl['RperKG']*$item->Weight)))/$price;
                                        }
                                    @endphp
                                    <td class="text-center text-bold {{ $price>0 && $current_gp <0.10?"bg-warning":$price>0 && $current_gp <0?"bg-danger btn-danger":"" }}">{{ $current_gp ?number_format($current_gp*100,2)."%":""  }}</td>
                                        @php
                                            $baseprice = $item->Price>0?$item->Price:0;

                                            $target_price = 0;

                                            if ($baseprice>0 && $item->Price > 0 && $item->PurchaseCost > 0) {
                                                //Cost + transport / (1 - base GP + all discount percents)
                                                $target_price = ($cost+($thepricelvl['RperKG']*$item->Weight))/(1-((($item->Price-$item->PurchaseCost)/$item->Price)+$SettlementDisc+$thepricelvl['perc_change']+$thepricelvl['ad_perc']));
                                            }
                                        @endphp
                                    @if($request->pricelevelID)
                                    <td class="text-center bg-success text-bold">{{ number_format($target_price,2) }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif

        </div>

    </div>

@endsection

@section('after-scripts-end')
    <script>

        $(document).ready(function(){
            var current;
            var e;

            $('.transin').focus(function () {
                $(this).closest('tr').css('background-color','yellow');
            }).blur(function () {
                $(this).closest('tr').css('background-color','');
            });

            $('.pricein').on('change',function () {

                e = $(this);

                $.ajax({
                    url: "{{ route("api.pricing.update") }}",
                    type: "post",
                    data: {
                        "pricelevelID": $('#pricelevelID').val(),
                        "itemID": e.attr('data-item'),
                        "customerID": "{{ $request->customerID }}",
                        "name": e.attr('data-name'),
                        "value": e.val()
                    },
                    beforeSend: function() {
                        e.css({'border-color':'red'});
                    },
                    success: function(d) {
                        e.css({'border-color':''});
                    },
                    error: function(d) {

                    }
                });
            });

        });
    </script>
@append
