@extends('backend.layouts.app')

@section ('title', 'Specials')

@section('page-header')
    <div class="row">
        <div class="col-md-3">
            <h2 class="no-margin">Current Specials</h2>
        </div>
    </div>
@endsection

@section('content')

    <div class="box box-primary">

        <div class="box-body">

        @if(isset($items) && $items)

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="specialstable" class="table table-bordered table-striped table-hover dataTable fixed-header">
                        <thead>
                            <tr>
                                <th>Supplier</th>
                                <th>Product</th>
                                <th>Past 6 Mnths Sales</th>
                                <th>Special Price</th>
                                <th>Per Unit / KG</th>
                                <th>Date End</th>
                                <th>Price Lists</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($items AS $item)
                            <tr>
                                <td>{{ $item->PreferredVendor }}</td>
                                <td><a class="" href="{{ route('admin.items.report', $item->ID) }}">{{ $item->Description }}</a></td>
                                <td>{!! $item->PastYearQtySales !!}</td>
                                <td>{!! \Helper::formatPrice($item->special_price) !!}</td>
                                <td>{!! \Helper::formatPrice($item->special_price/\App\Helpers\Helper::getKGUnit($item->unit, $item->weight)) !!}</td>
                                <td>{{ $item->special_end }}</td>
                                <td>{{ $item->pricelist }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif

        </div>
    </div>


@endsection

@section('after-scripts-end')

    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#specialstable').DataTable({
                'paging'      : false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'responsive' : true,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ]
            })
        })
    </script>
@endsection