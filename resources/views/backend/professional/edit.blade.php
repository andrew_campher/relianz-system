@extends ('backend.layouts.app')

@section ('title', 'Professional | Manager')

@section('page-header')
    <h1>
        Professionals Management
        <small>Edit</small>
    </h1>
@endsection

@section('content')

    @if( isset($professional) )
        {{ Form::model($professional, ['route' => ['admin.professional.update', $professional], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'novalidate']) }}
    @else
        {{ Form::open(['route' => 'admin.professional.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'novalidate']) }}
    @endif

    <div class="form-group">
        {{ Form::label('user_id', 'User', ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            {{ Form::select('user_id', $users, null, ['placeholder' => '-- Select Owner --', 'class' => 'form-control']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('status', 'Status', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::select('status', [0 => 'Pending',1 => 'Active',2 => 'Unconfirmed'], null, ['class' => 'form-control', 'placeholder' => 'Select Status']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('verify_status', 'Verified', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::checkbox('verify_status', 1, ['class' => 'form-control']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#info" aria-controls="home" role="tab" data-toggle="tab">Business Info</a></li>
        <li role="presentation"><a href="#branch" aria-controls="messages" role="tab" data-toggle="tab">Branches</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile Page</a></li>
        <li role="presentation"><a href="#system" aria-controls="messages" role="tab" data-toggle="tab">Settings</a></li>
    </ul>

    <div class="tab-content">
        @include('frontend.professional.includes.form_fields')
    </div>


    <div class="box box-success">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.question.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::close() }}
@stop

