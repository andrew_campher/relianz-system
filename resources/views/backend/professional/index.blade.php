@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Professionals Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Professionals</h3>

            <div class="box-tools pull-right">
                @include('backend.professional.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.professional.action') }}" method="post" class="">
                    <table id="professional-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Approved</th>
                            <th>Verified</th>
                            <th>User</th>
                            <th>Status</th>

                            <th>Created At</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($professionals AS $professional)
                            <tr>
                                <td width="30px"><input name="selected[]" type="checkbox" value="{{ $professional->id }}"></td>
                                <td>{{ $professional->id }}</td>

                                <td><a href="{{ route('public.professional.show', [$professional->id, $professional->slug]) }}">{{ $professional->title }} {!! $professional->verify_label  !!}</a></td>
                                <td>{!! $professional->approved_label !!}</td>
                                <td>{!! $professional->verified_label !!}</td>
                                <td>
                                    @if($professional->user)
                                    <a href="{{ route('admin.access.user.show', $professional->user->id) }}">{{ $professional->user->name }}</a>
                                    @endif
                                </td>
                                <td>{!! $professional->status_label !!}</td>

                                <td>{{ $professional->created_at }}</td>
                                <td>{!! $professional->action_buttons !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="pull-right">{{ $professionals->links() }}</div>

                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
