@extends ('backend.layouts.app')

@section ('title', 'Services | Show')

@section('page-header')
    <h1>
        {{ trans('labels.backend.service.management') }}
        <small>View Service</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $professional->title }} {!! $professional->verify_label  !!}</h3>

            {!! $professional->image_html !!}
        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>Description</th>
                    <td>{{ $professional->description }}</td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td>{!! $professional->approved_label !!}</td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td>{!! $professional->status_label !!}</td>
                </tr>

                <tr>
                    <th>Created At</th>
                    <td>{{ $professional->created_at }} ({{ $professional->created_at->diffForHumans() }})</td>
                </tr>


                @if($professional->status == 2)
                    @php
                        $pro_hash = md5($professional->id.$professional->notify_email);

                    @endphp
                <tr>
                    <th>Confirm Listing URL</th>
                    <td><input class="form-control" value="{{ route('public.professional.edit',[$professional->id, $pro_hash]) }}" onclick="this.select()"></td>
                </tr>
                @endif


                @if ($professional->trashed())
                    <tr>
                        <th>{{ trans('labels.backend.service.tabs.content.overview.deleted_at') }}</th>
                        <td>{{ $professional->deleted_at }} ({{ $professional->deleted_at->diffForHumans() }})</td>
                    </tr>
                @endif
            </table>

        </div><!-- /.box-body -->
    </div><!--box-->


@stop