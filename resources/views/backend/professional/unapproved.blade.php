@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.professional.management') . ' | ' . trans('labels.backend.professional.deactivated'))

@section('after-styles-end')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@stop

@section('page-header')
    <h1>
        Professionals Management
        <small>Un-Approved Professionals</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Pending Approval</h3>

            <div class="box-tools pull-right">
                @include('backend.professional.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="professionals-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ trans('labels.backend.general.table.image') }}</th>
                        <th>{{ trans('labels.backend.general.table.title') }}</th>
                        <th>{{ trans('labels.backend.general.table.created') }}</th>
                        <th>{{ trans('labels.backend.general.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@stop

@section('after-scripts-end')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

    <script>
        $(function() {
            $('#professionals-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.professional.get") }}',
                    type: 'post',
                    data: {status: 1, trashed: false, approved: 0}
                },
                columns: [
                    {data: 'id', name: 'professionals.id'},
                    {data: 'image_html', name: 'professionals.image_html'},
                    {data: 'title', name: 'professionals.title', render: $.fn.dataTable.render.text()},
                    {data: 'created_at', name: 'professionals.created_at'},
                    {data: 'updated_at', name: 'professionals.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@stop
