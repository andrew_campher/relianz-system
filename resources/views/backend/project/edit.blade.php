@extends ('backend.layouts.app')

@section ('title', 'Project | Manager')

@section('page-header')
    <h1>
        Projects Management
        <small>Edit</small>
    </h1>
@endsection

@section('content')
    @if( isset($project) )
        {{ Form::model($project, ['route' => ['admin.project.update', $project], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.project.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Project</h3>

            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    {{ Form::label('user_id', 'User', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('user_id', $users, null, ['placeholder' => '-- Select Owner --', 'class' => 'form-control']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('professional_id', 'Professional', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('professional_id', $professionals->prepend('-- UnHired --'), null, ['class' => 'form-control', 'readonly']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('service_id', 'Service', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('service_id', $services, null, ['placeholder' => '-- Select Service --', 'class' => 'form-control']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('description', 'Description', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'description required']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <h2>Answers</h2>

                @if(isset($answers) && $answers)
                    @foreach($answers AS $answer)
                        @if(is_array($answer))
                            @foreach($answer AS $key => $ans)
                                @if($ans->question)
                                <div class="answer_wrap">
                                    @if($key == 0)
                                        <hr>
                                        <div class="h6"><strong>{{$ans->question->title}}</strong>
                                        </div>
                                        <p>{{ Form::text('answer_'.$ans->id, $ans->answer_value, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}</p>
                                    @else
                                        <p>{{ Form::text('answer_'.$ans->id, $ans->answer_value, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}</p>
                                    @endif
                                </div>
                                @endif

                            @endforeach
                        @else
                            <hr>

                            <div class="answer_wrap">
                                <div class="h6">{{$answer->question->title}}</div>
                                <p><i class="fa fa-d"></i> {{$answer->answer_value}}</p>
                            </div>

                        @endif

                    @endforeach
                @endif

                <h2>Contact</h2>

                <div class="form-group">
                    {{ Form::label('customer_mobile', 'Customer Mobile', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('customer_mobile', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('customer_email', 'Customer Email', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('customer_email', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->


                <div class="form-group">
                    {{ Form::label('area', 'Location', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        @php($gmap_marker['draggable'] = 'true')
                        @include('frontend.wizard.custom.geocode', ['address' => $project->area, 'settings' => ['types' => '']])
                    </div><!--col-lg-10-->
                </div><!--form control-->


                <div class="form-group">
                    {{ Form::label('galleries', 'Photo Gallery', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        @if(isset($project))
                            @if($project->media)
                                @include('frontend.media.includes.partials.gallery', ['media' => $project->media, 'edit' => 1])
                            @endif
                            @include('frontend.media.includes.partials.multi_uploader')

                        @endif
                    </div><!--col-lg-10-->
                </div>


            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.question.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

