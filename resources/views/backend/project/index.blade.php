@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Projects Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Projects</h3>

            <div class="box-tools pull-right">
                @include('backend.project.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->

            <ul class="nav nav-tabs">
                <li class="{{ $status == 1 ? 'active' :'' }}"><a href="{{ route('admin.project.status', 1) }}">Open</a></li>
                <li class="{{ $status == '0' ? 'active' :'' }}"><a href="{{ route('admin.project.status', 0) }}">Pending <span class="badge label-danger">{{ $pending_projects_count }}</span> </a></li>
                <li class="{{ $status == 2 ? 'active' :'' }}"><a href="{{ route('admin.project.status', 2) }}">Hired</a></li>
                <li class="{{ $status == 3 ? 'active' :'' }}"><a href="{{ route('admin.project.status', 3) }}">Complete</a></li>
                <li class="{{ $status == 4 ? 'active' :'' }}"><a href="{{ route('admin.project.status', 4) }}">Closed</a></li>
                <li class="{{ $status == 5 ? 'active' :'' }}"><a href="{{ route('admin.project.status', 5) }}">Expired</a></li>
                <li class="{{ is_null($status) ? 'active' :'' }}"><a href="{{ route('admin.project.index') }}">All</a></li>
            </ul>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.project.action') }}" method="post" class="">
                    <table id="project-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>User</th>
                            <th>Service</th>
                            <th>Quotes</th>
                            <th>When</th>
                            <th>Area</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects AS $project)
                            <tr>
                                <td width="30px"><input name="selected[]" type="checkbox" value="{{ $project->id }}"></td>
                                <td>{{ $project->id }}</td>
                                <td>{{ $project->title }}</td>
                                <td>{{ isset($project->user)?$project->user->name:'No User' }}</td>
                                <td>{!!  isset($project->service) && $project->service?$project->service->title:'<span class="badge label-danger">No Service</span>' !!}</td>
                                <td>{{ $project->quote_count }}</td>
                                <td>{!! $project->when_label !!}</td>
                                <td>{{ $project->area }}</td>
                                <td>{!! $project->status_label !!}</td>
                                <td>{{ $project->created_at }}</td>
                                <td>{!! $project->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="pull-right">{{ $projects->links() }}</div>

                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
