@extends ('backend.layouts.app')

@section ('title', 'Projects | Show')

@section('page-header')
    <h1>
        Projects Management
        <small>View Project</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $project->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>Description</th>
                    <td>{{ $project->description }}</td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td>{!! $project->status_label !!}</td>
                </tr>

                <tr>
                    <th>Created At</th>
                    <td>{{ $project->created_at }} ({{ $project->created_at->diffForHumans() }})</td>
                </tr>

                <tr>
                    <th>Last Update</th>
                    <td>{{ $project->updated_at }} ({{ $project->updated_at->diffForHumans() }})</td>
                </tr>

                @if ($project->trashed())
                    <tr>
                        <th>{{ trans('labels.backend.project.tabs.content.overview.deleted_at') }}</th>
                        <td>{{ $project->deleted_at }} ({{ $project->deleted_at->diffForHumans() }})</td>
                    </tr>
                @endif

                @if($pros)
                    <tr>
                        <th>Approve</th>
                        <td>{!!  $project->approved_button !!}</td>
                    </tr>
                @else

                @endif
            </table>


            <div class="answer_item">
                <table class="table table-striped table-hover">
                    <tr>
                        <th style="border-top: none">Status</th>
                        <td style="border-top: none">{!!  $project->StatusLabel !!}</td>
                    </tr>
                    <tr>
                        <th>When</th>
                        <td>{!! $project->whenlabel !!} {{ $project->when_describe }}</td>
                    </tr>

                    @if($project->description)
                        <tr>
                            <th colspan="2">Description</th>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $project->description }}</td>
                        </tr>
                    @endif
                </table>

                @include('frontend.project.includes.partials.answers')

            </div>

            <div class="clearfix">

                @include('frontend.media.includes.partials.gallery', ['media' => $project->media])

            </div>

        </div><!-- /.box-body -->
    </div><!--box-->


    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Professionals in Service Range [Service ID: {{ $project->service_id }}] {{ $project->service ? $project->service->title: 'My'  }}</h3>
            <p>These professionals will be notified</p>

        </div><!-- /.box-header -->

        <div class="box-body">

            {{ Form::open(['route' => ['admin.project.resend_project_alert', $project->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
            <table class="table table-striped table-hover">
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Travel Range</th>
                    <th>Distance</th>
                    <th>Website</th>
                    <th>Tel</th>
                    <th>Address</th>
                    <th>Notify Email</th>
                    <th>Open Stat</th>
                    <th></th>
                </tr>
               @if($pros)
                   @foreach($pros AS $pro)
                   <tr>
                       <td><input type="checkbox" name="selected[]" value="{{ $pro->id }}"></td>
                       <td>{{ $pro->id }}</td>
                       <td><a href="{{ route('admin.professional.edit', $pro->professional_id) }}">{{ $pro->title }}</a></td>
                       <td>{!! $pro->status_label !!}</td>
                       <td>{{ $pro->distance_travelled }} KM</td>
                       <td>{{ $pro->distance }} KM</td>
                       <td>{{ $pro->website_url?'<a href="'.$pro->website_url.'">'.$pro->website_url.'</a>':'' }}</td>
                       <td>{{ $pro->telephone }}</td>
                       <td>{{ $pro->area }}</td>
                       <td>{{ $pro->notify_email }}</td>
                       <td width="150px">
                       @if($sent = $pro->entityNotifiableSentStats('project', $project->id, 'professional_branch', $pro->id))
                           <b title="Opened emails" class="badge label-primary">{{ $sent['opened'] }}</b>/{{ $sent['sent'] }} Sent <div class="badge label-success">{{ $sent['clicks'] }} clicks</div><br/>
                               {{ $sent['last_sent'] }}
                       @elseif($sent = $pro->emailSentStats($pro->notify_email))
                           <small>Emails <b title="Opened emails" class="badge">{{ $sent['opened'] }}</b>/{{ $sent['sent'] }} Sent <div class="badge label-success">{{ $sent['clicks'] }} clicks</div><br/>
                               {{ $sent['last_sent'] }}</small>
                       @else
                           <span class="badge label-default">No mail sent</span>
                       @endif
                       </td>
                       <td><a class="badge label-danger" href="{{ route('admin.professional.service.remove', [$pro->professional_id, $project->service_id ]) }}">Remove Service</a></td>
                   </tr>

                   @endforeach

                   @else
                   <tr>
                       <td colspan="50" class="alert alert-danger">No professionals in range</td>
                   </tr>
               @endif
            </table>

            <p>
                <button class="btn btn-primary" type="submit">Submit</button>
            </p>

            {{ Form::close() }}

            <hr>

            {{ Form::open(['route' => 'admin.professional.quick_store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
            <div>
                <h3>Quick Add Professional Contact</h3>

                <div class="form-group">
                    {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string' , 'autocomplete'=>'off']) }}
                        <div style="display: none" id="company_search" class="suggestresults">
                            <div class="h6 badge label-warning">Claim existing listing:</div>
                            <a onclick="$('#company_search').hide()" href="javascript:void(0)"><i class="fa fa-close fa-lg"></i> close</a>
                        </div>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('branch[notify_email]', 'Email', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::email('branch[notify_email]', null, ['class' => 'form-control', 'placeholder' => 'Email address', 'required']) }}
                        <div class="help-block with-errors"></div>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('branch[notify_mobile]', 'Mobile (optional)', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::number('branch[notify_mobile]', null, ['class' => 'form-control mobile_input', 'placeholder' => 'eg. 0821234567', 'data-minlength'=>'10', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('branch[telephone]', 'Telephone', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::number('branch[telephone]', null, ['class' => 'form-control mobile_input', 'placeholder' => 'eg. 0821234567', 'data-minlength'=>'10', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <h4>Location</h4>

                <div class="form-group">
                    {{ Form::label('optArea', 'Location', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('branch[area]', null, ['id' => 'optArea', 'checkme', 'class' => 'form-control input-clean input-lg', 'placeholder' => 'Type area here...', 'required']) }}

                        {{ Form::hidden('branch[lat]', null, ['id' => 'lat']) }}
                        {{ Form::hidden('branch[lng]', null, ['id' => 'lng']) }}

                        @include('frontend.wizard.custom.geocode', ['settings' => ['types' => '', 'disable_map' => true, 'exclude_fields'=>1]])
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <hr>

                <div class="form-group">
                    {{ Form::label('services', 'Your Services', ['class' => 'col-lg-2 control-label', 'id' => 'services']) }}

                    <div class="col-lg-10">
                        @include('backend.service.includes.partials.service-list', ['services' => isset($professional) ?$professional->services:null])
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <hr>

                <div class="pull-right">
                    {{ Form::submit('CREATE', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>

            </div>

            {{ Form::hidden('services[]', $project->service_id) }}
            {{ Form::hidden('approved', 1) }}
            {{ Form::hidden('status', 2) }}

            {{ Form::close() }}

        </div><!-- /.box-body -->
    </div><!--box-->


@stop
@section('after-scripts-end')
    <script>
        var ss = '';
        var timer = 0;
        var currentRequest = null;

        $('#title').on('keyup focus', function() {
            var el = $(this);
            var searchstring = el.val();

            if (ss == searchstring) return false;

            ss = searchstring;

            clearTimeout(timer);

            if (searchstring.length < 2 && searchstring.length > 0) return false;


            timer = setTimeout(function(){
                currentRequest = $.ajax({
                    url: '{{ route("api.professional.search") }}',
                    method: "GET",
                    dataType: "json",
                    data:  { string: searchstring },
                    beforeSend : function()    {
                        if(currentRequest != null) {
                            currentRequest.abort();
                        }
                    },
                    success: function (response) {
                        if (response.length != 0) {
                            $('#company_search .search_list').remove();

                            var items = [];
                            $.each(response, function (key, val) {
                                items.push("<li><a href='/professional/" + val.id + "-" + val.slug + "?claim=1'><i class='fa fa-star-o'></i> " + val.title + "</a></li>");
                            });

                            $("<ul/>", {
                                "class": "search_list servicelist list-unstyled",
                                html: items.join("")
                            }).appendTo('#company_search');

                            $('#company_search').show();
                        }
                    },
                });
            },200);
        });
    </script>

@append