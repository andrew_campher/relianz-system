@extends ('backend.layouts.app')

@section ('title', 'Reviews | Manager')

@section('page-header')
    <h1>
        Reviews Management
        <small>Edit</small>
    </h1>
@endsection

@section('content')
    @if( isset($project_review) )
        {{ Form::model($project_review, ['route' => ['admin.project_review.update', $project_review], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.project_review.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Reviews</h3>

            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('cost', 'Cost', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('cost', null, ['class' => 'form-control', 'placeholder' => 'Cost of project_review']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('per_credit', 'Per Credit Cost', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('per_credit', null, ['class' => 'form-control', 'placeholder' => 'The per credit cost on this project_review']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('discount_percent', 'Discount Percent', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('discount_percent', null, ['class' => 'form-control', 'placeholder' => 'The discount percent']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('free', 'Free', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('free', null, ['class' => 'form-control', 'placeholder' => 'Free project_review?']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->


            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.project_review.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

