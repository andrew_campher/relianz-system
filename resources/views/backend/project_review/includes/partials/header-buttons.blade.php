<div class="pull-right mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
           Reviews <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.project_review.index', 'All Reviews') }}</li>

            @permission('manage-users')
                <li>{{ link_to_route('admin.project_review.create', 'Create Reviews') }}</li>
            @endauth

        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>
