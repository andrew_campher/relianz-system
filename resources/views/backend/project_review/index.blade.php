@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Reviews Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Reviews</h3>

            <div class="box-tools pull-right">
                @include('backend.project_review.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.project_review.action') }}" method="post" class="">
                    <table id="project_review-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>User</th>
                            <th>Comments</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($project_reviews AS $project_review)
                            <tr>
                                <td width="30px"><input name="selected[]" type="checkbox" value="{{ $project_review->id }}"></td>
                                <td>{{ $project_review->id }}</td>
                                <td>{{ $project_review->user->name }}</td>
                                <td>{{ $project_review->comments }}</td>
                                <td>{!! $project_review->status_label !!}</td>
                                <td>{{ $project_review->created_at }}</td>
                                <td>{!! $project_review->action_buttons !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="pull-right">{{ $project_reviews->links() }}</div>

                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
