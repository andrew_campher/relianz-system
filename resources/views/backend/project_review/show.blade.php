@extends ('backend.layouts.app')

@section ('title', 'Reviews | Show')

@section('page-header')
    <h1>
        Reviews Management
        <small>View Reviews</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $project_review->professional->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            {!! $project_review->status_label !!}

            @include('frontend.project_review.includes.project_review_template')

        </div><!-- /.box-body -->
    </div><!--box-->

@stop