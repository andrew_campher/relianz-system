@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Questions Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">Questions</h3>

            <div class="box-tools pull-right">
                @include('backend.question.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.question.action') }}" method="post" class="">
                    <table id="question-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Approved</th>
                            <th>User</th>
                            <th>Status</th>

                            <th>Created At</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($questions AS $question)
                            <tr>
                                <td width="30px"><input name="selected[]" type="checkbox" value="{{ $question->id }}"></td>
                                <td>{{ $question->id }}</td>

                                <td><a href="{{ route('admin.question.show', [$question->id, $question->slug]) }}">{{ $question->title }} {!! $question->verify_label  !!}</a></td>
                                <td>{!! $question->approved_label !!}</td>
                                <td>
                                    @if($question->user)
                                        <a href="{{ route('admin.access.user.show', $question->user->id) }}">{{ $question->user->name }}</a>
                                    @endif
                                </td>
                                <td>{!! $question->status_label !!}</td>

                                <td>{{ $question->created_at }}</td>
                                <td>{!! $question->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="pull-right">{{ $questions->links() }}</div>

                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
