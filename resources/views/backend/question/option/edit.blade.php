@extends ('backend.layouts.app')

@section ('title', 'Options Management | Create Question')

@section('page-header')
    <h1>
        Options Management
        <small>Edit Options</small>
    </h1>
@endsection

@section('content')
    @if( isset($option) )
        {{ Form::model($option, ['route' => ['admin.question.option.update', 'question'=> $question->id, 'option'=> $option->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => ['admin.question.option.store', 'question'=> $question->id], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Create <b>{{$question->title }}</b> Option</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('set_value', 'Value', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('set_value', null, ['class' => 'form-control', 'placeholder' => 'Value']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('description', 'Description', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('special', 'Special', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('special', null, ['class' => 'form-control', 'placeholder' => 'Question Type']) }}

                    <h4>Specials Examples</h4>
                    <ul>
                        <li>FILES UPLOADER: files|Add Photos|files</li>
                        <li>SHOW NEW ELEMENT WHEN SELECTED: [show | input_type OR view | input_name | Title above input]
                            <ul>
                                <li>show|textarea|when_describe|When would you like to schedule this for?</li>
                                <li>show|datepicker|specific_date</li>
                                <li>show|textarea|description|Tell us more</li>
                                <li>show|textarea|other|Tell us more</li>
                            </ul>
                        </li>
                        <li>SHOW VIEW: "contact_method"</li>
                    </ul>
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::hidden('question_id', $question->id) }}

    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.question.show', trans('buttons.general.cancel'), ['question' => $question->id], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::close() }}
@stop

@section('after-scripts-end')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop
