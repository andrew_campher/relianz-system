@extends ('backend.layouts.app')

@section ('title', 'Questions | Show')

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>View Question</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $question->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>Description</th>
                    <td>{{ $question->description }}</td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td>{!! $question->status_label !!}</td>
                </tr>


                @if ($question->trashed())
                    <tr>
                        <th>{{ trans('labels.backend.access.users.tabs.content.overview.deleted_at') }}</th>
                        <td>{{ $question->deleted_at }} ({{ $question->deleted_at->diffForHumans() }})</td>
                    </tr>
                @endif
            </table>

        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Question Options</h3>

            <div class="pull-right mb-10">
                {{ link_to_route('admin.question.option.create', 'Create Option', ['question' => $question->id]) }}
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Special</th>
                    <th></th>
                </tr>

                @foreach($options AS $option)
                    <tr>
                        <td>{{ $option->id }}</td>
                        <td>{{ $option->title }}</td>
                        <td>{{ $option->description }}</td>
                        <td>{{ $option->special }}</td>
                        <td>
                             <a class="btn btn-default" href="{{ route('admin.question.option.edit', ['question' => $question->id, 'option' => $option->id]) }}"><i class="fa fa-pencil"></i></a>
                            @include('utils.delete',array( 'url' => URL::route('admin.question.option.destroy',['question' => $question->id, 'option' => $option->id] )))
                        </td>
                    </tr>
                @endforeach
            </table>

        </div><!-- /.box-body -->
    </div><!--box-->
@stop