@extends('backend.layouts.app')

@section ('title', 'Customers Report - Analytics')

@section('page-header')
    <div class="row">
        <div class="col-md-4">
            <h2 class="no-margin">Customers Report</h2>
        </div>
        <div class="col-md-8">
            <form action="" method="GET">
                <input name="fromDate" class="form-control inline" type="date" value="{{ isset($request->fromDate) && $request->fromDate ? $request->fromDate : date('Y-m-d', strtotime('Last Year')) }}"> <input class="form-control inline" name="toDate" type="date" value="{{ isset($request->toDate) && $request->toDate ? $request->toDate : "" }}">

                <select name="rep" class="form-control inline">
                    <option value="">-- All Reps --</option>
                    @foreach($reps AS $rep)
                        <option {{ $request->rep == $rep->Initial?'selected':'' }} value="{{ $rep->Initial }}">{{ $rep->SalesRepEntityRef_FullName }}</option>
                    @endforeach
                </select>

                <select name="customertype" class="form-control inline">
                    <option value="">-- All Customers --</option>
                    @foreach($customertypes AS $customertype)
                        <option {{ $request->customertype == $customertype->Name?'selected':'' }} value="{{ $customertype->Name }}">{{ $customertype->Name }}</option>
                    @endforeach
                </select>

                <button class="btn btn-primary" type="submit">GO</button>
            </form>
        </div>
    </div>
@endsection

@section('content')

<div class="box box-primary">

    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive bg-white">
                    <table id="calctable" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>City</th>
                                <th>Rep</th>
                                <th>In-House</th>
                                <th>Total Order</th>
                                <th>Shipped (t)</th>

                                <th>Order Count</th>
                                <th>Avg Order</th>
                                <th>Credits</th>
                                <th>Est GP</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($customers)
                            @foreach($customers AS $customer)
                                <tr>
                                    <td class="text-bold">
                                        @if(access()->user()->hasRoles(['Sales','Procurement']))
                                            <a href="{{ route('admin.customers.report', $customer->ID) }}">{{ $customer->Name }}</a>
                                        @else
                                            {{ $customer->Name }}
                                        @endif
                                    </td>
                                    <td>{{ $customer->BillingCity }}</td>
                                    <td>{{ $customer->SalesRep }}</td>
                                    <td>{{ $customer->JobType }}</td>
                                    <td>{!! \Helper::formatPrice($customer->TotalAmount) !!}</td>

                                    <td>{!! $customer->TotalKg>0? number_format($customer->TotalKg/1000,3):0 !!}</td>

                                    <td>{!! $customer->OrderCount !!}</td>
                                    <td>{!! \Helper::formatPrice($customer->AvgOrder) !!}</td>
                                    <td>{!! \Helper::formatPrice($customer->CreditAmount) !!}</td>

                                    <td>{!! \Helper::formatPrice($customer->GPmade) !!}</td>
                                </tr>
                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts-end')
    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#calctable').DataTable({
                'paging'      : false,
                'searching'   : false,
                'order' : [[ 6, 'asc' ]],
                'ordering'    : true,
                'info'        : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ],
            })
        })
    </script>

@endsection