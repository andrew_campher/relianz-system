@extends('backend.layouts.app')

@section ('title', 'Reports - Analytics')

@section('page-header')
    <div class="row">
        <div class="col-md-6">
            <h2 class="no-margin">GP Calculator</h2>
        </div>
        <div class="col-md-6 text-right">
            <select id="customerID" class="form-control" name="customerID">
                <option value="">-- Customer --</option>
                @foreach($customers AS $customer)
                    <option value="{{ $customer->ID }}">{{ $customer->FullName }}</option>
                @endforeach
            </select>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive bg-white">
                <table id="calctable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Qty</th>
                            <th>Del/ Col</th>
                            <th>Weight</th>
                            <th>Stock</th>
                            <th>Unit Cost</th>
                            <th>Unit Price</th>
                            <th>GP %</th>
                            <th>Per KG/Unit Price</th>
                            <th>SET DEAL PRICE</th>
                            <th>Freight R /Kg</th>
                            <th>Disc %</th>
                            <th>Settle Disc</th>
                            <th>Coll Disc</th>
                            <th>Single KG /Unit Price</th>
                            <th>Unit Price</th>
                            <th>Final GP %</th>
                            <th>Subtotal</th>
                            <th>SubTotal Margin GP</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="20">
                                <select class="item_sel transin form-control" id="first_sel">
                                    @foreach($items AS $item)
                                        <option value="{{ $item->ID }}">{{ $item->Description }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts-end')
    <script>

        $(document).ready(function(){
            var rowcount= 1;
            var e;

            $('.item_sel').on('change',function () {

                e = $(this);

                $.ajax({
                    url: "{{ route("api.pricing.get_item") }}",
                    type: "post",
                    dataType: 'json',
                    data: {
                        "itemID": e.val()
                    },
                    beforeSend: function() {
                        e.css({'border-color':'red'});
                    },
                    success: function(d) {
                        e.css({'border-color':''});

                        var gpval = (d.data['Price']-d.data['PurchaseCost'])/d.data['Price']*100;

                        $("#calctable").find('tbody')
                            .append($('<tr class="row'+rowcount+'">')
                                .append($('<td>')
                                    .append(d.data['Description'])
                                )
                                .append($('<td>')
                                    .append('<input class="qty transin" style="width: 10px" type="text" value="1" />')
                                )
                                .append($('<td>')
                                        .append('<select class="del transin"><option>DEL</option><option>COL</option></select>')
                                )
                                    .append($('<td>')
                                            .append(d.data['Weight'])
                                    )
                                    .append($('<td>')
                                            .append(d.data['QuantityOnHand'])
                                    )
                                    .append($('<td>')
                                            .append(d.data['PurchaseCost'])
                                    )
                                    .append($('<td>')
                                            .append(d.data['Price'])
                                    )
                                    .append($('<td>')
                                            .append(gpval.toFixed(2)+"%")
                                    )
                                    .append($('<td>')
                                            .append(d.data['KGUnit'])
                                    )
                                    .append($('<td>')
                                            .append('<input class="deal_price transin" type="text" value="" />')
                                    )
                                    .append($('<td>')
                                            .append('<input class="freight transin" type="text" value="" />')
                                    )
                                    .append($('<td>')
                                            .append('<input class="disc transin" type="text" value="" />')
                                    )
                                    .append($('<td>')
                                            .append('')
                                    )
                                    .append($('<td>')
                                            .append('')
                                    )
                                    .append($('<td>')
                                            .append('')
                                    )
                                    .append($('<td>')
                                            .append('')
                                    )
                                    .append($('<td>')
                                            .append('')
                                    )
                                    .append($('<td>')
                                            .append('')
                                    )
                                    .append($('<td>')
                                            .append('')
                                    )
                            );

                        calcRow(rowcount);
                    }
                });
            });

        });

        function calcRow(rowclass) {

            var r = $('.rowcount'+rowclass);



            var price = r.find('td:eq(8)').html();
            var qty = r.find('td:eq(1)').html();

            alert(price);


            r.eq(13).html(price*qty);
            r.eq(14).html("HELLO");
        }
    </script>
@append