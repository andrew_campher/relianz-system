@extends('backend.layouts.app')

@section ('title', 'Items Report - Analytics')

@section('page-header')
    <div class="row">
        <div class="col-md-4">
            <h2 class="no-margin">Items Report</h2>
        </div>
        <div class="col-md-8">
            <form action="" method="GET">
                <input name="fromDate" class="form-control inline" type="date" value="{{ isset($request->fromDate) && $request->fromDate ? $request->fromDate : date('Y-m-d', strtotime('Last Year')) }}"> <input class="form-control inline" name="toDate" type="date" value="{{ isset($request->toDate) && $request->toDate ? $request->toDate : "" }}">

                <button class="btn btn-primary" type="submit">GO</button>
            </form>
        </div>
    </div>
@endsection

@section('content')

<div class="box box-primary">

    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive bg-white">
                    <table id="calctable" class="table table-bordered table-condensed table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>CP</th>
                                <th>List</th>
                                <th>Created</th>
                                <th class="small">Qty On Hand</th>
                                <th>BO</th>

                                <th>Total Sales</th>
                                <th>Est GP</th>
                                <th>Shipped (t)</th>
                                <th>GP per kg</th>

                                <th class="small">No Cust</th>
                                <th class="small">Order Count</th>
                                <th>Avg Order</th>
                                <th>Credits</th>

                                <th>Last Sale</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($items)
                            @foreach($items AS $item)
                                <tr>
                                    <td class="text-bold">
                                        <a href="{{ route('admin.items.report', $item->ID) }}">{{ $item->Description }}</a>
                                        @if(access()->user()->hasRoles(['Administrator']))
                                         <a class="" href="{{ route('admin.items.edit', $item->ID) }}"><i class="fa fa-pencil"></i></a>
                                        @endif
                                    </td>

                                    <td>{!! $item->LineType !!}</td>
                                    <td>{!! $item->pricelist !!}</td>
                                    <td class="small">{!! date("Y-m-d" ,strtotime($item->TimeCreated)) !!}</td>
                                    <td>{!! $item->QuantityOnHand !!}</td>
                                    <td>{!! $item->QuantityOnSalesOrder !!}</td>

                                    <td>{!! \Helper::formatPrice($item->TotalAmount) !!}</td>
                                    <td>{!! \Helper::formatPrice($item->GPmade) !!}</td>

                                    <td>{!! $item->TotalKg>0? number_format($item->TotalKg/1000,3):0 !!}</td>

                                    <td>{!! $item->TotalKg>0?\Helper::formatPrice($item->GPmade/$item->TotalKg):0 !!}</td>

                                    <td>
                                        @php
                                        $nocust = $item->NumberCustomrs?count(array_unique(explode(",",$item->NumberCustomrs))):0;
                                        echo $nocust;
                                        @endphp
                                    </td>

                                    <td>{!! $item->OrderCount !!}</td>
                                    <td>{!! \Helper::formatPrice($item->AvgOrder) !!}</td>
                                    <td>{!! \Helper::formatPrice($item->CreditAmount) !!}</td>


                                    <td class="small">{{ $item->LastSale?date("Y-m-d" ,strtotime($item->LastSale)):''  }}</td>

                                    <td contenteditable="true" data-itemid="{{ $item->ID }}" class="changeable small {{ strpos($item->internal_notes,'DISCONTINUE')?'bg-danger':'' }}">{{ $item->internal_notes }}</td>
                                </tr>
                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts-end')
    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#calctable').DataTable({
                'paging'      : false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'colReorder'    : true,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print','copy', 'excel', 'pdf'
                ],
            })
        })
    </script>

    @if(access()->user()->hasRoles(['Procurement']))
        <script>
            var contents = '';
            $('.changeable').focus(function() {
                contents = $(this).html();
            }).blur(function() {
                e = $(this);
                if (contents.trim() != e.html().trim()){

                    contents = e.html();
                    $.ajax({
                        url: "{{ route("api.item.update") }}",
                        type: "post",
                        data: {
                            "ItemId": e.attr('data-itemid'),
                            "name": 'internal_notes',
                            "value": e.html()
                        },
                        beforeSend: function() {
                            e.css({'background-color':'yellow'});
                        },
                        success: function(d) {
                            e.css({'background-color':''});
                        },
                        error: function(d) {

                        }
                    });

                }
            });
        </script>
    @endif

@endsection