@extends('backend.layouts.app')

@section ('title', 'Month To Date - Analytics')

@section('page-header')
    <div class="row">
        <div class="col-md-4">
            <h2 class="no-margin">Month to Date</h2>
        </div>
        <div class="col-md-8">
            <small>THIS YEAR</small> <span class="h3 bg-info text-bold" >{!! \Helper::formatPrice($ThisYear) !!}</span>
            <small>LAST YEAR</small> <span class="h3 bg-warning text-bold">{!! \Helper::formatPrice($LastYear) !!}</span>
            <span class="label {{ $LastYear && ($ThisYear-$LastYear)/$LastYear >= 0.15 ?'label-success': 'label-danger' }}">{{ $LastYear>0?number_format(($ThisYear-$LastYear)/$LastYear*100,2):0 }}%</span>

            <span>AVG GP: <b class="h4 badge {{ $AvgGP >= 0.15 ?'label-success': '' }}">{{ number_format($AvgGP,2) }}%</b></span>
        </div>
    </div>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive bg-white">
                <table id="monthtodate" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Rep</th>
                            <th>Type</th>
                            <th><?php echo date('Y') ?></th>
                            <th><?php echo date('Y',strtotime('Last year')) ?></th>
                            <th width="110px">RandDiff</th>
                            <th>PercDiff</th>
                            <th><?php echo date('Y') ?> GP</th>
                            <th><?php echo date('Y',strtotime('Last year')) ?> GP</th>
                            <th>Last Month</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customers AS $customer)
                            <tr>
                                <td class="text-black text-bold">
                                    @if(access()->user()->hasRoles(['Sales','Procurement']))
                                        <a href="{{ route('admin.customers.report', $customer->ID) }}">{{ $customer->Name }}</a>
                                    @else
                                        {{ $customer->Name }}
                                    @endif
                                </td>
                                <td>{{ $customer->SalesRep }}</td>
                                <td>{{ $customer->Type }}</td>
                                <td class="bg-info {{ $customer->ThisYear < 1 ? 'bg-danger':''  }}">{!! \Helper::formatPrice($customer->ThisYear) !!}</td>
                                <td>{!! \Helper::formatPrice($customer->LastYear) !!}</td>
                                <td class="{{ $customer->RandDiff < 0 ? 'bg-danger':'bg-success'  }}" ><b>{!! \Helper::formatPrice($customer->RandDiff) !!}</b></td>
                                <td>{{ number_format($customer->PercDiff*100,2) }}%</td>
                                @if($customer->ThisYearGP>0)
                                    <td class="{{ $customer->ThisYearGP <= 0.1 ? ($customer->ThisYearGP <= 0.05?'btn-danger':'bg-warning'):'bg-success'  }}">{{ number_format($customer->ThisYearGP*100,2) }}%</td>
                                @else
                                    <td></td>
                                @endif
                                <td>{{ number_format($customer->LastYearGP*100,2) }}%</td>
                                <td>{!! \Helper::formatPrice($customer->LastMonth) !!}</td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('after-scripts-end')

    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#monthtodate').DataTable({
                'paging'      : false,
                'searching'   : true,
                'ordering'    : true,
                'order'       : [[5,'asc']],
                'info'        : false,
                'responsive' : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ]
            })
        })
    </script>
@append