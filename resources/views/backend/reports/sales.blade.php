@extends('backend.layouts.app')

@section ('title', 'Sales - Reports')

@section('page-header')
    <div class="row">
        <div class="col-md-4">
            <h2 class="no-margin">Sales Report</h2>
        </div>
        <div class="col-md-8">
            @if($LastYear)
                <small>THIS PERIOD</small> <span class="h3 bg-info text-bold" >{!! \Helper::formatPrice($ThisYear) !!}</span>
                <small>PREV YEAR</small> <span class="h3 bg-warning text-bold">{!! \Helper::formatPrice($LastYear) !!}</span>
                <span class="label {{ ($ThisYear-$LastYear)/$LastYear >= 0.15 ?'label-success': 'label-danger' }}">{{ number_format(($ThisYear-$LastYear)/$LastYear*100,2) }}%</span>
            @endif

            <form action="" method="GET">
                <input name="fromDate" class="form-control inline" type="date" value="{{ isset($request->fromDate) && $request->fromDate ? $request->fromDate : "" }}"> <input class="form-control inline" name="toDate" type="date" value="{{ isset($request->toDate) && $request->toDate ? $request->toDate : "" }}">

                <select name="rep" class="form-control inline">
                    <option value="">-- All Reps --</option>
                    @foreach($reps AS $rep)
                        <option {{ $request->rep == $rep->Initial?'selected':'' }} value="{{ $rep->Initial }}">{{ $rep->SalesRepEntityRef_FullName }}</option>
                    @endforeach
                </select>

                <select name="customertype" class="form-control inline">
                    <option value="">-- All Customers --</option>
                    @foreach($customertypes AS $customertype)
                        <option {{ $request->customertype == $customertype->Name?'selected':'' }} value="{{ $customertype->Name }}">{{ $customertype->Name }}</option>
                    @endforeach
                </select>

                <button class="btn btn-primary" type="submit">GO</button>
            </form>
        </div>
    </div>
@endsection

@section('content')

    <div class="box box-primary">

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive bg-white">
                        <table id="calctable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Rep</th>
                                <th>Type</th>
                                <th>This Year</th>
                                <th>Last Period</th>
                                <th width="110px">RandDiff</th>
                                <th>PercDiff</th>
                                <th>This Year GP</th>
                                <th>Last Year GP</th>
                                <th>Created</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($customers)
                                @foreach($customers AS $customer)
                                    <tr>
                                        <td class="text-bold">
                                            @if(access()->user()->hasRoles(['Sales','Procurement']))
                                                <a href="{{ route('admin.customers.report', $customer->ID) }}">{{ $customer->Name }}</a>
                                            @else
                                                {{ $customer->Name }}
                                            @endif
                                        </td>
                                        <td>{{ $customer->SalesRep }}</td>
                                        <td>{{ $customer->Type }}</td>
                                        <td class="text-bold {{ $customer->ThisYear < 1 ? 'bg-danger':''  }}">{!! \Helper::formatPrice($customer->ThisYear) !!}</td>
                                        <td>{!! \Helper::formatPrice($customer->LastYear) !!}</td>
                                        <td class="{{ $customer->RandDiff < 0 ? 'bg-danger':'bg-success'  }}" >{!! \Helper::formatPrice($customer->RandDiff) !!}</td>
                                        <td>{{ number_format($customer->PercDiff*100,2) }}%</td>
                                        @if($customer->ThisYearGP>0)
                                            <td class="{{ $customer->ThisYearGP <= 0.1 ? ($customer->ThisYearGP <= 0.05?'btn-danger':'bg-warning'):'bg-success'  }}">{{ number_format($customer->ThisYearGP*100,2) }}%</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{ number_format($customer->LastYearGP*100,2) }}%</td>
                                        <td>{{ $customer->CustomerCreated }}</td>
                                    </tr>
                                @endforeach

                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts-end')
    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#calctable').DataTable({
                'paging'      : false,
                'searching'   : false,
                'order' : [[ 6, 'asc' ]],
                'ordering'    : true,
                'info'        : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ],
            })
        })
    </script>

@endsection