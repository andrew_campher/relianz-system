@extends ('backend.layouts.app')

@section ('title', 'Service | Manager')

@section('page-header')
    <h1>
        Services Management
        <small>Edit</small>
    </h1>
@endsection

@section('content')
    @if( isset($service) )
        {{ Form::model($service, ['route' => ['admin.service.update', $service], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'admin.service.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    @endif

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Service</h3>

            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('pro_title', 'Professionals Called (Plural!)', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('pro_title', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('tags', 'Tags', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('tags', null, ['class' => 'form-control', 'placeholder' => 'Tags']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('occupation_id', 'Occupation', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('occupation_id', array_pluck($occupations, 'title', 'id'), null, ['placeholder' => '-- Occupation --']) }}<br/>
                        Create Occupation: {{ Form::text('new_occupation', null, ['class' => 'form-control', 'placeholder' => 'Title must be a string']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('image', 'Image', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                    @if( isset($service) )
                        {!! $service->image_html !!}
                    @endif
                        {{ Form::file('image') }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('fee', 'Credit Fee', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-2">
                        <div class="input-group">
                            {{ Form::text('fee', null, ['class' => 'form-control', 'placeholder' => 'Fee for this service']) }}
                            <div class="input-group-addon">Credits</div>
                        </div>

                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('description', 'Description', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('categories', 'Categories', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        @include('backend.category.includes.partials.category-list')
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-1">
                        {{ Form::checkbox('status', '1', isset($service) && $service->status == 1) }}
                    </div><!--col-lg-1-->
                </div><!--form control-->

            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.question.index', 'Cancel', [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Update', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

