<div class="padding15 selected_services">
    @if(isset($services) && !$services->isEmpty())
        @foreach($services AS $service)
            <span class="sinser_{{ $service->id }} badge btn-lg label-primary">{{ $service->title }} <i class="fa fa-remove fa-lg" onclick="removeService({{ $service->id }})"></i><input type="hidden" name="services[]" value="{{ $service->id }}" /></span>
        @endforeach
        <a onclick="removeAll()" href="javascript:void(0)"><i class="fa fa-remove"></i> Remove All</a>
    @endif
</div>

@if(isset($servicelist))
    <div class="h5">Service Categories</div>
<div class="panel-group serviceselect" id="accordion" role="tablist" aria-multiselectable="true">
@foreach( $servicelist[0] AS $category0 )

        <div class="panel form-horizontal">

            <div class="panel-heading">
                <div class="h4 panel-title">
                    <a role="button" onclick="getServices({{$category0['id']}})" style="display: block" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$category0['id']}}" aria-expanded="false" aria-controls="collapse{{$category0['id']}}" class="panel-heading" role="tab" id="heading{{$category0['id']}}">

                        <span class="fa-stack">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-sort-amount-desc fa-stack-1x fa-inverse"></i>
                        </span> {{$category0['title']}}
                        <div class="pull-right"><i class="fa fa-angle-down"></i></div>

                    </a>
                </div>
            </div>
            <div id="collapse{{$category0['id']}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$category0['id']}}">
                <div class="">

                    <div id="services_cat{{$category0['id']}}"></div>


                    @if(isset($servicelist[$category0['id']]))
                        <ul class="list-group">
                            @foreach($servicelist[$category0['id']] AS $category1)

                                <li class="list-group-item">

                                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">

                                    <a role="button" onclick="getServices({{$category1['id']}})" style="display: block" data-toggle="collapse" data-parent="#accordion2" href="#collapse{{$category1['id']}}" aria-expanded="false" aria-controls="collapse{{$category1['id']}}" class="panel-heading" role="tab" id="heading{{$category1['id']}}">

                                        <span class="fa-stack">
                                          <i class="fa fa-circle fa-stack-2x"></i>
                                          <i class="fa fa-sort-amount-desc fa-stack-1x fa-inverse"></i>
                                        </span> {{$category1['title']}}
                                        <div class="pull-right"><i class="fa fa-angle-down"></i></div>

                                    </a>



                                    <div id="collapse{{$category1['id']}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$category1['id']}}">
                                        <div class="">
                                            <div id="services_cat{{$category1['id']}}"></div>
                                        </div>
                                    </div>


                                    </div>

                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>

@endforeach
</div>
@endif

@section('after-scripts-end')
<script>
    function removeAll() {
        var id;
        $('.selected_services .badge').each(function(i, item) {
            id = $(item).attr('id').substring(7);
            removeService(id);
        });
    }

    function removeService(id) {
        if (typeof window.icheck !== 'undefined') {
            $('.inser_' + id).icheck('uncheck');
            $('#inser_' + id).icheck('uncheck');
        }
        $('.inser_'+id).prop('checked', false);
        $('.sinser_'+id).remove();
    }


    function getServices(cat_id) {

        var currentRequest = null;


        currentRequest = $.ajax({
            url: '{{ route("api.service.category_services") }}',
            method: "GET",
            dataType: "json",
            data: {category_id: cat_id},
            beforeSend: function () {
                if (currentRequest != null) {
                    currentRequest.abort();
                }
            },
            success: function (response) {
                if (response.length != 0) {
                    $('#services_cat'+cat_id).html('');
                    var items = [];
                    $.each(response, function (key, val) {
                        items.push("<div class='checkbox'><label><input type='checkbox'  name='services[]' class='inser_" + val.id + "' value='" + val.id + "' /> <span>" + val.title + "</span></label></div>");
                    });

                    $('#services_cat'+cat_id).html(items.join(""));

                    $("input[name='services[]']").on('change', function() {
                        if (this.checked) {
                            $('.selected_services').append('<span class="sinser_'+this.value+' badge label-primary">'+$(this).parent().parent().find('span').html()+'<i class="fa fa-remove fa-lg" onclick="removeService(\''+this.value+'\')"></i></span> ');
                        } else {
                            $('.sinser_'+this.value).remove();
                        }
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log('ajax error: ' + thrownError);
            }
        });

    }
</script>
@append