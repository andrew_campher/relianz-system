@extends ('backend.layouts.app')

@section('page-header')
    <h1>
        Services Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">
            <h3 class="box-title">{{ isset($category) && $category?$category->title.' ':'' }}Services</h3>

            <div class="box-tools pull-right">
                @include('backend.service.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ route('admin.service.action') }}" method="post" class="">
                    <table id="service-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Occupation</th>
                            <th>Tags</th>
                            <th>Fee</th>
                            <th>Status</th>
                            <th>hits</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($services AS $service)
                            <tr>
                                <td width="30px"><input name="selected[]" type="checkbox" value="{{ $service->id }}"></td>
                                <td>{{ $service->id }}</td>
                                <td>{{ $service->title }}</td>
                                <td>{!! $service->occupation?$service->occupation->title:'none' !!}</td>
                                <td>{!! $service->tags !!}</td>
                                <td>{{ $service->fee }}</td>
                                <td>{!! $service->status_label !!}</td>
                                <td>{{ $service->hits }}</td>
                                <td>{!! $service->action_buttons !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="form-inline">
                        <select class="form-control form-inline" name="action">
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    <div class="pull-right">{{ $services->links() }}</div>

                    <div class="clearfix"></div>


                </form>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
