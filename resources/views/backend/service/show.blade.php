@extends ('backend.layouts.app')

@section ('title', 'Services | Show')

@section('page-header')
    <h1>
        Services Management
        <small>View Service</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $service->title }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>Description</th>
                    <td>{{ $service->description }}</td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td>{!! $service->status_label !!}</td>
                </tr>

                <tr>
                    <th>Created At</th>
                    <td>{{ $service->created_at }} ({{ $service->created_at->diffForHumans() }})</td>
                </tr>

                <tr>
                    <th>Last Update</th>
                    <td>{{ $service->updated_at }} ({{ $service->updated_at->diffForHumans() }})</td>
                </tr>

                @if ($service->trashed())
                    <tr>
                        <th>{{ trans('labels.backend.service.tabs.content.overview.deleted_at') }}</th>
                        <td>{{ $service->deleted_at }} ({{ $service->deleted_at->diffForHumans() }})</td>
                    </tr>
                @endif
            </table>

        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $service->title }} Questions</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table id="servicequestions" class="table table-striped table-hover">
                <tr>
                    <th>Question</th>
                    <th>Description</th>
                    <th>Question Type</th>
                    <th></th>
                </tr>

                @foreach($service_questions AS $service_question)
                <tr>
                    <td>{{ $service_question->title }}</td>
                    <td>{{ $service_question->description }}</td>
                    <td>{{ $service_question->question_type }}</td>
                    <td>
                        {{ Form::open(['route' => ['admin.service.question.destroy', $service->id, $service_question->id], 'class' => 'form-horizontal', 'style'=>'display:inline', 'role' => 'form', 'method' => 'post']) }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash"></i>
                            </button>
                        {{ Form::close() }}

                        <i class="fa fa-arrow-up up"></i> <i class="fa fa-arrow-down down"></i>
                    </td>
                </tr>
                @endforeach

            </table>

            {{ Form::open(['route' => ['admin.service.question.store', $service->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
            <div class="input-group">
                {{ Form::select('question_id', $questions, null, ['placeholder' => '-- Add Question --', 'class' => 'form-control']) }}
                <span class="input-group-btn">
                <input class="btn btn-default" type="submit" value="save">
                </span>
                {{ Form::hidden('service_id', $service->id) }}
            </div>
            {{ Form::close() }}

        </div><!-- /.box-body -->
    </div><!--box-->

@stop

@section('after-scripts-end')
    <script>
        $(document).ready(function(){
            $(".up,.down").click(function(){

                var row = $(this).parents("tr:first");
                if ($(this).is(".up")) {
                    if (row.prev().index() == 0) return false;
                    row.insertBefore(row.prev());
                } else {
                    row.insertAfter(row.next());
                }
            });
        });
    </script>
@stop

