@extends ('backend.layouts.app')

@section ('title', 'Suppliers')

@section('page-header')
    <h1>
        Suppliers Management
        <small>Management</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">

        <div class="box-header with-border">

            <div class="col-md-6">
                <h2 class="box-title">Suppliers</h2>
            </div>
            <div class="col-md-6 text-right">
                <form action="" method="GET">
                    <input name="search" type="text" value="{{ isset($request->search) && $request->search ? $request->search : "" }}"> <input name="isactive" type="checkbox" value="1"> Incl DeActivated
                    <button type="submit">GO</button>
                </form>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                    <table id="item-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Class</th>
                            <th>Name</th>
                            <th></th>
                            <th>Phone</th>
                            <th>Terms</th>
                            <th>SalesRep</th>
                            <th>Area</th>
                            <th>COA</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($suppliers AS $supplier)

                                @php
                                $supplier->SupplierFields = json_encode($supplier->SupplierFields, true);
                                @endphp
                            <tr>
                                <td>{!! $supplier->showClass() !!}</td>
                                <td><a href="{{ route('admin.suppliers.show', $supplier->ID) }}">{{ $supplier->Name }}</a></td>
                                <td>
                                    @if(access()->user()->hasRoles(['Sales','Procurement']))
                                    <a class="btn btn-primary btn-xs" href="{{ route('admin.suppliers.report', $supplier->ID) }}"><i class="fa fa-bar-chart"></i></a>
                                    @endif
                                </td>
                                <td>{{ $supplier->Phone }}</td>
                                <td>{{ $supplier->Terms }}</td>
                                <td>{{ $supplier->SalesRep }}</td>
                                <td>{{ isset($supplier->SupplierFields['DeliveryArea']) ? $supplier->SupplierFields['DeliveryArea'] : "" }}</td>
                                <td>{{ isset($supplier->SupplierFields['COA']) ? $supplier->SupplierFields['COA'] : "" }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                    <div class="pull-right">{{ $suppliers->appends($request::capture()->except('page'))->links() }}</div>

                    <div class="clearfix"></div>


            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@stop
