@php($collapse=1)
@extends ('backend.layouts.app')

@section ('title', $supplier->Name. ' - Analytics Report')

@section('page-header')
    <h1>
        Supplier Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i><span>{{ trans('menus.backend.sidebar.dashboard') }}</span></a></li>
        <li><a href="{{ route('admin.suppliers.index') }}"><i class="fa fa-cubes"></i><span>Suppliers</span></a></li>
        <li class="active">{{ $supplier->Name }}</li>
    </ol>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{!! $supplier->showClass() !!} {{ $supplier->Name }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-responsive table-striped h4">
                        <tr>
                            <td></td>
                            <td>YTD Spent</td>
                            <td>Stock On Hand</td>
                            <td>Credit Limit</td>
                            <td>Credit Limit</td>
                            <td>Balance</td>
                        </tr>
                        <tr>
                            <td>{{ $supplier->Name }}</td>
                            <td>{!! \Helper::formatPrice($totalRand) !!}</td>
                            <td>{!! \Helper::formatPrice($totalStock) !!}</td>
                            <td>{{ $supplier->Terms }}</td>
                            <td>{!! \Helper::formatPrice($supplier->CreditLimit) !!}</td>
                            <td>{!! \Helper::formatPrice($supplier->Balance) !!}</td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-body">

                    <h5>Supplier Sales History</h5>
                    <div class="table-responsive">
                        <table id="report" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="text-center">On Hand</th>
                                    <th class="text-center sum">Past 5 Years</th>
                                    <th class="sum">This Year</th>
                                    <th class="sum">This Month</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-1 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-2 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-3 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-4 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-5 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-6 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-7 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-8 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-9 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-10 Months")) }}</th>
                                    <th class="text-center sum">{{ date("M Y", strtotime("-11 Months")) }}</th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach($SupplierArr AS $item => $itemArrs)
                                <tr>
                                    <td><a class="" href="{{ route('admin.items.report', $curr[$item]['ItemId']) }}">{{ $item }}</a></td>
                                    <td class="text-center">{{ isset($curr[$item]['onhand']) ?$curr[$item]['onhand']:0 }}</td>
                                    <td class="text-center {{ isset($curr[$item]['t']) && $curr[$item]['t'] ?"":"bg-danger" }}">{{ isset($curr[$item]['t']) ?$curr[$item]['t']:0 }}</td>
                                    <td class="text-center {{ isset($curr[$item]['y']) && $curr[$item]['y'] ?"":"bg-danger" }}">{{ isset($curr[$item]['y']) ?$curr[$item]['y']:0 }}</td>
                                    @php($cnt = 1)
                                    @foreach($monthTots AS $month => $monthTot)

                                        <td data-sort="{{ isset($curr[$item][$month]) ?$curr[$item][$month]:0 }}" class="text-center {{ $cnt ==1 ?'bg-warning border-left border-right':'' }} {{ isset($curr[$item][$month]) && $curr[$item][$month] ?"":"bg-danger" }}"><b>{{ isset($curr[$item][$month]) ?$curr[$item][$month]:0 }}</b>
                                            <br/><span class="small {{ isset($past[$item][$month]) && isset($curr[$item][$month]) && $past[$item][$month]>$curr[$item][$month]?'text-red':'' }} text-default">{{ isset($past[$item][$month]) ?$past[$item][$month]:0 }}</span>
                                        </td>
                                        @php($cnt = 0)
                                    @endforeach
                                </tr>
                            @endforeach

                            </tbody>

                        </table>

                    </div>


                </div>
            </div>

        </div><!-- /.box-body -->
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-body">
                    <canvas id="salesChart" width="100%"></canvas>
                </div>
            </div>
        </div>
    </div><!--box-->

@stop

@section('after-scripts-end')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>

    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#report').DataTable({
                'paging'      : false,
                'searching'   : false,
                'order' : [[ 4, 'desc' ]],
                'ordering'    : true,
                'info'        : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ],
                'initComplete': function (settings, json){
                    this.api().columns('.sum').every(function(){
                        var column = this;

                        var sum = column
                                .data()
                                .reduce(function (a, b) {
                                    a = parseInt(a, 10);
                                    if(isNaN(a)){ a = 0; }

                                    b = parseInt(b, 10);
                                    if(isNaN(b)){ b = 0; }

                                    return a + b;
                                });

                        $(column.footer()).html('Sum: ' + sum);
                    });
                }
            })
        })
    </script>

    <script>
        var ctx2 = document.getElementById("salesChart").getContext('2d');
        var myChart2 = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: [<?php
                    foreach ($monthTots AS $mnth => $mnthval) {
                        $yr = date("Y");
                        if ($mnth >= date("n")) {
                            $yr = date("Y", strtotime("-1 Year"));
                        }
                        echo '"'.date("M",strtotime("01-".$mnth."-".$yr)).'", ';
                    }
                    ?>],
                datasets: [{
                    label: 'This Period',
                    fill: false,
                    backgroundColor: "blue",
                    borderColor: "blue",
                    data: [<?php
                        foreach ($monthTots AS $monthTot) {
                            echo '"'.$monthTot.'", ';
                        }
                        ?>],
                    borderWidth: 3
                },{
                    label: 'Prev Period',
                    fill: false,
                    data: [<?php
                        foreach ($PrevMonthTots AS $PrevMonthTot) {
                            echo '"'.$PrevMonthTot.'", ';
                        }
                        ?>],
                    borderWidth: 3
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endsection