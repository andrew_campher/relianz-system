@extends ('backend.layouts.app')

@section ('title', $supplier->Name. ' - Supplier')

@section('page-header')
    <h1>
        Suppliers Management
        <small>View Supplier</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{!! $supplier->showClass() !!} {{ $supplier->Name }}
                @if(access()->user()->hasRoles(['Sales','Procurement']))
                <a class="btn btn-primary" href="{{ route('admin.suppliers.report', $supplier->ID) }}"><i class="fa fa-bar-chart"></i></a>
                @endif
            </h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <table class="table table-striped table-hover">

                <tr>
                    <th>Phone</th>
                    <td>{{ $supplier->Phone }}</td>
                </tr>

                <tr>
                    <th>Rep</th>
                    <td>{!! $supplier->SalesRep !!}</td>
                </tr>


            </table>

        </div><!-- /.box-body -->
    </div><!--box-->

@stop