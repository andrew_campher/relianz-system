@extends('backend.layouts.app')

@section ('title', 'Drop-Off Report')

@section('page-header')
    <div class="row">
        <div class="col-md-4">
            <h3 class="no-margin">{{ $dropline?COUNT($dropline):0 }} Drop Off Customer Lines</h3>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <form class="form-inline">
                    Years: <select class="form-control" name="years">
                        <option {{ $request->years == 1?'selected':'' }}>1</option>
                        <option {{ $request->years == 2?'selected':'' }}>2</option>
                        <option {{ $request->years == 3?'selected':'' }}>3</option>
                        <option {{ $request->years == 4?'selected':'' }}>4</option>
                    </select>
                    Rep: <select class="form-control" name="rep">
                        <option value="">-- ALL --</option>
                        @foreach($reps AS $rep)
                            <option {{ $request->rep == $rep->Initial?'selected':'' }}>{{ $rep->Initial }}</option>
                        @endforeach
                    </select>

                    Customer: <select class="form-control" name="CustomerId">
                        <option value="">-- ALL --</option>
                        @foreach($customers AS $customer)
                            <option value="{{ $customer->ID }}" {{ $request->CustomerId == $customer->ID?'selected':'' }}>{{ $customer->Name }}</option>
                        @endforeach
                    </select>

                    <button class="btn btn-info" type="submit">GO</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('content')

    @php($cnt=1)

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive bg-white">
                <table id="droptable" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Customer</th>
                        <th>Product</th>
                        <th>SalesRep</th>
                        <th>Last Sale</th>
                        <th>Sales Value</th>
                        <th>Months Sold</th>
                        @foreach($monthArray AS $mnth => $qty)
                            <th style="font-size: 10px">{{ date("M Y",strtotime($mnth.'01')) }}</th>
                        @endforeach
                    </tr>
                    </thead>

                    <tbody>

                    @foreach($dropline AS $drop)
                        <tr>
                            <td>{{ $cnt++ }}</td>
                            <td class="text-bold"><a href="{{ route('admin.customers.report', $drop->CustomerId) }}">{{ $drop->Name }}</a></td>
                            <td class="text-bold"><a href="{{ route('admin.items.report', $drop->ItemId) }}">{{ $drop->Description }}</a></td>
                            <td>{{ $drop->SalesRep }}</td>
                            <td>{{ $drop->LastSale }}</td>
                            <td>{!! \Helper::formatPrice($drop->SalesValue) !!}</td>
                            <td>{{ $drop->OrderCount }}</td>
                            @foreach($monthArray AS $mnth => $qty)
                            <td class="small center {{ isset($qtyarray[$drop->CustomerId][$drop->ItemId][$mnth]) ? '' :'bg-danger' }}">{{ isset($qtyarray[$drop->CustomerId][$drop->ItemId][$mnth]) ? number_format($qtyarray[$drop->CustomerId][$drop->ItemId][$mnth],0) :0 }}</td>
                            @endforeach
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts-end')
    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#droptable').DataTable({
                'paging'      : false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ],
            })
        })
    </script>

@endsection