@extends('backend.layouts.app')

@section ('title', 'Sitting Stock Report')

@section('page-header')
    <div class="row">
        <div class="col-md-6">
            <h2 class="no-margin">Sitting Stock Report</h2>
        </div>
        <div class="col-md-6 text-right">
            <form action="" class="form-inline" method="get" >
                Months: <select class="form-control" name="months">
                    <option {{ $request->months==1?'selected':'' }}>1</option>
                    <option {{ $request->months==2?'selected':'' }}>2</option>
                    <option {{ $request->months==3?'selected':'' }}>3</option>
                    <option {{ $request->months==4?'selected':'' }}>4</option>
                    <option {{ $request->months==5?'selected':'' }}>5</option>
                    <option {{ $request->months==6 || !$request->months?'selected':'' }}>6</option>
                </select>
                <input type="submit" value="submit">
            </form>
        </div>
    </div>
@endsection

@section('content')

    <div class="box box-primary">

        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive bg-white">
                        <table id="nostocks" class="table table-bordered table-striped table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Description</th>
                                    <th>Qty on Hand</th>
                                    <th>Last Receive</th>
                                    <th>Value</th>
                                    <th>BackOrder</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($items AS $item)
                                    <tr>
                                        <td>
                                            <?php
                                            if (isset($item->Class) && $item->Class) {
                                                switch ($item->Class) {
                                                    case "A":
                                                        echo '<span class="label label-success label-sm">A</span>';
                                                        break;
                                                    case "B":
                                                        echo '<span class="label label-warning label-sm">B</span>';
                                                        break;
                                                    case "C":
                                                        echo '<span class="label label-danger label-sm">C</span>';
                                                        break;
                                                }
                                            }

                                            ?>
                                        </td>
                                        <td>{{ substr( $item->Description, ( $pos = strpos( $item->Description, ':' ) ) === false ? 0 : $pos + 1 )  }} {{ $item->Class }}</td>
                                        <td class="{{ $item->QuantityOnHand == 0?'bg-danger':'' }} text-center">{{ $item->QuantityOnHand }}</td>
                                        <td class="text-center">{{ $item->LastRecieve?date("d-m-Y", strtotime($item->LastRecieve)):"" }}</td>
                                        <td class="text-center">{!! \Helper::formatPrice($item->QuantityOnHand*$item->PurchaseCost) !!}</td>
                                        <td align="center" class="text-bold {{ $item->BackOrderQty?'bg-success':'' }}">{{ $item->BackOrderQty }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts-end')

    {{ Html::style('css/backend/plugin/datatables/datatables.min.css') }}
    {{ Html::script("js/backend/plugin/datatables/datatables.min.js") }}

    <script>
        $(function () {
            $('#nostocks').DataTable({
                'paging'      : false,
                'searching'   : true,
                'ordering'    : true,
                'order'       : [[3,'asc']],
                'info'        : false,
                'responsive' : false,
                'fixedHeader': true,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ]
            })
        })
    </script>

    @if(access()->user()->hasRoles(['Procurement']))
    <script>
        var contents = '';
        $('.changeable').focus(function() {
            contents = $(this).html();
        }).blur(function() {
            e = $(this);
            if (contents.trim() != e.html().trim()){

                contents = e.html();
                $.ajax({
                    url: "{{ route("api.item.update") }}",
                    type: "post",
                    data: {
                        "ItemId": e.attr('data-itemid'),
                        "name": 'procurement_notes',
                        "value": e.html()
                    },
                    beforeSend: function() {
                        e.css({'border-color':'red'});
                    },
                    success: function(d) {
                        e.css({'border-color':''});
                    },
                    error: function(d) {

                    }
                });

            }
        });
    </script>
    @endif
@append