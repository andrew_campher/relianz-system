@extends('frontend.layouts.app')

@if(!$quote)
@section('sidebar')

    @include('frontend.account.includes.menu')

@endsection
@endif

@section('content')

    @if($quote)
        <h1 class="no-top-margin">Credits Required</h1>
    @else
        <h1 class="no-top-margin">Credits Purchase</h1>
    @endif

    <div class="row">
        <div class="col-xs-12">

            <div class="jumbotron">
                <div class="jumbotron-contents">

                    @if($quote)
                        <div class="alert alert-info"><h5>In order to send your quote off to {!! $quote->project->user->shortname !!} you will need to purchase some credits.</h5></div>

                        <div class="pull-left" style="margin-right: 20px;">{!! $quote->project->user->getImageHtml('thumb', 'avatar-med img-circle') !!}</div>
                        <div class="">
                            <div class="profile_side_name">
                                {!! $quote->project->user->shortname !!}
                            </div>

                            <div class="profile_side_area">{{ $quote->project->area }}</div>
                        </div>
                        <h4 class="no-top-margin">{{ $quote->project->title }}</h4>
                        <h4 class="thin">You could make: <b>{!!  \Helper::formatPrice($quote->price) !!} {!! trans('strings.frontend.quotes.price_types.'.$quote->price_type) !!}</b></h4>

                        <div class="clearfix"></div>

                        <hr>


                    @endif

                    @include('frontend.account.includes.credits_buy_form')

                </div>
            </div>
        </div>
    </div>

@endsection