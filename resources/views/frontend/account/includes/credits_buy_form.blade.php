{{ Form::open(['route' => 'api.payment.process', 'class' => '', 'role' => 'form', 'method' => 'post', 'id'=>'creditsForm']) }}

@if($logged_in_user->professional->outstanding_invoices->count())

    <div class="alert alert-warning">
        <h4>Payment Due</h4>
        <p>You currently have an amount outstanding on your account. Please visit the <a href="{{ route('frontend.invoice.index') }}">invoices page</a> to see more.</p>
    </div>
@endif

<div class="pull-left">
    <h2>Choose TopUp Amount</h2>

    <div class="pricing">
        <ul>
            @foreach($purchase_options AS $purchase_option)
                <li class="unit {{ $purchase_option['class'] ?$purchase_option['class']: 'price-primary'  }}  {{ $purchase_option['active'] ?'active': ''  }}">
                    <div class="price-title">
                        <h3>{{ $purchase_option['credit_quantity'] }}</h3>
                        <p>Credits</p>
                    </div>
                    <div class="price-body">
                        <h4>{!! Helper::formatPrice($purchase_option['amount']) !!}</h4>
                        <p>only</p>
                        <ul>
                            <li>{!! Helper::formatPrice($purchase_option['per_credit']) !!}/credit</li>
                            <li>Auto-refundable <i class="fa fa-info-circle"></i></li>
                            <li>Pay only when<br> customer views quote.</li>
                        </ul>
                    </div>
                    <div class="price-foot">
                        <input required name="credit_quantity" type="radio" class="" value="{{ $purchase_option['credit_quantity'] }}"> Choose
                    </div>
                </li>
            @endforeach
        </ul>
    </div>


    <div class="form-group">

        <hr>

        <h4>Payment Method</h4>

        <div class="form-group">
            <input name="payment_method" value="eft" type="radio" required> <b>Standard EFT</b> (Credits allocated once funds clear - usually 2 days)
        </div>

        <div class="form-group">
            <input name="payment_method" value="payfast" type="radio" required> <b>PayFast</b> <img src="{{ asset('img/frontend/payfast/v2_logo.png') }}" height="30px"> <i class="fa fa-cc-visa fa-2x" style="color:#ccc"></i> <i style="color:#ccc" class="fa fa-cc-mastercard fa-2x"></i> <i style="color:#ccc" class="fa fa-bitcoin fa-2x"></i>
        </div>

        @if(!$logged_in_user->professional->outstanding_invoices->count())
            <div class="padding15">
                <input type="submit" class="btn btn-success" value="Proceed">
            </div>
        @endif
    </div>
</div>

<div class="clearfix"></div>

{{ Form::hidden('description', config('app.name').' Credits Purchase') }}

{{ Form::hidden('user_id', $logged_in_user->id) }}
{{ Form::hidden('account_id', $logged_in_user->professional->account->id) }}
{{ Form::hidden('package_id', $logged_in_user->professional->account->package_id) }}
{{ Form::hidden('professional_id', $logged_in_user->professional->id) }}

{{ Form::hidden('api_token', $logged_in_user->api_token) }}


{{ Form::close() }}

@section('after-scripts-end')
    <script>
        $(document).ready(function() {

            $('#creditsForm').validator();

            $('#creditsForm').ajaxForm(function(resp) {
                if (resp.allow_continue == 0 && resp.redirect != '') {
                    sweetAlert('Successful', resp.alert, resp.alert_type);
                    setTimeout(function()
                    {
                        window.location.replace(resp.redirect);
                    }, 2500);
                } else if(resp.allow_continue == 1) {


                } else {
                    sweetAlert('Oops', resp.msg, 'warning');
                }
            });

        });

    </script>

    {{ Html::script("vendor/jquery-form/jquery.form.min.js") }}
@endsection

