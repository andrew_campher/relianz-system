<h2 class="no-top-margin">My Account</h2>
<ul class="sidebar-menu list-group">
    <li class="list-group-item"><a href="{{ route('frontend.account.index') }}">Account</a></li>
    <li class="list-group-item"><a href="{{ route('frontend.invoice.index') }}">Invoices</a>{!!  $logged_in_user->professional->outstanding_invoices->count()? '<div class="badge label-danger">'.$logged_in_user->professional->outstanding_invoices->count().' Pending</div>': '' !!}</li>
    <li class="list-group-item"><a href="{{ route('frontend.account.credits_purchase', $logged_in_user->professional->account->id) }}">Buy Credits</a></li>
</ul>