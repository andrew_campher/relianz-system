@extends('frontend.layouts.app')

@section('sidebar')

    @include('frontend.account.includes.menu')

@endsection

@section('content')

    <h1 class="no-top-margin">Account</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="jumbotron">
                <div class="jumbotron-contents">
                    <div class="row">
                        <div class="col-xs-6">
                            <h2>Credit Balance</h2>
                            <div class="fa-3x">{!! config('app.pricing.credit_icon') !!} {!! $logged_in_user->professional->account->credits_remain !!}
                                <a class="btn btn-success btn-lg" href="{{ route('frontend.account.credits_purchase', $logged_in_user->professional->account->id) }}"><i class="fa fa-cart-plus"></i> Buy Credits</a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <h2>Package</h2>
                            <div class="badge label-primary fa-2x"><i class="fa fa-cube"></i> {{ $logged_in_user->professional->account->package->title }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection