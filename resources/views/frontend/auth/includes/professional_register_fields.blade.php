<h2>Complete Form</h2>

<div class="alert alert-warning h5"><i class="fa fa-info-circle"></i> The more and accurate information you provide the better your chances are of customers accepting your quotes.</div>

<div class="box box-success">
    <div class="box-body">
        <h4>Business</h4>

        <div class="form-group">
            {{ Form::label('title', 'Business Name', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Company name', 'required', 'autocomplete'=>'off']) }}
                <div class="help-block with-errors"></div>
                <div style="display: none" id="company_search" class="suggestresults">
                    <div class="h6 badge label-warning">Claim existing listing:</div>
                    <a onclick="$('#company_search').hide()" href="javascript:void(0)"><i class="fa fa-close fa-lg"></i> close</a>
                </div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('logo', 'Business Logo', ['class' => 'col-lg-2 control-label', 'accept'=>'image/*']) }}

            <div class="col-lg-10">
                {{ Form::file('logo') }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('company_type', 'Company Type', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::select('company_type', ['Sole' => 'Sole Trader','CC' => 'Close Corporation','Partnership', 'Private Company' => 'Private Company (PTY LTD)', 'Public Company' => 'Public Company (LTD)', 'Inc' => 'Personal Liability Company', 'Business trust', 'External company', 'Non-Profit'], null, ['class' => 'form-control','id'=> 'company_type', 'placeholder' => 'Select Company Type', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('year_started', 'Year Started', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::selectYear('year_started', 1980, date('Y'), null, ['class' => 'form-control','id'=> 'company_type', 'placeholder' => 'Business start year']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('vat_registered', 'VAT Number (optional)', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('vat_registered', null, ['class' => 'form-control']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="company_entity_div" style="display: none">
            <div class="form-group">
                {{ Form::label('company_registration', 'Company Reg No?', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::text('company_registration', null, ['class' => 'form-control company_entity', 'placeholder' => '']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>

        <div class="soletrader_div" style="display: none">
            <div class="form-group">
                {{ Form::label('id_number', 'ID number or Work Permit', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::text('id_number', null, ['class' => 'form-control soletrader', 'placeholder' => '']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>

        <hr>

        <h4>Choose Services</h4>
        <p>Click to expand into the different levels of services.</p>

        @include('backend.service.includes.partials.service-list')

        <div class="form-group">
            {{ Form::label('services_manual', 'Other Services', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('services_manual', null, ['class' => 'form-control', 'placeholder' => '']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <hr>

        <h4>Profile (Public)</h4>


        <div class="form-group">
            {{ Form::label('description', 'About your business:', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::textarea('description', null, ['class' => 'form-control', 'rows'=> 8, 'placeholder' => 'Description', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('telephone', 'Contact Number', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('telephone', null, ['class' => 'form-control', 'placeholder' => '', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('company_email', 'Email', ['class' => 'col-lg-2 control-label business_email']) }}
            <div class="col-lg-10">
                {{ Form::text('company_email', null, ['class' => 'form-control', 'placeholder' => '', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('website_url', 'Website URL (optional)', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('website_url', null, ['class' => 'form-control', 'placeholder' => 'http://']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <hr>

        <h4>Location</h4>

        <div class="form-group">
            {{ Form::label('optArea', 'Location', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                @include('frontend.wizard.custom.geocode', ['settings' => ['types' => '', 'disable_map' => true]])
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('distance_travelled', 'Willing to travel', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::select('distance_travelled', ['0' => 'Not able to travel','1'=>'1 km','5'=>'5 km','10'=>'10 km','20'=>'20 km','30'=>'30 km','50'=>'50 km','100'=>'100 km','200'=>'200 km'], null, ['class' => 'form-control', 'placeholder' => 'Choose Distance', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="row padding15 bg-gray">
            <div class="col-xs-12">

                <div class="alert alert-warning"><b>Private Information:</b> Notification methods & resource documents not accessible to public or clients.</div>

                <h4><i class="fa fa-bell-o"></i> Notifications</h4>
                <p class="">Notifications from {{ config('app.name') }} will be sent to these details.</p>

                <div class="form-group">
                    {{ Form::label('notify_email', 'Email', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::email('notify_email', null, ['class' => 'form-control', 'placeholder' => 'Email address', 'required']) }}
                        <div class="help-block with-errors"></div>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('Mobile', 'Mobile (optional)', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::number('notify_mobile', null, ['class' => 'form-control mobile_input', 'placeholder' => 'eg. 0821234567', 'data-minlength'=>'10', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->


                <h4><i class="fa fa-upload"></i> Documents</h4>
                <div class="alert alert-warning soletrader_div" style="display: none">
                    Registration requires certain documents to verify your credentials.
                    <h6>Required Documents:</h6>
                    <ul>
                        <li>Proof of address (not older than 3 months)</li>
                    </ul>
                </div>

                <div class="alert alert-info">
                    Upload copies of license or qualifications that you might have in order to validate your expertise.
                    <br/>Eg. Electrical / Plumbing licenses etc.
                </div>


                <div class="form-group">
                    {{ Form::label('resources', 'Uploads', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">

                        @include('frontend.media.includes.partials.multi_uploader', ['group'=> 'resources', 'caption_otions' => ['Certification' => 'Certification', 'Qualification' => 'Qualification', 'License' => 'License', 'PoD' => 'Proof of Address', 'Other' => 'Other']])


                    </div><!--col-lg-10-->
                </div>

            </div>


        </div>


        {{ Form::hidden('role_id', 2) }}

        <hr>

    </div><!-- /.box-body -->
</div><!--box-->

@section('after-scripts-end')
    <script>
        $('#website_url').on('focus', function () {
            if ($(this).val() == '') {
                $(this).val('http://');
            }
        })

        $('#website_url').on('blur', function () {
            if ($(this).val() == 'http://') {
                $(this).val('');
            }
        })

        var toggle_el = 'company';
        $('#company_type').on('change', function() {
            if ($(this).val() == 'Sole') {
                $('.soletrader').attr('required','required');
                $('.company_entity').removeAttr('required','required');
                $('.company_entity_div').hide();
                $('.soletrader_div').show();
            } else {
                $('.soletrader').removeAttr('required','required');
                $('.company_entity').attr('required','required');
                $('.company_entity_div').show();
                $('.soletrader_div').hide();
            }

        });

        $('.business_email').blur(function() {
            if ($('#notify_email').val() == '') {
                $('#notify_email').val($('.business_email').val());
            }
            if ($('#email').val() == '') {
                $('#email').val($('.business_email').val());
            }
        });


        var ss = '';
        var timer = 0;

        $('#title').on('keyup focus', function() {
            var el = $(this);
            var searchstring = el.val();

            if (ss == searchstring) return false;

            ss = searchstring;

            clearTimeout(timer);

            if (searchstring.length < 2 && searchstring.length > 0) return false;


            timer = setTimeout(function(){
                currentRequest = $.ajax({
                    url: '{{ route("api.professional.search") }}',
                    method: "GET",
                    dataType: "json",
                    data:  { string: searchstring },
                    beforeSend : function()    {
                        if(currentRequest != null) {
                            currentRequest.abort();
                        }
                    },
                    success: function (response) {
                        if (response.length != 0) {
                            $('#company_search .search_list').remove();

                            var items = [];
                            $.each(response, function (key, val) {
                                items.push("<li><a href='/professional/" + val.id + "-" + val.slug + "?claim=1'><i class='fa fa-star-o'></i> " + val.title + "</a></li>");
                            });

                            $("<ul/>", {
                                "class": "search_list servicelist list-unstyled",
                                html: items.join("")
                            }).appendTo('#company_search');

                            $('#company_search').show();
                        }
                    },
                });
            },200);
        });

    </script>
@append