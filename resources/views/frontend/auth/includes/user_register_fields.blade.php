<h2 class="no-top-margin">User Details</h2>
<p>You will use these details to login.</p>

<div class="form-group">
    {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::input('first_name', 'first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name', 'required']) }}
        <div class="help-block with-errors"></div>
    </div><!--col-md-6-->
</div><!--form-group-->

<div class="form-group">
    {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::input('last_name', 'last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name', 'required']) }}
        <div class="help-block with-errors"></div>
    </div><!--col-md-6-->
</div><!--form-group-->

<div class="form-group">
    {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required']) }}
        <div class="help-block with-errors"></div>
    </div><!--col-md-6-->
</div><!--form-group-->

<div class="form-group">
    {{ Form::label('mobile', 'Mobile (optional)', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::number('mobile', 'mobile', ['class' => 'form-control mobile_input', 'data-minlength'=>'10', 'placeholder' => 'eg. 0821234567', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));']) }}
    </div><!--col-md-6-->
</div><!--form-group-->

<div class="form-group">
    {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::input('password', 'password', null, ['class' => 'form-control', 'data-minlength'=>'6', 'placeholder' => trans('validation.attributes.frontend.password'), 'required']) }}
        <div class="help-block with-errors"></div>
    </div><!--col-md-6-->
</div><!--form-group-->

<div class="form-group">
    {{ Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'data-match'=>'#password', 'data-match-error'=>'Whoops, these don\'t match', 'placeholder' => trans('validation.attributes.frontend.password_confirmation'), 'required']) }}
    </div><!--col-md-6-->
</div><!--form-group-->

@if (Request::segment(2) == 'professional')
    <div class="form-group">
        {{ Form::label('image', 'Photo of yourself', ['class' => 'col-md-4 control-label', 'accept'=>'image/*']) }}

        <div class="col-md-6">
            <i class="fa fa-user-circle default default-thumb"></i>
            <div class="alert bg-info"><i class="fa fa-info-circle"></i> A good photo of yourself will increase your chance of landing new clients.</div>
            {{ Form::file('image') }}
        </div><!--col-lg-10-->
    </div><!--form control-->
@endif

@if (config('access.captcha.registration'))
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {!! Form::captcha(null, ['data-callback'=> 'enableBtn']) !!}
            {{ Form::hidden('captcha_status', 'true') }}
        </div><!--col-md-6-->
    </div><!--form-group-->
@endif

@section('after-scripts-end')
    @if (config('access.captcha.registration'))
        {!! Captcha::script() !!}
    @endif
@append