@extends('frontend.layouts.app')

@section ('title', 'Create Account on '.config('app.name'))

@section('header')
    <div class="alert alert-success">
        <i class="fa pull-left fa-2x fa-lock"></i><span class="h5"> SECURE </span>
        Your connection to <b>HandyHire.co.za</b> is encrypted and safe.<br/><b>Whois:</b><i>South African company</i> - ONE INTERNET MEDIA CC t/a HandyHire - CK2008/162526/23 - <i class="fa fa-phone"></i> Tel: 021 917 1719
    </div>
@endsection

@section('content')
    <div class="row">

        <div class="{{ isset($professional_reg) && $professional_reg?'col-xs-12 col-md-9 col-md-offset-1':'col-xs-12 col-md-8 col-md-offset-2' }}">

            <h1 class="">{{ isset($professional_reg) && $professional_reg?'Join as Professional':'Create Account' }}</h1>

            <div class="panel panel-default">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified">
                    <li class="fa-lg {{ isset($professional_reg) && $professional_reg?'':'active' }}"><a href="{{ route('frontend.auth.register') }}"><i class="fa fa-user"></i> Customer</a></li>
                    <li class="fa-lg {{ isset($professional_reg) && $professional_reg?'active':'' }}"><a href="{{ route('frontend.auth.register_professional') }}"><i class="fa fa-briefcase"></i> Professional</a></li>
                </ul>

            <div class="panel-body">

                    @if(isset($professional_reg) && $professional_reg)

                    {{ Form::open(['route' => 'frontend.auth.register_professional', 'id'=>'professionalForm', 'class' => 'form-horizontal', 'novalidate', 'enctype' => 'multipart/form-data', 'data-toggle'=>'validator', 'data-disable'=>'false']) }}


                        <div id="prowizard">

                            <div style="display: none" class="navbar">
                                <div class="navbar-inner" >
                                    <div class="container">
                                        <ul>
                                            <li class="active"><a href="#info" data-toggle="tab">Business Info</a></li>
                                            <li><a href="#profile" data-toggle="tab">Profile Page</a></li>
                                            <li><a href="#branch" data-toggle="tab">Branches</a></li>
                                            <li><a href="#system" data-toggle="tab">Settings</a></li>
                                            <li><a href="#user" data-toggle="tab">User</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <div class="modal-header">
                                <div class="progress">
                                    <div id="bar" class="bar progress-bar progress-bar-primary" role="progressbar">
                                        <span></span>
                                    </div>
                                </div>
                            </div>



                            <div class="tab-content">
                                <div class="alert bg-info">Please go ahead and enter your business details below then submit the form.</div>

                                @include('frontend.professional.includes.form_fields')

                                <div role="tabpanel" class="tab-pane" id="user">
                                    @include('frontend.auth.includes.user_register_fields')
                                </div>
                            </div>


                            <ul class="pager wizard">
                                <li class="previous btn-lg"><a href="javascript:;">Back</a></li>
                                <li id="nextbtn" class="next btn-lg"><a href="javascript:;">Next</a></li>
                                <li class="finish btn-lg"><a href="javascript:;">Finish</a></li>
                            </ul>
                        </div>

                    @else
                    <div class=" tab-content">
                        <div class="row text-center">
                            {!! $socialite_links !!}
                        </div>

                        {{ Form::open(['route' => 'frontend.auth.register','enctype' => 'multipart/form-data', 'data-toggle'=>'validator', 'class' => 'form-horizontal']) }}


                        @include('frontend.auth.includes.user_register_fields')


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {{ Form::submit(trans('labels.frontend.auth.register_button'), ['class' => 'btn btn-success btn-lg', 'id' => 'register_submit']) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                    </div>

                    @endif


                        {{ Form::close() }}

            </div>



            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
@endsection


@section('after-scripts-end')
    <script>
        var back = true;

        $('#prowizard').bootstrapWizard({withVisible:false, onNext: function(tab, navigation, index) {

            if(checkinputs() == 1) {
                return false;
            }

            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#prowizard').find('.bar').css({width:$percent+'%'});
            $('#prowizard .progress-bar').html(Math.floor($percent)+ '% Complete');
        },
            onFinish: function() {
                if(!checkinputs()) {
                    back = false;

                    $('.finish a').hide();
                    $('.finish').append('<i class="fa fa-gear fa-spin fa-fw"></i>');

                    $('#professionalForm').submit();
                }
            }
        });

        $('#website_url').on('focus', function () {
            if ($(this).val() == '') {
                $(this).val('http://');
            }
        })

        $('#website_url').on('blur', function () {
            if ($(this).val() == 'http://') {
                $(this).val('');
            }
        })

        $('#company_type').on('change', function() {
            if ($(this).val() == 'Sole') {
                $('.soletrader').attr('required','required');
                $('.company_entity').removeAttr('required','required');
                $('.company_entity_div').hide();
                $('.soletrader_div').show();
            } else {
                $('.soletrader').removeAttr('required','required');
                $('.company_entity').attr('required','required');
                $('.company_entity_div').show();
                $('.soletrader_div').hide();
            }
        });

        window.onbeforeunload = function() {
            if(back == true)
                return "Your input details will not be saved, are you sure?";
        };


        $("#register_submit").prop("disabled",true);

        function enableBtn() {
            $("#register_submit").prop("disabled",false);
        }

        $(".mobile_input").on('blur',function() {
            $(this).val($(this).val().replace(/\s/g, ""));
            if ($(this).val().length == 10) {
                isMobile($(this).val());
            }
        });

        function isMobile(mobile) {
            jQuery.ajax({
                url: "/api/auth/is_mobile",
                data: {_token: Laravel.csrfToken, mobile: mobile},
                dataType: "json",
                type: "POST",
                success: function (resp) {
                    if (resp.status == 'error') {
                        sweetAlert('Mobile number not valid', 'Ensure that the mobile is in this format: 0821234567 (10 digits) and that it is a valid mobile.', 'warning');
                    } else if (resp.status == 'success') {
                        return true;
                    }
                }
            });
        }

        $("#email").on('blur',function() {
            if (isEmail($(this).val())) {
                jQuery.ajax({
                    url: "/api/auth/is_email_in_use",
                    data: {_token: Laravel.csrfToken, email: $('#email').val()},
                    dataType: "json",
                    type: "POST",
                    success: function (resp) {
                        if (resp.data.is_used > 0) {
                            $("label[for='email']").addClass('bg-danger');
                            sweetAlert('User Account Found', 'That email is already in use by another account.', 'warning');
                        } else {
                            $("label[for='email']").removeClass('bg-danger');
                        }
                    }
                });
            }
        });

        function checkinputs() {
            var unfilled = 0;
            $('#prowizard input,#prowizard textarea,#prowizard select').filter('[required]:visible').each(function(){
                if (($(this).is(':radio') || $(this).is(':checkbox')) && !$('input[name="'+$(this).attr('name')+'"]:checked').val()) {
                    sweetAlert('Please select a option');
                    unfilled = 1;
                } else if(!$(this).val()) {
                    sweetAlert('Please complete the question');
                    unfilled = 1;
                } else if ($(this).is('#customer_email') && !isValidEmailAddress( $(this).val() )) {
                    sweetAlert('Enter valid email');
                    unfilled = 1;
                }

                if (unfilled == 1) {
                    $(this).trigger('input');
                }
            });
            return unfilled;
        }

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        @if ($socialite_links)
        function SocialLogin(url) {
            var signinWin = window.open(url, "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 500 + ",top=" + 200);

            var pollTimer = window.setInterval(function() {
                if (signinWin.closed !== false) {
                    window.clearInterval(pollTimer);
                    if (signinWin.closed || signinWin.closed !== false) {
                        if($('#rootwizard').length && $('#rootwizard').is(':visible')) {

                        } else {
                            location.reload();
                        }
                    }
                }
            }, 200);
        }
        @endif


        @if(!isset($professional))
        var ss = '';
        var timer = 0;

        $('#title').on('keyup focus', function() {
            var el = $(this);
            var searchstring = el.val();

            if (ss == searchstring) return false;

            ss = searchstring;

            clearTimeout(timer);

            if (searchstring.length < 2 && searchstring.length > 0) return false;


            timer = setTimeout(function(){
                currentRequest = $.ajax({
                    url: '{{ route("api.professional.search") }}',
                    method: "GET",
                    dataType: "json",
                    data:  { string: searchstring },
                    beforeSend : function()    {
                        if(currentRequest != null) {
                            currentRequest.abort();
                        }
                    },
                    success: function (response) {
                        if (response.length != 0) {
                            $('#company_search .search_list').remove();

                            var items = [];
                            $.each(response, function (key, val) {
                                items.push("<li><a href='/professional/" + val.id + "-" + val.slug + "?claim=1'><i class='fa fa-star-o'></i> " + val.title + "</a></li>");
                            });

                            $("<ul/>", {
                                "class": "search_list servicelist list-unstyled",
                                html: items.join("")
                            }).appendTo('#company_search');

                            $('#company_search').show();
                        }
                    },
                });
            },200);
        });

        @endif

    </script>

@append
