@extends ('frontend.layouts.app')

@section ('title', 'Review '.$quote->professional->title)

@section('content')

    <h2>
        Review {{ $quote->project->user->name }}
    </h2>

    <div>
        {!!  $quote->project->user->image_html !!}
    </div>

    @if( isset($client_review) )
        {{ Form::model($client_review, ['route' => ['frontend.client_review.update', $client_review], 'novalidate', 'data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        {{ Form::open(['route' => 'frontend.client_review.store', 'class' => 'form-horizontal', 'novalidate', 'data-toggle' => 'validator', 'role' => 'form', 'method' => 'post']) }}
    @endif

    <div class="box box-success">
        <div class="box-body">

            <div class="form-group">
                {{ Form::label('recommended', 'Great client to work for?', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('recommended', [1 => 'Yes', 0 => 'No'], 1, ['class' => 'form-control', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('comments', 'What would you like to say to client?', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::textarea('comments', null, ['class' => 'form-control', 'rows'=>'4', 'placeholder' => 'Your message here...', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_pay', trans('labels.client_review.rating_pay_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_pay', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_understanding', trans('labels.client_review.rating_understanding_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_understanding', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_clear', trans('labels.client_review.rating_clear_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_clear', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_overall', 'Overall Rating', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_overall', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-body">

            {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}

        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::hidden('user_id', $quote->project->user_id) }}
    {{ Form::hidden('quote_id', $quote->id) }}
    {{ Form::hidden('professional_id', $quote->professional_id) }}
    {{ Form::hidden('project_id', $quote->project_id) }}

    {{ Form::close() }}

@stop

@section('after-scripts-end')

    <script type="text/javascript">
        $(function() {
            $('.barrating').barrating({
                theme: 'fontawesome-stars-o',
                showSelectedRating: true
            });
        });
    </script>
@append

