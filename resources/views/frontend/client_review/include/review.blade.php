<div class="chat-image pull-left">
    {!!  $review->professional->user->getImageHtml('icon') !!}
</div>

<div class="chat-message">

    <b>{{ $review->professional->user->name }}</b> from <a href="{{ route('public.professional.show',[$review->professional->id, $review->professional->slug]) }}">{{ $review->professional->title }}</a>
    <p>{{ $review->comments }}</p>

    <div class="">
        {!!  Helper::showStars($review->rating_overall) !!}
        <span><i class="fa fa-clock-o"></i> {{ $review->created_at->diffForHumans() }}</span>
        <span><i class="fa fa-map-marker"></i> {{ $review->user->area_city }}</span>
        <a class="badge label-primary" onclick="$('#ratings{{ $review->id }}{{ $review->user_id }}').toggle()" href="javascript:void(0)">See Ratings <i class="fa fa-angle-down"></i></a>
    </div>

    <div class="all_ratings" id="ratings{{ $review->id }}{{ $review->user_id }}" style="display: none">
        <table class="table table-striped">
              <tr>
                <th>Payment</th>
                <td >
                    {!!  Helper::showStars($review->rating_pay) !!}
                </td>
            </tr><!--form control-->

            <tr>
                <th>Client is understanding</th>
                <td>
                    {!!  Helper::showStars($review->rating_understanding) !!}
                </td>
            </tr><!--form control-->

            <tr>
                <th>Clarity of instructions</th>
                <td>
                    {!!  Helper::showStars($review->rating_clear) !!}
                </td>
            </tr><!--form control-->

        </table>
    </div>
</div>