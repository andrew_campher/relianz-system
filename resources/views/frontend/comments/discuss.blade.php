<div id="app" class="comments">

    <comments id_token="{{ access()->user()->api_token }}" disable_first_comment="{{ isset($disable_first_comment) && $disable_first_comment ?$disable_first_comment:0 }}" disable_add="{{ isset($disable_add) && $disable_add ?$disable_add:0 }}" user_id="{{ access()->id() }}" model="{{ $model }}" itemid="{{ $id }}" user_icon_url="{{ access()->user()->icon_url }}"></comments>

</div>
