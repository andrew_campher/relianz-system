<div id="com{{ $comment->id }}" class="{{ access()->id() == $comment->user_id ? 'my_comment' : '' }}">
    <div class="chat-image pull-left">
        {!!  $comment->user->getImageHtml('icon') !!}
        {!! $comment->user->online_label !!}
    </div>
    <div class="card chat-message ">
        <div class="card-content">
            <div class="card-body">
                <div class="text-bold">{{ $comment->user->name }}</div>
            <p>{{ $comment->body }}</p>
            </div>
            <div class="chattime" title="{{ $comment->created_at }}">{{ $comment->created_at->diffForHumans() }}</div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>