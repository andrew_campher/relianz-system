<li class="dropdown notifs">
    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true">
        <i class="fa fa-comments fa-lg"></i> <span id="messages_count" class="badge label-danger" {!!   $logged_in_user->unread_comment_rooms->count() ? '': 'style="display:none"' !!}>{{ $logged_in_user->unread_comment_rooms->count() }}</span>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu dropdown-messages">
        @if($logged_in_user->unread_comment_rooms->count())
            @foreach($logged_in_user->unread_comment_rooms AS $comments)
                <li>
                    {{ link_to_route('frontend.'.$comments->roomtable_type.'.show', str_limit($comments->body, 50), $comments->roomtable_id) }}
                </li>
            @endforeach
        @else
            <li>You have no messages</li>
        @endif
    </ul>
</li>