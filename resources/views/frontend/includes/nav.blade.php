<div class="nav-wrapper">
    <nav data-spy=affix data-offset-top=1 class="navbar navbar-custom navbar-default {{ strpos(Route::current()->getName(), 'public.') !== false?'navbar-public':'' }}" >
        <div class="container-fluid">

            <div class="navbar-header">
                <a class="navbar-brand" href="{{ access()->id() && access()->user()->isRole('Professional')?route('frontend.professional.dashboard'):route('public.index') }}">Relianz Foods</a>


                <button type="button" class="nav-search navbar-toggle collapsed" data-toggle="collapse" data-target="#frontend-navbar-collapse">
                    <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div><!--navbar-header-->

            <div class="collapse navbar-collapse" id="frontend-navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    @if (! $logged_in_user)

                        <li>{{ link_to_route('frontend.auth.login', trans('navs.frontend.login')) }}</li>
                    @else
                        <li>{{ link_to_route('admin.dashboard', 'Admin') }}</li>
                    @endif
                </ul>

                @if ($logged_in_user)
                    <notifications id_token="{{ access()->user()->api_token }}" user_id="{{ access()->id() }}"></notifications>
                @endif
            </div><!--navbar-collapse-->
        </div><!--container-->
    </nav>
</div>