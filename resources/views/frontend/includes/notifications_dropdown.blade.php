<li class="dropdown notifs">
    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true">
        <i class="fa fa-globe fa-lg"></i> <span id="notifs_count" class="badge label-danger" {!!   $logged_in_user->notifications->count() ? '': 'style="display:none"' !!}>{{ $logged_in_user->notifications->count() }}</span>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu dropdown-notifs">
        @if($logged_in_user->notifications->count())
            @foreach($logged_in_user->notifications AS $notification)

                <li class="{{ !$notification->read_at ? 'unread' : '' }}">
                @if(View::exists('notifications.types.'.snake_case(class_basename($notification->type))))
                    @include('notifications.types.'.snake_case(class_basename($notification->type)))
                @else
                    @include('notifications.types.general')
                @endif
                </li>
            @endforeach
        @else
            <li>You have no notifications</li>
        @endif
    </ul>
</li>