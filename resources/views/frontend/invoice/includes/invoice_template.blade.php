<section class="content content_content">
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <h2 class="pull-left"><i class="fa fa-handshake-o"></i> {{ config('app.name') }}</h2>
                    <div class="pull-right text-right">
                        <h2>INVOICE</h2>
                        <b>Status {!! $invoice->status_label !!}</b>
                        Date: {{ $invoice->created_at->format(config('app.date.short')) }}

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>
                        {{ config('app.company.name') }}<br>
                        {{ config('app.company.registration') }}
                    </strong><br>
                    {{ config('app.company.address') }}
                </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>{{ $invoice->professional->user->name }}</strong>
                    <br>
                    Address:
                    {{ $invoice->professional->area }}<br>
                    {{ $invoice->professional->user->email }}
                </address>
                </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col text-right">
                <b>Invoice {{ config('app.invoice.prefix') }}{{ $invoice->id }}</b><br>
                <br>
                <b>Account:</b> ACC0{{ $invoice->account_id }}
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>{{ $invoice->description }}</td>
                        <td>{!!  Helper::formatPrice($invoice->amount) !!}</td>
                        <td>{!!  Helper::formatPrice($invoice->amount) !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div><!-- /.col -->
        </div><!-- /.row -->

        @if($logged_in_user->id == $invoice->professional->user_id && $invoice->status == 0)
        <div class="row">
            <!-- accepted payments column -->
            <div class="col-md-12">
                <h4 class="lead">Amount Due by {{ $invoice->created_at->format(config('app.date.short')) }}</h4>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Total:</th>
                            <td><b>{!!  Helper::formatPrice($invoice->amount) !!}</b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
        @endif

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">

                <a href="{{ route('frontend.invoice.download', $invoice->id) }}" class="btn btn-primary pull-left" style="margin-right: 5px;"><i class="fa fa-download"></i>
                    Generate PDF
                </a>

                @if($logged_in_user->id == $invoice->professional->user_id && $invoice->status == 0)
                <form action="{{ route('frontend.payment.process') }}" method="post">

                    {{ csrf_field() }}

                    <input type="hidden" name="amount" type="text" value="{{ number_format($invoice->amount, 2) }}">
                    <input type="hidden" name="description" type="text" value="{{ $invoice->description }}">
                    <input type="hidden" name="payment_method" type="text" value="payfast">
                    <input type="hidden" name="m_payment_id" type="text" value="{{ $invoice->id }}">

                    <input type="hidden" name="account_id" type="text" value="{{ $invoice->account_id }}">

                    <button type="submit" class="btn btn-success pull-right">Pay with <b>PayFast</b> <img style="vertical-align: top" src="{{ asset('img/frontend/payfast/v2_logo.png') }}" height="30px"> <i class="fa fa-2x fa-arrow-circle-right"></i></button>
                </form>
                @endif


            </div>
        </div>
    </section>
</section>