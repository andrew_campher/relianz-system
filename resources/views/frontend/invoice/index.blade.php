@extends('frontend.layouts.app')

@section ('title', 'Invoices')

@section('sidebar')

    @include('frontend.account.includes.menu')

@endsection

@section('content')

    <h1 class="no-top-margin">Invoices</h1>

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <div class="row">
                <div class="col-xs-12">
                    <table class="table ">
                        <tr>
                            <th>Invoice No</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        @if($invoices)
                            @foreach($invoices AS $invoice)
                                <tr>
                                    <td>{{ $invoice->id }}</td>
                                    <td>{{ $invoice->created_at->format(config('app.date.short')) }}</td>
                                    <td>{!!  \App\Helpers\Helper::formatPrice($invoice->amount) !!}</td>
                                    <td>{!! $invoice->status_label !!}</td>
                                    <td class="text-right"><a class="btn btn-primary btn-sm" href="{{ route('frontend.invoice.show', $invoice->id) }}">View</a></td>
                                </tr>

                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection