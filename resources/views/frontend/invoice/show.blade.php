@extends('frontend.layouts.app')

@section ('title', 'Invoice '.config('app.invoice.prefix').' '.$invoice->id)

@section('sidebar')

    @include('frontend.account.includes.menu')

@endsection

@section('content')
<div class="jumbotron">
    <div class="jumbotron-contents">
    @include('frontend.invoice.includes.invoice_template')
    </div>
</div>

@endsection