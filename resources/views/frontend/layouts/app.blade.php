@if(Request::ajax())
@yield('content')

@yield('search_js')

@yield('before-scripts-end')



@yield('after-scripts-end')

@else
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>

    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', app_name())">

    @yield('meta')

    <!-- Styles -->
@yield('before-styles-end')

{{ Html::style(elixir('css/frontend.css')) }}

@yield('after-styles-end')

<!-- Html5 Shim and Respond.js IE8 support of Html5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    {{ Html::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
                'api_token' => (access()->id()?access()->user()->api_token:'')
        ]); ?>
    </script>
</head>
<body id="app-layout" class="{{ isset($body_class) && $body_class?$body_class:'' }}" data-spy="scroll" data-target="#sidebar-scroll">
<div id="app">
    @include('includes.partials.logged-in-as')

    <div class="topsection">
        @include('frontend.includes.nav')

        @hasSection('header')

        @yield('header')
        {!! Breadcrumbs::renderIfExists() !!}
        @else
            {!! Breadcrumbs::renderIfExists() !!}


            @include('includes.partials.messages')
        @endif

    </div>

    @hasSection('sidebar')
    <div class="fullwidth">
        <div class="sidebar">
            <div class="inside row">
                <div class="sidebar_left col-xs-12 col-sm-5 col-md-4 col-lg-4">
                    <div id="sidebar-scroll" class="sidebar_in">
                        @yield('sidebar-menu')

                        <div class="sidebar-content">
                            @yield('sidebar')
                        </div>
                    </div>
                </div>
                <div class="sidebar_content col-xs-12 col-sm-7 col-md-8 col-lg-7">
                    <div class="sidebar_content_in">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div><!-- container -->
    @else
        @hasSection('full-content')

        @yield('full-content')

        @else
            <div class="container container-main">
                @hasSection('left-column')
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        @yield('left-column')
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        @yield('content')
                    </div>
                </div>
                @else
                    @yield('content')
                @endif
            </div><!-- container -->
        @endif
    @endif

</div><!--#app-->


<footer class="footer">
    <div class="footer-container">
        <div class="row">
            <div class="col-xs-6">


            </div>
            <div class="col-xs-6 text-right">
                &copy; Copyright {{ config('app.name') }} {{ date('Y') }}
            </div>
        </div>
    </div>
</footer>

{!! Html::script(elixir('js/public.js')) !!}

@if(access()->id())
    {!! Html::script(elixir('js/frontend.js')) !!}
@endif



<!-- Scripts -->
@yield('before-scripts-end')
<script>
    /* Opens Modal*/
    $( document ).ready(function() {
        $('body').append('<div class="modal fade" id="sModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"><div class="modal-dialog" role="document"><div class="modal-content"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><div class="modal-body"></div></div></div></div>');
        /*site.js Init*/
        init();
    });

</script>

@yield('search_js')

@yield('after-scripts-end')

@include('includes.partials.ga')
</body>
</html>
@endif