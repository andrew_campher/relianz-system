
<input type="hidden" name="img_x" />
<input type="hidden" name="img_y" />
<input type="hidden" name="img_width" />
<input type="hidden" name="img_height" />

@section('after-scripts-end')
    {{ Html::style("vendor/cropper/cropper.min.css") }}
    {{ Html::script("vendor/cropper/cropper.min.js") }}
    <script>
        var $image = $(".image_crop > img");
        originalData = {};

        $image.cropper({
            aspectRatio: {{ config('app.images.thumb_height') }}/{{ config('app.images.thumb_width') }},
            minCropBoxWidth: {{ config('app.images.thumb_width') }},
            minCropBoxHeight: {{ config('app.images.thumb_height') }},
            resizable: true,
            zoomable: false,
            rotatable: false,
            multiple: true,
            crop: function(e) {
                $("input[name='img_x']").val(e.x);
                $("input[name='img_y']").val(e.y);
                $("input[name='img_width']").val(e.width);
                $("input[name='img_height']").val(e.height);
            }
        });
    </script>
@endsection