@if(isset($media) && $media)

    @php
        $group_dir = (isset($group) && $group ? $group : 'general' );

    @endphp

<div id="carousel" class="carousel slide gallery_carousel" data-ride="carousel">
    <!-- Indicators -->
    @php($cnt=0)
    <ol class="carousel-indicators">
        @foreach($media AS $image)
            <?php

            switch ($image->file_type) {
                case "jpeg":
                case "jpg":
                case "png":
                case "gif":
                ?>
                    <li data-target="#carousel" data-slide-to="{{ $cnt }}" {{ $cnt==1? 'class="active"':'' }}></li>
                <?php
                break;
            }

                $cnt++;
            ?>
        @endforeach
    </ol>


    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        @php($cnt=0)
        @foreach($media AS $image)

            @php
                $img_path = 'files/'.$image->modelname_type.'/'.$image->modelname_id.'/'.$group_dir.'/';

            @endphp

                <?php
                switch ($image->file_type) {
                    case "jpeg":
                    case "jpg":
                    case "png":
                    case "gif":

                    ?>
                    <div class="item {{ $cnt==0? 'active':'' }}">
                        <img class="img-rounded" src="{{ asset($img_path.$image->filename) }}" />

                        @if($image->title || $image->details)
                            <div class="carousel-caption">
                                <div class="caption text-center">
                                    @if($image->title)
                                        <h3 class="no-margin">{{ $image->title }}</h3>
                                    @endif
                                    @if($image->details)
                                        <p>{{ $image->details }}</p>
                                    @endif
                                </div>
                            </div>
                        @endif

                    </div>
                    <?php
                    break;
                }

                    $cnt++;
                ?>
        @endforeach

    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

@else
    <div class="alert info">There are no files</div>
@endif

@section('after-scripts-end')
<script>
    $('#carousel').carousel();
</script>
@append