@if(isset($media) && $media)

    @php
        $group_dir = (isset($group) && $group ? $group : 'general' );

    @endphp

<div class="gallery_items">
@foreach($media AS $file)

    @php
        $file_path = 'files/'.$file->modelname_type.'/'.$file->modelname_id.'/'.$group_dir.'/';

        if (!file_exists($file_path.$file->filename))
            continue;
    @endphp
            <div id="media_file_{{ $file->id }}" class="thumbnail">
                <?php
                switch ($file->file_type) {
                    case "jpeg":
                    case "jpg":
                    case "png":
                    case "gif":
                        ?>

                        <a class="" data-toggle="firemodal" data-itemprop="image" href="{{ asset($file_path.$file->filename) }}" >
                            <img src="{{ asset($file_path.'thumbnail/'.$file->filename) }}" />
                        </a>
                        <?php
                    break;

                    default:
                        ?>
                        <a target="_blank" href="{{ asset($file_path.$file->filename) }}" >
                            <i class="fa fa-file fa-2x"></i><br>{{ $file->filename }}
                        </a>
                        <?php
                    break;

                }

                ?>

                    @if($file->title || $file->details)
                        <div class="gallery-caption">
                            <div class="caption text-center">
                                @if($file->title)
                                    <h3 class="no-margin">{{ $file->title }}</h3>
                                @endif
                                @if($file->details)
                                    <p>{{ $file->details }}</p>
                                @endif
                            </div>
                        </div>
                    @endif

                @if(isset($edit) && $edit)
                    <i title="Remove" onclick="removeMedia('{{ route('api.media.destroy', $file->id) }}', {{ $file->id }})" class="close fa fa-times fa-2x"></i>
                @endif

            </div>
        @endforeach
        <div class="clearfix"></div>
    </div>

    @if(isset($edit) && $edit && (isset($user) || $logged_in_user))
    <script>
        function removeMedia(url, id) {
            $.ajax({
                url: url,
                type: 'DELETE',
                headers: {
                    'Authorization':'Bearer {{ isset($user) && $user?$user->api_token:$logged_in_user->api_token }}',
                },
                success: function(result) {
                    if (result.status == 'success') {
                        $('#media_file_' + id).remove();
                    }
                }
            });
        }
    </script>
    @endif

    @else
    <div class="alert info">There are no files</div>
@endif

