@php( $group_txt = isset($group) && $group? '_'.$group : '' )
<div class="row modal-header multiuploader" id="multiuploader{{ $group_txt }}">


    <div class="clearfix">
        <div class="">
            <div class="caption">
                <span class="btn btn-info btn-file" id="filei{{ $group_txt }}">
                   Upload Files... {{Form::file('file'.$group_txt.'[]', ['class'=>'fileinput', 'checkme', 'onchange'=>'loadFile'.$group_txt.'(this, event)', 'accept' => (isset($input_accept) && $input_accept ?$input_accept:'') ])}}
                </span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
</div>



@php
    $caption_inputs ='';
    if(isset($caption_otions) && $caption_otions) {
        $caption_inputs = '<select required name="file'.$group_txt.'_title[]" class="form-control checkme">';
        $caption_inputs .= '<option value="">- Select File Type -</option>';
        foreach ($caption_otions AS $key => $caption_otion) {
            $caption_inputs .= '<option>'.$caption_otion.'</option>';
        }
        $caption_inputs .= '</select>';
    } else {
        $caption_inputs = '<input type="text" name="file'.$group_txt.'_title[]" class="form-control input-clean" placeholder="Title?" />';
    }
    $caption_inputs .= ' <textarea name="file'.$group_txt.'_details[]" rows="3" class="form-control input-clean" placeholder="Description?"></textarea>';
@endphp

@section('after-scripts-end')
<script>
    var cnt=1;
    var loadFile{{ $group_txt }} = function(e, event) {
        var imgPath = $(e)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

        var filename = $(e).val().replace(/C:\\fakepath\\/i, '');

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            $('#multiuploader{{ $group_txt }}').prepend('<div class="upload_item img"><div class="thumbnail" id="cont' + cnt + '">' +
                    '<i title="Remove" onclick="$(this).closest(\'.img\').remove();" class="close fa fa-times fa-2x"></i>' +
                    '<img src="' + URL.createObjectURL(event.target.files[0]) + '" width="200px" id="output"/>' +
                    '{!! $caption_inputs !!}' +
                    '</div></div>');

        } else {
            $('#multiuploader{{ $group_txt }}').prepend('<div class="upload_item img"><div class="thumbnail" id="cont' + cnt + '">' +
                    '<i title="Remove" onclick="$(this).closest(\'.img\').remove();" class="close fa fa-times fa-2x"></i>' +
                    '<i class="fa fa-file fa-3x"></i> ' + filename +
                    '{!! $caption_inputs !!}' +
                    '</div></div>');
        }

        var inputs = $('#filei input');
        $('#cont'+cnt).append(inputs);
        $('#cont'+cnt+' .fileinput').addClass('invisible');
        /*Require
        * $('#cont'+cnt+' input').attr('checkme','checkme');*/

        cnt++;
        $('#filei{{ $group_txt }}').append('{{Form::file('file'.$group_txt.'[]', ['accept' => (isset($input_accept) && $input_accept ?$input_accept:''), 'onchange'=>'loadFile'.$group_txt.'(this, event)'])}}');


        var $form = $('form[data-toggle="validator"]')
        if ($form) {
            $form.validator('update')
        }

    };

</script>
@append