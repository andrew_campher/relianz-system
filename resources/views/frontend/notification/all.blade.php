@extends ('frontend.layouts.app')

@section ('title', 'All Notifications')

@section('content')

    <h1>All Notifications</h1>

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <ul class="list-group">
            @foreach($notifications AS $notification)
                <li class="list-group-item">
                    <a href="{{ $notification->url }}">{{ $notification->text }}</a>

                    <span class="badge label-primary" data-toggle="popover" data-content="{{ $notification->created_at }}">{{ $notification->human_date }}</span>

                    @if(!$notification->read_at)
                        <span class="badge label-danger pull-left">NEW</span>
                    @endif
                </li>
            @endforeach
            </ul>

            {{ $notifications->links() }}
        </div>
    </div>
@stop

