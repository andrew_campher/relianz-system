

<div class="form-group" style="display: none" id="by_email">
        {{ Form::label('customer_email', 'Email', ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            {{ Form::text('customer_email', (isset($logged_in_user->email)?$logged_in_user->email:null), ['class'=>'form-control', 'id' => 'customer_email','checkme','placeholder'=>'Enter email']) }}
        </div><!--col-lg-10-->
</div>
<div class="form-group" style="display: none" id="by_email_mobile">
    {{ Form::label('customer_mobile', 'Mobile', ['class' => 'col-lg-2 control-label']) }}

    <div class="col-lg-10">
    {{ Form::number('customer_mobile', null, ['class'=>'form-control mobile_input', 'id' => 'customer_mobile', 'data-minlength'=>'10', 'placeholder' => 'eg. 0821234567', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));','checkme']) }}
    </div><!--col-lg-10-->
</div>


@section('before-scripts-end')
<script>
    var user_email = '{{ isset($logged_in_user->email)?$logged_in_user->email:null }}';
    var user_mobile = '{{ isset($logged_in_user->mobile)?$logged_in_user->mobile:null }}';

    $("input[name='{{ $inputName }}']").click(function () {
        if (!user_email && $('#customer_email').val()) {
            user_email = $('#customer_email').val();
        }
        if (!user_mobile && $('#customer_mobile').val()) {
            user_mobile = $('#customer_mobile').val();
        }

        if ($(this).val() == 'by_email') {
            $('#by_email').css('display', 'block');
            $('#by_email_mobile').css('display', 'none');
            $('#customer_email').val(user_email);
            $('#customer_mobile').val('');
        } else {
            $('#by_email').css('display', 'block');
            $('#by_email_mobile').css('display', 'block');
            $('#customer_email').val(user_email);
            $('#customer_mobile').val(user_mobile);
        }
    });

    @if(isset($option['checked']) && $option['checked'])
        $("input[name='{{ $inputName }}'][value='{{ isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title'] }}']").trigger('click');
    @endif


    function part_get_quotes_method() {
        @if(config('app.settings.verify_mobile'))

            if ($('#customer_mobile').val()) {
                wizard_proceed = false;
                jQuery.ajax({
                    url: "/api/auth/is_mobile",
                    data: {_token: Laravel.csrfToken, mobile: $('#customer_mobile').val()},
                    dataType: "json",
                    type: "POST",
                    success: function (resp) {
                        if (resp.status == 'error') {
                            sweetAlert('Mobile number not valid', resp.message, 'warning');
                        } else {
                            wizard_proceed = true;
                            CheckEmail();
                            jQuery('#rootwizard').bootstrapWizard('next');
                        }
                    }
                });
            } else {
                wizard_proceed = true;
                CheckEmail();
                jQuery('#rootwizard').bootstrapWizard('next');
            }

        @else


            CheckEmail();
        @endif
    }

    function CheckEmail() {
        jQuery.ajax({
            url: "/api/auth/is_email_in_use",
            data: {_token: Laravel.csrfToken, email: $('#customer_email').val()},
            dataType: "json",
            type: "POST",
            success: function (resp) {
                if (resp.data.group) {
                    sweetAlert('Professional Account Found', 'We found a professional account matching that email address. You will need to create a new client account in order to continue posting this request.', 'warning');
                    $('#new_account').show();
                    $('#account_password').show();
                    $('#existing_account').hide();
                } else {
                    if (resp.data.is_used > 0) {
                        sweetAlert('Existing Account Found', resp.message, 'warning');
                        $('#new_account').hide();
                        if (resp.data.is_used == 1) {
                            $('#existing_account').show();
                            $('#account_password').show();
                            $('#username_in').val($('#customer_email').val());
                        } else {
                            $('.finish').hide();
                        }
                    } else {
                        $('#new_account').show();
                        $('#account_password').show();
                        $('#existing_account').hide();
                    }
                }
            }
        });
    }

</script>
@append