<div class="input-group">
    <div class="input-group-addon"><i class="fa fa-calendar "></i></div>
    <input name="{{ $special_arr[2] }}" class="form-control" type="text" value="" placeholder="Select Date" checkme id="datetimepicker">
</div>
@section('before-scripts-end')
<script>
    $('#datetimepicker').datetimepicker({
        format: 'dd-mm-yyyy hh:ii'
    });
</script>
@append