<div style="display: none" id="{{$special_arr[2]}}">
    @php($input_accept = 'image/*')
    @include('frontend.media.includes.partials.multi_uploader')
</div>
@section('before-scripts-end')
<script>
    $("input[name='{{ $inputName }}']").click(function () {
        $('#{{$special_arr[2]}}').css('display', ($(this).val() === '{{ $option['title'] }}') ? 'block':'none');
    });
    $("input[name='{{ $inputName }}'][value='{{ isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title'] }}']").trigger('click');
</script>
@append