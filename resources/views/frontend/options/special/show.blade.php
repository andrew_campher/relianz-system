<div class="modal-header" style="display: none" id="{{ $inputName }}_{{$special_arr[2]}}">
    @if(View::exists('frontend.options.special.'.$special_arr[1]))
        @include('frontend.options.special.'.$special_arr[1])
    @else
        {!! isset($special_arr[3]) && $special_arr[3] ? '<h4>'.$special_arr[3].'</h4>': '' !!}
        {{ Form::{$special_arr[1]}((isset($question['manual']) && $question['manual']?$special_arr[2]:'special['.$question['id'].']['.strtolower($special_arr[2]).']'), null, ['checkme','placeholder'=>(isset($special_arr[3])?$special_arr[3]:''), 'class' => 'form-control', 'rows' => 4]) }}
    @endif
</div>
@section('before-scripts-end')
<script>
    @if($question['question_type'] == 'select')
        $("select[name='{{ $inputName }}']").change(function () {
            $(this).each(function(index) {
                if ('{{ $inputName }}_'+$(this).val() == '{{ $inputName }}_{{ (isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title']) }}') {
                    $('#{{ $inputName }}_{{$special_arr[2]}}').show();
                } else {
                    $('#{{ $inputName }}_{{$special_arr[2]}}').hide();
                }
            });
        });

        @if(isset($option['checked']) && $option['checked'])
            $("select[name='{{ $inputName }}'][value='{{ isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title'] }}']").trigger('change');
        @endif

    @else
        $("input[name='{{ $inputName }}{{ $question['question_type'] == 'checkbox'?'[]':'' }}']").click(function () {
            $(this).each(function(index) {
                if ('{{ $inputName }}_'+$(this).val() == '{{ $inputName }}_{{ (isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title']) }}') {
                    if (this.checked) {
                        $('#{{ $inputName }}_{{$special_arr[2]}}').show();
                    } else {
                        $('#{{ $inputName }}_{{$special_arr[2]}}').hide();
                    }
                } else {
                    @if ($question['question_type']=='radio')
                        $('#{{ $inputName }}_{{$special_arr[2]}}').hide();
                    @endif
                }
            });
        });

        @if(isset($option['checked']) && $option['checked'])
            $("input[name='{{ $inputName }}{{ $question['question_type'] == 'checkbox'?'[]':'' }}'][value='{{ isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title'] }}']").trigger('click');
        @endif
    @endif


</script>
@append