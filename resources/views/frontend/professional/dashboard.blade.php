@extends('frontend.layouts.app')

@section ('title', 'Professional Dashboard')

@section('sidebar')

    <div class="sidebar_requests">
        <div class="dash_profile">
            <div class="pull-left chat-image">{!! $logged_in_user->professional->getLogoHtml() !!}</div>

            <div class="pull-left"><b>{{ link_to_route('public.professional.show', $logged_in_user->professional->title , [$logged_in_user->professional->id, $logged_in_user->professional->slug]) }}</b></div>

            <div class="badge pull-right">{{ link_to_route('frontend.professional.edit', 'Update', [$logged_in_user->professional->id], ['class' => '']) }}</div>

            <div class="clearfix"></div>
        </div>

        <div class="alert bg-info">
            <b><a href="{{ route('frontend.professional.quotes', [$logged_in_user->professional->id, 'status' => 'hired']) }}"><span class="badge label-success">{{ $logged_in_user->professional->in_progress_projects()->count() }}</span> Hired Jobs</a></b>
            <b><a href="{{ route('frontend.professional.quotes', $logged_in_user->professional->id) }}"><span class="badge label-warning">{{ $logged_in_user->professional->open_project_quotes()->count() }}</span> Pending Quotes</a></b>
        </div>


        <a class="visible-xs-block badge label-default" href="javascript:void(0)" onclick="$('#filterdiv').toggleClass('hidden-xs')">Show More</a>
        <div id="filterdiv" class="hidden-xs">
            <hr>

            <h5>Your Location</h5>

            <div class="dash-area"><i class="fa fa-map-marker"></i> {{ $logged_in_user->professional->area }}</div>

            <div class="dash-area"><i class="fa fa-car"></i> Travel: {{ $logged_in_user->professional->distance_travelled }}Km</div>


            <hr>

            <h5>Your Services</h5>
            @foreach( $services AS $service )
                <div class="badge label-default">{{ $service->title }}</div>
            @endforeach
            {{ link_to_route('frontend.professional.edit', 'Modify Services', [$logged_in_user->professional->id, '#services'], ['class' => '']) }}
        </div>

    </div>
@endsection

@section('content')

    <h1 class="no-top-margin">Quote Requests</h1>

    <div class="projects_wrap">

        @if($logged_in_user->professional->approved)

            @if(isset($projects) && $projects->count())

                <div class="project_wrap">
                    <div class="pull-left">{{ $projects->total() }} Projects found</div>
                    <div class="pull-right">Page {{ $projects->currentPage() }}</div>
                    <div class="clearfix"></div>
                </div>

                <div class="project_list list-unstyled">

                    @foreach($projects AS $project)

                        <div class="jumbotron">
                            <div class="jumbotron-contents">

                        <div id="project{{ $project->id }}" class="project_list_item">
                            <div class="project_list_head">
                                <h4 class="no-top-margin pull-left">{{ link_to_route('frontend.project.show', $project->title , [$project->id]) }}</h4>

                                <div class="pull-right">
                                    <span>
                                        Contact via: {!! $project->customer_mobile?'<i data-toggle="popover" title="Mobile Telephone" class="fa fa-lg fa-mobile"></i>':'' !!} {!! $project->customer_email?'<i data-toggle="popover" title="Email" class="fa fa-lg fa-envelope"></i>':'' !!}
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                            <div class="project_item_params no-top-margin">
                                <span class="badge">{{ $project->service ? $project->service->title: 'My'  }}</span>
                                <span class="inline"><i class="fa fa-clock-o"></i> {{ $project->created_at->diffForHumans() }} </span>
                                <span><i class="fa fa-calendar"></i> {!! $project->when_label  !!}</span>
                                <span><i class="fa fa-map-marker"></i> {{ strtok($project->area, ',') }}
                                    - {{ round($project->distance, 2) }} km away</span>
                            </div>



                            @if($project->description)
                                <div class="project_item_desc">{{ str_limit($project->description, 100) }}</div>
                            @endif


                            @if($project->quotes->contains('professional_id', $logged_in_user->professional->id))
                                <div title="Already quoted" class="pull-right"><i
                                            class="fa fa-check-circle-o fa-3x"></i></div>
                            @endif
                            <div class="clearfix"></div>
                        </div>

                        </div>
                    </div>
                    @endforeach
                </div>


                {{ $projects->render() }}
            @else

                <div class="alert bg-warning">
                    <h4><i class="fa fa-rss"></i> No projects found for your services and location</h4>
                    <p>There are no projects matching your services or within your travel distance. Don't worry we will notify you via email of any new projects.</p>
                    <p class="padding15"><b>You may ensure that you have selected all your services and ensure an accurate travel distance was selected.</b></p>
                </div>

            @endif

        @else

            <div class="alert alert-warning">
                <h3 class="no-margin"><i class="fa fa-warning"></i> Professional profile pending
                    approval</h3>
                <p>We are busy reviewing your Professional Profile and will notify you when
                    approved.</p>
            </div>
        @endif
    </div>

@endsection

@section('before-scripts-end')
    <script>
        Echo.channel('projects')
            .listen('.App.Events.Common.Project.ProjectClosed', function(e){
                console.log(e.project.title);
                $('#project'+e.project.id).fadeOut('fast');
            });
    </script>
@append