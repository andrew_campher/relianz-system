@extends ('frontend.layouts.app')

@section ('title', 'Edit Professional')

@section('content')

    @if( isset($professional) )
        <h1>
            Edit Professional
        </h1>
        {{ Form::model($professional, ['route' => ['frontend.professional.update', $professional], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id'=>'professionalForm', 'data-toggle'=>'validator', 'data-disable'=>'false']) }}
    @else
        <h1>
            Add Professional
        </h1>
        {{ Form::open(['route' => 'frontend.professional.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'data-toggle'=>'validator', 'id'=>'professionalForm']) }}
    @endif

    @if(Request::input('confirm'))
        <div class="alert alert-warning">
            <div class="h3 no-top-margin">Please confirm your business details</div>
        </div>
    @endif

    <div id="prowizard">

        <div style="display: none" class="">
            <div class="navbar-inner" >
                <div class="container">
                    <ul>
                        <li class="active"><a href="#info" data-toggle="tab">Business Info</a></li>
                        <li role="presentation"><a href="#branch" aria-controls="messages" role="tab" data-toggle="tab">Branches</a></li>
                        <li><a href="#profile" data-toggle="tab">Profile Page</a></li>
                        <li><a href="#system" data-toggle="tab">Settings</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="jumbotron">
            <div class="jumbotron-contents">

                <div class="tab-content">
                    @include('frontend.professional.includes.form_fields')
                </div>

            </div><!-- /.box-body -->
        </div><!--box-->


    {{ Form::hidden('user_id', isset($user) && $user?$user->id:$logged_in_user->id) }}

    {{ Form::hidden('confirmation_code', isset($confirmation_code) && $confirmation_code?$confirmation_code:null) }}

    {{ Form::close() }}

    <ul class="pager wizard">
        <li class="previous btn-lg"><a href="javascript:;">Back</a></li>
        <li id="nextbtn" class="next btn-lg"><a href="javascript:;">Next</a></li>
        <li class="finish btn-lg"><a href="javascript:;">Finish</a></li>
    </ul>

</div>


@stop

@section('after-scripts-end')
    @include('frontend.professional.includes.form_js')
@append
