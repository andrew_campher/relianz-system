<h4>Branch</h4>

<div class="form-group">
    {{ Form::label('branch[branch_title]', 'Branch Specific Name (OPTIONAL)', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-lg-10">
        {{ Form::text('branch[branch_title]', null, ['class' => 'form-control', 'placeholder' => 'Defaults to title']) }}
        <div class="help-block with-errors"></div>
    </div><!--col-lg-10-->
</div><!--form control-->

<hr>

<h4>Contacts / Notifications</h4>

<div class="">

    <div class="form-group">
        {{ Form::label('branch[telephone]', 'Branch Contact Number', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('branch[telephone]', null, ['class' => 'form-control mobile_input', 'data-minlength'=>'10', 'placeholder' => 'Telephone eg. 0219451234', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));', 'required']) }}
            <div class="help-block with-errors"></div>
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="alert alert-info"><i class="fa fa-info-circle"></i> Notifications from {{ config('app.name') }} will be sent to these details.</div>

    <div class="form-group">
        {{ Form::label('branch[notify_email]', 'Branch Email Notifications', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::email('branch[notify_email]', null, ['class' => 'form-control', 'placeholder' => '', 'required']) }}
            <div class="help-block with-errors"></div>
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('branch[notify_mobile]', 'Branch Mobile', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('branch[notify_mobile]', null, ['class' => 'form-control mobile_input', 'placeholder' => 'eg. 0821234567', 'data-minlength'=>'10', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
</div>



<hr>

<h4>Address & Travel</h4>

<div class="form-group">
    {{ Form::label('optArea', 'Branch Location', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-lg-10">

        {{ Form::text('branch[area]', null, ['id' => 'optArea', 'checkme', 'class' => 'form-control input-clean input-lg', 'placeholder' => 'Type area here...', 'required']) }}

        {{ Form::hidden('branch[lat]', null, ['id' => 'lat']) }}
        {{ Form::hidden('branch[lng]', null, ['id' => 'lng']) }}

        @php($gmap_marker['draggable'] = 'true')
        @include('frontend.wizard.custom.geocode', ['address' => isset($professional) && $professional->area, 'field_array'=> 'branch', 'settings' => ['types' => '', 'exclude_fields' => 1]])
    </div><!--col-lg-10-->
</div><!--form control-->

<div class="form-group">
    {{ Form::label('branch[distance_travelled]', 'Willing to travel', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-lg-10">
        {{ Form::select('branch[distance_travelled]', ['0'=>'None','1'=>'1 km','5'=>'5 km','10'=>'10 km','20'=>'20 km','30'=>'30 km','50'=>'50 km','100'=>'100 km','200'=>'200 km'], null, ['class' => 'form-control', 'placeholder' => 'Select a distance', 'required']) }}
        <div class="help-block with-errors"></div>
    </div><!--col-lg-10-->
</div><!--form control-->
