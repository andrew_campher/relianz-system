
<!-- Tab panes -->

    <div role="tabpanel" class="tab-pane" id="branch">

        <div id="Branch">

            @if(isset($professional) && $professional->branches && count($professional->branches) > 1)

                <table class="table">
                    <tr>
                        <th>Address</th>
                        <th>Title</th>
                        <th></th>
                    </tr>
                    @foreach($professional->branches AS $branch)
                    <tr>

                        <td>{{ $branch->area }}</td>
                        <td>{{ $branch->title }}</td>
                        <td>
                            <a onclick="fireModal('{{ route('frontend.professional.edit_branch', [$branch->id, $professional->id]) }}')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                            <a href="{{ route('frontend.professional.remove_branch', [$branch->id, $professional->id]) }}"><i class="fa fa-pencil"></i> Remove</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            @else

                @include('frontend.professional.includes.branch_fields')

            @endif

        </div>



    </div>


