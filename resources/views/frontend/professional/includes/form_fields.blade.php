
<!-- Tab panes -->

    <div role="tabpanel" class="tab-pane active" id="info">

        <h4>Business Info</h4>

        <div class="form-group">
            {{ Form::label('title', 'Business Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title of the business', 'required', 'autocomplete'=>'off']) }}
                <div class="help-block with-errors"></div>
                <div style="display: none" id="company_search" class="suggestresults">
                    <div class="h6 badge label-warning">Claim existing listing:</div>
                    <a onclick="$('#company_search').hide()" href="javascript:void(0)"><i class="fa fa-close fa-lg"></i> close</a>
                </div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('company_name', 'Company Name (If different to title)', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('company_name', null, ['class' => 'form-control', 'placeholder' => 'The registered company name']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('image', 'Logo', ['class' => 'col-lg-2 control-label', 'accept'=>'image/*']) }}

            <div class="col-lg-10">
                @if(isset($professional))
                    {!! $professional->getLogoHtml('md') !!}
                @endif
                {{ Form::file('logo') }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <hr>

        <div class="form-group">
            {{ Form::label('company_type', 'Business Type', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::select('company_type', ['Sole' => 'Sole Trader','CC' => 'Close Corporation','Partnership', 'Private Company' => 'Private Company (PTY LTD)', 'Public Company' => 'Public Company (LTD)', 'Inc' => 'Personal Liability Company', 'Business trust', 'External company', 'Non-Profit'], null, ['class' => 'form-control','id'=> 'company_type', 'placeholder' => 'Select Company Type', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('vat_registered', 'VAT Number', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('vat_registered', null, ['class' => 'form-control']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="company_entity_div" style="{{ isset($professional) && $professional->company_type == 'Sole'?'display: none':'' }}">
            <div class="form-group">
                {{ Form::label('company_registration', 'Company Reg No?', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::text('company_registration', null, ['class' => 'form-control company_entity', 'placeholder' => '']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>

        <div class="soletrader_div" style="{{ isset($professional) && $professional->company_type == 'Sole'?'':'display: none' }}">
            <div class="form-group">
                {{ Form::label('id_number', 'ID number or Work Permit', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::text('id_number', null, ['class' => 'form-control soletrader', 'placeholder' => '' ]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>

        <hr>

        <div class="form-group">
            {{ Form::label('services', 'Your Services', ['class' => 'col-lg-2 control-label', 'id' => 'services']) }}

            <div class="col-lg-10">
                @include('backend.service.includes.partials.service-list', ['services' => isset($professional) ?$professional->services:null])
            </div><!--col-lg-10-->
        </div><!--form control-->

        <hr>

        <h4>Business Contacts</h4>

        <div class="form-group">
            {{ Form::label('company_email', 'Email', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('company_email', null, ['class' => 'form-control', 'placeholder' => '', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('telephone', 'Contact Number', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::number('telephone', null, ['class' => 'form-control mobile_input', 'data-minlength'=>'10', 'placeholder' => 'Telephone eg. 0219451234', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('website_url', 'Website', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('website_url', null, ['class' => 'form-control', 'placeholder' => '']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

    </div>


    @include('frontend.professional.includes.branches')


    <div role="tabpanel" class="tab-pane" id="profile">
        <h4>Profile</h4>

        <div class="form-group">
            {{ Form::label('description', 'About Us', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::textarea('description', null, ['class' => 'form-control wysiwyg', 'placeholder' => 'Description', 'required']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->


            <hr>

            <div class="form-group">
                {{ Form::label('galleries', 'Photo Gallery', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">

                    @if(isset($professional))
                        @include('frontend.media.includes.partials.gallery', ['media' => $professional->media, 'edit' => 1])

                    @endif

                        @include('frontend.media.includes.partials.multi_uploader', ['input_accept'=>'image/*', 'file_mimes' => ['image/jpeg', 'image/png', 'image/gif', 'image/jpg']])

                </div><!--col-lg-10-->
            </div>

            <hr>

            <div class="form-group">
                {{ Form::label('hero_image', 'Header Image', ['class' => 'col-lg-2 control-label', 'accept'=>'image/*']) }}

                <div class="col-lg-10">
                    <p>Upload a large image to be displayed above your profile. Minimum photo width: {{ config('app.images.max_width') }}px</p>

                    @if( isset($professional) && $professional->hero_image_url )
                        <img style="width: 100%" src="{!! $professional->hero_image_url !!}" />
                    @endif
                    {{ Form::file('hero_image') }}
                </div><!--col-lg-10-->
            </div><!--form control-->


    </div>
    <div role="tabpanel" class="tab-pane" id="system">

        <h4>Credentials</h4>
        <div class="form-group">

            <div class="alert alert-info">
                Upload copies of certificates or qualifications that you might have in order to validate your expertise.
                <br/>Eg. Police clearance certificate, Electrical certifications etc.
                <br><b>{{ config('app.name') }} will verify and display the corresponding logo next to your business.</b>
            </div>

            {{ Form::label('resources', 'Resources', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">

                @if(isset($professional))

                    @include('frontend.media.includes.partials.gallery', ['media' => $professional->media_resources, 'edit' => 1, 'group'=> 'resources'])

                @endif

                <div class="alert alert-warning"><b>OFFICE USE ONLY:</b> Resource documents not accessible to public or clients.</div>

                @include('frontend.media.includes.partials.multi_uploader', ['group'=> 'resources', 'caption_otions' => ['Certification' => 'Certification', 'Qualification' => 'Qualification', 'License' => 'License', 'PoD' => 'Proof of Address', 'Other' => 'Other']])


            </div><!--col-lg-10-->
        </div>

    </div>


