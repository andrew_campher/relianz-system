<script>
    var editor = $('.wysiwyg').wysihtml5({
        stylesheets: ["{{ asset('css/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.css') }}"],
        toolbar: {
            "font-styles": false,"emphasis": true,"lists": true,"html": false,"link": true,"image": true,"color": false,"blockquote": true, //Blockquote
        },
        events: {
            change: function () {
                $('.wysiwyg').html(editor[0].value);
                $('#professionalForm').validator('update');
            }
        }
    });

    $(".mobile_input").on('blur',function() {
        $(this).val($(this).val().replace(/\s/g, ""));
        if ($(this).val().length == 10) {
            isMobile($(this).val());
        }
    });

    $('#website_url').on('focus', function () {
        if ($(this).val() == '') {
            $(this).val('http://');
        }
    })

    $('#website_url').on('blur', function () {
        if ($(this).val() == 'http://') {
            $(this).val('');
        }
    })

    $('#company_type').on('change', function() {
        if ($(this).val() == 'Sole') {
            $('.soletrader').attr('required','required');
            $('.company_entity').removeAttr('required','required');
            $('.company_entity_div').hide();
            $('.soletrader_div').show();
        } else {
            $('.soletrader').removeAttr('required','required');
            $('.company_entity').attr('required','required');
            $('.company_entity_div').show();
            $('.soletrader_div').hide();
        }
    });

    function isMobile(mobile) {
        jQuery.ajax({
            url: "/api/auth/is_mobile",
            data: {_token: Laravel.csrfToken, mobile: mobile},
            dataType: "json",
            type: "POST",
            success: function (resp) {
                if (resp.status == 'error') {
                    sweetAlert('Mobile number not valid', 'Ensure that the mobile is in this format: 0821234567 (10 digits) and that it is a valid mobile.', 'warning');
                } else if (resp.status == 'success') {
                    return true;
                }
            }
        });
    }

    var back = true;

    $('#prowizard').bootstrapWizard({withVisible:false, onNext: function(tab, navigation, index) {
        if(checkinputs() == 1) {
            return false;
        }

        if (index == 1) {
            $('#greetings').fadeOut();
        }

        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#prowizard').find('.bar').css({width:$percent+'%'});
        $('#prowizard .progress-bar').html(Math.floor($percent)+ '% Complete');
    },
        onFinish: function() {

            @if( isset($professional) && $professional->user && $professional->status != 2 )
                processForm();
            @else
                jQuery.ajax({
                    url: "/api/auth/is_email_in_use",
                    data: {_token: Laravel.csrfToken, email: $('#email').val()},
                    dataType: "json",
                    type: "POST",
                    success: function (resp) {
                        if (resp.data.is_used > 0) {
                            $("label[for='email']").addClass('bg-danger');
                            sweetAlert('User Account Found', 'That email is already in use by another account.', 'warning');
                        } else {
                            $("label[for='email']").removeClass('bg-danger');

                            processForm();
                        }
                    }
                });
            @endif
        }
    });

    function processForm() {

        if(!checkinputs()) {
            back = false;

            $('#professionalForm').submit();

            $('.finish a').hide();
            $('.finish').append('<i class="fa fa-gear fa-spin fa-fw"></i>');
        }

    }

    window.onbeforeunload = function() {
        if(back == true)
            return "Your project details will not be saved, are you sure?";
    };

    function checkinputs() {
        var unfilled = 0;
        $('#prowizard .tab-pane:visible input,#prowizard .tab-pane:visible textarea,#prowizard .tab-pane:visible select').filter('[required]').each(function(){
            if (($(this).is(':radio') || $(this).is(':checkbox')) && !$('input[name="'+$(this).attr('name')+'"]:checked').val()) {
                sweetAlert('Please complete all required fields');
                unfilled = 1;
            } else if(!$(this).val()) {
                sweetAlert('Please complete all required fields');
                unfilled = 1;
            } else if ($(this).is('#customer_email') && !isValidEmailAddress( $(this).val() )) {
                sweetAlert('Enter valid email');
                unfilled = 1;
            }

            if (unfilled == 1) {
                $(this).trigger('input');
            }
        });
        return unfilled;
    }

</script>