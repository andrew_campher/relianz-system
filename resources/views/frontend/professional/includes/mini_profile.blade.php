<div class="mini-profile">
    <div class="quote-logo pull-left">
        {!! $professional->getLogoHtml('md') !!}
    </div>
    <h3 class="brandname no-margin">
        <a href="{{ route('public.professional.show', [$professional->id, $professional->slug]) }}">{{ $professional->title }} </a> {!! $professional->getVerify('big') !!}
    </h3>

        <div>
            {!!  $professional->recommended_label !!}
        </div>
        <div>
            {!!  $professional->overall_rating_stars !!} <b>{{ $professional->rating_overall_tot }}</b> from {{ isset($professional->stats->reviews_count)?$professional->stats->reviews_count:0  }} Reviews
        </div>

    <div class="padding15"><i class="fa fa-map-marker"></i> {{  $professional->area }}</div>

    <div class="clearfix"></div>

</div>