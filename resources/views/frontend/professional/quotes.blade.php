@extends('frontend.layouts.app')

@section ('title', 'Quotes')

@section('content')

    <h1 class="">All Quotes</h1>

    <div class="jumbotron">

        <ul id="myTab1" class="nav nav-tabs nav-justified">
            <li class="{{ $status == 'pending' ? 'active' : '' }}">{{ link_to_route('frontend.professional.quotes', 'Pending', $professional->id) }}</li>
            <li class="{{ $status == 'hired' ? 'active' : '' }}">{{ link_to_route('frontend.professional.quotes', 'Hired', [$professional->id, 'status'=> 'hired']) }}</li>
            <li class="{{ $status == 'unsuccessful' ? 'active' : '' }}">{{ link_to_route('frontend.professional.quotes', 'Unsuccessful', [$professional->id, 'status'=> 'unsuccessful']) }}</li>
            <li class="{{ $status == 'draft' ? 'active' : '' }}">{{ link_to_route('frontend.professional.quotes', 'Drafts', [$professional->id, 'status'=> 'draft']) }}</li>
        </ul>

        <div class="jumbotron-contents">

            <div class="">

                <div class="tab-content">

                    <div class="alert bg-info">
                        <i class="fa fa-info-circle"></i> {{ trans('strings.frontend.quotes.status.professional.'.$status.'_description') }}
                    </div>

                    <div class="">
                        @if(!$quotes->isEmpty())
                            <table class="table">
                                <tr>
                                    <th>Client</th>
                                    <th>Project</th>
                                    <th>Estimate Cost</th>
                                    <th>Sent</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                @foreach($quotes AS $quote)
                                    <tr >

                                        <td width="150px">
                                            {!! $quote->project->user->getImageHtml('icon', 'profile-image user-profile-icon img-circle') !!} <a onclick="getPage({{ $quote->id }})" href="javascript:void(0)">{{ $quote->project->user->short_name }}</a>
                                        </td>
                                        <td>
                                            {{ link_to_route('frontend.project.show', $quote->project->title.' - '. ($quote->service ? $quote->service->pro_title : '').' Needed', $quote->project_id, ['class' => '']) }}
                                            @if ($status == 'hired' && $quote->review)
                                                <div class="bg-gray jumbotron-contents reviews_block">
                                                    <div class="h5 no-top-margin">Review</div>
                                                    @include('frontend.project_review.include.review', ['review' => $quote->review])

                                                    @if ($quote->client_review)
                                                        <blockquote>
                                                            <div class="h5 ">Your review of client</div>
                                                            @include('frontend.client_review.include.review', ['review' => $quote->client_review])
                                                        </blockquote>
                                                    @else
                                                        @if($quote->review && $quote->user_id == access()->id())
                                                            <div class="heading_btn">
                                                                <a class="btn btn-info" href="{{ route('frontend.quote.client_review.create', $quote->id) }}"><i class="fa fa-pencil-square-o"></i> Review Client</a>
                                                            </div>
                                                        @endif
                                                    @endif
                                                </div>
                                            @endif
                                        </td>


                                        <td>{!! $quote->price_label !!}</td>

                                        <td>{!! $quote->created_at->format(config('app.date.long_min')) !!}</td>

                                        <td>{!! $quote->status_label !!}</td>

                                        <td>
                                            {!! $quote->action_buttons !!}
                                        </td>

                                    </tr>
                                @endforeach
                            </table>

                            {{ $quotes->render() }}
                            @else
                            <div class="alert alert-warning"><b>No {{ $status }} quotes found</b></div>
                        @endif
                    </div>
                </div>
            </div>




        </div>
    </div>

@endsection