@extends('frontend.layouts.app')

@section ('title', 'All Projects')

@section('content')

    <h1>All Projects</h1>

    <div class="jumbotron">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified">
            <li class="{{ $status == 'active' ? 'active' : '' }}" ><a href="{{ route('frontend.project.all') }}">In Progress</a></li>
            <li class="{{ $status == 'completed' ? 'active' : '' }}"><a href="{{ route('frontend.project.all', 'completed') }}">Completed</a></li>
            <li class="{{ $status == 'expired' ? 'active' : '' }}"><a href="{{ route('frontend.project.all', 'expired') }}">Expired</a></li>
            <li class="{{ $status == 'closed' ? 'active' : '' }}" ><a href="{{ route('frontend.project.all', 'closed') }}">Closed</a></li>
        </ul>

        <div class="jumbotron-contents">

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active">
                    <table class="table table-striped">
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Created</th>
                        </tr>
                        @if (isset($projects) && $projects->count())

                            @foreach($projects AS $project)
                                <tr>
                                    <td>{{ $project->id }}</td>
                                    <td><a href="{{ route('frontend.project.manage', $project->id) }}">{{ $project->title }}</a></td>
                                    <td>{!! $project->StatusLabel !!}</td>
                                    <td>{{ $project->created_at }}</td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td colspan="50">No projects found</td>
                            </tr>
                        @endif
                    </table>

                    {{ $projects->render() }}
                </div>
            </div>

        </div>
    </div>

@endsection