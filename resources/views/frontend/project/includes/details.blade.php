

<h2 class="no-top-margin" id="project_heading">{{ $project->title }}</h2>
<div class="h4">{{ $project->service ? $project->service->title: 'My'  }} Project</div>
<div class="h5">
    <div class="pull-right"><small>Status: </small>{!!  $project->StatusLabel !!}
        @if($project->user_id == access()->id() && $project->status < 3)
        <a data-toggle="firemodal" href="{{ route('frontend.project.status_change', $project->id) }}">Close</a>
        @endif
    </div>
</div>

<div class="user-mini-profile">
    <div class="profile_side_map"><img src="https://maps.googleapis.com/maps/api/staticmap?size=550x200&zoom=14&maptype=roadmap\&markers=size:mid%7Ccolor:red%7C{{ $project->lat }},{{ $project->lng }}&key={{ config('app.google.api_maps_key') }}" /></div>

    <div class="profile_side_img">{!! $project->user->getImageHtml('thumb', 'avatar-med img-circle') !!}</div>
    <div class="profile_side_det">
        <div class="profile_side_name">
            {!! $project->user->shortname !!} {!! $project->user->online_label !!}
        </div>

        <div class="profile_side_area">{{ $project->area }}</div>
    </div>
</div>

@if (access()->isRole('Professional') && $project->status < 3)
<div class="row project_stats">
    <div class="col-xs-4">
        <i class="fa fa-tag"></i> {{ $project->quote_count }} / {{ config('app.settings.quote_limit') }}
        <div class="project_stats_txt">quotes sent</div>
    </div>
    <div class="col-xs-4">
        <i class="fa fa-clock-o"></i> {{ $project->approved_at->addDays(config('app.settings.request_days_expire'))->diffForHumans(null, true) }}
        <div class="project_stats_txt">until request expires</div>
    </div>
    <div class="col-xs-4">
        @if (config('app.settings.enable_credits'))
        <div title="{{ trans('tooltips.account.credits.credit_fee_cost') }}" data-toggle="popover" data-trigger="hover" class="credit_cost_lg">{!! config('app.pricing.credit_icon') !!} {!!  $project->service->getFee() !!}</div>
        <div class="project_stats_txt">credits</div>
        @endif
    </div>
</div>
@endif

    <div class="answer_item">
        <table class="table table-striped table-hover">
            <tr>
                <th style="border-top: none">Status</th>
                <td style="border-top: none">{!!  $project->StatusLabel !!}</td>
            </tr>
            <tr>
                <th>When</th>
                <td>{!! $project->whenlabel !!} {{ $project->when_describe }}</td>
            </tr>

            @if($project->description)
                <tr>
                    <th colspan="2">Description</th>
                </tr>
                <tr>
                    <td colspan="2">{{ $project->description }}</td>
                </tr>
            @endif
        </table>

        @include('frontend.project.includes.partials.answers')

    </div>

<div class="clearfix">

    @include('frontend.media.includes.partials.gallery', ['media' => $project->media])

</div>