@if(isset($answers) && $answers)
    @foreach($answers AS $answer)
        @if(is_array($answer))
            @foreach($answer AS $key => $ans)
                @if($ans->question)
                <div class="answer_wrap">

                    @if($key == 0)
                        <hr>
                        <div class="h6"><strong>{{$ans->question->title}}</strong>
                        </div>
                        <p><i class="fa fa-circle"></i> {{$ans->answer_value}}</p>
                    @else
                        <p><i class="fa fa-circle"></i> {{$ans->answer_value}}</p>
                    @endif
                </div>

                @endif
            @endforeach
        @else
            @if($ans->question)
            <hr>

            <div class="answer_wrap">
                <div class="h6">{{$answer->question->title}}</div>
                <p><i class="fa fa-d"></i> {{$answer->answer_value}}</p>
            </div>

            @endif
        @endif

    @endforeach
@endif