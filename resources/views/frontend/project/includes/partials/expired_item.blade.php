<div class="jumbotron">
    <div class="jumbotron-contents">
        <div class="h5 no-top-margin">{{ link_to_route('frontend.project.manage', $project->title, [$project->id], ['class' => '']) }} <a class="text-danger" data-toggle="firemodal" href="{{ route('frontend.project.status_change', $project->id) }}"><i class="fa fa-times"></i> Close</a></div>

        @if(isset($unopened_quotes[$project->id]))
            <div data-toggle="popover" data-content="{{ count($unopened_quotes[$project->id]) }} new quote received." class="heading_btn"><div class="badge label-danger">{{ count($unopened_quotes[$project->id]) }} PENDING</div></div>
        @endif

        <div class="clearfix">
            <div class="pull-left" >
                <span data-toggle="popover" title="{{ $project->created_at }}"><i class="fa fa-clock-o"></i> {!!  $project->created_at->diffForHumans()  !!}</span>

                <span>| {!!  $project->StatusLabel !!} </span>
            </div>

            <div class="pull-right">
                {{ $project->quote_count }}/{{ config('app.settings.quote_limit') }} Quote{{ $project->quote_count==1 ? '' : 's' }}
            </div>
        </div>

    </div>
</div>