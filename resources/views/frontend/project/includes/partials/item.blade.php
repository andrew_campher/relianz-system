<div class="jumbotron">
    <div class="jumbotron-contents project_item">
        <h2 class="no-top-margin">{{ link_to_route('frontend.project.manage', $project->title, [$project->id], ['class' => '']) }}</h2>

        @if(isset($unopened_quotes[$project->id]))
            <div data-toggle="popover" data-content="{{ count($unopened_quotes[$project->id]) }} new quote received." class="heading_btn"><div class="badge label-danger">{{ count($unopened_quotes[$project->id]) }} NEW</div></div>
        @endif

        <div class="clearfix">
            <div class="pull-left" >
                <span data-toggle="popover" title="{{ $project->created_at }}"><i class="fa fa-clock-o"></i> {!!  $project->created_at->diffForHumans()  !!}</span>

                <span>| {!!  $project->StatusLabel !!} <a data-toggle="firemodal" href="{{ route('frontend.project.status_change', $project->id) }}">Close</a></span>
            </div>

            <div class="pull-right text-right">
                {{ $project->quote_count }}/{{ config('app.settings.quote_limit') }} Quote{{ $project->quote_count==1 ? '' : 's' }}
            </div>
        </div>

    </div>
</div>