@extends('frontend.layouts.app')

@section ('title', 'Manage Project Quotes')

@section('sidebar-menu')
    <div class="sidebar-menu">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#project_heading">Project Details</a></li>
            <li><a href="#quote_heading">Quotes Received <span class="badge badge-danger">{{ $project->quote_count }}</span></a></li>
        </ul>
    </div>
@endsection

@section('sidebar')

    @include('frontend.project.includes.details')
@endsection

@section('content')

    <?php
        switch ($project->status) {
            #Pending
            case "0":
                echo '<div class="alert bg-warning"><h4><i class="fa fa-hourglass-half fa-pulse fa-fw"></i> Project Processing</h4>'.trans('strings.frontend.projects.status.'.access()->group().'.pending_description').'</div>';
                break;
            #Completed
            case "3":
                echo '<div class="alert bg-success"><h4><i class="fa fa-check-square-o"></i> Project Completed</h4>'.trans('strings.frontend.projects.status.'.access()->group().'.completed_description').'</div>';
            break;
            #Closed
            case "4":
                echo '<div class="alert bg-warning"><h4><i class="fa fa-times"></i> Project Closed</h4>'.trans('strings.frontend.projects.status.'.access()->group().'.closed_description').'</div>';
            break;
            #Expired
            case "5":
                echo '<div class="alert bg-danger"><h4><i class="fa fa-history"></i> Project Expired</h4>'.trans('strings.frontend.projects.status.'.access()->group().'.expired_description').'</div>';
            break;


        }
    ?>

    <h3 id="quote_heading" class="no-top-margin">Quotes Received <div data-toggle='popover' data-trigger='hover' data-content='We send you a maximum of {{ config('app.settings.quote_limit') }} quotes.' class="pull-right">{{ $project->quote_count }} / {{ config('app.settings.quote_limit') }} <i class="fa text-primary fa-question-circle"></i></div></h3>


    @if ($project->unopened_quotes->count())
        <div class="row">
            <div class="col-xs-12">
                <div class="h6 no-top-margin"><i class="fa fa-star"></i> New Quotes</div>

                @foreach($project->unopened_quotes AS $quote)
                    <div class="col-xs-3 jumbotron">
                        <div class="jumbotron-contents bg-danger">
                            <div class="chat-image pull-left">
                                {!! $quote->professional->getLogoHtml('sm')  !!}
                            </div>
                            <b>{{ link_to_route('frontend.quote.show', $quote->professional->title, [$quote->id]) }}</b>

                            <p>{!! $quote->price_label !!}</p>

                            <div class="clearfix"></div>

                            <div class="new_quote_price"></div>

                            {{ link_to_route('frontend.quote.show', 'Open Quote', $quote->id, ['class' => 'btn btn-success btn-sm']) }}
                        </div>
                    </div>
                @endforeach
            </div>


        </div>
    @endif


    <div class="row">
        <div class="col-xs-12">
            <div class="h6">Opened Quotes</div>
        </div>

        <div class="col-xs-12">

            <div class="white_bg">
                @if($project->opened_quotes->count())
                    <table class="table">
                        <tr>
                            <th>Professional</th>
                            <th>Rating</th>
                            <th>Estimate</th>
                            <th>{{ $project->user_id == $logged_in_user->id? 'Received' : 'Sent' }}</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        @foreach($project->opened_quotes AS $quote)

                            @if($quote->status == 3)
                                <tr>
                                    <td class="alert-success" colspan="50">
                                        <div class="h5"><i class="fa fa-handshake-o"></i> Professional Hired</div>
                                    </td>
                                </tr>
                            @endif
                            <tr {!! $quote->status == 3?'class="hired_row"':'' !!}>

                                <td>
                                    <div class="chat-image pull-left">
                                        {!!  $quote->professional->getLogoHtml() !!}
                                    </div>

                                    {{ link_to_route('frontend.quote.show', $quote->professional->title, [$quote->id]) }} {!! $quote->professional->verify_label  !!}
                                </td>
                                <td>{!! $quote->professional->overall_rating_stars !!}<br/><b>{{ $quote->professional->rating_overall_tot }}</b> from {{ $quote->professional->stats ? $quote->professional->stats->reviews_count : 0 }} Reviews</td>


                                <td>{!! $quote->price_label !!}</td>

                                <td>{{ $quote->created_at->format(config('app.date.long_min')) }}
                                @if($quote->read_at)
                                    <br><small>Opened: {{ $quote->read_at->format(config('app.date.long_min')) }}</small>
                                @endif
                                </td>

                                <td>{!! $quote->status_label !!}</td>

                                <td align="right">
                                    {{ link_to_route('frontend.quote.show', 'View', $quote->id, ['class' => 'btn btn-primary btn-sm']) }}

                                    @if($quote->status == 7)
                                        <a id="qt-btn-{{ $quote->id }}" title="" href="javascript:void(0)" onclick="MarkQuoteHired({{ $quote->id }}, this)" class="btn btn-success btn-sm"><i class="fa fa-thumbs-up"></i> Confirm Hire</a>
                                    @endif
                                    @if($quote->status == 3)
                                        <a id="qt-btn-{{ $quote->id }}" href="{{ route('frontend.quote.project_review.create', $quote->id) }}" class="btn btn-info btn-sm"><i class="fa fa-pencil-square"></i> Review Work</a>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                    </table>
                @elseif ($project->unopened_quotes->count())
                    <div class="alert alert-default">
                        <h4>{{ $project->unopened_quotes->count() }} Unopened quote</h4>
                        <p>Click to open the new quote recieved above.</p>
                    </div>
                @else
                    <div class="alert alert-default">
                        <h4>Nothing yet...</h4>
                        <p>Professionals have not yet submitted any quotes.</p>
                    </div>
                @endif
            </div>

        </div>
    </div>


@endsection

@section('after-scripts-end')
    <script>
        function getPage(id) {
            $('#quote_details').html('<i class="fa fa-align-center fa-spinner fa-spin fa-2x"></i>');
            jQuery.ajax({
                url: "/quote/"+id,
                data: {"ajax":1},
                type: "GET",
                success:function(data){$('#quote_details').html(data);}
            });
        }
    </script>
@endsection

