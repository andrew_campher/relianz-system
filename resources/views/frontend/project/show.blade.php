@extends('frontend.layouts.app')

@section ('title', 'Project '.$project->title)

@section('sidebar-menu')
    <div class="sidebar-menu">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#quote_heading"><i class="fa fa-info-circle"></i> Project</a></li>
            <li class=""><a href="#quote_heading"><i class="fa fa-pencil-square"></i> Quote</a></li>
            @if ($project->status == 3)
                <li class=""><a href="#reviews_heading"><i class="fa fa-star"></i> Reviews</a></li>
            @endif
            @if (isset($existing_quote) && $existing_quote && $existing_quote->count())
                <li class=""><a href="#messages_heading"><i class="fa fa-comment"></i> Messages</a></li>
            @endif
        </ul>
    </div>
@endsection

@section('sidebar')

    @include('frontend.project.includes.details')
@endsection

@section('content')

    <div class="quotes-inside">
        @if(!access()->id())
            <h1 class="no-top-margin">New Quote</h1>

        <div class="h4">Professional</div>

            <div class="jumbotron">
                <div class="jumbotron-contents">

                @include('frontend.professional.includes.mini_profile')

                </div>
            </div>

            <div class="h4">Quote Details</div>

            @if (isset($existing_quote) && $existing_quote && $existing_quote->count() && !Request::input('quote_id'))
                <div class="alert">
                    <h1>Existing quote found</h1>
                    <p>It appears that you have already submitted a quote under this company.</p>
                </div>

            @elseif($professional->status == 1)
                <div class="alert alert-warning">
                    <h2 class="no-top-margin">Login Required</h2>
                    <p>Please login with the user account associated with this professional</p>
                    <a class="btn btn-success" onclick="fireModal('{{ route('frontend.auth.login') }}')" href="javascript:void(0)">Click to Login</a>
                </div>
            @endif



        @else
            @if(access()->user()->isRole('Professional'))
                @if(!access()->user()->professional->approved)
                    <div class="alert alert-warning"><h2>Professional Account Pending Approval</h2></div>
                @else
                    @if (isset($existing_quote) && $existing_quote && $existing_quote->count() && !Request::input('quote_id'))
                        @include('frontend.quote.includes.details', ['quote' => $existing_quote, 'model'=>'quote'])

                    @else
                        @if($project->status == 1)
                            @include('frontend.quote.includes.sendquote')
                        @else
                            <div class="alert alert-warning">
                                <h3 class="no-top-margin">Project no longer open to quote or to make modifications to existing quote.</h3>
                            </div>
                        @endif
                    @endif
                @endif
            @endif
        @endif
    </div>

@endsection
