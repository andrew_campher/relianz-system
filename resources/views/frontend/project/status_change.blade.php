@extends ('frontend.layouts.app')

@section ('title', 'Status Change - '.$project->title)

@section('content')


    <div class="jumbotron">
        <div class="jumbotron-contents">

    @if($project->status < 3 || $project->status == 5)

        @if($project->status == 2)
            <div class="padding15">
                <h2 class="no-top-margin">You have marked a professional as hired on this project.</h2>
            </div>

            <div class="h4">Would you like to leave a review of their work?</div>

            <a class="btn btn-success" href="{{ route('frontend.quote.project_review.create', $project->hired_quote->id) }}"><i class="fa fa-pencil-square-o"></i> Click here to Review</a>

        @else
            <div class="padding15">
                <h2 class="no-top-margin">Have you hired a professional on {{ config('app.name') }} for your {{ $project->service ? $project->service->title: 'My'  }} project?</h2>
            </div>
        @endif




            {{ Form::open(['route' => ['frontend.project.update_status', $project->id], 'class' => 'form-horizontal', 'novalidate', 'data-toggle' => 'validator', 'role' => 'form', 'method' => 'post']) }}


                @if($project->opened_quotes->count() && ($project->status == 1 || $project->status == 5 || $project->status == 0))

                    <div class="h5">Yes, I hired...</div>

                    <div class="radio" id="">
                            @foreach($project->opened_quotes AS $quote)
                                <label>{{ Form::radio('quote_id', $quote->id, null, ['class' => 'form-control', 'required']) }} {!!  $quote->professional->getLogoHtml()  !!}{{ $quote->professional->title }}</label>
                                <hr/>
                            @endforeach
                    </div><!--form control-->
                @endif

                <div class="padding15 h5">No, because...</div>

                <div class="radio">
                    <label>
                        {{ Form::radio('close_reason', 'I decided to do it myself or had a friends help.', null, ['class' => 'form-control', 'required']) }} I decided to do it myself or had a friends help.
                    </label><!--col-lg-10-->
                </div><!--form control-->
                <div class="radio">
                    <label>
                        {{ Form::radio('close_reason', 'I hired a pro who\'s not on '.config('app.name').'.', null, ['class' => 'form-control', 'required']) }} I hired a pro who's not on {{ config('app.name') }}.
                    </label><!--col-lg-10-->
                </div><!--form control-->
                <div class="radio">
                    <label>
                        {{ Form::radio('close_reason', 'I\'m going to put it on hold or had a change of plans.', null, ['class' => 'form-control', 'required']) }} I'm going to put it on hold or had a change of plans.
                    </label><!--col-lg-10-->
                </div><!--form control-->
                <div class="radio">
                    <label>
                        {{ Form::radio('close_reason', 'The quotes were not the right fit for me.', null, ['class' => 'form-control', 'required']) }} The quotes were not the right fit for me.
                    </label><!--col-lg-10-->
                </div><!--form control-->


                <div class="box box-success">
                    <div class="box-body">

                        {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}

                    </div><!-- /.box-body -->
                </div><!--box-->

                {{ Form::hidden('status') }}

                {{ Form::close() }}


            </div><!-- /.box-body -->
        </div><!--box-->





    @else


    @endif

@stop


