@extends ('frontend.layouts.app')

@section ('title', 'Review '.$professional->title)

@section('content')

    <div class="padding15">
        <div class="pull-left chat-image">
            {!!  $professional->getLogoHtml('md') !!}
        </div>

        <h2>Review</h2>
        <h4>{{ $professional->title }} {!! $professional->verify_label  !!}</h4>

        <div class="">Go ahead and complete the review form below giving your honest experience of {{ $professional->title }}.</div>

        <div class="clearfix"></div>
    </div>


    @if(isset($existing_review) && $existing_review)
        <div class="alert alert-warning">
            <i class="fa fa-warning"></i> <b>Existing Review Found</b> -
            You have already written a review on this professional.
        </div>
    @endif

    @if(!$logged_in_user)
        <div class="alert alert-warning">
            <i class="fa fa-warning"></i> <b>Not Logged In</b> -
            If you have an existing <b>{{ config('app.name') }}</b> account please <a href="{{ route('frontend.auth.login') }}">login</a>, otherwise continue...
        </div>

    @else

    @endif



    @if( isset($project_review) )
        {{ Form::model($project_review, ['route' => ['frontend.project_review.update', $project_review], 'novalidate', 'data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
        @if(isset($logged_in_user) && $logged_in_user)
            {{ Form::open(['route' => 'frontend.project_review.store', 'class' => 'form-horizontal', 'novalidate', 'data-toggle' => 'validator', 'role' => 'form', 'method' => 'post']) }}
        @else
            {{ Form::open(['route' => ['public.project_review.store', $professional->id], 'class' => 'form-horizontal', 'novalidate', 'data-toggle' => 'validator', 'role' => 'form', 'method' => 'post']) }}
        @endif
    @endif

    <div class="jumbotron">
        <div class="jumbotron-contents">

            <div class="form-group">
                {{ Form::label('project_success', 'Job Completed Successfully?', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('project_success', [1 => 'Yes', 0 => 'No'], 1, ['class' => 'form-control', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            @if (isset($quote->id) && $quote->id)
            <div class="form-group" id="project_status" style="display: none">
                {{ Form::label('', 'This project is now:', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::radio('project_status', 3, null, ['class' => 'form-control', 'required']) }} {{ Form::label('project_status', 'Complete Project', ['class' => 'control-label']) }}
                    {{ Form::radio('project_status', 4, 1, ['class' => 'form-control', 'required']) }} {{ Form::label('project_status', 'Close Project', ['class' => 'control-label']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            @endif

            <div class="form-group">
                {{ Form::label('comments', 'Comments', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::textarea('comments', null, ['class' => 'form-control', 'rows'=>'4', 'placeholder' => 'How was your experience?', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('recommended', 'Would your recommend this professional?', ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('recommended', [1 => 'Yes', 0 => 'No'], 1, ['class' => 'form-control', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_quality', trans('labels.project_review.rating_quality_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_quality', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_time', trans('labels.project_review.rating_time_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_time', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_cooperative', trans('labels.project_review.rating_cooperative_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_cooperative', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_price', trans('labels.project_review.rating_price_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_price', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_overall', trans('labels.project_review.rating_overall_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_overall', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

        </div><!-- /.box-body -->
    </div><!--box-->

    @if(!$logged_in_user)
        <div class="jumbotron">
            <div class="jumbotron-contents">

                <div class="alert alert-info">These details will not be shared publicly.</div>

            @include('frontend.auth.includes.user_register_fields')

            </div>
        </div>
    @endif

    <div class="box box-success">
        <div class="box-body">

            {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}

        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::hidden('status') }}

    {{ Form::hidden('user_id', isset($logged_in_user->id) ? $logged_in_user->id: 0 ) }}
    {{ Form::hidden('quote_id', isset($quote->id) ? $quote->id: 0) }}
    {{ Form::hidden('professional_id',  isset($professional->id) ? $professional->id: $professional->id) }}
    {{ Form::hidden('project_id',  isset($quote->project_id) ? $quote->project_id: 0) }}

    {{ Form::close() }}

@stop

@section('after-scripts-end')
    <script type="text/javascript">
        @if (isset($quote->id) && $quote->id)
        $(function() {
            $('#project_success').change(function(){
                $('#project_status').hide();
                if ($(this).val() == 0)
                {
                    $('#project_status').fadeIn();
                }
            });
        });
        @endif

        $(function() {
            $('.barrating').barrating({
                theme: 'fontawesome-stars-o',
                showSelectedRating: true
            });
        });
    </script>

@append

