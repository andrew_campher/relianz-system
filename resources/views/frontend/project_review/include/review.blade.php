<div class="chat-image pull-left">
    {!!  $review->user->getImageHtml('icon', 'profile-image user-profile-icon img-circle') !!}
</div>

<div class="chat-message">

    <b>{{ $review->user->shortname }}</b>
    <p>{{ $review->comments }}</p>

    <div class="">
        {!!  Helper::showStars($review->rating_overall) !!}
        <span><i class="fa fa-clock-o"></i> {{ $review->created_at->diffForHumans() }}</span>
        <span><i class="fa fa-map-marker"></i> {{ $review->user->area_city }}</span>
        <a class="badge label-primary" onclick="$('#ratings{{ $review->id }}').toggle()" href="javascript:void(0)">See Ratings <i class="fa fa-angle-down"></i></a>
    </div>

    <div class="all_ratings" id="ratings{{ $review->id }}" style="display: none">
        <table class="table table-striped">
              <tr>
                <th>Quality of work</th>
                <td >
                    {!!  Helper::showStars($review->rating_quality) !!}
                </td>
            </tr><!--form control-->

            <tr>
                <th>Finished on-time</th>
                <td>
                    {!!  Helper::showStars($review->rating_time) !!}
                </td>
            </tr><!--form control-->

            <tr>
                <th>Handling of changes</th>
                <td>
                    {!!  Helper::showStars($review->rating_cooperative) !!}
                </td>
            </tr><!--form control-->

            <tr>
                <th>Cost</th>
                <td>
                    {!!  Helper::showStars($review->rating_price) !!}
                </td>
            </tr><!--form control-->

        </table>
    </div>
</div>