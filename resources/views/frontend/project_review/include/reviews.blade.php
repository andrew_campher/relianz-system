@php($cnt=1)
@if(isset($reviews) && !$reviews->isEmpty())

        @foreach($reviews AS $review)

            @include('frontend.project_review.include.review', ['review' => $review])

            @if(count($reviews) > $cnt)
            <hr>
            @endif

            @php($cnt++)

        @endforeach

@else

    <div>
        <div class="alert alert-info h6"><i class="fa fa-info-circle"></i> Be the first to write a review</div>
    </div>
@endif