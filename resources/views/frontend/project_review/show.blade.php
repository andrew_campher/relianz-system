@extends ('frontend.layouts.app')

@section ('title', 'Review '.$project_review->professional->title)

@section('content')

    <div class="jumbotron">
        <div class="jumbotron-contents">

            <div class="pull-left chat-image">
                {!!  $project_review->professional->getLogoHtml('md') !!}
            </div>

            <h2>Review</h2>
            <h4>{{ $project_review->professional->title }} {!! $project_review->professional->verify_label  !!}</h4>

            <div class="clearfix"></div>

        </div>
    </div>

@stop
