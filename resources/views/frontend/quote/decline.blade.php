@extends ('frontend.layouts.app')

@section ('title', 'Decline Quote - '.$quote->title)

@section('content')

    {{ Form::open(['route' => ['frontend.quote.decline_store', $quote->id], 'class' => 'form-horizontal', 'novalidate', 'data-toggle' => 'validator', 'role' => 'form', 'method' => 'post']) }}

    <div class="jumbotron">
        <div class="jumbotron-contents">

            <div class="padding15">
                <h2 class="no-top-margin">Do you wish decline {{ $quote->professional->title }} offer?</h2>
            </div>

            <div>{!! $quote->professional->getLogoHtml('md') !!}</div>

            <div class="h5">Yes, because...</div>

            <div class="radio">
                <label>
                    {{ Form::radio('decline_reason', 'Quoted price is too expensive.', null, ['class' => 'form-control', 'required']) }} Quoted price is too expensive
                </label><!--col-lg-10-->
            </div><!--form control-->
            <div class="radio">
                <label>
                    {{ Form::radio('decline_reason', 'I don\'t trust your company.', null, ['class' => 'form-control', 'required']) }} I don't trust your company.
                </label><!--col-lg-10-->
            </div><!--form control-->
            <div class="radio">
                <label>
                    {{ Form::radio('decline_reason', 'I don\'t think your company is the right fit.', null, ['class' => 'form-control', 'required']) }} I don't think your company is the right fit.
                </label><!--col-lg-10-->
            </div><!--form control-->
            <div class="radio">
                <label>
                    {{ Form::radio('decline_reason', 'Other', null, ['class' => 'form-control', 'required']) }} Other (please specify)
                </label><!--col-lg-10-->
                <input name="decline_custom" type="text" class="form-control">
            </div><!--form control-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-body">

            <div class="alert alert-info"><b>Please Note:</b> Reason will be forwarded on to the company representative.</div>

            {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}

        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::hidden('status') }}

    {{ Form::close() }}

@stop


