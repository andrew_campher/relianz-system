@if ($quote->status == 7)
    <div class="alert alert-warning">
        <h4 class="no-top-margin"><i class="fa fa-question-circle-o"></i> Confirm Hire</h4>
        <p>{{ trans('strings.frontend.quotes.status.'.access()->group().'.request_hired_description') }}</p>
    </div>
@endif

<div class="row">
    <div class="col-xs-12">
        <h2 id="quote_heading" class="qt_heading no-top-margin pull-left"><i class="fa fa-address-card"></i> Quote </h2>
        <div class="qt_label label pull-left btn-lg">QT0{{ $quote->id }}</div>
        <div class="qt_label pull-left">{!! $quote->status_label !!}</div>
        <div class="pull-right">
            Sent: {{ $quote->created_at->format(config('app.date.long_min')) }}
        </div>
    </div>
</div>

<div class="jumbotron">
    <div class="jumbotron-contents">
        <div class="row">


            <div class="col-sm-7">
                @include('frontend.professional.includes.mini_profile', ['professional' => $quote->professional])
            </div>

            <div class="col-sm-5 text-right">
                <div class="h4"><i class="fa fa-phone-square"></i> {{ $quote->professional->telephone }}</div>
                @if($quote->professional->company_email)
                <div><i class="fa fa-envelope-square"></i> <a target="_blank" href="mailto:{{ $quote->professional->company_email }}">Email Address</a></div>
                @endif
            </div>
        </div>

        <hr>


        <div class="quote_avatar chat-image pull-left">
            {!! $quote->user->getImageHtml('thumb', 'avatar-med img-circle') !!}<br/>
            Written by,<br/>
            <b>{{ $quote->user->name }} {!! $quote->user->online_label !!}</b>
        </div>
        <div class="card-body quote-message">
            <h5 class="no-top-margin">Quote Details:</h5>
            <p>{!! $quote->description !!}</p>

            @if($quote->media->count())
            <div class="quote_attachments clearfix">
                <div class="h6"><i class="fa fa-paperclip"></i> Attachments</div>

                @include('frontend.media.includes.partials.gallery', ['media' => $quote->media])

            </div>
            @endif

        </div>
        <div class="clearfix"></div>

        <div class="text-center {{ $quote->need_more_info?'bg-warning':'' }}">
            <hr>
            <div class="h5">Estimated Price:</div>
            <div class="h1 quoteprice ">
                {!! Helper::formatPrice($quote->price) !!} <span>{!!  trans('strings.frontend.quotes.price_types.'.$quote->price_type) !!}</span>
                @if($quote->need_more_info)
                    <br/>
                    <div data-toggle="popover" data-content="The professional has indicated that he is unable to estimate your project. Please be in contact in order to answer any questions he has." class="badge btn-lg label-primary"><i class="fa fa-question-circle-o"></i> Require more information</div>
                @endif
            </div>
        </div>



        @if($quote->project->status != 4 && $quote->project->status != 3)

            <div class="tip quote_actions">
            @if($quote->user_id == access()->id())

                <div class="h5 no-top-margin">Quote Status: {!!  $quote->StatusLabel !!}</div>

                @if($quote->status != 4)
                <table width="100%">
                    <tr>
                    <td align="center"><a class="btn btn-primary" href="#messages_heading"><i class="fa fa-reply"></i> Reply</a></td>
                    @if($quote->status == 7)
                            <td align="center"><div class="alert alert-warning"><i class="fa fa-hourglass-2"></i> Waiting for client to confirm hire</div></td>
                    @elseif($quote->status == 3)
                        <td align="center"><div class="alert alert-info"><i class="fa fa-hourglass-2"></i> Waiting for client review</div></td>
                    @else
                        @php($title = "This will send a notification to the client to confirm the hire.")

                        <td align="center"><a class="btn btn-success" id="qt-btn-{{ $quote->id }}" data-toggle="popover" data-trigger="hover" data-content="{{ $title }}" href="javascript:void(0)" onclick="MarkQuoteHired({{ $quote->id }}, this)" ><i class="fa fa-handshake-o"></i> Mark as Hired</a></td>
                    @endif
                    </tr>
                </table>
                @endif
            @elseif(access()->id() == $quote->project->user_id)

                @php($title = "This will mark the project as \"hired\" allowing you to review the professional's work once done.")


                    <table width="100%">
                        <tr>
                        @if($quote->status == 1 || $quote->status == 2 || $quote->status == 6)
                        <td align="center"><a class="btn btn-default" href="#messages_heading"><i class="fa fa-reply"></i> Reply</a></td>
                        @endif
                        @if($quote->hired)
                            @if ($quote->status == 3)
                                <td align="center"><a id="qt-btn-{{ $quote->id }}" href="{{ route('frontend.quote.project_review.create', $quote->id) }}" class="btn btn-info"><i class="fa fa-pencil-square"></i> Review Work</a></td>
                            @endif
                        @else
                            <td align="center"><a class="btn btn-success" id="qt-btn-{{ $quote->id }}" data-toggle="popover" data-trigger="hover" data-content="{{ $title }}" href="javascript:void(0)" onclick="MarkQuoteHired({{ $quote->id }}, this)" ><i class="fa fa-handshake-o"></i> {{ $quote->status == 7?'Confirm Hire':'Mark as Hired' }}</a></td>
                        @endif
                        @if ($quote->status == 1 || $quote->status == 2 || $quote->status == 6 || $quote->status == 7)
                            <td align="center"><a class="btn btn-danger" data-toggle="firemodal" href="{{ route('frontend.quote.decline', $quote->id) }}"><i class="fa fa-thumbs-down"></i> Decline</a></td>
                        @endif
                        </tr>
                    </table>


            @endif
            </div>

        @endif



    </div>
</div>


@if($quote->status == 5 && $quote->review)
    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h4 id="reviews_heading" class="no-top-margin">Client Review</h4>

            @include('frontend.project_review.include.review', ['review' => $quote->review])

            @if ($quote->client_review)
                <hr>
                <div class="">
                    <div class="h5">Professional Review</div>
                @include('frontend.client_review.include.review', ['review' => $quote->client_review])
                </div>
            @else
                @if($quote->review && $quote->user_id == access()->id())
                    <div class="heading_btn">
                        <a class="btn btn-info" href="{{ route('frontend.quote.client_review.create', $quote->id) }}"><i class="fa fa-pencil-square-o"></i> Review Client</a>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endif

@php($disable_add = 0)
@php($disable_first_comment = 0)

@if($quote->status == 4 || $quote->project->status == 4 || $quote->project->status == 3)
    @php($disable_add = 1)
@endif
@if($quote->user_id == $logged_in_user->id)
    @php($disable_first_comment = 1)
@endif
<div class="row">
    <div class="col-xs-12">
        <h2 id="messages_heading" class="no-top-margin pull-left"><i class="fa fa-comment"></i> Messages</h2>
    </div>
</div>

<div>
    @include('frontend.comments.discuss', ['model'=>'quote', 'id'=>$quote->id, 'disable_add' => $disable_add, 'disable_first_comment' => $disable_first_comment])
</div>
