
@if( isset($existing_quote) && $existing_quote)
    {{ Form::model($existing_quote, ['route' => ['frontend.quote.update', $existing_quote->id ], 'enctype' => 'multipart/form-data', 'class' => '', 'role' => 'form', 'method' => 'PATCH', 'id' => 'quoteForm', 'data-toggle'=>'validator', 'data-disable'=>'false']) }}
@else
    {{ Form::open(['route' => 'frontend.quote.store', 'enctype' => 'multipart/form-data', 'class' => '', 'role' => 'form', 'method' => 'post', 'id' => 'quoteForm', 'data-toggle'=>'validator', 'data-disable'=>'false']) }}
@endif

<h4 id="quote_heading"><i class="fa fa-drivers-license"></i> New Quote</h4>

<div class="jumbotron">
    <div class="jumbotron-contents">

        @include('frontend.professional.includes.mini_profile', ['professional' => $logged_in_user->professional])

        <hr>

        <div class="form-group pro_textarea">
            <h5>Write Details</h5>
            <p class="tip"><i class="fa fa-lightbulb-o"></i> Tell the customer why you would be the best fit for this project and give an idea of what you intend todo.</p>
            <div style="display: inline">
                <div class="pull-left">{!! $logged_in_user->getImageHTML('icon') !!}</div>
                <br> By {{ $logged_in_user->name }}
            </div>
            {{ Form::textarea('description', null, ['class' => 'form-control wysiwyg', 'placeholder' => 'Write to client...', 'required', 'data-validate'=>'true', 'data-minlength'=>'50']) }}
            <div class="help-block with-errors"></div>
        </div>



        <div class="form-group">
            <div class="fileattach pull-left">
                <span class="fileselector">
                    <a href="javascript:void(0)" id="clipicon" onclick="$(this).next().trigger('click');" class="btn btn-primary"><i class="fa fa-paperclip"></i> Attach Files</a>
                    <input style="display: none" type="file" class="att attach1" name="file[]">
                </span>
                <div id="attachments"></div>
            </div>
            <div class="clearfix"></div>
            @if( isset($existing_quote) && $existing_quote )
                @include('frontend.media.includes.partials.gallery', ['media' => $existing_quote->media, 'edit' => 1])
            @endif
        </div>

        <hr>

        <div class="form-group">
            <h5 class="inblock" data-toggle="popover" data-trigger="hover" title="Please try give a accurate estimate">Estimate Cost <i class="fa fa-question-circle text-primary fa-lg"></i></h5>
            <div class="form-group form-inline">
                <div class="input-group">
                    <div class="input-group-addon">{{ config('app.currency.symbol') }}</div>
                    {{ Form::number('price', null, ['class' => 'form-control input-lg', 'placeholder' => '0.00', 'step'=>'0.01', 'required', 'id'=>'price_input']) }}
                    <div class="input-group-addon help-block with-errors"></div>
                </div>
                <div class="input-group">
                    <div class="col-lg-10">
                    {{ Form::select('price_type', ['fixed' => 'Fixed Price', 'per_hour' => 'Per Hour', 'per_day' => 'Reoccurring - Daily', 'per_month' => 'Reoccurring - Monthly'], null, ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::checkbox('need_more_info', 1, null, ['class' => 'form-control ', 'id' => 'need_more_info']) }} <label for="need_more_info" data-toggle="popover" data-trigger="hover" data-content="If you are really unable to estimate the job then please select this option. Clients are less likely to respond to quotes that don't give them a rough estimate.">Need more info</label> <i class="fa fa-question-circle text-primary fa-lg"></i>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="pull-right quote_submit">
            @if( isset($existing_quote) && $existing_quote )
                <button type="submit" class="btn btn-success btn-lg">Update Quote <i class="fa fa-save"></i></button>
            @else
                <button type="submit" class="btn btn-success btn-lg">Send Quote <i class="fa fa-send-o"></i></button>
            @endif
        </div>

        <div class="clearfix"></div>

    </div>

</div>

@section('after-scripts-end')
<script>

    var cnt = 2;
    $(document).on('change', '.att', function(){

        str = $(this).val();

        if (str.length) {
            $('#attachments').prepend('<div class="fileat fil' + cnt + '"><i class="fa fa-paperclip"></i> ' + str.split(/(\\|\/)/g).pop() + ' <i file-data="attach' + cnt + '" class="removefile fa fa-remove"></i></div>');
            var firstitem = '.attach'+(cnt-1);

            $('#clipicon').after($(firstitem).clone().attr('class', 'att attach' + cnt));
            $('.attach'+ cnt).val('');
            cnt = cnt + 1;
        }
    });
    $(document).on("click", ".removefile", function (e) {
        $('.'+$(this).attr('file-data')).remove();
        $(this).parent('.fileat').remove();
    })



    $("#need_more_info").change(function() {
        if(this.checked) {
            $('#price_input').removeAttr('required');
        } else {
            $('#price_input').addAttr('required');
        }
    });

    var editor = $('.wysiwyg').wysihtml5({
        stylesheets: ["{{ asset('css/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.css') }}"],
        toolbar: {
            "font-styles": false,"emphasis": true,"lists": true,"html": false,"link": false,"image": false,"color": false,"blockquote": true, //Blockquote
        },
        events: {
            change: function () {
                $('.wysiwyg').html(editor[0].value);
                $('#quoteForm').validator('update');
            }
        }
    });

    @if( !isset($existing_quote) || !$existing_quote)
    $('#form').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            sweetAlert('Please fill in all required fields', 'warning');
        } else {
            $('#quoteForm').on('submit', function (e) {
                        @if($logged_in_user->professional->account->credits_remain >= $project->service->getFee())
                var form = this;
                e.preventDefault();
                swal({
                            title: "Send Quote to {{ $project->user->short_name }}",
                            text: 'It will be <b>{!! config('app.pricing.credit_icon') !!} {{ $project->service->getFee(true) }}</b> to send this quote.',
                            type: "info",
                            html: true,
                            showCancelButton: true,
                            confirmButtonColor: '#8cc152',
                            confirmButtonText: 'Send it!',
                            cancelButtonText: "Cancel",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                $('.sa-confirm-button-container .confirm').html('<i class="fa fa-gear fa-spin fa-fw"></i>');
                                form.submit();
                            }
                        });
                @endif
            });
        }
    })



    @endif


</script>

@append


{{ Form::hidden('professional_id', $logged_in_user->professional->id) }}
{{ Form::hidden('project_id', $project->id) }}
{{ Form::hidden('service_id', $project->service_id) }}
{{ Form::hidden('user_id', $logged_in_user->id) }}

{{ Form::close() }}

