@extends('frontend.layouts.app')

@section ('title', 'Quote: '.$quote->professional->title)

@section('content')

    @include('frontend.quote.includes.details')

@endsection