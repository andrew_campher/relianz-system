
<form action="{{ route('public.project.wizard') }}" class="searchform" role="search" data-toggle="validator" data-disable="false">
    <div class="search-only">
        <i class="search-icon glyphicon fa fa-pencil-square"></i>
        <input name="searchstring" class="searchservice form-control search-query" required type="text" value="" autocomplete="off" placeholder="What do you need done?" />
        @if(!isset($search_embed) || !$search_embed )
            <a class="btn btn-primary" onclick="getStarted(this)" href="javascript:void(0)"><span class="visible-xs">GO</span><span class="hidden-xs">Get Started</span></a>
        @endif
        <div class="suggestresults"></div>
    </div>

</form>


@section('search_js')
    <script>
        var timer = 0;
        var currentRequest = null;
        var ss = 'nothing';
        var suggestresults = null;

        var offset_scroll = 65;
        if($(window).width() < 500) {
            offset_scroll = 5;
        }

        $('.searchservice').on('keyup focus', function() {
            var el = $(this);
            var searchstring = el.val();
            if (searchstring == ss) {
                return false;
            }
            ss = searchstring;

            $('html, body').animate({
                scrollTop: $(this).offset().top-offset_scroll
            }, 400);

            clearTimeout(timer);
            @if(isset($search_embed) && $search_embed)
                suggestresults = $('.suggestresults_embed');
            @else
                suggestresults = el.parent().find('.suggestresults');
            @endif

            @if(!isset($search_embed) || !$search_embed )
            if($('.search-backdrop').length == 0) {
                $('<div class="search-backdrop"></div>').appendTo('body').hide().fadeIn(500);
                $('.search-backdrop').on('click', function () {
                    $('.search-backdrop').remove();
                    suggestresults.html('');
                    ss = 'none';
                    if(currentRequest != null) {
                        currentRequest.abort();
                    }
                })
            }
            @endif

            if (searchstring.length < 2 && searchstring.length > 0) return false;


            timer = setTimeout(function(){
                currentRequest = $.ajax({
                    url: '',
                    method: "GET",
                    dataType: "json",
                    data:  { string: searchstring },
                    beforeSend : function()    {
                        if(currentRequest != null) {
                            currentRequest.abort();
                        }
                    },
                    success: function (response) {
                        if (response.length != 0) {
                            $(suggestresults).html('');
                            var items = [];
                            $.each(response, function (key, val) {
                                items.push("<li><a onclick=\"fireModal('{{ route("public.project.wizard") }}?service=" + val.id + "&string="+encodeURI(searchstring)+"')\" href='javascript:void(0)'><i class='fa fa-hand-pointer-o'></i> " + val.title + "</a></li>");
                            });

                            $("<ul/>", {
                                "class": "search_list servicelist list-unstyled",
                                html: items.join("")
                            }).appendTo(suggestresults);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log('ajax error: ' + thrownError);
                    }
                });
            },200);
        });

        function getStarted(el) {
            input = $(el).parent().find('.search-query');
            input_val = input.val();
            if (input_val && input_val.length > 6) {
                fireModal('{{ route("public.project.wizard") }}?service=&string='+encodeURI(input_val));
            } else {
                $(input).popover({
                    placement:'right',
                    delay: 1500,
                    trigger:'manual',
                    content:'That\'s too little letters to know what your looking for...'
                });
                $(input).popover('show')
            }
        }
    </script>
@stop