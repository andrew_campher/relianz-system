{{ Form::model($logged_in_user, ['route' => 'frontend.user.profile.update', 'class' => 'form-horizontal', 'method' => 'PATCH', 'files' => true]) }}

    <div class="form-group">
        {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('first_name', null, ['class' => 'form-control', 'required', 'placeholder' => 'First name']) }}
        </div>
    </div>
<div class="form-group">
    {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('last_name', null, ['class' => 'form-control', 'required', 'placeholder' => 'Last name']) }}
    </div>
</div>

    <div class="form-group">
        {{ Form::label('image', 'Image', ['class' => 'col-md-4 control-label']) }}

        <div class="col-md-6">

            @if( isset($user) )
                <div class="image_crop">
                    <img src="{!! asset('images/users/'.$user->image) !!}" />
                </div>

                @include('frontend.media.includes.croptool')
            @endif
            {{ Form::file('image') }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    @if ($logged_in_user->canChangeEmail())
        <div class="form-group">
            {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
            <div class="col-md-6">
                {{ Form::input('email', 'email', null, ['class' => 'form-control', 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
            </div>
        </div>
    @endif

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-primary']) }}
        </div>
    </div>

{{ Form::close() }}