<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.frontend.user.profile.avatar') }}</th>
        <td>{!! $logged_in_user->getImageHtml('thumb', 'user-profile-image') !!}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.name') }}</th>
        <td>{{ $logged_in_user->name }}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.email') }}</th>
        <td>{{ $logged_in_user->email }}</td>
    </tr>
    <tr>
        <th>Role</th>
        <td>
        @foreach($logged_in_user->roles AS $role)
                {{ $role->name }},
        @endforeach
        </td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.created_at') }}</th>
        <td>{{ $logged_in_user->created_at }} ({{ $logged_in_user->created_at->diffForHumans() }})</td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.last_updated') }}</th>
        <td>{{ $logged_in_user->updated_at }} ({{ $logged_in_user->updated_at->diffForHumans() }})</td>
    </tr>
</table>

@if(!$logged_in_user->reviews->isEmpty())
    <div class="h5">Reviews</div>
    <p>These are the reviews and ratings by professionals you hired.</p>

    @foreach($logged_in_user->reviews AS $review)
        @include('frontend.client_review.include.review')
    @endforeach
@endif