@extends('frontend.layouts.app')

@section ('title', 'Professional Contacts')

@section('content')

    <h1>Contacts</h1>

    @if ($contacts->count())
        <div class="panel-group panel-group-lists collapse in" id="accordion1">
            @foreach($contacts AS $professional)
                <div class="panel">
                    <div class="panel-heading">
                        <div class="h5 panel-title">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse{{ $professional->id }}" class="collapsed">
                                <i class="fa fa-star"></i> {{ $professional->title }} {!! $professional->verify_label  !!}
                            </a>
                        </div>
                    </div>
                    <div id="collapse{{ $professional->id }}" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <table class="table no-border">
                                <tr>
                                    <th>Telephone</th>
                                    <th>Email</th>
                                    <th>Website</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>{{ $professional->telephone }}</td>
                                    <td>{{ $professional->company_email }}</td>
                                    <td>{{ $professional->website_url }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" href="{{ route('public.professional.show', [$professional->id, $professional->slug]) }}"><i class="fa fa-drivers-license-o"></i> Profile</a>
                                        <a data-toggle="firemodal" class="btn btn-primary btn-sm" href="{{ route('public.project_review.professional_review', [$professional->id]) }}"><i class="fa fa-pencil-square-o"></i> Write Review</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    @else
        <div>No professional contacts found. </div>
    @endif


@endsection