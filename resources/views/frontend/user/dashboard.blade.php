@extends('frontend.layouts.app')

@section ('title', 'Dashboard')

@section('content')

    @if ($active_projects && !$active_projects->isEmpty())
        <div class="row">
            <div class="col-xs-12">
                <h3 class="">In-Progress Projects</h3>
                <p>A professional was hired and the project is in-progress</p>
            </div>

            @foreach($active_projects AS $project)
                <div class="col-xs-12 col-sm-4 col-md-3">

                    @include('frontend.project.includes.partials.item')

                </div><!--col-md-8-->
            @endforeach

        </div><!-- row -->

    @else
        @if($open_projects->isEmpty())
            <div class="jumbotron">
                <div class="jumbotron-contents">
                    <div class="projects_first">
                        <div class="text-center">
                            <h1 class="text-center text-primary"><i class="fa fa-paper-plane"></i> Start Here</h1>
                            <div class="h4 no-top-margin">Get quotes on your project by first searching for a service below:</div>
                        </div>
                        @include('frontend.service.partials.search')
                    </div>
                </div>
            </div>

        @endif
    @endif


    <div class="row">
        <div class="col-xs-12">
            <h3 class="">Open Projects</h3>
            <p>These are projects waiting for quotes and no hire has yet been made.</p>
        </div>

        @if ($open_projects && !$open_projects->isEmpty())
            @foreach($open_projects AS $project)
                <div class="col-xs-12 col-sm-6">

                @include('frontend.project.includes.partials.item')

                </div><!--col-md-8-->
            @endforeach
        @endif
        <div class="col-xs-12 col-sm-4 col-md-3">

            <a data-toggle="firemodal" href="{{ route('public.project.select_service') }}" class="h2 btn btn-success"><i class="fa fa-plus"></i> Add Project</a>
        </div>
    </div><!-- row -->

    @if ($expired_projects && !$expired_projects->isEmpty())
    <div class="row">
        <div class="col-xs-12">
            <h3 class="">Expired Projects</h3>
            <p>These projects have been listed for the maximum amount of days. <b>Consider hiring a profession, closing or relisting them</b>.</p>
        </div>


            @foreach($expired_projects AS $project)
                <div class="col-xs-12 col-sm-6">

                    @include('frontend.project.includes.partials.expired_item')

                </div><!--col-md-8-->
            @endforeach

    </div><!-- row -->
    @endif

    <div class="margin15">
    {{ link_to_route('frontend.project.all', $projects_complete. ' Closed Project'.($projects_complete == 1 ? '' : 's'), ['status' => 'completed']) }}
    </div>

@endsection