
<div class="h5">{{ isset($settings['types']) && !$settings['types']? "Enter Address:" : "Enter your area:" }}</div>
@if(!isset($settings['exclude_fields']) || !$settings['exclude_fields'])
    {{ Form::text('area', null, ['id' => 'optArea', 'checkme', 'class' => 'form-control input-clean input-lg', 'placeholder' => 'Type area here...', 'required']) }}

    {{ Form::hidden('lat', null, ['id' => 'lat']) }}
    {{ Form::hidden('lng', null, ['id' => 'lng']) }}
@endif

<div id="map" style="height:300px;display:none"></div>

<div class="input-group">
    <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
    <input name="selected_location" style="display:none" id="selected_location" value="" readonly class="checkme form-control input-clean">
</div>




@section('after-scripts-end')
    <script>
        if (typeof window.initAutocomplete === 'undefined') {
            window.initAutocomplete = function() {
                var map_options = {
                    center: new google.maps.LatLng(-28.7810137, 23.5007343),
                    zoom: 6.5,
                    {{ isset($gmap['scaleControl']) && $gmap['scaleControl']?'scaleControl: '.$gmap['scaleControl'].',':'' }}
                            {{ isset($gmap['mapTypeControl']) && $gmap['mapTypeControl']?'mapTypeControl: '.$gmap['mapTypeControl'].',':'' }}
                            {{ isset($gmap['draggable']) && $gmap['draggable']?'draggable: '.$gmap['draggable'].',':'' }}
                            {{ isset($gmap['navigationControl']) && $gmap['navigationControl']?'navigationControl: '.$gmap['navigationControl'].',':'' }}
                            {{ isset($gmap['scrollwheel']) && $gmap['scrollwheel']?'scrollwheel: '.$gmap['scrollwheel'].',':'' }}
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("map"), map_options);

                var input = document.getElementById("optArea");
                var options = {
                    componentRestrictions: {country: "za"}
                    {!! isset($settings['types']) && !$settings['types']? '' : ', types: [\'(regions)\']' !!}

                };
                var autocomplete = new google.maps.places.Autocomplete(input, options);
                autocomplete.bindTo("bounds", map);

                var marker = new google.maps.Marker({map: map});

                if (input.value != '') {
                    document.getElementById('map').style.display = 'block';
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'address': input.value}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
                            var marker2 = new google.maps.Marker({
                                map: map,
                                {{ isset($gmap_marker['draggable']) && $gmap_marker['draggable']?'draggable: '.$gmap_marker['draggable'].',':'' }}
                                position: results[0].geometry.location
                            });
                            map.setZoom(15);
                            marker2.addListener('dragend', function (event) {
                                $('#lat').val(event.latLng.lat());
                                $('#lng').val(event.latLng.lng());
                            });

                        }
                    });
                }

                google.maps.event.addListener(autocomplete, "place_changed", function () {
                    $('#map').show();
                    google.maps.event.trigger(map, 'resize');

                    var place = this.getPlace();

                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(14);
                    }

                    marker.setPosition(place.geometry.location);
                    $('#lat').val(place.geometry.location.lat());
                    $('#lng').val(place.geometry.location.lng());

                    $('#selected_location').show().val(input.value);
                });
            }

            if (window.google) {
                initAutocomplete();
            }
        } else {
            initAutocomplete();
        }

        if (!window.google) {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            document.body.appendChild(s);
            s.src = "https://maps.googleapis.com/maps/api/js?key={{ config('app.google.api_maps_key') }}&libraries=places&callback=initAutocomplete";
            s.setAttribute("async","");
            s.setAttribute("defer","");
            void(0);
        }
    </script>
@append