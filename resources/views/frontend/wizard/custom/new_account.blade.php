<div class="clearfix">

    <div id="socialLogins" class="padding15 text-center">
        {!! $socialite_links !!}
    </div>

    <div id="existing_account" style="display: none">
        <h4 class="no-top-margin">Existing Account</h4>
        <div class="alert alert-warning">We found an existing account with that email address. Please enter the password for it below:</div>

        <div class="form-group">
            {{ Form::label('username', 'Login Email', ['class' => 'col-md-4 control-label']) }}
            <div class="col-md-6">
                {{ Form::text('username', null, ['id' => 'username_in', 'class' => 'form-control', 'checkme', 'placeholder' => 'Enter email address', 'required']) }}
            </div>
        </div>
    </div>

    <div id="new_account" style="display: none">
        <h4 class="no-top-margin">New Account</h4>
        <p>We did not find an existing account with that email address. Lets create a new account for you! You only need to register once.</p>

        <div class="form-group">
            {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label']) }}
            <div class="col-md-6">
                {{ Form::text('first_name', null, ['class' => 'form-control', 'checkme', 'placeholder' => 'Enter first name', 'required', 'autocomplete'=>'off']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label']) }}
            <div class="col-md-6">
                {{ Form::text('last_name', null, ['class' => 'form-control', 'checkme', 'placeholder' => 'Enter last name', 'required', 'autocomplete'=>'off']) }}
                <div class="help-block with-errors"></div>
            </div><!--col-lg-10-->
        </div><!--form control-->

    </div>

    <div id="account_password" class="form-group" style="display: none">
        {{ Form::label('password', 'Password', ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::password('password', ['class' => 'form-control', 'checkme', 'placeholder' => 'Password', 'required', 'autocomplete'=>'off']) }}
            <div class="help-block with-errors"></div>
        </div><!--col-lg-10-->
    </div><!--form control-->

</div>

<div class="wizard_terms small text-center padding15">
    <p>By clicking "Finish" button you agree to the <a href="{{ route('public.page', ['terms']) }}" target="_blank">Terms of Use</a> & <a href="{{ route('public.page', ['terms', '#privacy_policy']) }}" target="_blank">Privacy Policy</a></p>
</div>


@if ($socialite_links)
@section('after-scripts-end')
    <script>
        function SocialLogin(url) {
            back = false;

            var signinWin = window.open(url, "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 500 + ",top=" + 200);

            var pollTimer = window.setInterval(function() {
                if (signinWin.closed !== false) {
                    window.clearInterval(pollTimer);
                    /*TEST IF his logged in Then Submit FORM.*/
                    jQuery.ajax({
                        url: "/user/is_logged_in",
                        success:function(resp){
                            if (resp.status == 'success') {
                                swal("Login Successful", 'We will now submit your quote request.', "success");
                                $('#WizardForm').submit();
                            } else {
                                swal("Login Error Occurred", resp.message+'. Please try login using a different method.', "error");
                            }
                        }
                    });
                }
            }, 200);

        }

    </script>
@append
@endif