@extends('frontend.layouts.app')

@section('content')

    <h1 class="text-primary">Please select a service</h1>

    <p>Go ahead and search or choose a matched service below...</p>


    <div class="jumbotron">
        <div class="jumbotron-contents">

            @php($search_embed = 1)
            @include('frontend.service.partials.search')

            <div class="suggestresults_embed">
                @if($found_services)
                <ul class="servicelist list-unstyled">
                    @foreach($found_services AS $found_service)
                        <li>
                            <a href="javascript:void(0)" onclick="fireModal('{{ route('public.project.wizard') }}?service={{ $found_service->id }}')"><i class="fa fa-hand-pointer-o"></i> {{ $found_service->title }}</a>
                        </li>
                    @endforeach
                </ul>
                @endif
            </div>

            <div class="padding15"><i class="fa fa-info-circle"></i> If you do not see the service above try searching something else or <a class="h6 badge label-success" href="javascript:void(0)" onclick="fireModal('{{ route('public.project.wizard') }}?service=0')">click here</a> to begin the service request.</div>
        </div>
    </div>

@endsection




