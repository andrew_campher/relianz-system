@extends('frontend.layouts.app')

@section ('title', 'Project Create Wizard')

@section('content')
@php
    $cnt=0;
@endphp
<div id="rootwizard">
    <div class="navbar" style="display:none">
        <div class="navbar-inner" >
            <div class="container">
                <ul>
                    @foreach($services['questions'] AS $question)
                        <li class="hidden"><a href="#tab{{ $cnt }}" data-toggle="tab">Step {{ $cnt }}</a></li>
                        @php $cnt++; @endphp
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="modal-header">
        <div class="progress">
            <div id="bar" class="bar progress-bar progress-bar-primary" role="progressbar">
                <span></span>
            </div>
        </div>
    </div>

    @php $cnt=0;  @endphp
    {{ Form::open(['route' => 'public.project.store', 'enctype' => 'multipart/form-data', 'id'=>'WizardForm','onsubmit'=>'retun SubmitWizard()', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
    <div class="tab-content">

        @foreach($services['questions'] AS $question)

            @if(isset($question['name']) && $question['name'])
                @php($inputName = $question['name'])
            @else
                @php($inputName = 'q_'.$question['id'])
            @endif

        @php
            $gmap['scaleControl'] = 'false';
            $gmap['scrollwheel'] = 'false';
            $gmap['mapTypeControl'] = 'false';
            $gmap['default_area'] = access()->id() && access()->user()->area?access()->user()->area:null;

        $selected = null;
        @endphp

            <div class="tab-pane fade" data-name="part_{{ $question['id'] }}" role="tabpanel" id="tab{{ $cnt }}">
                @if($question['question_type'] == 'custom')
                    @if(View::exists('frontend.wizard.custom.'.$question['custom']))
                        @include('frontend.wizard.custom.'.$question['custom'])
                    @endif
                @else
                    <h3 class="no-top-margin">{{ $question['title'] }}</h3>
                    <div class="">
                        @if($question['question_type'] == 'textarea')
                            {{ Form::textarea($inputName, null, [(isset($question['optional']) && $question['optional']?'optional':'checkme'),'placeholder'=>'Please enter here', 'class' => 'form-control', 'rows' => 4]) }}

                        @elseif($question['question_type'] == 'select')
                            @foreach($options[$question['id']] AS $option)
                                @php
                                if (isset($option['set_value']) && $option['set_value']) {
                                    if(isset($option['checked']) && $option['checked']) {
                                        $selected = $option['set_value'];
                                    }
                                    $selectoptions[$option['set_value']] = $option['title'];
                                } else{
                                    if(isset($option['checked']) && $option['checked']) {
                                        $selected = $option['title'];
                                    }
                                    $selectoptions[$option['title']] = $option['title'];
                                }


                                @endphp
                            @endforeach

                            {{ Form::{$question['question_type']}($inputName, $selectoptions, $selected, ['id'=>'option'.$question['id'],(isset($question['optional']) && $question['optional']?'optional':'checkme'), 'placeholder' => '-- Select Option --', 'class' => 'form-control']) }}
                            @foreach($options[$question['id']] AS $option )
                                @php($special_arr = explode("|", $option['special']))
                                @if(View::exists('frontend.options.special.'.$special_arr[0]))
                                    @include('frontend.options.special.'.$special_arr[0])
                                @endif
                            @endforeach
                        @else
                            @if(isset($options[$question['id']]))
                                @foreach($options[$question['id']] AS $option )

                                    @if(isset($option['name']) && $option['name'])
                                        @php($inputName = $option['name'])
                                    @endif

                                    @if($question['question_type']=='radio' || $question['question_type']=='checkbox')
                                        <div class="radio">
                                        <label>
                                    @endif

                                    @if($question['question_type']!='radio' && $question['question_type']!='checkbox' && $option['title'])
                                        <label for="{{ 'opt'.$cnt.$option['id'] }}">{{ $option['title'] }}</label>
                                    @endif

                                    @if($question['question_type']=='checkbox')
                                        {{ Form::{$question['question_type']}($inputName.'[]', (isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title']), null, [(isset($question['optional']) && $question['optional']?'optional':'checkme'), (isset($option['checked']) && $option['checked']?'checked':'unchecked'), 'id'=>'opt'.$cnt.$option['id'], 'class' => 'form-control']) }}
                                    @elseif ($question['question_type']=='radio')
                                        {{ Form::{$question['question_type']}($inputName, (isset($option['set_value']) && $option['set_value']?$option['set_value']:$option['title']), null, [(isset($question['optional']) && $question['optional']?'optional':'checkme'), (isset($option['checked']) && $option['checked']?'checked':'unchecked'), 'id'=>'opt'.$cnt.$option['id'], 'class' => 'form-control']) }}
                                    @else
                                        @php($question_type = (isset($option['question_type']) && $option['question_type']?$option['question_type']:$question['question_type']))
                                        @if($question_type=='password')
                                            {{ Form::{$question_type}($inputName, [(isset($question['optional']) && $question['optional']?'optional':'checkme'), 'class' => 'form-control', 'id'=>'opt'.$option['id'], 'placeholder'=>(isset($option['placeholder']) && $option['placeholder']?$option['placeholder']:'')]) }}
                                        @else
                                            {{ Form::{$question_type}($inputName, (isset($option['set_value']) && $option['set_value']?$option['set_value']:''), [(isset($question['optional']) && $question['optional']?'optional':'checkme'), 'class' => 'form-control', 'id'=>'opt'.$cnt.$option['id'], 'placeholder'=>(isset($option['placeholder']) && $option['placeholder']?$option['placeholder']:''), 'onclick'=>(isset($option['onclick']) && $option['onclick']?$option['onclick']:'')]) }}
                                        @endif
                                    @endif
                                    @if($question['question_type']=='radio' || $question['question_type']=='checkbox')
                                       {{ $option['title'] }}
                                    @endif

                                    @if($question['question_type']=='radio' || $question['question_type']=='checkbox')
                                        </label></div>
                                    @endif

                                    @if($option['special'])
                                        @php($special_arr = explode("|", $option['special']))
                                        @if(View::exists('frontend.options.special.'.$special_arr[0]))
                                            @include('frontend.options.special.'.$special_arr[0])
                                        @endif
                                    @endif
                                @endforeach

                            @endif
                        @endif

                        @if(View::exists('frontend.options.custom.'.$question['id']))
                            @include('frontend.options.custom.'.$question['id'])
                        @endif

                    </div>
                @endif
            </div>
            @php $cnt++; @endphp
        @endforeach

            <div style="display: none" id="submit_progress" class="progress">
                <div class="bar progress-bar-success">Uploading...</div>
                <div class="percent">0%</div >
            </div>

        <ul class="pager wizard">
            <li class="previous btn-lg"><a href="javascript:;">Back</a></li>
            <li id="nextbtn" class="next btn-lg"><a href="javascript:;">Next</a></li>
            <li class="finish btn-lg"><a href="javascript:;">Finish</a></li>
        </ul>
    </div>
    {{ Form::hidden('service_id', (isset($services['id'])?$services['id']:0)) }}

    {{ Form::close() }}
</div>

@endsection


@section('before-scripts-end')
<script>
    var back = true;
    var disableForm = false;
    var wizard_proceed = false;

    $(document).ready(function() {

        $('#rootwizard').bootstrapWizard({withVisible:false,onPrevious:  function(tab, navigation, index) {
            $('#complete').hide();
            $('#nextbtn').show();
            wizard_proceed = false;
        }, onNext: function(tab, navigation, index) {

            $('#complete').hide();
            $('#nextbtn').show();

            if(checkinputs() == 1) {
                return false;
            }

            /*Auto fire function*/
            var tab_func = $('#tab'+(index-1)).attr('data-name');
            if (typeof window[tab_func] == 'function') {
                if (!wizard_proceed) {
                    window[tab_func]();
                    return false;
                }
            }

            if (index >= {{ count($services['questions'])-1 }} ) {
                $('#complete').show();
                $('#nextbtn').hide();
            }

            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.progress-bar').css({width:$percent+'%'});
            $('#rootwizard .progress-bar').html(Math.floor($percent)+ '% Complete');
        },
            onFinish: function() {
                if(!disableForm && !checkinputs()) {
                    back = false;

                    disableForm = true;

                    $('.finish a').hide();
                    $('.finish').append('<i class="fa fa-gear fa-spin fa-fw"></i>');

                    $('#WizardForm').submit();
                }
            }
        });

        $('#sModal').on('hidden.bs.modal', function () {
            back = false;
        })

        window.onbeforeunload = function() {
            if(back == true)
                return "Your project details will not be saved, are you sure?";
        };


        var bar = $('#submit_progress .bar');
        var percent = $('#submit_progress .percent');

        $('#WizardForm').ajaxForm({
            beforeSend: function() {
                $('#submit_progress').show();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function(resp) {
                $('#submit_progress').hide();
                if (resp.responseJSON.status == 'success') {
                    sweetAlert('Successful', 'Project was posted successfully, please wait to be redirected...', 'success');
                    $('#sModal').modal('hide');
                    setTimeout(function()
                    {
                        window.location.replace("/my-projects");
                    }, 2500);
                } else {
                    disableForm = false;
                    sweetAlert('Oops', resp.responseJSON.message, 'warning');

                    $('.finish a').show();
                    $('.finish .fa-gear').remove();
                }
            },
            error: function(){
                disableForm = false;
                sweetAlert('Oops', 'There was a problem sending in this request.', 'warning');

                $('.finish a').show();
                $('.finish .fa-gear').remove();
            }
        });

    });

    function checkinputs() {
        var unfilled = 0;
        $('#rootwizard input,#rootwizard textarea,#rootwizard select').filter('[checkme]:visible').each(function(){
            if (($(this).is(':radio') || $(this).is(':checkbox')) && !$('input[name="'+$(this).attr('name')+'"]:checked').val()) {
                sweetAlert('Please select a option');
                unfilled = 1;
            } else if(!$(this).val()) {
                sweetAlert('Please complete the question');
                unfilled = 1;
            } else if ($(this).is('#customer_email') && !isValidEmailAddress( $(this).val() )) {
                sweetAlert('Enter valid email');
                unfilled = 1;
            }
        });
        return unfilled;
    }

</script>
{{ Html::script("vendor/jquery-form/jquery.form.min.js") }}

@append