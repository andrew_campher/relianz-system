@if ($errors->any())
    <div class="main_alert alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <i class="alert_fa fa fa-warning"></i>
        @foreach ($errors->all() as $error)
            {!! $error !!}<br/>
        @endforeach
        <div class="clearfix"></div>
    </div>
@elseif (session()->get('flash_success'))
    <div class="main_alert alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <i class="alert_fa fa fa-thumbs-up"></i>
        @if(is_array(json_decode(session()->get('flash_success'), true)))
            {!! implode('', session()->pull('flash_success')->all(':message<br/>')) !!}
        @else
            {!! session()->pull('flash_success') !!}
        @endif
        <div class="clearfix"></div>
    </div>
@elseif (session()->get('flash_warning'))
    <div class="main_alert alert alert-warning">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <i class="alert_fa fa fa-warning"></i>
        @if(is_array(json_decode(session()->get('flash_warning'), true)))
            {!! implode('', session()->pull('flash_warning')->all(':message<br/>')) !!}
        @else
            {!! session()->pull('flash_warning') !!}
        @endif
        <div class="clearfix"></div>
    </div>
@elseif (session()->get('flash_info'))
    <div class="main_alert alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <i class="alert_fa fa fa-info-circle"></i>
        @if(is_array(json_decode(session()->get('flash_info'), true)))
            {!! implode('', session()->pull('flash_info')->all(':message<br/>')) !!}
        @else
            {!! session()->pull('flash_info') !!}
        @endif
        <div class="clearfix"></div>
    </div>
@elseif (session()->get('flash_danger'))
    <div class="main_alert alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <i class="alert_fa fa fa-warning"></i>
        @if(is_array(json_decode(session()->get('flash_danger'), true)))
            {!! implode('', session()->pull('flash_danger')->all(':message<br/>')) !!}
        @else
            {!! session()->pull('flash_danger') !!}
        @endif
        <div class="clearfix"></div>
    </div>
@elseif (session()->get('flash_message'))
    <div class="main_alert alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <i class="alert_fa fa fa-info-circle"></i>
        @if(is_array(json_decode(session()->get('flash_message'), true)))
            {!! implode('', session()->pull('flash_message')->all(':message<br/>')) !!}
        @else
            {!! session()->pull('flash_message') !!}
        @endif
        <div class="clearfix"></div>
    </div>
@endif