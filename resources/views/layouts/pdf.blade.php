<!DOCTYPE html>
<html lang="en">
<head>

    <title>@yield('title', app_name())</title>

<!-- Styles -->
@yield('before-styles-end')

{{ Html::style(elixir('css/frontend.css')) }}

</head>
<body style="background: #fff;">
@yield('content')

</body>
</html>
