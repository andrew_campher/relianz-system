@extends('layouts.pdf')

@section('content')
<div style="margin-top: 50px;padding: 20px;">
    <div class="">
    <section class="content content_content">
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="">
                    <table class="table">
                        <tr>
                            <td>
                                <h2 class=""><i class="fa fa-handshake-o"></i> {{ config('app.name') }}</h2>
                            </td>
                            <td>
                                <div class="text-right">
                                    <h2>INVOICE</h2>
                                    <b>Status: "{!! $invoice->getStatusKey($invoice->status) !!}"</b>
                                    Date: {{ $invoice->created_at->format(config('app.date.short')) }}

                                </div>
                            </td>
                        </tr>
                    </table>
                </div><!-- /.col -->
            </div>
            <!-- info row -->
            <table class="table invoice-info">
                <tr>
                    <td class="invoice-col">
                        From
                        <address>
                            <strong>
                                {{ config('app.company.name') }} T/A {{ config('app.name') }}<br>
                                {{ config('app.company.registration') }}
                            </strong><br>
                            {{ config('app.company.address') }}
                        </address>
                    </td><!-- /.col -->
                    <td class="invoice-col">
                        To
                        <address>
                            <h4>{{ $invoice->professional->title }}</h4>
                            <strong>{{ $invoice->professional->user->name }}</strong>
                            <br>
                            Address:
                            {{ $invoice->professional->area }}<br>
                            {{ $invoice->professional->user->email }}
                        </address>
                    </td><!-- /.col -->
                    <td class="invoice-col text-right">
                        <b>Invoice {{ config('app.invoice.prefix') }}{{ $invoice->id }}</b><br>
                        <br>
                        <b>Account:</b> ACC0{{ $invoice->account_id }}
                    </td><!-- /.col -->
                </tr>
            </table><!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class=" table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Qty</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Sub Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>{{ $invoice->description }}</td>
                            <td>{!!  Helper::formatPrice($invoice->amount) !!}</td>
                            <td>{!!  Helper::formatPrice($invoice->amount) !!}</td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- /.col -->
            </div><!-- /.row -->

            @if($invoice->status == 0)
            <div class="row">
                <!-- accepted payments column -->
                <div class="">
                    <h4 class="lead">Amount Due by {{ $invoice->created_at->format(config('app.date.short')) }}</h4>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>Total:</th>
                                <td>{!!  Helper::formatPrice($invoice->amount) !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->

                @endif
        </section>
    </section>
    </div>
</div>

@endsection