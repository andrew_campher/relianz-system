@extends('frontend.layouts.app')

@section ('title', $category->title.' Services')

@section('header')
    @include('public.includes.partials.header')
@endsection

@section('content')

    <h1 class="padding15">{{ $category->title }} Services</h1>

    <hr>

    <div class="panel-group cat_services" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach($category->subcategories AS $category)
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{ $category->id }}">
                    <h4 class="subcategory panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $category->id }}" aria-expanded="true" aria-controls="collapse{{ $category->id }}">
                            {{ $category->title }}
                        </a>
                    </h4>
                </div>
                <div id="collapse{{ $category->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $category->id }}">
                    <div class="panel-body">
                        <ul>
                            @foreach($category->services AS $service)
                                <li class="service_item"><a onclick="event.preventDefault(); fireModal('{{ route('public.project.wizard') }}?service={{ $service->id }}')" href="{{ route('public.service.show', $service->slug) }}">{{ $service->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
        @endforeach

    </div>



@stop