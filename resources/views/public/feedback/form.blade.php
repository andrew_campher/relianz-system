@extends ('frontend.layouts.app')

@section ('title', 'Feedback Form')

@section('content')

    <h1>
        Give us your feedback
    </h1>

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <p>Please give us your feedback below</p>

            <div class="form-group">
                {{ Form::label('comments', 'Comments', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::textarea('comments', null, ['class' => 'form-control', 'rows'=>'4', 'placeholder' => 'How was your experience?', 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('rating_overall', trans('labels.project_review.rating_overall_label'), ['class' => 'col-lg-2 control-label']) }}
                <div class="col-lg-10">
                    {{ Form::select('rating_overall', [1 => 'Bad', 2 => 'Not so Good', 3 => 'Average', 4 => 'Good', 5 => 'Excellent'], null, ['class' => 'form-control barrating','placeholder' => 'Please select rating', 'required', 'data-validate'=>'true']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->


            @if (!access()->id() && config('access.captcha.registration'))
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {!! Form::captcha(null, ['data-callback'=> 'enableBtn']) !!}
                        {{ Form::hidden('captcha_status', 'true') }}
                    </div><!--col-md-6-->
                </div><!--form-group-->
            @endif

        </div><!-- /.box-body -->
    </div><!--box-->
@stop


@section('after-scripts-end')

    @if (config('access.captcha.registration'))
        {!! Captcha::script() !!}
    @endif

    <script type="text/javascript">
        $(function() {
            $('.barrating').barrating({
                theme: 'fontawesome-stars-o',
                showSelectedRating: true
            });
        });
    </script>
@append

