@extends('frontend.layouts.app')

@section ('title', 'Contact '.config('app.name'))

@section('header')
    @php($mini_header = 1)
    @include('public.includes.partials.header')

@endsection

@section('content')
<h1>Contact {{ config('app.name') }}</h1>

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <h3>{{ env('COMPANY_NAME') }}</h3>
                    {{ env('COMPANY_STREET') }}<br/>
                    {{ env('COMPANY_SUB') }}<br/>
                    {{ env('COMPANY_CITY') }}<br/>
                    {{ env('COMPANY_PROVINCE') }}

                    <hr>

                    <h3>Live Chat</h3>
                    <p>Mondays - Fridays 8am - 5pm</p>
                    <a class="h6 label label-success" href="javascript:void(Tawk_API.toggle())"><i class="fa fa-comment"></i> Click to Chat </a>

                    <h3>Phone / Email</h3>
                    <p><i class="fa fa-phone"></i> 021 917 1719</p>
                    <a class="" target="_blank" href="mailto:connect@handyhire.co.za"><i class="fa fa-envelope"></i> connect@handyhire.co.za</a>
                </div>
                <div class="col-xs-12 col-sm-8">

                    <h3>Send us a message</h3>

                    {{ Form::open(['route' => 'public.page.contact_submit', 'class' => 'form-horizontal', 'novalidate', 'data-toggle' => 'validator', 'role' => 'form', 'method' => 'post']) }}

                    <div class="form-group">
                        {{ Form::label('name', 'Your Name', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('name', 'name', null, ['class' => 'form-control', 'placeholder' => 'Your Name', 'required']) }}
                            <div class="help-block with-errors"></div>
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => 'Your Email', 'required']) }}
                            <div class="help-block with-errors"></div>
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('tel', 'Contact Number', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('tel', 'tel', null, ['class' => 'form-control', 'placeholder' => 'Contact Number']) }}
                            <div class="help-block with-errors"></div>
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('message', 'Message', ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-6">
                            {{ Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Message', 'required']) }}
                            <div class="help-block with-errors"></div>
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::captcha(null, ['data-callback'=> 'enableBtn']) !!}
                            {{ Form::hidden('captcha_status', 'true') }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">

                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}

                        </div><!--col-md-6-->
                    </div><!-- /.box-body -->

                    {{ Form::close() }}
                </div>
            </div>



        </div>
    </div>


@endsection

@section('after-scripts-end')
    @if (config('access.captcha.registration'))
        {!! Captcha::script() !!}
    @endif

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/58bcf6f25b8fe5150eec12ab/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
@append
