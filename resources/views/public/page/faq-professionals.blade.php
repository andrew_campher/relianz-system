@extends('frontend.layouts.app')

@section ('title', 'FAQ')

@section('content')
<h1>FAQ for Pro's</h1>


@php
    $faqs = [
        ['title' => 'What is HandyHire?', 'answer' => 'HandyHire is a platform that makes it easy to get quotes from professionals for anything that needs to be done. Customers are able to create a Quote Request for any service they need, professionals matching these services are notified and asked to quote.'],
        ['title' => 'How do you have my details?', 'answer' => 'If you have never heard of HandyHire before and have received a Quote Request email don\'t be alarmed. When customers post a job we find professionals near them offering their requested service. It\'s a good sign to be found online for a particular service.'],
        ['title' => 'Why no customer contact details in Request?', 'answer' => 'The first response to a customer is done on the HandyHire website. This gives you a opportunity to present your business and offer to the customer. Customers are able to review your business profile and offer and if happy may respond with their direct contact information.'],
        ['title' => 'How to respond to Customer?', 'answer' => 'Before responding you would need to confirm a few details of your business. You are then able to respond by completing the Quote form. Customers are notified via SMS and email of this new pending Quote.'],
        ['title' => 'I am unable to quote a price?', 'answer' => 'It is always preferred to respond to the customer’s request with a rough estimate but if you are not able to then you may select the “Need more information” option on the quote form. Once the customer responds with contact details you are able to proceed to setup an appointment or ask the questions you need.'],
        ['title' => 'How much does it cost?', 'answer' => 'Our model is to charge a set amount of credits per lead. A credit will have a rand value. Eg. if a credit costs R3 and it requires 2 credits to respond to a quote request then you be paying R6 for that lead.'],
        ['title' => 'Are leads refundable?', 'answer' => 'Yes, if a customer does not open your quote within 48 hours then we will refund you the credits spent on that lead. You may also apply for a refund for fake or illegitimate leads.'],
    ];
@endphp

<div class="jumbotron">
    <div class="jumbotron-contents">

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <h3 class="no-top-margin">Common Questions</h3>


        @foreach($faqs AS $key => $faq)
            <div class="panel">
                <div class="panel-heading" role="tab" id="heading{{ $key }}">
                    <div class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" aria-expanded="true" aria-controls="collapse{{ $key }}">
                            <i class="fa fa-question-circle"></i> {{ $faq['title'] }}
                        </a>
                    </div>
                </div>
                <div id="collapse{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $key }}">
                    <div class="panel-body">
                        {{ $faq['answer'] }}
                    </div>
                </div>
            </div>

        @endforeach

        <hr>

        <h3>Want to signup?</h3>
        <p><a href="{{ route('frontend.auth.register_professional') }}">Click here</a> to get your business added.</p>

        <h3>Contact us</h3>
        <p>If you have further questions please don't hesitate to contact us.</p>

    </div>
</div>


@endsection
