@extends('frontend.layouts.app')

@section ('title', 'FAQ')

@section('header')
    @include('public.includes.partials.header')
@endsection

@section('content')
<h1>User FAQ</h1>

@php
    $faqs = [
        ['title' => 'What is HandyHire?', 'answer' => 'HandyHire is a platform that makes it easy for you to get quotes from professionals for anything you need done. You are able to create a Quote Request for any service needed, professionals matching those services are notified and asked to send you a quote.'],
        ['title' => 'Does it cost me anything?', 'answer' => 'NO! Its complete FREE for you to use.'],
        ['title' => 'How do I request quotes?', 'answer' => 'Use the search input box above to start entering what you need done.']
    ];
@endphp

<div class="jumbotron">
    <div class="jumbotron-contents">

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <h3 class="no-top-margin">Common Questions</h3>


            @foreach($faqs AS $key => $faq)
                <div class="panel">
                    <div class="panel-heading" role="tab" id="heading{{ $key }}">
                        <div class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" aria-expanded="true" aria-controls="collapse{{ $key }}">
                                <i class="fa fa-question-circle"></i> {{ $faq['title'] }}
                            </a>
                        </div>
                    </div>
                    <div id="collapse{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $key }}">
                        <div class="panel-body">
                            {{ $faq['answer'] }}
                        </div>
                    </div>
                </div>

            @endforeach

            <hr>

            <h3>Contact us</h3>
            <p>If you have further questions please don't hesitate to contact us.</p>

        </div>
    </div>


@endsection
