@extends('frontend.layouts.app')

@section ('title', 'FAQ')

@section('content')
<h1 >FAQ</h1>
<p>Allow us to answer some of your pressing questions.</p>

<div class="jumbotron">
    <div class="jumbotron-contents text-center">
        <div class="row">
            <div class="col-sm-6">
                <div><img src="{{ asset('img/pro-laptop.jpg') }}"></div>
                <h1><a href="{{ route('public.page', 'faq-professionals') }}">Professional FAQ</a></h1>
            </div>
            <div class="col-sm-6">
                <div><img src="{{ asset('img/typing-laptop.jpg') }}"></div>
                <h1><a href="{{ route('public.page', 'faq-user') }}">Customer FAQ</a></h1>
            </div>
        </div>
    </div>
</div>


@endsection
