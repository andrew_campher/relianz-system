@extends('frontend.layouts.app')

@section ('title', 'Join as a Professional on '.config('app.name'))

@section('content')
    <h1>🙋 Join as a Pro</h1>
    <div class="h3">Get requests for your service.
        <div class="pull-right">
            <a class="btn btn-lg btn-success" href="{{ route('frontend.auth.register_professional') }}">SIGN ME UP <i class="fa fa-thumbs-up"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="jumbotron">
        <div class="jumbotron-contents">


            <div class="row padding15 text-center">
                <div class="col-xs-12">

                    <h1 class="padding15">How <i class="fa fa-handshake-o"></i> {{ config('app.name') }} works</h1>

                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="text-center"><img style="width: 100%" src="{{ asset('img/request_service.jpg') }}"></div>
                    <div class="h2 text-primary text-bold">1. See quote requests from customers in your area</div>
                    <div>We show you customers in your area looking for your services. We will also notify you of any new requests as they come in.</div>
                </div><!-- col-md-10 -->

                <div class="col-xs-12 col-sm-4">
                    <div class="text-center"><img style="width: 100%" src="{{ asset('img/give_quote.jpg') }}"></div>

                    <div class="h2 text-primary text-bold">2. Pay to quote</div>

                    @if(!config('app.settings.enable_credits'))
                        <div class="alert alert-success text-bold">
                            BETA LAUNCH SPECIAL: All quotes are FREE till end of March 2017
                        </div>
                    @endif

                    <div>You only pay to send a quote to the request of your choice.</div>
                </div><!-- col-md-10 -->

                <div class="col-xs-12 col-sm-4">
                    <div class="text-center"><img style="width: 100%" src="{{ asset('img/pro-laptop.jpg') }}"></div>
                    <div class="h2 text-primary text-bold">3. Get hired</div>
                    <div>Customers receive up to {{ config('app.settings.quote_limit') }} quotes. This gives you the best chance of winning their business.</div>

                </div><!-- col-md-10 -->

            </div><!--row-->
        </div>
    </div>
    <div class="pull-left h4">
        <i class="fa fa-question-circle"></i> Still have questions? <a class="" href="{{ route('public.page', 'faq-professionals') }}">Click here for FAQ </a>
    </div>
    <div class="pull-right">
        <a class="btn btn-lg btn-success" href="{{ route('frontend.auth.register_professional') }}">SIGN ME UP <i class="fa fa-thumbs-up"></i></a>
    </div>
    <div class="clearfix"></div>


@endsection
