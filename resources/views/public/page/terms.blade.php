@extends('frontend.layouts.app')

@section ('title', 'Terms & Conditions')

@section('sidebar')
    <h4>Sections</h4>
    <ul class="sidebar-menu list-group">
        <li class="list-group-item"><a href="#user_agreement">User Agreement</a></li>
        <li class="list-group-item"><a href="#provider_agreement">Provider Agreement</a></li>
        <li class="list-group-item"><a href="#privacy_policy">Privacy Policy</a></li>
        <li class="list-group-item"><a href="#refund_policy">Code of Conduct</a></li>
    </ul>
@endsection

@section('content')
<h1>Terms & Conditions</h1>


<div class="jumbotron">
    <div class="jumbotron-contents">

        <div class="h5">Viewing Terms and Conditions version 3: Effective 1 March 2017</div>

        @include('public.page.terms.user_agreement')

        <hr />

        @include('public.page.terms.provider_agreement')

        <hr />

        @include('public.page.terms.privacy_policy')

        <hr />

        @include('public.page.terms.code_conduct')

        <hr />

        @include('public.page.terms.refund_policy')
    </div>
</div>

@endsection
