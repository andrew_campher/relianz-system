<h3 id="code_of_conduct">HandyHire Terms and Conditions - Code of Conduct</h3>

<div>
    <h4>Identification</h4>
    <ul>
        <li>
            <p>Be fully identifiable to customers through the use of name badges, business cards, and (where applicable) wearing company uniform</p>
        </li>
        <li>
            <p>Make available to customers this code of conduct, and information as to how to contact HandyHire if any concerns arise</p>
        </li>
    </ul>
    <h4>Knowledge of processes</h4>
    <ul>
        <li>
            <p>Have an up to date working knowledge of how to best complete the work accepted</p>
        </li>
        <li>
            <p>Have full knowledge of the process through which customers can make complaints</p>
        </li>
        <li>
            <p>Understand how customers make payment to service providers, and how receipts are issued</p>
        </li>
        <li>
            <p>Ensure that they have full comprehension of any other policy or process that HandyHire adopts</p>
        </li>
    </ul>
    <h4>Approach to work</h4>
    <ul>
        <li>
            <p>Preserve the HandyHire reputation and brand image at all times, wherever service providers are</p>
        </li>
        <li>
            <p>Maintain a professional dress standard at all times</p>
        </li>
        <li>
            <p>Maintain their tools, equipment and work environment to a professional level at all times</p>
        </li>
        <li>
            <p>Take ownership of their dealings with customers by trying at all times to resolve any issues in the first instance, and by following through when the issue is escalated to a next level</p>
        </li>
        <li>
            <p>Be courteous and efficient at all times when providing service to customers</p>
        </li>
        <li>
            <p>Respect the need to maintain confidentiality and security</p>
        </li>
        <li>
            <p>Endeavour to work towards best practice in everything they do by providing service excellence training programs and continually benchmarking</p>
        </li>
        <li>
            <p>Be punctual at all times</p>
        </li>
        <li>
            <p>Take due care and show consideration for the safety and security of their co-workers, customers, and themselves</p>
        </li>
        <li>
            <p>Freely discuss ideas they have for business improvements</p>
        </li>
        <li>
            <p>Ensure that their work environment is free from discrimination and harassment and that all people are treated fairly</p>
        </li>
        <li>
            <p>Air any grievances they have through the correct channels</p>
        </li>
        <li>
            <p>Provide and accept constructive feedback in their daily work practices</p>
        </li>
    </ul>

</div>

<hr />