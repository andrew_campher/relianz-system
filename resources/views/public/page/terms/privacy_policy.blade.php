<h3 id="privacy_policy">HandyHire Terms and Conditions - Privacy Policy</h3>

<div>
    <p>HandyHire is committed to protecting your privacy. HandyHire recognises that users of <a href="https://HandyHire.co.za">www.HandyHire.co.za</a> place ongoing trust in our services, and we take your privacy seriously. HandyHire does not collect personally identifying information except when you specifically and knowingly provide it subject to the exceptions set out below.</p>
    <p>HandyHire provides customer feedback, message boards and other services for both public and private forums. Please remember that any information that is disclosed in public forums becomes public information. You should exercise caution when deciding to disclose your personal information.</p>
    <p>If you have queries, concerns or would like to revise your personal records maintained by HandyHire , please email us at contact us</p>

    <h4>What information do we collect?</h4>
    <p>HandyHire collects information in two ways; automatically when you log onto our web servers (<a href="https://handyhire.co.za">www.handyhire.co.za</a>) and when you provide your details through an HandyHire online form.</p>
    <p>Web servers: For each visitor to our web page, our web server may automatically recognise the users domain, IP address and sometimes an email address.</p>
    <p>Online forms: HandyHire has introduced a login service, designed to offer users a more rewarding, efficient and personalised experience at HandyHire web site. You are required to login.</p>
    <p>HandyHire users are required to provide a valid email address and other personal details as a condition of registration. HandyHire users will be placed on our mailing list only by their explicit request.</p>
    <h4>Why do we require your personal information?</h4>
    <p>The information we collect is used to continually improve your site experience, by customising the content and web layout according to your preferences indicated by your use of the site.</p>
    <p>HandyHire needs to collect, use and disclose the information required on online forms to provide you with the services to which the online forms relate. Your information may also be used by HandyHire to tell you about useful products, discounts, special offers, competitions and invitations to special events. HandyHire may notify you about updates to our web site. If you do not want to receive this material, please email HandyHire at contact us.</p>
    <h4>Disclosure of personal information.</h4>
    <p>To provide you with services or information you request from the web site by filling out an online form, HandyHire may need to disclose your information to our business partners and contracted service providers.</p>
    <p>In submitting your details, you should be mindful that information published on the Internet is available to everyone using the Internet. You should not provide this information if you do not want it to be publicly available.</p>
    <h4>In addition, HandyHire may disclose personally identifying information if HandyHire determines that disclosure of such information is:</h4>
    <ul>
        <li>
            <p>a) required by law; and</p>
        </li>
        <li>
            <p>b) reasonably necessary to protect the rights, property, or safety of HandyHire , its users, or other persons or entities.</p>
        </li>
    </ul>
    <p>Under the foregoing circumstances, HandyHire may disclose personally identifying information without first notifying you or obtaining your consent.</p>
    <h4>Contacting us about access and correction of your personal details</h4>
    <p>HandyHire aims to ensure your personal details are up-to-date. If you have queries, concerns or would like to access or revise your personal records maintained by HandyHire , please change your details using My Toolbox or email HandyHire on Contact Us.</p>
    <h4>HandyHire shares non-personal aggregate, statistical information</h4>
    <p>HandyHire may share non-personal aggregate, or summary, information about our visitors with partners or other third parties, which is a customary practice on the Internet. For example, we might provide a count of our users from a particular area code.</p>
    <p>HandyHire uses market research to monitor traffic to and across its web site. This technology involves inserting special scripting in the code of each web page. This code is downloaded by you when you visit each page and then sends information about your use of the page, eg. the duration that the page was open. Information collected using this technology does not identify you personally in any way and is only used for compiling aggregate statistics of web site use for HandyHire.</p>
    <h4>HandyHire uses "cookies" to improve your experience</h4>
    <p>HandyHire uses a standard Web browser feature called "cookies" to help us improve your experience. A cookie is a small text file that many web sites write through your browser when you visit them. A cookie can only be read by the site that places it, so HandyHire web site cannot "see where you've been" based on any other cookies in your browser. Netscape Navigator uses a single file called "cookies.txt" to record all cookies, and you can open this file in any text editor to see what it looks like.</p>
    <p>On some areas of <a href="https://handyhire.co.za">www.handyhire.co.za</a> cookies are used to distinguish you from other users. However, cookies merely provide us with a unique anonymous number that we can use in identifying -- and enhancing -- the use of sections of our site.</p>
    <p>Cookies can also be used to store your user name and encrypted password, if you so choose. You may choose to disable cookies in your browser or use security software to prevent the storage of cookies. However if you disable cookies, we may not be able to fulfil your request or provide you with an appropriate level of service in some areas of the HandyHire web site. Below are instructions to help you disable cookies if you use Internet Explorer or Netscape Navigator. If these options don't apply to you, Contact Us for further assistance.</p>
    <p>If you use Internet Explorer:</p>
    <ul>
        <li>
            <p>Select Tools -&gt; Internet Options from the menu.</p>
        </li>
        <li>
            <p>Select the "Security" tab</p>
        </li>
        <li>
            <p>Click the "Custom Level..." button</p>
        </li>
        <li>
            <p>In the "Cookies" section make sure that "Allow per-session cookies (not stored)" is set to "Disable"</p>
        </li>
        <li>
            <p>Confirm the changes by clicking OK</p>
        </li>
        <li>
            <p>Answer "Yes" to the following dialog box</p>
        </li>
        <li>
            <p>Close and save your changes by clicking "OK" on the "Internet Options" dialog box.</p>
        </li>
    </ul>
    <p>If you use Netscape Navigator:</p>
    <ul>
        <li>
            <p>Select Edit -&gt; Preferences from the menu.</p>
        </li>
        <li>
            <p>Select "Advanced" from the menu on the left hand side</p>
        </li>
        <li>
            <p>In the "Cookies" section select "Disable cookies"</p>
        </li>
        <li>
            <p>Close the dialog and save your changes by clicking "OK"</p>
        </li>
    </ul>
    <h4 >Third party information and links</h4>
    <p >Some sections of <a href="https://handyhire.co.za">www.handyhire.co.za</a> may be powered by our business partners and service providers, to bring you the most relevant and up-to-date information. As a result, these companies have access to information such as your domain, IP address and clickstream information. HandyHire will take reasonable steps to protect the privacy of any personally identifying information that these companies have access to.</p>
    <p>HandyHire may also use agencies to place advertising on our web pages. These agencies may collect information such as your domain, IP address and clickstream information. HandyHire will take reasonable steps to protect the privacy of any personally identifying information that these agencies have access to.</p>
    <p>HandyHire may present advertisements or links to partner companies. When you click on these links or enter information, you may be transferred to an advertiser's or partner's Web site. HandyHire has no control over the privacy practices or information that these sites may request of you. We are not and cannot be held responsible for the privacy practices or content of these sites.</p>
    <p>If you order catalogues from HandyHire third party advertisers or partners, any such order will indicate your consent to receive from them other catalogues, announcements, product news, newsletters, and/or other information, via shipping methods including but not limited to South African Post, overnight ground/air courier, or other electronic means, that may or may not be of interest to you. You will need to contact them directly to 'opt-out' of receiving this material.</p>
    <h4>Storage and security of your personal information</h4>
    <p>HandyHire takes all reasonable steps to secure your personal details. HandyHire stores your information on secure servers which are housed in controlled environments to protect against loss, misuse or alteration of your information collected at the HandyHire web site. All HandyHire employees and service providers are contractually obligated to respect the confidentiality of your personal details.</p>
    <p>To assist in keeping your data secure, we recommend that you keep your HandyHire password and account details secure. Please contact us Contact Us if you require a change of password and username.</p>
    <h4>Changes to this Policy</h4>
    <p>HandyHire is committed to bringing you the most up-to-date and relevant information online. As a result our web site is constantly changing and there may be a requirement to review our Privacy Policy. We will promptly post any changes on our web site. If you are concerned about your privacy, you should check our privacy policy periodically.</p>

</div>

<hr />