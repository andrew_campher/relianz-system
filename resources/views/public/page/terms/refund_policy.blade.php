<h3 id="refund_policy">HandyHire Terms and Conditions - Refund Policy</h3>

<div>
    <h4>Refund and Reversal Policy</h4>
    <p>This refund policy applies only to payments made through the payment gateway offered on <a href="https://handyhire.co.za">www.handyhire.co.za</a></p>
    <p>For all other policies regarding refunds please view the Terms and Conditions.</p>
    <p>This document does not replace any part of the Terms and Conditions.</p>
    <h4>Cancellation of Account</h4>
    <p>In accordance with HandyHire's Terms and Conditions an account may be cancelled.</p>
    <p>When an account is cancelled by HandyHire there is no refund of registration or unused Provider Credits.</p>
    <h4>Suspension of Account</h4>
    <p>If an account is suspended either by the Provider or HandyHire, in accordance with HandyHire's Terms and Conditions there is no refund of Registration or unused Provider Credits</p>
    <h4>Mistaken payment</h4>
    <p>If you feel you have made a mistake in payment contact HandyHire immediately.</p>
    <p>Mistaken payments may include, but are not limited to, double payments, over payment, and payments from incorrect cards.</p>
    <p>In the instance that you have made a mistake to a payment, HandyHire reserves the right to return funds, and if a refund is agreed they will be applied back directly to the Credit Card used in the initial purchase.</p>
    <h4>Transfer of Funds</h4>
    <p>All Transfers of unused Provider Credits are at the discretion of HandyHire and will only be allowed where the accounts two HandyHire accounts involved in the transfer are both owned by the same parent company.</p>
    <h4>Proof of Purchase</h4>
    <p>HandyHire may require proof of purchase before any refund/reimbursement is agreed.</p>
    <h4>Method of Refund</h4>
    <p>Where a refund/reimbursement has been agreed and the initial payment was made via Credit Card that payment will be refunded/reimbursed back to the original credit card.</p>
    <p>Where payment was made by another form, HandyHire may make payment by Cheque or Electronic Funds Transfer to a nominated bank account.</p>
    <p>HandyHire reserves the right to nominate how it would prefer to make payment.</p>


</div>