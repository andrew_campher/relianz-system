<h3 id="user_agreement">HandyHire Terms and Conditions - User Agreement</h3>

<div>
    <p>BETWEEN: One Internet Media CC - 2008/162526/23 ("HandyHire") and a party that uses the System and/or the Site ("User").</p>
    <p>By using the System and/or Site you the User agree to be bound by the terms of this Agreement with HandyHire.</p>
    <h4>1. User Eligibility</h4>
    <p>User acknowledges and agrees that the System is available only to persons aged 18 years old and over. User represents and warrants that he/she is 18 years old or over.</p>
    <h4>2. HandyHire's Role</h4>
    <p>The System operates as both a finder and facilitator seeking to match the needs of Users with Providers. HandyHire is as a neutral facilitator and the User acknowledges and agrees it is not directly involved in the transactions between Users and Providers.</p>
    <h4>3. Feedback feature of Site</h4>
    <p>3.1 User acknowledges and agrees that HandyHire operates a user feedback system, on which User may post comments, compliments, criticisms and other observations regarding Providers on the Site that are honest, fair and reasonable and which can be substantiated.</p>
    <p>3.2 User acknowledges and agrees that any information or material submitted by it to the Site for inclusion in the feedback system is and will be treated by HandyHire as non confidential and non-proprietary and it may use such material without restriction.</p>
    <p>3.3 User must not post or transmit any material on the Site which is offensive, defamatory, obscene, unlawful, vulgar, harmful, threatening, abusive, harassing or otherwise reasonably objectionable.</p>
    <p>3.4 User agrees not to disrupt the flow of dialogue or otherwise act in a manner which negatively effects other users of the Site.</p>
    <p>3.5 User agrees not to impersonate any other person when posting material to the Site.</p>
    <p>3.6 User acknowledges that any material it posts to the Site may be removed by HandyHire from the Site without notice at any time.</p>
    <p>3.7 HandyHire does not warrant to the User that it or Providers will respond to questions or comments submitted by User to the Site.</p>
    <h4>4. User Warranties</h4>
    <p>4.1 Without limiting clause 3, User agrees that it will:</p>
    <ul>
        <li>
            <p>(a) not use the Site for illegal purposes including without limitation by posting to the Site information encouraging conduct that would constitute a criminal offence;</p>
        </li>
        <li>
            <p>(b) not use any feature of the Site to send unsolicited commercial emails to Users, whether individually or as a group;</p>
        </li>
        <li>
            <p>(c) not copy, translate, reproduce, communicate to the public, adapt, vary or modify anything on the Site without HandyHire's prior written consent;</p>
        </li>
        <li>
            <p>(d) not use anything on this Site for or in connection with any business or enterprise (whether for profit or otherwise) and in particular in any enterprise of a service which is in competition with HandyHire;</p>
        </li>
        <li>
            <p>(e) not post to the Site or otherwise propagate material of any kind which contains computer worms, viruses or other types of malicious or harmful programs;</p>
        </li>
        <li>
            <p>(f) not take any action that imposes an unreasonable or disproportionately large load on HandyHire's infrastructure;</p>
        </li>
        <li>
            <p>(g) not damage, modify, interfere with, disrupt or destroy the files, data, passwords, devices or resources that belong to HandyHire or do anything that compromises the security of the Site;</p>
        </li>
        <li>
            <p>(h) not use the Site to engage in misleading or deceptive on-line marketing practices;</p>
        </li>
        <li>
            <p>(i) not use the Site to transmit junk mail, spam, chain letters or engage in other flooding techniques or mass distribution of unsolicited email; or</p>
        </li>
        <li>
            <p>(j) provide HandyHire with complete and accurate contact details when using the System enabling Providers to contact User as requested.</p>
        </li>
    </ul>
    <h4>5. HandyHire's Reservation of Rights</h4>
    <p>5.1 HandyHire reserves the right to:</p>
    <ul>
        <li>
            <p>(a) remove or refuse to post any information and/or materials (in whole or in part) that it, in its sole and absolute discretion, regards in any way objectionable or in violation of any applicable law or this Agreement without notice to User; and</p>
        </li>
        <li>
            <p>(b) modify or discontinue any services HandyHire offers at any time without notice or liability to User.</p>
        </li>
    </ul>
    <h4>6. Intellectual Property</h4>
    <p>6.1 All intellectual property rights in information, data and materials used or appearing on the Site including (without limitation) all software, tools, know-how, equipment or processes, trade marks, logos and other materials shall remain the sole and exclusive property of HandyHire or its licensors. User acknowledges and agrees that it shall not acquire any rights, title or interest in or to any of HandyHire's intellectual property rights.</p>
    <p>6.2 To the extent that User submits any information or material to HandyHire relating to a Quote Request or for otherwise posting on the Site, User automatically grants HandyHire a licence to use the information or material for the purpose for which it is provided.</p>
    <h4>7. Disclaimer and Acknowledgments</h4>
    <p>7.1 HandyHire provides the System and Provider details on an 'as is' basis and without any warranty, express, implied or statutory as to the quality, efficacy, reliability or other attribute or characteristic of any Provider.</p>
    <p>7.2 To the fullest extent legally possible, HandyHire excludes any representation or warranty concerning the System, the Site or any third party Site referred to or connected via the Site.</p>
    <p>7.3 Without limiting the foregoing, User acknowledges that:</p>
    <ul>
        <li>
            <p>(a) HandyHire does not check the truth or currency of the material or information that Providers provide or make available to it;</p>
        </li>
        <li>
            <p>(b) HandyHire does not control, endorse, approve or warrant to the User the merchantability or fitness for any particular purpose of any of the goods or services of third parties (including Providers) referred to at the Site or whose identities become known to User as a result of using the System or otherwise;</p>
        </li>
        <li>
            <p>(c) HandyHire does not warrant to the User that anything on the Site or any third-party Site referred to or connected via the Site is accurate, complete or up to date and makes no performance warranty whatsoever concerning anything on or implied from them;</p>
        </li>
        <li>
            <p>(d) User should satisfy itself through its own enquiries as to the condition or suitability of any Provider, Provider Services or any other goods and/or services supplied, offered by or recommended by on behalf of a Provider. User agrees to consult professionals for advice that is specifically tailored to User's particular circumstances;</p>
        </li>
        <li>
            <p>(e) HandyHire does not offer professional advice on the fitness of any goods, services or information supplied by any third parties including Providers;</p>
        </li>
        <li>
            <p>(f) HandyHire does not endorse or recommend any Provider, goods or services, including where details of a Provider are provided by HandyHire to User or otherwise become known using the System;</p>
        </li>
        <li>
            <p>(g) HandyHire is not a party to any transactions between User and Providers and User (and not HandyHire) is responsible for paying Provider for all Projects;</p>
        </li>
        <li>
            <p>(h) the User is responsible for completing all transactions it participates in (including monitoring the status and complying with all relevant legal obligations);</p>
        </li>
        <li>
            <p>(i) HandyHire does not warrant to the User that the Site or any information or communication it provides is or will be reliable, timely, error or fault free, complete or accurate;</p>
        </li>
        <li>
            <p>(j) the Site may experience interruptions and access difficulties from time to time and that HandyHire will not be responsible for such interruptions or access difficulties; and</p>
        </li>
        <li>
            <p>(k) HandyHire shall not be liable for any loss or damage whether directly or indirectly incurred by User or any third person as a result of any failure or delay in HandyHire doing any thing, including, but not limited to transmitting any information to User or removing any information from the Site or any Provider from the System.</p>
        </li>
    </ul>
    <p>7.4 User agrees that HandyHire and all affiliates and related entities of HandyHire have no responsibility for the legality of the actions of other users.</p>
    <h4>8. Limitation of Liability</h4>
    <p>8.1 To the extent permitted by law HandyHire excludes all liability to the User or any other person for any loss claim or damage (whether arising in contract, negligence, tort, equity or otherwise for any loss, whether it be consequential, indirect, incidental, special, punitive, exemplary or otherwise, including, without limitation any loss of profits, loss or corruption of data or loss of or damages to reputation or goodwill) arising out of or in connection with any:</p>
    <ul>
        <li>
            <p>(a) removal or termination of User's access to the System and Site; or</p>
        </li>
        <li>
            <p>(b) use of the Site or the System or information on or provided through the Site or the System or any information or advice otherwise provided by HandyHire or a Provider, even if HandyHire has been advised of the possibility of such loss or damage.</p>
        </li>
    </ul>
    <p>8.2 To the extent permitted by law, HandyHire and all affiliates and related entities of HandyHire expressly limit their liability for breach of a non-excludable condition or warranty implied by virtue of any legislation to the following remedies (the choice of which is to be at HandyHire's sole discretion):</p>
    <ul>
        <li>
            <p>(a) in the case of goods, any one or more of the following:</p>
            <ul>
                <li>
                    <p>(i) the replacement of the goods or the supply of equivalent goods;</p>
                </li>
                <li>
                    <p>(ii) the repair of the goods;</p>
                </li>
                <li>
                    <p>(iii) the payment of the cost of replacing the goods or of acquiring equivalent goods; or</p>
                </li>
                <li>
                    <p>(iv) the payment of the cost of having the goods repaired; and</p>
                </li>
            </ul>
        </li>
        <li>
            <p>(b) in the case of services and:</p>
            <ul>
                <li>
                    <p>(i) the supply of the services again; or</p>
                </li>
                <li>
                    <p>(ii) the payment of the cost of having the services supplied again.</p>
                </li>
            </ul>
        </li>
    </ul>
    <h4>9. Indemnity</h4>
    <p>User indemnifies and holds HandyHire and its parent, subsidiaries, affiliates, officers, directors, agents, employees, partners, suppliers, franchisors and franchisees, harmless from and against any claims, demands, proceedings, losses and damages (whether actual, special and consequential of every kind and nature including all legal fees) made by User or a Provider arising out of or in any way related to Users' use of the Site or its breach of this Agreement, or User's violation of any law or the rights of a third party.</p>
    <h4>10. Breach</h4>
    <p>10.1 Without limiting other remedies available to HandyHire at law, in equity or under this Agreement, HandyHire may, in its sole discretion, immediately issue a warning, temporarily suspend, indefinitely suspend or terminate User's access to the System and refuse to provide services to User if:</p>
    <ul>
        <li>
            <p>(a) User breaches any of this Agreement, the Privacy Policy or the terms and policies those documents incorporate by reference;</p>
        </li>
        <li>
            <p>(b) HandyHire is unable to verify or authenticate any information the User provides to it; or</p>
        </li>
        <li>
            <p>(c) HandyHire believes that User's actions may cause damage and/or legal liability for HandyHire, Provider or other users.</p>
        </li>
    </ul>
    <h4>11. Termination</h4>
    <p>This Agreement is effective until terminated by HandyHire, and HandyHire may terminate it and User's right to post information and material to the Site at any time without prior notice.</p>
    <h4>12. No Agency</h4>
    <p>User acknowledges and agrees that HandyHire is not the User's agent and that the User is free to accept or reject its appointment of a Provider at its sole discretion.</p>
    <h4>13. Circumstances beyond HandyHire's control</h4>
    <p>HandyHire will not be liable for any failure or delay in the performance of its obligations to the User if that failure or delay is due to circumstances beyond our reasonable control including, without limitation, any act of God or other cause beyond its reasonable control including any mechanical, electronic, communications or third party supplier failure.</p>
    <h4>14. Use of Material on Site</h4>
    <p>14.1 Users agree that information contained on this Site is for personal use only and may not be sold, used or redistributed for any other purpose.</p>
    <p>14.2 Users may not use screen scraping, data mining or any similar data gathering and extraction technological devices on this Site for the purpose of reproducing information contained on this Site on or through any other medium, except with HandyHire's prior written consent.</p>
    <h4>15. Website Links</h4>
    <p>15.1 HandyHire may provide links and pointers to websites maintained by third parties from its Site which websites are not under the control of HandyHire and HandyHire is not responsible for the contents of any linked website or any website link contained in any linked website.</p>
    <p>15.2 HandyHire will not be liable for any damages or loss arising in any way out of or in connection with or incidental to any information or third party service provided by or through any linked websites whether linked to or from HandyHire's Site.</p>
    <h4>16. General</h4>
    <p>16.1 Interpretation</p>
    <ul>
        <li>
            <p>(a) Headings are for reference purposes only and in no way define, limit, construe or describe the scope or extent of such section.</p>
        </li>
        <li>
            <p>(b) User agree that this Agreement may not be construed adversely against HandyHire solely because HandyHire prepared it.</p>
        </li>
        <li>
            <p>(c) The singular includes the plural and vice-versa.</p>
        </li>
        <li>
            <p>(d) A reference to a person includes any corporation or body corporate.</p>
        </li>
    </ul>
    <p>16.2 Definitions</p>
    <p>In this Agreement the following terms have the following meanings:</p>
    <ul>
        <li>
            <p>"Project" means the provision of goods and/or services by a Provider to a User that arises out of a Quote Request.</p>
        </li>
        <li>
            <p>"Quote Request" means a request made by the User using the System (whether made by phone, through the Site or otherwise) seeking offers for the provision of goods and/or services from Providers.</p>
        </li>
        <li>
            <p>"Provider" and/or &ldquo;Professional&rdquo; means any person or entity who is or may potentially be matched to a User Quote Request and except where the context requires otherwise includes any person engaged by such a person to provide goods or services in connection with a Project.</p>
        </li>
        <li>
            <p>"Provider Services" and/or &ldquo;Professional Services&rdquo; means the goods and/or services that Provider offers and/or provides to Users and proposals to provide.</p>
        </li>
        <li>
            <p>"Site" means the website operated by HandyHire at <a href="https://handyhire.co.za">www.handyhire.co.za</a> or on any affiliate website.</p>
        </li>
        <li>
            <p>"System" and/or &ldquo;Application&rdquo; means the system operated by HandyHire as described in this Agreement, and as developed by HandyHire from time to time.</p>
        </li>
    </ul>
    <p>16.3 Governing Law</p>
    <p>This Agreement shall be governed in all respects by the laws of South Africa.</p>
    <p>16.4 Severability</p>
    <p>The provisions of this Agreement are severable, and if any provision of this Agreement is held to be illegal, invalid or unenforceable, under present or future law, such provision may be removed and the remaining provisions shall be enforced.</p>
    <p>16.5 No Waiver</p>
    <p>HandyHire's failure to take action with respect to a breach by User or others does not waive HandyHire's right to take action with respect to subsequent or similar breaches.</p>
    <p>16.6 Entire Agreement</p>
    <p>This Agreement and those policies incorporated by reference herein set out the entire understanding and agreement between the parties with respect to the subject matter hereof.</p>
    <p>16.7 Survival</p>
    <p>Those clauses capable of surviving termination of this Agreement shall do so.</p>
    <p>16.8 Notices</p>
    <p>HandyHire may provide notices to User by simply posting the notice on the Site. This is in addition to any other mode of services permitted by law.</p>
    <p>16.9 Variation</p>
    <p>HandyHire may modify this Agreement at any time and such modifications shall be effective immediately upon posting the new or revised terms on the website.</p>


</div>

<hr />