@extends ('frontend.layouts.app')

@section ('title', 'Confirm - '.$professional->title)

@section('header')
    <div class="alert alert-success">
        <i class="fa pull-left fa-2x fa-lock"></i><span class="h5"> SECURE </span>
        Your connection to <b>HandyHire.co.za</b> is encrypted and safe.<br/><b>Whois:</b><i>South African company</i> - ONE INTERNET MEDIA CC t/a HandyHire - CK2008/162526/23 - <i class="fa fa-phone"></i> Tel: 021 917 1719
    </div>
@endsection

@section('content')

    <h1>Confirm Business Details</h1>

    <div id="greetings" class="alert bg-warning">
        <div class="h2 no-top-margin"><i class="fa fa-hand-spock-o "></i> Greetings!</div>
        <div class="h4">Before connecting with the potential customer, please confirm some of your company details.</div>
        These details are presented with your quote to the client and therefore accurate and complete information will ensure you stand the best chance of winning his business.
    </div>

    @if(!config('app.settings.enable_credits'))
        <p style="white-space: normal" class="label badge-success">FREE LEADS - as part of our launch leads are free until end of March 2017</p>
    @endif
    <div class="alert bg-info">
        <span class="h4">Free to Join</span>
        - It's completely free to list your business. We only ask a small fee when you wish to contact a lead.
    </div>

    {{ Form::model($professional, ['route' => ['public.professional.update_confirm', $professional->id], 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id'=>'professionalForm', 'data-toggle'=>'validator', 'data-disable'=>'false']) }}


    <div id="prowizard">

        <div style="display: none" class="">
            <div class="navbar-inner" >
                <div class="container">
                    <ul>
                        <li class="active"><a href="#info" data-toggle="tab">Business Info</a></li>
                        <li role="presentation"><a href="#branch" aria-controls="messages" role="tab" data-toggle="tab">Branches</a></li>
                        <li><a href="#profile" data-toggle="tab">Profile Page</a></li>
                        <li><a href="#system" data-toggle="tab">Settings</a></li>
                        <li><a href="#user" data-toggle="tab">User</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="jumbotron">
            <div class="jumbotron-contents">

                <div class="progress">
                    <div id="bar" class="bar progress-bar progress-bar-primary" role="progressbar">
                        <span></span>
                    </div>
                </div>

                <div class="tab-content">
                    @include('frontend.professional.includes.form_fields')

                    <div role="tabpanel" class="tab-pane" id="user">
                        <h2>User Details</h2>
                        <p>You will use these details to login and send quotes to interested customers.</p>

                        <div class="form-group">
                            {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::input('first_name', 'first_name', ($professional->user?$professional->user->first_name:null), ['class' => 'form-control', 'placeholder' => 'First Name', 'required']) }}
                                <div class="help-block with-errors"></div>
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::input('last_name', 'last_name', ($professional->user?$professional->user->last_name:null), ['class' => 'form-control', 'placeholder' => 'Last Name', 'required']) }}
                                <div class="help-block with-errors"></div>
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::email('email', ($professional->user?$professional->user->email:null), ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required']) }}
                                <div class="help-block with-errors"></div>
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::number('mobile', ($professional->user?$professional->user->mobile:null), ['class' => 'form-control mobile_input', 'placeholder' => 'eg. 0821234567', 'onKeyUp'=> 'if(this.value.length>10) $(this).val(this.value.substr(0, 10));']) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::input('password', 'password', null, ['class' => 'form-control', 'data-minlength'=>'6', 'placeholder' => trans('validation.attributes.frontend.password'), 'required']) }}
                                <div class="help-block with-errors"></div>
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'data-match'=>'#password', 'data-match-error'=>'Whoops, these don\'t match', 'placeholder' => trans('validation.attributes.frontend.password_confirmation'), 'required']) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('image', 'Photo of yourself', ['class' => 'col-md-4 control-label', 'accept'=>'image/*']) }}

                            <div class="col-md-6">
                                <i class="fa fa-user-circle default default-thumb"></i>
                                <div class="alert bg-info"><i class="fa fa-info-circle"></i> This photo is sent through with your quotes. Nothing sells it like a smile!</div>
                                {{ Form::file('image') }}
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!--box-->


        {{ Form::hidden('user_id', isset($user) && $user?$user->id:isset($logged_in_user) && $logged_in_user?$logged_in_user->id:0) }}

        {{ Form::hidden('confirmation_code', isset($confirmation_code) && $confirmation_code?$confirmation_code:null) }}

        {{ Form::hidden('project_id', isset($project_id) && $project_id?$project_id:null) }}



        <ul class="pager wizard">
            <li class="previous btn-lg"><a href="javascript:;">Back</a></li>
            <li id="nextbtn" class="next btn-lg"><a href="javascript:;">Next</a></li>
            <li class="finish btn-lg"><a href="javascript:;">Finish</a></li>
        </ul>

    </div>

    {{ Form::close() }}
@stop

@section('after-scripts-end')
    @include('frontend.professional.includes.form_js')
@append
