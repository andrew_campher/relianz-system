<div class="jumbotron">
    <div class="jumbotron-contents pr_left">
        @if($professional->user_id)
        <div class="pr_userimg">{!! $professional->user->getImageHtml('thumb', 'profile-image img-circle') !!}</div>
        <div class="row">
            <div class="col-xs-1"><i class="fa fa-user-circle"></i></div>
            <div class="col-xs-10">{{ $professional->user->name }}
            </div>
        </div>
        @endif
        <hr>
        <div class="row">
            <div class="col-xs-1"><i class="fa fa-calendar"></i></div>
            <div class="col-xs-10">Member since<br/>
                {{ $professional->created_at->format(config('app.date.short')) }} - {{ $professional->created_at->diffForHumans() }}
            </div>
        </div>


        @if($professional->company_type && $professional->company_type != 'Sole' && $professional->company_registration)
            <hr>
            <div class="row">
                <div class="col-xs-1"><i class="fa fa-drivers-license"></i></div>
                <div class="col-xs-10">Company<br/>
                    {{ $professional->company_type_label }} - {{ $professional->company_registration }}
                </div>
            </div>
        @endif
        <hr>

        <div class="pr_ratings">
            <div class="pr_left_reviews">
                {{ isset($professional->stats->reviews_count)?$professional->stats->reviews_count:0  }} Review{{ isset($professional->stats->reviews_count) && $professional->stats->reviews_count==1?'':'s' }}
            </div>
            <div class="pr_left_stars">
                {!!  $professional->overall_rating_stars !!} <b>{{ $professional->rating_overall_tot }}</b>
            </div>
        </div>
        <div class="pr_left_badges"></div>
    </div>
</div>

<p>
    @include('public.service.includes.request_service')
</p>
<div class="jumbotron">
    <div class="jumbotron-contents">
        <h4 class="no-top-margin">Address & Service Coverage</h4>
        <p><i class="fa fa-map-marker"></i> {{ $professional->area }}</p>
        <div id="promap"></div>

        <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google.api_maps_key') }}&callback=initMap&libraries=places&callback=initMap" async defer></script>

        <script>
            function initMap() {
                var mapCenter = new google.maps.LatLng({{ $professional->lat }}, {{ $professional->lng }});
                var map = new google.maps.Map(document.getElementById('promap'), {
                    'zoom': 11,
                    'center': mapCenter,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                });
                // Create marker
                var marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng({{ $professional->lat }}, {{ $professional->lng }}),
                    title: 'My Location'
                });

                // Add circle overlay and bind to marker
                var circle = new google.maps.Circle({
                    map: map,
                    fillOpacity: 0.1,
                    strokeWeight: 1,
                    strokeOpacity: 0.5,
                    radius: {{ $professional->distance_travelled*1000 }},    // 10 miles in metres
                    fillColor: '#6aa84f'
                });
                circle.bindTo('center', marker, 'position');
            }

        </script>
    </div>
</div>