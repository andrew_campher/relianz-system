<div class="project_list_item">
    <div class="h5"><a href="{{ route('public.professional.show', [$professional->id, $professional->slug]) }}"><span class="pull-left chat-image">{!! $professional->getLogoHtml('sm') !!}</span> {{ $professional->title }} {!! $professional->verify_label  !!}</a></div>

    {!!  $professional->overall_rating_stars !!} <b>{{ $professional->rating_overall_tot }}</b> from {{ isset($professional->stats->reviews_count)?$professional->stats->reviews_count:0  }} reviews

    <div><i class="fa fa-map-marker"></i> {!! $professional->area !!}</div>
</div>