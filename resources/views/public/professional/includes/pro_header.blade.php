@include('includes.partials.messages')

<div id="profile-header" class="">
    <div class="hero_image" style="background-image: url({{ $professional->hero_image ? $professional->hero_image_url : asset('img/frontend/'.config('app.defaults.hero_image')) }})">

        <div class="hero_headline {{ !$professional->hero_image ? 'no_hero' : '' }}">
            <div class="header_info text-white">
                <div class="container">

                    <div class="lg-square">{!! $professional->getLogoHtml('lg') !!}</div>

                    <div class="header_text">
                        <div class="pull-right"><a onclick="addContact({{ $professional->id }})" href="javascript:void(0)"><i class="contact{{ $professional->id }} fa {{ $logged_in_user && $logged_in_user->IsContact($professional->id)?'fa-heart':'fa-heart-o' }} fa-2x"></i></a></div>

                        <h1>
                            {{ $professional->title }} {!! $professional->verify_label  !!}
                        </h1>



                        {!!  $professional->overall_rating_stars !!} <b>{{ $professional->rating_overall_tot }}</b> from {{ isset($professional->stats->reviews_count)?$professional->stats->reviews_count:0  }} reviews

                        <div>{{ $professional->area }}</div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>

    @if(!$professional->approved)
        <div class="alert alert-warning"><h4 class="no-top-margin"><i class="fa fa-warning"></i> Pending Approval</h4>Your professional profile page is not yet approved.</div>
    @endif

    @if (access()->id() && (access()->id() == $professional->user_id || access()->user()->isRole('Administrator')))
        <div class="alert bg-info"><a href="{{ route('frontend.professional.edit', $professional->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit Profile</a></div>
    @endif
</div>