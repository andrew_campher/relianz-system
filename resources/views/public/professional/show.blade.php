@extends ('frontend.layouts.app')

@section ('title', $professional->title)

@section('header')

    @if(Request::input('claim') && $professional->status == 2)
        <div class="main_alert alert alert-warning">

            <h2 class="no-top-margin">Claim this listing</h2>
            <p>Please confirm that the below business is yours. If you are sure that it is yours then you may claim it.</p>

            <div class="panel alert">
                <div class="h5">Claim Process:</div> When claiming a listing we will mail a confirmation email to the email associated with it. <b>Please click the link in that email.</b>
            @if($professional->user_id)
                <a class="btn btn-danger" href="{{ route('frontend.auth.account.confirm.resend', $professional->user->id) }}">SEND CONFIRMATION EMAIL</a>
            @else

                <form action="{{ route('public.professional.claim_listing', $professional->id) }}" method="GET">

                    @if (config('access.captcha.registration'))
                        <div class="form-group">

                            {!! Form::captcha(null, ['data-callback'=> 'enableBtn']) !!}
                            {{ Form::hidden('captcha_status', 'true') }}

                        </div><!--form-group-->
                    @endif

                    <button type="submit" class="btn btn-danger">SEND CONFIRMATION EMAIL</button>

                    @section('after-scripts-end')
                        @if (config('access.captcha.registration'))
                            {!! Captcha::script() !!}
                        @endif
                    @append
                        <div class="clearfix"></div>
                </form>
            @endif
            </div>
            <div class="h5">If not yours: </div> Click here to go back to adding a new business: <a class="btn btn-primary" href="{{ route('frontend.auth.register_professional') }}">Add New Listing</a>
        </div>
    @endif


    @include('public.professional.includes.pro_header')


@endsection

@section('content')

    <div class="row profile-content">
        <div class="col-xs-12 col-sm-12 col-md-4">
            @include('public.professional.includes.left_column')
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8">
            @if($professional->description)
            <div class="jumbotron">
                <div class="jumbotron-contents">
                    <h4 class="no-top-margin">About Us</h4>
                    <p>{!!  $professional->description !!}</p>
                </div>
            </div>
            @endif

            @if($professional->media->count())
            <div class="jumbotron">
                <div class="jumbotron-contents">
                    <h4 class="no-top-margin">Gallery</h4>
                    @include('frontend.media.includes.partials.carousel', ['media' => $professional->media])
                </div>
            </div>
            @endif

            <div class="jumbotron">
                <div class="jumbotron-contents">
                    <h4 class="no-top-margin">Reviews</h4>
                    <a class="btn btn-sm btn-success heading_btn" href="{{ route('public.project_review.professional_review', $professional->id) }}"><i class="fa fa-pencil-square-o"></i> Write Review</a>
                    @include('frontend.project_review.include.reviews', ['reviews' => $professional->approved_reviews])
                </div>
            </div>

            <div class="jumbotron">
                <div class="jumbotron-contents">
                    <h4 class="no-top-margin">Services</h4>
                    @if($professional->services)
                        @foreach($professional->services As $service)
                            <a href="{{ route('public.service.show', $service->slug) }}" onclick="event.preventDefault(); fireModal('{{ route('public.project.wizard') }}?service={{ $service->id }}')" class="badge label-default">{{ $service->title }}</a>
                        @endforeach
                    @endif
                </div>
            </div>

        </div>
    </div>

@stop