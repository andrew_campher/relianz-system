@extends ('frontend.layouts.app')

@section ('title', $professional->title)

@section('header')


    @include('public.professional.includes.pro_header')


@endsection

@section('content')

    <div class="row profile-content">
        <div class="col-xs-12 col-sm-12 col-md-4">
            @include('public.professional.includes.left_column')
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8">

            <div class="jumbotron">
                <div class="jumbotron-contents">
                    <h4 class="no-top-margin">Reviews</h4>
                    <a class="btn btn-sm btn-success heading_btn" href="{{ route('public.project_review.professional_review', $professional->id) }}"><i class="fa fa-pencil-square-o"></i> Write Review</a>
                    @include('frontend.project_review.include.reviews', ['reviews' => $professional->approved_reviews])
                </div>
            </div>

        </div>
    </div>

@stop