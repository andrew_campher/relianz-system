<div class="project_list_item">
    <div class="h6"><div class="pull-left chat-image">{!! $project->user->getImageHtml('icon', '', true) !!}</div> {{ $project->title }}</div>
    {!! $project->status_label !!}
    <div><i class="fa fa-map-marker"></i> {{ $project->area }}</div>
</div>