@extends('frontend.layouts.app')

@section ('title', 'Project '.$project->title)


@section('content')


    @if(isset($invite_code) && $invite_code)
        <a data-spy="affix" data-offset-top="60" class="btn btn-block btn-success btn-lg" href=""><i class="fa fa-pencil-square"></i> Write Quote</a>
    @endif


    <h2 class="" id="project_heading">{{ $project->title }} <div class="pull-right h4">Quotes sent: {{ $project->quote_count }} out of {{ config('app.settings.quote_limit') }}</div></h2>

    <div class="clearfix"></div>

    <div class="h4">{{ $project->service ? $project->service->title: 'My'  }} Project</div>

    <div class="jumbotron">
        <div class="jumbotron-contents">

            <div class="user-mini-profile">
                <div class="text-center">{!! $project->user->getImageHtml('thumb', 'avatar-med img-circle', true) !!}</div>
                <div class="profile_side_det">
                    <div class="profile_side_name">
                        {!! $project->user->first_name !!} {!! $project->user->online_label !!}
                    </div>

                    <div class="profile_side_area padding15">{{ $project->area }}</div>
                </div>
            </div>

            @if (access()->isRole('Professional') && $project->status < 3)
                <div class="row project_stats">
                    <div class="col-xs-4">
                        <i class="fa fa-tag"></i> {{ $project->quote_count }} / {{ config('app.settings.quote_limit') }}
                        <div class="project_stats_txt">quotes sent</div>
                    </div>
                    <div class="col-xs-4">
                        <i class="fa fa-clock-o"></i> {{ $project->created_at->addDays(config('app.settings.request_days_expire'))->diffForHumans(null, true) }}
                        <div class="project_stats_txt">until request expires</div>
                    </div>
                    <div class="col-xs-4">
                        <div title="{{ trans('tooltips.account.credits.credit_fee_cost') }}" data-toggle="popover" data-trigger="hover" class="credit_cost_lg">{!! config('app.pricing.credit_icon') !!} {!!  $project->service->getFee() !!}</div>
                        <div class="project_stats_txt">credits</div>
                    </div>
                </div>
            @endif

            <div class="answer_item">
                <table class="table table-striped table-hover">
                    <tr>
                        <th style="border-top: none">Status</th>
                        <td style="border-top: none">{!!  $project->StatusLabel !!}</td>
                    </tr>
                    <tr>
                        <th>When</th>
                        <td>{!! $project->whenlabel !!} {{ $project->when_describe }}</td>
                    </tr>

                    @if($project->description)
                        <tr>
                            <th colspan="2">Description</th>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $project->description }}</td>
                        </tr>
                    @endif
                </table>

                @include('frontend.project.includes.partials.answers')

            </div>

            <div class="clearfix">

                @include('frontend.media.includes.partials.gallery', ['media' => $project->media])

            </div>
        </div>
    </div>


@endsection
