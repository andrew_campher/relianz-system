@extends('frontend.layouts.app')

@section ('title', 'Professionals in South Africa')

@section('header')
    @include('public.includes.partials.header')
@endsection

@section('content')

    <h1 class="padding15">All Professionals</h1>

    <hr>

    <div class="row">
    @foreach($occupations AS $occupation)
        <div class="col-xs-6 col-sm-4 service_item">

            <a href="{{ route('public.service.occupation',$occupation->slug ) }}" class="h5">{{ ucfirst($occupation->title) }}</a>

        </div>
    @endforeach
    </div>

    {{ $occupations->links() }}

@stop