<div class="request_service">
    <div class="h2">{{ isset($occupation) && $occupation?ucfirst($occupation->title):'' }} Professionals are waiting to quote you...</div>

    <a class="btn btn-lg btn-primary" onclick="openWizard(event, {{ isset($service) && $service?$service->id:0}}, '{{ isset($occupation) && $occupation?$occupation->title:''}}' )" href="{{ route('public.project.wizard',['searchstring'=>isset($occupation) && $occupation?$occupation->title:'']) }}">Request Quote</a>
</div>