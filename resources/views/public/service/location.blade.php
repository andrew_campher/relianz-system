@extends('frontend.layouts.app')

@section ('title', $location->title .' Professionals')

@section('header')
    @include('public.includes.partials.header')

@endsection

@section('content')


    <div class="row padding15">
        <div class="col-xs-12">

            <h1 class="padding15">Popular {{ $location->title }} Professionals</h1>

            <div class="row">
            @foreach($occupations AS $occupation)
                <div class="col-xs-6 col-sm-3">
                    <div class="jumbotron">
                        <div class="jumbotron-contents">
                            <a class="h4" href="{{ route('public.service.service_location', [$occupation->slug, $location->slug]) }}"><small>{{ $location->title }}</small><br> {{ ucfirst($occupation->title) }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
                <div class="col-xs-12">
                <a class="btn btn-success pull-right" href="{{ route('public.service.all_occupations') }}">All Professionals</a>
                </div>
            </div>

        </div><!-- col-md-10 -->

    </div><!--row-->

    @if(!$suburbs->isEmpty())
    <div class="row padding15">
        <div class="col-xs-12">

            <h1 class="padding15">Top {{ $location->title }} Service Suburbs</h1>

            <div class="row">
                @foreach($suburbs AS $suburb)
                    <div class="col-xs-6 col-sm-3">
                        <a class="h4" href="{{ route('public.service.location', $suburb->slug) }}">{{ ucfirst($suburb->title) }}</a>
                    </div>
                @endforeach
            </div>

        </div><!-- col-md-10 -->

    </div><!--row-->
    @endif

@endsection