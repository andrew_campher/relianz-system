@extends('frontend.layouts.app')

@section ('title', ucfirst(str_plural($occupation->title)).' on '.config('app.name'))

@section('header')
    @include('public.includes.partials.header')
@endsection

@section('left-column')
    <h1>{{ ucfirst(str_plural($occupation->title)) }}</h1>

    @include('public.service.includes.request_service')

    @if($occupation->services)
        <div class="h5">{{ ucfirst($occupation->title) }} Services</div>
        <p>{{ ucfirst(str_plural($occupation->title)) }} offering the following services. Please go ahead and click on the most relevant service below in order to request quotes.</p>

        <ul class="servicelist list-unstyled">
        @foreach($occupation->services AS $service)
            <li><a onclick="openWizard(event, {{ $service->id }})" href="{{ route('public.service.show',$service->slug ) }}" class=""><i class="fa fa-asterisk" aria-hidden="true"></i> {{ $service->title }}</a></li>
        @endforeach
        </ul>

    @endif

@endsection

@section('content')


    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>Best {{ ucfirst($occupation->title) }} Professionals</h2>
            @if(!$professionals->isEmpty())
                @foreach($professionals AS $professional)
                    <hr>
                    @include('public.professional.includes.list_item')

                @endforeach
                {{ $professionals->links() }}
            @endif
            <hr>
                <div class="h5">Are you a {{ ucfirst($occupation->title) }}?</div>
                <a class="btn btn-primary" href="{{ route('public.page', 'join-as-pro') }}"><i class="fa fa-plus-circle"></i> Join {{ config('app.name') }}</a>
        </div>
    </div>


    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>Latest {{ ucfirst($occupation->title) }} Projects</h2>
            @if(!$projects->isEmpty())
                @foreach($projects AS $project)
                    <hr>
                    @include('public.project.includes.list_item')

                @endforeach
            @else
                <div class="alert bg-info">There are currently no {{ $occupation->title }} projects</div>

            @endif
        </div>
    </div>


@endsection
