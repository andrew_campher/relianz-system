@extends('frontend.layouts.app')

@section ('title', $service->title.' - '.config('app.name'))

@section('sidebar')
    <h1>{{ $service->title }}</h1>

@endsection

@section('content')

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>{{ $service->title }} Professionals</h2>
            @if($service->professionals->count())
                @foreach($service->professionals AS $professional)
                    <hr>
                    @include('public.professional.includes.list_item')
                @endforeach
            @endif
        </div>
    </div>

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>{{ $service->title }} Projects</h2>
            @if($service->projects->count())
                @foreach($service->projects AS $project)
                    <hr>
                    @include('public.project.includes.list_item')
                @endforeach
            @else
                <div>There are currently no {{ $service->title }} projects</div>
            @endif
        </div>
    </div>



@endsection
