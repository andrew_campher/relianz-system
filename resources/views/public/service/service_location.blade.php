@extends('frontend.layouts.app')

@section ('title', ucfirst(str_plural($occupation->title)).' in '.$location->title.' - '.config('app.name'))

@section('header')
    <div class="header-map hidden-xs">
        <div id="promap"></div>
    </div>

@endsection

@section('left-column')
    <h1 class="no-top-margin">{{ ucfirst(str_plural($occupation->title)) }} in {{ $location->title }}</h1>

    @include('public.service.includes.request_service')

    <div class="h4">{{ ucfirst($occupation->title) }} Services</div>
    <p>These are the different <b>{{ strtolower($occupation->title) }} services</b> that our listed {{ strtolower(str_plural($occupation->title)) }} can give you a quote on. Please go ahead and click on the most relevant service below.</p>
    <ul class="servicelist list-unstyled">
        @foreach($occupation->services AS $service)
            <li><a onclick="openWizard(event, {{ $service->id }})" href="{{ route('public.service.show',$service->slug ) }}" class=""><i class="fa fa-asterisk" aria-hidden="true"></i> {{ $service->title }}</a></li>
        @endforeach
    </ul>
@endsection

@section('content')

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>{{ $location->title }} {{ ucfirst($occupation->title) }} Professionals</h2>
            @if(!$professionals->isEmpty())
                @foreach($professionals AS $professional)
                    <hr>
                    @include('public.professional.includes.list_item')

                @endforeach

                {{ $professionals->render() }}
            @endif
            <hr>
            <div class="h5">Are you a {{ ucfirst($occupation->title) }}?</div>
            <a class="btn btn-primary" href="{{ route('public.page', 'join-as-pro') }}"><i class="fa fa-plus-circle"></i> Join {{ config('app.name') }}</a>
        </div>
    </div>

    @if(!$projects->isEmpty())
    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>{{ $location->title }} {{ ucfirst($occupation->title) }} Projects</h2>
            @foreach($projects AS $project)
                <hr>
                @include('public.project.includes.list_item')
            @endforeach
        </div>
    </div>
    @endif


    <div class="jumbotron">
        <div class="jumbotron-contents pr_left">
            <h2>Nearby Areas to {{ $location->title }}</h2>
            <ul>
                @if(!$nearby_locations->isEmpty())
                    @foreach($nearby_locations AS $nearby_location)
                        <li>{{ link_to_route('public.service.service_location', $nearby_location->title.' '.round($nearby_location->distance,2).'km', [$occupation->slug, str_slug($nearby_location->slug)] ) }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>


@endsection

@section('after-scripts-end')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google.api_maps_key') }}&callback=initMap&libraries=places" async defer></script>

    <script>
        function initMap() {
            var mapCenter = new google.maps.LatLng({{ $location->lat }}, {{ $location->lng }});
            var map = new google.maps.Map(document.getElementById('promap'), {
                'zoom': 11,
                'center': mapCenter,
                'mapTypeId': google.maps.MapTypeId.ROADMAP
            });
            // Create marker
            var marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng({{ $location->lat }}, {{ $location->lng }}),
                title: '{{ str_plural($occupation->title) }} {{ $location->title }}'
            });
        }

    </script>
@endsection
