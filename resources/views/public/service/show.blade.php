@extends('frontend.layouts.app')

@section ('title', $service->title.' - '.config('app.name'))

@section('left-column')
    <h1 class="no-top-margin">{{ $service->title }}</h1>
    <div class="h4">Request a {{ $service->occupation?ucfirst($service->occupation->title):'Professionals' }} service</div>

    @include('public.service.includes.request_service', ['occupation' => $service->occupation])

@endsection

@section('content')

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>{{ $service->title }} Professionals</h2>
            @if($service->professionals->count())
                @foreach($service->professionals AS $professional)
                    <hr>
                    @include('public.professional.includes.list_item', ['occupation' => $service->occupation])

                @endforeach
            @endif
            <hr>
            <div class="h5">Are you a {{ $service->occupation?ucfirst($service->occupation->title):'Professional' }}?</div>
            <a class="btn btn-primary" href="{{ route('public.page', 'join-as-pro') }}"><i class="fa fa-plus-circle"></i> Join {{ config('app.name') }}</a>
        </div>
    </div>

    <div class="jumbotron">
        <div class="jumbotron-contents">
            <h2>{{ $service->title }} Projects</h2>
            @if($service->projects->count())
                @foreach($service->projects AS $project)
                    <hr>
                    @include('public.project.includes.list_item')
                @endforeach
            @else
                <div>There are currently no {{ $service->title }} projects</div>
            @endif
        </div>
    </div>



@endsection
