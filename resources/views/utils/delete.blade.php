<form style="display:inline" action="{{  $url or Request::url() }}" method="POST">
{{ method_field('DELETE') }}
{{ csrf_field() }}
    <button type="submit" class="{{$class or 'btn btn-danger'}}">
        <i class="fa fa-trash"></i>
    </button>
</form>