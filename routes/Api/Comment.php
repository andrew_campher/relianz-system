<?php

use Illuminate\Http\Request;

Route::group(['namespace' => 'Comment'], function () {


    Route::get('comment/{model}/{itemid}', 'ApiCommentController@index');


    Route::get('comment/{comment}', 'ApiCommentController@show');


    Route::post('comment/store', 'ApiCommentController@store')->name('store');

    #UPDATE
    Route::patch('comment/{id}', function (Request $request, $id) {
        \Log::info('update');
        \App\Models\Comment\Comment::findOrFail($id)->update(['body' => $request->input(['body'])]);
    });

    Route::delete('comment/{id}', function ($id) {
        return \App\Models\Comment\Comment::destroy($id);
    });


});