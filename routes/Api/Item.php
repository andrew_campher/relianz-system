<?php

Route::group(['namespace' => 'Item', 'as' => 'item.'], function () {

    Route::post('update_pricing', 'ApiItemController@update_pricing')->name('update_pricing');

    Route::post('update', 'ApiItemController@update')->name('update');

    Route::post('get_item', 'ApiItemController@get_item')->name('get_item');

});