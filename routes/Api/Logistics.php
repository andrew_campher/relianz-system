<?php

Route::group(['namespace' => 'Logistics', 'as' => 'logistics.'], function () {

    Route::post('change_status', 'ApiLogisticsController@change_status')->name('change_status');

});