<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'api.'
 */
Route::group(['namespace' => 'Media'], function() {

    Route::resource('media', 'ApiMediaController');

});
