<?php

Route::group(['namespace' => 'Notification'], function () {

    Route::get('notifications/mark_all_read', 'ApiNotificationController@mark_all_read');

    Route::get('notifications/{notification}/mark_read', 'ApiNotificationController@mark_read');

    Route::get('notifications/{user}', 'ApiNotificationController@index');


});