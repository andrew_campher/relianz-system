<?php

Route::group(['namespace' => 'Pricing'], function () {
    Route::post('pricing/update', 'ApiPricingController@update')->name('pricing.update');

    Route::post('pricing/get_item', 'ApiPricingController@get_item')->name('pricing.get_item');

});
