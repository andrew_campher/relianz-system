<?php

Route::group(['namespace' => 'Widgets', 'as' => 'widgets.','prefix' => 'widgets'], function () {

    /**
     * Items
     */

    Route::get('ship_tracking', 'ApiWidgetsController@ship_tracking')->name('ship_tracking');

    Route::get('item_sales_increase', 'ApiWidgetsController@item_sales_increase')->name('item_sales_increase');

    Route::get('item_sales_decrease', 'ApiWidgetsController@item_sales_decrease')->name('item_sales_decrease');

    Route::get('get_latest_nostocks', 'ApiWidgetsController@get_latest_nostocks')->name('get_latest_nostocks');

    Route::get('credits_tracking', 'ApiWidgetsController@credits_tracking')->name('credits_tracking');

});