<?php

/**
 * All route names are prefixed with 'admin.access'
 */

Route::group(['namespace' => 'Account'], function () {
    /**
     * For DataTables
     */
    Route::post('account/get', 'AccountTableController')->name('account.get');

    /**
     * User CRUD
     */
    Route::resource('account', 'AccountController');

});

