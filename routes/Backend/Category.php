<?php

/**
 * All route names are prefixed with 'services'
 */
Route::group(['namespace' => 'Category'], function () {
    /**
     * For Action
     */
    Route::post('category/action', 'CategoryController@action')->name('category.action');

    /**
     * Category Status'
     */
    Route::get('category/deactivated', 'CategoryController@getDeactivated')->name('category.deactivated');
    Route::get('category/deleted', 'CategoryController@getDeleted')->name('category.deleted');

    /**
     * User CRUD
     */
    Route::resource('category', 'CategoryController');
    /**
     * Specific Category
     */
    Route::group(['prefix' => 'category/{category}'], function () {
        // Status
        Route::get('mark/{status}', 'CategoryStatusController@mark')->name('category.mark')->where(['status' => '[0,1]']);
    });

    /**
     * Deleted Categories
     */
    Route::group(['prefix' => 'category/{deletedCategory}'], function () {
        Route::get('delete', 'CategoryStatusController@delete')->name('category.delete-permanently');
        Route::get('restore', 'CategoryStatusController@restore')->name('category.restore');
    });
});

