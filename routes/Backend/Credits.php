<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::resource('credits', 'CreditsController');

Route::get('credits/report/{credit}', 'CreditsController@report')->name('credits.report');