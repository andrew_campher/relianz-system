<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::get('customers/notes', 'CustomersController@notes')->name('customers.notes');

Route::resource('customers', 'CustomersController');

Route::get('customers/report/{customer}', 'CustomersController@report')->name('customers.report');
