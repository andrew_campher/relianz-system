<?php

/**
 * All route names are prefixed with 'admin.'
 */

Route::get('items/nostocks', 'ItemsController@nostocks')->name('items.nostocks');

Route::resource('items', 'ItemsController');

Route::get('items/report/{item}', 'ItemsController@report')->name('items.report');

