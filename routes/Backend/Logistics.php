<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::get('logistics/collections', 'LogisticsController@collections')->name('logistics.collections');

Route::get('logistics', 'LogisticsController@index')->name('logistics');

