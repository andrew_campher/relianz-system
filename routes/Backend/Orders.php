<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::get('orders/showso/{sonumber}', 'OrdersController@showso')->name('orders.showso');

Route::get('orders/showbo/{ID}', 'OrdersController@showbo')->name('orders.showbo');

Route::get('orders/backorders/{instock}', 'OrdersController@backorders')->name('orders.backorders');

Route::resource('orders', 'OrdersController');


