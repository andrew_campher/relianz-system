<?php

/**
 * All route names are prefixed with 'admin.'
 */

    Route::get('pricing', 'PricingController@index')->name('pricing');

    Route::get('pricing/specials', 'PricingController@specials')->name('pricing.specials');
