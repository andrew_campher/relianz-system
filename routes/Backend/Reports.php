<?php

/**
 * All route names are prefixed with 'admin.'
 */

Route::get('reports/index', 'ReportsController@index')->name('reports.index');

Route::get('reports/monthtodate', 'ReportsController@monthtodate')->name('reports.monthtodate');

Route::get('reports/sales', 'ReportsController@sales')->name('reports.sales');

Route::get('reports/customers', 'ReportsController@customers')->name('reports.customers');

Route::get('reports/items', 'ReportsController@items')->name('reports.items');
