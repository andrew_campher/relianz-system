<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::resource('suppliers', 'SuppliersController');

Route::get('suppliers/report/{supplier}', 'SuppliersController@report')->name('suppliers.report');