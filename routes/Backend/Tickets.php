<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::get('tickets', 'TicketsController@index')->name('tickets');
