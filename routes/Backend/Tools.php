<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::get('tools/dropoffs', 'ToolsController@dropoffs')->name('tools.dropoffs');

Route::get('tools/sittingstock', 'ToolsController@sittingstock')->name('tools.sittingstock');
