<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Account'], function() {

    Route::get('account/{account}/credits_purchase', 'AccountController@credits_purchase')->name('account.credits_purchase');

    Route::resource('account', 'AccountController', ['except' => [
         'create', 'update', 'destroy', 'store'
    ]]);

});
