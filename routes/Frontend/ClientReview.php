<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */

Route::group(['namespace' => 'ClientReview'], function() {

    #Client REVIEW Create route
    Route::get('quote/{quote}/client_review/create', 'ClientReviewController@create')->name('quote.client_review.create');

    Route::resource('client_review', 'ClientReviewController', ['except' => [
        'destroy'
    ]]);

});
