<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Comment'], function() {

    Route::resource('quote.comment', 'CommentController', ['parameters' => [
        'quote_id'
    ]]);

});
