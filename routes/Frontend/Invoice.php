<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Invoice'], function() {

    Route::get('invoice/{invoice}/download', 'InvoiceController@download')->name('invoice.download');

    Route::resource('invoice', 'InvoiceController', ['except' => [
         'update', 'destroy'
    ]]);

});
