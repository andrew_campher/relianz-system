<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Media'], function() {

    Route::get('media/{media}', 'MediaController@show')->name('media.show');

    #For some reason I had to define the actual resource routes as the resource definition did not want to work (Model object was empty)
    Route::delete('media/{media}', 'MediaController@destroy')->name('media.destroy');

    Route::get('media/croptool', 'MediaController@croptool')->name('media.croptool');

});
