<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Notification'], function() {

    Route::get('notifications/all', 'NotificationController@all')->name('notification.all');

});
