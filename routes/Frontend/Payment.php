<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Payment'], function() {

    Route::post('payment/process', 'PaymentController@process')->name('payment.process');

});
