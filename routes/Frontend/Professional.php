<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Professional'], function() {


    Route::group(['as' => 'professional.', 'middleware' => 'access.routeNeedsProfessional'], function() {
        Route::get('professional/dashboard', 'ProfessionalDashboardController@index')->name('dashboard');
    });

    #Quotes
    Route::get('professional/{professional}/quotes/{status?}', 'ProfessionalQuotesController@quotes')->name('professional.quotes');

    /**
     * BRANCHES
     */
    Route::get('professional/{professional}/edit_branch/{branch}', 'ProfessionalBranchController@edit_branch')->name('professional.edit_branch');
    Route::get('professional/{professional}/remove_branch/{branch}', 'ProfessionalBranchController@remove_branch')->name('professional.remove_branch');


    Route::resource('professional', 'ProfessionalController', ['except' => [
        'show', 'destroy', 'index', 'claim_listing'
    ]]);

});