<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Project'], function() {


    Route::post('project/{project}/update_status', 'ProjectController@update_status')->name('project.update_status');

    Route::get('project/{project}/status_change', 'ProjectController@status_change')->name('project.status_change');

    Route::get('project/{project}/manage', 'ProjectController@manage')->name('project.manage');

    Route::get('project/all/{status?}', 'ProjectController@all')->name('project.all');

    #Client Review

    Route::get('project/{project}', 'ProjectController@show')->where('project', '[0-9]+')->name('project.show');

});
