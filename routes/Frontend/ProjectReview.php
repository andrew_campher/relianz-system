<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */



Route::group(['namespace' => 'ProjectReview'], function() {
    
    #Project REVIEW Create route
    Route::get('quote/{quote}/project_review/create', 'ProjectReviewController@create')->name('quote.project_review.create');

    Route::resource('project_review', 'ProjectReviewController', ['except' => [
        'create', 'destroy'
    ]]);

});
