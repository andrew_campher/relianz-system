<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Quote'], function() {

    Route::post('quote/{quote}/hired', 'QuoteController@hired')->name('quote.hired');

    Route::get('quote/{quote}/decline', 'QuoteController@decline')->name('quote.decline');
    Route::post('quote/{quote}/decline_store', 'QuoteController@decline_store')->name('quote.decline_store');


    #Verify for the client to click and verify the hire
    Route::get('quote/{quote}/confirm', 'QuoteController@confirm')->name('quote.confirm');

    #Review work
    Route::get('quote/{quote}/review', 'QuoteController@review')->name('quote.review');

    Route::resource('quote', 'QuoteController', ['except' => [
         'create', 'destroy', 'edit'
    ]]);

});
