<?php
/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'User', 'as' => 'user.'], function() {
    /**
     * User Dashboard Specific
     */
    Route::get('my-projects', 'DashboardController@index')->name('dashboard');

    Route::get('user/contacts', 'UserController@contacts')->name('contacts');

    /**
     * User Account Specific
     */
    Route::get('user', 'UserController@index')->name('account');

    /**
     * User Profile Specific
     */
    Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
});
