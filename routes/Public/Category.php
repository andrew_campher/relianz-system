<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'public.'
 */
#Category
Route::group(['namespace' => 'Category', 'as' => 'category.'], function () {

    Route::get('{category_slug}/services', 'CategoryPublicController@services')
        ->where(['category_slug' => '[a-z-]+'])
        ->name('services');
});
