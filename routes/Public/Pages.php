<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'frontend.'
 */
#Project
Route::group(['namespace' => 'Page'], function () {

    Route::get('page/{page}', 'PagePublicController@page')
        ->where(['page' => '[a-z-_]+'])
        ->name('page');

    Route::post('page/contact_submit', 'PagePublicController@contact_submit')
        ->name('page.contact_submit');

});
