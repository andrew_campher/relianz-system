<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Professional'], function() {

    Route::get('professional/{professional}', function ($professional) {
        $pro = \App\Models\Professional\Professional::find($professional);
        return redirect()->route('public.professional.show', [$pro->id, $pro->slug]);
    })->where(['professional' => '[0-9]+']);

    /**
     * CLAIM
     */
    Route::get('claim_listing/{professional}', 'ProfessionalPublicController@claimListing')->where(['professional' => '[0-9]+'])->name('professional.claim_listing');
    /*
  * Details page
  * eg professional/11-pro-plumber
  */
    Route::get('professional/{professional}-{slug}', 'ProfessionalPublicController@show')
        ->where(['professional' => '[0-9]+', 'slug' => '[a-z-]+'])
        ->name('professional.show');

    Route::get('professional/{professional}-{slug}/reviews', 'ProfessionalPublicController@show_reviews')
        ->where(['professional' => '[0-9]+', 'slug' => '[a-z-]+'])
        ->name('professional.show_reviews');


    Route::patch('professional/{professional}/update_confirm', 'ProfessionalPublicController@update_confirm')->name('professional.update_confirm');


    Route::get('professional/{professional}/{hash}', 'ProfessionalPublicController@edit')
        ->where(['professional' => '[0-9]+'])
        ->name('professional.edit');

});
