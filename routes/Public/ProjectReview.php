<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'ProjectReview'], function() {

    Route::get('professional/{professional}/professional_review', 'ProjectReviewPublicController@professional_review')->name('project_review.professional_review');
    Route::post('professional/{professional}/professional_review/store', 'ProjectReviewPublicController@store')->name('project_review.store');

});
