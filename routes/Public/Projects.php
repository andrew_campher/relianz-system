<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'frontend.'
 */
#Project
Route::group(['namespace' => 'Project', 'as' => 'project.'], function () {

    Route::get('project/wizard', 'ProjectWizardController@wizard')->name('wizard');

    Route::get('project/select_service', 'ProjectWizardController@select_service')->name('select_service');

    Route::get('project/{project}/details/{professional}/{hash}', 'ProjectPublicController@details')->name('details');

    Route::post('project/store', 'ProjectWizardController@store')->name('store');

    Route::get('project/{project}/quote', 'ProjectPublicController@quote')->name('quote');

});
