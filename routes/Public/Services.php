<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'frontend.'
 */
#Service
Route::group(['namespace' => 'Service'], function () {




    Route::get('professionals/all', 'ServicePublicController@all_occupations')->name('service.all_occupations');


    /**
     * Location Details PAge
     */
    Route::get('{location_slug}-services', 'ServicePublicController@location')
        ->where(['location_slug' => '[a-z-]+'])
        ->name('service.location');


    /**
     * Occupation Main Page
     */
    Route::get('{pro_slug}', 'ServicePublicController@occupation')
        ->where(['pro_slug' => '[a-z-]+'])
        ->name('service.occupation');

    /**
     * Occupation Main Page
     */
    Route::get('service/{slug}', 'ServicePublicController@show')
        ->where(['slug' => '[a-z-]+'])
        ->name('service.show');




    /**
     * Location Services Details PAge
     */
    Route::get('{pro_slug}/in/{location}', 'ServicePublicController@service_location')
        ->where(['pro_slug' => '[a-z-]+', 'location' => '[a-z-]+'])
        ->name('service.service_location');


    /**
     * Location Details PAge
     */
    Route::get('{pro_slug}/service/{service}', 'ServicePublicController@pro_service')
        ->where(['pro_slug' => '[a-z-]+', 'service' => '[a-z-]+'])
        ->name('service.pro_service');

});