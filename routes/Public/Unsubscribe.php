<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'public.'
 */
Route::group(['namespace' => 'Unsubscribe'], function() {

    Route::get('unsubscribe/{notification}/{email}', 'UnsubscribeController@remove')
        ->name('unsubscribe.remove');

});
