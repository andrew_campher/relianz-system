<?php
/**
 * PUBLIC access
 * All route names are prefixed with 'frontend.'
 */
#Project
Route::group(['namespace' => 'User'], function () {

    Route::get('user/is_logged_in', 'UserController@isLoggedIn')->name('user.is_logged_in');

});
