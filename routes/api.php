<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function () {
    return 'Welcome to API';
});

/**
 * /Api namespace
 */
Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {

    /**
     * PUBLIC ROUTES - No authentication required
     */
    Route::group(['namespace' => 'User', 'as' => 'user'], function () {
        Route::post('auth/is_email_in_use', [
            'as' => 'isEmailInUse',
            'uses' => 'ApiUserController@isEmailInUse']);

        Route::post('auth/is_mobile', [
            'as' => 'isMobile',
            'uses' => 'ApiUserController@isMobile']);
    });

    require(__DIR__ . '/Api/Pricing.php');

    require(__DIR__ . '/Api/Logistics.php');

    require(__DIR__ . '/Api/Item.php');

    require(__DIR__ . '/Api/Widgets.php');

    /**
     * SECURED ROUTES - requires token authentication
     */

    Route::group(['middleware' => 'auth:api'], function () {

        require(__DIR__ . '/Api/User.php');

        #Comments
        require(__DIR__ . '/Api/Comment.php');

        require(__DIR__ . '/Api/Notification.php');

        require(__DIR__ . '/Api/Media.php');


    });


});

/*Route::get('user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');*/
