<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend
 */

// Switch between the included languages
#Route::get('lang/{lang}', 'LanguageController@swap');

/* ----------------------------------------------------------------------- */

/**
 * FRONTEND Routes
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'
 */
Route::group(['namespace' => 'Frontend'], function () {

    /**
     * Authentication required Routes
     * These frontend controllers require the user to be logged in
     * All route names are prefixed with 'frontend.'
     */
    Route::group(['as' => 'frontend.', 'middleware' => 'auth'], function () {
        /**
         * DIRECT ROUTES - REQUIRE LOGGED
         *  Chat routes
         */

        require(__DIR__ . '/Frontend/User.php');
        require(__DIR__ . '/Frontend/Project.php');
        require(__DIR__ . '/Frontend/Professional.php');
        require(__DIR__ . '/Frontend/Service.php');
        require(__DIR__ . '/Frontend/Quote.php');
        require(__DIR__ . '/Frontend/Comment.php');
        require(__DIR__ . '/Frontend/Media.php');
        require(__DIR__ . '/Frontend/ProjectReview.php');
        require(__DIR__ . '/Frontend/ClientReview.php');
        require(__DIR__ . '/Frontend/Payment.php');
        require(__DIR__ . '/Frontend/Invoice.php');
        require(__DIR__ . '/Frontend/Account.php');
        require(__DIR__ . '/Frontend/Notification.php');
    });




});

/* ----------------------------------------------------------------------- */

/**
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
	/**
	 * These routes need view-backend permission
	 * (good if you want to allow more than one group in the backend,
	 * then limit the backend features by different roles or permissions)
	 *
	 * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
	 */
	require (__DIR__ . '/Backend/Dashboard.php');
    require (__DIR__ . '/Backend/Items.php');
    require (__DIR__ . '/Backend/Orders.php');
    require (__DIR__ . '/Backend/Customers.php');
    require (__DIR__ . '/Backend/Logistics.php');
    require (__DIR__ . '/Backend/Pricing.php');
    require (__DIR__ . '/Backend/Suppliers.php');
    require (__DIR__ . '/Backend/Reports.php');
    require (__DIR__ . '/Backend/Tools.php');
    require (__DIR__ . '/Backend/Tickets.php');
    require (__DIR__ . '/Backend/Calc.php');
    require (__DIR__ . '/Backend/Access.php');
    require (__DIR__ . '/Backend/Account.php');
    require (__DIR__ . '/Backend/Category.php');
	require (__DIR__ . '/Backend/Search.php');
    require (__DIR__ . '/Backend/Credits.php');
    require (__DIR__ . '/Backend/Links.php');
	require (__DIR__ . '/Backend/LogViewer.php');
});


/**
 * PUBLIC ROUTES
 */

Route::group(['namespace' => 'Frontend'], function () {

    #Core jazz... dont mess with it for now
    require(__DIR__ . '/Frontend/Access.php');


    Route::group(['as' => 'public.'], function () {

        Route::get('/', 'FrontendController@index')->name('index');

        require(__DIR__ . '/Public/Pages.php');

        require(__DIR__ . '/Public/Category.php');
        require(__DIR__ . '/Public/User.php');
        require(__DIR__ . '/Public/ProjectReview.php');
        require(__DIR__ . '/Public/Professionals.php');
        require(__DIR__ . '/Public/Projects.php');
        require(__DIR__ . '/Public/Services.php');

    });

});